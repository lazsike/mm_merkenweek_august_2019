(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.grey_field = function() {
	this.initialize(img.grey_field);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,320,84);


(lib.intro_img = function() {
	this.initialize(img.intro_img);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,562,228);


(lib.logobalk = function() {
	this.initialize(img.logobalk);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,716,92);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.vanaf = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "11px 'Geomanist'");
	this.dynText.name = "dynText";
	this.dynText.textAlign = "right";
	this.dynText.lineHeight = 1;
	this.dynText.lineWidth = 86;
	this.dynText.parent = this;
	this.dynText.setTransform(88,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.vanaf, new cjs.Rectangle(0,0,90,15), null);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.grey_field();
	this.instance.parent = this;
	this.instance.setTransform(-320,-42);

	this.instance_1 = new lib.grey_field();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,-42);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-320,-42,640,84);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.grey_field();
	this.instance.parent = this;
	this.instance.setTransform(0,-42);

	this.instance_1 = new lib.grey_field();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-320,-42);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-320,-42,640,84);


(lib.text3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "13px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 12;
	this.dynText.lineWidth = 141;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text3, new cjs.Rectangle(0,0,145,25.6), null);


(lib.text2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "24px 'Geomanist'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 26;
	this.dynText.lineWidth = 596;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text2, new cjs.Rectangle(0,0,600,28), null);


(lib.text1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "24px 'Geomanist'", "#E31E26");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 26;
	this.dynText.lineWidth = 596;
	this.dynText.parent = this;
	this.dynText.setTransform(2,-26);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text1, new cjs.Rectangle(0,-28,600,28), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgDgEgCIgMgGQgSgJAAgPQAAgOAJgIQAKgHAOAAQAOAAAKAHQAJAHACAHIgXANQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape.setTransform(149.675,18.75);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgfAkQgKgKAAgRIAAg2IAhAAIAAA3QAAAJAIAAQAJAAAAgJIAAg3IAhAAIAAA2QAAARgLAKQgLAKgUAAQgTAAgMgKg");
	this.shape_1.setTransform(141.4,18.825);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgQAuIAAhBIgRAAIAAgZIBDAAIAAAZIgSAAIAABBg");
	this.shape_2.setTransform(133.175,18.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgDgEgCIgMgGQgSgJAAgPQAAgOAJgIQAKgHAOAAQAOAAAKAHQAJAHACAHIgXANQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_3.setTransform(125.775,18.75);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgeAkQgLgKAAgRIAAg2IAhAAIAAA3QAAAJAIAAQAJAAAAgJIAAg3IAhAAIAAA2QAAARgLAKQgLAKgUAAQgTAAgLgKg");
	this.shape_4.setTransform(117.5,18.825);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AghAiQgNgNAAgVQAAgTAOgOQAOgNAUAAQAZAAAOAOIgSAUQgIgGgJAAQgHAAgFAFQgFAGAAAHQAAAJAFAFQAFAGAHAAQAHAAAEgCIAAgEIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgNg");
	this.shape_5.setTransform(107.975,18.75);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgfAkQgKgKAAgRIAAg2IAhAAIAAA3QAAAJAIAAQAJAAAAgJIAAg3IAhAAIAAA2QAAARgLAKQgLAKgUAAQgTAAgMgKg");
	this.shape_6.setTransform(98.65,18.825);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghaIAfAAIAgBagAAEAJIgEgSIgFASIAJAAg");
	this.shape_7.setTransform(89.225,18.75);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgHAuIAAg+IgIACIgHgYIAVgGIAYAAIAABag");
	this.shape_8.setTransform(79.2,18.75);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgIAuIAAg+IgIACIgGgYIAVgGIAYAAIAABag");
	this.shape_9.setTransform(73.95,18.75);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAYAuIAAgtIgJAlIgdAAIgJglIAAAtIgfAAIAAhaIApAAIANAzIAPgzIAoAAIAABag");
	this.shape_10.setTransform(62.95,18.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AggA4IAnhvIAZAAIgmBvg");
	this.shape_11.setTransform(53.85,19.125);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgQAuIAAhBIgRAAIAAgZIBDAAIAAAZIgSAAIAABBg");
	this.shape_12.setTransform(47.075,18.75);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgZAmQgHgHgDgKIAcgHQABAGAFAAQAFAAAAgGQAAgDgCgCQAAAAgBAAQAAgBgBAAQAAAAAAAAQgBAAAAAAQgEAAgCADIgagEIACg0IA9AAIAAAYIghAAIgBAHIAIgBQANAAAKAIQAJAIAAANQAAAOgKAJQgLAJgPAAQgPAAgKgIg");
	this.shape_13.setTransform(37.3,18.825);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASArIAAgrIAhAAIAABag");
	this.shape_14.setTransform(26.425,18.75);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghaIAfAAIAgBagAAEAJIgEgSIgFASIAJAAg");
	this.shape_15.setTransform(16.925,18.75);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgPAuIgihaIAkAAIAOA0IAOg0IAiAAIggBag");
	this.shape_16.setTransform(7.05,18.75);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgeAxIAAhhIAjAAIAABFIAaAAIAAAcg");
	this.shape_17.setTransform(211.175,7.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_18.setTransform(204.175,7.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_19.setTransform(197.225,7.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgRAxIgkhhIAnAAIAOA5IAQg5IAlAAIgjBhg");
	this.shape_20.setTransform(188.35,7.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgeAxIAAhhIAjAAIAABFIAaAAIAAAcg");
	this.shape_21.setTransform(176.925,7.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_22.setTransform(169.925,7.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_23.setTransform(162.975,7.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAKAxIAAgjIgTAAIAAAjIgkAAIAAhhIAkAAIAAAjIATAAIAAgjIAkAAIAABhg");
	this.shape_24.setTransform(154.5,7.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_25.setTransform(142.875,7.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_26.setTransform(133.075,7.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E90000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAWAAQAaAAAQAQIgTAVQgJgGgJAAQgIgBgGAGQgFAGgBAIQAAAJAGAHQAGAFAIAAQAHAAAFgDIAAgDIgLAAIAAgXIAqAAIAAArQgSAQgeAAQgXAAgOgPg");
	this.shape_27.setTransform(119.55,7.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E90000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_28.setTransform(109.375,7.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E90000").s().p("AgRAxIAAhhIAjAAIAABhg");
	this.shape_29.setTransform(102.2,7.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E90000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_30.setTransform(96.1,7.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E90000").s().p("AAFAxIgNgfIAAAfIgjAAIAAhhIArAAQARAAAKAJQAJAJAAAOQAAAIgEAHQgEAFgHAEIAXApgAgIgHIAEAAQAEgBAAgCQABgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAgDgEAAIgEAAg");
	this.shape_31.setTransform(88.15,7.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E90000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_32.setTransform(77.875,7.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E90000").s().p("AAHAxIgQgoIAAAoIgkAAIAAhhIAkAAIAAAjIAPgjIAnAAIgYAvIAZAyg");
	this.shape_33.setTransform(67.625,7.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E90000").s().p("AANAsQgIgIAAgKQAAgLAIgHQAIgHAMAAQAMAAAHAHQAIAHAAALQAAAKgIAIQgHAHgMAAQgMAAgIgHgAAeAaQAAAEADAAQADAAAAgEQAAgGgDAAQgDAAAAAGgAgxAxIBHhhIAbAAIhHBhgAgzgHQgJgHABgLQgBgKAJgIQAHgHAMAAQAMAAAIAHQAHAIAAAKQAAALgHAHQgIAHgMAAQgMAAgHgHgAgjgZQAAAFADABQAEgBgBgFQABgEgEAAQgDAAAAAEg");
	this.shape_34.setTransform(53.65,7.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E90000").s().p("AgbApQgIgIgDgLIAfgGQAAAFAGAAQAFABAAgHQAAgDgCgCQAAAAgBgBQAAAAgBgBQAAAAAAAAQgBAAAAAAQgEAAgCAEIgcgEIACg5IBBAAIAAAaIgjAAIgBAIIAIgBQAPAAAJAIQALAJAAAOQAAAPgLAKQgMAKgQAAQgQAAgLgJg");
	this.shape_35.setTransform(43.425,7.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E90000").s().p("AgjAyIAAgMQAAgKAFgJQAFgGAJgIIANgLQAGgFgBgFQAAgFgCAAQgFAAgCAIIgegGQACgMAJgJQAKgJAQAAQAQAAALAIQALAIgBAQQAAAPgQAMIgLAIQgFADAAACIAhAAIAAAbg");
	this.shape_36.setTransform(35.4,7.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#E90000").s().p("AgQAxIAAhFIgUAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_37.setTransform(24.75,7.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#E90000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_38.setTransform(15.325,7.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#E90000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_39.setTransform(5.9,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,216.7,28.6), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgYAnQgIgHgDgIIAYgNQAFAJAGAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgDgEgCIgMgGQgSgIAAgQQAAgOAJgHQAKgIAOAAQAOAAAKAIQAJAGACAHIgXAOQgEgJgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape.setTransform(154.725,29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFAuIgMgeIAAAeIggAAIAAhaIAnAAQAQAAAJAIQAJAIAAANQAAAIgEAGQgEAFgHAEIAVAmgAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQAAgBAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAIgEAAg");
	this.shape_1.setTransform(147.225,29.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAIIAVAAIAAAWIgVAAIAAAJIAXAAIAAAag");
	this.shape_2.setTransform(139.575,29.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgQAuIAAhBIgRAAIAAgZIBDAAIAAAZIgSAAIAABBg");
	this.shape_3.setTransform(132.725,29.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASArIAAgrIAhAAIAABag");
	this.shape_4.setTransform(124.425,29.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgQAuIAAhaIAhAAIAABag");
	this.shape_5.setTransform(117.75,29.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAFAuIgMgeIAAAeIggAAIAAhaIAnAAQAQAAAJAIQAJAIAAANQAAAIgEAGQgEAFgHAEIAVAmgAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQAAgBAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAIgEAAg");
	this.shape_6.setTransform(111.925,29.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgjAuIAAhaIAlAAQAQAAAJAIQAJAIAAAOQAAAOgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgCAAgDQAAgDgCgCQgBgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAIgDAAg");
	this.shape_7.setTransform(103.825,29.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASArIAAgrIAhAAIAABag");
	this.shape_8.setTransform(92.675,29.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAIIAVAAIAAAWIgVAAIAAAJIAXAAIAAAag");
	this.shape_9.setTransform(84.775,29.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASArIAAgrIAhAAIAABag");
	this.shape_10.setTransform(74.375,29.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAIIAVAAIAAAWIgVAAIAAAJIAXAAIAAAag");
	this.shape_11.setTransform(66.475,29.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAFAuIgMgeIAAAeIggAAIAAhaIAnAAQAQAAAJAIQAJAIAAANQAAAIgEAGQgEAFgHAEIAVAmgAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQAAgBAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAIgEAAg");
	this.shape_12.setTransform(59.475,29.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgkAiQgNgNAAgVQAAgTANgOQAOgNAWAAQAXAAAOANQANAOAAATQAAAVgNANQgOANgXAAQgWAAgOgNgAgLgOQgEAGAAAIQAAAJAEAGQAFAFAGAAQAIAAAEgFQAEgGAAgJQAAgIgEgGQgEgFgIAAQgGAAgFAFg");
	this.shape_13.setTransform(49.925,29.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgQAuIAAhBIgRAAIAAgZIBDAAIAAAZIgSAAIAABBg");
	this.shape_14.setTransform(41.175,29.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgPAuIAAhaIAfAAIAABag");
	this.shape_15.setTransform(35.55,29.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASArIAAgrIAhAAIAABag");
	this.shape_16.setTransform(28.825,29.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgkAiQgNgNAAgVQAAgTANgOQAOgNAWAAQAXAAAOANQANAOAAATQAAAVgNANQgOANgXAAQgWAAgOgNgAgLgOQgEAGAAAIQAAAJAEAGQAFAFAGAAQAIAAAEgFQAEgGAAgJQAAgIgEgGQgEgFgIAAQgGAAgFAFg");
	this.shape_17.setTransform(19.025,29.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAXAuIAAgtIgJAlIgcAAIgJglIAAAtIgfAAIAAhaIApAAIANAzIAOgzIAqAAIAABag");
	this.shape_18.setTransform(7.95,29.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgPANQAIgEACgGQgLgDABgKQAAgGAEgFQAFgEAGAAQAHAAAEAEQAFAFABAHQAAAKgGAJQgGAKgIAGg");
	this.shape_19.setTransform(132.95,22.525);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_20.setTransform(127.425,18.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgjAuIAAhaIAlAAQAQgBAJAJQAJAJAAANQAAAOgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgCAAgDQAAgDgCgCQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_21.setTransform(120.025,18.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgkAhQgNgMAAgVQAAgTANgOQAOgNAWAAQAXAAAOANQANAOAAATQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgOQgEAGAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgGQgEgFgIAAQgGAAgFAFg");
	this.shape_22.setTransform(110.925,18.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgQAuIAAhBIgRAAIAAgZIBDAAIAAAZIgSAAIAABBg");
	this.shape_23.setTransform(102.175,18.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAGAuIgPgmIAAAmIghAAIAAhaIAhAAIAAAgIAPggIAkAAIgXArIAYAvg");
	this.shape_24.setTransform(94.175,18.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_25.setTransform(85.725,18.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_26.setTransform(78.775,18.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgpAuIAAhaIAhAAQAWAAAOALQANANAAAUQAAAVgNAMQgOANgWAAgAgIAUIACAAQAOAAAAgUQAAgSgOAAIgCAAg");
	this.shape_27.setTransform(71.3,18.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgOANQAHgEACgGQgLgDABgKQAAgGAEgFQAFgEAGAAQAHAAAFAEQAEAFABAHQAAAKgGAJQgGAKgIAGg");
	this.shape_28.setTransform(60.05,22.525);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_29.setTransform(54.525,18.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgjAuIAAhaIAlAAQAQgBAJAJQAJAJAAANQAAAOgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgCAAgDQAAgDgCgCQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_30.setTransform(47.125,18.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgkAhQgNgMAAgVQAAgTANgOQAOgNAWAAQAXAAAOANQANAOAAATQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgOQgEAGAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgGQgEgFgIAAQgGAAgFAFg");
	this.shape_31.setTransform(38.025,18.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgQAuIAAhBIgRAAIAAgZIBDAAIAAAZIgSAAIAABBg");
	this.shape_32.setTransform(29.275,18.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgjAuIAAhaIAlAAQAQgBAJAJQAJAJAAANQAAAOgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgCAAgDQAAgDgCgCQgBgBAAAAQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_33.setTransform(21.975,18.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghaIAfAAIAgBagAAEAJIgEgSIgFASIAJAAg");
	this.shape_34.setTransform(13.175,18.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgcAuIAAhaIAgAAIAABAIAZAAIAAAag");
	this.shape_35.setTransform(5.3,18.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgjAuIAAhbIAlAAQAQAAAJAJQAJAJAAAOQAAANgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgBAAgEQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_36.setTransform(132.975,7.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AAJAuIAAghIgSAAIAAAhIghAAIAAhbIAhAAIAAAhIASAAIAAghIAiAAIAABbg");
	this.shape_37.setTransform(124.35,7.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_38.setTransform(113.975,7.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgpAuIAAhbIAhAAQAWAAAOANQANAMAAAUQAAAVgNAMQgOANgWAAgAgIAUIABAAQAPgBAAgTQAAgSgPgBIgBAAg");
	this.shape_39.setTransform(106.5,7.2);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AAFAuIgMgeIAAAeIggAAIAAhbIAnAAQAQAAAJAJQAJAIAAANQAAAHgEAHQgEAFgHADIAVAngAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAAAAAgBQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAIgEAAg");
	this.shape_40.setTransform(98.175,7.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_41.setTransform(90.525,7.2);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_42.setTransform(84.075,7.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgQAuIAAhBIgRAAIAAgaIBDAAIAAAaIgSAAIAABBg");
	this.shape_43.setTransform(77.225,7.2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgZAiQgOgNAAgVQAAgTAOgNQANgOAVAAQARAAANAJIgJAaQgIgHgJAAQgIAAgFAFQgFAFAAAIQABAJAEAFQAFAFAIAAQAJAAAIgHIAKAaQgNAJgSAAQgUAAgOgNg");
	this.shape_44.setTransform(69.4,7.2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_45.setTransform(62.075,7.2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AgcAuIAAhbIAgAAIAABBIAZAAIAAAag");
	this.shape_46.setTransform(55.8,7.2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_47.setTransform(49.275,7.2);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgYAnQgIgGgDgKIAYgMQAFAKAGAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAADAGADIAMAHQASAIAAAPQAAAOgKAHQgKAIgQAAQgNAAgLgIg");
	this.shape_48.setTransform(42.275,7.2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_49.setTransform(35.325,7.2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AghAhQgNgMAAgVQAAgTAOgNQAOgOAUAAQAZAAAOAPIgSATQgIgGgJAAQgHAAgFAFQgFAFAAAIQAAAJAFAFQAFAFAHABQAHAAAEgDIAAgDIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_50.setTransform(27.175,7.2);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AgjAuIAAhbIAlAAQAQAAAJAJQAJAJAAAOQAAANgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgBAAgEQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_51.setTransform(16.225,7.2);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AgkAhQgNgMAAgVQAAgUANgMQAOgOAWAAQAXAAAOAOQANAMAAAUQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgNQgEAFAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgFQgEgGgIAAQgGAAgFAGg");
	this.shape_52.setTransform(7.125,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,160.7,39), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282828").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape.setTransform(110.925,18.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#282828").s().p("AAFAuIgMgeIAAAeIggAAIAAhaIAnAAQAQgBAJAJQAJAIAAANQAAAIgEAGQgEAFgHAEIAVAmgAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQABgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBgBAAQAAgBAAAAQAAAAgBAAQAAgBgBAAQgBAAAAAAIgEAAg");
	this.shape_1.setTransform(103.425,18.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_2.setTransform(95.775,18.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#282828").s().p("AghAhQgNgMAAgVQAAgTAOgOQAOgNAUAAQAZAAAOAOIgSAUQgIgGgJAAQgHAAgFAFQgFAGAAAHQAAAJAFAFQAFAGAHAAQAHAAAEgCIAAgEIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_3.setTransform(87.625,18.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#282828").s().p("AgQAuIAAhaIAhAAIAABag");
	this.shape_4.setTransform(80.85,18.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#282828").s().p("AgfAkQgKgKAAgRIAAg2IAhAAIAAA3QAAAJAIAAQAJAAAAgJIAAg3IAhAAIAAA2QAAARgLAKQgLAKgUAAQgTAAgMgKg");
	this.shape_5.setTransform(74.25,18.275);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#282828").s().p("AgmAuIAAgRIAjgwIgiAAIAAgZIBMAAIAAARIgjAvIAjAAIAAAag");
	this.shape_6.setTransform(65.6,18.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAALIAVAAIAAAXIgVAAIAAAfg");
	this.shape_7.setTransform(58.525,18.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#282828").s().p("AgkAhQgNgMAAgVQAAgTANgOQAOgNAWAAQAXAAAOANQANAOAAATQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgOQgEAGAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgGQgEgFgIAAQgGAAgFAFg");
	this.shape_8.setTransform(50.175,18.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#282828").s().p("AgQAuIAAhBIgRAAIAAgZIBDAAIAAAZIgSAAIAABBg");
	this.shape_9.setTransform(41.425,18.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#282828").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_10.setTransform(34.025,18.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#282828").s().p("AghAhQgNgMAAgVQAAgTAOgOQAOgNAUAAQAZAAAOAOIgSAUQgIgGgJAAQgHAAgFAFQgFAGAAAHQAAAJAFAFQAFAGAHAAQAHAAAEgCIAAgEIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_11.setTransform(22.875,18.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_12.setTransform(14.875,18.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#282828").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghaIAfAAIAgBagAAEAJIgEgSIgFASIAJAAg");
	this.shape_13.setTransform(6.825,18.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_14.setTransform(113.975,7.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#282828").s().p("AgpAuIAAhbIAhAAQAWAAAOANQANAMAAAUQAAAVgNAMQgOANgWAAgAgIAUIABAAQAPgBAAgTQAAgSgPgBIgBAAg");
	this.shape_15.setTransform(106.5,7.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#282828").s().p("AAFAuIgMgeIAAAeIggAAIAAhbIAnAAQAQAAAJAJQAJAIAAANQAAAHgEAHQgEAFgHADIAVAngAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAAAAAgBQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAIgEAAg");
	this.shape_16.setTransform(98.175,7.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_17.setTransform(90.525,7.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_18.setTransform(84.075,7.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#282828").s().p("AgQAuIAAhBIgRAAIAAgaIBDAAIAAAaIgSAAIAABBg");
	this.shape_19.setTransform(77.225,7.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#282828").s().p("AgZAiQgOgNAAgVQAAgTAOgNQANgOAVAAQARAAANAJIgJAaQgIgHgJAAQgIAAgFAFQgFAFAAAIQABAJAEAFQAFAFAIAAQAJAAAIgHIAKAaQgNAJgSAAQgUAAgOgNg");
	this.shape_20.setTransform(69.4,7.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_21.setTransform(62.075,7.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#282828").s().p("AgcAuIAAhbIAgAAIAABBIAZAAIAAAag");
	this.shape_22.setTransform(55.8,7.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_23.setTransform(49.275,7.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#282828").s().p("AgYAnQgIgGgDgKIAYgMQAFAKAGAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAADAGADIAMAHQASAIAAAPQAAAOgKAHQgKAIgQAAQgNAAgLgIg");
	this.shape_24.setTransform(42.275,7.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_25.setTransform(35.325,7.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#282828").s().p("AghAhQgNgMAAgVQAAgTAOgNQAOgOAUAAQAZAAAOAPIgSATQgIgGgJAAQgHAAgFAFQgFAFAAAIQAAAJAFAFQAFAFAHABQAHAAAEgDIAAgDIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_26.setTransform(27.175,7.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#282828").s().p("AgjAuIAAhbIAlAAQAQAAAJAJQAJAJAAAOQAAANgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgBAAgEQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_27.setTransform(16.225,7.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#282828").s().p("AgkAhQgNgMAAgVQAAgUANgMQAOgOAWAAQAXAAAOAOQANAMAAAUQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgNQgEAFAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgFQgEgGgIAAQgGAAgFAGg");
	this.shape_28.setTransform(7.125,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,119.4,28), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282828").s().p("AgYAnQgIgHgDgIIAYgNQAFAJAGAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgDgEgCIgMgGQgSgIAAgQQAAgOAJgHQAKgIAOAAQAOAAAKAIQAJAGACAHIgXAOQgEgJgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape.setTransform(75.625,29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#282828").s().p("AAFAuIgMgeIAAAeIggAAIAAhaIAnAAQAQAAAJAIQAJAIAAANQAAAIgEAGQgEAFgHAEIAVAmgAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQAAgBAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAIgEAAg");
	this.shape_1.setTransform(68.125,29.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAIIAVAAIAAAWIgVAAIAAAJIAXAAIAAAag");
	this.shape_2.setTransform(60.475,29.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#282828").s().p("AghAiQgNgNAAgVQAAgTAOgOQAOgNAUAAQAZAAAOAOIgSAVQgIgHgJAAQgHAAgFAFQgFAFAAAIQAAAJAFAGQAFAFAHAAQAHAAAEgCIAAgEIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgNg");
	this.shape_3.setTransform(52.325,29.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#282828").s().p("AgkAiQgNgNAAgVQAAgTANgOQAOgNAWAAQAXAAAOANQANAOAAATQAAAVgNANQgOANgXAAQgWAAgOgNgAgLgOQgEAGAAAIQAAAJAEAGQAFAFAGAAQAIAAAEgFQAEgGAAgJQAAgIgEgGQgEgFgIAAQgGAAgFAFg");
	this.shape_4.setTransform(42.425,29.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#282828").s().p("AAFAuIgMgeIAAAeIggAAIAAhaIAnAAQAQAAAJAIQAJAIAAANQAAAIgEAGQgEAFgHAEIAVAmgAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQABgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAgBgBAAQAAgBAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAIgEAAg");
	this.shape_5.setTransform(33.525,29.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#282828").s().p("AgpAuIAAhaIAhAAQAWgBAOAMQAOANgBAUQAAAVgNAMQgOAMgWABgAgIATIACAAQAOABAAgUQAAgSgOAAIgCAAg");
	this.shape_6.setTransform(24.85,29.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#282828").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASArIAAgrIAhAAIAABag");
	this.shape_7.setTransform(13.125,29.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAIIAVAAIAAAWIgVAAIAAAJIAXAAIAAAag");
	this.shape_8.setTransform(5.225,29.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#282828").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_9.setTransform(133.325,18.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_10.setTransform(126.375,18.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#282828").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASAqIAAgqIAhAAIAABag");
	this.shape_11.setTransform(118.475,18.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#282828").s().p("AgPAuIAAhaIAgAAIAABag");
	this.shape_12.setTransform(111.8,18.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#282828").s().p("AAJAuIAAghIgSAAIAAAhIggAAIAAhaIAgAAIAAAgIASAAIAAggIAhAAIAABag");
	this.shape_13.setTransform(105.1,18.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#282828").s().p("AgZAiQgOgNAAgVQAAgTANgOQAOgNAVAAQASAAAMAJIgKAaQgGgHgLAAQgHAAgFAFQgEAGgBAHQAAAIAFAGQAFAFAHAAQALAAAHgHIAKAaQgMAJgTAAQgUAAgOgNg");
	this.shape_14.setTransform(96.25,18.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#282828").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghaIAfAAIAgBagAAEAJIgEgSIgFASIAJAAg");
	this.shape_15.setTransform(87.325,18.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#282828").s().p("AAYAuIAAgsIgJAkIgdAAIgJgkIAAAsIgfAAIAAhaIApAAIANAyIAPgyIAoAAIAABag");
	this.shape_16.setTransform(76.55,18.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#282828").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_17.setTransform(66.825,18.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#282828").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghaIAfAAIAgBagAAEAJIgEgSIgFASIAJAAg");
	this.shape_18.setTransform(58.275,18.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#282828").s().p("AALAuIgLgvIgKAvIghAAIgYhaIAjAAIAJAwIALgwIAZAAIALAwIAJgwIAjAAIgYBag");
	this.shape_19.setTransform(46.575,18.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_20.setTransform(33.975,18.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#282828").s().p("AgcAuIAAhaIAgAAIAABAIAZAAIAAAag");
	this.shape_21.setTransform(27.7,18.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_22.setTransform(21.175,18.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#282828").s().p("AgPAuIAAhaIAgAAIAABag");
	this.shape_23.setTransform(15.95,18.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#282828").s().p("AAXAuIAAgsIgJAkIgcAAIgJgkIAAAsIgfAAIAAhaIApAAIANAyIAOgyIAqAAIAABag");
	this.shape_24.setTransform(7.95,18.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_25.setTransform(113.975,7.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#282828").s().p("AgpAuIAAhbIAhAAQAWAAAOANQANAMAAAUQAAAVgNAMQgOANgWAAgAgIAUIABAAQAPgBAAgTQAAgSgPgBIgBAAg");
	this.shape_26.setTransform(106.5,7.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#282828").s().p("AAFAuIgMgeIAAAeIggAAIAAhbIAnAAQAQAAAJAJQAJAIAAANQAAAHgEAHQgEAFgHADIAVAngAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAAAAAgBQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAIgEAAg");
	this.shape_27.setTransform(98.175,7.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_28.setTransform(90.525,7.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_29.setTransform(84.075,7.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#282828").s().p("AgQAuIAAhBIgRAAIAAgaIBDAAIAAAaIgSAAIAABBg");
	this.shape_30.setTransform(77.225,7.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#282828").s().p("AgZAiQgOgNAAgVQAAgTAOgNQANgOAVAAQARAAANAJIgJAaQgIgHgJAAQgIAAgFAFQgFAFAAAIQABAJAEAFQAFAFAIAAQAJAAAIgHIAKAaQgNAJgSAAQgUAAgOgNg");
	this.shape_31.setTransform(69.4,7.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_32.setTransform(62.075,7.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#282828").s().p("AgcAuIAAhbIAgAAIAABBIAZAAIAAAag");
	this.shape_33.setTransform(55.8,7.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_34.setTransform(49.275,7.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#282828").s().p("AgYAnQgIgGgDgKIAYgMQAFAKAGAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAADAGADIAMAHQASAIAAAPQAAAOgKAHQgKAIgQAAQgNAAgLgIg");
	this.shape_35.setTransform(42.275,7.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_36.setTransform(35.325,7.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#282828").s().p("AghAhQgNgMAAgVQAAgTAOgNQAOgOAUAAQAZAAAOAPIgSATQgIgGgJAAQgHAAgFAFQgFAFAAAIQAAAJAFAFQAFAFAHABQAHAAAEgDIAAgDIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_37.setTransform(27.175,7.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#282828").s().p("AgjAuIAAhbIAlAAQAQAAAJAJQAJAJAAAOQAAANgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgBAAgEQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_38.setTransform(16.225,7.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#282828").s().p("AgkAhQgNgMAAgVQAAgUANgMQAOgOAWAAQAXAAAOAOQANAMAAAUQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgNQgEAFAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgFQgEgGgIAAQgGAAgFAGg");
	this.shape_39.setTransform(7.125,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,141.8,39), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#282828").s().p("AgYAnQgIgHgDgIIAYgNQAFAJAGAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgDgEgCIgMgGQgSgIAAgQQAAgOAJgHQAKgIAOAAQAOAAAKAIQAJAGACAHIgXAOQgEgJgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape.setTransform(76.675,29.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#282828").s().p("AAFAuIgMgeIAAAeIggAAIAAhaIAnAAQAQAAAJAIQAJAIAAANQAAAIgEAGQgEAFgHAEIAVAmgAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAgBgBAAQAAgBAAAAQAAAAgBgBQAAAAgBAAQgBAAAAAAIgEAAg");
	this.shape_1.setTransform(69.175,29.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#282828").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghaIAfAAIAgBagAAEAJIgEgSIgFASIAJAAg");
	this.shape_2.setTransform(59.925,29.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#282828").s().p("AglAuIAAhaIAnAAQAPgBAJAIQAJAGAAAMQAAAMgJAGQAFACADAFQADAGABAFQAAANgKAIQgJAHgPABgAgFAVIAFAAQABAAAAAAQABAAAAAAQABgBAAAAQABAAAAgBQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQAAAAgBAAIgFAAgAgFgKIAFAAQAFAAAAgFQAAgFgFAAIgFAAg");
	this.shape_3.setTransform(51.2,29.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#282828").s().p("AgpAuIAAhaIAhAAQAWgBANAMQAPANAAAUQAAAVgOAMQgOAMgWABgAgJATIACAAQAPABAAgUQAAgSgPAAIgCAAg");
	this.shape_4.setTransform(42.8,29.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#282828").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASArIAAgrIAhAAIAABag");
	this.shape_5.setTransform(33.575,29.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#282828").s().p("AgeAkQgLgKAAgRIAAg2IAhAAIAAA3QAAAJAIAAQAJAAAAgJIAAg3IAhAAIAAA2QAAARgLAKQgLAKgUAAQgTAAgLgKg");
	this.shape_6.setTransform(24.35,29.275);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#282828").s().p("AgkAiQgNgNAAgVQAAgTANgOQAOgNAWAAQAXAAAOANQANAOAAATQAAAVgNANQgOANgXAAQgWAAgOgNgAgLgOQgEAGAAAIQAAAJAEAGQAFAFAGAAQAIAAAEgFQAEgGAAgJQAAgIgEgGQgEgFgIAAQgGAAgFAFg");
	this.shape_7.setTransform(14.625,29.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#282828").s().p("AgYAnQgIgHgDgIIAYgNQAFAJAGAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgDgEgCIgMgGQgSgIAAgQQAAgOAJgHQAKgIAOAAQAOAAAKAIQAJAGACAHIgXAOQgEgJgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_8.setTransform(5.725,29.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#282828").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASAqIAAgqIAhAAIAABag");
	this.shape_9.setTransform(112.125,18.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#282828").s().p("AgbAuIAAhaIA3AAIAAAZIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_10.setTransform(104.225,18.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#282828").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_11.setTransform(94.725,18.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#282828").s().p("AgPANQAHgEAEgGQgMgDAAgKQAAgHAFgEQAFgEAGAAQAHAAAEAEQAGAEgBAIQAAAJgFAKQgGAKgIAGg");
	this.shape_12.setTransform(88.8,15.675);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#282828").s().p("AgPAuIgihaIAkAAIAOA0IAOg0IAiAAIghBag");
	this.shape_13.setTransform(81.55,18.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#282828").s().p("AgQAuIAAhBIgRAAIAAgZIBDAAIAAAZIgSAAIAABBg");
	this.shape_14.setTransform(72.875,18.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#282828").s().p("AghAhQgNgMAAgVQAAgTAOgOQAOgNAUAAQAZAAAOAOIgSAUQgIgGgJAAQgHAAgFAFQgFAGAAAHQAAAJAFAFQAFAGAHAAQAHAAAEgCIAAgEIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_15.setTransform(61.825,18.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#282828").s().p("AAKAuIgTgsIAAAsIghAAIAAhaIAiAAIASAqIAAgqIAhAAIAABag");
	this.shape_16.setTransform(52.375,18.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#282828").s().p("AgfAkQgKgKAAgRIAAg2IAhAAIAAA3QAAAJAIAAQAJAAAAgJIAAg3IAhAAIAAA2QAAARgLAKQgLAKgUAAQgTAAgMgKg");
	this.shape_17.setTransform(43.15,18.275);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#282828").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_18.setTransform(34.775,18.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#282828").s().p("AAXAuIAAgsIgJAkIgbAAIgJgkIAAAsIghAAIAAhaIAqAAIANAyIAOgyIAqAAIAABag");
	this.shape_19.setTransform(25.1,18.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#282828").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghaIAfAAIAgBagAAEAJIgEgSIgFASIAJAAg");
	this.shape_20.setTransform(14.325,18.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#282828").s().p("AgYAnQgIgHgDgJIAYgMQAFAJAGABQAAgBABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAABQAAACAGADIAMAGQASAJAAAPQAAANgKAIQgKAIgQAAQgNAAgLgIg");
	this.shape_21.setTransform(5.725,18.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_22.setTransform(113.975,7.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#282828").s().p("AgpAuIAAhbIAhAAQAWAAAOANQANAMAAAUQAAAVgNAMQgOANgWAAgAgIAUIABAAQAPgBAAgTQAAgSgPgBIgBAAg");
	this.shape_23.setTransform(106.5,7.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#282828").s().p("AAFAuIgMgeIAAAeIggAAIAAhbIAnAAQAQAAAJAJQAJAIAAANQAAAHgEAHQgEAFgHADIAVAngAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAAAAAgBQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAIgEAAg");
	this.shape_24.setTransform(98.175,7.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_25.setTransform(90.525,7.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_26.setTransform(84.075,7.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#282828").s().p("AgQAuIAAhBIgRAAIAAgaIBDAAIAAAaIgSAAIAABBg");
	this.shape_27.setTransform(77.225,7.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#282828").s().p("AgZAiQgOgNAAgVQAAgTAOgNQANgOAVAAQARAAANAJIgJAaQgIgHgJAAQgIAAgFAFQgFAFAAAIQABAJAEAFQAFAFAIAAQAJAAAIgHIAKAaQgNAJgSAAQgUAAgOgNg");
	this.shape_28.setTransform(69.4,7.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_29.setTransform(62.075,7.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#282828").s().p("AgcAuIAAhbIAgAAIAABBIAZAAIAAAag");
	this.shape_30.setTransform(55.8,7.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_31.setTransform(49.275,7.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#282828").s().p("AgYAnQgIgGgDgKIAYgMQAFAKAGAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAADAGADIAMAHQASAIAAAPQAAAOgKAHQgKAIgQAAQgNAAgLgIg");
	this.shape_32.setTransform(42.275,7.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#282828").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_33.setTransform(35.325,7.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#282828").s().p("AghAhQgNgMAAgVQAAgTAOgNQAOgOAUAAQAZAAAOAPIgSATQgIgGgJAAQgHAAgFAFQgFAFAAAIQAAAJAFAFQAFAFAHABQAHAAAEgDIAAgDIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_34.setTransform(27.175,7.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#282828").s().p("AgjAuIAAhbIAlAAQAQAAAJAJQAJAJAAAOQAAANgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgBAAgEQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_35.setTransform(16.225,7.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#282828").s().p("AgkAhQgNgMAAgVQAAgUANgMQAOgOAWAAQAXAAAOAOQANAMAAAUQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgNQgEAFAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgFQgEgGgIAAQgGAAgFAGg");
	this.shape_36.setTransform(7.125,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,121.5,39), null);


(lib.shape2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjHLuIAA3bIGPAAIAAXbg");
	this.shape.setTransform(7.5,50.0213,0.375,0.6668);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape2, new cjs.Rectangle(0,0,15,100.1), null);


(lib.shape1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");
	this.shape.setTransform(159.9999,49.9992,1.0667,0.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape1, new cjs.Rectangle(0,0,320,100), null);


(lib.shape_grey = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E6E7E8").s().p("A0TLuIAA3bMAonAAAIAAXbg");
	this.shape.setTransform(152.5169,34.997,1.1731,0.4666);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("A0TLuIAA3bMAonAAAIAAXbg");
	this.shape_1.setTransform(159.9993,34.9945,1.2308,0.6666);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-15,320,100);


(lib.services_button = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "9px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.textAlign = "center";
	this.dynText.lineHeight = 7;
	this.dynText.lineWidth = 106;
	this.dynText.parent = this;
	this.dynText.setTransform(55,6);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.services_button, new cjs.Rectangle(0,4,110,18.9), null);


(lib.price_old_line = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E31E26").s().p("AjxAyIAAhjIHjAAIAABjg");
	this.shape.setTransform(46.0198,1,1.9008,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AjxAyIAAhjIHjAAIAABjg");
	this.shape_1.setTransform(46.0198,1,1.9008,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,92,2);


(lib.merken_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAPBwIgmhcIAABcIhRAAIAAjfIBRAAIAABSIAlhSIBZAAIg3BrIA5B0g");
	this.shape.setTransform(205.775,14.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA0AAIAAA6Ig0AAIAAAUIA7AAIAAA/g");
	this.shape_1.setTransform(186.35,14.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA0AAIAAA6Ig0AAIAAAUIA7AAIAAA/g");
	this.shape_2.setTransform(170.45,14.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAbBwIgbh1IgaB1IhSAAIg6jfIBXAAIAUB6IAch6IA/AAIAcB6IAUh6IBXAAIg6Dfg");
	this.shape_3.setTransform(145.575,14.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("AAXBwIgvhpIAABpIhRAAIAAjfIBTAAIAvBpIAAhpIBRAAIAADfg");
	this.shape_4.setTransform(117.175,14.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA0AAIAAA6Ig0AAIAAAUIA7AAIAAA/g");
	this.shape_5.setTransform(97.7,14.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AAPBwIgmhcIAABcIhRAAIAAjfIBRAAIAABSIAlhSIBZAAIg3BrIA5B0g");
	this.shape_6.setTransform(78.875,14.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AALBwIgehJIAABJIhPAAIAAjfIBiAAQAnAAAXAVQAWAUAAAgQAAASgLAQQgKANgPAJIAzBegAgTgTIAJAAQAHAAAEgFQAEgEAAgHQAAgFgEgFQgEgFgHAAIgJAAg");
	this.shape_7.setTransform(58.075,14.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA1AAIAAA6Ig1AAIAAAUIA7AAIAAA/g");
	this.shape_8.setTransform(39.25,14.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AA6BwIAAhsIgWBbIhGAAIgXhbIAABsIhPAAIAAjfIBmAAIAiB/IAjh/IBmAAIAADfg");
	this.shape_9.setTransform(16.65,14.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.merken_text, new cjs.Rectangle(0,0,218.9,36), null);


(lib.mediamarkt_logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EB1D24").s().p("AgzA2QgWgWAAggQAAgeAWgWQAVgXAeABQAfgBAVAXQAWAWAAAeQAAAfgWAXQgVAWgfgBQgeABgVgWgAgqgrQgSATAAAYQAAAaASASQASASAYAAQAZAAASgSQASgSAAgaQAAgZgSgSQgSgRgZgBQgYABgSARgAASAoIgNggIgSAAIAAAgIgRAAIAAhQIAkAAQAeAAAAAYQAAAPgPAHIAPAigAgNgFIAQAAQAPAAAAgLQAAgLgNABIgSAAg");
	this.shape.setTransform(392.5653,-32.1875,0.9958,0.9957);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AhIBeQgKAAgGgIQgHgIACgNIAaiMQABgHAGgGQAGgFAHAAIB4AAQAKAAAGAIQAGAHgBAKIgaCNQgCAIgFAHQgHAGgHAAg");
	this.shape_1.setTransform(-100.5306,-29.8475,0.9958,0.9957);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EB1D24").s().p("AN5IUQhHgPg7g+Qg6g/gOhMQgHgnABgqQgHAGgIAAIn0AAIgBAGQAAAHAHAXIACAHQARAsAoAaQAnAaAxgBQBhgCBQhbQAGgHAJAAIBwAAQALAAAGAKQAHAKgFAKQgwBmhhA+QhiA+hwAAQhNgDg/gkQg+glgkhAQglhFAChcIgxEOQgBAGgFADQgEAEgFAAIiDAAQgHAAgFgHQgDgDAAgGIAAgEQAgiGApj/QAskNAKgrQhEBQnLJ6QgFAGgHABIiGAAQgHAAgFgHQgEgEAAgGIAAgDQAeiMArj6IA2k2QhBBPooJ7QgFAFgIABIivAAQgJAAgFgLQgEgKAHgHINmv6QAFgGAHAAICJgBQAIAAAFAGQADAGAAAFIAAADQheIhgUBjQBFhPG3pDQAFgFAHAAICMAAQAIAAAEAGQAFAGgCAIIhnI8IADgHQAxhfBehCQBchABngNIACAAQBcgFBGAjQBGAjArBHIAAACQAjBGAEBUQAkhpBWhQQBIhFBXgaQBWgaBYAUIAEABIALAFQA3AYAYAWIAyj1QACgIAGgEQAFgFAIAAIBsAAQAKAAAHAIQAGAHgCAKIieNlQgDAPgSgBIhsABQgMAAgEgFQgFgFAAgIIAFgdQhpBDhkAAQgcAAgbgFgAQBgZQgqAIghAXQg6ArgfA4QgfA3AAA7QAAAXADAPQAGAeAQAZQAQAaAYARQAzAiBXgSQA8gOA0g2QA0g2ARhBQAGgXAAgZQACg0gXglQgjg5g6gOQgVgFgWAAQgTAAgTAEgAIZBTQgEglgkglQgYgUgXgIQgegKgtAEQgwAGgrAbQgvAegXAtIFDAAIAAAAg");
	this.shape_2.setTransform(-255.3606,-0.0539,0.9958,0.9957);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EB1D24").s().p("ADVHHIgKgEQgEAEgDAAIiCAAQgMAAgGgJQh+jWgrhDIgeCrIgVBpQgFAOgPAAIh1AAQgJAAgHgHQgGgIACgJIBYoWIgJABIgIABQh4ASggB5IhPGQQgCAIgFAFQgGAEgIAAIhoAAQgKAAgGgHQgHgHACgKIB3qVQABgHAGgEQAGgFAIAAIBsAAQALAAAFAHQAHAIgCAJIgDAQQAggUAkgNQAogOAkgBQAKAAAHAJIAAAAQAQhhAUheQABgIAGgFQAFgFAIAAIB3AAQAKAAAGAIQAFAGAAAHIAAAEQg0EXgRBkQA6hABhh9QAFgHAKAAICLAAQALAAAGAJQAGgJAMAAIBDAAIAji+QABgIAGgFQAGgFAIAAIB5AAQAJAAAHAIQAFAGAAAIIgjC6IB7AAQAIAAAIAHQAEAGAAAIIAAAEIgSBrQgBAIgGAFQgGAFgIAAIiDAAIhhIWQgBAHgGAFQgGAFgHAAgAAPBEIDGFJQAKhAAnjTIAojaIg7AAQgKAAgGgIQgGgHABgLIAQhSIjfEQg");
	this.shape_3.setTransform(329.764,6.0739,0.9958,0.9957);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EB1D24").s().p("Aq8IDQBKgdBFguQAjgXAUgRQB6hrAKiQQALiNhmhHQAJALAFAOQBOBgguCEQgvCHiQBHQgXALgnANQhNAahNAJQg0gigtgwQBLALBOgFQAmgCAYgEQCegfBQh3QBOh1g0hxIAAAGQAAAHgBAIQAYB9hqBeQhrBfiigMQgYgCgmgIQhMgQhGgdQggg8gRhDIgCgLQA8AvBFAkQAjASAWAIQCYA1B/g9QB8g8ANh8QgGAOgIAIQgsB1iIAZQiKAZiEhbQgXgSghgdQhAg6gsg0IhfIIQgDAPgSgBIhrAAQgMAAgFgGQgFgFAAgGIAFgdQiFBVh/gXQhHgPg7g+Qg6g/gNhMQgDgQgDgjIgCghIgwEOQgDAPgSgBIh4ABQgMAAgEgFQgGgHABgKIB4qWQABgIAGgFQAGgFAIAAIB4AAQAKAAAGAHQAGAIgBAKIgyENIACgFQAlhrBUhOQBJhFBWgaQBXgbBYAWIAPAFQA2AYAYAWQADgQAEgNIADgJQAIgRAPAAIBtAAQAKAAAHAGQAEAFAEAKQAYBFAtBLQAXAlASAYQBrB+CQAJQCNAJBFhtQgIAGgHAEIgEACQheBZiHgtQiJguhJiWQgKgWgMgmQgYhLgKhMQAmhBA2g0QgKBNAEBRQACAoAEAZQAfCiB3BQQB0BPBxg4IgQABIgKAAQh7AYhchrQhehtAMikQACgZAIgoQAQhPAehIQA6ggA9gQQguA9gjBGIgaA7Qg1CdBACCQA/CAB+AHIgRgKIgBgBQh7gmgdiMQgdiQBciLIAogzQA1g8A9gvQBNABBIAWQhJAdhEAtIg2AnQh8BtgJCSQgICPBqBFQgJgKgFgLQhVheAtiIQAuiLCThJIA+gYQBOgZBPgKQA7AlAwA0QhOgMhRAFIhDAGQigAghPB5QhPB3A5ByIgBgOIAAgJQgah/BqheQBshhCjAMIBCALQBRARBJAgQAgA9APA+IAEAUQg+gzhLgnIg+gdQiXg1h/A9Qh9A8gNB8QAHgOAJgLQAthzCHgYQCKgZCDBbIA1ArQA9A5AvBBQgCBBgTBAQgdhKgshFIgmg2Qhph6iNgMQiJgLhIBlQANgHAHgCQBfhSCDAvQCGAvBHCSIAfBUQAgBjAIBOILYtXQAGgHAGAAICZABQAHAAAFAFQADAEAAAIIAAADQhaIjgZBhQBEhOG0pFQAGgFAGAAICRgBQAHAAAFAGQAFAGgCAJIhsJMQAlhWBIhEQBJhFBWgaQBWgaBZAUIAEABIALAFQAyAUAgAeIAJgqQABgIAGgEQAGgEAHAAIBtAAQAKAAAGAGQAFAGAAAJIh5KZQgCAPgTAAIhrAAQgNAAgEgFQgEgGAAgHIAAgEIAEgZQiFBVh/gXQhHgPg7g+Qg6g/gNhMQgDgPADgZQAEgiAAgHIAAgSIgwEfQgEANgMAAIiEAAQgHAAgFgHQgEgFAAgFIABgDQAYh6AskBQApjvANg+QgqAyj1FGIjsE+QgEAFgHABIiHAAQgHAAgFgGQgDgEAAgGIAAgEQAaiHAqj1IA1ktQhBBPo0JoQgFAGgIAAIirAAQgKAAgEgKQgEgIAFgIIAAAAIAAgBIAQgaQASgjAHgjQALhMgIhRIgKhCQgeifhzhQQhxhQhwAyIAFAAQALAAAJACQB5gUBZBrQBbBsgLCiIgLBDQgRBRgfBKQg5Adg6APIgTAFQAyhAAnhLQATgmAJgZQA1idhAiDQg/h/h/gHQALAEAJAHQB6AmAdCMQAdCQhcCLQgPAVgcAgQg3A+hBAxQhIgEhHgYgAZHgaQgqAIghAXQg5ArgeA3QgfA3AAA8QAAATADATQASBDAsAeQAxAhBWgPQA7gOA1g3QA0g2AUhBQAGgUAAgeQAAgygVgmQgeg3hBgPQgVgFgVAAQgTAAgUAEgA2hgaQgqAIghAXQg5ArgeA3QgfA3AAA8QAAAXAEAPQATBEAqAdQAXAQAlAEQAmAEAsgIQA6gPAzg1QAxg1AThBQAHgWAAgcQAAgxgWgnQgeg3hBgPQgVgFgVAAQgTAAgUAEg");
	this.shape_4.setTransform(75.8694,0.0439,0.9958,0.9957);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mediamarkt_logo, new cjs.Rectangle(-400,-54,799.9,108.1), null);


(lib.logobalk_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logobalk();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.logobalk_1, new cjs.Rectangle(0,0,358,46), null);


(lib.label_service = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.intro_img_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.intro_img();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.intro_img_1, new cjs.Rectangle(0,0,281,114), null);


(lib.endframe_static_line4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAQBNIgghJIAABJIg4AAIAAiZIA5AAIAgBIIAAhIIA4AAIAACZg");
	this.shape.setTransform(242.975,10.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_1.setTransform(229.6,10.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAIBNIgVgxIAAAxIg2AAIAAiZIBDAAQAbAAAPAPQAPANAAAWQAAAMgHALQgGAJgLAHIAjBAgAgNgNIAHAAQAFAAACgDQADgDAAgEQAAgFgDgDQgCgDgFAAIgHAAg");
	this.shape_2.setTransform(217.725,10.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAYBNIgHgYIgjAAIgHAYIg3AAIA2iZIA2AAIA1CZgAAHAOIgHgeIgJAeIAQAAg");
	this.shape_3.setTransform(202.075,10.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgbBNIg4iZIA8AAIAYBZIAZhZIA6AAIg4CZg");
	this.shape_4.setTransform(185.35,10.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAIBNIgVgxIAAAxIg2AAIAAiZIBDAAQAbAAAPAPQAPANAAAWQAAAMgHALQgGAJgLAHIAjBAgAgNgNIAHAAQAFAAACgDQADgDAAgEQAAgFgDgDQgCgDgFAAIgHAAg");
	this.shape_5.setTransform(170.375,10.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_6.setTransform(157.45,10.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_7.setTransform(142.35,10.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgbBNIAAhtIgeAAIAAgsIBzAAIAAAsIgeAAIAABtg");
	this.shape_8.setTransform(130.8,10.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AhGBNIAAiZIA3AAQAnAAAXAUQAXAVABAjQgBAjgXAVQgXAVgnAAgAgQAhIAEAAQAZAAAAghQAAgggZAAIgEAAg");
	this.shape_9.setTransform(113.25,10.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgbBNIAAiZIA3AAIAACZg");
	this.shape_10.setTransform(102.1,10.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgzA9QgTgSAAgcIAAhdIA4AAIAABeQAAAQAOABQAPgBAAgQIAAheIA4AAIAABdQAAAcgSASQgTARgiABQghgBgSgRg");
	this.shape_11.setTransform(90.9,10.95);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgwBNIAAiZIA3AAIAABtIAqAAIAAAsg");
	this.shape_12.setTransform(77.95,10.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_13.setTransform(66.95,10.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("Ag4A5QgWgWAAgjQAAghAXgXQAXgXAkAAQApAAAZAZIggAhQgMgLgQABQgMgBgJAKQgJAJAAANQAAAPAJAJQAIAJANAAQAMAAAHgDIAAgHIgRAAIAAgkIBDAAIAABEQgdAZguAAQglAAgXgXg");
	this.shape_14.setTransform(53.175,10.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgbBNIAAhtIgeAAIAAgsIBzAAIAAAsIgeAAIAABtg");
	this.shape_15.setTransform(34.8,10.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_16.setTransform(23.2,10.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAQBNIAAg3IgfAAIAAA3Ig4AAIAAiZIA4AAIAAA3IAfAAIAAg3IA4AAIAACZg");
	this.shape_17.setTransform(9.875,10.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,256,26);


(lib.endframe_static_line3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AALA0IgWgxIAAAxIgmAAIAAhnIAnAAIAWAxIAAgxIAlAAIAABng");
	this.shape.setTransform(168.05,8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_1.setTransform(158.925,8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAFA0IgNgiIAAAiIgmAAIAAhnIAuAAQASAAALAKQAKAJAAAOQAAAJgFAHQgEAHgIADIAZAsgAgIgJIAEAAQAEAAABgCQACgBAAgDQAAgEgCgCQgBgCgEAAIgEAAg");
	this.shape_2.setTransform(150.85,8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAQA0IgFgQIgXAAIgFAQIglAAIAkhnIAlAAIAkBngAAFAJIgFgUIgGAUIALAAg");
	this.shape_3.setTransform(140.175,8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgSA0IgmhnIApAAIAQA8IAQg8IAoAAIgmBng");
	this.shape_4.setTransform(128.775,8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAGA0IgPgiIAAAiIglAAIAAhnIAuAAQASAAALAKQAKAJAAAOQAAAJgFAHQgFAHgGADIAXAsgAgJgJIAFAAQADAAABgCQACgBABgDQgBgEgCgCQgBgCgDAAIgFAAg");
	this.shape_5.setTransform(118.6,8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_6.setTransform(109.775,8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_7.setTransform(99.475,8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgSA0IAAhKIgUAAIAAgdIBOAAIAAAdIgVAAIAABKg");
	this.shape_8.setTransform(91.6,8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgcAtQgIgIgFgKIAcgOQAHALAGgBQABAAAAAAQABAAAAAAQABAAAAgBQAAAAAAgBQAAgDgGgDIgNgHQgVgKAAgSQAAgPALgKQAKgIARgBQAQABANAIQAJAHACAJIgbAQQgEgKgHAAQgBAAAAAAQgBAAAAABQgBAAAAAAQAAABAAAAQAAAEAGAEIAPAGQAUAKAAASQAAAPgLAJQgLAKgTgBQgQABgMgKg");
	this.shape_9.setTransform(80.2,8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgRAPQAIgFAEgHQgNgDAAgMQAAgHAGgFQAFgFAHAAQAIAAAGAFQAFAFAAAIQAAAMgGALQgGALgKAHg");
	this.shape_10.setTransform(73.375,5.075);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgSA0IgmhnIApAAIAQA8IAQg8IAoAAIgmBng");
	this.shape_11.setTransform(65.025,8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgSA0IAAhKIgUAAIAAgdIBOAAIAAAdIgVAAIAABKg");
	this.shape_12.setTransform(55.05,8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_13.setTransform(44.275,8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgvA0IAAhnIAlAAQAaAAAQANQAQAOAAAYQAAAYgQAPQgQAOgagBgAgKAXIACAAQARAAABgXQgBgWgRABIgCAAg");
	this.shape_14.setTransform(35.65,8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAbA0IAAgyIgKAqIghAAIgKgqIAAAyIglAAIAAhnIAvAAIAQA6IAQg6IAwAAIAABng");
	this.shape_15.setTransform(20.725,8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgpAnQgQgPAAgYQAAgXAQgPQAQgQAZAAQAaAAAQAQQAQAPAAAXQAAAYgQAPQgQAPgaAAQgZAAgQgPgAgNgPQgEAFAAAKQAAALAEAFQAFAHAIAAQAJAAAFgHQAEgFAAgLQAAgKgEgFQgFgHgJAAQgIAAgFAHg");
	this.shape_16.setTransform(7.925,8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgSA0IAAhnIAlAAIAABng");
	this.shape_17.setTransform(98.6,8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgjApQgNgMAAgSIAAg/IAmAAIAAA/QAAALAKAAQALAAAAgLIAAg/IAlAAIAAA/QAAASgMAMQgNAMgXAAQgWAAgNgMg");
	this.shape_18.setTransform(91,8.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AghA0IAAhnIAlAAIAABKIAeAAIAAAdg");
	this.shape_19.setTransform(82.2,8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgmAnQgPgPAAgYQAAgWAQgQQAPgQAZAAQAcAAARARIgWAYQgIgIgLAAQgIAAgGAGQgGAGAAAJQAAAKAGAGQAFAHAJAAQAIAAAFgDIAAgEIgMAAIAAgZIAuAAIAAAuQgUARgfAAQgZAAgQgPg");
	this.shape_20.setTransform(65.25,8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgSA0IAAhKIgVAAIAAgdIBPAAIAAAdIgVAAIAABKg");
	this.shape_21.setTransform(52.75,8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_22.setTransform(44.825,8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AALA0IAAglIgVAAIAAAlIgmAAIAAhnIAmAAIAAAmIAVAAIAAgmIAmAAIAABng");
	this.shape_23.setTransform(35.775,8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14,p:{x:35.65}},{t:this.shape_13,p:{x:44.275}},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8,p:{x:91.6}},{t:this.shape_7,p:{x:99.475}},{t:this.shape_6,p:{x:109.775}},{t:this.shape_5,p:{x:118.6}},{t:this.shape_4,p:{x:128.775}},{t:this.shape_3,p:{x:140.175}},{t:this.shape_2,p:{x:150.85}},{t:this.shape_1,p:{x:158.925}},{t:this.shape,p:{x:168.05}}]}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_13,p:{x:74.675}},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_14,p:{x:106.2}},{t:this.shape_8,p:{x:118.15}},{t:this.shape_7,p:{x:126.025}},{t:this.shape_6,p:{x:136.325}},{t:this.shape_5,p:{x:145.15}},{t:this.shape_4,p:{x:155.325}},{t:this.shape_3,p:{x:166.725}},{t:this.shape_2,p:{x:177.4}},{t:this.shape_1,p:{x:185.475}},{t:this.shape,p:{x:194.6}}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,209.6,19);


(lib.endframe_static_line2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgbAtQgJgIgFgKIAcgOQAGALAHgBQABAAAAAAQABAAAAAAQABAAAAgBQAAAAAAgBQAAgDgFgDIgPgHQgUgKAAgSQAAgPAKgKQALgIARgBQAQABAMAIQAJAHAEAJIgbAQQgFgKgHAAQgBAAAAAAQgBAAAAABQgBAAAAAAQAAABAAAAQAAAEAHAEIANAGQAVAKAAASQAAAPgLAJQgLAKgTgBQgQABgLgKg");
	this.shape.setTransform(170.15,8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AggA0IAAhnIAlAAIAABKIAcAAIAAAdg");
	this.shape_1.setTransform(162.35,8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_2.setTransform(154.825,8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAHA0IgRgrIAAArIgmAAIAAhnIAmAAIAAAmIARgmIApAAIgaAxIAbA2g");
	this.shape_3.setTransform(146.025,8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AALA0IgWgxIAAAxIglAAIAAhnIAmAAIAWAxIAAgxIAmAAIAABng");
	this.shape_4.setTransform(135.25,8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgSA0IAAhnIAlAAIAABng");
	this.shape_5.setTransform(127.5,8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AANA0IgNg2IgMA2IgmAAIgbhnIAoAAIAKA5IANg5IAdAAIANA5IAKg5IAoAAIgbBng");
	this.shape_6.setTransform(117.225,8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_7.setTransform(102.725,8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgsA0IAAgTIAog3IgnAAIAAgdIBXAAIAAATIgnA3IAoAAIAAAdg");
	this.shape_8.setTransform(94.325,8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AALA0IgWgxIAAAxIgmAAIAAhnIAnAAIAWAxIAAgxIAlAAIAABng");
	this.shape_9.setTransform(84.25,8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgpAnQgQgPAAgYQAAgXAQgPQAQgQAZAAQAaAAAQAQQAQAPAAAXQAAAYgQAPQgQAPgaAAQgZAAgQgPgAgNgPQgEAFAAAKQAAALAEAFQAFAHAIAAQAJAAAFgHQAEgFAAgLQAAgKgEgFQgFgHgJAAQgIAAgFAHg");
	this.shape_10.setTransform(72.925,8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AALA0IgWgxIAAAxIglAAIAAhnIAmAAIAWAxIAAgxIAmAAIAABng");
	this.shape_11.setTransform(58.75,8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAQA0IgFgQIgXAAIgFAQIglAAIAkhnIAlAAIAkBngAAFAJIgFgUIgGAUIALAAg");
	this.shape_12.setTransform(47.775,8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgSA0IgmhnIApAAIAQA8IAQg8IAoAAIgmBng");
	this.shape_13.setTransform(36.375,8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AALA0IgWgxIAAAxIglAAIAAhnIAmAAIAWAxIAAgxIAmAAIAABng");
	this.shape_14.setTransform(22.3,8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AggBCIAAhoIBBAAIAAAeIgcAAIAAAIIAZAAIAAAbIgZAAIAAAKIAcAAIAAAdgAgQgqIALgXIAiAAIgSAXg");
	this.shape_15.setTransform(13.175,6.625);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AggBCIAAhoIBBAAIAAAeIgcAAIAAAIIAZAAIAAAbIgZAAIAAAKIAcAAIAAAdgAgQgqIALgXIAiAAIgSAXg");
	this.shape_16.setTransform(5.725,6.625);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_17.setTransform(426.325,10.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_18.setTransform(413.575,10.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AAIBJIgVgvIAAAvIgzAAIAAiRIBAAAQAZgBAPAOQAOANAAAVQABAMgIALQgFAIgLAGIAiA9gAgNgMIAHAAQAFAAABgEQADgCAAgFQAAgEgDgCQgBgEgFAAIgHAAg");
	this.shape_19.setTransform(402.25,10.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAWBJIgGgWIghAAIgHAWIg0AAIAziRIA0AAIAyCRgAAHAOIgHgeIgIAeIAPAAg");
	this.shape_20.setTransform(387.3,10.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgaBJIg1iRIA6AAIAWBUIAXhUIA4AAIg1CRg");
	this.shape_21.setTransform(371.375,10.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AAHBJIgTgvIAAAvIg0AAIAAiRIBAAAQAagBAPAOQANANAAAVQAAAMgGALQgHAIgJAGIAhA9gAgMgMIAGAAQAEAAACgEQAEgCAAgFQAAgEgEgCQgCgEgEAAIgGAAg");
	this.shape_22.setTransform(357.1,10.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_23.setTransform(344.775,10.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_24.setTransform(330.325,10.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgZBJIAAhoIgdAAIAAgpIBuAAIAAApIgdAAIAABog");
	this.shape_25.setTransform(319.25,10.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AhCBJIAAiRIA0AAQAkgBAWAUQAXATAAAiQAAAigWAUQgXATgkAAgAgPAgIADAAQAZAAAAggQAAgfgZABIgDAAg");
	this.shape_26.setTransform(302.475,10.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgaBJIAAiRIA0AAIAACRg");
	this.shape_27.setTransform(291.85,10.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgyA6QgRgRAAgbIAAhYIA1AAIAABZQAAAQAOAAQAOAAAAgQIAAhZIA2AAIAABYQAAAbgSARQgSARggAAQggAAgSgRg");
	this.shape_28.setTransform(281.175,10.55);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_29.setTransform(268.825,10.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_30.setTransform(258.325,10.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("Ag1A2QgWgVAAghQABggAVgVQAXgWAiAAQAnAAAYAXIgeAgQgMgKgQAAQgLAAgJAIQgHAJgBANQABAOAHAJQAJAJAMAAQAMAAAGgEIAAgGIgQAAIAAgiIBAAAIAABAQgcAYgrAAQgkAAgWgWg");
	this.shape_31.setTransform(245.15,10.425);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgZBJIAAhoIgdAAIAAgpIBtAAIAAApIgcAAIAABog");
	this.shape_32.setTransform(227.55,10.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_33.setTransform(216.525,10.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AAPBJIAAg0IgeAAIAAA0Ig1AAIAAiRIA1AAIAAA1IAeAAIAAg1IA2AAIAACRg");
	this.shape_34.setTransform(203.825,10.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AAmBJIAAhHIgPA8IguAAIgOg8IAABHIg0AAIAAiRIBDAAIAWBSIAXhSIBDAAIAACRg");
	this.shape_35.setTransform(182.675,10.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("Ag7A2QgWgVABghQgBggAWgVQAXgWAkAAQAlAAAWAWQAXAVAAAgQAAAhgXAVQgWAWglAAQgkAAgXgWgAgSgWQgGAIAAAOQAAAOAGAJQAHAJALAAQAMAAAHgJQAHgJgBgOQABgOgHgIQgHgKgMAAQgLAAgHAKg");
	this.shape_36.setTransform(164.8,10.425);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgnA/QgNgLgFgOIAmgVQAJAQAKAAQAEAAAAgEQAAgEgIgFIgUgJQgcgOAAgaQAAgVAPgNQAPgMAYAAQAWAAARAMQANAKAEAMIglAWQgHgOgKAAQgEAAAAAEQAAAEAJAFIAUAKQAdAOAAAZQAAAVgQANQgQANgaAAQgWAAgRgNg");
	this.shape_37.setTransform(146.375,10.425);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_38.setTransform(135.425,10.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_39.setTransform(124.925,10.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AAKBJIgZg8IAAA8Ig1AAIAAiRIA1AAIAAA1IAZg1IA5AAIgkBGIAmBLg");
	this.shape_40.setTransform(112.6,10.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_41.setTransform(97.525,10.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AgZBJIAAiRIA0AAIAACRg");
	this.shape_42.setTransform(86.7,10.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AASBJIgShLIgRBLIg1AAIgmiRIA4AAIAOBQIAShQIApAAIASBQIAOhQIA5AAIgmCRg");
	this.shape_43.setTransform(72.3,10.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_44.setTransform(51.975,10.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("Ag+BJIAAgbIA3hOIg2AAIAAgoIB7AAIAAAbIg4BNIA5AAIAAApg");
	this.shape_45.setTransform(40.225,10.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_46.setTransform(26.125,10.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("Ag7A2QgVgVAAghQAAggAVgVQAXgWAkAAQAlAAAXAWQAVAVAAAgQAAAhgVAVQgXAWglAAQgkAAgXgWgAgSgWQgGAIgBAOQABAOAGAJQAHAJALAAQAMAAAHgJQAGgJABgOQgBgOgGgIQgHgKgMAAQgLAAgHAKg");
	this.shape_47.setTransform(10.3,10.425);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,473,25);


(lib.endframe_static_line1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E31E26").s().p("AgbAtQgJgIgFgKIAcgOQAGALAHgBQABAAAAAAQABAAAAAAQABAAAAgBQAAAAAAgBQAAgDgFgDIgPgHQgUgKAAgSQAAgPALgKQAKgIARgBQAQABAMAIQAJAHAEAJIgbAQQgFgKgHAAQgBAAAAAAQgBAAAAABQgBAAAAAAQAAABAAAAQAAAEAHAEIANAGQAVAKAAASQAAAPgLAJQgLAKgTgBQgPABgMgKg");
	this.shape.setTransform(214.6,8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E31E26").s().p("AglAnQgQgPAAgYQAAgWAQgQQAPgQAYAAQAdAAAQARIgUAYQgKgIgKAAQgIAAgGAGQgGAGAAAJQAAAKAGAGQAFAHAJAAQAIAAAFgDIAAgEIgMAAIAAgZIAuAAIAAAuQgUARgfAAQgZAAgPgPg");
	this.shape_1.setTransform(204.6,8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E31E26").s().p("AALA0IgWgxIAAAxIgmAAIAAhnIAnAAIAWAxIAAgxIAlAAIAABng");
	this.shape_2.setTransform(193.7,8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E31E26").s().p("AAQA0IgFgQIgXAAIgFAQIglAAIAkhnIAlAAIAkBngAAFAJIgFgUIgGAUIALAAg");
	this.shape_3.setTransform(182.725,8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E31E26").s().p("AghA0IAAhnIAlAAIAABKIAeAAIAAAdg");
	this.shape_4.setTransform(173.65,8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E31E26").s().p("AAbA0IAAgyIgKAqIghAAIgKgqIAAAyIglAAIAAhnIAvAAIAQA6IAQg6IAwAAIAABng");
	this.shape_5.setTransform(160.175,8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E31E26").s().p("AgpAnQgQgPAAgYQAAgXAQgPQAQgQAZAAQAaAAAQAQQAQAPAAAXQAAAYgQAPQgQAPgaAAQgZAAgQgPgAgNgPQgEAFAAAKQAAALAEAFQAFAHAIAAQAJAAAFgHQAEgFAAgLQAAgKgEgFQgFgHgJAAQgIAAgFAHg");
	this.shape_6.setTransform(147.375,8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E31E26").s().p("AAHA0IgRgrIAAArIgmAAIAAhnIAmAAIAAAmIARgmIApAAIgaAxIAbA2g");
	this.shape_7.setTransform(136.375,8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgfA0IAAhnIA/AAIAAAdIgbAAIAAAMIAZAAIAAAbIgZAAIAAAjg");
	this.shape_8.setTransform(124.65,8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgpAnQgQgPAAgYQAAgXAQgPQAQgQAZAAQAaAAAQAQQAQAPAAAXQAAAYgQAPQgQAPgaAAQgZAAgQgPgAgNgPQgEAFAAAKQAAALAEAFQAFAHAIAAQAJAAAFgHQAEgFAAgLQAAgKgEgFQgFgHgJAAQgIAAgFAHg");
	this.shape_9.setTransform(115.025,8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_10.setTransform(102.525,8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AALA0IgWgxIAAAxIglAAIAAhnIAmAAIAWAxIAAgxIAmAAIAABng");
	this.shape_11.setTransform(93.4,8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgSA0IAAhnIAlAAIAABng");
	this.shape_12.setTransform(85.65,8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AghA0IAAhnIAmAAIAABKIAdAAIAAAdg");
	this.shape_13.setTransform(79.8,8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AALA0IgWgxIAAAxIgmAAIAAhnIAnAAIAWAxIAAgxIAlAAIAABng");
	this.shape_14.setTransform(70.6,8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgpAnQgQgPAAgYQAAgXAQgPQAQgQAZAAQAaAAAQAQQAQAPAAAXQAAAYgQAPQgQAPgaAAQgZAAgQgPgAgNgPQgEAFAAAKQAAALAEAFQAFAHAIAAQAJAAAFgHQAEgFAAgLQAAgKgEgFQgFgHgJAAQgIAAgFAHg");
	this.shape_15.setTransform(59.275,8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AghA0IAAhnIAlAAIAABKIAdAAIAAAdg");
	this.shape_16.setTransform(47,8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_17.setTransform(39.475,8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgSA0IAAhKIgVAAIAAgdIBOAAIAAAdIgUAAIAABKg");
	this.shape_18.setTransform(31.6,8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgcAtQgIgIgFgKIAcgOQAHALAGgBQABAAAAAAQABAAAAAAQABAAAAgBQAAAAAAgBQAAgDgGgDIgNgHQgVgKAAgSQAAgPALgKQAKgIARgBQAQABANAIQAJAHACAJIgbAQQgEgKgHAAQgBAAAAAAQgBAAAAABQgBAAAAAAQAAABAAAAQAAAEAGAEIAPAGQAUAKAAASQAAAPgLAJQgLAKgTgBQgQABgMgKg");
	this.shape_19.setTransform(23.05,8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AggA0IAAhnIBBAAIAAAdIgcAAIAAAJIAZAAIAAAbIgZAAIAAAJIAcAAIAAAdg");
	this.shape_20.setTransform(15.025,8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgqA0IAAhnIAsAAQASAAALAHQAKAIAAAOQAAANgLAHQAGADAEAGQADAGAAAHQAAAPgLAJQgKAIgRAAgAgGAYIAGAAQADAAACgBQACgDAAgDQAAgCgCgCQgCgDgDAAIgGAAgAgGgMIAGAAQAGAAAAgGQAAgFgGAAIgGAAg");
	this.shape_21.setTransform(6.775,8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_static_line1, new cjs.Rectangle(0,0,237.1,19), null);


(lib.emptyMovieClip = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.dollar_text_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgMAjIAAhFIAZAAIAABFg");
	this.shape.setTransform(234.475,6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAHAjIgOghIAAAhIgZAAIAAhFIAZAAIAPAhIAAghIAZAAIAABFg");
	this.shape_1.setTransform(229.3,6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgXAcQgIgIAAgNIAAgqIAZAAIAAAqQAAAIAGAAQAHAAAAgIIAAgqIAZAAIAAAqQAAANgIAIQgJAIgPAAQgOAAgJgIg");
	this.shape_2.setTransform(222.175,6.075);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAiIACgUIAGABQAFAAgBgEIAAgtIAZAAIAAAxQAAAUgWAAQgIAAgHgBg");
	this.shape_3.setTransform(216.5,6.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgZAkIAAgJQAAgHAEgGQADgFAHgFIAKgIQADgEAAgEQAAAAAAgBQgBgBAAAAQAAAAgBgBQAAAAAAAAQgDAAgCAGIgVgEQACgJAGgGQAHgHALAAQAMAAAHAGQAIAGAAALQAAAKgMAJIgIAGQgDACAAACIAXAAIAAATg");
	this.shape_4.setTransform(209.675,5.925);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgZAaQgKgKAAgQQAAgOAKgLQALgKAQgBQATAAALAMIgPAPQgFgFgHAAQgFAAgEAEQgEAEAAAGQAAAHADAEQAEAEAGAAQAFAAADgBIAAgEIgIAAIAAgQIAfAAIAAAfQgNALgVAAQgQAAgLgKg");
	this.shape_5.setTransform(201.125,6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AALAjIgDgKIgQAAIgDAKIgZAAIAYhFIAYAAIAZBFgAADAHIgDgOIgEAOIAHAAg");
	this.shape_6.setTransform(193.775,6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgfAjIAAhFIAZAAQARAAAKAJQALAJAAAQQAAAQgLAKQgKAJgRAAgAgHAPIACAAQALAAAAgPQAAgOgLAAIgCAAg");
	this.shape_7.setTransform(186.8,6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAHAjIgOghIAAAhIgZAAIAAhFIAZAAIAPAhIAAghIAZAAIAABFg");
	this.shape_8.setTransform(179.7,6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgcAaQgKgKABgQQgBgOAKgLQALgLARAAQASAAAKALQALALAAAOQAAAQgLAKQgKAKgSAAQgRAAgLgKgAgIgKQgDAEAAAGQAAAHADAEQADAEAFABQAGgBADgEQADgEAAgHQAAgGgDgEQgDgFgGAAQgFAAgDAFg");
	this.shape_9.setTransform(172.15,6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgdAjIAAgNIAaglIgZAAIAAgTIA6AAIAAANIgbAlIAbAAIAAATg");
	this.shape_10.setTransform(165.125,6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AASAjIAAgiIgHAcIgVAAIgHgcIAAAiIgZAAIAAhFIAgAAIAKAnIALgnIAgAAIAABFg");
	this.shape_11.setTransform(155.525,6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgYArIAdhVIAUAAIgdBVg");
	this.shape_12.setTransform(148.55,6.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgMAjIAAgyIgOAAIAAgTIA0AAIAAATIgNAAIAAAyg");
	this.shape_13.setTransform(143.35,6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgMAjIAAhFIAZAAIAABFg");
	this.shape_14.setTransform(137.125,6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAATIgSAAIAAAGIAQAAIAAASIgQAAIAAAHIASAAIAAATg");
	this.shape_15.setTransform(133.075,6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AASAjIAAgiIgHAcIgVAAIgHgcIAAAiIgZAAIAAhFIAgAAIAKAnIALgnIAgAAIAABFg");
	this.shape_16.setTransform(126.025,6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAAAjIAAgOIgdAAIAAgOIAagpIAaAAIAAAkIAHAAIAAATIgHAAIAAAOgAgIACIAIAAIAAgMg");
	this.shape_17.setTransform(116.425,6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgZAkIAAgJQAAgHAEgGQADgFAHgFIAKgIQADgEAAgEQAAAAAAgBQgBgBAAAAQAAAAgBgBQAAAAAAAAQgDAAgCAGIgVgEQACgJAGgGQAHgHALAAQAMAAAHAGQAIAGAAALQAAAKgMAJIgIAGQgDACAAACIAXAAIAAATg");
	this.shape_18.setTransform(110.375,5.925);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgZAaQgKgKAAgQQAAgOAKgLQALgKAQgBQATAAALAMIgPAPQgFgFgHAAQgFAAgEAEQgEAEAAAGQAAAHADAEQAEAEAGAAQAFAAADgBIAAgEIgIAAIAAgQIAfAAIAAAfQgNALgVAAQgQAAgLgKg");
	this.shape_19.setTransform(101.825,6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AALAjIgDgKIgQAAIgDAKIgZAAIAYhFIAYAAIAZBFgAADAHIgDgOIgEAOIAHAAg");
	this.shape_20.setTransform(94.475,6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgfAjIAAhFIAZAAQARAAAKAJQALAJAAAQQAAAQgLAKQgKAJgRAAgAgGAPIABAAQALAAAAgPQAAgOgLAAIgBAAg");
	this.shape_21.setTransform(87.5,6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgSAiIACgUIAGABQAFAAgBgEIAAgtIAZAAIAAAxQAAAUgXAAQgHAAgHgBg");
	this.shape_22.setTransform(81.85,6.025);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgMAjIAAhFIAZAAIAABFg");
	this.shape_23.setTransform(78.325,6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AADAjIgJgWIAAAWIgYAAIAAhFIAeAAQAMAAAHAGQAHAHAAAKQAAAGgDAEQgDAEgFADIAQAdgAgGgFIAEAAQAAAAABAAQAAAAABAAQAAgBAAAAQAAAAAAgBIABgDIgBgDQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBAAAAAAIgEAAg");
	this.shape_24.setTransform(73.85,6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgMAjIgZhFIAbAAIALAoIALgoIAaAAIgZBFg");
	this.shape_25.setTransform(66.575,6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAIAjIgPghIAAAhIgZAAIAAhFIAZAAIAPAhIAAghIAZAAIAABFg");
	this.shape_26.setTransform(57.2,6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AALAjIgDgKIgQAAIgDAKIgZAAIAYhFIAYAAIAZBFgAADAHIgDgOIgEAOIAHAAg");
	this.shape_27.setTransform(49.925,6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgMAjIgZhFIAbAAIALAoIALgoIAaAAIgZBFg");
	this.shape_28.setTransform(42.325,6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgZAaQgKgKAAgQQAAgOAKgLQALgKAQgBQATAAALAMIgPAPQgFgFgHAAQgFAAgEAEQgEAEAAAGQAAAHADAEQAEAEAGAAQAFAAADgBIAAgEIgIAAIAAgQIAfAAIAAAfQgNALgVAAQgQAAgLgKg");
	this.shape_29.setTransform(32.775,6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgMAjIAAhFIAZAAIAABFg");
	this.shape_30.setTransform(27.575,6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgfAjIAAhFIAZAAQAQAAAMAJQAKAJAAAQQAAAQgKAKQgMAJgQAAgAgHAPIACAAQALAAAAgPQAAgOgLAAIgCAAg");
	this.shape_31.setTransform(22.75,6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgVAjIAAhFIAYAAIAAAyIATAAIAAATg");
	this.shape_32.setTransform(16.925,6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAATIgSAAIAAAGIAQAAIAAASIgQAAIAAAHIASAAIAAATg");
	this.shape_33.setTransform(11.925,6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgZAaQgKgKAAgQQAAgOAKgLQALgKAQgBQATAAALAMIgPAPQgFgFgHAAQgFAAgEAEQgEAEAAAGQAAAHADAEQAEAEAGAAQAFAAADgBIAAgEIgIAAIAAgQIAfAAIAAAfQgNALgVAAQgQAAgLgKg");
	this.shape_34.setTransform(5.675,6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_3, new cjs.Rectangle(0,0,238.2,14), null);


(lib.dollar_text_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AhMBUIAAinIA8AAQAqAAAZAWQAaAXAAAmQAAAngaAWQgZAXgqAAgAgRAkIAEAAQAcAAAAgkQAAgjgcAAIgEAAg");
	this.shape.setTransform(204.15,11.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("Ag1BUIAAinIA8AAIAAB4IAvAAIAAAvg");
	this.shape_1.setTransform(190.125,11.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E90000").s().p("AgZBIQgXgOgIgZIgSAAIAAgdIALAAIAAgGIgLAAIAAgeIASAAQAJgXAWgPQAXgQAbAAQAdAAAUAMIgUAoQgMgHgNAAQgOAAgLAJIAnAAIgEAeIgsAAIgBADIABADIAsAAIgDAdIgfAAQAIAIARAAQAPAAALgIIATAoQgRAOgeAAQgeAAgXgPg");
	this.shape_2.setTransform(175.525,11.575);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E90000").s().p("Ag9A+QgYgYAAgmQAAgkAZgZQAZgZAnAAQAtAAAbAbIgiAlQgOgNgRAAQgOAAgJALQgKAJAAAPQAAARAJAJQAKAKAOAAQANABAIgFIAAgHIgTAAIAAgnIBJAAIAABJQggAcgxAAQgpAAgZgZg");
	this.shape_3.setTransform(158.475,11.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("Ag0BUIAAinIBoAAIAAAvIgsAAIAAAOIAnAAIAAAsIgnAAIAAAPIAsAAIAAAvg");
	this.shape_4.setTransform(143.7,11.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AgeBUIAAinIA9AAIAACng");
	this.shape_5.setTransform(134,11.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AgeBUIAAh4IggAAIAAgvIB+AAIAAAvIgiAAIAAB4g");
	this.shape_6.setTransform(123.6,11.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AASBUIgkhPIAABPIg8AAIAAinIA+AAIAjBPIAAhPIA8AAIAACng");
	this.shape_7.setTransform(108.275,11.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AAaBUIgIgZIgmAAIgIAZIg7AAIA6inIA7AAIA6CngAAIAPIgIghIgKAhIASAAg");
	this.shape_8.setTransform(90.725,11.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AALBUIgchFIAABFIg9AAIAAinIA9AAIAAA9IAcg9IBCAAIgpBQIArBXg");
	this.shape_9.setTransform(73.65,11.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AAaBUIgIgZIgmAAIgIAZIg7AAIA6inIA7AAIA6CngAAIAPIgIghIgKAhIASAAg");
	this.shape_10.setTransform(56.125,11.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E90000").s().p("AgeBUIg9inIBCAAIAaBiIAbhiIBAAAIg9Cng");
	this.shape_11.setTransform(37.875,11.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("AgzBUIAAinIBnAAIAAAvIgsAAIAAAOIAoAAIAAAsIgoAAIAAAPIAsAAIAAAvg");
	this.shape_12.setTransform(18,11.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E90000").s().p("AgtBQIAHgtIAMABQALAAAAgJIAAhvIA8AAIAAB4QAAAxg3AAQgRAAgSgFg");
	this.shape_13.setTransform(6.8,11.675);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_2, new cjs.Rectangle(0,0,214.2,28), null);


(lib.dollar_text_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeBUIAAh4IggAAIAAgvIB+AAIAAAvIgiAAIAAB4g");
	this.shape.setTransform(173.8,11.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgeBUIAAinIA9AAIAACng");
	this.shape_1.setTransform(163.35,11.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag4BCQgVgTAAgeIAAhlIA9AAIAABmQAAARAQABQARgBAAgRIAAhmIA9AAIAABlQgBAegTATQgVAUglAAQgkAAgUgUg");
	this.shape_2.setTransform(151.15,11.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgtBIQgOgMgGgRIArgXQALARALABQAEgBAAgEQAAgEgJgGIgWgKQghgQAAgdQAAgZARgPQASgOAbAAQAaAAATAOQAPALAEAOIgqAZQgIgQgLAAQgFABAAAEQAAAFAKAGIAXAKQAhARAAAcQAAAZgSAOQgSAPgeAAQgaAAgTgPg");
	this.shape_3.setTransform(131.175,11.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgzBUIAAinIBnAAIAAAvIgsAAIAAAOIAoAAIAAAsIgoAAIAAAPIAsAAIAAAvg");
	this.shape_4.setTransform(118.35,11.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("Ag1BUIAAinIA8AAIAAB4IAvAAIAAAvg");
	this.shape_5.setTransform(106.775,11.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ag1BUIAAinIA8AAIAAB4IAvAAIAAAvg");
	this.shape_6.setTransform(95.075,11.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAaBUIgIgZIgmAAIgIAZIg7AAIA6inIA7AAIA6CngAAIAPIgIghIgKAhIASAAg");
	this.shape_7.setTransform(80.075,11.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag1BUIAAinIA8AAIAAB4IAvAAIAAAvg");
	this.shape_8.setTransform(60.925,11.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAaBUIgIgZIgmAAIgIAZIg7AAIA6inIA7AAIA6CngAAIAPIgIghIgKAhIASAAg");
	this.shape_9.setTransform(45.925,11.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAaBUIgIgZIgmAAIgIAZIg7AAIA6inIA7AAIA6CngAAIAPIgIghIgKAhIASAAg");
	this.shape_10.setTransform(28.075,11.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AASBUIAAg8IgjAAIAAA8Ig9AAIAAinIA9AAIAAA9IAjAAIAAg9IA9AAIAACng");
	this.shape_11.setTransform(10.575,11.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_1, new cjs.Rectangle(0,0,182.7,28), null);


(lib.dollar_prijs = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgMAHIAAgNIAZAAIAAANg");
	this.shape.setTransform(100.025,5.975);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgFAGQgDgCAAgEQAAgDADgCQACgDADAAQAEAAACADQADACAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_1.setTransform(97.175,7.975);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AADAfIAAgNIgbAAIAAgMIAYglIASAAIAAAjIAHAAIAAAOIgHAAIAAANgAgIAEIALAAIAAgSg");
	this.shape_2.setTransform(93.3,5.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AADAfIAAgNIgbAAIAAgMIAYglIASAAIAAAjIAHAAIAAAOIgHAAIAAANgAgIAEIALAAIAAgSg");
	this.shape_3.setTransform(87.9,5.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgOAfIATgwIgZAAIAAgOIApAAIAAALIgTAzg");
	this.shape_4.setTransform(82.85,5.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgFAWQgDgDAAgDQAAgEADgDQACgDADABQAEgBACADQADADAAAEQAAADgDADQgCACgEAAQgDAAgCgCgAgFgIQgDgDAAgDQAAgEADgCQACgDADAAQAEAAACADQADACAAAEQAAADgDADQgCACgEAAQgDAAgCgCg");
	this.shape_5.setTransform(77.625,6.45);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAFAgIgMgVIAAAVIgPAAIAAhAIAPAAIAAAkIAMgSIARAAIgPAVIAQAZg");
	this.shape_6.setTransform(74.175,5.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgMASQgHgHAAgLQAAgKAHgHQAIgHAKAAQAIAAAGAFIgEAMQgFgDgFAAQgFAAgCADQgDADAAAEQAAAFADAEQADADAFAAQAFAAAEgEIAEANQgHAEgHAAQgKAAgIgHg");
	this.shape_7.setTransform(69.325,6.375);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgPAVQgFgFAAgHQAAgHAFgDQAFgFAIAAQAEAAAEACIAAgCQAAgFgGAAQgGAAgGADIgGgKQAJgGAKAAQALAAAFAGQAEAEAAAJIAAAdIgNAAIgBgDQgEAEgGAAQgHAAgFgEgAgEAFQAAAAAAABQgBAAAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAABAAQAAABAAAAIAEABQAGAAAAgEQAAgBAAAAQAAgBAAgBQgBAAAAAAQAAgBgBAAQAAgBgBAAQAAAAgBAAQAAgBgBAAQgBAAAAAAQAAAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABg");
	this.shape_8.setTransform(64.675,6.375);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgGAgIgEgEIAAAEIgOAAIAAhAIAPAAIAAAWIAEgDQAEgDADAAQALABAGAGQAGAIAAAJQAAALgGAHQgHAHgLAAQgDAAgEgBgAgGAAQgDADAAAFQAAAFACADQADAEAEAAQAEAAADgDQACgDABgGQgBgEgCgEQgDgCgEAAQgEAAgCACg");
	this.shape_9.setTransform(59.7,5.55);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAGAgIAAgcQAAgFgGgBQgCAAgCACQgBACAAACIAAAcIgPAAIAAhAIAPAAIAAAXQADgGAHAAQAKAAAEAIQACAFAAAIIAAAag");
	this.shape_10.setTransform(54.225,5.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgLAWQgEgCgCgDIAIgIQAFAEAEAAQAFAAgBgDQAAAAAAgBQAAAAAAAAQgBgBAAAAQgBAAgBgBIgHgDQgKgEAAgJQAAgHAFgEQAFgEAHAAQALAAAGAIIgKAIQgDgEgEAAQgBAAAAAAQgBABAAAAQgBAAAAABQAAAAAAABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAIAHACQALAEAAAJQgBAHgFAFQgGAEgHAAQgGAAgFgDg");
	this.shape_11.setTransform(49.7,6.375);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgPAVQgFgFAAgHQAAgHAFgDQAFgFAIAAQAEAAAEACIAAgCQAAgFgGAAQgGAAgGADIgGgKQAJgGAKAAQALAAAFAGQAEAEAAAJIAAAdIgNAAIgBgDQgEAEgGAAQgHAAgFgEgAgEAFQAAAAAAABQgBAAAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAABAAQAAABAAAAIAEABQAGAAAAgEQAAgBAAAAQAAgBAAgBQgBAAAAAAQAAgBgBAAQAAgBgBAAQAAAAgBAAQAAgBgBAAQgBAAAAAAQAAAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABg");
	this.shape_12.setTransform(45.225,6.375);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgMASQgHgHAAgLQAAgKAHgHQAIgHAKAAQAIAAAGAFIgEAMQgFgDgFAAQgFAAgCADQgDADAAAEQAAAFADAEQADADAFAAQAFAAAEgEIAEANQgHAEgHAAQgKAAgIgHg");
	this.shape_13.setTransform(40.775,6.375);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgPAVQgFgFAAgHQAAgHAFgDQAFgFAIAAQAEAAAEACIAAgCQAAgFgGAAQgGAAgGADIgGgKQAJgGAKAAQALAAAFAGQAEAEAAAJIAAAdIgNAAIgBgDQgEAEgGAAQgHAAgFgEgAgEAFQAAAAAAABQgBAAAAAAQAAABAAABQAAAAAAABQAAAAAAABQAAAAAAABQAAAAABAAQAAABAAAAIAEABQAGAAAAgEQAAgBAAAAQAAgBAAgBQAAAAgBAAQAAgBgBAAQAAgBgBAAQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAAAAAAAQgBAAgBABQAAAAgBAAQAAAAgBABg");
	this.shape_14.setTransform(34.375,6.375);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAGAYIAAgbQAAgHgGAAQgCAAgCACQgBACAAADIAAAbIgPAAIAAguIAOAAIAAAFQAEgGAHAAQAKAAAEAHQACAFAAAKIAAAZg");
	this.shape_15.setTransform(29.525,6.325);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgLAWQgEgCgCgDIAIgIQAFAEAEAAQAFAAgBgDQAAAAAAgBQAAAAAAAAQgBgBAAAAQgBAAgBgBIgHgDQgKgEAAgJQAAgHAFgEQAFgEAHAAQALAAAGAIIgKAIQgEgEgDAAQgBAAAAAAQgBABAAAAQgBAAAAABQAAAAAAABQAAAAAAABQAAAAABABQAAAAABAAQAAABABAAIAHACQALAEAAAJQgBAHgFAFQgGAEgHAAQgGAAgFgDg");
	this.shape_16.setTransform(23.25,6.375);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgLApIAAgMIACAAQAHAAAAgFIAAguIAOAAIAAAuQAAASgUAAIgDgBgAgCgbQgBgCAAgDQAAgFABgCQACgCAEAAQADAAACACQADACAAAFQAAADgDACQgCADgDAAQgEAAgCgDg");
	this.shape_17.setTransform(19.7,6.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgGAhIAAguIAOAAIAAAugAgFgSQgCgCAAgEQAAgEACgCQACgCADAAQAEAAACACQACACAAAEQAAADgCADQgCACgEAAQgDAAgCgCg");
	this.shape_18.setTransform(17.725,5.425);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgNAYIAAguIAOAAIAAAGQAEgHAIAAIAAAPIgCAAQgJAAAAALIAAAVg");
	this.shape_19.setTransform(15.05,6.325);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgYAhIAAg/IAOAAIAAADIAEgDQAEgCADAAQALAAAGAHQAHAHAAALQAAAKgGAHQgHAHgJAAQgEAAgEgCIgEgDIAAAVgAgHgPQgCADgBAFQAAAFADADQADADAEAAQAEAAADgEQACgCAAgFQAAgFgCgDQgDgEgEAAQgEAAgDAEg");
	this.shape_20.setTransform(10.6,7.175);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgEARIAAgJIgHAEIgFgIIAHgEIgHgDIAFgIIAHADIAAgIIAJAAIAAAIIAHgDIAFAIIgHADIAHAEIgFAIIgHgEIAAAJg");
	this.shape_21.setTransform(3.95,4.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_prijs, new cjs.Rectangle(0,0,104,13), null);


(lib.dollar_cashback_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAEAmIgKgZIAAAZIgbAAIAAhLIAhAAQANAAAIAGQAIAIgBAKQAAAHgDAGQgEADgFAEIASAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape.setTransform(10.6,7.15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAMAmIgDgLIgRAAIgEALIgcAAIAbhLIAaAAIAbBLgAADAHIgDgPIgEAPIAHAAg");
	this.shape_1.setTransform(2.8,7.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AALAmIgDgLIgRAAIgDALIgbAAIAahLIAbAAIAbBLgAADAHIgDgPIgEAPIAHAAg");
	this.shape_2.setTransform(-5.35,7.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgfAmIAAhLIAgAAQAOAAAHAFQAIAHAAAJQAAAKgIAFQAEABADAFQACAEAAAGQABALgIAHQgIAFgNAAgAgEASIAEAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAAAQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAIgEAAgAgEgIIAEAAQAFAAgBgFQABgEgFAAIgEAAg");
	this.shape_3.setTransform(-12.75,7.15);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgRAKIAAgSIAjAAIAAASg");
	this.shape_4.setTransform(24.675,-2.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbAcQgMgKAAgSQAAgQAMgLQALgMASAAQAUAAANANIgQARQgGgHgIAAQgGABgEAEQgFAFABAGQAAAHADAFQAFAFAGAAQAGAAADgDIAAgDIgIAAIAAgRIAhAAIAAAiQgOAMgXAAQgSAAgLgMg");
	this.shape_5.setTransform(18.3,-2.85);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgUAlIADgVIAGABQAFAAAAgEIAAgzIAbAAIAAA3QAAAWgZAAQgIAAgIgCg");
	this.shape_6.setTransform(11.85,-2.825);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgNAmIAAhLIAbAAIAABLg");
	this.shape_7.setTransform(7.95,-2.85);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAEAmIgKgZIAAAZIgbAAIAAhLIAhAAQANAAAIAGQAHAIAAAKQAAAHgDAGQgDADgFAEIARAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape_8.setTransform(3,-2.85);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAFAmIgMgfIAAAfIgcAAIAAhLIAcAAIAAAcIAMgcIAeAAIgSAkIATAng");
	this.shape_9.setTransform(-4.45,-2.85);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAEAmIgKgZIAAAZIgbAAIAAhLIAhAAQANAAAIAGQAIAIAAAKQgBAHgDAGQgDADgGAEIASAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape_10.setTransform(-11.6,-2.85);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgXAmIAAhLIAvAAIAAAVIgVAAIAAAHIASAAIAAATIgSAAIAAAHIAVAAIAAAVg");
	this.shape_11.setTransform(-18.05,-2.85);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgNAmIgchLIAeAAIALAsIANgsIAdAAIgcBLg");
	this.shape_12.setTransform(-25.05,-2.85);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgUAhQgGgGgDgHIAUgLQAEAJAFAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgDgCIgLgFQgPgHAAgOQAAgKAIgIQAIgGAMAAQAMAAAIAGQAIAGABAGIgTAMQgEgIgFABQAAAAgBAAQAAAAAAAAQgBAAAAABQAAAAAAAAQAAADAFACIAKAFQAPAIAAAMQAAALgIAHQgIAHgOAAQgLAAgJgHg");
	this.shape_13.setTransform(19.3,-12.85);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgeAdQgMgMAAgRQAAgQAMgLQALgMATAAQATAAAMAMQAMALAAAQQAAARgMAMQgMALgTAAQgTAAgLgLgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQAEgFAAgHQAAgGgEgFQgEgFgGAAQgGAAgDAFg");
	this.shape_14.setTransform(11.85,-12.85);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgXAmIAAhMIAbAAIAAA3IAUAAIAAAVg");
	this.shape_15.setTransform(4.95,-12.85);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAFAmIgMgfIAAAfIgcAAIAAhMIAcAAIAAAdIAMgdIAfAAIgTAlIATAng");
	this.shape_16.setTransform(-3.65,-12.85);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgeAdQgMgMABgRQgBgQAMgLQALgMATAAQATAAAMAMQALALAAAQQAAARgLAMQgMALgTAAQgTAAgLgLgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQADgFABgHQgBgGgDgFQgEgFgGAAQgGAAgDAFg");
	this.shape_17.setTransform(-11.9,-12.85);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgeAdQgMgMAAgRQAAgQAMgLQALgMATAAQATAAAMAMQAMALAAAQQAAARgMAMQgMALgTAAQgTAAgLgLgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQAEgFAAgHQAAgGgEgFQgEgFgGAAQgGAAgDAFg");
	this.shape_18.setTransform(-20.55,-12.85);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgRAJIAAgSIAjAAIAAASg");
	this.shape_19.setTransform(11.975,3.85);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgJAJQgEgEAAgFQAAgEAEgEQAEgEAFAAQAGAAAEAEQAEAEAAAEQAAAFgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_20.setTransform(8.275,6.125);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgeAAQAAgnAeAAQAfAAAAAnQAAAogfAAQgeAAAAgogAgDAAQAAASADAAQAEAAAAgSQAAgRgEAAQgDAAAAARg");
	this.shape_21.setTransform(3.525,3.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgeAAQAAgnAeAAQAfAAAAAnQAAAogfAAQgeAAAAgogAgDAAQAAASADAAQAEAAAAgSQAAgRgEAAQgDAAAAARg");
	this.shape_22.setTransform(-3.075,3.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgVAgQgGgGgCgJIAXgFQABAFAEAAQAEAAAAgFQAAgBAAAAQAAgBAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAgBAAAAQAAAAgBAAQgDAAgBADIgWgDIACgsIAyAAIAAAVIgcAAIAAAGIAGgBQAMAAAHAGQAIAHAAALQAAALgIAIQgJAIgNAAQgMAAgJgHg");
	this.shape_23.setTransform(-9.525,3.475);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAFAmIgMgfIAAAfIgcAAIAAhLIAcAAIAAAbIAMgbIAeAAIgSAkIATAng");
	this.shape_24.setTransform(26.6,-6.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgVAdQgMgLAAgSQAAgQALgMQAMgLASAAQAPAAAKAIIgIAVQgGgFgIgBQgHAAgEAFQgEAFAAAGQAAAHAEAFQAEAEAHAAQAIAAAHgGIAIAXQgLAHgPAAQgSAAgLgLg");
	this.shape_25.setTransform(19.075,-6.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AALAmIgCgMIgRAAIgEAMIgcAAIAbhLIAaAAIAbBLgAADAHIgDgPIgEAPIAHAAg");
	this.shape_26.setTransform(11.55,-6.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgfAmIAAhLIAhAAQANgBAHAHQAIAFAAAKQAAAKgIAFQAEACADAEQADAFgBAFQAAALgHAHQgIAFgNAAgAgEASIAEAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAgBQABAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAgBIgEAAgAgEgJIAEAAQAEAAAAgDQAAgFgEAAIgEAAg");
	this.shape_27.setTransform(4.15,-6.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAIAmIAAgbIgPAAIAAAbIgcAAIAAhLIAcAAIAAAbIAPAAIAAgbIAcAAIAABLg");
	this.shape_28.setTransform(-3.25,-6.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgUAhQgHgGgDgHIAUgLQAGAIAEAAQAAAAABAAQAAAAAAAAQABAAAAgBQAAAAAAAAQAAgDgEgCIgKgFQgPgHAAgOQAAgKAIgIQAIgGAMAAQAMAAAJAGQAGAGADAGIgUALQgEgHgEAAQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAAAQAAADAFACIAKAGQAPAGAAANQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape_29.setTransform(-10.35,-6.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAMAmIgEgMIgRAAIgDAMIgcAAIAbhLIAaAAIAbBLgAAEAHIgEgPIgEAPIAIAAg");
	this.shape_30.setTransform(-17.55,-6.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgVAdQgMgLAAgSQAAgQALgMQAMgLASAAQAPAAAKAIIgIAVQgGgFgIgBQgHAAgEAFQgEAFAAAGQAAAHAEAFQAEAEAHAAQAIAAAHgGIAIAXQgLAHgPAAQgSAAgLgLg");
	this.shape_31.setTransform(-25.225,-6.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgOAIIAAgPIAdAAIAAAPg");
	this.shape_32.setTransform(21.275,10.225);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_33.setTransform(18.275,12.075);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgVAcIAHgQQAEADAEAAQAGAAAAgGQgCABgEAAQgIAAgEgEQgGgGAAgJQAAgJAHgHQAGgHALAAQAZAAAAAeQAAAOgGAJQgIAMgOAAQgLAAgHgFgAgCgNIgBAEIABAEQAAAAABABQAAAAAAAAQABAAAAAAQAAAAAAAAIACgBQABgJgDAAQAAAAgBAAQAAAAAAAAQgBABAAAAQAAAAAAAAg");
	this.shape_34.setTransform(14.45,9.85);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgSAbQgGgFAAgLQAAgIAGgEQgFgEAAgIQAAgIAGgFQAHgGAKAAQALAAAGAGQAHAFAAAIQAAAIgFAEQAGAEAAAIQAAALgHAFQgGAGgMAAQgKAAgIgGgAgCAIIAAADQgBAFADAAQAEAAAAgFIgBgDQAAgBgBAAQAAAAAAgBQgBAAAAAAQAAAAgBAAQAAAAAAAAQAAAAgBAAQAAABAAAAQgBAAAAABgAgCgLQAAAEACAAQADAAAAgEQAAgEgDgBQgCABAAAEg");
	this.shape_35.setTransform(9.15,9.85);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_36.setTransform(3.625,12.075);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgLAfIgXg+IAZAAIAJAlIAKglIAYAAIgWA+g");
	this.shape_37.setTransform(-1.1,9.85);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_38.setTransform(-5.775,12.075);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AAIAfIgIgfIgHAfIgWAAIgRg+IAYAAIAGAiIAIgiIARAAIAIAiIAFgiIAZAAIgRA+g");
	this.shape_39.setTransform(-11.75,9.85);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_40.setTransform(-17.675,12.075);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgLAfIAAgsIgMAAIAAgSIAvAAIAAASIgNAAIAAAsg");
	this.shape_41.setTransform(-21.375,9.85);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_42.setTransform(27.425,1.85);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_43.setTransform(21.975,1.85);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgWAYQgKgKABgOQgBgNAKgJQAKgKANAAQASAAAJALIgMANQgFgEgHAAQgEAAgEADQgDAEgBAFQABAGADAEQADAEAGAAQAEAAADgCIAAgDIgHAAIAAgOIAcAAIAAAcQgNAJgSABQgOgBgKgIg");
	this.shape_44.setTransform(16.35,1.85);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_45.setTransform(9.825,1.85);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AAJAgIgCgKIgOAAIgCAKIgXAAIAWg+IAVAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_46.setTransform(3.3,1.85);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AAHAgIAAgXIgMAAIAAAXIgYAAIAAg+IAYAAIAAAWIAMAAIAAgWIAXAAIAAA+g");
	this.shape_47.setTransform(-3.25,1.85);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgYAgIAAg+IAaAAQAKAAAHAFQAGAHAAAJQAAAJgGAGQgHAFgKAAIgEAAIAAAVgAgCgEIACAAIADgBQAAAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQgBAAAAgBQAAAAgBAAQAAAAgBAAQAAgBAAAAIgCAAg");
	this.shape_48.setTransform(-9,1.85);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgYAXQgKgJAAgOQAAgNAKgJQAJgKAPAAQAQAAAJAKQAKAJAAANQAAAOgKAJQgJAKgQAAQgPAAgJgKgAgHgJQgDAEAAAFQAAAGADAEQADAEAEAAQAFAAADgEQADgEAAgGQAAgFgDgEQgDgEgFAAQgEAAgDAEg");
	this.shape_49.setTransform(-15.3,1.85);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_50.setTransform(-23.775,1.85);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_51.setTransform(-29.225,1.85);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_52.setTransform(24.275,-6.15);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_53.setTransform(18.825,-6.15);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AADAgIgIgVIAAAVIgWAAIAAg+IAbAAQALAAAHAFQAFAGABAJQAAAFgEAEQgCADgEADIAOAbgAgFgEIADAAQAAAAABgBQAAAAABAAQAAAAAAAAQAAgBAAAAIABgDIgBgDQAAAAAAgBQAAAAAAAAQgBAAAAAAQgBAAAAAAIgDAAg");
	this.shape_54.setTransform(14,-6.15);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_55.setTransform(8.725,-6.15);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAWAAIAAAsIARAAIAAASg");
	this.shape_56.setTransform(4.375,-6.15);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAWAAIAAAsIARAAIAAASg");
	this.shape_57.setTransform(-0.025,-6.15);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AAJAgIgCgKIgOAAIgDAKIgWAAIAVg+IAWAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_58.setTransform(-5.6,-6.15);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgLAgIAAgtIgMAAIAAgRIAvAAIAAARIgNAAIAAAtg");
	this.shape_59.setTransform(-11.425,-6.15);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgGIARgJQADAHAEAAQAAAAABAAQAAAAAAAAQAAgBABAAQAAAAAAAAQAAgCgDgCIgJgEQgMgFAAgLQAAgKAHgFQAGgGAKABQAJAAAIAEQAFAFACAFIgQAJQgDgFgEAAQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAABAAAAQAAACAEACIAIAFQANAFAAALQAAAIgHAGQgHAFgLABQgJgBgHgFg");
	this.shape_60.setTransform(-16.525,-6.15);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_61.setTransform(-22.325,-6.15);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgKAgIAAg+IAVAAIAAA+g");
	this.shape_62.setTransform(-26.95,-6.15);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgGIARgJQADAHAEAAQAAAAABAAQAAAAAAAAQAAgBABAAQAAAAAAAAQAAgCgDgCIgJgEQgMgFAAgLQAAgKAHgFQAGgGAKABQAJAAAIAEQAFAFACAFIgQAJQgDgFgEAAQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAABAAAAQAAACAEACIAIAFQANAFAAALQAAAIgHAGQgHAFgLABQgJgBgHgFg");
	this.shape_63.setTransform(12.975,-14.15);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgLAgIAAg+IAXAAIAAA+g");
	this.shape_64.setTransform(9,-14.15);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AgLAgIAAgtIgMAAIAAgRIAvAAIAAARIgNAAIAAAtg");
	this.shape_65.setTransform(5.125,-14.15);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AAKAgIgDgKIgOAAIgCAKIgXAAIAVg+IAWAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_66.setTransform(-0.7,-14.15);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AADAgIgIgVIAAAVIgWAAIAAg+IAbAAQALAAAHAFQAFAGAAAJQAAAFgCAEQgDADgFADIAPAbgAgFgEIADAAQABAAAAgBQAAAAABAAQAAAAAAAAQAAgBABAAIABgDIgBgDQgBAAAAgBQAAAAAAAAQgBAAAAAAQAAAAgBAAIgDAAg");
	this.shape_67.setTransform(-6.65,-14.15);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FFFFFF").s().p("AgWAYQgKgKAAgOQAAgNAKgJQAKgJANAAQARAAAKAKIgMANQgGgEgGAAQgFAAgDADQgDAEgBAFQABAGADAEQADADAFABQAFgBADgBIAAgDIgHAAIAAgNIAcAAIAAAbQgMAKgTAAQgOAAgKgJg");
	this.shape_68.setTransform(-13.1,-14.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19}]},1).to({state:[{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33.5,-19.7,66.3,37);


(lib.dollar_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AkfD2IgigJIg9ldQATAGAPADQBMAQBXgZQA1gOBmgwQBlgvA1gPQBXgYBNAPQARADARAGIA9FdIgigKQhMgQhYAZQg1APhlAwQhmAvg0APQg1APgxAAQggAAgegGgAkkDWQBMAQBXgZQA1gOBlgwQBmgvA1gPQBYgZBMAQIgzkdQhMgQhXAZQg1APhlAvQhmAwg2AOQhXAZhMgQgAg+ByQgmgTgIgwQgIguAaguQAaguAtgVQArgWAnAVQAmATAIAwQAIAugaAuQgaAtgtAWQgWALgVAAQgUAAgTgKgAAHArIhOAkIACANQAAADADABQAEACAEgCQAYgJAugWIBGggQAEgBACgEQACgFgBgEIgCgMQgcAMg0AYgAA6hkIhFAgQgvAWgYAJQgEACgCAEQgCAFAAAEIAQBXQABAEADABQADACAEgCIBGgfIBGghQAEgBADgDQABgEAAgEIgQhZQgBgDgCgCIgEgBIgEABgAg+BYIgBgEIA4gaIABADIAggPIAAgDIA4gZIABAFQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAAAIhHAgIhFAgIgBABQgBAAAAgBQAAAAAAAAQgBAAAAAAQAAgBAAAAgAhDA+IgPhYQAAgBAAAAQAAgBAAAAQAAAAABgBQAAAAAAAAIBBgdIAAAAQABAIAGgEQAHgCgBgIIA/gdQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAAAIAPBZQABABAAAAQAAAAgBABQAAAAAAABQgBAAAAAAIhHAfQgtAWgYAJIgBABIgCgBgAkhAQIgHgBIgCgLIAGABIgBgEIgGAAIgCgLIAJABQACgKAHgGQAIgHANAAQALgBAIAEIgEAQQgFgDgHAAQgLAAgEAIIAVgBIABAKIgWAAIAAAFIAWAAIACALIgSAAQAGAJALgBQAGgBAFgDIAKANQgGAGgMABIgCAAQgaAAgNgZgAElAoQgdgCgLgVIgHAAIgCgKIAFgBIgBgEIgFAAIgCgKIAIgBQABgLAIgGQAJgGALAAQAMAAAJAFIgFAOQgHgDgFAAQgLgBgDAIIAUAAIABAKIgXAAIABAEIAWAAIABAMIgQgBQAFAHALABQAFABAGgDIAKAPQgEAEgKAAIgEgBg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AkkDWIgzkdQBMAQBXgZQA2gOBmgwQBlgvA1gPQBXgZBMAQIAzEdQhMgQhYAZQg1APhmAvQhlAwg1AOQg1APgwAAQggAAgegGgAgThwQgtAVgaAuQgaAuAIAuQAIAwAmATQAmAUAsgVQAtgWAagtQAagugIguQgIgwgmgTQgTgKgUAAQgVAAgWALgAkoAPIAHABQAOAaAbgBQAMgBAGgGIgKgNQgFADgGABQgLABgGgJIASAAIgCgLIgWAAIAAgFIAWAAIgBgKIgVABQAEgIALAAQAHAAAFADIAEgQQgIgEgLABQgNAAgIAHQgHAGgCAKIgJgBIACALIAGAAIABAEIgGgBgAElAoQANABAFgEIgKgPQgGADgFgBQgLgBgFgHIAQABIgBgMIgWAAIgBgEIAXAAIgBgKIgUAAQADgIALABQAFAAAHADIAFgOQgJgFgMAAQgLAAgJAGQgIAGgBALIgIABIACAKIAFAAIABAEIgFABIACAKIAHAAQALAVAdACgAhCBgQgDgBAAgDIgCgNIBOgkQA0gYAcgMIACAMQABAEgCAFQgCAEgEABIhGAgQguAWgYAJIgEABIgEgBgAg/BUIABAEQAAABAAAAQABAAAAAAQAAABABAAQAAAAABgBIBFggIBHggQAAAAABAAQAAAAAAgBQABAAAAAAQAAgBAAAAIgBgFIg4AZIAAADIggAPIgBgDgAhGBGQgDgBgBgEIgQhXQAAgEACgFQACgEAEgCQAYgJAvgWIBFggQAFgBADABQACACABADIAQBZQAAAEgBAEQgDADgEABIhGAhIhGAfIgDABIgEgBgAhRgdQAAAAAAAAQgBABAAAAQAAAAAAABQAAAAAAABIAPBYQABABAAAAQAAAAAAAAQABAAAAAAQABAAAAgBQAYgJAtgWIBHgfQAAAAABAAQAAgBAAAAQABgBAAAAQAAAAgBgBIgPhZQAAAAAAAAQAAgBAAAAQgBAAAAAAQAAAAgBAAIg/AdQABAIgHACQgGAEgBgIIAAAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_5, new cjs.Rectangle(-38.3,-25.2,76.6,50.4), null);


(lib.dollar_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AD7EIQg6g0hYgWQg1gNhvgLQgwgEglgEQgzgHgegJQhPgTg3gsIgMgKIgZgZIB5lMIAZAZQA6A0BZAVQA0APBwAJQBwALA1AOQBYAWA6AzQANALAMAOIh5FMIgZgZgAloAnQA6A0BYAWQA1AOBwAKQBvAKA1ANQBZAWA6A0IBjkQQg6gzhYgXQg1gNhwgLQhwgJg0gOQhZgWg6g0gADqC2QgYgQABgZIgHgCIAEgLIAFACIACgFIgFgCIADgKIAHADQAHgJAKgBQALgCAKAHQAKAFAFAKIgLAJQgEgGgFgEQgKgEgGAEIASALIgFAKIgTgMIgCAFIATALIgFAKIgOgKQABAKAJAGQAEADAHABIABARIgCABQgHAAgIgGgAgmBsQgxgDgXglQgXgkARgsQAQgtAugbIAFgDQArgYAuAEQAxAEAXAkQAMAVABAXQAAASgHATQgQAsguAbQgnAYgqAAIgNgBgAhEgxIgaBIQgDAHAIABIAcADIgBADQgBADABADQACACADABIBPAHQAIABADgIIABgDIAcADQADAAADgBQAEgCABgDIAbhJQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQgCgCgDAAIgxgGQgJgBgCAHIgSAwIgkgDIASgvQACgIgIAAIgxgGIgCAAQgGAAgDAHgAg2AmIAKgbIBPAHIgLAcgAgnATQgEADgCADQgBAEACADQACADAFAAQADABAEgDQAEgDACgEQABgEgCgCQgCgDgEAAIgBAAQgEAAgDACgAgOAcQAAAAAAABQAAAAAAABQAAAAABAAQAAABABAAIAZACQABAAABAAQAAAAABgBQAAAAABAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAAAQgBAAgBAAIgZgDQgBAAAAABQgBAAAAAAQgBAAAAABQAAAAgBABgABAAnIgbgEIACgHIAEABQAHAAAIgEQAHgFACgGQADgHgEgFQgEgGgHAAQgHAAgIAEQgHADgCAHIgHAAIASgxIAxAGIAAAAIgaBIgAAygVQgBAEACAEQACACAEABQALABADgKQACgDgCgEQgDgDgEgBIgCAAQgJAAgDAJgAgnAZQABgDAFABQAEgBgBAEQgCAEgEAAQgEAAABgFgAhWAXIAbhIQAYAEAZACIgSAvIgGAAQACgGgDgFQgEgGgIAAQgHgBgIAEQgGAFgDAFQgCAHADAGQAEAFAHABIAEAAIgDAHgAguglQgEADgCADQgBAEACAEQACADAEAAQAFABAEgDQAFgCABgFQABgDgCgEQgCgCgEgBIgCAAQgEAAgDACgAAtAXIgDgBIABgDQACgHgIgBIgBgBQACgEAFgDQAFgDAFAAQAFABACADQADAEgCAFQgCAFgEADQgEACgEAAIgCAAgAhAAJQgDgDACgFQACgEAEgDQAGgDAFAAQAEACADADQADAEgCADIgBABIgHAAQgDADgBADIgBADIgEABQgFgBgCgEgAA7gPQgEgBABgEQABgEAFAAQAFABgCAEQgBAEgEAAIgBAAgAgrgZQgEgBACgEQABgEAFABQAEgBgBAFQgCAEgEAAIgBAAgAjrhYQgYgNABgdIgFgDIADgLIAEADIACgFIgEgCIAEgKIAGADIAEgDQAFgFAHgBQALgBAKAGQALAGAFAHIgMAKQgCgFgHgDQgJgEgHAEIASAJIgFALIgTgLIgCAFIATAKIgFAKIgPgIQACAJAJAGQAHACAFgBIABARIgFABQgFAAgIgEg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("ABzCgQg1gNhvgKQhwgKg1gOQhYgWg6g0IBjkQQA6A0BZAWQA0AOBwAJQBwALA1ANQBYAXA6AzIhjEQQg6g0hZgWgADMCLIAHACQgBAZAYAQQAKAHAHgCIgBgRQgHgBgEgDQgJgGgBgKIAOAKIAFgKIgTgLIACgFIATAMIAFgKIgSgLQAGgEAKAEQAFAEAEAGIALgJQgFgKgKgFQgKgHgLACQgKABgHAJIgHgDIgDAKIAFACIgCAFIgFgCgAgxhXIgFADQguAbgQAtQgRAsAXAkQAXAlAxADQAxAEAtgbQAugbAQgsQAHgTAAgSQgBgXgMgVQgXgkgxgEIgOgBQgmAAglAVgAkHiFIAFADQgBAdAYANQAMAGAGgDIgBgRQgFABgHgCQgJgGgCgJIAPAIIAFgKIgTgKIACgFIATALIAFgLIgSgJQAHgEAJAEQAHADACAFIAMgKQgFgHgLgGQgKgGgLABQgHABgFAFIgEADIgGgDIgEAKIAEACIgCAFIgEgDgAg5AuQgDgBgCgCQgBgDABgDIABgDIgcgDQgIgBADgHIAahIQADgIAIABIAxAGQAIAAgCAIIgSAvIAkADIASgwQACgHAJABIAxAGQADAAACACQAAABABAAQAAABAAAAQAAABAAABQAAAAAAABIgbBJQgBADgEACQgDABgDAAIgcgDIgBADQgDAIgIgBgAg2AmIBOAIIALgcIhPgHgAAlAjIAbAEIABAAIAahIIAAAAIgxgGIgSAxIAHAAQACgHAHgDQAIgEAHAAQAHAAAEAGQAEAFgDAHQgCAGgHAFQgIAEgHAAIgEgBgAhWAXIAcADIADgHIgEAAQgHgBgEgFQgDgGACgHQADgFAGgFQAIgEAHABQAIAAAEAGQADAFgCAGIAGAAIASgvQgZgCgYgEgAArADQgFADgCAEIABABQAIABgCAHIgBADIADABQAFAAAFgCQAEgDACgFQACgFgDgEQgCgDgFgBQgFAAgFADgAg7gGQgEADgCAEQgCAFADADQACAEAFABIAEgBIABgDQABgDADgDIAHAAIABgBQACgDgDgEQgDgDgEgCQgFAAgGADgAglAjQgFAAgCgDQgCgDABgEQACgDAEgDQAEgCAEAAQAEAAACADQACACgBAEQgCAEgEADQgDACgCAAIgCAAgAgnAZQgBAFAEAAQAEAAACgEQABgEgEABIgBgBQgEAAgBADgAgMAfQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAAAABAAQAAgBABAAIAZADQABAAABAAQAAAAAAAAQABABAAAAQAAABgBABQAAAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAgAA5gKQgEgBgCgCQgCgEABgEQAEgKAKABQAEABADADQACAEgCADQgDAJgJAAIgCAAgAA4gUQgBAEAEABQAFAAABgEQACgEgFgBQgFAAgBAEgAgtgUQgEAAgCgDQgCgEABgEQACgDAEgDQAEgCAFAAQAEABACACQACAEgBADQgBAFgFACQgDADgEAAIgCgBgAgtgeQgCAEAEABQAFAAACgEQABgFgEABIgBgBQgEAAgBAEg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_4, new cjs.Rectangle(-39.7,-28.9,79.5,57.8), null);


(lib.dollar_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ADoEZQg2g5hXgdQgzgRhvgQQhwgQg0gRQhXgcg1g6QgMgNgLgPICRlCQAKAOAMANQA2A6BXAcQA0ARBvARQBvAQA0AQQBXAdA2A6QAMANAKAOIiQFCQgNgQgKgLgAlqAOQA1A5BXAdQA0ARBwAPQBvARAzARQBXAcA2A5IB3kIQg2g5hXgcQg0gRhvgQQhvgRg0gRQhXgcg2g5gADdDEQgXgQADgZIgGgEIAEgLIAFADIAAgCIACgCIgFgCIAFgLIAGAEQAIgIAKgBQALgBAJAIQAJAGAFAKIgMAIQgCgFgGgFQgJgGgHAEQAHAEAKAIIgFAKIgTgOIgCAFIASANIgFAJIgOgJQABAJAIAHQAHAEAEAAIAAASIgCAAQgGAAgJgIgAguBpQgxgGgUgmQgUgmATgrQAUgqAvgYQAwgYAwAGQAxAHAVAmQAUAlgUArQgTAqgwAYQglAUglAAQgLAAgLgCgAgShGIg8CEQgDAKAKABIBJAMQALABAEgJIA7iFQACgEgCgDQgCgDgEgBIhJgKIgDgBQgIAAgEAIgAhEBCQgBAAAAgBQAAAAgBAAQAAAAAAgBQAAAAABAAIA8iFQAAAAAAgBQAAAAAAAAQABAAAAAAQABAAAAAAIBKALQAAAAABAAQAAAAAAAAQABABAAAAQgBABAAAAIg8CFQAAAAAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAgAghA6QgCADACADQABACADABQAJAAACgGQAEgHgIgCIgCAAQgHAAgCAGgAAEg3IAoAGIADgHIgogFgAjlhnQgLgHgFgMQgFgMACgMIgGgEIAFgKIAEADIAAgCIACgDIgEgDIAEgKIAGAEQAQgPAWAOQAKAGAEAIIgMAKQgDgFgGgEQgJgGgGAFIARAKIgGAKIgSgMIgCACIAAACIASAMIgGAKIgOgJQABAJAJAGQAFADAGAAIAAAQIgDABQgGAAgJgFg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("ABoCmQgzgRhwgRQhvgPg0gRQhXgdg1g5IB2kIQA2A5BWAcQA0ARBwARQBvAQA0ARQBXAcA1A5Ih3EIQg1g5hXgcgADDCXIAFAEQgCAZAXAQQAKAJAGgBIABgSQgEAAgHgEQgJgHgBgJIAPAJIAFgJIgSgNIACgFIATAOIAEgKQgKgIgGgEQAHgEAIAGQAHAFABAFIAMgIQgEgKgJgGQgKgIgKABQgKABgIAIIgGgEIgFALIAFACIgCACIAAACIgFgDgAgxhWQgwAYgTAqQgTArATAmQAVAmAxAGQAxAHAvgZQAwgYASgqQAUgrgUglQgUgmgxgHIgVgBQgmAAglATgAj+iWIAFAEQgBAMAFAMQAFAMALAHQALAHAHgDIAAgQQgGAAgGgDQgIgGgBgJIAOAJIAFgKIgSgMIABgCIACgCIASAMIAFgKIgRgKQAHgFAJAGQAGAEACAFIANgKQgEgIgKgGQgXgOgPAPIgGgEIgFAKIAFADIgCADIAAACIgFgDgAACBVIhKgMQgJgBADgKIA8iEQAFgJAKACIBJAKQAEABACADQACADgCAEIg7CFQgEAIgJAAIgCAAgAgJhFIg9CFQAAAAAAAAQAAABAAAAQAAAAABAAQAAABAAAAIBLALQAAAAAAAAQABAAAAAAQAAgBABAAQAAAAAAAAIA8iFQAAAAAAgBQAAAAAAgBQAAAAAAAAQgBAAgBAAIhJgLQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAABAAAAgAgdBDQgDgBgCgCQgBgDABgDQADgHAIABQAJACgEAHQgCAGgHAAIgCAAgAADg3IAEgGIAoAFIgDAHg");
	this.shape_1.setTransform(0.05,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_3, new cjs.Rectangle(-39.9,-30.8,79.9,61.6), null);


(lib.dollar_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AmAhpIAdgSQBDgkBbgJQA2gHBvACQBuACA2gGQBagKBDgkIAegRIBCFaIgdASQhDAkhaAJQg2AHhvgCQhvgBg2AFQhaAKhDAkQgNAHgQAKgAi/iJQhbAKhDAjIA3EdQBDgkBagKQA2gGBvABQBvACA2gGQBagKBDgjIg3kdQhDAkhaAKQg2AGhugBIgqgBQhQAAgrAFgAAWBxQgwgBgpghQgogggJguQgJguAcgiQAcgiAwABQAwABApAhQAoAgAJAuQAJAvgcAhQgbAhguAAIgDAAgAhEhDQgFAGACAKIAVBsQABAIAIAHQAIAGAJAAIBQABQAJABAFgHQAGgGgCgJIgVhsQgCgKgHgGQgIgHgKAAIhPgBQgJABgGAGgAEYBXQgJgEgGgJIgHACIgCgLIAFgBIgBgGIgFACIgCgLIAHgCQABgLAIgJQAHgJAMgFQAMgEAHACIgEARQgFgCgHADQgKADgCAJIATgHIABAMIgVAHIABAFIAUgHIABALIgQAGQAHAGAJgEQAFgCAGgGIAJAMQgEAIgNAEQgHADgGAAQgFAAgFgCgAgZBGQgGAAgFgDQgFgFgBgFIgThhIBsACIATBgQABAFgDAFQgEADgGAAgAgigRQgLANADARQAEASAQANQAPAMASABQATAAALgNQALgMgEgSQgDgSgQgMQgPgNgSgBIgCAAQgSAAgKANgAAKAxQgPAAgNgKQgNgLgCgPQgDgOAJgLQAJgKAPAAQAPABAMAKQANALADAOQADAPgJAKQgIAKgPAAIgBAAgAkpgZIgHACIgCgLIAFgCIgBgEIgFABIgCgKIAHgDQABgMAIgKQAHgKAMgDQALgDAIADIgEAPQgHgBgFACQgJACgEAKIATgGIACALIgWAHIABAFIAVgHIABALIgQAFQAGAGAKgDQAIgCADgFIAJANQgEAHgNAEQgHACgGAAQgRAAgIgOgAg+gwIgBgDQgBgHADgDQAEgFAGAAIBPABQAGAAAFAFQAFAEABAGIABAEIhsgCgAgRg4QACAHAHAAQAIAAgCgHQgCgIgHAAQgHAAABAIgAgug9QgCACABACQABAIAIAAQADAAACgCQABgCAAgDQgBgIgIAAIgBAAQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAABg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AldhbQBDgkBbgKQA2gGBuACQBuABA2gGQBbgKBDgkIA3EdQhDAjhbAKQg2AHhvgCQhugCg2AGQhaAKhEAkgAhihPQgbAiAJAuQAIAuApAhQApAgAvABQAxABAcghQAcgigJgvQgJgugoggQgpghgwgBIgDAAQguAAgcAhgAEYBYQAKADANgEQAMgFAFgIIgJgLQgGAFgFACQgJAEgHgGIAQgGIgBgLIgVAHIAAgEIAUgIIgBgLIgTAHQADgJAKgEQAGgCAGABIADgRQgHgCgLAEQgMAFgHAJQgIAJgBALIgHACIACALIAFgBIABAFIgFABIACALIAGgCQAHAKAJAEgAkDgNQANgEADgHIgJgMQgCAEgIACQgKADgHgGIARgFIgBgLIgVAHIgBgFIAWgHIgCgLIgTAGQAEgKAJgCQAFgCAGACIAFgQQgJgDgLADQgLAEgIAKQgHAJgCAMIgGADIACALIAEgCIABAEIgEACIACALIAHgCQAMATAagHgAgYBPQgKAAgHgHQgIgGgCgJIgVhsQgCgKAGgGQAFgGAKAAIBPABQAJAAAJAGQAHAHACAJIAVBsQACAJgGAGQgFAHgKAAgAgrA5QACAFAFAFQAFAEAFAAIBQABQAGAAADgEQADgEgBgGIgShgIhsgBgAg9g9QgEAEACAGIAAAEIBsABIAAgEQgBgGgGgEQgEgEgHAAIhPgBQgFAAgEAEgAALA5QgSAAgQgNQgPgNgEgSQgDgRAKgNQAMgNASABQATAAAPANQAQANADARQAEASgLANQgLAMgSAAIgBAAgAgbgLQgKAKADAOQADAPANALQANAKAOAAQAQABAJgLQAIgKgCgPQgDgOgNgKQgNgLgOAAIgBAAQgPAAgIAKgAgSg4QAAgHAHAAQAHAAACAHQACAIgJAAQgHAAgCgIgAgvg4QgBgDACgCQABgDADABQAJAAAAAHQABADgCACQgCACgCAAQgJAAAAgHg");
	this.shape_1.setTransform(0.05,-0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_2, new cjs.Rectangle(-38.5,-24.1,77.1,48.3), null);


(lib.dollar_1_v2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjYDoIgdgEIhUkcIAcAFQBCAFBGgdQArgRBRgxQBPgxAqgSQBHgdBBAGQAQABAOADIBUEcIgdgFQhBgGhHAeQgqARhRAxQhPAygrARQg6AYg3AAIgXgBgAjfDPQBBAFBHgdQAqgRBQgxQBRgxAqgSQBGgdBBAGIhFjoQhBgHhHAeQgqARhPAyQhRAxgrARQhGAdhBgGgAgoBmQghgNgMgnQgLgmARgoQARgpAjgWQAjgXAhAOQAhANAMAnQALAlgRAoQgRApgjAXQgXANgUAAQgNAAgMgEgAgVBPQgDADACACQAAABABAAQAAAAABAAQAAAAABAAQAAAAABAAIAggoIArgGQAAAAAAAAQAAAAAAAAQAAAAAAAAQABgBAAAAQAAAAABgBQAAAAAAAAQABgBAAAAQAAgBgBAAQACgEgEAAIgfAFIAugcQADgBACgEQACgDgCgDIgWhMQgBgEgDgBQgDgBgEACIg5AjQgmAYgVAKIgEAGQgCAEABADIAWBLQABADAEACQAEABACgCIAugbgAgwBGIgWhKQgBgBAAAAQAAAAABgBQAAAAAAgBQAAAAABAAQATgLAngYIA6gjQAAAAAAAAQABAAAAAAQAAAAAAAAQABABAAAAIAWBMQABAAAAAAQAAAAgBABQAAAAAAAAQAAABgBAAIg6AjQgmAYgUAKIgBABIgBgCg");
	this.shape.setTransform(0,-0.0243);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AjfDPIhFjpQBBAGBGgdQArgRBRgxQBPgyAqgRQBHgeBBAHIBFDoQhAgGhHAdQgqAShRAxQhQAxgqARQg8AZg3AAIgVgBgAgahbQgkAWgRApQgRAoALAmQAMAnAhANQAiAMAigVQAjgXARgpQARgogLglQgLgngigNQgMgFgNAAQgUAAgWAOgAgWBUQgCgCADgDIAXgcIguAbQgCACgEgBQgDgCgBgDIgXhLQgBgDACgEIAFgGQAUgKAngYIA5gjQADgCADABQADABABAEIAXBMQABADgCADQgBAEgEABIguAcIAfgFQAEAAgBAEQAAAAAAABQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAABQAAAAAAAAQgBAAAAAAQAAAAAAAAIgqAGIghAoIgBAAIgDgBgAAvhNIg6AjQgmAYgUALQgBAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABIAWBKQABABAAAAQAAABAAAAQABAAAAAAQAAAAAAgBQAUgKAmgYIA6gjQABAAAAgBQAAAAABAAQAAgBAAAAQAAAAAAAAIgXhMIgBgBIgBAAg");
	this.shape_1.setTransform(-0.025,-0.0148);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1_v2, new cjs.Rectangle(-33,-23.3,66.1,46.6), null);


(lib.dollar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjzEuQgTgIgVgNIhwlQQAVANAUAJQBZAkBPguQAwgdBThfQBShfAwgdQBPgwBZAmQATAIAWANIBwFQQgVgOgUgIQhZglhQAvQgwAdhSBfQhSBfgwAdQgtAbgvAAQglAAgogRgAj9EQQBZAmBQgwQAwgdBShfQBShfAwgdQBQgvBZAlIhckTQhZgmhPAwQgwAdhSBfQhTBfgwAdQhPAvhZglgAgkCSQgmgFgPgvQgPgsAQg5QAQg6AlgrQAjgrAlAGQAlAFAQAuQAOAtgQA5QgQA5gkAsQgfAmghAAIgIgBgAAqh6QgVAUgnAuQgoAvgVAUQgIAHADALIAeBZQABAEAEAAQADABAEgDQAQgQAfgkIgXAtQgDAFADADQABAAAAAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAgBQAAAAABAAQAAAAAAAAQAAgBAAAAIAfg/IAtgYIABgBQADgCAAgEQAAgFgEACIghASQAYgcAYgYQADgDACgGQABgFgBgEIgehaQgBgEgDgBIgBAAQgDAAgDAEgAj9BtQgMgIgIgLIgIgCIgDgKIAGABIgBgCIgBgCIgGgCIgDgLIAIADQACgTAbgBQANAAAIADIgDAOQgFgCgHAAQgMABgCAGQALABAKgBIADAKQgMABgMgBIACAFIAXABIACAKIgSAAQAJAIAKgBQAGAAAFgEIALANQgEAHgNAAIgBAAQgNAAgMgHgAgvBwIgehaQAAgBgBAAQAAgBABAAQAAgBAAAAQAAgBABAAQAVgUAoguQAnguAUgVQABAAAAgBQAAAAABAAQAAAAAAABQABAAAAABIAeBZQAAABAAAAQAAABgBAAQAAABAAAAQAAABgBAAQgTAUgpAuQgmAtgWAWIgCABIAAgBgAEdgiQgfgFgPgUIgHABIgDgLIAFAAIgBgDIgBgCIgFAAIgDgKIAHgBQAAgKAIgHQAIgGANACQAMABAKAIIgDAMQgGgEgIgCQgLgBgDAHIAWACIACAKIgYgCIACAFIAXACIADAKIgTgBQAIAHALACQAHAAAGgBIAMAPQgDACgHAAIgJAAg");
	this.shape.setTransform(-0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("Aj8EQIhckTQBZAlBPgvQAxgdBThfQBRhfAwgdQBQgwBYAmIBcETQhZglhPAvQgwAdhTBfQhSBfgwAdQgsAbgwAAQglAAgngRgAgjhsQgkArgRA6QgQA5APAsQAQAvAlAFQAlAGAjgrQAlgsAQg5QAQg5gPgtQgPgugmgFIgIgBQghAAgfAmgAkYBYIAIACQAHALANAIQANAHANAAQAMAAAFgHIgMgNQgFAEgGAAQgKABgJgIIASAAIgCgKIgXgBIgCgFQAMABAMgBIgDgKQgKABgLgBQACgGAMgBQAIAAAFACIACgOQgHgDgNAAQgcABgBATIgIgDIACALIAHACIAAACIABACIgGgBgAEdgiQAPABAEgDIgMgPQgGABgHAAQgLgCgHgHIASABIgCgKIgYgCIgBgFIAYACIgDgKIgVgCQACgHALABQAIACAGAEIADgMQgKgIgMgBQgNgCgIAGQgIAHAAAKIgHABIADAKIAGAAIAAACIABADIgFAAIAEALIAHgBQAOAUAfAFgAgxB6QgDAAgBgEIgehZQgDgLAHgHQAWgUAogvQAnguAUgUQADgEAEAAQADABACAEIAdBaQACAEgCAFQgBAGgEADQgYAYgYAcIAhgSQAEgCAAAFQAAAEgDACIgBABIgtAYIgfA/QAAAAAAABQAAAAAAAAQAAAAgBAAQAAABAAAAQAAAAgBABQAAAAgBAAQAAAAgBAAQAAAAAAAAQgDgDADgFIAWgtQgfAkgQAQQgDADgDAAIgBgBgAAthzQgVAVgnAuQgoAugVAUQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABIAeBaQAAAAAAABQAAAAABAAQAAAAAAAAQABAAABgBQAVgWAmgtQApguATgUQABAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBIgehZIgBgCIgBABg");
	this.shape_1.setTransform(-0.05,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1, new cjs.Rectangle(-39.6,-31.9,79.2,63.8), null);


(lib.cta_rollover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E39126").s().p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	this.shape.setTransform(10,12);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta_rollover, new cjs.Rectangle(0,0,20,24), null);


(lib.cta_arrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag/AKIAAgTIB/AAIAAATg");
	this.shape.setTransform(1.9821,5.4827,0.3891,0.5,0,-50.0001,130.0012);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag/AKIAAgTIB/AAIAAATg");
	this.shape_1.setTransform(1.9679,2.2327,0.3891,0.5,50.0001);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta_arrow, new cjs.Rectangle(0,0,4,7.8), null);


(lib.cover1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AygE2IAAprMAlBAAAIAAJrg");
	this.shape.setTransform(-0.7,25.3,1.2658,2.2544,-4.9997,0,0,-119,-31.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cover1, new cjs.Rectangle(0,0,311.1,165.4), null);


(lib.counterMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.counter_days = new cjs.Text("", "11px 'Kokonor'", "#29A945");
	this.counter_days.name = "counter_days";
	this.counter_days.lineHeight = 20;
	this.counter_days.lineWidth = 169;
	this.counter_days.parent = this;
	this.counter_days.setTransform(2,2);

	this.counter_txt = new cjs.Text("", "11px 'Kokonor'", "#29A945");
	this.counter_txt.name = "counter_txt";
	this.counter_txt.lineHeight = 20;
	this.counter_txt.lineWidth = 177;
	this.counter_txt.parent = this;
	this.counter_txt.setTransform(2,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.counter_txt},{t:this.counter_days}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.counterMC, new cjs.Rectangle(0,0,181,35.9), null);


(lib.arrowLeftRight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// hitArea
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,0,0,0.008)").s().p("AiuC5IAAlxIFdAAIAAFxg");
	this.shape.setTransform(3.9785,-0.0027,0.5714,0.5405);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	// arrow
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#969696").s().p("AguAiIBEhQIAJALIANgLIACACIhMBbg");
	this.shape_1.setTransform(4.1,3.2,0.8655,0.8671,0,0,0,0.1,-0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#969696").s().p("AgugnIAQgMIBMBbIgPAMg");
	this.shape_2.setTransform(4.0246,-3.4735,0.8655,0.8671);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AguAiIBEhQIAJALIANgLIACACIhMBbg");
	this.shape_3.setTransform(4.1,3.4,0.8656,0.8672,0,0,0,0.1,-0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgugnIAQgMIBMBbIgPAMg");
	this.shape_4.setTransform(4.0249,-3.4736,0.8656,0.8672);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).to({state:[{t:this.shape_4},{t:this.shape_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-10,20,20);


(lib.subsubtext = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer_1
	this.m00 = new lib.Symbol1();
	this.m00.name = "m00";
	this.m00.parent = this;

	this.m10 = new lib.Symbol2();
	this.m10.name = "m10";
	this.m10.parent = this;

	this.m20 = new lib.Symbol3();
	this.m20.name = "m20";
	this.m20.parent = this;

	this.m30 = new lib.Symbol4();
	this.m30.name = "m30";
	this.m30.parent = this;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.m00}]}).to({state:[{t:this.m10}]},1).to({state:[{t:this.m20}]},1).to({state:[{t:this.m30}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,160.7,39);


(lib.price_old_line_container = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.line = new lib.price_old_line();
	this.line.name = "line";
	this.line.parent = this;
	this.line.setTransform(0,24);

	this.timeline.addTween(cjs.Tween.get(this.line).wait(1));

}).prototype = getMCSymbolPrototype(lib.price_old_line_container, new cjs.Rectangle(0,0,92,26), null);


(lib.loopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(320,42);

	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,42);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},299).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,x:0},299).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-320,0,960,84);


(lib.endframe_static = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AyWGuIAAjIMAktAAAIAADIg");
	mask.setTransform(117.5001,42.9993);

	// line4
	this.l3 = new lib.endframe_static_line4();
	this.l3.name = "l3";
	this.l3.parent = this;
	this.l3.setTransform(0,66);

	var maskedShapeInstanceList = [this.l3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.l3).wait(1));

	// mask3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AzhDSIAAiMMAnDAAAIAACMg");
	mask_1.setTransform(124.9964,20.9993);

	// line3
	this.l2 = new lib.endframe_static_line3();
	this.l2.name = "l2";
	this.l2.parent = this;
	this.l2.setTransform(0,28);

	var maskedShapeInstanceList = [this.l2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.l2).wait(1));

	// mask2 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("AzhCMIAAiVMAnDAAAIAACVg");
	mask_2.setTransform(124.9964,13.9991);

	// line2
	this.l1 = new lib.endframe_static_line2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(0,14);

	var maskedShapeInstanceList = [this.l1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.l1).wait(1));

	// mask1 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("AzhBGIAAiLMAnDAAAIAACLg");
	mask_3.setTransform(124.9964,6.9993);

	// line1
	this.l0 = new lib.endframe_static_line1();
	this.l0.name = "l0";
	this.l0.parent = this;

	var maskedShapeInstanceList = [this.l0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.l0).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_static, new cjs.Rectangle(0,0,250,42), null);


(lib.dollar_cashback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_2
	this.text = new lib.dollar_cashback_text();
	this.text.name = "text";
	this.text.parent = this;
	this.text.setTransform(0,1.45,1,1,9.9999);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AkTETQhyhyAAihQAAihByhyQBzhyCgAAQCiAABxByQBzByAAChQAAChhzByQhxBziiAAQigAAhzhzg");
	this.shape.setTransform(-0.0216,-0.0194,0.8974,0.8974);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_cashback, new cjs.Rectangle(-35,-35,70,70), null);


(lib.cta = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// text
	this.dynText = new cjs.Text("", "11px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 9;
	this.dynText.lineWidth = 226;
	this.dynText.parent = this;
	this.dynText.setTransform(-248,5);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

	// arrowsMask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	mask.setTransform(-10,12);

	// arrows
	this.arrow2 = new lib.cta_arrow();
	this.arrow2.name = "arrow2";
	this.arrow2.parent = this;
	this.arrow2.setTransform(-13,8);

	this.arrow1 = new lib.cta_arrow();
	this.arrow1.name = "arrow1";
	this.arrow1.parent = this;
	this.arrow1.setTransform(-10,8);

	var maskedShapeInstanceList = [this.arrow2,this.arrow1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.arrow1},{t:this.arrow2}]}).wait(1));

	// rollover
	this.rollover1MC = new lib.emptyMovieClip();
	this.rollover1MC.name = "rollover1MC";
	this.rollover1MC.parent = this;
	this.rollover1MC.setTransform(-250,0);
	this.rollover1MC.alpha = 0;

	this.rollover2MC = new lib.cta_rollover();
	this.rollover2MC.name = "rollover2MC";
	this.rollover2MC.parent = this;
	this.rollover2MC.setTransform(-10,12,1,1,0,0,0,10,12);
	this.rollover2MC.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.rollover2MC},{t:this.rollover1MC}]}).wait(1));

	// bgr
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FAA332").s().p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	this.shape.setTransform(-10,12);

	this.shapeMC = new lib.emptyMovieClip();
	this.shapeMC.name = "shapeMC";
	this.shapeMC.parent = this;
	this.shapeMC.setTransform(-250,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shapeMC},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta, new cjs.Rectangle(-250,0,250,25.3), null);


// stage content:
(lib.index = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var self = this;
		
		
		var countDownDate = new Date("August 11, 2019 23:59:59").getTime();
		
		var x = setInterval(function() {
		  var now = new Date().getTime();
		
		  var distance = countDownDate - now;
		  function padder(n) {
		         return (n < 10 ? "0" + n : n);
		       }
		  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		
		  self.counterMC.counter_days.text = "nog " + padder(days) + " dagen"
		  self.counterMC.counter_txt.text = "en " + padder(hours) + ":"
		  + padder(minutes) + ":" + padder(seconds);
		  if (distance < 0) {
		    clearInterval(x);
		    self.counterMC.counter_days.text = "alleen vandaag nog";
			  self.counterMC.counter_txt.text = "";
		  }
		}, 1000);
		//stage.enableMouseOver(6);
		
		var mmMC = this.mediamarkt;
		var mm2MC = this.mediamarkt2;
		var greyMC = this.grey;
		
		var logoMC = this.logo;
		var productMC = this.product;
		var priceMC = this.price;
		var labelMC = this.label;
		var leftMC = this.left;
		var rightMC = this.right;
		var ctaMC = this.cta;
		var coverMC = this.cover;
		var hitAreaMC = this.hitAreaMC;
		var leftButtonDiv = document.getElementById("arrow_left");
		var rightButtonDiv = document.getElementById("arrow_right");
		var ctaButtonDiv = document.getElementById("cta");
		var lineMC = this.lineContainer;
		var campaignMC = this.campaign;
		var whiteMC = this.white;
		var servButtonMC = this.servButton;
		var vanafMC = this.vanaf;
		var endMC = this.endframe;
		
		var text1MC = this.text1;
		var text2MC = this.text2;
		var text3MC = this.text3;
		
		var numberOfProducts = productID = numberOfServices = prevProductID = 0;
		dynamic.productID = productID;
		var productsObject = new Array();
		
		leftMC.mouseChildren = rightMC.mouseChildren = false;
		
		var bigJump = false;
		
		var productImgScale = 0.155; //1,552795031055901 //0.163 //0.18
		if (typeof VARIATION == "undefined") {
			VARIATION = "";
		}
		if (VARIATION == "dollar") {
			//productImgScale *= 1.25;
			//leftMC.y -= 25;
			//rightMC.y -= 25;
		}
		var numberImgScale = 0.275;
		var logoScale = 0.5;
		var campaignScale = 0.2;
		
		var autoRotateTime = 3.0;
		var ctaHover = false;
		
		var dollarCashbackToX = 205;
		var dollarCashbackToY = 20;
		var prijsToX = 108;
		var logoToY = -24;
		var productToX = productMC.x;
		var labelToX = labelMC.x;
		var priceToX = priceMC.x;
		var text1ToY = text1MC.y;
		var text2ToY = text2MC.y;
		var text3ToX = text3MC.x;
		var leftToX = leftMC.x;
		var rightToX = rightMC.x;
		var ctaToX = dynamic.width - 5;
		var mm2ToX = mm2MC.x;
		var lineToX = 56;
		var servButtonToX = servButtonMC.y;
		var vanafToX = 17;
		var lines = 3;
		self.counterMC.x = 400;
		for (var i = 0; i<lines; i++) {
			var lMC = endMC["l" + i];
			lMC.toY = lMC.y;
			lMC.y = lMC.toY + 14;
			lMC.rotation = 7;
		}
		var endToX = 13;
		var maniaLabelToX = 5;
		
		var isLeftClick = false;
		
		productMC.x = dynamic.width + 150;
		//priceMC.x = - dynamic.width / 2;
		text1MC.y = text1ToY + 25;
		text2MC.y = text2ToY + 25;
		leftMC.x = leftToX + 25;
		rightMC.x = rightToX - 25;
		ctaMC.x = ctaToX - 70;
		ctaMC.alpha = 0.0;
		mm2MC.y = 228;
		if (VARIATION == "dollar") {
			mm2MC.y = 92;
			priceMC.y -= 5;
			lineMC.y -= 5;
		}
		if (VARIATION == "merkenweek") {
			mm2MC.y = 92;
			//priceMC.y -= 5;
			lineMC.y -= 5;
		}
		mm2MC.alpha = 0;
		mm2MC.x = mm2ToX + 50;
		lineMC.x = lineToX;
		lineMC.line.rotation = -14;
		lineMC.line.scaleX = 0.0;
		vanafMC.x = vanafToX + 20;
		endMC.x = endToX;
		
		// DCO 2.0 (!)
		this.getShotDuration = function (shot) {
			
			var duration = 3.0;
		
			return duration;
			
		}
		
		this.getShotData = function (i) {
		
			// IF NOT EMPTY SHOT
			if (dynamic.getDynamic("shot_type", i) == "") {
				return undefined;
			}
			
			return {
				
				"products": dynamic.getDynamic("shot_type", i).split("|"),
				"main_specs": dynamic.getDynamic("copy", i).split("|"),
				"sub_specs": dynamic.getDynamic("price_type", i).split("|"),
				//"label_img": dynamic.getDynamic("price", i).split("|"),
				"price": dynamic.getDynamic("price", i).split("|"),
				"cta": dynamic.getDynamic("cta_copy", i),
				"colorPanel": dynamic.getDynamic("font_scale", i).split("|")[0]
				//"categoryType": dynamic.getDynamic("font_scale", i).split("|")[1]
				//"logo": dynamicContent.dcodata[0].Logo1.split("|")
		
			}
		
		}
		
		this.playShot = function (shot) {
		
			if (shot.index == 1) {
				
				numberOfProducts = shot.products.length;
				
				var h = 0;
				
				for (var i = 0; i<numberOfProducts; i++) {
					
					if (shot.products[i] != "" && shot.products[i] != "-") {
						
						var productName = shot.products[i];
						var checkProductName = productName.toLowerCase();
						var isService = false;
						if (shot.products[i][0] == "^") {
							isService = true;
							productName = productName.substring(1, productName.length);
							numberOfServices++;
						}
						
						var productObject = {
							name: productName,
							main_specs: shot.main_specs[i],
							sub_specs: shot.sub_specs[i],
							label: !dynamic.isImageEmpty(LABEL + i),
							price: shot.price[i].split("^")[0],
							priceOld: shot.price[i].split("^")[1],
							service: isService
						}
						
						var imgMC = new createjs.MovieClip();
						productMC.addChild(imgMC);
						if (!(checkProductName == "tv" || checkProductName == "audio")) {
							dynamic.setDynamicImage(imgMC, (PRODUCT + i));
						}
						var toScale = productImgScale;
						var toX = 0;
						var toY = 0;
						
						if (VARIATION == "merkenweek" && i == 0) {
							toScale *= 1.75;
							toY += 10;
							toX -= 40;
						}
						if (VARIATION == "merkenweek" && i == 1) {
							toScale *= 1.5;
							toY += 10;
							toX -= 42;
						}
						if (VARIATION == "merkenweek" && i == 2) {
							toScale *= 1.85;
							toY += 15;
							toX -= 40;
						}
						if (VARIATION == "merkenweek" && i == 3) {
							toScale *= 1.55;
							toY += 10;
							toX -= 40;
						}
						imgMC.scaleX = imgMC.scaleY = toScale;
						imgMC.x = h * 300;
						imgMC.y += toY;
						h++;
						
						var logoImgMC = new createjs.MovieClip();
						logoMC.addChild(logoImgMC);
						logoImgMC.alpha = 0;
						//dynamic.setDynamicImage(logoImgMC, (LOGO + i), 0, 0);
						logoImgMC.scaleX = logoImgMC.scaleY = logoScale;
						
						var labelImgMC = new createjs.MovieClip();
						labelMC.addChild(labelImgMC);
						if (!dynamic.isImageEmpty(LABEL + i)) {
							//dynamic.setDynamicImage(labelImgMC, (LABEL + i));
						}
						
						if (checkProductName == "tv" || checkProductName == "audio") {
							productObject = checkProductName;
							shot.main_specs[i] = shot.sub_specs[i] = shot.price[i] = "";
							if (checkProductName == "audio") {
								endMC.l2.gotoAndStop(1);
							}
						}
						
						productsObject.push(productObject);
						
					}
				}
				
				numberOfProducts = productsObject.length;
				
				if (shot.colorPanel == "red") {
					greyMC.gotoAndStop(1);
					leftMC.gotoAndStop(1);
					rightMC.gotoAndStop(1);
					lineMC.line.gotoAndStop(1);
					whiteMC.visible = false;
					text1MC.dynText.color = text2MC.dynText.color = vanafMC.dynText.color = "#ffffff";
				}
				
				if (!dynamic.isImageEmpty(IMAGE_COUNT + 1)) {
					dynamic.setDynamicImage(campaignMC, (IMAGE_COUNT + 1));
					campaignMC.scaleX = campaignMC.scaleY = 0;
				}
				
				if (numberOfServices > 0) {
					buildServicesButton("BEKIJK ONZE SERVICES");
					//rightToX = servButtonToX - 19;
					//rightMC.x = rightToX - 25;
					var servButtonWidth = 110;
					servButtonMC.x = dynamic.width - servButtonWidth - 5;
					if (greyMC.currentFrame == 0) {
						//servButtonWidth -= 50;
					}
					//rightButtonDiv.style.right = servButtonWidth + "px";
					//productToX -= Math.round((dynamic.width - servButtonToX) / 3);
					var div = document.createElement("div");
					div.style.width = servButtonWidth + "px";
					div.style.height = "23px";
					div.style.right = "5px";
					div.style.top = "0px";
					div.id = "services";
					document.getElementById("dom_overlay_container").appendChild(div);
					document.getElementById("services").addEventListener("click", doServices, false);
					if (greyMC.currentFrame == 0) {
						//div.style.top = "10px";
					}
				}
				
				//buildCta(shot.cta);
				updateLogoTextsLabelPrice();
				animateInCreative();
				
				ctaButtonDiv.addEventListener("mouseover", doRollOver, false);
				ctaButtonDiv.addEventListener("mouseout", doRollOut, false);
				ctaButtonDiv.addEventListener("click", dynamic.onMouseClick.bind(dynamic));
				
				if (productsObject[productID] != "video" && VARIATION != "dollar") {
					
					//checkNumberOfProducts();
					
				}
				
			}
			
			//if (shot.isLastShot != true) {
		
		}
		
		function checkNumberOfProducts() {
						
			if (numberOfProducts > 1) {
				
				leftMC.visible = rightMC.visible = true;
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
				leftButtonDiv.addEventListener("click", doLeftRightClick, false);
				rightButtonDiv.addEventListener("click", doLeftRightClick, false);
				
				TweenMax.delayedCall(autoRotateTime + 1, doAutoRotate);
				
				enableSwipe();
				
			}
			
		}
		
		function enableSwipe() {
			swipedetect(document.getElementById("swipe_area"), function (swipedir) {
				self.cancelAutoRotate();
				var maxID = numberOfProducts - numberOfServices - 1;
				var minID = 0;
				if (productsObject[productID].service) {
					maxID = numberOfProducts - 1;
					minID = numberOfProducts - numberOfServices;
				} 
				if (swipedir == "right") {
					if (productID > minID) {
						productID--;
						bigJump = false;
					} else {
						bigJump = true;
						productID = maxID;
					}
					isLeftClick = true;
					changeProduct();
				}
				if (swipedir == "left") {
					if (productID < maxID) {
						productID++;
						bigJump = false;
					} else {
						bigJump = true;
						productID = minID;
					}
					isLeftClick = false;
					changeProduct();
				}
				if (swipedir == "click") {
					dynamic.onMouseClick();
				}
			});
		}
		
		function buildCta(ctaText) {
			
			var rightMargin = 20;
			var buttonHeight = 24;
			var textPadding = 10;
			
			//var ctaText = "BEKJIK DEAL";
			ctaMC.dynText.text = ctaText;
			var ctaRect = new createjs.Shape();
			var ctaTextWidth = Math.round(ctaMC.dynText.getMeasuredWidth()) + textPadding * 2;
			ctaMC.dynText.x = - rightMargin - ctaTextWidth + textPadding - 1;
			ctaMC.shapeMC.x = ctaMC.rollover1MC.x = - rightMargin - ctaTextWidth - 1;
			ctaRect.graphics.beginFill("#FAA332").drawRoundRectComplex(0, 0, ctaTextWidth, buttonHeight, 3, 0, 0, 3);
			ctaMC.shapeMC.addChild(ctaRect);
			
			var rolloverRect = new createjs.Shape();
			rolloverRect.graphics.beginFill("#E39126").drawRoundRectComplex(0, 0, ctaTextWidth, buttonHeight, 3, 0, 0, 3);
			ctaMC.rollover1MC.addChild(rolloverRect);
			
			ctaButtonDiv.style.width = (ctaTextWidth + 1 + rightMargin) + "px";
			
		}
		
		function doRollOver(event) {
			
			TweenMax.to([ctaMC.rollover1MC, ctaMC.rollover2MC], 0.4, {alpha: 1.0});
			ctaHover = true;
			ctaTL.play();
			
		}
		
		function doRollOut(event) {
			
			TweenMax.to([ctaMC.rollover1MC, ctaMC.rollover2MC], 0.4, {alpha: 0.0});
			ctaHover = false;
			
		}
		
		var ctaTL = new TimelineMax({paused:true, onComplete: function(){
			if(ctaHover) ctaTL.restart();
		}});
		
		ctaTL.fromTo(ctaMC.arrow1, 0.3, {x: -10}, {x: 4, ease: Expo.easeIn}, 0.0)
			 .fromTo(ctaMC.arrow2, 0.3, {x: -13}, {x: 1, ease: Expo.easeIn}, 0.05)
			 .set(ctaMC.arrow1, {x:-25})
			 .set(ctaMC.arrow2, {x:-28})
			 .to(ctaMC.arrow1, 0.5, {x: -10, ease: Expo.easeOut}, 0.35)
			 .to(ctaMC.arrow2, 0.5, {x: -13, ease: Expo.easeOut}, 0.45);
		
		function doAutoRotate() {
			productID++;
			bigJump = false;
			changeProduct();
			if (productID < numberOfProducts - 1) {
				TweenMax.delayedCall(autoRotateTime, doAutoRotate);
			}
		}
		self.cancelAutoRotate = function() {
			TweenMax.killDelayedCallsTo(doAutoRotate);
		}
		
		function animateInCreative() {
			
			var d = 0.1;
			
			if (VARIATION != "dollar" && VARIATION !="merkenweek") {
				TweenMax.to(mmMC, 1.1, {y: 50, ease: Expo.easeInOut, delay: d});
				
				/*TweenMax.to(coverMC, 1.2, {y: 60, ease: Expo.easeInOut, delay: d, onComplete: function() {
					coverMC.visible = false;
				}});*/
				
				d += 1.05;
			}
			/*
			TweenMax.to(mmMC, 0.6, {rotation: 3, ease: Quart.easeIn, delay: d});
			TweenMax.to(mmMC, 0.6, {rotation: 0, ease: Expo.easeOut, delay: d + 0.6});
			TweenMax.to(mmMC, 1.2, {scaleX: 0.15, scaleY: 0.15, x: 79, y: 228, ease: Expo.easeInOut, delay: d});
			*/
			
			//d += 0.35;
			var greyToX = 15;
			if (greyMC.currentFrame == 1) {
				greyToX = 0;
			}
			//TweenMax.to(greyMC, 0.4, {x: greyToX + 80, ease: Expo.easeIn, delay: d});
			//TweenMax.to(greyMC, 0.8, {x: greyToX, ease: Expo.easeOut, delay: d + 0.4});
			
			TweenMax.to(mm2MC, 0.9, {x: mm2ToX, ease: Expo.easeOut, delay: d + 0.6});
			TweenMax.to(mm2MC, 0.8, {alpha: 1.0, delay: d + 0.6});
			TweenMax.to(self.counterMC, 0.5, {x: 240, ease: Expo.easeOut, delay: d + 5.6});
			
			d += 0.1;
			
			TweenMax.to(self.merkentext, 0.5, {x:120, alpha:1.0, ease:Sine.easeInOut, delay:d});
			d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:140, alpha:1.0,ease:Sine.easeInOut, delay:d});
			
			self.intro_img.scaleX=self.intro_img.scaleY=0.55;
			d += 2.1;
			
			TweenMax.to(self.merkentext, 0.5, {y:60, ease:Sine.easeInOut, delay:d});
			d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:-300, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			
			d += 0.5;
			
			TweenMax.to(self.intro_text, 0.5, {x:75,ease:Sine.easeInOut, delay:d});
			TweenMax.to(self.intro_img, 0.5, {x:250,ease:Sine.easeInOut, delay:d});
			
			d += 3.1;
			TweenMax.delayedCall(d, animateOutMerken);
			/*
			TweenMax.to(logoMC, 0.8, {y: logoToY, ease: Expo.easeOut, delay: d});
			TweenMax.to(logoMC, 0.5, {alpha: 1.0, delay: d});
			*/
			
			TweenMax.to(campaignMC, 0.5, {scaleX: campaignScale * 0.5, scaleY: campaignScale * 0.5, ease: Expo.easeIn, delay: d});
			TweenMax.to(campaignMC, 0.5, {scaleX: campaignScale, scaleY: campaignScale, ease: Elastic.easeOut.config(2.0, 1.0), delay: d + 0.5});
			TweenMax.from(campaignMC, 0.9, {rotation: -30, ease: Expo.easeInOut, delay: d});
			
			d += 0.3;
			
			d += 0.2;
			if (VARIATION == "dollar") {
				var d0ToY = 23;
				self.d0.y = d0ToY + 30;
				self.d1.y = d0ToY + 20 + 30;
				self.d2.y = d0ToY + 40;
				self.d2.alpha = 0;
				
				self.doll0.rotation = -10;
				self.doll1.rotation = -30;
				self.doll2.rotation = 20;
				self.doll0.scaleX = self.doll0.scaleY = 0.6 * 0.75;
				self.doll1.scaleX = self.doll1.scaleY = 0.7 * 0.75;
				self.doll2.scaleX = self.doll2.scaleY = 0.65 * 0.75;
				self.doll0.x = 25 + 10;
				self.doll1.x = 145 + 20;
				self.doll2.x = 270 + 30;
				
				TweenMax.from(self.d0, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
				TweenMax.to(self.d0, 0.8, {y: d0ToY, ease: Expo.easeOut, delay: d});
				
				d -= 0.2;
				TweenMax.to(self.doll0, 1.9, {x: 25, y: 120, scaleX: 0.6, scaleY: 0.6, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll0, 0.4, {rotation: 30, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll0, 0.6, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.2});
				
				d += 0.15;
				TweenMax.to(self.doll1, 2.5, {x: 145, y: 215, scaleX: 0.7, scaleY: 0.7, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll1, 0.5, {rotation: 20, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll1, 0.7, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.9});
			
				d += 0.15;
				TweenMax.to(self.doll2, 1.7, {x: 270, y: 25, scaleX: 0.65, scaleY: 0.65, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll2, 0.4, {rotation: -30, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll2, 0.7, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.2});
				d -= 0.3;
				
				d += 0.2 + 0.2;
				TweenMax.from(self.d1, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
				TweenMax.to(self.d1, 0.8, {y: d0ToY + 20, ease: Expo.easeOut, delay: d});
				
				d += 0.2;
				TweenMax.from(self.d2, 0.7, {x: 323, ease: Expo.easeOut, delay: d});
				TweenMax.to(self.d2, 0.0, {alpha: 1.0, delay: d});
				
				d += 2.5;
				TweenMax.to(self.doll1, 0.65, {y: dynamic.height + 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.doll0, 0.7, {y: dynamic.height + 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d + 0.05});
				TweenMax.to(self.doll2, 0.8, {y: dynamic.height + 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d + 0.07});
				TweenMax.to(self.doll1, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.doll2, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.doll0, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.d1, 0.4, {y: d0ToY + 20 + 27, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.d1, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				TweenMax.to(self.d2, 0.3, {alpha: 0.0, delay: d});
				TweenMax.to(self.d2, 0.3, {y: "+= 20", ease: Expo.easeIn, delay: d});
				TweenMax.to(self.d2, 0.7, {rotation: 4, ease: Expo.easeInOut, delay: d});
				
				d += 0.075;
				TweenMax.to(self.d0, 0.4, {y: 92 + 27, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.d0, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				d += 0.4;
			}
			if (VARIATION == "merkenweek" && productID==0) {
			//leftMC.y -= 25;
			//rightMC.y -= 25;
			self.dollarcashback.scaleX=self.dollarcashback.scaleY = 0.78;
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: 29,y:82, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:34,ease: Expo.easeIn, delay: 0.0});
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0, ease: Power3.easeIn, delay: 2});
				
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0.78, scaleY:0.78, ease: Power3.easeOut, delay: 2.3});
				TweenMax.delayedCall(d, animateInManiaLabels);	
				}
			else if (VARIATION == "merkenweek") {
				
				TweenMax.delayedCall(d, animateInManiaLabels);
				
			}
			TweenMax.from(text1MC, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text1MC, 0.8, {y: text1ToY, ease: Expo.easeOut, delay: d});
			
			TweenMax.delayedCall(d - 1.5, checkNumberOfProducts);
			
			d += 0.2;
			TweenMax.from(text2MC, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text2MC, 0.8, {y: text2ToY, ease: Expo.easeOut, delay: d});
			
			if (text3MC.dynText.text != "") {
				d += 0.3;
				TweenMax.to(text3MC, 0.7, {x: text3ToX, ease: Expo.easeOut, delay: d});
				TweenMax.to(text3MC, 1.0, {alpha: 1.0, delay: d});
				d -= 0.6;
			} else {
				d -= 0.3;
			}
			
			d += 0.2;
			TweenMax.to(productMC, 1.3, {x: productToX, ease: Expo.easeOut, delay: d});
			
			if (VARIATION == "dollar") {
				TweenMax.delayedCall(d + 0.1, animateInCash);
			}
			
			d += 0.2;
			TweenMax.to(leftMC, 0.9, {x: leftToX, ease: Expo.easeOut, delay: d});
			TweenMax.to(leftMC, 0.8, {alpha: 1.0, delay: d});
			TweenMax.to(rightMC, 0.9, {x: rightToX, ease: Expo.easeOut, delay: d});
			TweenMax.to(rightMC, 0.8, {alpha: 1.0, delay: d});
			
			if (servButtonToX >= 0) {
				d += 0.3;
				TweenMax.to(servButtonMC, 0.6, {y: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: d});
				d -= 0.3;
			}
			
			d += 0.2;
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				TweenMax.delayedCall(d, animateInPrice);
			}
			
			//TweenMax.delayedCall(d, animateInLabels);
			
			d += 0.2;
			TweenMax.delayedCall(d, animateInLabels2);
			
			if (productsObject[productID].label == true) {
				d += 0.2;
				TweenMax.to(labelMC, 0.6, {x: labelToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: d});
			}
			
			d += -0.2;
			TweenMax.to(ctaMC, 0.7, {alpha: 1.0, delay: d});
			TweenMax.to(ctaMC, 0.8, {x: ctaToX, ease: Expo.easeOut, delay: d});
			
		}
		
		function doLeftRightClick(event) {
			//console.log(event.target)
			var maxID = numberOfProducts - numberOfServices - 1;
			var minID = 0;
			if (productsObject[productID].service) {
				maxID = numberOfProducts - 1;
				minID = numberOfProducts - numberOfServices;
			} 
			if (event.target.name == "left" || event.target == leftButtonDiv) {
				if (productID > minID) {
					productID--;
					bigJump = false;
				} else {
					bigJump = true;
					productID = maxID;
				}
				isLeftClick = true;
			} else {
				if (productID < maxID) {
					productID++;
					bigJump = false;
				} else {
					bigJump = true;
					productID = minID;
				}
				isLeftClick = false;
			}
			self.cancelAutoRotate();
			changeProduct();
		}
		
		function adjustTextHeader() {
			
			var H1TextField = text1MC.dynText;
			var H2TextField = text2MC.dynText;
			
			var headerText1Width = H1TextField.getMeasuredWidth();
			var headerText2Width = H2TextField.getMeasuredWidth();
			/*
			if (H2TextField.getMeasuredWidth() > headerTextWidth) {
				headerTextWidth = H2TextField.getMeasuredWidth();
			}
			*/
			var idealHeaderTextWidth = 145;
			
			var headerFontScale = 1.0;
			if (headerText1Width > idealHeaderTextWidth) {
				headerFontScale = 1 / (headerText1Width / idealHeaderTextWidth);
			}
			text1MC.scaleX = text1MC.scaleY = headerFontScale;
			
			headerFontScale = 1.0;
			if (headerText2Width > idealHeaderTextWidth) {
				headerFontScale = 1 / (headerText2Width / idealHeaderTextWidth);
			}
			text2MC.scaleX = text2MC.scaleY = headerFontScale;
			
		}
		
		function updateLogoTextsLabelPrice() {
			
			text3MC.x = text3ToX + 30;
			text1MC.dynText.text = productsObject[productID].name;
			text2MC.dynText.text = productsObject[productID].main_specs;
			text3MC.dynText.text = productsObject[productID].sub_specs;
			adjustTextHeader();
			
			logoMC.y = - 70;
			labelMC.x = dynamic.width + 50;
			for (var i = 0; i<numberOfProducts; i++) {
				if (i == productID && logoMC.children[i] != undefined) {
					logoMC.children[i].alpha = labelMC.children[i].alpha = 1.0;
				} else if (logoMC.children[i] != undefined) {
					logoMC.children[i].alpha = labelMC.children[i].alpha = 0.0;
				}
			}
			
			var z = productsObject[productID].label_new;
			var lab2W = 0;
			if (z != undefined && z != "") {
				if (z.split("_")[0].toLowerCase() == "cashback") {
					self.cashback.dynText.text = z.split("_")[1];
					lab2W = Math.floor(self.cashback.dynText.getMeasuredWidth()) + 11;
					if (self.cashback.dynText.getMeasuredHeight() > 11) {
						lab2W = 65;
					}
				} else if (z.split("_")[0].toLowerCase() == "service") {
					var b = z.split("_")[1].toLowerCase();
					var ID = 0;
					switch (b) {
						case "a": 
						ID = 1;
						break;
						
						case "b": 
						ID = 2;
						break;
						
						case "c": 
						ID = 3;
						break;
						
						case "d": 
						ID = 4;
						break;
						
						case "e": 
						ID = 5;
						break;
						
						case "f": 
						ID = 6;
						break;
						
						case "g": 
						ID = 7;
						break;
						
						case "h": 
						ID = 8;
						break;
						
						case "i": 
						ID = 9;
						break;
					}
					self.servicelabel.gotoAndStop(ID);
				}
			}
			/*
			if (self.servicelabel.currentFrame == 9) {
				self.servicelabel.y = 154;
			} else {
				self.servicelabel.y = 63;
			}
			*/
			
			lineMC.x = lineToX;
			TweenMax.killTweensOf(lineMC.line);
			lineMC.line.scaleX = 0;
			lineMC.alpha = 1.0;
			priceMC.x = priceToX;
			priceMC.alpha = 1.0;
			vanafMC.dynText.text = "";
			vanafMC.x = vanafToX + 15;
			self.prijs.x = prijsToX + 20;
			while (priceMC.numChildren > 0) {
				for (var k = 0; k<priceMC.children[0].numChildren; k++) {
					TweenMax.killTweensOf(priceMC.children[0].children[k]);
					for (var h = 0; h<priceMC.children[0].children[k].numChildren; h++) {
						TweenMax.killTweensOf(priceMC.children[0].children[k].children[h]);
					}
				}
				priceMC.removeChild(priceMC.children[0]);
			}
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				var newPriceMC = new createjs.MovieClip();
				priceMC.addChild(newPriceMC);
				var priceStr = productsObject[productID].price.split(" ");
				if (priceStr.length > 1) {
					vanafMC.dynText.text = priceStr[0].toUpperCase();
					priceStr = priceStr[1].split("");
				} else {
					priceStr = priceStr[0].split("");
				}
				for (i = 0; i<priceStr.length; i++) {
					var childMC = new createjs.MovieClip();
					newPriceMC.addChild(childMC);
					
					var charMC = new createjs.MovieClip();
					//var maskRect = new createjs.Shape();
					childMC.addChild(charMC);
					if (priceStr[i] != ".") {
						var numberID = Number(priceStr[i]) + 1;
						if (greyMC.currentFrame == 1) {
							numberID += 12;
						}
						dynamic.setDynamicImage(charMC, (NUMBERS + numberID), 0, 0);
					} else {
						var numbi = IMAGE_COUNT - 1;
						if (greyMC.currentFrame == 0) {
							numbi -= 12;
						}
						dynamic.setDynamicImage(charMC, (numbi), 0, 0);
					}
					if (i > 0) {
						childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale) - 5;
						if (priceStr[(i - 1)] != "." && newPriceMC.dot == true) {
							childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.7) - 3;
						}
					} else {
						childMC.toX = 0;
					}
					childMC.scaleX = childMC.scaleY = numberImgScale;
					if (newPriceMC.dot) {
						childMC.scaleX = childMC.scaleY = numberImgScale * 0.7;
						childMC.y += 1;
					}
					if (priceStr[i] == ".") {
						childMC.toX -= 0;
						newPriceMC.dot = true;
					}
					if (priceStr[(i - 1)] == ".") {
						childMC.toX -= 2;
					}
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					
				}
				if (!newPriceMC.dot) {
					var childMC = new createjs.MovieClip();
					newPriceMC.addChild(childMC);
					var charEndingMC = new createjs.MovieClip();
					childMC.addChild(charEndingMC);
					var numbi = IMAGE_COUNT;
					if (greyMC.currentFrame == 0) {
						numbi -= 12;
					}
					dynamic.setDynamicImage(charEndingMC, (numbi), 0, 0);
					childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale) - 6;
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					childMC.scaleX = childMC.scaleY = numberImgScale;
				}
			}
			
			if (productsObject[productID].priceOld != "" && productsObject[productID].priceOld != "-" && !isNaN(productsObject[productID].priceOld) && newPriceMC != undefined) {
				newPriceMC.oldPrice = true;
				var oldPriceMC = new createjs.MovieClip();
				newPriceMC.addChild(oldPriceMC);
				var oldPriceStr = productsObject[productID].priceOld.split("");
				oldPriceMC.y = 10;
				oldPriceMC.x = -50;
				for (i = 0; i<oldPriceStr.length; i++) {
					var childMC = new createjs.MovieClip();
					oldPriceMC.addChild(childMC);
					var charMC = new createjs.MovieClip();
					//var maskRect = new createjs.Shape();
					childMC.addChild(charMC);
					if (oldPriceStr[i] != ".") {
						var numberID = Number(oldPriceStr[i]) + 1;
						dynamic.setDynamicImage(charMC, (NUMBERS + numberID), 0, 0);
					} else {
						dynamic.setDynamicImage(charMC, (IMAGE_COUNT - 1 - 12), 0, 0);
					}
					if (i > 0) {
						childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.6) - 5 * 0.6;
						if (oldPriceStr[(i - 1)] != "." && oldPriceMC.dot == true) {
							childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.7 * 0.6) - 3 * 0.6;
						}
					} else {
						childMC.toX = 0;
					}
					childMC.scaleX = childMC.scaleY = numberImgScale * 0.6;
					if (oldPriceMC.dot) {
						childMC.scaleX = childMC.scaleY = numberImgScale * 0.7 * 0.6;
						childMC.y += 0;
					}
					if (oldPriceStr[i] == ".") {
						childMC.toX -= 0;
						oldPriceMC.dot = true;
					}
					if (oldPriceStr[(i - 1)] == ".") {
						childMC.toX -= 1;
					}
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
				}
				if (!oldPriceMC.dot) {
					var childMC = new createjs.MovieClip();
					oldPriceMC.addChild(childMC);
					var charEndingMC = new createjs.MovieClip();
					childMC.addChild(charEndingMC);
					dynamic.setDynamicImage(charEndingMC, (IMAGE_COUNT - 12), 0, 0);
					childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.6) - 6 * 0.6;
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					childMC.scaleX = childMC.scaleY = numberImgScale * 0.6;
				}
				oldPriceMC.priceWidth = oldPriceMC.children[oldPriceMC.numChildren - 1].toX + 9;
				if (oldPriceMC.dot == true) {
					oldPriceMC.priceWidth += 5;
				}
			}
		}
		
		function animateInPrice() {
			var currentPrice = priceMC.children[priceMC.numChildren - 1];
			var indexTill = currentPrice.numChildren;
			var delayPrice = 0.0;
			TweenMax.killTweensOf(lineMC.line);
			lineMC.line.scaleX = 0.0;
			lineMC.scaleY = 0.55;
			if (currentPrice.oldPrice == true) {
				indexTill--;
				lineMC.scaleX = 1 / (90 / currentPrice.children[currentPrice.numChildren - 1].priceWidth);
				for (var j = 0; j<currentPrice.children[currentPrice.numChildren - 1].numChildren; j++) {
					var oldNumberMC = currentPrice.children[currentPrice.numChildren - 1].children[j];
					TweenMax.to(oldNumberMC, 0.8, {x: oldNumberMC.toX, alpha: 1.0, ease: Expo.easeOut, delay: j * 0.03});
				}
				delayPrice = 0.3;
				TweenMax.to(lineMC.line, 0.7, {scaleX: 1.0, ease: Expo.easeOut, delay: 0.15});
			}
			for (var i = 0; i<indexTill; i++) {
				var numberMC = currentPrice.children[i];
				TweenMax.to(numberMC, 0.9, {x: numberMC.toX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + i * 0.03});
			}
			TweenMax.to(vanafMC, 0.9, {x: vanafToX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + 0});
			if (VARIATION == "dollar" && productID == 1) {
				TweenMax.to(self.prijs, 0.9, {x: prijsToX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + 0});
			}
		}
		
		function changeProduct() {
			
			dynamic.productID = productID;
			
			var d = 0.0;
			TweenMax.killDelayedCallsTo(animateInManiaLabels);
			TweenMax.killDelayedCallsTo(updateLogoTextsLabelPrice);
			TweenMax.killDelayedCallsTo(animateInPrice);
			TweenMax.killDelayedCallsTo(editServicesButton);
			TweenMax.killDelayedCallsTo(animateInEndframe);
			TweenMax.killDelayedCallsTo(animateOutEndframe);
			TweenMax.killTweensOf([text1MC, text2MC, text3MC, productMC, logoMC, labelMC, self.cashback, self.servicelabel, priceMC, lineMC, lineMC.line, ctaMC, vanafMC, servButtonMC, leftMC, rightMC, self.prijs, self.dollarcashback, self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3, self.subsubtext]);
			TweenMax.to([logoMC, labelMC, priceMC, lineMC, vanafMC, self.subsubtext], 0.3, {alpha: 0.0});
			var moveBy = 100;
			if (isLeftClick == false || isLeftClick == undefined) {
				moveBy = -moveBy;
			}
			TweenMax.to(labelMC, 0.4, {x: "+=" + (moveBy + 50), ease: Expo.easeIn});
			TweenMax.to(text3MC, 0.3, {x: text3ToX + moveBy, ease: Expo.easeIn, delay: 0.05});
			TweenMax.to(priceMC, 0.4, {x: priceToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			TweenMax.to(vanafMC, 0.4, {x: vanafToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			TweenMax.to(lineMC, 0.4, {x: lineToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			if (VARIATION == "dollar") {
				TweenMax.to([self.prijs, self.dollarcashback, self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.3, {alpha: 0.0});
				TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			if (VARIATION == "merkenweek") {
				TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:34,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			if (VARIATION == "merkenweek" && productID==0) {
			//leftMC.y -= 25;
			//rightMC.y -= 25;
			self.dollarcashback.scaleX=self.dollarcashback.scaleY = 0.78;
				TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: 29,y:82, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:34,ease: Expo.easeIn, delay: 0.0});
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0, ease: Power3.easeIn, delay: 1.5});
				
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0.78, scaleY:0.78, ease: Power3.easeOut, delay: 1.8});
					
				}
				//
			if (VARIATION == "merkenweek" && productID==3) {
			//leftMC.y -= 25;
			//rightMC.y -= 25;
		
				TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:34,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			if (productsObject[productID].service && servButtonMC.dynText.text == "BEKIJK ONZE SERVICES") {
				TweenMax.to(servButtonMC, 0.4, {y: -40, alpha: 0.0, ease: Expo.easeIn, delay: 0});
				TweenMax.to(servButtonMC, 0.6, {y: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0.4});
				TweenMax.to(servButtonMC, 0.4, {alpha: 1.0, delay: 0.4});
				TweenMax.delayedCall(0.4, editServicesButton, ["BEKIJK ONZE DEALS"]);
			}
			if (!productsObject[productID].service && servButtonMC.dynText.text == "BEKIJK ONZE DEALS") {
				TweenMax.to(servButtonMC, 0.4, {y: -40, ease: Expo.easeIn, alpha: 0.0, delay: 0});
				TweenMax.to(servButtonMC, 0.6, {y: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0.4});
				TweenMax.to(servButtonMC, 0.4, {alpha: 1.0, delay: 0.4});
				TweenMax.delayedCall(0.4, editServicesButton, ["BEKIJK ONZE SERVICES"]);
			}
			
			TweenMax.to(text3MC, 0.3, {alpha: 0.0});
			
			var productTargetX = productToX - productID * 300;
			if (bigJump == true) {
				var productTargetPrevX = productTargetX - 75;
				if (!(productID == 0) && prevProductID <= numberOfProducts - numberOfServices - 1) {
					productTargetPrevX += 150;
				} else if (productID == numberOfProducts - 1) {
					productTargetPrevX += 150;
				}
				TweenMax.to(productMC, 0.8, {x: productTargetPrevX, ease: Expo.easeIn, delay: d});
				TweenMax.to(productMC, 0.6, {x: productTargetX, ease: Expo.easeOut, delay: d + 0.8});
			} else {
				TweenMax.to(productMC, 1.2, {x: productTargetX, ease: Expo.easeInOut, delay: d});
			}
			prevProductID = productID;
			
			/*
			if (numberOfServices > 0) {
				var whichCtaMC = ctaMC;
				if (productsObject[productID].service) {
					TweenMax.to(ctaMC, 0.9, {x: ctaToX + dynamic.width / 2, ease: Expo.easeInOut, delay: d});
					TweenMax.to(ctaMC, 0.4, {alpha: 0.0, delay: d});
					TweenMax.to(cta2MC, 0.9, {x: ctaToX, ease: Expo.easeInOut, delay: d});
					TweenMax.to(cta2MC, 0.9, {alpha: 1.0, delay: d + 0.4});
					whichCtaMC = cta2MC;
				} else {
					TweenMax.to(ctaMC, 0.9, {x: ctaToX, ease: Expo.easeInOut, delay: d});
					TweenMax.to(ctaMC, 1.3, {alpha: 1.0, delay: d});
					TweenMax.to(cta2MC, 0.9, {x: ctaToX - 200, ease: Expo.easeInOut, delay: d});
					TweenMax.to(cta2MC, 0.5, {alpha: 0.0, delay: d});
				}
				editCtaButtonDivWidth(whichCtaMC.ctaTextWidth);
			}
			*/
			
			TweenMax.to(text2MC, 0.4, {y: text2ToY + 25, ease: Expo.easeInOut, delay: d});
			TweenMax.to(text2MC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			
			d += 0.075;
			TweenMax.to(text1MC, 0.4, {y: text1ToY + 25, ease: Expo.easeInOut, delay: d});
			TweenMax.to(text1MC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			
			d += 0.4;
			TweenMax.delayedCall(d, updateLogoTextsLabelPrice);
			
			if (typeof(productsObject[productID]) == "string") {
				TweenMax.to(productMC, 0.3, {alpha: 0.0, delay: d - 0.3});
				TweenMax.delayedCall(d, animateInEndframe);
				leftToX = 12;
				//rightToX = 465;
				TweenMax.to(campaignMC, 0.3, {alpha: 0.0, delay: d - 0.2});
			} else {
				TweenMax.to(productMC, 0.6, {alpha: 1.0, delay: d + 0.2});
				TweenMax.delayedCall(d - 0.5, animateOutEndframe);
				leftToX = 166;
				//rightToX = 410;
				TweenMax.to(campaignMC, 0.6, {alpha: 1.0, delay: d});
			}
			if (productsObject[productID].service) {
				if (numberOfServices <= 1) {
					leftButtonDiv.style.display = rightButtonDiv.style.display = "none";
					TweenMax.to(leftMC, 0.8, {x: leftToX + 15, ease: Expo.easeOut, delay: d - 0.2});
					TweenMax.to(rightMC, 0.8, {x: rightToX - 15, ease: Expo.easeOut, delay: d - 0.2});
					TweenMax.to(leftMC, 0.4, {alpha: 0, delay: d - 0.4});
					TweenMax.to(rightMC, 0.4, {alpha: 0, delay: d - 0.4});
				} else {
					leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
					TweenMax.to(leftMC, 0.8, {x: leftToX, alpha: 1.0, ease: Expo.easeOut, delay: d});
					TweenMax.to(rightMC, 0.8, {x: rightToX, alpha: 1.0, ease: Expo.easeOut, delay: d});
				}
			} else if (numberOfProducts > 1) {
				TweenMax.to(leftMC, 0.8, {x: leftToX, alpha: 1, ease: Expo.easeOut, delay: d});
				TweenMax.to(rightMC, 0.8, {x: rightToX, alpha: 1, ease: Expo.easeOut, delay: d});
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
			}
			leftButtonDiv.style.left = (leftToX - 13) + "px";
			//rightButtonDiv.style.left = (rightToX - 4) + "px";
			/*
			TweenMax.to(logoMC, 0.8, {y: logoToY, ease: Expo.easeOut, delay: d});
			TweenMax.to(logoMC, 0.5, {alpha: 1.0, delay: d});
			*/
			
			if (VARIATION == "dollar") {
				TweenMax.delayedCall(d + 0.1, animateInCash);
			}
			if (VARIATION == "merkenweek") {
				//TweenMax.delayedCall(d + 0.1, animateInStripe);
				TweenMax.delayedCall(d, animateInManiaLabels);
			}
			
			//d += 0.1;
			TweenMax.to(text1MC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text1MC, 0.8, {y: text1ToY, ease: Expo.easeOut, delay: d});
			
			d += 0.2;
			TweenMax.to(text2MC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text2MC, 0.8, {y: text2ToY, ease: Expo.easeOut, delay: d});
			
			if (text3MC.dynText.text != "") {
				d += 0.1;
				TweenMax.to(text3MC, 0.7, {x: text3ToX, ease: Expo.easeOut, delay: d});
				TweenMax.to(text3MC, 1.0, {alpha: 1.0, delay: d});
			}
			
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				d += 0.2;
				TweenMax.delayedCall(d, animateInPrice);
			}
			
			//TweenMax.delayedCall(d, animateInLabels);
			
			//if (productsObject[productID].label == true) {
			d += 0.2;
			TweenMax.delayedCall(d, animateInLabels2);
			
		}
		function animateInManiaLabels() {
			var k = self.subsubtext;
			k.x = maniaLabelToX;
			k.alpha = 1.0;
			k.gotoAndStop(productID);
			var s = 0;
			while (k["m" + productID + s] != undefined) {
				s++;
			}
			var howMany = s;
			for (var i = 0; i<howMany; i++) {
				var n = k["m" + productID+i]
				console.log(n)
				TweenMax.from(n, 0.8, {x: 50, alpha: 0.0, delay: i * 0.05});
			}
		}
		function animateInCash() {
			var cash = self["dollProd" + productID];
			cash.rotation = -10;
			var toX = [190, 200, 190, 265];
			var toY = [60, 35, 60, 35];
			var d = 0.0;
			cash.x = toX[productID];
			cash.y = toY[productID];
			cash.scaleX = cash.scaleY = 0.0;
			cash.alpha = 1.0;
			//cash.x = dynamic.width + 50;
			//TweenMax.to(cash, 1.0, {y: toY[productID], ease: Sine.easeOut, delay: d});
			TweenMax.to(cash, 0.2, {rotation: 10, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d + 0.5});
			TweenMax.to(cash, 0.8, {rotation: 0, ease: Elastic.easeOut.config(1.0, 1.0), delay: d + 1.1});
			TweenMax.to(cash, 1.0, {scaleX: 0.6, scaleY: 0.6, ease: Elastic.easeOut.config(1.0, 0.8), delay: d + 0.5});
			
			d += 0.1;
			var order = [1, 0, 2];
			for (var i = 0; i<3; i++) {
				var cash2 = self["dollRandom" + productID + i];
				cash2.rotation = Math.floor(Math.random() * 60) - 30;
				cash2.scaleX = cash2.scaleY = Math.floor(Math.random() * 25) / 100 + 0.5;
				var toRot = -cash2.rotation;
				cash2.y = -50;
				cash2.x = 50 + i * 100 + Math.floor(Math.random() * 40);
				var time = Math.floor(Math.random() * 10) / 10 + 1.2;
				TweenMax.to(cash2, time, {x: "-= 30", y: dynamic.height + 50, ease: Sine.easeOut, delay: d + order[i] * 0.3});
				TweenMax.to(cash2, 0.5, {rotation: toRot, ease: Sine.easeInOut, repeat: 6, yoyo: true, delay: d + order[i] * 0.3});
				//TweenMax.to(cash2, 0.6, {rotation: 0, ease: Cubic.easeOut, delay: d + i * 0.1 + 1.2});
			}
		}
		
		function animateOutMerken() {
			
		
			var d = 0.0;
			TweenMax.to(self.merkentext, 0.5, {x:-300, ease:Sine.easeInOut, delay:d});
			d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:-300, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			d += 0.1;
			
			TweenMax.to(self.intro_text, 0.5, {x:-300, alpha:0.0,ease:Sine.easeInOut, delay:d});
			TweenMax.to(self.intro_img, 0.5, {x:500, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			d += 0.5;
			//self.manual_counter_mc.scaleX = self.manual_counter_mc.scaleY = 0.13;
			//TweenMax.to(self.manual_counter_mc, 0.5, {x:265, y:180,ease:Sine.easeInOut, delay:d});
		}
		function buildServicesButton(caption) {
			
			var servTextField = servButtonMC.dynText;
			editServicesButton(caption);
			var servButtonWidth = 110;
			var servRect = new createjs.Shape();
			servRect.graphics.beginFill("#ffffff").drawRoundRectComplex(0, -10, servButtonWidth, 33, 0, 0, 10, 10);
			servButtonMC.addChild(servRect);
			servButtonMC.swapChildren(servButtonMC.children[0], servButtonMC.children[1]);
			servButtonToX = 0;
			if (greyMC.currentFrame == 1) {
				//servButtonToX -= 10;
			}
			
		}
		
		function editServicesButton(caption) {
			
			var servTextField = servButtonMC.dynText;
			servTextField.text = caption;
		
		}
		
		function doServices() {
			self.cancelAutoRotate();
			if (servButtonMC.dynText.text == "BEKIJK ONZE DEALS") {
				productID = 0;
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
			} else {
				productID = numberOfProducts - numberOfServices;
			}
			bigJump = true;
			changeProduct();
		}
		
		function animateInEndframe() {
			var d = 0.0;
			for (var i = 0; i<lines; i++) {
				var lMC = endMC["l" + i];
				d = i * 0.12;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
				TweenMax.to(lMC, 0.8, {y: lMC.toY, ease: Expo.easeOut, delay: d});
			}
		}
		
		function animateOutEndframe() {
			var d = 0.0;
			for (var i = 0; i<lines; i++) {
				var lMC = endMC["l" + i];
				d = (lines - i) * 0.05;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.4, {y: lMC.toY + 14, ease: Expo.easeInOut, delay: d});
				TweenMax.to(lMC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			}
		}
		
		function animateInLabels2() {
			if (self.cashback != undefined && self.cashback.dynText.text.length > 0) {
				TweenMax.to(self.cashback, 0, {alpha: 1.0});
				TweenMax.to(self.cashback, 0.6, {x: labelToX, ease: Elastic.easeOut.config(0.8, 1.2)});
			} else if (self.servicelabel.currentFrame > 0) {
				TweenMax.to(self.servicelabel, 0.6, {x: serviceLabelToX, ease: Expo.easeOut});
			}
			if (VARIATION == "dollar") {
				var animIn = true;
				self.dollarcashback.alpha = 1.0;
				self.dollarcashback.x = dynamic.width + 150;
				if (productID == 1) {
					self.dollarcashback.text.gotoAndStop(0);
				} else if (productID == 2) {
					self.dollarcashback.text.gotoAndStop(1);
				} else {
					animIn = false;
				}
				if (animIn == true) {
					TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
			}
		 if (VARIATION == "merkenweek") {
				var animIn = true;
				self.dollarcashback.alpha = 1.0;
				self.dollarcashback.x = dynamic.width+150;
				// self.dollarcashback.text.gotoAndStop(3);
				if (productID == 0) {
					self.dollarcashback.scaleX=self.dollarcashback.scaleY = 0.78;
				
					self.dollarcashback.text.gotoAndStop(1);
					self.dollarcashback.scaleX=self.dollarcashback.scaleY=0.78;
					//TweenMax.to(self.dollarcashback, 0.6, {x: 29, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				TweenMax.delayedCall(0.825, gotoLastFrame);
				} 
				if (productID == 3) {
					self.dollarcashback.text.gotoAndStop(0);
					
				} else {
					animIn = false;
					//self.dollarcashback.text.gotoAndStop(0);
				}
				if (animIn == true && productID == 0) {
					self.dollarcashback.scaleX=self.dollarcashback.scaleY = 0.78;
				
					//TweenMax.to(self.dollarcashback, 0.6, {x: 29,y:82, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
				else if (animIn == true && productID == 3) {
					self.dollarcashback.scaleX=self.dollarcashback.scaleY = 0.78;
				
					//TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
			}
			
		}
		function gotoLastFrame(){
			
			self.dollarcashback.text.gotoAndStop(2);
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// border
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape.setTransform(160.0068,99.4776,0.955,0.04);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_1.setTransform(160.0068,0.5248,0.955,0.04);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_2.setTransform(0.523,49.9838,0.0028,4.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_3.setTransform(319.5231,49.9838,0.0028,4.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// counter
	this.counterMC = new lib.counterMC();
	this.counterMC.name = "counterMC";
	this.counterMC.parent = this;
	this.counterMC.setTransform(161.5,1.1);

	this.timeline.addTween(cjs.Tween.get(this.counterMC).wait(1));

	// elements_over
	this.dollProd3 = new lib.dollar_4();
	this.dollProd3.name = "dollProd3";
	this.dollProd3.parent = this;
	this.dollProd3.setTransform(85.1,-40,1,1,0,0,0,0.1,0);

	this.dollProd2 = new lib.dollar_5();
	this.dollProd2.name = "dollProd2";
	this.dollProd2.parent = this;
	this.dollProd2.setTransform(80,-200);

	this.dollProd0 = new lib.dollar_1_v2();
	this.dollProd0.name = "dollProd0";
	this.dollProd0.parent = this;
	this.dollProd0.setTransform(100,-129.7);

	this.doll2 = new lib.dollar_3();
	this.doll2.name = "doll2";
	this.doll2.parent = this;
	this.doll2.setTransform(350,-80);

	this.doll1 = new lib.dollar_2();
	this.doll1.name = "doll1";
	this.doll1.parent = this;
	this.doll1.setTransform(200,-60);

	this.doll0 = new lib.dollar_1();
	this.doll0.name = "doll0";
	this.doll0.parent = this;
	this.doll0.setTransform(80,-70);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.doll0},{t:this.doll1},{t:this.doll2},{t:this.dollProd0},{t:this.dollProd2},{t:this.dollProd3}]}).wait(1));

	// text2Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3bEdIAAj6MAu3AAAIAAD6g");
	mask.setTransform(134.9995,28.4945);

	// text2
	this.text2 = new lib.text2();
	this.text2.name = "text2";
	this.text2.parent = this;
	this.text2.setTransform(9.9,36.5,1,1,0,0,0,2.9,10.5);

	var maskedShapeInstanceList = [this.text2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.text2).wait(1));

	// text1Mask (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("A3bClIAAjlMAu3AAAIAADlg");
	mask_1.setTransform(134.9995,16.4911);

	// text1
	this.text1 = new lib.text1();
	this.text1.name = "text1";
	this.text1.parent = this;
	this.text1.setTransform(9.9,32,1,1,0,0,0,2.9,-2);

	var maskedShapeInstanceList = [this.text1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.text1).wait(1));

	// arrows
	this.left = new lib.arrowLeftRight();
	this.left.name = "left";
	this.left.parent = this;
	this.left.setTransform(166,50,1,1,0,0,180);
	this.left.alpha = 0;
	this.left.visible = false;

	this.right = new lib.arrowLeftRight();
	this.right.name = "right";
	this.right.parent = this;
	this.right.setTransform(301,50);
	this.right.alpha = 0;
	this.right.visible = false;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.right},{t:this.left}]}).wait(1));

	// text1_mask (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("A3WDmIAAjmMAutAAAIAADmg");
	mask_2.setTransform(149.5,23.0021);

	// text1
	this.d0 = new lib.dollar_text_1();
	this.d0.name = "d0";
	this.d0.parent = this;
	this.d0.setTransform(8,400);

	var maskedShapeInstanceList = [this.d0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.d0).wait(1));

	// text2_mask (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("A3WFKIAAjmMAutAAAIAADmg");
	mask_3.setTransform(149.5,33.0021);

	// text2
	this.d1 = new lib.dollar_text_2();
	this.d1.name = "d1";
	this.d1.parent = this;
	this.d1.setTransform(8,420);

	var maskedShapeInstanceList = [this.d1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.d1).wait(1));

	// rest
	this.d2 = new lib.dollar_text_3();
	this.d2.name = "d2";
	this.d2.parent = this;
	this.d2.setTransform(8,440);

	this.timeline.addTween(cjs.Tween.get(this.d2).wait(1));

	// elements
	this.intro_img = new lib.intro_img_1();
	this.intro_img.name = "intro_img";
	this.intro_img.parent = this;
	this.intro_img.setTransform(397.5,52.5,1,1,0,0,0,67.5,42.5);

	this.intro_text = new lib.Symbol10();
	this.intro_text.name = "intro_text";
	this.intro_text.parent = this;
	this.intro_text.setTransform(-175.95,65,1,1,0,0,0,70.4,35);

	this.merkentext = new lib.merken_text();
	this.merkentext.name = "merkentext";
	this.merkentext.parent = this;
	this.merkentext.setTransform(436.9,60,1,1,0,0,0,116.9,55);

	this.logobalk = new lib.logobalk_1();
	this.logobalk.name = "logobalk";
	this.logobalk.parent = this;
	this.logobalk.setTransform(480,63,1,1,0,0,0,160,23);

	this.subsubtext = new lib.subsubtext();
	this.subsubtext.name = "subsubtext";
	this.subsubtext.parent = this;
	this.subsubtext.setTransform(328.8,37);

	this.mediamarkt2 = new lib.mediamarkt_logo();
	this.mediamarkt2.name = "mediamarkt2";
	this.mediamarkt2.parent = this;
	this.mediamarkt2.setTransform(38.05,328,0.085,0.0847,0,0,0,0.6,0.6);

	this.servicelabel = new lib.label_service();
	this.servicelabel.name = "servicelabel";
	this.servicelabel.parent = this;
	this.servicelabel.setTransform(700,63);

	this.dollRandom32 = new lib.dollar_4();
	this.dollRandom32.name = "dollRandom32";
	this.dollRandom32.parent = this;
	this.dollRandom32.setTransform(210.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom30 = new lib.dollar_4();
	this.dollRandom30.name = "dollRandom30";
	this.dollRandom30.parent = this;
	this.dollRandom30.setTransform(130.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom31 = new lib.dollar_4();
	this.dollRandom31.name = "dollRandom31";
	this.dollRandom31.parent = this;
	this.dollRandom31.setTransform(30.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom22 = new lib.dollar_5();
	this.dollRandom22.name = "dollRandom22";
	this.dollRandom22.parent = this;
	this.dollRandom22.setTransform(235,-130);

	this.dollRandom20 = new lib.dollar_5();
	this.dollRandom20.name = "dollRandom20";
	this.dollRandom20.parent = this;
	this.dollRandom20.setTransform(130,-126);

	this.dollRandom21 = new lib.dollar_5();
	this.dollRandom21.name = "dollRandom21";
	this.dollRandom21.parent = this;
	this.dollRandom21.setTransform(30,-125);

	this.dollRandom12 = new lib.dollar_3();
	this.dollRandom12.name = "dollRandom12";
	this.dollRandom12.parent = this;
	this.dollRandom12.setTransform(235.05,-80);

	this.dollRandom10 = new lib.dollar_3();
	this.dollRandom10.name = "dollRandom10";
	this.dollRandom10.parent = this;
	this.dollRandom10.setTransform(125,-40);

	this.dollRandom11 = new lib.dollar_3();
	this.dollRandom11.name = "dollRandom11";
	this.dollRandom11.parent = this;
	this.dollRandom11.setTransform(45,-90);

	this.dollRandom00 = new lib.dollar_1_v2();
	this.dollRandom00.name = "dollRandom00";
	this.dollRandom00.parent = this;
	this.dollRandom00.setTransform(160,-40,0.8,0.8);

	this.dollRandom02 = new lib.dollar_1_v2();
	this.dollRandom02.name = "dollRandom02";
	this.dollRandom02.parent = this;
	this.dollRandom02.setTransform(265,-30,0.8,0.8);

	this.dollRandom01 = new lib.dollar_1_v2();
	this.dollRandom01.name = "dollRandom01";
	this.dollRandom01.parent = this;
	this.dollRandom01.setTransform(40,-30,0.8,0.8);

	this.dollarcashback = new lib.dollar_cashback();
	this.dollarcashback.name = "dollarcashback";
	this.dollarcashback.parent = this;
	this.dollarcashback.setTransform(450,27,0.6572,0.6572);

	this.dollProd1 = new lib.dollar_3();
	this.dollProd1.name = "dollProd1";
	this.dollProd1.parent = this;
	this.dollProd1.setTransform(100,-38.05);

	this.prijs = new lib.dollar_prijs();
	this.prijs.name = "prijs";
	this.prijs.parent = this;
	this.prijs.setTransform(640,86);
	this.prijs.alpha = 0;

	this.lineContainer = new lib.price_old_line_container();
	this.lineContainer.name = "lineContainer";
	this.lineContainer.parent = this;
	this.lineContainer.setTransform(-130,77);

	this.price = new lib.emptyMovieClip();
	this.price.name = "price";
	this.price.parent = this;
	this.price.setTransform(108,66);

	this.campaign = new lib.emptyMovieClip();
	this.campaign.name = "campaign";
	this.campaign.parent = this;
	this.campaign.setTransform(32,78);

	this.endframe = new lib.endframe_static();
	this.endframe.name = "endframe";
	this.endframe.parent = this;
	this.endframe.setTransform(-513,27);

	this.vanaf = new lib.vanaf();
	this.vanaf.name = "vanaf";
	this.vanaf.parent = this;
	this.vanaf.setTransform(-224,77);
	this.vanaf.alpha = 0;

	this.servButton = new lib.services_button();
	this.servButton.name = "servButton";
	this.servButton.parent = this;
	this.servButton.setTransform(190.1,-39.9,1,1,0,0,0,0.1,0.1);

	this.text3 = new lib.text3();
	this.text3.name = "text3";
	this.text3.parent = this;
	this.text3.setTransform(7,263);
	this.text3.alpha = 0;

	this.white = new lib.shape2();
	this.white.name = "white";
	this.white.parent = this;
	this.white.setTransform(-20,0);

	this.cta = new lib.cta();
	this.cta.name = "cta";
	this.cta.parent = this;
	this.cta.setTransform(570,71);

	this.logo = new lib.emptyMovieClip();
	this.logo.name = "logo";
	this.logo.parent = this;
	this.logo.setTransform(10,0);
	this.logo.alpha = 0;

	this.label = new lib.emptyMovieClip();
	this.label.name = "label";
	this.label.parent = this;
	this.label.setTransform(225,168);

	this.product = new lib.emptyMovieClip();
	this.product.name = "product";
	this.product.parent = this;
	this.product.setTransform(234,50);

	this.grey = new lib.shape_grey();
	this.grey.name = "grey";
	this.grey.parent = this;
	this.grey.setTransform(320,15);

	this.cover = new lib.cover1();
	this.cover.name = "cover";
	this.cover.parent = this;
	this.cover.setTransform(0,109);

	this.mediamarkt = new lib.mediamarkt_logo();
	this.mediamarkt.name = "mediamarkt";
	this.mediamarkt.parent = this;
	this.mediamarkt.setTransform(160.15,125.1,0.27,0.27,0,0,0,0.4,0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mediamarkt},{t:this.cover},{t:this.grey},{t:this.product},{t:this.label},{t:this.logo},{t:this.cta},{t:this.white},{t:this.text3},{t:this.servButton},{t:this.vanaf},{t:this.endframe},{t:this.campaign},{t:this.price},{t:this.lineContainer},{t:this.prijs},{t:this.dollProd1},{t:this.dollarcashback},{t:this.dollRandom01},{t:this.dollRandom02},{t:this.dollRandom00},{t:this.dollRandom11},{t:this.dollRandom10},{t:this.dollRandom12},{t:this.dollRandom21},{t:this.dollRandom20},{t:this.dollRandom22},{t:this.dollRandom31},{t:this.dollRandom30},{t:this.dollRandom32},{t:this.servicelabel},{t:this.mediamarkt2},{t:this.subsubtext},{t:this.logobalk},{t:this.merkentext},{t:this.intro_text},{t:this.intro_img}]}).wait(1));

	// Layer_1
	this.instance = new lib.loopy();
	this.instance.parent = this;
	this.instance.setTransform(320,42,1,1,0,0,0,320,42);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// hitAreaMC
	this.hitAreaMC = new lib.shape1();
	this.hitAreaMC.name = "hitAreaMC";
	this.hitAreaMC.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.hitAreaMC).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-353,-178.9,1097,632.9);
// library properties:
lib.properties = {
	id: 'CB71CC1E45F7401086B269D4402BC0DC',
	width: 320,
	height: 100,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/grey_field.png", id:"grey_field"},
		{src:"images/intro_img.png", id:"intro_img"},
		{src:"images/logobalk.png", id:"logobalk"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['CB71CC1E45F7401086B269D4402BC0DC'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;