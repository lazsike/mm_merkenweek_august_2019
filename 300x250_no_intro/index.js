(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.grey_color = function() {
	this.initialize(img.grey_color);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,323,206);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.vanaf = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "12px 'Geomanist'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 0;
	this.dynText.lineWidth = 86;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.vanaf, new cjs.Rectangle(0,0,90,23.9), null);


(lib.text3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "13px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 12;
	this.dynText.lineWidth = 86;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text3, new cjs.Rectangle(0,0,90,25.6), null);


(lib.text2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "28px 'Geomanist'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 30;
	this.dynText.lineWidth = 696;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text2, new cjs.Rectangle(0,0,700,32), null);


(lib.text1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "28px 'Geomanist'", "#E90000");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 30;
	this.dynText.lineWidth = 696;
	this.dynText.parent = this;
	this.dynText.setTransform(2,-30);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text1, new cjs.Rectangle(0,-32,700,32), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E6E7E8").s().p("A0TLuIAA3bMAonAAAIAAXbg");
	this.shape.setTransform(130,84.9997,1,1.1333);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,260,170), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAgBAAQAAAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape.setTransform(119.225,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgiAAIAAhhIAqAAQARAAAKAJQAKAJAAAOQAAAIgFAHQgEAFgHAEIAWApgAgIgHIAEAAQADgBACgCQAAgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgCgDgDAAIgEAAg");
	this.shape_1.setTransform(111.15,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_2.setTransform(102.925,7.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAXAAQAaAAAPAQIgUAVQgHgGgLAAQgHgBgFAGQgHAGAAAIQABAJAFAHQAGAFAHAAQAIAAAFgDIAAgDIgMAAIAAgXIArAAIAAArQgSAQgdAAQgYAAgOgPg");
	this.shape_3.setTransform(94.15,7.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgRAxIAAhhIAjAAIAABhg");
	this.shape_4.setTransform(86.85,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AggAmQgMgKAAgTIAAg6IAkAAIAAA7QAAAKAIAAQAKAAAAgKIAAg7IAjAAIAAA6QAAATgLAKQgMAMgWAAQgUAAgMgMg");
	this.shape_5.setTransform(79.725,7.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgpAxIAAgTIAlgyIgkAAIAAgcIBRAAIAAASIglAzIAmAAIAAAcg");
	this.shape_6.setTransform(70.475,7.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgeAxIAAhhIA8AAIAAAcIgZAAIAAALIAXAAIAAAZIgXAAIAAAhg");
	this.shape_7.setTransform(62.85,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_8.setTransform(53.875,7.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_9.setTransform(44.45,7.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_10.setTransform(36.525,7.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAXAAQAaAAAPAQIgUAVQgHgGgLAAQgHgBgFAGQgHAGAAAIQABAJAFAHQAGAFAHAAQAIAAAFgDIAAgDIgLAAIAAgXIAqAAIAAArQgSAQgdAAQgYAAgOgPg");
	this.shape_11.setTransform(24.5,7.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_12.setTransform(15.875,7.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAPAxIgEgPIgWAAIgEAPIgjAAIAihhIAhAAIAiBhgAAFAJIgFgTIgFATIAKAAg");
	this.shape_13.setTransform(7.2,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,125.5,18), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgYAnQgIgGgDgKIAYgMQAFAKAGAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAADAGADIAMAHQASAIAAAPQAAAOgKAHQgKAIgQAAQgNAAgLgIg");
	this.shape.setTransform(133.325,7.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_1.setTransform(126.375,7.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAKAuIgTgrIAAArIghAAIAAhbIAiAAIASArIAAgrIAhAAIAABbg");
	this.shape_2.setTransform(118.475,7.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgPAuIAAhbIAgAAIAABbg");
	this.shape_3.setTransform(111.8,7.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAJAuIAAghIgSAAIAAAhIggAAIAAhbIAgAAIAAAhIASAAIAAghIAhAAIAABbg");
	this.shape_4.setTransform(105.1,7.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgZAiQgOgNAAgVQAAgTANgNQAOgOAVAAQASAAAMAJIgKAaQgGgHgLAAQgHAAgFAFQgEAFgBAIQAAAJAFAFQAFAFAHAAQALAAAHgHIAKAaQgMAJgTAAQgUAAgOgNg");
	this.shape_5.setTransform(96.25,7.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghbIAfAAIAgBbgAAEAIIgEgSIgFASIAJAAg");
	this.shape_6.setTransform(87.325,7.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAYAuIAAgsIgJAlIgdAAIgJglIAAAsIgfAAIAAhbIApAAIANAzIAPgzIAoAAIAABbg");
	this.shape_7.setTransform(76.55,7.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgYAnQgIgGgDgKIAYgMQAFAKAGAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAADAGADIAMAHQASAIAAAPQAAAOgKAHQgKAIgQAAQgNAAgLgIg");
	this.shape_8.setTransform(66.825,7.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAOAuIgEgPIgUAAIgEAPIghAAIAghbIAfAAIAgBbgAAEAIIgEgSIgFASIAJAAg");
	this.shape_9.setTransform(58.275,7.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AALAuIgLgvIgKAvIghAAIgYhbIAjAAIAJAyIALgyIAZAAIALAyIAJgyIAjAAIgYBbg");
	this.shape_10.setTransform(46.575,7.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_11.setTransform(33.975,7.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgcAuIAAhbIAgAAIAABBIAZAAIAAAag");
	this.shape_12.setTransform(27.7,7.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_13.setTransform(21.175,7.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgPAuIAAhbIAgAAIAABbg");
	this.shape_14.setTransform(15.95,7.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAXAuIAAgsIgJAlIgcAAIgJglIAAAsIgfAAIAAhbIApAAIANAzIAOgzIAqAAIAABbg");
	this.shape_15.setTransform(7.95,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,139.3,17), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape.setTransform(120.425,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_1.setTransform(111.925,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAgBAAQAAAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_2.setTransform(101.725,7.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgQAOQAIgEADgHQgMgDAAgLQAAgHAFgFQAFgEAHAAQAIAAAEAEQAGAFAAAIQAAALgGAKQgGAKgJAHg");
	this.shape_3.setTransform(95.375,4.875);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgRAxIgjhhIAmAAIAPA5IAPg5IAmAAIgkBhg");
	this.shape_4.setTransform(87.6,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_5.setTransform(78.25,7.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAWAAQAbAAAPAQIgUAVQgHgGgLAAQgHgBgFAGQgHAGAAAIQABAJAFAHQAGAFAHAAQAIAAAFgDIAAgDIgLAAIAAgXIAqAAIAAArQgSAQgeAAQgXAAgOgPg");
	this.shape_6.setTransform(66.35,7.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_7.setTransform(56.175,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AggAmQgMgKAAgTIAAg6IAkAAIAAA7QAAAKAIAAQAKAAAAgKIAAg7IAjAAIAAA6QAAATgLAKQgMAMgWAAQgUAAgMgMg");
	this.shape_8.setTransform(46.225,7.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_9.setTransform(37.275,7.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAaAxIAAgvIgKAoIgfAAIgJgoIAAAvIgjAAIAAhhIAsAAIAPA3IAQg3IAsAAIAABhg");
	this.shape_10.setTransform(26.85,7.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAPAxIgFgPIgVAAIgFAPIgjAAIAihhIAjAAIAiBhgAAEAJIgEgTIgGATIAKAAg");
	this.shape_11.setTransform(15.25,7.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_12.setTransform(6.025,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,130.4,18), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgQAOQAIgEADgHQgMgDAAgLQAAgHAFgFQAFgEAHAAQAIAAAFAEQAFAFAAAIQAAALgGAKQgGALgJAGg");
	this.shape.setTransform(161.075,12.275);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_1.setTransform(155.175,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_2.setTransform(147.225,7.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_3.setTransform(137.425,7.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgQAxIAAhFIgUAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_4.setTransform(128,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAHAxIgQgoIAAAoIgkAAIAAhhIAkAAIAAAjIAPgjIAnAAIgYAvIAZAyg");
	this.shape_5.setTransform(119.375,7.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_6.setTransform(110.325,7.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_7.setTransform(102.825,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgsAxIAAhhIAjAAQAYAAAPANQAPANAAAWQAAAXgPANQgPANgYAAgAgKAVIACAAQARAAgBgVQABgUgRAAIgCAAg");
	this.shape_8.setTransform(94.75,7.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgQAOQAIgEADgHQgMgDAAgLQAAgHAFgFQAFgEAHAAQAIAAAFAEQAFAFAAAIQAAALgGAKQgGALgJAGg");
	this.shape_9.setTransform(85.275,12.275);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_10.setTransform(79.375,7.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_11.setTransform(71.425,7.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_12.setTransform(61.625,7.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgQAxIAAhFIgUAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_13.setTransform(52.2,7.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_14.setTransform(44.375,7.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAPAxIgFgPIgVAAIgFAPIgiAAIAhhhIAiAAIAiBhgAAFAJIgFgTIgFATIAKAAg");
	this.shape_15.setTransform(34.9,7.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgeAxIAAhhIAjAAIAABFIAaAAIAAAcg");
	this.shape_16.setTransform(26.425,7.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_17.setTransform(16.275,7.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAKAxIAAgjIgTAAIAAAjIgkAAIAAhhIAkAAIAAAjIATAAIAAgjIAkAAIAABhg");
	this.shape_18.setTransform(7,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,165.2,18), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape.setTransform(122.525,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgsAxIAAhhIAjAAQAYAAAPANQAPANAAAWQAAAXgPANQgPANgYAAgAgJAVIABAAQAQAAAAgVQAAgUgQAAIgBAAg");
	this.shape_1.setTransform(114.45,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgiAAIAAhhIAqAAQARAAAKAJQAKAJAAAOQgBAIgEAHQgEAFgHAEIAXApgAgIgHIAEAAQADgBACgCQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBABgBQgBAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgCgDgDAAIgEAAg");
	this.shape_2.setTransform(105.5,7.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_3.setTransform(97.275,7.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_4.setTransform(90.325,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_5.setTransform(82.95,7.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgbAlQgPgPAAgWQAAgUAOgPQAPgPAXAAQATAAAOAKIgLAcQgIgIgKABQgJgBgFAGQgFAGAAAIQAAAJAFAGQAFAGAJgBQAKAAAJgHIAKAcQgOAKgTAAQgWAAgPgOg");
	this.shape_6.setTransform(74.525,7.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_7.setTransform(66.625,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgeAxIAAhhIAjAAIAABFIAaAAIAAAcg");
	this.shape_8.setTransform(59.875,7.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_9.setTransform(52.875,7.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_10.setTransform(45.375,7.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_11.setTransform(37.875,7.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAWAAQAaAAAQAQIgTAVQgJgGgJAAQgIgBgFAGQgGAGgBAIQAAAJAGAHQAGAFAIAAQAHAAAFgDIAAgDIgLAAIAAgXIAqAAIAAArQgSAQgeAAQgXAAgOgPg");
	this.shape_12.setTransform(29.1,7.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_13.setTransform(17.325,7.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_14.setTransform(7.525,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,128.2,18), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape.setTransform(113.975,7.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgpAuIAAhbIAhAAQAWAAAOANQANAMAAAUQAAAVgNAMQgOANgWAAgAgIAUIABAAQAPgBAAgTQAAgSgPgBIgBAAg");
	this.shape_1.setTransform(106.5,7.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAFAuIgMgeIAAAeIggAAIAAhbIAnAAQAQAAAJAJQAJAIAAANQAAAHgEAHQgEAFgHADIAVAngAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAAAAAgBQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAIgEAAg");
	this.shape_2.setTransform(98.175,7.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_3.setTransform(90.525,7.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_4.setTransform(84.075,7.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgQAuIAAhBIgRAAIAAgaIBDAAIAAAaIgSAAIAABBg");
	this.shape_5.setTransform(77.225,7.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgZAiQgOgNAAgVQAAgTAOgNQANgOAVAAQARAAANAJIgJAaQgIgHgJAAQgIAAgFAFQgFAFAAAIQABAJAEAFQAFAFAIAAQAJAAAIgHIAKAaQgNAJgSAAQgUAAgOgNg");
	this.shape_6.setTransform(69.4,7.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_7.setTransform(62.075,7.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgcAuIAAhbIAgAAIAABBIAZAAIAAAag");
	this.shape_8.setTransform(55.8,7.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_9.setTransform(49.275,7.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgYAnQgIgGgDgKIAYgMQAFAKAGAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAADAGADIAMAHQASAIAAAPQAAAOgKAHQgKAIgQAAQgNAAgLgIg");
	this.shape_10.setTransform(42.275,7.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_11.setTransform(35.325,7.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AghAhQgNgMAAgVQAAgTAOgNQAOgOAUAAQAZAAAOAPIgSATQgIgGgJAAQgHAAgFAFQgFAFAAAIQAAAJAFAFQAFAFAHABQAHAAAEgDIAAgDIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_12.setTransform(27.175,7.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgjAuIAAhbIAlAAQAQAAAJAJQAJAJAAAOQAAANgJAIQgJAJgPAAIgGAAIAAAdgAgDgGIADAAQACAAACgCQACgBAAgEQAAgDgCgCQgBAAAAgBQgBAAAAAAQgBgBAAAAQgBAAAAAAIgDAAg");
	this.shape_13.setTransform(16.225,7.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgkAhQgNgMAAgVQAAgUANgMQAOgOAWAAQAXAAAOAOQANAMAAAUQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgNQgEAFAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgFQgEgGgIAAQgGAAgFAGg");
	this.shape_14.setTransform(7.125,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,119.4,17), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape.setTransform(122.525,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgsAxIAAhhIAjAAQAYAAAPANQAPANAAAWQAAAXgPANQgPANgYAAgAgJAVIABAAQAQAAAAgVQAAgUgQAAIgBAAg");
	this.shape_1.setTransform(114.45,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgiAAIAAhhIAqAAQARAAAKAJQAKAJAAAOQgBAIgEAHQgEAFgHAEIAXApgAgIgHIAEAAQADgBACgCQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBABgBQgBAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgCgDgDAAIgEAAg");
	this.shape_2.setTransform(105.5,7.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_3.setTransform(97.275,7.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_4.setTransform(90.325,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_5.setTransform(82.95,7.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgbAlQgPgPAAgWQAAgUAOgPQAPgPAXAAQATAAAOAKIgLAcQgIgIgKABQgJgBgFAGQgFAGAAAIQAAAJAFAGQAFAGAJgBQAKAAAJgHIAKAcQgOAKgTAAQgWAAgPgOg");
	this.shape_6.setTransform(74.525,7.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_7.setTransform(66.625,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgeAxIAAhhIAjAAIAABFIAaAAIAAAcg");
	this.shape_8.setTransform(59.875,7.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_9.setTransform(52.875,7.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_10.setTransform(45.375,7.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_11.setTransform(37.875,7.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAWAAQAaAAAQAQIgTAVQgJgGgJAAQgIgBgFAGQgGAGgBAIQAAAJAGAHQAGAFAIAAQAHAAAFgDIAAgDIgLAAIAAgXIAqAAIAAArQgSAQgeAAQgXAAgOgPg");
	this.shape_12.setTransform(29.1,7.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_13.setTransform(17.325,7.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_14.setTransform(7.525,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,128.2,18), null);


(lib.shape3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhjC5IAAlxIDHAAIAAFxg");
	this.shape.setTransform(25.4997,19.9747,2.55,1.0811);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-72.5,0,72.5,0).s().p("ArUDIIAAmPIWpAAIAAGPg");
	this.shape_1.setTransform(97.5051,20,0.6552,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape3, new cjs.Rectangle(0,0,145,40), null);


(lib.shape2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjHLuIAA3bIGPAAIAAXbg");
	this.shape.setTransform(20,124.9945,1,1.6666);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape2, new cjs.Rectangle(0,0,40,250), null);


(lib.shape1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");
	this.shape.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape1, new cjs.Rectangle(0,0,300,250), null);


(lib.services_button = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "11px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.textAlign = "center";
	this.dynText.lineHeight = 10;
	this.dynText.lineWidth = 76;
	this.dynText.parent = this;
	this.dynText.setTransform(40,7);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.services_button, new cjs.Rectangle(0,5,80,22.3), null);


(lib.price_old_line = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AjxAyIAAhjIHjAAIAABjg");
	this.shape.setTransform(46.0198,1,1.9008,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AjxAyIAAhjIHjAAIAABjg");
	this.shape_1.setTransform(46.0198,1,1.9008,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,92,2);


(lib.mediamarkt_logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EB1D24").s().p("AgzA2QgWgWAAggQAAgeAWgWQAVgXAeABQAfgBAVAXQAWAWAAAeQAAAfgWAXQgVAWgfgBQgeABgVgWgAgqgrQgSATAAAYQAAAaASASQASASAYAAQAZAAASgSQASgSAAgaQAAgZgSgSQgSgRgZgBQgYABgSARgAASAoIgNggIgSAAIAAAgIgRAAIAAhQIAkAAQAeAAAAAYQAAAPgPAHIAPAigAgNgFIAQAAQAPAAAAgLQAAgLgNABIgSAAg");
	this.shape.setTransform(392.5653,-32.1875,0.9958,0.9957);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AhIBeQgKAAgGgIQgHgIACgNIAaiMQABgHAGgGQAGgFAHAAIB4AAQAKAAAGAIQAGAHgBAKIgaCNQgCAIgFAHQgHAGgHAAg");
	this.shape_1.setTransform(-100.5306,-29.8475,0.9958,0.9957);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EB1D24").s().p("AN5IUQhHgPg7g+Qg6g/gOhMQgHgnABgqQgHAGgIAAIn0AAIgBAGQAAAHAHAXIACAHQARAsAoAaQAnAaAxgBQBhgCBQhbQAGgHAJAAIBwAAQALAAAGAKQAHAKgFAKQgwBmhhA+QhiA+hwAAQhNgDg/gkQg+glgkhAQglhFAChcIgxEOQgBAGgFADQgEAEgFAAIiDAAQgHAAgFgHQgDgDAAgGIAAgEQAgiGApj/QAskNAKgrQhEBQnLJ6QgFAGgHABIiGAAQgHAAgFgHQgEgEAAgGIAAgDQAeiMArj6IA2k2QhBBPooJ7QgFAFgIABIivAAQgJAAgFgLQgEgKAHgHINmv6QAFgGAHAAICJgBQAIAAAFAGQADAGAAAFIAAADQheIhgUBjQBFhPG3pDQAFgFAHAAICMAAQAIAAAEAGQAFAGgCAIIhnI8IADgHQAxhfBehCQBchABngNIACAAQBcgFBGAjQBGAjArBHIAAACQAjBGAEBUQAkhpBWhQQBIhFBXgaQBWgaBYAUIAEABIALAFQA3AYAYAWIAyj1QACgIAGgEQAFgFAIAAIBsAAQAKAAAHAIQAGAHgCAKIieNlQgDAPgSgBIhsABQgMAAgEgFQgFgFAAgIIAFgdQhpBDhkAAQgcAAgbgFgAQBgZQgqAIghAXQg6ArgfA4QgfA3AAA7QAAAXADAPQAGAeAQAZQAQAaAYARQAzAiBXgSQA8gOA0g2QA0g2ARhBQAGgXAAgZQACg0gXglQgjg5g6gOQgVgFgWAAQgTAAgTAEgAIZBTQgEglgkglQgYgUgXgIQgegKgtAEQgwAGgrAbQgvAegXAtIFDAAIAAAAg");
	this.shape_2.setTransform(-255.3606,-0.0539,0.9958,0.9957);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EB1D24").s().p("ADVHHIgKgEQgEAEgDAAIiCAAQgMAAgGgJQh+jWgrhDIgeCrIgVBpQgFAOgPAAIh1AAQgJAAgHgHQgGgIACgJIBYoWIgJABIgIABQh4ASggB5IhPGQQgCAIgFAFQgGAEgIAAIhoAAQgKAAgGgHQgHgHACgKIB3qVQABgHAGgEQAGgFAIAAIBsAAQALAAAFAHQAHAIgCAJIgDAQQAggUAkgNQAogOAkgBQAKAAAHAJIAAAAQAQhhAUheQABgIAGgFQAFgFAIAAIB3AAQAKAAAGAIQAFAGAAAHIAAAEQg0EXgRBkQA6hABhh9QAFgHAKAAICLAAQALAAAGAJQAGgJAMAAIBDAAIAji+QABgIAGgFQAGgFAIAAIB5AAQAJAAAHAIQAFAGAAAIIgjC6IB7AAQAIAAAIAHQAEAGAAAIIAAAEIgSBrQgBAIgGAFQgGAFgIAAIiDAAIhhIWQgBAHgGAFQgGAFgHAAgAAPBEIDGFJQAKhAAnjTIAojaIg7AAQgKAAgGgIQgGgHABgLIAQhSIjfEQg");
	this.shape_3.setTransform(329.764,6.0739,0.9958,0.9957);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EB1D24").s().p("Aq8IDQBKgdBFguQAjgXAUgRQB6hrAKiQQALiNhmhHQAJALAFAOQBOBgguCEQgvCHiQBHQgXALgnANQhNAahNAJQg0gigtgwQBLALBOgFQAmgCAYgEQCegfBQh3QBOh1g0hxIAAAGQAAAHgBAIQAYB9hqBeQhrBfiigMQgYgCgmgIQhMgQhGgdQggg8gRhDIgCgLQA8AvBFAkQAjASAWAIQCYA1B/g9QB8g8ANh8QgGAOgIAIQgsB1iIAZQiKAZiEhbQgXgSghgdQhAg6gsg0IhfIIQgDAPgSgBIhrAAQgMAAgFgGQgFgFAAgGIAFgdQiFBVh/gXQhHgPg7g+Qg6g/gNhMQgDgQgDgjIgCghIgwEOQgDAPgSgBIh4ABQgMAAgEgFQgGgHABgKIB4qWQABgIAGgFQAGgFAIAAIB4AAQAKAAAGAHQAGAIgBAKIgyENIACgFQAlhrBUhOQBJhFBWgaQBXgbBYAWIAPAFQA2AYAYAWQADgQAEgNIADgJQAIgRAPAAIBtAAQAKAAAHAGQAEAFAEAKQAYBFAtBLQAXAlASAYQBrB+CQAJQCNAJBFhtQgIAGgHAEIgEACQheBZiHgtQiJguhJiWQgKgWgMgmQgYhLgKhMQAmhBA2g0QgKBNAEBRQACAoAEAZQAfCiB3BQQB0BPBxg4IgQABIgKAAQh7AYhchrQhehtAMikQACgZAIgoQAQhPAehIQA6ggA9gQQguA9gjBGIgaA7Qg1CdBACCQA/CAB+AHIgRgKIgBgBQh7gmgdiMQgdiQBciLIAogzQA1g8A9gvQBNABBIAWQhJAdhEAtIg2AnQh8BtgJCSQgICPBqBFQgJgKgFgLQhVheAtiIQAuiLCThJIA+gYQBOgZBPgKQA7AlAwA0QhOgMhRAFIhDAGQigAghPB5QhPB3A5ByIgBgOIAAgJQgah/BqheQBshhCjAMIBCALQBRARBJAgQAgA9APA+IAEAUQg+gzhLgnIg+gdQiXg1h/A9Qh9A8gNB8QAHgOAJgLQAthzCHgYQCKgZCDBbIA1ArQA9A5AvBBQgCBBgTBAQgdhKgshFIgmg2Qhph6iNgMQiJgLhIBlQANgHAHgCQBfhSCDAvQCGAvBHCSIAfBUQAgBjAIBOILYtXQAGgHAGAAICZABQAHAAAFAFQADAEAAAIIAAADQhaIjgZBhQBEhOG0pFQAGgFAGAAICRgBQAHAAAFAGQAFAGgCAJIhsJMQAlhWBIhEQBJhFBWgaQBWgaBZAUIAEABIALAFQAyAUAgAeIAJgqQABgIAGgEQAGgEAHAAIBtAAQAKAAAGAGQAFAGAAAJIh5KZQgCAPgTAAIhrAAQgNAAgEgFQgEgGAAgHIAAgEIAEgZQiFBVh/gXQhHgPg7g+Qg6g/gNhMQgDgPADgZQAEgiAAgHIAAgSIgwEfQgEANgMAAIiEAAQgHAAgFgHQgEgFAAgFIABgDQAYh6AskBQApjvANg+QgqAyj1FGIjsE+QgEAFgHABIiHAAQgHAAgFgGQgDgEAAgGIAAgEQAaiHAqj1IA1ktQhBBPo0JoQgFAGgIAAIirAAQgKAAgEgKQgEgIAFgIIAAAAIAAgBIAQgaQASgjAHgjQALhMgIhRIgKhCQgeifhzhQQhxhQhwAyIAFAAQALAAAJACQB5gUBZBrQBbBsgLCiIgLBDQgRBRgfBKQg5Adg6APIgTAFQAyhAAnhLQATgmAJgZQA1idhAiDQg/h/h/gHQALAEAJAHQB6AmAdCMQAdCQhcCLQgPAVgcAgQg3A+hBAxQhIgEhHgYgAZHgaQgqAIghAXQg5ArgeA3QgfA3AAA8QAAATADATQASBDAsAeQAxAhBWgPQA7gOA1g3QA0g2AUhBQAGgUAAgeQAAgygVgmQgeg3hBgPQgVgFgVAAQgTAAgUAEgA2hgaQgqAIghAXQg5ArgeA3QgfA3AAA8QAAAXAEAPQATBEAqAdQAXAQAlAEQAmAEAsgIQA6gPAzg1QAxg1AThBQAHgWAAgcQAAgxgWgnQgeg3hBgPQgVgFgVAAQgTAAgUAEg");
	this.shape_4.setTransform(75.8694,0.0439,0.9958,0.9957);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mediamarkt_logo, new cjs.Rectangle(-400,-54,799.9,108.1), null);


(lib.m33 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape.setTransform(122.525,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgsAxIAAhhIAjAAQAYAAAPANQAPANAAAWQAAAXgPANQgPANgYAAgAgJAVIABAAQAQAAAAgVQAAgUgQAAIgBAAg");
	this.shape_1.setTransform(114.45,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgiAAIAAhhIAqAAQARAAAKAJQAKAJAAAOQgBAIgEAHQgEAFgHAEIAXApgAgIgHIAEAAQADgBACgCQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBABgBQgBAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgCgDgDAAIgEAAg");
	this.shape_2.setTransform(105.5,7.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_3.setTransform(97.275,7.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_4.setTransform(90.325,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_5.setTransform(82.95,7.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgbAlQgPgPAAgWQAAgUAOgPQAPgPAXAAQATAAAOAKIgLAcQgIgIgKABQgJgBgFAGQgFAGAAAIQAAAJAFAGQAFAGAJgBQAKAAAJgHIAKAcQgOAKgTAAQgWAAgPgOg");
	this.shape_6.setTransform(74.525,7.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_7.setTransform(66.625,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgeAxIAAhhIAjAAIAABFIAaAAIAAAcg");
	this.shape_8.setTransform(59.875,7.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_9.setTransform(52.875,7.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_10.setTransform(45.375,7.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_11.setTransform(37.875,7.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAWAAQAaAAAQAQIgTAVQgJgGgJAAQgIgBgFAGQgGAGgBAIQAAAJAGAHQAGAFAIAAQAHAAAFgDIAAgDIgLAAIAAgXIAqAAIAAArQgSAQgeAAQgXAAgOgPg");
	this.shape_12.setTransform(29.1,7.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_13.setTransform(17.325,7.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_14.setTransform(7.525,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m33, new cjs.Rectangle(0,0,128.2,18), null);


(lib.m31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAgBAAQAAAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape.setTransform(166.275,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgjAAIAAhhIArAAQARAAAKAJQAJAJAAAOQABAIgFAHQgEAFgHAEIAWApgAgIgHIAFAAQACgBABgCQABgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAgBAAAAQAAgBgBAAQAAgBgBAAQgBgDgCAAIgFAAg");
	this.shape_1.setTransform(158.2,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_2.setTransform(149.975,7.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_3.setTransform(142.6,7.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_4.setTransform(133.675,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgRAxIAAhhIAjAAIAABhg");
	this.shape_5.setTransform(126.5,7.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgiAAIAAhhIAqAAQARAAAKAJQAKAJAAAOQAAAIgFAHQgEAFgHAEIAWApgAgIgHIAEAAQAEgBABgCQAAgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgDgEAAIgEAAg");
	this.shape_6.setTransform(120.25,7.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgmAxIAAhhIApAAQAQAAAKAKQAKAIAAAPQAAAPgKAJQgKAJgQAAIgGAAIAAAfgAgDgHIADAAQACAAACgBQACgCAAgEQAAgDgCgCQgCgCgCgBIgDAAg");
	this.shape_7.setTransform(111.575,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_8.setTransform(99.575,7.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_9.setTransform(91.075,7.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_10.setTransform(79.875,7.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_11.setTransform(71.375,7.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgjAAIAAhhIArAAQARAAAKAJQAJAJAAAOQAAAIgEAHQgEAFgHAEIAXApgAgIgHIAEAAQAEgBABgCQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgDgEAAIgEAAg");
	this.shape_12.setTransform(63.85,7.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_13.setTransform(53.575,7.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgRAxIAAhFIgTAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_14.setTransform(44.15,7.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgRAxIAAhhIAjAAIAABhg");
	this.shape_15.setTransform(38.1,7.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_16.setTransform(30.875,7.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_17.setTransform(20.325,7.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAaAxIAAgvIgKAoIgfAAIgJgoIAAAvIgjAAIAAhhIAsAAIAPA3IAQg3IAsAAIAABhg");
	this.shape_18.setTransform(8.4,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m31, new cjs.Rectangle(0,0,172.5,18), null);


(lib.m12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgYAnQgIgGgDgKIAYgMQAFAKAGAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgDIgMgGQgSgJAAgPQAAgNAJgJQAKgHAOAAQAOAAAKAHQAJAHACAIIgXAMQgEgIgGAAQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAADAGADIAMAHQASAIAAAPQAAAOgKAHQgKAIgQAAQgNAAgLgIg");
	this.shape.setTransform(75.625,7.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFAuIgMgeIAAAeIggAAIAAhbIAnAAQAQAAAJAJQAJAIAAANQAAAHgEAHQgEAFgHADIAVAngAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAAAAAgBQABAAAAgBQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAIgEAAg");
	this.shape_1.setTransform(68.125,7.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_2.setTransform(60.475,7.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AghAhQgNgMAAgVQAAgTAOgNQAOgOAUAAQAZAAAOAPIgSATQgIgGgJAAQgHAAgFAFQgFAFAAAIQAAAJAFAFQAFAFAHABQAHAAAEgDIAAgDIgKAAIAAgVIAoAAIAAAnQgRAPgbAAQgWAAgOgOg");
	this.shape_3.setTransform(52.325,7.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgkAhQgNgMAAgVQAAgUANgMQAOgOAWAAQAXAAAOAOQANAMAAAUQAAAVgNAMQgOAOgXAAQgWAAgOgOgAgLgNQgEAFAAAIQAAAJAEAFQAFAGAGAAQAIAAAEgGQAEgFAAgJQAAgIgEgFQgEgGgIAAQgGAAgFAGg");
	this.shape_4.setTransform(42.425,7.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAFAuIgMgeIAAAeIggAAIAAhbIAnAAQAQAAAJAJQAJAIAAANQAAAHgEAHQgEAFgHADIAVAngAgHgHIAEAAQAAAAABAAQABAAAAgBQABAAAAAAQAAAAAAgBQABAAAAgBQABgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAgBgBQAAAAAAgBQAAAAgBAAQAAgBgBAAQgBAAAAAAIgEAAg");
	this.shape_5.setTransform(33.525,7.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgpAuIAAhbIAhAAQAWAAAOANQAOAMgBAUQAAAVgNAMQgOANgWAAgAgIAUIACAAQAOgBAAgTQAAgSgOgBIgCAAg");
	this.shape_6.setTransform(24.85,7.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAKAuIgTgrIAAArIghAAIAAhbIAiAAIASArIAAgrIAhAAIAABbg");
	this.shape_7.setTransform(13.125,7.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgbAuIAAhbIA3AAIAAAaIgXAAIAAAHIAVAAIAAAYIgVAAIAAAIIAXAAIAAAag");
	this.shape_8.setTransform(5.225,7.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m12, new cjs.Rectangle(0,0,81.6,17), null);


(lib.m03 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape.setTransform(82.425,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgiAAIAAhhIAqAAQARAAAKAJQAKAJAAAOQAAAIgFAHQgEAFgHAEIAWApgAgIgHIAEAAQAEgBABgCQAAgBAAAAQABgBAAAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgDgEAAIgEAAg");
	this.shape_1.setTransform(74.35,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAPAxIgFgPIgVAAIgFAPIgjAAIAihhIAjAAIAiBhgAAFAJIgFgTIgGATIALAAg");
	this.shape_2.setTransform(64.4,7.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgnAxIAAhhIApAAQAQAAAKAHQAKAIAAANQAAAMgKAHQAGACADAGQAEAFAAAGQAAAPgLAIQgJAIgQAAgAgFAWIAFAAQABAAAAAAQABAAABAAQAAAAABAAQAAgBABAAQACgCAAgDQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAgBAAgBQgBAAAAAAQgBgBAAAAQgBAAgBAAQAAAAgBAAIgFAAgAgFgLIAFAAQAGAAAAgGQAAgFgGAAIgFAAg");
	this.shape_3.setTransform(54.95,7.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgsAxIAAhhIAjAAQAYAAAPANQAPANAAAWQAAAXgPANQgPANgYAAgAgKAVIADAAQAPAAAAgVQAAgUgPAAIgDAAg");
	this.shape_4.setTransform(45.9,7.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_5.setTransform(35.975,7.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AggAmQgMgKAAgTIAAg6IAkAAIAAA7QAAAKAIAAQAKAAAAgKIAAg7IAjAAIAAA6QAAATgLAKQgMAMgWAAQgUAAgMgMg");
	this.shape_6.setTransform(26.025,7.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_7.setTransform(15.575,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgaAqQgIgHgEgJIAagPQAGALAGAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAgBQAAgCgFgDIgNgGQgTgKAAgQQAAgOAKgKQAKgIAQAAQAPAAALAIQAJAHADAJIgZANQgFgIgGgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAADAGADIANAGQATAKAAAQQAAAOgKAJQgKAJgSAAQgPAAgLgJg");
	this.shape_8.setTransform(6.025,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m03, new cjs.Rectangle(0,0,88.7,18), null);


(lib.label_service = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgSAeQgGgFgDgHIASgKQAFAIAEgBQAAAAABAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAAgCgEgCIgJgEQgOgHAAgMQAAgKAIgGQAHgGALgBQAKAAAIAHQAHAEACAGIgSAKQgDgGgFAAQAAAAgBAAQAAAAgBAAQAAABAAAAQAAAAAAABQAAACAEACIAKAFQANAGAAAMQAAAKgHAGQgHAHgNgBQgKABgIgHg");
	this.shape.setTransform(88.125,25);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_1.setTransform(82.775,25);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAIAjIgPghIAAAhIgZAAIAAhFIAaAAIAOAhIAAghIAZAAIAABFg");
	this.shape_2.setTransform(76.7,25);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgbAaQgKgLAAgPQAAgOAKgLQAKgLARAAQASAAAKALQALALgBAOQABAPgLALQgKAKgSAAQgRAAgKgKgAgIgKQgDAEAAAGQAAAHADAEQADAEAFAAQAGAAADgEQADgEAAgHQAAgGgDgEQgDgFgGABQgFgBgDAFg");
	this.shape_3.setTransform(69.15,25);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAHAjIAAgZIgNAAIAAAZIgaAAIAAhFIAaAAIAAAZIANAAIAAgZIAaAAIAABFg");
	this.shape_4.setTransform(61.625,25);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgbAjIAAhFIAdAAQAMAAAHAGQAHAIgBAKQAAAKgGAGQgIAHgLAAIgEAAIAAAWgAgCgEIACAAQAAAAAAgBQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQAAAAAAgBQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAAAAAIgCAAg");
	this.shape_5.setTransform(55.25,25);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgMAjIAAgxIgOAAIAAgUIA1AAIAAAUIgPAAIAAAxg");
	this.shape_6.setTransform(49.45,25);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AADAjIgIgWIAAAWIgZAAIAAhFIAeAAQAMAAAIAGQAGAHAAAKQAAAFgEAFQgCAEgFADIAQAdgAgFgFIACAAQABAAABAAQAAAAABgBQAAAAAAAAQAAAAABgBIABgDIgBgDQgBgBAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgCAAg");
	this.shape_7.setTransform(43.75,25);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_8.setTransform(36.675,25);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AASAjIAAgiIgHAdIgVAAIgHgdIAAAiIgZAAIAAhFIAgAAIAKAnIALgnIAgAAIAABFg");
	this.shape_9.setTransform(28.375,25);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgSAeQgGgFgDgHIASgKQAFAIAEgBQAAAAABAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAAgCgEgCIgJgEQgOgHAAgMQAAgKAIgGQAHgGALgBQAKAAAIAHQAHAEACAGIgSAKQgDgGgFAAQAAAAgBAAQAAAAgBAAQAAABAAAAQAAAAAAABQAAACAEACIAKAFQANAGAAAMQAAAKgHAGQgHAHgNgBQgKABgIgHg");
	this.shape_10.setTransform(20.925,25);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAIAjIgPghIAAAhIgZAAIAAhFIAZAAIAPAhIAAghIAZAAIAABFg");
	this.shape_11.setTransform(12.55,25);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_12.setTransform(6.475,25);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgSAeQgGgFgDgHIASgKQAFAIAEgBQAAAAABAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAAgCgEgCIgJgEQgOgHAAgMQAAgKAIgGQAHgGALgBQAKAAAIAHQAHAEACAGIgSAKQgDgGgFAAQAAAAgBAAQAAAAgBAAQAAABAAAAQAAAAAAABQAAACAEACIAKAFQANAGAAAMQAAAKgHAGQgHAHgNgBQgKABgIgHg");
	this.shape_13.setTransform(134.675,16);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgMAjIAAgxIgOAAIAAgUIA0AAIAAAUIgNAAIAAAxg");
	this.shape_14.setTransform(129.05,16);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_15.setTransform(123.775,16);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgVAjIAAhFIAYAAIAAAxIATAAIAAAUg");
	this.shape_16.setTransform(118.975,16);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgcAjIAAhFIAeAAQALAAAHAGQAHAFAAAJQAAAIgHAFQAEABADAFQACAEAAAEQAAAKgHAHQgHAFgMAAgAgDAQIADAAIADgBQABAAAAgBQAAAAAAgBQABAAAAgBQAAAAAAAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAIgDgBIgDAAgAgDgIIADAAQAEAAAAgDQAAgEgEgBIgDAAg");
	this.shape_17.setTransform(113.425,16);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_18.setTransform(106.575,16);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgMAjIAAgxIgNAAIAAgUIA0AAIAAAUIgPAAIAAAxg");
	this.shape_19.setTransform(100.1,16);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAHAjIgOghIAAAhIgZAAIAAhFIAaAAIAOAhIAAghIAZAAIAABFg");
	this.shape_20.setTransform(91.8,16);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_21.setTransform(84.525,16);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgMAjIgZhFIAbAAIALAoIALgoIAaAAIgZBFg");
	this.shape_22.setTransform(76.925,16);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_23.setTransform(68.675,16);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgMAjIAAhFIAZAAIAABFg");
	this.shape_24.setTransform(64.675,16);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgMAjIAAgxIgOAAIAAgUIA0AAIAAAUIgOAAIAAAxg");
	this.shape_25.setTransform(60.35,16);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_26.setTransform(53.875,16);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AAEAjIgJgWIAAAWIgZAAIAAhFIAeAAQAMAAAIAGQAGAHAAAKQAAAFgDAFQgDAEgFADIAQAdgAgFgFIACAAQABAAABAAQAAAAABgBQAAAAAAAAQAAAAABgBIABgDIgBgDQgBgBAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgCAAg");
	this.shape_27.setTransform(47.25,16);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_28.setTransform(40.175,16);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgbAjIAAhFIAdAAQAMAAAHAGQAHAIgBAKQAAAKgGAGQgIAHgLAAIgEAAIAAAWgAgCgEIACAAQAAAAAAgBQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQAAAAAAgBQABAAAAgBQAAAAgBgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAAAAAIgCAAg");
	this.shape_29.setTransform(33.65,16);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_30.setTransform(28.125,16);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AAEAjIgJgWIAAAWIgZAAIAAhFIAeAAQAMAAAHAGQAHAHAAAKQAAAFgDAFQgDAEgFADIAQAdgAgFgFIADAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAAAAAgBIABgDIgBgDQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBAAAAAAIgDAAg");
	this.shape_31.setTransform(22.75,16);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgSAiIADgUIAFABQAEAAABgEIAAgtIAYAAIAAAxQAAAUgXAAQgHAAgHgBg");
	this.shape_32.setTransform(15.3,16.025);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgMAjIAAhFIAZAAIAABFg");
	this.shape_33.setTransform(11.775,16);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgcAjIAAhFIAeAAQALAAAHAGQAHAFAAAJQAAAIgHAFQAEABADAFQACAEAAAEQAAAKgHAHQgHAFgMAAgAgDAQIADAAIADgBQABAAAAgBQAAAAAAgBQABAAAAgBQAAAAAAAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAIgDgBIgDAAgAgDgIIADAAQAEAAAAgDQAAgEgEgBIgDAAg");
	this.shape_34.setTransform(7.175,16);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E31E26").s().p("AADAjIgJgWIAAAWIgYAAIAAhFIAeAAQAMAAAHAGQAHAHAAAKQAAAFgEAFQgCAEgFADIAQAdgAgGgFIADAAQABAAABAAQAAAAABgBQAAAAAAAAQAAAAABgBIAAgDIAAgDQgBgBAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgDAAg");
	this.shape_35.setTransform(130.8,7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E31E26").s().p("AgcAaQgKgLABgPQgBgOAKgLQALgLARAAQASAAAKALQAKALAAAOQAAAPgKALQgKAKgSAAQgRAAgLgKgAgIgKQgDAEAAAGQAAAHADAEQADAEAFABQAFgBAEgEQADgEAAgHQAAgGgDgEQgEgFgFABQgFgBgDAFg");
	this.shape_36.setTransform(123.45,7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#E31E26").s().p("AgMAjIAAgxIgNAAIAAgUIA0AAIAAAUIgPAAIAAAxg");
	this.shape_37.setTransform(116.75,7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#E31E26").s().p("AgTAaQgLgKAAgQQAAgOAKgLQALgKAQgBQANAAAKAHIgHAUQgGgFgHAAQgGAAgEAEQgDAEAAAGQAAAHADAEQAEAEAGAAQAHAAAGgGIAIAVQgKAGgOAAQgQAAgKgKg");
	this.shape_38.setTransform(110.725,7);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#E31E26").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAHIASAAIAAATg");
	this.shape_39.setTransform(105.075,7);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#E31E26").s().p("AgMAjIAAgxIgOAAIAAgUIA1AAIAAAUIgPAAIAAAxg");
	this.shape_40.setTransform(99.85,7);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#E31E26").s().p("AgbAaQgKgLAAgPQAAgOAKgLQALgLAQAAQASAAAKALQALALgBAOQABAPgLALQgKAKgSAAQgQAAgLgKgAgIgKQgDAEAAAGQAAAHADAEQADAEAFABQAGgBADgEQADgEAAgHQAAgGgDgEQgDgFgGABQgFgBgDAFg");
	this.shape_41.setTransform(93.1,7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#E31E26").s().p("AADAjIgJgWIAAAWIgYAAIAAhFIAeAAQAMAAAHAGQAHAHAAAKQAAAFgEAFQgCAEgFADIAQAdgAgGgFIADAAQABAAABAAQAAAAABgBQAAAAAAAAQAAAAABgBIABgDIgBgDQgBgBAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgDAAg");
	this.shape_42.setTransform(86.25,7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#E31E26").s().p("AgaAjIAAhFIAcAAQAMAAAHAGQAGAIAAAKQABAKgIAGQgGAHgMAAIgEAAIAAAWgAgCgEIACAAQAAAAAAgBQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAAAAAIgCAAg");
	this.shape_43.setTransform(80.05,7);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#E31E26").s().p("AAIAjIgPghIAAAhIgZAAIAAhFIAZAAIAPAhIAAghIAZAAIAABFg");
	this.shape_44.setTransform(73.4,7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#E31E26").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAHIASAAIAAATg");
	this.shape_45.setTransform(67.325,7);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#E31E26").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAHIASAAIAAATg");
	this.shape_46.setTransform(62.375,7);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#E31E26").s().p("AADAjIgJgWIAAAWIgYAAIAAhFIAeAAQAMAAAIAGQAGAHAAAKQAAAFgEAFQgCAEgFADIAQAdgAgGgFIADAAQABAAABAAQAAAAABgBQAAAAAAAAQAAAAABgBIAAgDIAAgDQgBgBAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgDAAg");
	this.shape_47.setTransform(57,7);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#E31E26").s().p("AgTAaQgLgKAAgQQAAgOAKgLQALgKAQgBQANAAAKAHIgHAUQgGgFgHAAQgGAAgEAEQgDAEAAAGQAAAHADAEQAEAEAGAAQAHAAAGgGIAIAVQgKAGgOAAQgQAAgKgKg");
	this.shape_48.setTransform(50.375,7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#E31E26").s().p("AgSAeQgGgFgDgHIASgKQAFAIAEAAQAAgBABAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAAgCgEgCIgJgEQgOgHAAgMQAAgKAIgGQAHgGALgBQAKAAAIAHQAHAEACAGIgSAKQgDgGgFAAQAAAAgBAAQAAAAgBAAQAAABAAAAQAAAAAAABQAAACAEACIAKAFQANAGAAAMQAAAKgHAGQgHAHgNgBQgKABgIgHg");
	this.shape_49.setTransform(44.325,7);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#E31E26").s().p("AgSAeQgGgFgDgHIASgKQAFAIAEAAQAAgBABAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAAgCgEgCIgJgEQgOgHAAgMQAAgKAIgGQAHgGALgBQAKAAAIAHQAHAEACAGIgSAKQgDgGgFAAQAAAAgBAAQAAAAgBAAQAAABAAAAQAAAAAAABQAAACAEACIAKAFQANAGAAAMQAAAKgHAGQgHAHgNgBQgKABgIgHg");
	this.shape_50.setTransform(36.675,7);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#E31E26").s().p("AgMAjIAAhFIAZAAIAABFg");
	this.shape_51.setTransform(32.275,7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#E31E26").s().p("AgMAjIAAgxIgOAAIAAgUIA1AAIAAAUIgPAAIAAAxg");
	this.shape_52.setTransform(27.95,7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#E31E26").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAHIgDgOIgEAOIAHAAg");
	this.shape_53.setTransform(21.475,7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#E31E26").s().p("AAEAjIgJgWIAAAWIgZAAIAAhFIAeAAQAMAAAIAGQAGAHAAAKQAAAFgDAFQgDAEgFADIAQAdgAgFgFIACAAQABAAABAAQAAAAABgBQAAAAAAAAQAAAAABgBIABgDIgBgDQgBgBAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgCAAg");
	this.shape_54.setTransform(14.85,7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#E31E26").s().p("AgZAaQgKgKAAgQQAAgOAKgLQALgKAQgBQATAAALAMIgPAPQgFgFgHAAQgFAAgEAEQgEAEAAAGQAAAHADAEQAEAEAGAAQAFAAADgBIAAgEIgIAAIAAgPIAfAAIAAAeQgNALgVAAQgQAAgLgKg");
	this.shape_55.setTransform(7.675,7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("AgRAJIAAgSIAjAAIAAASg");
	this.shape_56.setTransform(122.475,21.85);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AgJAJQgEgEAAgFQAAgEAEgEQAEgEAFAAQAGAAAEAEQAEAEAAAEQAAAFgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_57.setTransform(118.775,24.125);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AgaAiIAJgTQAFADAFAAQAHAAABgIQgDADgFAAQgKAAgGgGQgGgHAAgLQAAgMAIgIQAIgIANAAQAeAAAAAkQAAARgHALQgJAPgTAAQgMAAgJgGgAgDgQIgBAFIABAEQABABAAAAQAAABABAAQAAAAABAAQAAABAAAAIADgBQABgMgEgBQgBAAAAABQAAAAgBAAQAAAAAAABQgBAAAAAAg");
	this.shape_58.setTransform(114.075,21.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AgVAgQgGgGgCgJIAXgFQABAFAEAAQAEAAAAgFQAAgBAAAAQAAgBAAgBQAAAAgBAAQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAgBAAAAQAAAAgBAAQgDAAgBADIgWgDIACgsIAyAAIAAAVIgcAAIAAAGIAGgBQAMAAAHAGQAIAHAAALQAAALgIAIQgJAIgNAAQgMAAgJgHg");
	this.shape_59.setTransform(107.725,21.475);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AgXAnIAAhNIAvAAIAAAWIgUAAIAAAJIASAAIAAAUIgSAAIAAAag");
	this.shape_60.setTransform(99.925,21.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AAMAnIgEgNIgRAAIgDANIgcAAIAbhNIAaAAIAbBNgAAEAHIgEgPIgEAPIAIAAg");
	this.shape_61.setTransform(93.15,21.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_62.setTransform(85.125,21.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AALAnIgDgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAAEAHIgEgPIgEAPIAIAAg");
	this.shape_63.setTransform(77.1,21.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("AgNAnIgchNIAeAAIALAsIANgsIAdAAIgcBNg");
	this.shape_64.setTransform(68.75,21.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#E31E26").s().p("AgNAnIAAg3IgPAAIAAgWIA5AAIAAAWIgPAAIAAA3g");
	this.shape_65.setTransform(59.325,21.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#E31E26").s().p("AAEAnIgKgZIAAAZIgbAAIAAhNIAhAAQANAAAIAIQAHAGAAAMQAAAGgDAFQgEAEgEADIARAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_66.setTransform(53.05,21.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#E31E26").s().p("AgeAcQgLgLAAgRQAAgQALgMQALgLATAAQATAAAMALQALAMAAAQQAAARgLALQgLAMgUAAQgTAAgLgMgAgJgLQgDAFgBAGQABAHADAFQAEAFAFAAQAGAAAEgFQADgFAAgHQAAgGgDgFQgEgFgGAAQgFAAgEAFg");
	this.shape_67.setTransform(45,21.4);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#E31E26").s().p("AgdAnIAAhNIAfAAQANABAIAHQAHAHAAAMQAAALgHAHQgIAHgNABIgFAAIAAAYgAgDgFIADAAQAAAAAAAAQAAAAABAAQAAgBABAAQAAAAABgBQAAAAABAAQAAgBAAAAQABgBAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAAAgBgBQAAAAAAAAQgBAAgBAAQAAgBAAAAIgDAAg");
	this.shape_68.setTransform(37.575,21.4);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#E31E26").s().p("AgdAnIAAhNIAfAAQANABAIAHQAHAHAAAMQAAALgHAHQgIAHgNABIgFAAIAAAYgAgDgFIADAAQAAAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAABAAQAAgBAAAAQAAgBABgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBAAAAIgDAAg");
	this.shape_69.setTransform(31.125,21.4);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#E31E26").s().p("AgaAeQgJgIAAgOIAAguIAcAAIAAAuQAAAIAHAAQAIAAAAgIIAAguIAbAAIAAAuQAAAOgJAIQgJAJgRAAQgQAAgKgJg");
	this.shape_70.setTransform(23.9,21.475);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#E31E26").s().p("AgUAhQgGgFgEgIIAVgLQAFAIAEAAQAAAAABAAQAAAAAAAAQABAAAAgBQAAAAAAgBQAAgCgEgCIgKgFQgPgHAAgNQAAgMAIgGQAIgHAMAAQAMAAAIAHQAIAFABAGIgTALQgEgGgEgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAABAFADIAKAGQAPAHAAAMQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape_71.setTransform(16.85,21.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#E31E26").s().p("AAOAiQgIAEgMAAQgOAAgIgHQgIgHAAgLQAAgNAMgHQgEgGAAgGQAAgKAHgFQAHgGALgBQALAAAIAHQAGAGAAAJQAAALgMAIIAFADIABgDIAUAAQAAALgGAIIAHAHIgPAQgAgLAMQgBAEADABQABAAAAABQABAAAAAAQABAAAAAAQABAAABAAIAEAAIgJgJQgBAAAAAAQgBABAAAAQAAABAAAAQAAABAAAAgAgGgSQABADADACQACgBAAgEQAAgBAAgBQAAAAAAgBQgBAAAAAAQgBgBAAAAQgBAAgBABQAAAAgBAAQAAABAAAAQgBABAAABg");
	this.shape_72.setTransform(7.8,21.55);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#E31E26").s().p("AgiAmIAAhLIAbAAQATgBALALQAMAKAAARQAAARgMALQgLALgTgBgAgHARIABAAQANAAAAgRQAAgQgNAAIgBAAg");
	this.shape_73.setTransform(104.975,11.4);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#E31E26").s().p("AgaAeQgJgIAAgOIAAguIAcAAIAAAuQAAAIAHAAQAIAAAAgIIAAguIAbAAIAAAuQAAAOgJAIQgJAJgRAAQgQAAgKgJg");
	this.shape_74.setTransform(97.25,11.475);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#E31E26").s().p("AgeAcQgMgLAAgRQAAgQAMgLQAMgMASAAQATAAAMAMQAMALAAAQQAAARgMALQgLAMgUAAQgSAAgMgMgAgJgLQgEAFAAAGQAAAHAEAFQAEAFAFAAQAGAAAEgFQADgFAAgHQAAgGgDgFQgEgFgGAAQgFAAgEAFg");
	this.shape_75.setTransform(89.05,11.4);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#E31E26").s().p("AAIAmIAAgbIgPAAIAAAbIgcAAIAAhLIAcAAIAAAbIAPAAIAAgbIAcAAIAABLg");
	this.shape_76.setTransform(80.8,11.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#E31E26").s().p("AAEAmIgKgYIAAAYIgbAAIAAhLIAhAAQANgBAIAIQAHAGAAAMQAAAGgDAFQgDAEgFAEIARAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_77.setTransform(73.65,11.4);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#E31E26").s().p("AgXAmIAAhLIAvAAIAAAVIgUAAIAAAGIASAAIAAAUIgSAAIAAAHIAUAAIAAAVg");
	this.shape_78.setTransform(67.2,11.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#E31E26").s().p("AgiAmIAAhLIAbAAQATgBALALQAMAKAAARQAAARgMALQgLALgTgBgAgHARIABAAQANAAAAgRQAAgQgNAAIgBAAg");
	this.shape_79.setTransform(60.875,11.4);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#E31E26").s().p("AAIAmIgQgkIAAAkIgbAAIAAhLIAcAAIAQAkIAAgkIAbAAIAABLg");
	this.shape_80.setTransform(53.075,11.4);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#E31E26").s().p("AgeAcQgMgLABgRQgBgQAMgLQALgMATAAQATAAAMAMQALALAAAQQAAARgLALQgMAMgTAAQgTAAgLgMgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQADgFABgHQgBgGgDgFQgEgFgGAAQgGAAgDAFg");
	this.shape_81.setTransform(44.8,11.4);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#000000").s().p("AAEAmIgKgYIAAAYIgbAAIAAhLIAhAAQANgBAIAIQAHAGAAAMQABAGgEAFQgEAEgEAEIARAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_82.setTransform(35.15,11.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#000000").s().p("AAMAmIgEgMIgRAAIgDAMIgbAAIAahLIAbAAIAbBLgAAEAHIgEgPIgEAPIAIAAg");
	this.shape_83.setTransform(27.35,11.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#000000").s().p("AALAmIgCgMIgRAAIgEAMIgcAAIAbhLIAaAAIAbBLgAADAHIgDgPIgEAPIAHAAg");
	this.shape_84.setTransform(19.2,11.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#000000").s().p("AgUAlIADgVIAGABQAFAAAAgEIAAgzIAbAAIAAA3QAAAWgZAAQgIAAgIgCg");
	this.shape_85.setTransform(12.7,11.425);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#000000").s().p("AgGAmIAAgzIgHABIgFgUIASgFIATAAIAABLg");
	this.shape_86.setTransform(6.15,11.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#E31E26").s().p("AgWAlIAAhJIAtAAIAAAVIgTAAIAAAGIARAAIAAASIgRAAIAAAHIATAAIAAAVg");
	this.shape_87.setTransform(83.05,21.775);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#E31E26").s().p("AgMAlIAAg0IgPAAIAAgVIA3AAIAAAVIgPAAIAAA0g");
	this.shape_88.setTransform(77.525,21.775);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#E31E26").s().p("AAIAlIgPgjIAAAjIgbAAIAAhJIAbAAIAPAjIAAgjIAbAAIAABJg");
	this.shape_89.setTransform(70.825,21.775);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#E31E26").s().p("AgWAlIAAhJIAtAAIAAAVIgUAAIAAAGIASAAIAAASIgSAAIAAAHIAUAAIAAAVg");
	this.shape_90.setTransform(64.45,21.775);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#E31E26").s().p("AAEAlIgKgYIAAAYIgaAAIAAhJIAhAAQAMAAAHAHQAIAHAAAKQgBAGgDAFQgDAEgFADIAQAfgAgGgGIADAAQABAAABAAQAAAAABAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAAAQAAAAAAAAQgBgBAAAAQgBAAgBAAIgDAAg");
	this.shape_91.setTransform(58.8,21.775);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#E31E26").s().p("AAKAhQgGgGAAgIQAAgIAGgFQAGgGAJAAQAJAAAGAGQAFAFAAAIQAAAIgFAGQgGAFgJAAQgJAAgGgFgAAWATQAAABAAABQABAAAAABQAAAAABABQAAAAABAAQAAAAABAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBAAAAAAQgBAAAAAAQgBAAAAABQAAAAgBABQAAAAAAABgAgkAlIA1hJIAUAAIg1BJgAgmgFQgGgFAAgIQAAgIAGgGQAFgFAJAAQAJAAAGAFQAGAGAAAIQAAAIgGAFQgGAFgJAAQgJAAgFgFgAgagSQAAABAAAAQAAABAAAAQABABAAAAQABAAAAAAQABAAAAAAQABAAAAgBQAAAAABgBQAAAAAAgBQAAgBAAgBQgBAAAAgBQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAABgBAAQAAABAAAAQAAABAAABg");
	this.shape_92.setTransform(48.575,21.775);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#E31E26").s().p("AgcAAQAAglAcAAQAdAAAAAlQAAAmgdAAQgcAAAAgmgAgDAAQAAARADAAQADAAAAgRQAAgQgDAAQgDAAAAAQg");
	this.shape_93.setTransform(40.775,21.775);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#E31E26").s().p("AAIAlIgPgjIAAAjIgbAAIAAhJIAbAAIAPAjIAAgjIAbAAIAABJg");
	this.shape_94.setTransform(31.825,21.775);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#E31E26").s().p("AgWAlIAAhJIAtAAIAAAVIgTAAIAAAGIARAAIAAASIgRAAIAAAHIATAAIAAAVg");
	this.shape_95.setTransform(25.45,21.775);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#E31E26").s().p("AgaAbQgLgKAAgRQAAgPALgLQALgLARAAQATAAAMAMIgPAQQgGgGgHAAQgGAAgEAFQgEAEAAAGQAAAHAEAFQAEAEAGAAQAGAAADgCIAAgDIgJAAIAAgRIAhAAIAAAgQgOAMgWAAQgRAAgLgLg");
	this.shape_96.setTransform(18.925,21.775);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#E31E26").s().p("AgWAlIAAhJIAtAAIAAAVIgUAAIAAAGIASAAIAAASIgSAAIAAAHIAUAAIAAAVg");
	this.shape_97.setTransform(12.45,21.775);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#E31E26").s().p("AgMAlIAAg0IgPAAIAAgVIA3AAIAAAVIgPAAIAAA0g");
	this.shape_98.setTransform(6.925,21.775);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#000000").s().p("AAIAlIgPgjIAAAjIgbAAIAAhJIAbAAIAPAjIAAgjIAbAAIAABJg");
	this.shape_99.setTransform(135.325,12.225);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#000000").s().p("AgWAlIAAhJIAtAAIAAAVIgTAAIAAAGIARAAIAAASIgRAAIAAAHIATAAIAAAVg");
	this.shape_100.setTransform(128.95,12.225);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#000000").s().p("AghAlIAAhJIAbAAQARAAALAKQAMAKAAAQQAAARgMAKQgLAKgRAAgAgHAQIACAAQAMAAAAgQQAAgPgMAAIgCAAg");
	this.shape_101.setTransform(122.925,12.225);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#000000").s().p("AAIAlIgPgjIAAAjIgbAAIAAhJIAbAAIAPAjIAAgjIAbAAIAABJg");
	this.shape_102.setTransform(115.475,12.225);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#000000").s().p("AAMAlIgEgLIgQAAIgDALIgbAAIAahJIAZAAIAaBJgAAEAHIgEgOIgEAOIAIAAg");
	this.shape_103.setTransform(107.8,12.225);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#000000").s().p("AAMAlIgEgLIgQAAIgEALIgaAAIAahJIAZAAIAaBJgAADAHIgDgOIgEAOIAHAAg");
	this.shape_104.setTransform(100,12.225);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#000000").s().p("AATAlIAAgkIgHAeIgWAAIgIgeIAAAkIgaAAIAAhJIAiAAIAKApIALgpIAiAAIAABJg");
	this.shape_105.setTransform(91.3,12.225);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#000000").s().p("AgcAAQAAglAcAAQAdAAAAAlQAAAmgdAAQgcAAAAgmgAgDAAQAAARADAAQADAAAAgRQAAgQgDAAQgDAAAAAQg");
	this.shape_106.setTransform(81.375,12.225);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#000000").s().p("AgGAlIAAgyIgGACIgGgTIARgGIAUAAIAABJg");
	this.shape_107.setTransform(76.075,12.225);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#000000").s().p("AAIAlIgPgjIAAAjIgbAAIAAhJIAbAAIAPAjIAAgjIAbAAIAABJg");
	this.shape_108.setTransform(68.225,12.225);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#000000").s().p("AgNAlIAAhJIAaAAIAABJg");
	this.shape_109.setTransform(62.85,12.225);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#000000").s().p("AgYAdQgJgIAAgOIAAgrIAbAAIAAAsQAAAHAGABQAHgBABgHIAAgsIAaAAIAAArQAAAOgJAIQgJAJgQgBQgPABgJgJg");
	this.shape_110.setTransform(55.5,12.3);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#000000").s().p("AAIAlIgPgjIAAAjIgbAAIAAhJIAbAAIAPAjIAAgjIAbAAIAABJg");
	this.shape_111.setTransform(48.025,12.225);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#000000").s().p("AgXAlIAAhJIAaAAIAAA0IAUAAIAAAVg");
	this.shape_112.setTransform(39.8,12.225);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#000000").s().p("AALAlIgDgLIgQAAIgDALIgaAAIAZhJIAZAAIAZBJgAADAHIgDgOIgEAOIAHAAg");
	this.shape_113.setTransform(33.25,12.225);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#000000").s().p("AAMAlIgEgLIgQAAIgDALIgaAAIAZhJIAZAAIAaBJgAAEAHIgEgOIgEAOIAIAAg");
	this.shape_114.setTransform(25.45,12.225);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#000000").s().p("AgMAlIAAg0IgPAAIAAgVIA3AAIAAAVIgPAAIAAA0g");
	this.shape_115.setTransform(18.625,12.225);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#000000").s().p("AgWAlIAAhJIAtAAIAAAVIgUAAIAAAGIASAAIAAASIgSAAIAAAHIAUAAIAAAVg");
	this.shape_116.setTransform(13.1,12.225);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#000000").s().p("AgdAlIAAhJIAfAAQAMAAAIAGQAHAGAAAJQAAAJgIAFQAFACACAEQACAEAAAFQAAALgHAGQgHAGgNAAgAgDARIADAAQABAAAAAAQABAAAAAAQABAAAAgBQAAAAABAAQAAgBAAAAQABAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAAAgBAAQAAgBgBAAQAAAAgBAAIgDAAgAgDgIIADAAQAEAAAAgEQAAgEgEAAIgDAAg");
	this.shape_117.setTransform(7.325,12.225);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#E31E26").s().p("AgRAJIAAgRIAjAAIAAARg");
	this.shape_118.setTransform(136.075,22.2);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#E31E26").s().p("AgMALQAGgEACgFQgIgCAAgIQAAgFADgEQAEgDAFAAQAGAAADADQAEAEAAAGQAAAHgEAIQgFAIgGAFg");
	this.shape_119.setTransform(132.575,25.275);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#E31E26").s().p("AgcAAQAAglAcAAQAdAAAAAlQAAAmgdAAQgcAAAAgmgAgDAAQAAARADAAQADAAAAgRQAAgQgDAAQgDAAAAAQg");
	this.shape_120.setTransform(121.725,21.775);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#E31E26").s().p("AgbAmIAAgJQAAgIAFgHQADgEAHgGIAKgIQAEgFAAgEQAAAAgBgBQAAAAAAgBQgBAAAAAAQAAAAgBAAQgDAAgBAFIgXgEQADgJAFgGQAIgIAMAAQAMABAIAFQAIAHAAALQAAALgMAJIgJAGQgDADAAACIAZAAIAAAUg");
	this.shape_121.setTransform(115.55,21.7);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#E31E26").s().p("AgKAgQgKgHgEgLIgIAAIAAgMIAFAAIAAgCIgFAAIAAgOIAIAAQAEgKAKgGQAKgHALAAQANAAAIAFIgIARQgFgCgGAAQgGAAgFADIARAAIgCAOIgTAAIAAAAIAAACIATAAIgBAMIgNAAQADAEAHAAQAHAAAFgEIAIASQgHAGgOAAQgMAAgKgGg");
	this.shape_122.setTransform(108.775,21.775);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#E31E26").s().p("AgdAbQgLgLAAgQQAAgPALgLQAMgLARAAQATAAAKALQAMALgBAPQABAQgMALQgKALgTAAQgRAAgMgLgAgIgLQgEAFAAAGQAAAHAEAFQADAEAFAAQAGAAAEgEQADgFAAgHQAAgGgDgFQgEgEgGAAQgFAAgDAEg");
	this.shape_123.setTransform(93.35,21.775);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#E31E26").s().p("AgMAlIAAg0IgPAAIAAgVIA3AAIAAAVIgPAAIAAA0g");
	this.shape_124.setTransform(78.425,21.775);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#E31E26").s().p("AAEAlIgJgYIAAAYIgaAAIAAhJIAgAAQAMAAAIAHQAGAHABAKQAAAGgEAFQgDAEgFADIAQAfgAgFgGIACAAQABAAABAAQAAAAABAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBAAAAQgBgBAAAAQAAAAAAAAQgBgBAAAAQgBAAgBAAIgCAAg");
	this.shape_125.setTransform(72.45,21.775);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#E31E26").s().p("AAMAlIgEgLIgQAAIgEALIgaAAIAahJIAZAAIAZBJgAADAHIgDgOIgEAOIAHAAg");
	this.shape_126.setTransform(65,21.775);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#E31E26").s().p("AALAlIgDgLIgQAAIgDALIgaAAIAZhJIAZAAIAZBJgAADAHIgDgOIgEAOIAHAAg");
	this.shape_127.setTransform(57.2,21.775);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#E31E26").s().p("AAFAlIgMgeIAAAeIgbAAIAAhJIAbAAIAAAbIAMgbIAdAAIgSAjIASAmg");
	this.shape_128.setTransform(49.75,21.775);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#E31E26").s().p("AgYAdQgJgIAAgOIAAgsIAbAAIAAAtQAAAHAGAAQAIAAgBgHIAAgtIAbAAIAAAsQAAAOgJAIQgJAJgQgBQgPABgJgJg");
	this.shape_129.setTransform(42.3,21.85);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#E31E26").s().p("AALAlIgDgLIgQAAIgDALIgaAAIAZhJIAZAAIAZBJgAADAHIgDgOIgEAOIAHAAg");
	this.shape_130.setTransform(34.7,21.775);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#E31E26").s().p("AghAlIAAhJIAbAAQARAAALAKQAMAKAAAQQAAARgMAKQgLAKgRAAgAgHAQIACAAQAMAAAAgQQAAgPgMAAIgCAAg");
	this.shape_131.setTransform(22.175,21.775);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#E31E26").s().p("AAMAlIgEgLIgQAAIgEALIgaAAIAahJIAZAAIAZBJgAADAHIgDgOIgEAOIAHAAg");
	this.shape_132.setTransform(14.6,21.775);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#E31E26").s().p("AgUAcQgLgLAAgRQAAgPAKgLQAMgLAQAAQAOAAALAHIgIAVQgGgGgIAAQgGAAgEAFQgEAEAAAGQAAAHAEAEQAEAFAGAAQAIAAAGgGIAIAVQgKAHgPAAQgQAAgLgKg");
	this.shape_133.setTransform(7.325,21.775);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#000000").s().p("AgWAlIAAhJIAtAAIAAAVIgTAAIAAAGIARAAIAAASIgRAAIAAAHIATAAIAAAVg");
	this.shape_134.setTransform(61.55,12.225);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#000000").s().p("AgaAbQgLgKAAgRQAAgPALgLQALgLARAAQATAAAMAMIgPAQQgGgGgHAAQgGAAgEAFQgEAEAAAGQAAAHAEAFQAEAEAGAAQAGAAADgCIAAgDIgJAAIAAgRIAhAAIAAAgQgOAMgWAAQgRAAgLgLg");
	this.shape_135.setTransform(53.025,12.225);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#000000").s().p("AgMAlIgbhJIAdAAIALAqIALgqIAcAAIgaBJg");
	this.shape_136.setTransform(29.725,12.225);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#000000").s().p("AgdAbQgLgLABgQQgBgPALgLQALgLASAAQATAAAKALQAMALAAAPQAAAQgMALQgKALgTAAQgSAAgLgLgAgJgLQgDAFAAAGQAAAHADAFQAEAEAFAAQAGAAADgEQAEgFAAgHQAAgGgEgFQgDgEgGAAQgFAAgEAEg");
	this.shape_137.setTransform(8.15,12.225);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#000000").s().p("AgMALQAGgDACgFQgJgDAAgIQAAgGAEgEQAEgDAFAAQAGAAAEADQAEAEAAAHQAAAHgFAJQgEAIgHAFg");
	this.shape_138.setTransform(55.475,25.075);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#000000").s().p("AgaAiIAJgTQAFADAFAAQAHAAABgIQgDADgFAAQgKAAgGgGQgGgHAAgLQAAgMAIgIQAIgIANAAQAeAAAAAkQAAARgHALQgJAPgTAAQgMAAgJgGgAgDgQIgBAFIABAEQABABAAAAQAAABABAAQAAAAABAAQAAABAAAAIADgBQABgMgEgBQgBAAAAABQAAAAgBAAQAAAAAAABQgBAAAAAAg");
	this.shape_139.setTransform(50.775,21.4);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#000000").s().p("AgcAnIAAgJQAAgIAFgHQADgFAHgGIALgIQAEgFAAgEQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAAAAAQgEAAgCAGIgXgFQACgKAGgGQAIgHANAAQANAAAHAGQAJAHAAALQAAAMgMAJIgJAHQgEADAAABIAaAAIAAAVg");
	this.shape_140.setTransform(44.4,21.325);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#000000").s().p("AgOAnIgbhNIAeAAIALAsIAMgsIAeAAIgcBNg");
	this.shape_141.setTransform(31.9,21.4);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#000000").s().p("AgJAJQgEgEAAgFQAAgEAEgEQAEgEAFAAQAGAAAEAEQAEAEAAAEQAAAFgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_142.setTransform(26.125,24.125);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#000000").s().p("AAJAnIgJgoIgIAoIgcAAIgUhNIAdAAIAHAqIAKgqIAVAAIAJAqIAIgqIAdAAIgUBNg");
	this.shape_143.setTransform(18.875,21.4);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#000000").s().p("AgJAJQgEgEAAgFQAAgEAEgEQAEgEAFAAQAGAAAEAEQAEAEAAAEQAAAFgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_144.setTransform(11.575,24.125);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#000000").s().p("AgNAnIAAg3IgPAAIAAgWIA5AAIAAAWIgPAAIAAA3g");
	this.shape_145.setTransform(7.075,21.4);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#E31E26").s().p("AAEAmIgKgYIAAAYIgbAAIAAhLIAhAAQANgBAIAIQAHAGAAAMQABAGgEAFQgEAEgEAEIARAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_146.setTransform(135.5,11.4);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#E31E26").s().p("AgeAcQgLgLAAgRQAAgQALgLQAMgMASAAQATAAAMAMQALALAAAQQAAARgLALQgLAMgUAAQgSAAgMgMgAgJgLQgDAFgBAGQABAHADAFQAEAFAFAAQAGAAAEgFQADgFAAgHQAAgGgDgFQgEgFgGAAQgFAAgEAFg");
	this.shape_147.setTransform(127.45,11.4);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#E31E26").s().p("AgNAmIAAg2IgPAAIAAgVIA5AAIAAAVIgPAAIAAA2g");
	this.shape_148.setTransform(120.075,11.4);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#E31E26").s().p("AgVAdQgMgLAAgSQAAgQALgLQAMgMASAAQAPAAAKAIIgIAVQgGgFgIgBQgHAAgEAFQgEAFAAAGQAAAHAEAFQAEAEAHABQAIgBAHgGIAIAXQgLAHgPAAQgSAAgLgLg");
	this.shape_149.setTransform(113.425,11.4);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#E31E26").s().p("AgNAmIAAg2IgPAAIAAgVIA5AAIAAAVIgPAAIAAA2g");
	this.shape_150.setTransform(101.475,11.4);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#E31E26").s().p("AgeAcQgMgLAAgRQAAgQAMgLQALgMATAAQATAAAMAMQAMALAAAQQAAARgMALQgMAMgTAAQgTAAgLgMgAgJgLQgDAFAAAGQAAAHADAFQAEAFAFAAQAGAAAEgFQAEgFAAgHQAAgGgEgFQgEgFgGAAQgFAAgEAFg");
	this.shape_151.setTransform(94.1,11.4);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#E31E26").s().p("AAEAmIgKgYIAAAYIgbAAIAAhLIAhAAQANgBAIAIQAIAGgBAMQABAGgEAFQgEAEgFAEIASAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_152.setTransform(86.55,11.4);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#E31E26").s().p("AgdAmIAAhLIAfAAQANAAAIAHQAHAHAAAMQAAALgHAHQgIAIgNAAIgFAAIAAAXgAgDgFIADAAQAAAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAABAAQAAgBAAAAQAAgBABgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQAAgBAAAAIgDAAg");
	this.shape_153.setTransform(79.725,11.4);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#E31E26").s().p("AgRAJIAAgSIAjAAIAAASg");
	this.shape_154.setTransform(74.125,11.85);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#E31E26").s().p("AgXAmIAAhLIAvAAIAAAVIgVAAIAAAGIATAAIAAAUIgTAAIAAAHIAVAAIAAAVg");
	this.shape_155.setTransform(61.3,11.4);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#E31E26").s().p("AgXAmIAAhLIAvAAIAAAVIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAVg");
	this.shape_156.setTransform(55.85,11.4);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#E31E26").s().p("AAEAmIgKgYIAAAYIgbAAIAAhLIAhAAQANgBAIAIQAHAGAAAMQAAAGgDAFQgEAEgEAEIARAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_157.setTransform(49.9,11.4);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#E31E26").s().p("AgVAdQgMgLAAgSQAAgQALgLQAMgMASAAQAPAAAKAIIgIAVQgGgFgIgBQgHAAgEAFQgEAFAAAGQAAAHAEAFQAEAEAHABQAIgBAHgGIAIAXQgLAHgPAAQgSAAgLgLg");
	this.shape_158.setTransform(42.575,11.4);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#E31E26").s().p("AgUAhQgGgGgDgHIAUgLQAEAIAFAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAAAQAAgDgDgCIgLgFQgPgHAAgOQAAgKAIgIQAIgGAMAAQAMAAAIAGQAIAGABAGIgTALQgEgHgFAAQAAAAgBABQAAAAAAAAQgBAAAAABQAAAAAAAAQAAADAFACIAKAGQAPAGAAANQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape_159.setTransform(35.95,11.4);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#E31E26").s().p("AgJAJQgEgEAAgFQAAgEAEgEQAEgEAFAAQAGAAAEAEQAEAEAAAEQAAAFgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_160.setTransform(29.225,14.125);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#E31E26").s().p("AgXAmIAAhLIAbAAIAAA2IAUAAIAAAVg");
	this.shape_161.setTransform(25.2,11.4);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#E31E26").s().p("AgVAdQgMgLAAgSQAAgQALgLQAMgMASAAQAPAAAKAIIgIAVQgGgFgIgBQgHAAgEAFQgEAFAAAGQAAAHAEAFQAEAEAHABQAIgBAHgGIAIAXQgLAHgPAAQgSAAgLgLg");
	this.shape_162.setTransform(18.825,11.4);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#E31E26").s().p("AAIAmIgQgkIAAAkIgbAAIAAhLIAcAAIAQAkIAAgkIAbAAIAABLg");
	this.shape_163.setTransform(11.425,11.4);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#E31E26").s().p("AgNAmIAAhLIAbAAIAABLg");
	this.shape_164.setTransform(5.75,11.4);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#000000").s().p("AgVAhQgJgHABgMQAAgKAHgGQgGgFAAgJQAAgJAHgHQAJgHAMAAQANAAAJAHQAHAHAAAJQAAAJgGAFQAIAGAAAKQgBAMgHAHQgJAHgOAAQgOAAgHgHgAgDAJIAAAFQAAAGADAAQAEAAABgGIgBgFQgBAAAAAAQgBgBAAAAQgBAAAAAAQAAgBgBAAQAAAAAAABQgBAAAAAAQgBAAAAABQAAAAgBAAgAgDgOQABAFACAAQADAAABgFQgBgFgDAAQgCAAgBAFg");
	this.shape_165.setTransform(111.95,21.4);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#E31E26").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_166.setTransform(68.425,21.4);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#E31E26").s().p("AgXAnIAAhNIAvAAIAAAWIgVAAIAAAGIATAAIAAAUIgTAAIAAAHIAVAAIAAAWg");
	this.shape_167.setTransform(61.75,21.4);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#E31E26").s().p("AAEAnIgKgZIAAAZIgbAAIAAhNIAhAAQANAAAIAIQAIAGAAAMQAAAGgEAFQgDAEgGADIASAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_168.setTransform(55.8,21.4);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#E31E26").s().p("AgXAnIAAhNIAvAAIAAAWIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAWg");
	this.shape_169.setTransform(49.35,21.4);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#E31E26").s().p("AgYAnIAAhNIAbAAIAAA3IAWAAIAAAWg");
	this.shape_170.setTransform(44.05,21.4);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#E31E26").s().p("AgXAnIAAhNIAbAAIAAA3IAUAAIAAAWg");
	this.shape_171.setTransform(38.7,21.4);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#E31E26").s().p("AAMAnIgEgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAAEAHIgEgPIgEAPIAIAAg");
	this.shape_172.setTransform(31.85,21.4);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#E31E26").s().p("AgUAhQgGgFgDgIIAUgLQAEAIAFAAQABAAAAAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgDgCIgLgFQgPgHAAgNQAAgMAIgGQAIgHAMAAQAMAAAIAHQAIAFABAGIgTALQgDgGgGgBQAAAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAABAFADIAKAGQAPAHAAAMQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape_173.setTransform(18.5,21.4);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#E31E26").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_174.setTransform(11.425,21.4);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#E31E26").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_175.setTransform(5.75,21.4);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#E31E26").s().p("AgXAmIAAhLIAvAAIAAAVIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAVg");
	this.shape_176.setTransform(108.05,11.4);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#E31E26").s().p("AgcAcQgLgKAAgSQAAgQAMgLQAMgMARAAQAVAAALAMIgPARQgGgFgIgBQgFAAgFAFQgEAFgBAGQAAAHAFAFQAEAEAGABQAGgBADgBIAAgEIgIAAIAAgRIAiAAIAAAhQgPANgXAAQgSAAgMgMg");
	this.shape_177.setTransform(101.15,11.4);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#E31E26").s().p("AAIAmIgQgkIAAAkIgbAAIAAhLIAcAAIAQAkIAAgkIAbAAIAABLg");
	this.shape_178.setTransform(93.175,11.4);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#E31E26").s().p("AAMAmIgDgMIgRAAIgEAMIgcAAIAbhLIAaAAIAbBLgAADAHIgDgPIgEAPIAHAAg");
	this.shape_179.setTransform(85.15,11.4);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#E31E26").s().p("AgdAmIAAhLIAfAAQANAAAIAHQAHAHAAAMQAAALgHAHQgIAIgNAAIgFAAIAAAXgAgDgFIADAAQAAAAAAAAQABAAAAAAQAAgBABAAQAAAAABgBQAAAAABAAQAAgBAAAAQAAgBABgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAQAAAAgBgBQAAAAAAAAQgBAAgBAAQAAgBAAAAIgDAAg");
	this.shape_180.setTransform(70.125,11.4);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#E31E26").s().p("AgeAcQgLgLAAgRQAAgQALgLQALgMATAAQATAAAMAMQALALAAAQQAAARgLALQgMAMgTAAQgTAAgLgMgAgJgLQgDAFAAAGQAAAHADAFQADAFAGAAQAGAAAEgFQADgFABgHQgBgGgDgFQgEgFgGAAQgGAAgDAFg");
	this.shape_181.setTransform(62.45,11.4);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#000000").s().p("AgXAmIAAhLIAvAAIAAAVIgUAAIAAAJIASAAIAAAUIgSAAIAAAZg");
	this.shape_182.setTransform(53.425,11.4);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#000000").s().p("AgXAmIAAhLIAvAAIAAAVIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAVg");
	this.shape_183.setTransform(48,11.4);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#000000").s().p("AgNAmIAAhLIAbAAIAABLg");
	this.shape_184.setTransform(43.55,11.4);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#000000").s().p("AgUAhQgGgGgDgHIAUgLQAEAIAFAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAAAQAAgDgDgCIgLgFQgPgHAAgOQAAgKAIgIQAIgGAMAAQAMAAAIAGQAIAGABAGIgTALQgEgHgFAAQAAAAgBABQAAAAAAAAQgBAAAAABQAAAAAAAAQAAADAFACIAKAGQAPAGAAANQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape_185.setTransform(38.65,11.4);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#000000").s().p("AgaAeQgIgIgBgOIAAguIAcAAIAAAuQABAIAGAAQAIAAgBgIIAAguIAcAAIAAAuQAAAOgJAIQgKAJgQAAQgQAAgKgJg");
	this.shape_186.setTransform(31.65,11.475);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#000000").s().p("AgXAmIAAhLIAbAAIAAA2IAUAAIAAAVg");
	this.shape_187.setTransform(25.2,11.4);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#000000").s().p("AgVAdQgMgLAAgSQAAgQALgLQAMgMASAAQAPAAAKAIIgIAVQgGgFgIgBQgHAAgEAFQgEAFAAAGQAAAHAEAFQAEAEAHABQAIgBAHgGIAIAXQgLAHgPAAQgSAAgLgLg");
	this.shape_188.setTransform(18.825,11.4);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#000000").s().p("AAIAmIgQgkIAAAkIgbAAIAAhLIAcAAIAQAkIAAgkIAbAAIAABLg");
	this.shape_189.setTransform(11.425,11.4);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#000000").s().p("AgNAmIAAhLIAbAAIAABLg");
	this.shape_190.setTransform(5.75,11.4);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#E31E26").s().p("AgQAIIAAgQIAhAAIAAAQg");
	this.shape_191.setTransform(122.475,20.4);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#E31E26").s().p("AgIAIQgDgDAAgFQAAgEADgDQADgEAFAAQAFAAAEAEQADADABAEQgBAFgDADQgEAEgFAAQgFAAgDgEg");
	this.shape_192.setTransform(119.15,22.475);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#E31E26").s().p("AgbAAQAAgjAbAAQAcAAAAAjQAAAlgcAAQgbAAAAglgAgCAAQgBAQADABQADgBAAgQQAAgPgDAAQgDAAABAPg");
	this.shape_193.setTransform(114.8,20);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#E31E26").s().p("AgTAdQgGgGgBgHIAVgFQAAAEAEAAQAEAAAAgEQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAAAAAQAAAAgBAAQgBAAAAAAQgBAAAAAAQgBABAAAAQAAABgBAAIgTgCIABgpIAuAAIAAATIgaAAIAAAGIAGgBQAKAAAHAGQAIAFAAALQgBAKgHAHQgJAIgLAAQgMAAgHgHg");
	this.shape_194.setTransform(109,20.075);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#E31E26").s().p("AgZAkIAAgJQAAgHAEgGQADgFAHgFIAKgIQADgEAAgEQAAgBAAAAQgBgBAAAAQAAAAgBgBQAAAAAAAAQgDAAgCAGIgVgEQACgJAGgGQAHgHALAAQAMAAAHAGQAIAGAAALQAAAKgMAJIgIAGQgDACAAACIAXAAIAAATg");
	this.shape_195.setTransform(103.225,19.925);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#E31E26").s().p("AgMAjIAAgyIgNAAIAAgTIA0AAIAAATIgPAAIAAAyg");
	this.shape_196.setTransform(95.7,20);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#E31E26").s().p("AgbAaQgKgLAAgPQAAgPAKgKQAKgKARAAQASAAAKAKQALAKgBAPQABAPgLALQgKALgSAAQgRAAgKgLgAgIgKQgDAEAAAGQAAAHADAEQADAFAFgBQAGABADgFQADgEAAgHQAAgGgDgEQgDgEgGgBQgFABgDAEg");
	this.shape_197.setTransform(88.95,20);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#E31E26").s().p("AgMAjIAAgyIgOAAIAAgTIA0AAIAAATIgNAAIAAAyg");
	this.shape_198.setTransform(82.25,20);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#E31E26").s().p("AgMAjIAAgyIgNAAIAAgTIA0AAIAAATIgPAAIAAAyg");
	this.shape_199.setTransform(74.8,20);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#E31E26").s().p("AAEAjIgJgXIAAAXIgZAAIAAhFIAeAAQAMAAAHAHQAHAGAAAJQAAAHgDAFQgDADgFACIAQAegAgFgFIADAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAgBAAAAIABgDIgBgEQAAAAAAAAQAAAAAAgBQgBAAAAAAQgBAAAAAAIgDAAg");
	this.shape_200.setTransform(69.1,20);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#E31E26").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_201.setTransform(62.025,20);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#E31E26").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_202.setTransform(54.625,20);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#E31E26").s().p("AAEAjIgLgdIAAAdIgZAAIAAhFIAZAAIAAAaIALgaIAcAAIgRAiIASAjg");
	this.shape_203.setTransform(47.55,20);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#E31E26").s().p("AgXAcQgIgIAAgNIAAgqIAZAAIAAAqQAAAIAGAAQAHAAAAgIIAAgqIAZAAIAAAqQAAANgIAIQgJAIgPAAQgOAAgJgIg");
	this.shape_204.setTransform(40.425,20.075);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#E31E26").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_205.setTransform(33.225,20);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#E31E26").s().p("AgVAjIAAhFIArAAIAAATIgSAAIAAAHIAQAAIAAARIgQAAIAAAGIASAAIAAAUg");
	this.shape_206.setTransform(27.025,20);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#E31E26").s().p("AgfAjIAAhFIAZAAQAQAAAMAKQAKAJAAAPQAAAQgKAKQgMAJgQAAgAgHAPIACAAQALAAAAgPQAAgOgLAAIgCAAg");
	this.shape_207.setTransform(21.3,20);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#E31E26").s().p("AALAjIgDgLIgQAAIgDALIgZAAIAYhFIAYAAIAZBFgAADAGIgDgNIgEANIAHAAg");
	this.shape_208.setTransform(14.125,20);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#E31E26").s().p("AgTAbQgLgLAAgQQAAgOAKgKQALgLAQAAQANAAAKAGIgHAVQgGgGgHAAQgGAAgEAFQgDAEAAAFQAAAHADAEQAEAEAGAAQAHAAAGgFIAIATQgKAIgOAAQgQgBgKgJg");
	this.shape_209.setTransform(7.175,20);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAATIgSAAIAAAHIAQAAIAAARIgQAAIAAAGIASAAIAAAUg");
	this.shape_210.setTransform(133.075,11);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAATIgSAAIAAAHIAQAAIAAARIgQAAIAAAGIASAAIAAAUg");
	this.shape_211.setTransform(128.125,11);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#000000").s().p("AgZAaQgKgKAAgQQAAgOAKgKQALgLAQAAQATAAALALIgPAQQgFgGgHAAQgFAAgEAFQgEAEAAAFQAAAHADAEQAEAEAGAAQAFAAADgCIAAgCIgIAAIAAgRIAfAAIAAAfQgNAMgVAAQgQAAgLgLg");
	this.shape_212.setTransform(119.975,11);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#000000").s().p("AgMAjIAAgyIgNAAIAAgTIA0AAIAAATIgPAAIAAAyg");
	this.shape_213.setTransform(91.2,11);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#000000").s().p("AAIAjIgPghIAAAhIgZAAIAAhFIAaAAIAOAhIAAghIAZAAIAABFg");
	this.shape_214.setTransform(84.8,11);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#000000").s().p("AgbAaQgKgLAAgPQAAgPAKgKQAKgKARAAQASAAAKAKQALAKgBAPQABAPgLALQgKALgSAAQgRAAgKgLgAgIgKQgDAEAAAGQAAAHADAEQADAFAFgBQAGABADgFQADgEAAgHQAAgGgDgEQgDgEgGgBQgFABgDAEg");
	this.shape_215.setTransform(77.25,11);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAATIgSAAIAAAHIAQAAIAAARIgQAAIAAAGIASAAIAAAUg");
	this.shape_216.setTransform(61.725,11);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#000000").s().p("AAEAjIgJgXIAAAXIgZAAIAAhFIAeAAQAMAAAIAHQAGAGAAAJQAAAGgDAGQgDADgFACIAQAegAgFgFIACAAQABAAABAAQAAAAABAAQAAgBAAAAQAAgBABAAIABgDIgBgEQgBAAAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgCAAg");
	this.shape_217.setTransform(54.45,11);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAATIgSAAIAAAHIAQAAIAAARIgQAAIAAAGIASAAIAAAUg");
	this.shape_218.setTransform(48.575,11);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#000000").s().p("AgMAjIgZhFIAbAAIALAoIALgoIAaAAIgZBFg");
	this.shape_219.setTransform(42.225,11);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#000000").s().p("AgbAaQgLgLAAgPQAAgPALgKQAKgKARAAQASAAALAKQAKAKAAAPQAAAPgKALQgLALgSAAQgRAAgKgLgAgIgKQgDAEAAAGQAAAHADAEQADAFAFgBQAGABADgFQADgEAAgHQAAgGgDgEQgDgEgGgBQgFABgDAEg");
	this.shape_220.setTransform(34.4,11);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#000000").s().p("AgaAjIAAhFIAcAAQAMAAAHAHQAHAGAAALQgBAKgGAGQgIAHgLAAIgEAAIAAAWgAgCgFIACAAQAAAAAAAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBAAgBQAAAAAAgBQAAAAAAgBQgBAAAAgBQAAAAgBAAQAAAAgBgBQAAAAgBAAQAAAAAAAAIgCAAg");
	this.shape_221.setTransform(25.75,11);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#000000").s().p("AgMAjIAAgyIgOAAIAAgTIA1AAIAAATIgPAAIAAAyg");
	this.shape_222.setTransform(12.55,11);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#000000").s().p("AgSAeQgGgFgDgHIASgJQAFAGAEAAQAAAAABAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAAgCgEgCIgJgFQgOgGAAgMQAAgKAIgGQAHgHALABQAKAAAIAFQAHAGACAFIgSALQgDgHgFAAQAAAAgBAAQAAAAgBABQAAAAAAAAQAAABAAAAQAAACAEACIAKAFQANAGAAALQAAALgHAGQgHAGgNABQgKgBgIgGg");
	this.shape_223.setTransform(6.875,11);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#000000").s().p("AgQAJIAAgQIAhAAIAAAQg");
	this.shape_224.setTransform(58.075,25.4);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#000000").s().p("AgIAIQgEgDAAgFQAAgEAEgDQAEgEAEAAQAGAAADAEQAEADgBAEQABAFgEADQgDAEgGAAQgEAAgEgEg");
	this.shape_225.setTransform(54.75,27.475);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#000000").s().p("AgXAgIAIgSQAFACAEAAQAGABABgIQgCACgFABQgJAAgFgGQgHgGAAgKQAAgLAIgHQAIgIALAAQAcABAAAgQAAAQgHAKQgIANgRAAQgLAAgIgEgAgCgOIgBAEIABAEQAAAAAAABQABAAAAAAQABABAAAAQAAAAAAAAIADgBQAAgLgDAAQAAAAAAABQgBAAAAAAQAAAAgBAAQAAABAAAAg");
	this.shape_226.setTransform(50.475,25);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#000000").s().p("AgbAEQAAgPAHgLQAIgOARAAQAMAAAIAGIgIASQgFgDgFgBQgGABgBAGQADgCAFABQAIAAAGAFQAGAFAAALQAAALgIAHQgHAIgMgBQgcABAAghgAgCAGQAAAKADAAQABAAAAAAQAAAAABAAQAAAAAAAAQABgBAAAAIAAgDIgBgFQAAAAAAgBQgBAAAAgBQAAAAgBAAQAAAAgBAAIgCABg");
	this.shape_227.setTransform(44.625,25);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("#000000").s().p("AgGAjIAAgvIgGACIgFgTIAQgFIATAAIAABFg");
	this.shape_228.setTransform(39.675,25);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("#000000").s().p("AgIAIQgEgDAAgFQAAgEAEgDQAEgEAEAAQAGAAADAEQAEADgBAEQABAFgEADQgDAEgGAAQgEAAgEgEg");
	this.shape_229.setTransform(34.5,27.475);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("#000000").s().p("AgIAIQgDgDAAgFQAAgEADgDQADgEAFAAQAGAAADAEQAEADAAAEQAAAFgEADQgDAEgGAAQgFAAgDgEg");
	this.shape_230.setTransform(24.1,27.475);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("#000000").s().p("AAJAjIgJgkIgHAkIgaAAIgShFIAbAAIAHAmIAIgmIATAAIAJAmIAGgmIAbAAIgSBFg");
	this.shape_231.setTransform(17.475,25);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("#000000").s().p("AgIAIQgDgDgBgFQABgEADgDQADgEAFAAQAGAAADAEQADADAAAEQAAAFgDADQgDAEgGAAQgFAAgDgEg");
	this.shape_232.setTransform(10.9,27.475);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("#E31E26").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_233.setTransform(123.575,16);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("#E31E26").s().p("AAHAjIgOghIAAAhIgZAAIAAhFIAZAAIAPAhIAAghIAZAAIAABFg");
	this.shape_234.setTransform(110.05,16);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("#E31E26").s().p("AAHAjIAAgZIgNAAIAAAZIgaAAIAAhFIAaAAIAAAZIANAAIAAgZIAaAAIAABFg");
	this.shape_235.setTransform(95.475,16);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("#E31E26").s().p("AgbAjIAAhFIAdAAQAMAAAHAGQAHAIAAAKQgBAKgGAGQgIAHgLAAIgEAAIAAAWgAgCgEIACAAQAAAAAAgBQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAAAAAgBQgBAAAAAAQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAAAAAIgCAAg");
	this.shape_236.setTransform(89.1,16);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("#E31E26").s().p("AgbAaQgLgLAAgPQAAgOALgLQAKgLARAAQASAAALALQAKALgBAOQABAPgKALQgLAKgSAAQgRAAgKgKgAgIgKQgDAEAAAGQAAAHADAEQADAEAFAAQAGAAADgEQADgEAAgHQAAgGgDgEQgDgFgGABQgFgBgDAFg");
	this.shape_237.setTransform(82.1,16);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("#E31E26").s().p("AAIAjIgPghIAAAhIgZAAIAAhFIAaAAIAOAhIAAghIAZAAIAABFg");
	this.shape_238.setTransform(72.65,16);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("#E31E26").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_239.setTransform(66.575,16);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("#E31E26").s().p("AAHAjIgOghIAAAhIgZAAIAAhFIAaAAIAOAhIAAghIAZAAIAABFg");
	this.shape_240.setTransform(58.6,16);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("#E31E26").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_241.setTransform(52.525,16);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("#E31E26").s().p("AAEAjIgJgWIAAAWIgZAAIAAhFIAeAAQAMAAAIAGQAGAHAAAKQAAAFgDAFQgDAEgFADIAQAdgAgFgFIACAAQABAAABAAQAAAAABgBQAAAAAAAAQAAAAAAgBIACgDIgCgDQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgCAAg");
	this.shape_242.setTransform(47.15,16);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("#E31E26").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAGIASAAIAAAUg");
	this.shape_243.setTransform(41.275,16);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("#E31E26").s().p("AAEAjIgJgWIAAAWIgZAAIAAhFIAeAAQAMAAAIAGQAGAHAAAKQAAAFgDAFQgDAEgFADIAQAdgAgFgFIACAAQABAAABAAQAAAAABgBQAAAAAAAAQAAAAAAgBIACgDIgCgDQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBAAgBAAIgCAAg");
	this.shape_244.setTransform(35.9,16);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("#E31E26").s().p("AgcAjIAAhFIAeAAQALAAAHAGQAHAFAAAJQAAAIgHAFQAEABADAFQACAEAAAEQAAAKgHAHQgHAFgMAAgAgDAQIADAAIADgBQABAAAAgBQAAAAAAgBQABAAAAgBQAAAAAAAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQAAgBgBAAIgDgBIgDAAgAgDgIIADAAQAEAAAAgDQAAgEgEgBIgDAAg");
	this.shape_245.setTransform(29.475,16);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("#E31E26").s().p("AgVAjIAAhFIAYAAIAAAxIATAAIAAAUg");
	this.shape_246.setTransform(20.875,16);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("#E31E26").s().p("AAEAjIgLgdIAAAdIgZAAIAAhFIAZAAIAAAZIALgZIAcAAIgRAhIASAkg");
	this.shape_247.setTransform(7.6,16);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("#E31E26").s().p("AgLAKQAFgDACgFQgIgCAAgHQABgFADgEQAEgDAEAAQAFAAAEADQADAEABAFQAAAIgFAHQgEAIgGAEg");
	this.shape_248.setTransform(116.55,10.325);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("#E31E26").s().p("AAHAjIgOghIAAAhIgZAAIAAhFIAaAAIAOAhIAAghIAZAAIAABFg");
	this.shape_249.setTransform(111.6,7);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("#E31E26").s().p("AAEAjIgJgWIAAAWIgZAAIAAhFIAeAAQAMAAAHAGQAHAHAAAKQAAAFgDAFQgDAEgFADIAQAdgAgFgFIADAAQAAAAABAAQAAAAABgBQAAAAAAAAQAAAAAAgBIABgDIgBgDQAAgBAAAAQAAAAAAgBQgBAAAAAAQgBAAAAAAIgDAAg");
	this.shape_250.setTransform(100.15,7);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("#E31E26").s().p("AgVAjIAAhFIAYAAIAAAyIATAAIAAATg");
	this.shape_251.setTransform(89.475,7);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("#E31E26").s().p("AgVAjIAAhFIAYAAIAAAyIATAAIAAATg");
	this.shape_252.setTransform(84.625,7);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("#E31E26").s().p("AAIAjIgPghIAAAhIgZAAIAAhFIAZAAIAPAhIAAghIAZAAIAABFg");
	this.shape_253.setTransform(59.8,7);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("#E31E26").s().p("AgMAjIAAhFIAZAAIAABFg");
	this.shape_254.setTransform(54.675,7);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAIIAQAAIAAASIgQAAIAAAXg");
	this.shape_255.setTransform(48.875,7);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("#000000").s().p("AgVAjIAAhFIArAAIAAAUIgSAAIAAAFIAQAAIAAASIgQAAIAAAHIASAAIAAATg");
	this.shape_256.setTransform(43.925,7);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("#000000").s().p("AgSAeQgGgFgDgHIASgKQAFAIAEAAQAAgBABAAQAAAAAAAAQAAAAABAAQAAgBAAAAQAAgCgEgCIgJgEQgOgHAAgMQAAgKAIgGQAHgGALgBQAKAAAIAHQAHAEACAGIgSAKQgDgGgFAAQAAAAgBAAQAAAAgBAAQAAABAAAAQAAAAAAABQAAACAEACIAKAFQANAGAAAMQAAAKgHAGQgHAHgNgBQgKABgIgHg");
	this.shape_257.setTransform(35.475,7);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("#000000").s().p("AgXAcQgIgIAAgNIAAgqIAZAAIAAAqQAAAIAGAAQAHAAAAgIIAAgqIAZAAIAAAqQAAANgIAIQgJAIgPAAQgOAAgJgIg");
	this.shape_258.setTransform(29.075,7.075);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("#000000").s().p("AgVAjIAAhFIAYAAIAAAyIATAAIAAATg");
	this.shape_259.setTransform(23.225,7);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("#000000").s().p("AgTAaQgLgKAAgQQAAgOAKgLQALgKAQgBQANAAAKAHIgHAUQgGgFgHAAQgGAAgEAEQgDAEAAAGQAAAHADAEQAEAEAGAAQAHAAAGgGIAIAVQgKAGgOAAQgQAAgKgKg");
	this.shape_260.setTransform(17.475,7);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("#000000").s().p("AgWAkQgHgGgDgIIAVgMQAGAJAFAAQAAAAABAAQAAAAAAAAQABgBAAAAQAAAAAAgBQAAgDgEgCIgLgGQgQgHgBgPQAAgMAJgIQAIgGAOAAQANAAAJAGQAIAHACAHIgVALQgEgHgFAAQgBAAgBAAQAAAAAAABQgBAAAAAAQAAABAAAAQAAACAFADIALAGQARAIAAANQAAANgJAHQgJAHgPAAQgNAAgJgHg");
	this.shape_261.setTransform(145.45,31.85);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("#000000").s().p("AgaAqIAAhTIAeAAIAAA7IAXAAIAAAYg");
	this.shape_262.setTransform(139.175,31.85);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("#000000").s().p("AgZAqIAAhTIAzAAIAAAXIgWAAIAAAHIAUAAIAAAWIgUAAIAAAHIAWAAIAAAYg");
	this.shape_263.setTransform(133.175,31.85);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("#000000").s().p("AAGAqIgOgjIAAAjIgeAAIAAhTIAeAAIAAAeIAOgeIAgAAIgUAoIAWArg");
	this.shape_264.setTransform(126.15,31.85);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("#000000").s().p("AAJAqIgSgnIAAAnIgeAAIAAhTIAfAAIARAnIAAgnIAfAAIAABTg");
	this.shape_265.setTransform(117.575,31.85);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("#000000").s().p("AgOAqIAAhTIAdAAIAABTg");
	this.shape_266.setTransform(111.375,31.85);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("#000000").s().p("AAKAqIgKgrIgJArIgfAAIgWhTIAhAAIAIAtIAKgtIAXAAIALAtIAHgtIAgAAIgVBTg");
	this.shape_267.setTransform(103.2,31.85);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("#000000").s().p("AgZAqIAAhTIAzAAIAAAXIgWAAIAAAHIAUAAIAAAWIgUAAIAAAHIAWAAIAAAYg");
	this.shape_268.setTransform(91.575,31.85);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("#000000").s().p("AgjAqIAAgQIAfgsIgeAAIAAgXIBGAAIAAAPIghAsIAhAAIAAAYg");
	this.shape_269.setTransform(84.9,31.85);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("#000000").s().p("AAJAqIgSgnIAAAnIgeAAIAAhTIAfAAIARAnIAAgnIAfAAIAABTg");
	this.shape_270.setTransform(76.875,31.85);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("#000000").s().p("AghAfQgNgNAAgSQAAgSANgMQANgMAUAAQAVAAANAMQANAMAAASQAAASgNANQgNAMgVAAQgUAAgNgMgAgKgMQgEAEABAIQgBAJAEAFQAEAEAGAAQAHAAAEgEQADgFAAgJQAAgIgDgEQgEgFgHgBQgGABgEAFg");
	this.shape_271.setTransform(67.85,31.85);

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("#000000").s().p("AAJAqIgSgnIAAAnIgeAAIAAhTIAfAAIARAnIAAgnIAfAAIAABTg");
	this.shape_272.setTransform(56.525,31.85);

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("#000000").s().p("AANAqIgEgNIgSAAIgEANIgeAAIAdhTIAdAAIAdBTgAAEAIIgEgRIgFARIAJAAg");
	this.shape_273.setTransform(47.75,31.85);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("#000000").s().p("AgOAqIgfhTIAhAAIAMAwIAOgwIAgAAIgfBTg");
	this.shape_274.setTransform(38.675,31.85);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("#000000").s().p("AAJAqIgSgoIAAAoIgeAAIAAhTIAfAAIARAoIAAgoIAfAAIAABTg");
	this.shape_275.setTransform(137.425,20.85);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("#000000").s().p("AgZA1IAAhTIAzAAIAAAXIgWAAIAAAHIAUAAIAAAWIgUAAIAAAHIAWAAIAAAYgAgNghIAJgTIAbAAIgOATg");
	this.shape_276.setTransform(130.125,19.75);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("#000000").s().p("AgZA1IAAhTIAzAAIAAAXIgWAAIAAAHIAUAAIAAAWIgUAAIAAAHIAWAAIAAAYgAgNghIAJgTIAbAAIgOATg");
	this.shape_277.setTransform(124.175,19.75);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("#000000").s().p("AAJAqIgSgoIAAAoIgeAAIAAhTIAfAAIARAoIAAgoIAfAAIAABTg");
	this.shape_278.setTransform(114.625,20.85);

	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("#000000").s().p("AgOAqIAAhTIAdAAIAABTg");
	this.shape_279.setTransform(108.425,20.85);

	this.shape_280 = new cjs.Shape();
	this.shape_280.graphics.f("#000000").s().p("AgWAkQgHgGgDgIIAVgMQAGAJAFAAQAAAAABAAQAAAAAAAAQABgBAAAAQAAAAAAgBQAAgDgEgCIgLgFQgQgIgBgPQAAgMAJgIQAIgGAOgBQANABAJAGQAIAHACAHIgVAMQgEgIgFAAQgBAAgBAAQAAAAAAABQgBAAAAAAQAAABAAAAQAAACAFADIALAGQARAIAAANQAAANgJAHQgJAHgPAAQgMAAgKgHg");
	this.shape_280.setTransform(100.8,20.85);

	this.shape_281 = new cjs.Shape();
	this.shape_281.graphics.f("#000000").s().p("AgeAfQgMgMAAgTQAAgSAMgMQANgNATAAQAWAAAOAOIgRATQgHgHgJAAQgFAAgFAFQgFAFAAAHQAAAIAFAFQAEAFAGAAQAIABADgDIAAgDIgKAAIAAgUIAlAAIAAAlQgPANgZAAQgVAAgMgMg");
	this.shape_281.setTransform(92.8,20.85);

	this.shape_282 = new cjs.Shape();
	this.shape_282.graphics.f("#000000").s().p("AAJAqIgSgoIAAAoIgeAAIAAhTIAfAAIARAoIAAgoIAfAAIAABTg");
	this.shape_282.setTransform(84.125,20.85);

	this.shape_283 = new cjs.Shape();
	this.shape_283.graphics.f("#000000").s().p("AANAqIgEgNIgSAAIgEANIgeAAIAdhTIAdAAIAdBTgAAEAIIgEgRIgFARIAJAAg");
	this.shape_283.setTransform(75.35,20.85);

	this.shape_284 = new cjs.Shape();
	this.shape_284.graphics.f("#000000").s().p("AgaAqIAAhTIAeAAIAAA7IAXAAIAAAYg");
	this.shape_284.setTransform(68.075,20.85);

	this.shape_285 = new cjs.Shape();
	this.shape_285.graphics.f("#000000").s().p("AAWAqIAAgpIgIAiIgaAAIgJgiIAAApIgeAAIAAhTIAnAAIAMAvIANgvIAnAAIAABTg");
	this.shape_285.setTransform(57.3,20.85);

	this.shape_286 = new cjs.Shape();
	this.shape_286.graphics.f("#000000").s().p("AghAfQgMgNAAgSQAAgSAMgMQANgNAUAAQAVAAANANQAMAMAAASQAAASgMANQgNAMgVAAQgUAAgNgMgAgKgMQgDAEgBAIQABAIADAGQAEAEAGAAQAHAAADgEQAEgGAAgIQAAgIgEgEQgDgGgHAAQgGAAgEAGg");
	this.shape_286.setTransform(47.1,20.85);

	this.shape_287 = new cjs.Shape();
	this.shape_287.graphics.f("#000000").s().p("AAGAqIgOgiIAAAiIgeAAIAAhTIAeAAIAAAfIAOgfIAgAAIgUAoIAVArg");
	this.shape_287.setTransform(38.3,20.85);

	this.shape_288 = new cjs.Shape();
	this.shape_288.graphics.f("#000000").s().p("AgZAqIAAhTIAzAAIAAAYIgWAAIAAAJIAUAAIAAAWIgUAAIAAAcg");
	this.shape_288.setTransform(132.05,9.85);

	this.shape_289 = new cjs.Shape();
	this.shape_289.graphics.f("#000000").s().p("AghAfQgMgMAAgTQAAgSAMgMQANgNAUAAQAVAAANANQANAMAAASQAAATgNAMQgNANgVAAQgUAAgNgNgAgKgNQgDAGgBAHQABAJADAEQAEAGAGgBQAHABAEgGQADgEAAgJQAAgHgDgGQgEgEgHAAQgGAAgEAEg");
	this.shape_289.setTransform(124.4,9.85);

	this.shape_290 = new cjs.Shape();
	this.shape_290.graphics.f("#000000").s().p("AgZAqIAAhTIAzAAIAAAYIgWAAIAAAHIAUAAIAAAVIgUAAIAAAHIAWAAIAAAYg");
	this.shape_290.setTransform(114.375,9.85);

	this.shape_291 = new cjs.Shape();
	this.shape_291.graphics.f("#000000").s().p("AAJAqIgSgoIAAAoIgeAAIAAhTIAfAAIARAoIAAgoIAfAAIAABTg");
	this.shape_291.setTransform(107.125,9.85);

	this.shape_292 = new cjs.Shape();
	this.shape_292.graphics.f("#000000").s().p("AgOAqIAAhTIAdAAIAABTg");
	this.shape_292.setTransform(100.925,9.85);

	this.shape_293 = new cjs.Shape();
	this.shape_293.graphics.f("#000000").s().p("AgaAqIAAhTIAeAAIAAA7IAXAAIAAAYg");
	this.shape_293.setTransform(96.225,9.85);

	this.shape_294 = new cjs.Shape();
	this.shape_294.graphics.f("#000000").s().p("AAJAqIgSgoIAAAoIgeAAIAAhTIAfAAIARAoIAAgoIAfAAIAABTg");
	this.shape_294.setTransform(88.925,9.85);

	this.shape_295 = new cjs.Shape();
	this.shape_295.graphics.f("#000000").s().p("AghAfQgNgMAAgTQAAgSANgMQANgNAUAAQAVAAANANQAMAMAAASQAAATgMAMQgNANgVAAQgUAAgNgNgAgKgNQgEAGABAHQgBAJAEAEQAEAGAGgBQAHABADgGQAFgEAAgJQAAgHgFgGQgDgEgHAAQgGAAgEAEg");
	this.shape_295.setTransform(79.9,9.85);

	this.shape_296 = new cjs.Shape();
	this.shape_296.graphics.f("#000000").s().p("AAGAqIgOgiIAAAiIgeAAIAAhTIAeAAIAAAfIAOgfIAgAAIgUAoIAVArg");
	this.shape_296.setTransform(68.8,9.85);

	this.shape_297 = new cjs.Shape();
	this.shape_297.graphics.f("#000000").s().p("AgVAoIACgXIAHABQAFAAAAgEIAAg3IAeAAIAAA7QAAAYgbAAQgJAAgIgCg");
	this.shape_297.setTransform(61.9,9.875);

	this.shape_298 = new cjs.Shape();
	this.shape_298.graphics.f("#000000").s().p("AgOAqIAAhTIAdAAIAABTg");
	this.shape_298.setTransform(57.625,9.85);

	this.shape_299 = new cjs.Shape();
	this.shape_299.graphics.f("#000000").s().p("AAGAqIgOgiIAAAiIgeAAIAAhTIAeAAIAAAfIAOgfIAgAAIgUAoIAVArg");
	this.shape_299.setTransform(51.7,9.85);

	this.shape_300 = new cjs.Shape();
	this.shape_300.graphics.f("#000000").s().p("AgZAqIAAhTIAzAAIAAAYIgWAAIAAAHIAUAAIAAAVIgUAAIAAAHIAWAAIAAAYg");
	this.shape_300.setTransform(44.425,9.85);

	this.shape_301 = new cjs.Shape();
	this.shape_301.graphics.f("#000000").s().p("AghAqIAAhTIAjAAQAOAAAIAGQAJAHAAALQAAAKgJAGQAFACADAFQACAEAAAGQABAMgJAIQgIAGgOAAgAgEATIAEAAQABAAAAAAQABAAAAAAQABAAAAAAQABgBAAAAQAAgBABAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQgBgBAAAAQAAAAgBgBQAAAAgBgBQAAAAgBAAQAAAAgBAAIgEAAgAgEgJIAEAAQAFAAAAgFQAAgFgFABIgEAAg");
	this.shape_301.setTransform(37.8,9.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_55,p:{x:7.675,y:7}},{t:this.shape_54},{t:this.shape_53,p:{x:21.475}},{t:this.shape_52},{t:this.shape_51,p:{x:32.275,y:7}},{t:this.shape_50},{t:this.shape_49,p:{x:44.325}},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45,p:{x:67.325}},{t:this.shape_44,p:{x:73.4,y:7}},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40,p:{x:99.85}},{t:this.shape_39,p:{x:105.075}},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33,p:{x:11.775,y:16}},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25,p:{x:60.35,y:16}},{t:this.shape_24,p:{x:64.675,y:16}},{t:this.shape_23},{t:this.shape_22,p:{x:76.925,y:16}},{t:this.shape_21},{t:this.shape_20,p:{x:91.8,y:16}},{t:this.shape_19},{t:this.shape_18,p:{x:106.575,y:16}},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11,p:{x:12.55,y:25}},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8,p:{x:36.675,y:25}},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2,p:{x:76.7,y:25}},{t:this.shape_1},{t:this.shape}]},1).to({state:[{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80,p:{x:53.075}},{t:this.shape_79},{t:this.shape_78,p:{x:67.2}},{t:this.shape_77},{t:this.shape_76,p:{x:80.8}},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65,p:{x:59.325}},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59,p:{x:107.725}},{t:this.shape_58,p:{x:114.075}},{t:this.shape_57,p:{x:118.775}},{t:this.shape_56,p:{x:122.475}}]},1).to({state:[{t:this.shape_117},{t:this.shape_116},{t:this.shape_115,p:{x:18.625}},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108,p:{x:68.225}},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103,p:{x:107.8}},{t:this.shape_102,p:{x:115.475}},{t:this.shape_101},{t:this.shape_100,p:{x:128.95}},{t:this.shape_99,p:{x:135.325}},{t:this.shape_98,p:{x:6.925}},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93,p:{x:40.775}},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90,p:{x:64.45}},{t:this.shape_89},{t:this.shape_88,p:{x:77.525}},{t:this.shape_87}]},1).to({state:[{t:this.shape_137},{t:this.shape_108,p:{x:16.025}},{t:this.shape_115,p:{x:22.725}},{t:this.shape_136},{t:this.shape_103,p:{x:37.7}},{t:this.shape_102,p:{x:45.375}},{t:this.shape_135},{t:this.shape_134},{t:this.shape_100,p:{x:66.75}},{t:this.shape_99,p:{x:73.125}},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_90,p:{x:28.2}},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_98,p:{x:86.275}},{t:this.shape_123},{t:this.shape_88,p:{x:100.375}},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_93,p:{x:128.025}},{t:this.shape_119},{t:this.shape_118}]},1).to({state:[{t:this.shape_164},{t:this.shape_163,p:{x:11.425}},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156,p:{x:55.85}},{t:this.shape_155},{t:this.shape_80,p:{x:67.975}},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_78,p:{x:107.25}},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145,p:{x:7.075}},{t:this.shape_144,p:{x:11.575}},{t:this.shape_143,p:{x:18.875}},{t:this.shape_142,p:{x:26.125}},{t:this.shape_141,p:{x:31.9}},{t:this.shape_57,p:{x:37.625}},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_58,p:{x:60.175}},{t:this.shape_59,p:{x:66.525}}]},1).to({state:[{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_76,p:{x:77.15}},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_163,p:{x:114.725}},{t:this.shape_156,p:{x:123.5}},{t:this.shape_80,p:{x:130.175}},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_65,p:{x:24.725}},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_145,p:{x:77.525}},{t:this.shape_144,p:{x:82.025}},{t:this.shape_143,p:{x:89.325}},{t:this.shape_142,p:{x:96.575}},{t:this.shape_141,p:{x:102.35}},{t:this.shape_165},{t:this.shape_58,p:{x:118.375}},{t:this.shape_57,p:{x:123.075}},{t:this.shape_56,p:{x:126.775}}]},1).to({state:[{t:this.shape_223},{t:this.shape_222},{t:this.shape_18,p:{x:19.025,y:11}},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_11,p:{x:67.8,y:11}},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_22,p:{x:97.825,y:11}},{t:this.shape_8,p:{x:105.425,y:11}},{t:this.shape_2,p:{x:112.7,y:11}},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_20,p:{x:139.15,y:11}},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202,p:{x:54.625,y:20}},{t:this.shape_201,p:{x:62.025,y:20}},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191}]},1).to({state:[{t:this.shape_33,p:{x:5.575,y:7}},{t:this.shape_20,p:{x:10.7,y:7}},{t:this.shape_260},{t:this.shape_259},{t:this.shape_258},{t:this.shape_257},{t:this.shape_24,p:{x:39.925,y:7}},{t:this.shape_256},{t:this.shape_255},{t:this.shape_254},{t:this.shape_253},{t:this.shape_49,p:{x:66.275}},{t:this.shape_40,p:{x:71.95}},{t:this.shape_53,p:{x:78.425}},{t:this.shape_252},{t:this.shape_251},{t:this.shape_45,p:{x:94.275}},{t:this.shape_250},{t:this.shape_39,p:{x:105.525}},{t:this.shape_249},{t:this.shape_248},{t:this.shape_247},{t:this.shape_202,p:{x:14.675,y:16}},{t:this.shape_246},{t:this.shape_51,p:{x:24.775,y:16}},{t:this.shape_245},{t:this.shape_244},{t:this.shape_243},{t:this.shape_242},{t:this.shape_241},{t:this.shape_240},{t:this.shape_239},{t:this.shape_238},{t:this.shape_237},{t:this.shape_236},{t:this.shape_235},{t:this.shape_201,p:{x:102.775,y:16}},{t:this.shape_234},{t:this.shape_55,p:{x:117.325,y:16}},{t:this.shape_233},{t:this.shape_44,p:{x:129.65,y:16}},{t:this.shape_25,p:{x:6.8,y:25}},{t:this.shape_232},{t:this.shape_231},{t:this.shape_230},{t:this.shape_22,p:{x:29.275,y:25}},{t:this.shape_229},{t:this.shape_228},{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224}]},1).to({state:[{t:this.shape_301},{t:this.shape_300},{t:this.shape_299},{t:this.shape_298},{t:this.shape_297},{t:this.shape_296},{t:this.shape_295},{t:this.shape_294},{t:this.shape_293},{t:this.shape_292},{t:this.shape_291},{t:this.shape_290},{t:this.shape_289},{t:this.shape_288},{t:this.shape_287},{t:this.shape_286},{t:this.shape_285},{t:this.shape_284},{t:this.shape_283},{t:this.shape_282},{t:this.shape_281},{t:this.shape_280},{t:this.shape_279},{t:this.shape_278},{t:this.shape_277},{t:this.shape_276},{t:this.shape_275},{t:this.shape_274},{t:this.shape_273},{t:this.shape_272},{t:this.shape_271},{t:this.shape_270},{t:this.shape_269},{t:this.shape_268},{t:this.shape_267},{t:this.shape_266},{t:this.shape_265},{t:this.shape_264},{t:this.shape_263},{t:this.shape_262},{t:this.shape_261}]},1).wait(1));

	// Layer_1
	this.shape_302 = new cjs.Shape();
	this.shape_302.graphics.f("#FFFFFF").s().p("AtMCTIAAklIaZAAIAAElg");
	this.shape_302.setTransform(74.9784,15.9976,0.8876,1.0847);
	this.shape_302._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_302).wait(1).to({_off:false},0).wait(8).to({scaleY:1.4233,x:101.9784,y:20.9909},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,185.1,42);


(lib.label_green_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(7));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgoBBIAHgNQAPAJARAAQARAAAJgHQAMgJAAgRIAAgJQgNAUgYAAQgXAAgPgRQgOgPAAgZQAAgZAOgQQAPgQAXAAQAOAAALAIQAIAFAFAHIAAgRIAQAAIAABdQAAA3g2AAQgWAAgSgLgAgZgwQgLAMAAASQAAASALAMQAKALAQAAQAQAAALgMQAJgLABgSQgBgTgKgMQgLgLgQAAQgPAAgKAMg");
	this.shape.setTransform(148.15,14.175);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgGgIgMAAQgMAAgHAJQgIAIAAANIAABCIgRAAIAAhsIAQAAIAAAPQAFgHAIgFQAJgGAKAAQATAAAKANQAKAMAAASIAABEg");
	this.shape_1.setTransform(136.4,12.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgHBKIAAhsIAQAAIAABsgAgHg2QgEgDAAgFQAAgFAEgDQADgDAEAAQAFAAADADQADADABAFQgBAFgDADQgDADgFAAQgEAAgDgDg");
	this.shape_2.setTransform(128.35,10.325);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgoBBIAHgNQAOAJATAAQAQAAAKgHQALgJAAgRIAAgJQgNAUgYAAQgXAAgPgRQgOgPAAgZQAAgZAOgQQAPgQAXAAQAOAAAMAIQAIAFAEAHIAAgRIAQAAIAABdQAAA3g2AAQgWAAgSgLgAgZgwQgLAMAAASQAAASALAMQALALAPAAQAQAAALgMQAKgLAAgSQgBgTgKgMQgLgLgQAAQgPAAgKAMg");
	this.shape_3.setTransform(119.45,14.175);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_4.setTransform(110.575,12.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYAQgQQAQgQAYAAQAYAAARAQQAQAQAAAYQAAAYgQARQgQAQgZABQgXgBgRgQgAgcgdQgLAMAAARQAAASALAMQAMAMAQAAQARAAAMgMQALgMAAgSQAAgSgLgLQgMgNgRAAQgQAAgMANg");
	this.shape_5.setTransform(100.35,12.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgoA2IAAgLIA9hSIg8AAIAAgPIBQAAIAAAMIg+BTIA+AAIAAANg");
	this.shape_6.setTransform(89.1,12.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgkAqQgPgQgBgaQABgZAPgPQAPgRAVABQAYAAAOAPQAOAPABAZIAAAFIhYAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgJAHQgNAHgSABQgXAAgQgQgAAkgIQgBgQgKgKQgKgIgPAAQgMAAgKAJQgLAKgBAPIBGAAIAAAAg");
	this.shape_7.setTransform(78.35,12.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AglA5IAAARIgPAAIAAiXIAQAAIAAA9QAEgHAIgFQAMgJAOABQAWgBAPARQAPAQAAAYQAAAZgQAQQgPAQgWABQgYAAgOgVgAgagKQgLALAAATQAAASAKAMQALANAQAAQAPAAALgMQAKgMABgTQAAgSgLgLQgKgNgQAAQgQAAgKAMg");
	this.shape_8.setTransform(66.35,10.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AglAiIAMgIQAJARARAAQAJAAAFgFQAGgEAAgJQAAgIgIgFQgEgCgOgGQgNgFgGgGQgIgIAAgMQAAgOAKgHQAJgJAPABQANAAAKAHQAIAGACAHIgMAIQgHgPgPAAQgHAAgFAFQgFADAAAHQAAAJAIAFIASAIQANAGAGAEQAJAIAAANQAAAOgLAKQgLAIgQABQgaAAgLgYg");
	this.shape_9.setTransform(49.875,12.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgHBKIAAhsIAPAAIAABsgAgHg2QgEgDABgFQgBgFAEgDQADgDAEAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_10.setTransform(43.25,10.325);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFAMAAIAEgBIAAAPIgKABQgfAAAAgng");
	this.shape_11.setTransform(37.85,11.225);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_12.setTransform(29.225,12.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_13.setTransform(21.325,12.15);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgtA3QgVgWAAghQAAggAVgVQAWgWAfAAQAhAAAVAVIgLALQgSgRgZAAQgYAAgQASQgQARAAAZQAAAaAQARQAQARAYAAQAbAAAQgPIAAgfIghAAIAAgOIAxAAIAAA0QgYAYgkAAQgeAAgWgVg");
	this.shape_14.setTransform(9.5,10.425);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgoBBIAGgNQAPAJATAAQAPAAALgHQALgJAAgRIAAgJQgMAUgZAAQgXAAgPgRQgPgPAAgZQAAgZAPgQQAPgQAXAAQANAAANAIQAHAFAEAHIAAgRIARAAIAABdQgBA3g1AAQgWAAgSgLgAgagwQgKAMAAASQAAASALAMQAKALAPAAQARAAALgMQAKgLgBgSQABgTgLgMQgLgLgQAAQgPAAgLAMg");
	this.shape_15.setTransform(149.8,14.175);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgNAAgHAJQgIAIAAANIAABCIgQAAIAAhsIAPAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_16.setTransform(138.05,12.15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgHBKIAAhsIAQAAIAABsgAgHg2QgEgDABgFQgBgFAEgDQADgDAEAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_17.setTransform(130,10.325);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFALAAIAFgBIAAAPIgKABQgfAAAAgng");
	this.shape_18.setTransform(124.6,11.225);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAHAIALgBQAMABAIgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgJAGgKAAQgSAAgLgNg");
	this.shape_19.setTransform(111.4,12.45);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgHBMIAAiXIAPAAIAACXg");
	this.shape_20.setTransform(103.475,10.15);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgNAAgHAJQgIAIAAANIAABCIgQAAIAAhsIAPAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_21.setTransform(86.75,12.15);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_22.setTransform(64.625,12.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AglAiIAMgIQAJARARAAQAJAAAFgFQAGgEAAgJQAAgIgIgFQgEgCgOgGQgNgFgGgGQgIgIAAgMQAAgOAKgHQAJgJAPABQANAAAKAHQAIAGACAHIgMAIQgHgPgPAAQgHAAgFAFQgFADAAAHQAAAJAIAFIASAIQANAGAGAEQAJAIAAANQAAAOgLAKQgLAIgQABQgaAAgLgYg");
	this.shape_23.setTransform(49.875,12.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_24.setTransform(29.225,12.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgIAHgHQAKgGALAAIAAARQgfAAAAApIAAA1g");
	this.shape_25.setTransform(109.525,34.15);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AghArQgKgLAAgUIAAhDIARAAIAABEQAAANAHAHQAIAIALAAQALAAAIgJQAIgIAAgOIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgJAGgKAAQgSAAgLgNg");
	this.shape_26.setTransform(99.95,34.45);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYARgQQAQgRAXAAQAYAAARARQAQAQAAAYQAAAYgQARQgRAQgYAAQgYAAgQgQgAgcgdQgLAMAAARQAAATALALQALAMARAAQARAAAMgMQALgMAAgSQAAgRgLgNQgMgMgRABQgRAAgLAMg");
	this.shape_27.setTransform(87.9,34.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgkApQgQgPAAgZQAAgaAQgQQAQgPAUgBQAYAAAOAQQAOAQAAAYIAAAEIhXAAQABASALALQALAKAQAAQAWABAMgSIALAIQgGAKgJAGQgNAJgRgBQgYAAgQgQgAAjgIQAAgQgKgJQgKgJgPAAQgMAAgKAJQgLAKgBAPIBFAAIAAAAg");
	this.shape_28.setTransform(69.45,34.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgIAHgHQAKgGALAAIAAARQgfAAAAApIAAA1g");
	this.shape_29.setTransform(60.675,34.15);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AglAiIAMgHQAJAQARAAQAJAAAFgEQAGgFAAgJQAAgHgIgGQgEgDgOgGQgNgEgGgFQgIgJAAgMQAAgNAKgJQAJgHAPgBQANAAAKAIQAIAGACAIIgMAGQgHgOgPABQgHAAgFADQgFAFAAAGQAAAJAIAFIASAIQANAGAGAEQAJAIAAANQAAAOgLAJQgLAKgQgBQgaABgLgYg");
	this.shape_30.setTransform(47.125,34.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgHBKIAAhsIAPAAIAABsgAgHg2QgDgDgBgFQABgFADgDQADgDAEAAQAFAAADADQADADABAFQgBAFgDADQgDADgFAAQgEAAgDgDg");
	this.shape_31.setTransform(40.5,32.325);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgNAcIAAg5IgNAAIAAgOIANAAIAAgXIAPAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFALAAIAFgBIAAAPIgKABQgeAAAAgng");
	this.shape_32.setTransform(35.1,33.225);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgQAMgJQAMgKASAAQATAAAJAKIAAgNQAAgMgHgGQgHgHgMABQgQgBgPAKIgHgMQATgMAUgBQAqABAAAoIAABHIgPAAIgBgJQgLAMgSgBQgSAAgMgKgAgSAEQgIAHAAAKQAAAKAIAGQAHAHAMgBQAMABAHgHQAIgGAAgKQAAgKgIgHQgHgFgMAAQgMAAgHAFg");
	this.shape_33.setTransform(26.475,34.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgIAHgHQAKgGALAAIAAARQgfAAAAApIAAA1g");
	this.shape_34.setTransform(18.575,34.15);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgoBBIAGgNQAPAJATAAQAPAAALgHQALgJAAgRIAAgJQgMAUgZAAQgXAAgPgRQgOgPAAgZQAAgZAOgQQAPgQAXAAQANAAANAIQAHAFAEAHIAAgRIARAAIAABdQgBA3g1AAQgWAAgSgLgAgZgwQgLAMAAASQAAASALAMQAKALAPAAQARAAALgMQAKgLgBgSQABgTgLgMQgLgLgQAAQgPAAgKAMg");
	this.shape_35.setTransform(8.05,36.175);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFAMAAIAEgBIAAAPIgKABQgfAAAAgng");
	this.shape_36.setTransform(138.2,11.225);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("Ag1BMIAAiUIAQAAIAAARQAOgUAXAAQAYAAAPAQQAOAQAAAZQAAAZgOAQQgPAQgWAAQgPAAgLgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQALALAPAAQAQAAAKgMQALgMgBgSQAAgSgLgMQgKgMgPAAQgQAAgLANg");
	this.shape_37.setTransform(90.35,14.125);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("Ag0BMIAAiUIAPAAIAAARQAOgUAYAAQAWAAAQAQQAPAQAAAZQAAAZgPAQQgPAQgWAAQgOAAgMgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQAKALAQAAQAQAAAKgMQALgMAAgSQAAgSgMgMQgJgMgQAAQgQAAgLANg");
	this.shape_38.setTransform(77.5,14.125);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_39.setTransform(65.075,12.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgjAqQgRgQABgaQgBgZARgPQAOgRAWABQAYAAANAPQAPAPAAAZIAAAFIhXAAQAAARALALQAKALARAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgRABQgXAAgPgQgAAkgIQgBgQgLgKQgJgIgOAAQgNAAgKAJQgLAKAAAPIBFAAIAAAAg");
	this.shape_40.setTransform(48.8,12.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AglA9QgPgQgBgZQAAgYAPgQQAOgRAYABQANgBAMAJQAIAFAEAHIAAg9IAQAAIAACXIgQAAIAAgRQgNAVgYAAQgWgBgPgQgAgZgJQgLALAAASQAAATALAMQAKAMAPAAQARAAAKgNQALgMgBgSQABgTgLgLQgLgMgQAAQgPAAgKANg");
	this.shape_41.setTransform(36.05,10.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAIAIAKgBQANABAHgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgKAGgJAAQgSAAgLgNg");
	this.shape_42.setTransform(24.3,12.45);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("Ag1A2QgVgWAAggQAAggAVgWQAWgVAfAAQAgAAAWAWQAVAWAAAfQAAAggVAWQgWAWggAAQgfAAgWgWgAgogqQgQARAAAZQAAAaAQARQARARAXAAQAZAAAQgRQAQgRAAgaQAAgZgQgRQgQgSgZAAQgXAAgRASg");
	this.shape_43.setTransform(10.3,10.425);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgkAqQgPgQAAgaQAAgZAPgPQAQgRAUABQAZAAAOAPQAOAPgBAZIAAAFIhXAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgQABQgYAAgQgQgAAjgIQAAgQgLgKQgJgIgPAAQgMAAgKAJQgKAKgBAPIBEAAIAAAAg");
	this.shape_44.setTransform(151.1,12.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AgkAqQgQgQAAgaQAAgZAQgPQAQgRAUABQAZAAAOAPQAOAPgBAZIAAAFIhXAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgQABQgYAAgQgQgAAjgIQAAgQgLgKQgJgIgPAAQgMAAgKAJQgKAKgCAPIBFAAIAAAAg");
	this.shape_45.setTransform(132.55,12.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgHgIgLAAQgLAAgIAJQgIAIAAANIAABCIgRAAIAAhsIARAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMAAASIAABEg");
	this.shape_46.setTransform(120.9,12.15);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAIAIAKgBQANABAHgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgKAGgJAAQgTAAgKgNg");
	this.shape_47.setTransform(102.7,12.45);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYAQgQQAQgQAYAAQAZAAAQAQQAQAQAAAYQAAAYgRARQgPAQgZABQgYgBgQgQgAgcgdQgLAMAAARQAAASALAMQAMAMAQAAQASAAALgMQALgMAAgSQAAgSgLgLQgLgNgSAAQgQAAgMANg");
	this.shape_48.setTransform(90.65,12.3);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_49.setTransform(63.425,12.15);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_50.setTransform(21.325,12.15);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AglA9QgPgQgBgZQAAgYAPgQQAOgRAXABQAPgBALAJQAIAFAEAHIAAg9IAQAAIAACXIgQAAIAAgRQgNAVgYAAQgXgBgOgQgAgZgJQgLALAAASQAAATALAMQALAMAOAAQARAAAKgNQAKgMAAgSQAAgTgLgLQgKgMgQAAQgOAAgLANg");
	this.shape_51.setTransform(186.9,10.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AgVBeIAAgOIAFAAQASAAAAgXIAAhvIARAAIAABwQAAAkgiAAIgGAAgAAChKQgDgDABgFQgBgFADgDQADgDAFAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgFAAgDgDg");
	this.shape_52.setTransform(177.6,12.325);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AAVBMIgug6IAAA6IgQAAIAAiXIAQAAIAABZIAsguIAVAAIguAvIAwA9g");
	this.shape_53.setTransform(161.375,10.15);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgGgIgMAAQgMAAgHAJQgIAIAAANIAABCIgRAAIAAhsIAQAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_54.setTransform(150,12.15);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#000000").s().p("AgkAqQgQgQAAgaQAAgZAQgPQAQgRAUABQAYAAAOAPQAOAPABAZIAAAFIhYAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgJAHQgNAHgRABQgYAAgQgQgAAjgIQAAgQgKgKQgKgIgPAAQgMAAgKAJQgLAKgBAPIBFAAIAAAAg");
	this.shape_55.setTransform(138.3,12.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("AgmA9QgOgQAAgZQAAgYAOgQQAPgRAWABQAOgBAMAJQAIAFAEAHIAAg9IAQAAIAACXIgPAAIAAgRQgOAVgYAAQgXgBgPgQgAgZgJQgLALAAASQAAATALAMQAKAMAQAAQARAAAKgNQAJgMABgSQAAgTgMgLQgJgMgQAAQgQAAgKANg");
	this.shape_56.setTransform(125.55,10.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AglA5IAAARIgQAAIAAiXIARAAIAAA9QAEgHAIgFQALgJAPABQAWgBAPARQAOAQAAAYQAAAZgOAQQgPAQgYABQgXAAgOgVgAgagKQgLALAAATQAAASAKAMQALANAQAAQAPAAAKgMQALgMAAgTQABgSgLgLQgKgNgQAAQgPAAgLAMg");
	this.shape_57.setTransform(101.6,10.3);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgMAAgIAJQgIAIAAANIAABCIgQAAIAAhsIAQAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMgBASIAABEg");
	this.shape_58.setTransform(83.8,12.15);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AgjAqQgRgQABgaQgBgZARgPQAPgRAVABQAXAAAPAPQANAPAAAZIAAAFIhWAAQAAARALALQAKALARAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgRABQgXAAgPgQgAAkgIQgCgQgKgKQgJgIgOAAQgNAAgKAJQgKAKgBAPIBFAAIAAAAg");
	this.shape_59.setTransform(72.1,12.3);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AglA9QgPgQAAgZQgBgYAPgQQAOgRAYABQANgBAMAJQAIAFAEAHIAAg9IARAAIAACXIgRAAIAAgRQgNAVgYAAQgWgBgPgQgAgZgJQgKALAAASQAAATAKAMQAKAMAPAAQARAAALgNQAKgMgBgSQABgTgLgLQgLgMgQAAQgPAAgKANg");
	this.shape_60.setTransform(35.95,10.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AglA4QgKgSAAgmQAAgmAKgRQAMgUAZAAQAbAAALAUQAJARABAmQgBAmgJASQgLAUgbAAQgZAAgMgUgAgYgqQgFAMAAAeQAAAeAFANQAGARASAAQATAAAGgRQAFgNAAgeQAAgegFgMQgGgSgTAAQgSAAgGASg");
	this.shape_61.setTransform(18.8,10.425);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AggBAQgJgIgEgNIAOgFQAHAVAYAAQAMAAAJgIQAIgIAAgNQAAgNgJgIQgJgIgQAAIgLAAIAAgNIAmguIg+AAIAAgQIBRAAIAAAQIgmAtQAQAAANAKQAOALAAAVQAAAUgNANQgNANgUAAQgTAAgNgLg");
	this.shape_62.setTransform(7.425,10.525);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AgnBBIAFgNQAQAJARAAQAQAAAKgHQAMgJAAgRIAAgJQgMAUgZAAQgXAAgPgRQgPgPAAgZQAAgZAPgQQAPgQAXAAQANAAAMAIQAJAFADAHIAAgRIAQAAIAABdQABA3g2AAQgWAAgRgLgAgagwQgKAMAAASQAAASALAMQALALAOAAQARAAAKgMQAKgLAAgSQAAgTgLgMQgKgLgQAAQgPAAgLAMg");
	this.shape_63.setTransform(135.25,14.175);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgMAAgIAJQgIAIAAANIAABCIgQAAIAAhsIAQAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMAAASIAABEg");
	this.shape_64.setTransform(123.5,12.15);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#000000").s().p("AgHBKIAAhsIAPAAIAABsgAgHg2QgDgDgBgFQABgFADgDQADgDAEAAQAFAAADADQAEADAAAFQAAAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_65.setTransform(115.45,10.325);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#000000").s().p("Ag0BMIAAiUIAPAAIAAARQAOgUAYAAQAWAAAPAQQAQAQAAAZQAAAZgPAQQgPAQgWAAQgOAAgMgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQAKALAQAAQAQAAAKgMQALgMAAgSQgBgSgKgMQgLgMgPAAQgQAAgLANg");
	this.shape_66.setTransform(66.35,14.125);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_14},{t:this.shape_13,p:{x:21.325}},{t:this.shape_12,p:{x:29.225}},{t:this.shape_11,p:{x:37.85,y:11.225}},{t:this.shape_10},{t:this.shape_9,p:{x:49.875}},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4,p:{x:110.575}},{t:this.shape_3},{t:this.shape_2,p:{x:128.35}},{t:this.shape_1},{t:this.shape,p:{x:148.15}}]},1).to({state:[{t:this.shape_14},{t:this.shape_4,p:{x:21.325}},{t:this.shape_24,p:{x:29.225}},{t:this.shape_11,p:{x:37.85,y:11.225}},{t:this.shape_10},{t:this.shape_23},{t:this.shape_22,p:{x:64.625}},{t:this.shape_12,p:{x:75.325}},{t:this.shape_21},{t:this.shape_9,p:{x:96.825}},{t:this.shape_20,p:{x:103.475}},{t:this.shape_19},{t:this.shape_2,p:{x:119.35}},{t:this.shape_18,p:{x:124.6}},{t:this.shape_17,p:{x:130}},{t:this.shape_16,p:{x:138.05}},{t:this.shape_15}]},1).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40,p:{x:48.8}},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_24,p:{x:101.475}},{t:this.shape_4,p:{x:110.975}},{t:this.shape_22,p:{x:118.875}},{t:this.shape_12,p:{x:129.575}},{t:this.shape_36,p:{x:138.2}},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:35.1,y:33.225}},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_11,p:{x:78.35,y:33.225}},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25}]},1).to({state:[{t:this.shape_14},{t:this.shape_50},{t:this.shape_12,p:{x:29.225}},{t:this.shape_11,p:{x:37.85,y:11.225}},{t:this.shape_10},{t:this.shape_9,p:{x:49.875}},{t:this.shape_49},{t:this.shape_40,p:{x:72.2}},{t:this.shape_32,p:{x:81.1,y:11.225}},{t:this.shape_48},{t:this.shape_47},{t:this.shape_13,p:{x:112.275}},{t:this.shape_46},{t:this.shape_45},{t:this.shape_4,p:{x:142.325}},{t:this.shape_44},{t:this.shape_16,p:{x:162.8}}]},1).to({state:[{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_12,p:{x:47.825}},{t:this.shape,p:{x:59.5}},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_40,p:{x:113.6}},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_18,p:{x:168.7}},{t:this.shape_17,p:{x:174.1}},{t:this.shape_52},{t:this.shape_51}]},1).to({state:[{t:this.shape_14},{t:this.shape_4,p:{x:21.325}},{t:this.shape_24,p:{x:29.225}},{t:this.shape_36,p:{x:37.85}},{t:this.shape_10},{t:this.shape_23},{t:this.shape_66},{t:this.shape_20,p:{x:74.675}},{t:this.shape_22,p:{x:81.975}},{t:this.shape_12,p:{x:92.675}},{t:this.shape_11,p:{x:101.3,y:11.225}},{t:this.shape_9,p:{x:108.825}},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,195.9,47);


(lib.label_green_circle_checl_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhOASIADgjICaABIAAAig");
	this.shape.setTransform(7.9,1.725);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle_checl_2, new cjs.Rectangle(0,0,15.8,3.5), null);


(lib.label_green_circle_check_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgygRIBjAAIACAhIhlACg");
	this.shape.setTransform(5.075,1.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle_check_1, new cjs.Rectangle(0,0,10.2,3.5), null);


(lib.greybg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.grey_color();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.greybg, new cjs.Rectangle(0,0,323,206), null);


(lib.endframe_video_line4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape.setTransform(153.075,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_1.setTransform(139.125,11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAIBRIgWg1IAAA1Ig4AAIAAihIBGAAQAcABAQAPQAQAOAAAXQAAANgIAMQgHAJgKAGIAkBEgAgOgOIAHAAQAFABADgEQADgDAAgFQAAgEgDgEQgDgCgFAAIgHAAg");
	this.shape_2.setTransform(126.725,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhAA8QgYgYAAgkQAAgjAYgYQAZgXAnAAQAoAAAZAXQAYAYAAAjQAAAkgYAYQgYAXgpAAQgnAAgZgXgAgUgZQgHAKAAAPQAAAQAHAKQAIAKAMAAQANAAAHgKQAIgKAAgQQAAgPgIgKQgHgKgNABQgMgBgIAKg");
	this.shape_3.setTransform(109.85,11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgrBFQgOgMgFgPIApgXQAKARALAAQAEAAAAgEQAAgEgJgGIgWgKQgegQAAgcQAAgWAQgPQARgNAaAAQAZAAASANQAPALADAOIgoAXQgIgOgKAAQgFAAAAADQAAAFAKAFIAWALQAfAQAAAaQAAAZgRAOQgRAOgdgBQgZAAgSgOg");
	this.shape_4.setTransform(94.1,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgrBFQgOgMgFgPIApgXQAKARALAAQAEAAAAgEQAAgEgJgGIgVgKQgggQAAgcQAAgWARgPQARgNAaAAQAYAAATANQAPALAEAOIgqAXQgGgOgLAAQgFAAAAADQAAAFAKAFIAVALQAgAQAAAaQAAAZgRAOQgRAOgcgBQgZAAgTgOg");
	this.shape_5.setTransform(80.85,11.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_6.setTransform(68.575,11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgtA9QgZgYAAglQAAgiAYgYQAYgYAmAAQAfAAAXAPIgSAvQgMgNgSAAQgOAAgIAJQgJAKAAAOQAAAPAJAKQAIAJAOAAQASAAANgNIASAuQgXARgggBQglAAgYgWg");
	this.shape_7.setTransform(55.425,11.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhAA8QgYgYAAgkQAAgjAYgYQAZgXAnAAQAoAAAZAXQAYAYAAAjQAAAkgYAYQgZAXgoAAQgnAAgZgXgAgUgZQgHAKAAAPQAAAQAHAKQAIAKAMAAQANAAAHgKQAIgKAAgQQAAgPgIgKQgHgKgNABQgMgBgIAKg");
	this.shape_8.setTransform(39.1,11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAIBRIgWg1IAAA1Ig4AAIAAihIBGAAQAcABAQAPQAQAOAAAXQAAANgIAMQgHAJgKAGIAkBEgAgOgOIAHAAQAFABADgEQADgDAAgFQAAgEgDgEQgDgCgFAAIgHAAg");
	this.shape_9.setTransform(23.325,11.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag+BRIAAihIBCAAQAbAAAQAQQARAPgBAYQAAAZgQAOQgQAQgaAAIgMAAIAAAzgAgHgLIAGAAQAEgBAEgDQADgDAAgGQAAgFgDgEQgDgCgFAAIgGAAg");
	this.shape_10.setTransform(9.05,11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAOBDIgcg/IAAA/IgwAAIAAiEIAxAAIAcA+IAAg+IAwAAIAACEg");
	this.shape_11.setTransform(208.075,9.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_12.setTransform(196.65,9.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAHBDIgSgrIAAArIgvAAIAAiEIA7AAQAWAAAOAMQANAMgBATQAAAKgFAKQgGAHgJAGIAeA4gAgLgLIAFAAQAFAAACgDQACgCAAgEQAAgEgCgDQgCgCgFAAIgFAAg");
	this.shape_13.setTransform(186.5,9.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAUBDIgGgVIgeAAIgGAVIgvAAIAuiEIAvAAIAuCEgAAGANIgGgbIgIAbIAOAAg");
	this.shape_14.setTransform(173.125,9.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgYBDIgviEIA0AAIATBMIAVhMIA0AAIgxCEg");
	this.shape_15.setTransform(158.8,9.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAHBDIgSgrIAAArIgvAAIAAiEIA6AAQAXAAANAMQAOAMAAATQgBAKgFAKQgGAHgKAGIAfA4gAgLgLIAFAAQAFAAABgDQADgCAAgEQAAgEgDgDQgBgCgFAAIgFAAg");
	this.shape_16.setTransform(145.95,9.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgoBDIAAiEIBSAAIAAAlIgjAAIAAALIAfAAIAAAiIgfAAIAAAMIAjAAIAAAmg");
	this.shape_17.setTransform(134.9,9.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_18.setTransform(122.05,9.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgXBDIAAhfIgaAAIAAglIBjAAIAAAlIgaAAIAABfg");
	this.shape_19.setTransform(112.175,9.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("Ag8BDIAAiEIAvAAQAiAAATARQAVASAAAeQAAAegVATQgTASgiAAgAgNAcIADAAQAWAAAAgcQAAgbgWAAIgDAAg");
	this.shape_20.setTransform(97.15,9.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgXBDIAAiEIAvAAIAACEg");
	this.shape_21.setTransform(87.625,9.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgsA0QgRgOABgZIAAhQIAwAAIAABRQAAAOAMAAQANAAAAgOIAAhRIAxAAIAABQQAAAZgQAOQgRAQgdAAQgcAAgQgQg");
	this.shape_22.setTransform(78.05,9.725);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgqBDIAAiEIAwAAIAABeIAlAAIAAAmg");
	this.shape_23.setTransform(67,9.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_24.setTransform(57.6,9.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgwAxQgTgTAAgeQAAgdATgTQAVgUAeAAQAkAAAVAWIgbAcQgLgJgNAAQgLAAgHAIQgIAHAAAMQAAANAHAIQAIAIALAAQAKAAAGgEIAAgFIgPAAIAAgfIA6AAIAAA6QgZAWgnAAQggAAgUgUg");
	this.shape_25.setTransform(45.825,9.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgXBDIAAhfIgaAAIAAglIBjAAIAAAlIgaAAIAABfg");
	this.shape_26.setTransform(30.125,9.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgpBDIAAiEIBTAAIAAAlIgjAAIAAALIAfAAIAAAiIgfAAIAAAMIAjAAIAAAmg");
	this.shape_27.setTransform(20.2,9.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AAOBDIAAgwIgbAAIAAAwIgxAAIAAiEIAxAAIAAAvIAbAAIAAgvIAwAAIAACEg");
	this.shape_28.setTransform(8.8,9.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,261.1,27);


(lib.endframe_video_line3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AgyBRIAAihIA5AAIAABzIAsAAIAAAug");
	this.shape.setTransform(209.025,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_1.setTransform(197.525,11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E90000").s().p("AgcBRIAAhzIggAAIAAguIB5AAIAAAuIggAAIAABzg");
	this.shape_2.setTransform(185.425,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E90000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape_3.setTransform(170.725,11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("AgcBRIAAihIA5AAIAAChg");
	this.shape_4.setTransform(158.875,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_5.setTransform(145.175,11.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AgcBRIAAihIA5AAIAAChg");
	this.shape_6.setTransform(135.875,11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AgcBRIAAhzIggAAIAAguIB5AAIAAAuIggAAIAABzg");
	this.shape_7.setTransform(125.875,11.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AAYBRIgGgZIglAAIgHAZIg6AAIA5ihIA4AAIA3ChgAAIAPIgIggIgJAgIARAAg");
	this.shape_8.setTransform(110.9,11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AAIBRIgWg1IAAA1Ig4AAIAAihIBGAAQAcABAQAPQAQAOAAAXQAAANgIAMQgHAJgKAGIAkBEgAgOgOIAHAAQAFABADgEQADgDAAgFQAAgEgDgEQgDgCgFAAIgHAAg");
	this.shape_9.setTransform(95.675,11.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_10.setTransform(82.175,11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E90000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape_11.setTransform(68.175,11.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_12.setTransform(54.225,11.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E90000").s().p("Ag7A8QgXgYABgkQAAgiAXgYQAYgYAmAAQArAAAaAaIghAiQgNgLgQAAQgNAAgKAJQgIAKgBAOQAAAQAJAJQAJAKAOAAQANAAAGgFIAAgFIgSAAIAAgnIBHAAIAABHQgeAbgxgBQgmAAgZgXg");
	this.shape_13.setTransform(39.8,11.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_14.setTransform(21.275,11.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E90000").s().p("AguBFQgRgPAAgZQAAgVAQgNQgNgLAAgSQAAgUAQgOQARgOAbAAQAcAAARAOQAQAOAAAUQAAASgNALQAQANAAAVQAAAZgRAPQgRAOgeAAQgdAAgRgOgAgGAUQgCAEAAAEQAAAOAIAAQAJAAAAgOQAAgEgCgEQgCgEgFAAQgEAAgCAEgAgGgeQAAALAGAAQAHAAAAgLQAAgLgHAAQgGAAAAALg");
	this.shape_15.setTransform(8.775,11.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_video_line3, new cjs.Rectangle(0,0,243.1,27), null);


(lib.endframe_video_line2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgcBRIAAhzIggAAIAAguIB5AAIAAAuIggAAIAABzg");
	this.shape.setTransform(178.675,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_1.setTransform(166.575,11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAqBRIAAhOIgQBCIgzAAIgQhCIAABOIg5AAIAAihIBJAAIAZBbIAZhbIBJAAIAAChg");
	this.shape_2.setTransform(150.35,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape_3.setTransform(127.125,11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_4.setTransform(113.175,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgcBRIAAhzIggAAIAAguIB5AAIAAAuIggAAIAABzg");
	this.shape_5.setTransform(101.075,11.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgtA9QgZgYAAglQAAgiAYgYQAYgYAmAAQAfAAAXAPIgSAvQgMgNgSAAQgOAAgIAJQgJAKAAAOQAAAPAJAKQAIAJAOAAQASAAANgNIASAuQgXARgggBQglAAgYgWg");
	this.shape_6.setTransform(87.225,11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag2BAQgTgTAAgdIAAhhIA6AAIAABiQAAARAPAAQAQAAAAgRIAAhiIA6AAIAABhQAAAdgTATQgUASgjAAQgjAAgTgSg");
	this.shape_7.setTransform(71.875,11.35);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhJBRIAAihIA6AAQAnAAAZAWQAZAVAAAlQAAAlgZAVQgZAXgnAAgAgQAjIADAAQAbAAAAgjQAAghgbgBIgDAAg");
	this.shape_8.setTransform(56.225,11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AhAA8QgYgYAAgkQAAgjAYgYQAZgXAnAAQAoAAAZAXQAYAYAAAjQAAAkgYAYQgZAXgoAAQgnAAgZgXgAgUgZQgHAKAAAPQAAAQAHAKQAIAKAMAAQANAAAHgKQAIgKAAgQQAAgPgIgKQgHgKgNABQgMgBgIAKg");
	this.shape_9.setTransform(39.1,11.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAIBRIgWg1IAAA1Ig4AAIAAihIBGAAQAcABAQAPQAQAOAAAXQAAANgIAMQgHAJgKAGIAkBEgAgOgOIAHAAQAFABADgEQADgDAAgFQAAgEgDgEQgDgCgFAAIgHAAg");
	this.shape_10.setTransform(23.325,11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag+BRIAAihIBCAAQAbAAAQAQQARAPgBAYQAAAZgQAOQgQAQgaAAIgMAAIAAAzgAgHgLIAGAAQAEgBAEgDQADgDAAgGQAAgFgDgEQgDgCgFAAIgGAAg");
	this.shape_11.setTransform(9.05,11.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_video_line2, new cjs.Rectangle(0,0,244.1,27), null);


(lib.endframe_video_line1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape.setTransform(168.475,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhEBRIAAgeIA9hVIg7AAIAAguICGAAIAAAeIg9BVIA+AAIAAAug");
	this.shape_1.setTransform(155.575,11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape_2.setTransform(140.125,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhAA8QgYgYAAgkQAAgjAYgYQAZgXAnAAQAoAAAZAXQAYAYAAAjQAAAkgYAYQgZAXgoAAQgnAAgZgXgAgUgZQgHAKAAAPQAAAQAHAKQAIAKAMAAQANAAAHgKQAIgKAAgQQAAgPgIgKQgHgKgNABQgMgBgIAKg");
	this.shape_3.setTransform(122.8,11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgyBRIAAihIA5AAIAABzIAsAAIAAAug");
	this.shape_4.setTransform(103.925,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAYBRIgHgZIgkAAIgIAZIg5AAIA4ihIA5AAIA4ChgAAHAPIgIggIgJAgIARAAg");
	this.shape_5.setTransform(89.55,11.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AALBRIgbhDIAABDIg6AAIAAihIA6AAIAAA7IAag7IBAAAIgnBOIAoBTg");
	this.shape_6.setTransform(68.8,11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgqBNIAGgsIAMACQAKAAAAgJIAAhqIA6AAIAABzQAAAug2AAQgQAAgQgEg");
	this.shape_7.setTransform(55.55,11.275);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgcBRIAAihIA5AAIAAChg");
	this.shape_8.setTransform(47.375,11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AALBRIgbhDIAABDIg6AAIAAihIA6AAIAAA7IAag7IBAAAIgnBOIAoBTg");
	this.shape_9.setTransform(35.95,11.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_10.setTransform(22.025,11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AhBBRIAAihIBEAAQAcAAAQANQAQAMAAAVQAAAUgRAMQAJADAGAKQAFAJAAAKQAAAYgQANQgQANgbABgAgJAlIAJAAQAEAAAEgDQADgDAAgEQAAgFgDgDQgEgEgEABIgJAAgAgJgSIAJAAQAJgBAAgIQAAgKgJABIgJAAg");
	this.shape_11.setTransform(9.325,11.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_video_line1, new cjs.Rectangle(0,0,225,27), null);


(lib.endframe_tv_audio_line4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape.setTransform(194.875,10.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_1.setTransform(182.125,10.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAHBJIgTgvIAAAvIg0AAIAAiRIBAAAQAagBAPAOQANANAAAVQAAAMgGALQgGAIgKAGIAhA9gAgMgMIAGAAQAEAAACgEQAEgCAAgFQAAgEgEgCQgCgEgEAAIgGAAg");
	this.shape_2.setTransform(170.8,10.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAXBJIgHgWIgiAAIgGAWIg0AAIAziRIAzAAIAzCRgAAHAOIgHgeIgJAeIAQAAg");
	this.shape_3.setTransform(155.85,10.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgaBJIg1iRIA6AAIAWBUIAXhUIA4AAIg1CRg");
	this.shape_4.setTransform(139.925,10.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAHBJIgUgvIAAAvIgzAAIAAiRIBAAAQAagBAOAOQAPANAAAVQgBAMgGALQgHAIgKAGIAiA9gAgNgMIAHAAQAEAAADgEQACgCAAgFQAAgEgCgCQgDgEgEAAIgHAAg");
	this.shape_5.setTransform(125.65,10.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_6.setTransform(113.325,10.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_7.setTransform(98.875,10.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgaBJIAAhoIgcAAIAAgpIBtAAIAAApIgdAAIAABog");
	this.shape_8.setTransform(87.8,10.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgnA/QgNgLgFgOIAmgVQAJAQAKAAQAEAAAAgEQAAgEgIgFIgUgJQgcgOAAgaQAAgVAPgNQAPgMAYAAQAWAAARAMQANAKAEAMIglAWQgHgOgKAAQgEAAAAAEQAAAEAJAFIAUAKQAdAOAAAZQAAAVgQANQgQANgaAAQgWAAgRgNg");
	this.shape_9.setTransform(71.825,10.425);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgYAWQALgIAFgJQgSgGAAgQQAAgLAIgGQAHgIALABQAMgBAHAIQAHAGAAANQAAAQgIAPQgJARgPAJg");
	this.shape_10.setTransform(62.25,6.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgaBJIg1iRIA6AAIAWBUIAXhUIA4AAIg1CRg");
	this.shape_11.setTransform(50.575,10.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgZBJIAAhoIgeAAIAAgpIBvAAIAAApIgdAAIAABog");
	this.shape_12.setTransform(36.55,10.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_13.setTransform(21.475,10.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhCBJIAAiRIA0AAQAkgBAWAUQAXATAAAiQAAAigWAUQgXATgkAAgAgPAgIADAAQAZAAAAggQAAgfgZABIgDAAg");
	this.shape_14.setTransform(9.375,10.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAOBDIgcg/IAAA/IgwAAIAAiEIAxAAIAcA+IAAg+IAwAAIAACEg");
	this.shape_15.setTransform(208.075,9.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_16.setTransform(196.65,9.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAHBDIgSgrIAAArIgvAAIAAiEIA7AAQAWAAAOAMQANAMgBATQAAAKgFAKQgGAHgJAGIAeA4gAgLgLIAFAAQAFAAACgDQACgCAAgEQAAgEgCgDQgCgCgFAAIgFAAg");
	this.shape_17.setTransform(186.5,9.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAUBDIgGgVIgeAAIgGAVIgvAAIAuiEIAvAAIAuCEgAAGANIgGgbIgIAbIAOAAg");
	this.shape_18.setTransform(173.125,9.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgYBDIgviEIA0AAIATBMIAVhMIA0AAIgxCEg");
	this.shape_19.setTransform(158.8,9.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAHBDIgSgrIAAArIgvAAIAAiEIA6AAQAXAAANAMQAOAMAAATQgBAKgFAKQgGAHgKAGIAfA4gAgLgLIAFAAQAFAAABgDQADgCAAgEQAAgEgDgDQgBgCgFAAIgFAAg");
	this.shape_20.setTransform(145.95,9.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgoBDIAAiEIBSAAIAAAlIgjAAIAAALIAfAAIAAAiIgfAAIAAAMIAjAAIAAAmg");
	this.shape_21.setTransform(134.9,9.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_22.setTransform(122.05,9.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgXBDIAAhfIgaAAIAAglIBjAAIAAAlIgaAAIAABfg");
	this.shape_23.setTransform(112.175,9.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("Ag8BDIAAiEIAvAAQAiAAATARQAVASAAAeQAAAegVATQgTASgiAAgAgNAcIADAAQAWAAAAgcQAAgbgWAAIgDAAg");
	this.shape_24.setTransform(97.15,9.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgXBDIAAiEIAvAAIAACEg");
	this.shape_25.setTransform(87.625,9.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgsA0QgRgOABgZIAAhQIAwAAIAABRQAAAOAMAAQANAAAAgOIAAhRIAxAAIAABQQAAAZgQAOQgRAQgdAAQgcAAgQgQg");
	this.shape_26.setTransform(78.05,9.725);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgqBDIAAiEIAwAAIAABeIAlAAIAAAmg");
	this.shape_27.setTransform(67,9.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_28.setTransform(57.6,9.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgwAxQgTgTAAgeQAAgdATgTQAVgUAeAAQAkAAAVAWIgbAcQgLgJgNAAQgLAAgHAIQgIAHAAAMQAAANAHAIQAIAIALAAQAKAAAGgEIAAgFIgPAAIAAgfIA6AAIAAA6QgZAWgnAAQggAAgUgUg");
	this.shape_29.setTransform(45.825,9.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgXBDIAAhfIgaAAIAAglIBjAAIAAAlIgaAAIAABfg");
	this.shape_30.setTransform(30.125,9.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgpBDIAAiEIBTAAIAAAlIgjAAIAAALIAfAAIAAAiIgfAAIAAAMIAjAAIAAAmg");
	this.shape_31.setTransform(20.2,9.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AAOBDIAAgwIgbAAIAAAwIgxAAIAAiEIAxAAIAAAvIAbAAIAAgvIAwAAIAACEg");
	this.shape_32.setTransform(8.8,9.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,245,25);


(lib.endframe_tv_audio_line3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAmBJIAAhHIgPA8IguAAIgOg8IAABHIg0AAIAAiRIBDAAIAWBSIAXhSIBDAAIAACRg");
	this.shape.setTransform(182.675,10.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag7A2QgWgVABghQgBggAWgVQAXgWAkAAQAlAAAWAWQAXAVAAAgQAAAhgXAVQgWAWglAAQgkAAgXgWgAgSgWQgGAIAAAOQAAAOAGAJQAHAJALAAQAMAAAHgJQAHgJgBgOQABgOgHgIQgHgKgMAAQgLAAgHAKg");
	this.shape_1.setTransform(164.8,10.425);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgnA/QgNgLgFgOIAmgVQAJAQAKAAQAEAAAAgEQAAgEgIgFIgUgJQgcgOAAgaQAAgVAPgNQAPgMAYAAQAWAAARAMQANAKAEAMIglAWQgHgOgKAAQgEAAAAAEQAAAEAJAFIAUAKQAdAOAAAZQAAAVgQANQgQANgaAAQgWAAgRgNg");
	this.shape_2.setTransform(146.375,10.425);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_3.setTransform(135.425,10.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_4.setTransform(124.925,10.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAKBJIgZg8IAAA8Ig1AAIAAiRIA1AAIAAA1IAZg1IA5AAIgkBGIAmBLg");
	this.shape_5.setTransform(112.6,10.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_6.setTransform(97.525,10.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgZBJIAAiRIA0AAIAACRg");
	this.shape_7.setTransform(86.7,10.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AASBJIgShLIgRBLIg1AAIgmiRIA4AAIAOBQIAShQIApAAIASBQIAOhQIA5AAIgmCRg");
	this.shape_8.setTransform(72.3,10.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_9.setTransform(51.975,10.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag+BJIAAgbIA3hOIg2AAIAAgoIB7AAIAAAbIg4BNIA5AAIAAApg");
	this.shape_10.setTransform(40.225,10.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_11.setTransform(26.125,10.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Ag7A2QgVgVAAghQAAggAVgVQAXgWAkAAQAlAAAXAWQAVAVAAAgQAAAhgVAVQgXAWglAAQgkAAgXgWgAgSgWQgGAIgBAOQABAOAGAJQAHAJALAAQAMAAAHgJQAGgJABgOQgBgOgGgIQgHgKgMAAQgLAAgHAKg");
	this.shape_12.setTransform(10.3,10.425);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_tv_audio_line3, new cjs.Rectangle(0,0,194.5,25), null);


(lib.endframe_tv_audio_line2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape.setTransform(208.625,10.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAWBJIgGgWIghAAIgHAWIg0AAIAziRIA0AAIAyCRgAAHAOIgHgeIgIAeIAPAAg");
	this.shape_1.setTransform(193.25,10.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgaBJIg1iRIA6AAIAWBUIAXhUIA4AAIg1CRg");
	this.shape_2.setTransform(177.325,10.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_3.setTransform(157.575,10.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgtBcIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApgAgXg7IAPggIAwAAIgZAgg");
	this.shape_4.setTransform(144.825,8.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgtBcIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApgAgXg7IAPggIAwAAIgZAgg");
	this.shape_5.setTransform(134.425,8.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AgnA/QgNgLgFgOIAmgVQAJAQAKAAQAEAAAAgEQAAgEgIgFIgUgJQgcgOAAgaQAAgVAPgNQAPgMAYAAQAWAAARAMQANAKAEAMIglAWQgHgOgKAAQgEAAAAAEQAAAEAJAFIAUAKQAdAOAAAZQAAAVgQANQgQANgaAAQgWAAgRgNg");
	this.shape_6.setTransform(119.075,10.425);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("Ag1A2QgWgVABghQAAggAVgVQAXgWAiAAQAnAAAYAXIgeAgQgNgKgPAAQgLAAgJAIQgIAJAAANQAAAOAIAJQAJAJAMAAQALAAAHgEIAAgGIgQAAIAAgiIBAAAIAABAQgcAYgrAAQgkAAgWgWg");
	this.shape_7.setTransform(105.1,10.425);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_8.setTransform(89.875,10.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AAXBJIgHgWIgiAAIgGAWIg0AAIAziRIAzAAIAzCRgAAHAOIgHgeIgJAeIAQAAg");
	this.shape_9.setTransform(74.5,10.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_10.setTransform(61.775,10.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E90000").s().p("AAmBJIAAhHIgPA8IguAAIgOg8IAABHIg0AAIAAiRIBDAAIAWBSIAXhSIBDAAIAACRg");
	this.shape_11.setTransform(42.825,10.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("Ag7A2QgWgVABghQgBggAWgVQAXgWAkAAQAlAAAWAWQAXAVAAAgQAAAhgXAVQgWAWglAAQgkAAgXgWgAgSgWQgGAIAAAOQAAAOAGAJQAHAJALAAQAMAAAHgJQAHgJAAgOQAAgOgHgIQgHgKgMAAQgLAAgHAKg");
	this.shape_12.setTransform(24.95,10.425);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E90000").s().p("AAKBJIgZg8IAAA8Ig1AAIAAiRIA1AAIAAA1IAZg1IA5AAIgkBGIAmBLg");
	this.shape_13.setTransform(9.55,10.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_tv_audio_line2, new cjs.Rectangle(0,0,218.4,25), null);


(lib.endframe_tv_audio_line1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAARIAjAAIAAAmIgjAAIAAAxg");
	this.shape.setTransform(173.7,10.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag7A2QgVgVgBghQABggAVgVQAXgWAkAAQAlAAAXAWQAVAVAAAgQAAAhgVAVQgXAWglAAQgkAAgXgWgAgSgWQgGAIgBAOQABAOAGAJQAHAJALAAQAMAAAHgJQAGgJABgOQgBgOgGgIQgHgKgMAAQgLAAgHAKg");
	this.shape_1.setTransform(160.25,10.425);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_2.setTransform(142.725,10.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_3.setTransform(129.975,10.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgaBJIAAiRIA0AAIAACRg");
	this.shape_4.setTransform(119.15,10.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_5.setTransform(110.925,10.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_6.setTransform(98.075,10.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag6A2QgXgVAAghQAAggAXgVQAWgWAkAAQAlAAAWAWQAXAVgBAgQABAhgXAVQgWAWglAAQgkAAgWgWgAgSgWQgHAIABAOQgBAOAHAJQAHAJALAAQAMAAAHgJQAGgJAAgOQAAgOgGgIQgHgKgMAAQgLAAgHAKg");
	this.shape_7.setTransform(82.25,10.425);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_8.setTransform(64.975,10.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_9.setTransform(54.475,10.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgZBJIAAhoIgdAAIAAgpIBuAAIAAApIgdAAIAABog");
	this.shape_10.setTransform(43.4,10.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgnA/QgNgLgFgOIAmgVQAJAQAKAAQAEAAAAgEQAAgEgIgFIgUgJQgcgOAAgaQAAgVAPgNQAPgMAYAAQAWAAARAMQANAKAEAMIglAWQgHgOgKAAQgEAAAAAEQAAAEAJAFIAUAKQAdAOAAAZQAAAVgQANQgQANgaAAQgWAAgRgNg");
	this.shape_11.setTransform(31.475,10.425);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_12.setTransform(20.275,10.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("Ag8BJIAAiRIA/AAQAZAAAPALQAPALAAATQgBASgPALQAJADAEAJQAGAIAAAJQgBAXgPAMQgOAMgYgBgAgJAiIAJAAQAEAAADgDQADgCgBgFQABgEgDgDQgDgDgEAAIgJAAgAgJgRIAJAAQAIAAAAgIQAAgJgIAAIgJAAg");
	this.shape_13.setTransform(8.7,10.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_tv_audio_line1, new cjs.Rectangle(0,0,225,25), null);


(lib.emptyMovieClip = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.dollar_text_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape.setTransform(258.05,6.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_1.setTransform(252.375,6.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgZAeQgKgIABgOIAAguIAcAAIAAAuQgBAIAHAAQAHAAABgIIAAguIAcAAIAAAuQAAAOgKAIQgKAJgQAAQgQAAgJgJg");
	this.shape_2.setTransform(244.55,6.475);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgUAlIADgVIAGABQAFAAAAgEIAAgzIAbAAIAAA3QAAAWgZAAQgIAAgIgCg");
	this.shape_3.setTransform(238.25,6.425);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgcAnIAAgJQAAgIAFgHQADgFAHgGIALgIQAEgFAAgEQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAAAAAQgEAAgCAGIgXgFQACgKAHgGQAHgHANAAQANAAAHAGQAJAHAAALQAAAMgMAJIgJAHQgEADAAABIAaAAIAAAVg");
	this.shape_4.setTransform(230.8,6.325);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgbAdQgLgMAAgRQgBgQAMgMQALgLASAAQAUAAANANIgQAQQgGgFgIAAQgFgBgFAFQgFAFABAGQAAAIADAEQAFAFAGgBQAGAAAEgCIAAgCIgJAAIAAgSIAhAAIAAAiQgOAMgXAAQgSAAgLgLg");
	this.shape_5.setTransform(221.35,6.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AALAnIgCgMIgRAAIgEAMIgcAAIAbhNIAaAAIAbBNgAADAHIgDgOIgEAOIAHAAg");
	this.shape_6.setTransform(213.25,6.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgiAnIAAhNIAbAAQATABALAJQAMALAAARQAAARgMALQgLALgTAAgAgHAQIABAAQANAAAAgQQAAgPgNAAIgBAAg");
	this.shape_7.setTransform(205.575,6.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_8.setTransform(197.775,6.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgeAdQgMgMAAgRQAAgQAMgMQALgLATAAQATAAAMALQAMAMAAAQQAAARgMAMQgMALgTAAQgTAAgLgLgAgJgLQgDAFAAAGQAAAIADAEQAEAFAFAAQAGAAAEgFQAEgEAAgIQAAgGgEgFQgEgFgGAAQgFAAgEAFg");
	this.shape_9.setTransform(189.5,6.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AggAnIAAgPIAdgoIgcAAIAAgWIBAAAIAAAPIgeAoIAeAAIAAAWg");
	this.shape_10.setTransform(181.725,6.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAUAnIAAglIgIAeIgYAAIgHgeIAAAlIgbAAIAAhNIAjAAIALArIAMgrIAjAAIAABNg");
	this.shape_11.setTransform(171.2,6.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgbAvIAhhdIAVAAIgfBdg");
	this.shape_12.setTransform(163.5,6.725);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgNAnIAAg3IgPAAIAAgWIA5AAIAAAWIgPAAIAAA3g");
	this.shape_13.setTransform(157.775,6.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_14.setTransform(150.9,6.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgXAnIAAhNIAvAAIAAAWIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAWg");
	this.shape_15.setTransform(146.45,6.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAUAnIAAglIgIAeIgYAAIgHgeIAAAlIgbAAIAAhNIAjAAIALArIAMgrIAjAAIAABNg");
	this.shape_16.setTransform(138.7,6.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAAAnIAAgQIggAAIAAgQIAdgtIAdAAIAAApIAHAAIAAAUIgHAAIAAAQgAgIADIAIAAIAAgOg");
	this.shape_17.setTransform(128.125,6.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgcAnIAAgJQAAgIAFgHQADgFAIgGIAKgIQAEgFAAgEQAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAAAAAQgEAAgCAGIgXgFQACgKAHgGQAHgHANAAQANAAAHAGQAJAHAAALQAAAMgMAJIgJAHQgEADAAABIAaAAIAAAVg");
	this.shape_18.setTransform(121.55,6.325);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgbAdQgLgMAAgRQAAgQALgMQALgLASAAQAUAAANANIgQAQQgGgFgIAAQgFgBgFAFQgEAFAAAGQAAAIADAEQAFAFAGgBQAGAAAEgCIAAgCIgJAAIAAgSIAhAAIAAAiQgOAMgXAAQgSAAgLgLg");
	this.shape_19.setTransform(112.1,6.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AALAnIgDgMIgRAAIgDAMIgbAAIAahNIAbAAIAbBNgAADAHIgDgOIgEAOIAHAAg");
	this.shape_20.setTransform(104,6.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgiAnIAAhNIAbAAQATABALAJQAMALAAARQAAARgMALQgLALgTAAgAgHAQIABAAQANAAAAgQQAAgPgNAAIgBAAg");
	this.shape_21.setTransform(96.325,6.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgUAlIADgVIAGABQAFAAAAgEIAAgzIAbAAIAAA3QAAAWgZAAQgIAAgIgCg");
	this.shape_22.setTransform(90.05,6.425);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_23.setTransform(86.15,6.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAEAnIgKgaIAAAaIgbAAIAAhNIAhAAQANABAIAGQAHAIABAKQgBAHgDAFQgDAFgFACIARAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQABAAAAgBQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape_24.setTransform(81.2,6.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgOAnIgbhNIAeAAIALAsIAMgsIAeAAIgcBNg");
	this.shape_25.setTransform(73.2,6.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_26.setTransform(62.875,6.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AALAnIgCgMIgRAAIgEAMIgcAAIAbhNIAaAAIAbBNgAADAHIgDgOIgEAOIAHAAg");
	this.shape_27.setTransform(54.85,6.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgNAnIgchNIAeAAIALAsIANgsIAdAAIgcBNg");
	this.shape_28.setTransform(46.5,6.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgbAdQgLgMAAgRQgBgQAMgMQALgLASAAQAUAAANANIgQAQQgGgFgIAAQgFgBgFAFQgFAFABAGQAAAIADAEQAFAFAGgBQAGAAAEgCIAAgCIgJAAIAAgSIAhAAIAAAiQgOAMgXAAQgSAAgLgLg");
	this.shape_29.setTransform(35.95,6.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_30.setTransform(30.2,6.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgiAnIAAhNIAbAAQATABALAJQAMALAAARQAAARgMALQgLALgTAAgAgHAQIABAAQANAAAAgQQAAgPgNAAIgBAAg");
	this.shape_31.setTransform(24.875,6.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgXAnIAAhNIAbAAIAAA3IAUAAIAAAWg");
	this.shape_32.setTransform(18.45,6.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgXAnIAAhNIAvAAIAAAWIgUAAIAAAGIASAAIAAAUIgSAAIAAAHIAUAAIAAAWg");
	this.shape_33.setTransform(12.95,6.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgbAdQgMgMAAgRQAAgQAMgMQAMgLARAAQAVAAAMANIgQAQQgGgFgIAAQgGgBgEAFQgFAFABAGQAAAIAEAEQAEAFAGgBQAGAAADgCIAAgCIgIAAIAAgSIAhAAIAAAiQgOAMgXAAQgSAAgLgLg");
	this.shape_34.setTransform(6.05,6.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_3, new cjs.Rectangle(0,0,262,15), null);


(lib.dollar_text_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AhZBiIAAjDIBGAAQAxAAAdAaQAfAbAAAsQAAAtgeAaQgeAbgxAAgAgUAqIAEAAQAhAAAAgqQAAgpghAAIgEAAg");
	this.shape.setTransform(237.925,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("Ag+BiIAAjDIBGAAIAACMIA2AAIAAA3g");
	this.shape_1.setTransform(221.55,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E90000").s().p("AgeBUQgagRgKgdIgVAAIAAgiIANAAIAAgGIgNAAIAAgkIAVAAQALgbAZgRQAbgTAgAAQAiAAAXAOIgXAvQgOgIgQAAQgQAAgMAKIAsAAIgDAkIg0AAIAAADIAAADIAzAAIgDAiIgkAAQAKAKATAAQARAAAOgJIAVAuQgTARgjAAQgkAAgbgSg");
	this.shape_2.setTransform(204.5,13.175);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E90000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_3.setTransform(184.575,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_4.setTransform(167.35,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AgjBiIAAjDIBGAAIAADDg");
	this.shape_5.setTransform(156.05,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_6.setTransform(143.875,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_7.setTransform(125.975,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_8.setTransform(105.525,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AANBiIghhRIAABRIhHAAIAAjDIBHAAIAABHIAghHIBOAAIgxBeIAzBlg");
	this.shape_9.setTransform(85.6,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_10.setTransform(65.175,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E90000").s().p("AgjBiIhHjDIBNAAIAeByIAfhyIBLAAIhHDDg");
	this.shape_11.setTransform(43.9,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_12.setTransform(20.65,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E90000").s().p("Ag0BeIAHg2QAIACAHAAQANAAAAgKIAAiCIBGAAIAACNQAAA4hBAAQgUAAgUgFg");
	this.shape_13.setTransform(7.6,13.275);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_2, new cjs.Rectangle(0,0,249.3,32), null);


(lib.dollar_text_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape.setTransform(202.525,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgiBiIAAjDIBFAAIAADDg");
	this.shape_1.setTransform(190.35,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhCBNQgXgVAAglIAAh2IBHAAIAAB4QAAAUASAAQATAAAAgUIAAh4IBHAAIAAB2QAAAlgXAVQgYAXgrAAQgqAAgYgXg");
	this.shape_2.setTransform(176.075,13.375);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_3.setTransform(152.675,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_4.setTransform(137.7,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("Ag9BiIAAjDIBFAAIAACMIA2AAIAAA3g");
	this.shape_5.setTransform(124.2,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ag9BiIAAjDIBGAAIAACMIA2AAIAAA3g");
	this.shape_6.setTransform(110.55,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_7.setTransform(93.075,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag+BiIAAjDIBGAAIAACMIA2AAIAAA3g");
	this.shape_8.setTransform(70.7,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_9.setTransform(53.225,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_10.setTransform(32.425,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAVBiIAAhHIgpAAIAABHIhHAAIAAjDIBHAAIAABGIApAAIAAhGIBHAAIAADDg");
	this.shape_11.setTransform(12.025,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_1, new cjs.Rectangle(0,0,212.5,32), null);


(lib.dollar_prijs = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgQAJIAAgRIAhAAIAAARg");
	this.shape.setTransform(127.4,7.125);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgHAIQgEgDABgFQgBgDAEgEQADgEAEAAQAFAAADAEQAEAEgBADQABAFgEADQgDADgFABQgEgBgDgDg");
	this.shape_1.setTransform(123.7,9.65);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AADAoIAAgSIgiAAIAAgOIAfgvIAXAAIAAAtIAJAAIAAAQIgJAAIAAASgAgLAGIAOAAIAAgYg");
	this.shape_2.setTransform(118.775,6.625);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AADAoIAAgSIgiAAIAAgOIAfgvIAXAAIAAAtIAJAAIAAAQIgJAAIAAASgAgLAGIAOAAIAAgYg");
	this.shape_3.setTransform(111.875,6.625);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgSAoIAYg+IggAAIAAgRIA2AAIAAAOIgZBBg");
	this.shape_4.setTransform(105.4,6.625);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgHAbQgEgCAAgGQAAgEAEgEQADgDAEAAQAEAAAEADQADAEABAEQgBAGgDACQgEAEgEAAQgEAAgDgEgAgHgKQgEgDAAgFQAAgFAEgEQADgDAEAAQAEAAAEADQADAEABAFQgBAFgDADQgEADgEAAQgEAAgDgDg");
	this.shape_5.setTransform(98.65,7.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAHApIgQgcIAAAcIgTAAIAAhSIATAAIAAAuIAPgXIAWAAIgUAbIAVAgg");
	this.shape_6.setTransform(94.225,6.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgPAXQgJgJAAgOQAAgNAJgJQAJgJANAAQAKAAAIAGIgFAQQgFgEgHAAQgHAAgCAEQgEAEAAAFQAAAHAEAEQADAEAHAAQAGAAAFgEIAFAPQgIAGgKAAQgNAAgJgJg");
	this.shape_7.setTransform(88.025,7.625);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgUAbQgGgGAAgKQAAgJAGgEQAHgFAKAAQAGAAAFACIAAgCQAAgHgIAAQgIAAgHAEIgIgNQALgIANAAQAOAAAHAHQAFAGAAAMIAAAkIgRAAIgBgDQgFAFgIAAQgJAAgHgFgAgFAGQgCACAAADQAAADACABQACACADAAQAIAAAAgGQAAgBAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQgDgCgDAAQgDAAgCACg");
	this.shape_8.setTransform(82.1,7.625);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgIAoIgFgEIAAAFIgSAAIAAhSIATAAIAAAbIAFgEQAGgCAFAAQANAAAHAJQAIAIAAAOQAAAOgIAIQgJAJgNAAQgEAAgGgCgAgJAAQgDAEAAAHQgBAGAEAEQAEAEAFAAQAFAAAEgEQAEgEAAgGQAAgHgEgEQgEgDgFAAQgFAAgEADg");
	this.shape_9.setTransform(75.75,6.575);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAIApIAAgkQAAgHgIAAQgDAAgCACQgCACgBADIAAAkIgSAAIAAhSIASAAIAAAdQAFgHAKgBQAMAAAFAKQADAGAAAMIAAAhg");
	this.shape_10.setTransform(68.8,6.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgOAcQgGgCgDgFIALgKQAGAFAHAAQAEAAABgDQgBgCgEgCIgIgDQgOgGAAgMQAAgJAHgFQAGgFAJAAQAOAAAIALIgNAKQgEgFgFAAQgEAAAAADQAAACAEACIAJADQAOAFAAAMQAAAJgHAFQgHAGgKAAQgIAAgGgEg");
	this.shape_11.setTransform(62.95,7.625);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgUAbQgGgGAAgKQAAgJAGgEQAGgFALAAQAGAAAFACIAAgCQAAgHgJAAQgHAAgHAEIgIgNQAMgIAMAAQAOAAAHAHQAFAGAAAMIAAAkIgRAAIgBgDQgFAFgIAAQgJAAgHgFgAgEAGQgDACAAADQAAADADABQACACACAAQAIAAAAgGQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQgDgCgDAAQgCAAgCACg");
	this.shape_12.setTransform(57.25,7.625);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgPAXQgJgJAAgOQAAgNAJgJQAJgJANAAQAKAAAIAGIgFAQQgFgEgHAAQgHAAgCAEQgEAEAAAFQAAAHAEAEQADAEAHAAQAGAAAFgEIAFAPQgIAGgKAAQgNAAgJgJg");
	this.shape_13.setTransform(51.575,7.625);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgUAbQgGgGAAgKQAAgJAGgEQAHgFAKAAQAGAAAFACIAAgCQAAgHgIAAQgIAAgHAEIgIgNQALgIANAAQAOAAAHAHQAFAGAAAMIAAAkIgRAAIgBgDQgFAFgIAAQgJAAgHgFgAgFAGQgCACAAADQAAADACABQACACADAAQAIAAAAgGQAAgBAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQgDgCgDAAQgDAAgCACg");
	this.shape_14.setTransform(43.4,7.625);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAIAfIAAgjQAAgIgIAAQgDgBgCADQgCACAAAEIAAAjIgTAAIAAg8IATAAIAAAHQAFgIAJAAQAMAAAFAKQADAGAAAMIAAAhg");
	this.shape_15.setTransform(37.225,7.55);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgOAcQgGgCgDgFIALgKQAGAFAGAAQAGAAgBgDQAAgCgDgCIgJgDQgOgGAAgMQAAgJAHgFQAGgFAKAAQAOAAAHALIgMAKQgFgFgGAAQgDAAAAADQAAACADACIAKADQAOAFAAAMQAAAJgHAFQgHAGgKAAQgHAAgHgEg");
	this.shape_16.setTransform(29.15,7.625);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgOA1IAAgQIACAAQAIAAAAgGIAAg7IASAAIAAA7QAAAXgYAAIgEgBgAgCgiQgDgDAAgFQAAgFADgDQACgCAFAAQAEAAADACQADADAAAFQAAAFgDADQgDACgEAAQgFAAgCgCg");
	this.shape_17.setTransform(24.65,7.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgJAqIAAg7IASAAIAAA7gAgHgXQgDgDAAgFQAAgFADgDQADgCAEAAQAEAAADACQAEADAAAFQAAAFgEADQgDACgEAAQgEAAgDgCg");
	this.shape_18.setTransform(22.125,6.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgRAfIAAg8IASAAIAAAJQAFgKALAAIAAATIgDAAQgNABAAAPIAAAag");
	this.shape_19.setTransform(18.7,7.55);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgfAqIAAhRIASAAIAAAFQACgDADgBQAFgDAFAAQANAAAJAJQAIAJAAAOQAAANgIAJQgHAJgOAAQgFAAgFgDIgFgDIAAAagAgJgTQgEAEAAAGQABAHAEADQADAEAFAAQAFAAAEgEQAEgDgBgHQABgHgEgEQgEgEgFAAQgFAAgEAFg");
	this.shape_20.setTransform(13,8.625);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgFAVIAAgKIgKAFIgGgLIAJgFIgJgEIAGgLIAKAFIAAgKIALAAIAAAKIAJgFIAGALIgJAEIAKAFIgHALIgJgFIAAAKg");
	this.shape_21.setTransform(4.525,4.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_prijs, new cjs.Rectangle(0,0,131.8,15.6), null);


(lib.dollar_cashback_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}
	this.frame_1 = function() {
		this.stop();
	}
	this.frame_2 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1).call(this.frame_1).wait(1).call(this.frame_2).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgRAKIAAgTIAjAAIAAATg");
	this.shape.setTransform(11.375,5.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgJAJQgEgEAAgFQAAgEAEgEQAEgEAFAAQAGAAAEAEQAEAEAAAEQAAAFgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_1.setTransform(7.675,7.675);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgeAAQAAgnAeAAQAfAAAAAnQAAAogfAAQgeAAAAgogAgDAAQAAASADAAQAEAAAAgSQAAgRgEAAQgDAAAAARg");
	this.shape_2.setTransform(2.925,4.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgeAAQAAgnAeAAQAfAAAAAnQAAAogfAAQgeAAAAgogAgDAAQAAASADAAQAEAAAAgSQAAgRgEAAQgDAAAAARg");
	this.shape_3.setTransform(-3.675,4.95);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgVAgQgGgGgCgJIAXgFQABAFAEAAQAEAAAAgFQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAgBAAAAQAAAAgBAAQgDAAgBADIgWgDIACgsIAyAAIAAAVIgcAAIAAAGIAGgBQAMAAAHAGQAIAHAAALQAAALgIAIQgJAIgNAAQgMAAgJgHg");
	this.shape_4.setTransform(-10.125,5.025);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAFAnIgMggIAAAgIgcAAIAAhNIAcAAIAAAcIAMgcIAeAAIgTAlIAUAog");
	this.shape_5.setTransform(26,-5.05);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgVAdQgMgLAAgSQAAgQALgMQAMgLASAAQAPAAAKAIIgIAVQgGgFgIAAQgHgBgEAFQgEAFAAAGQAAAHAEAFQAEAFAHgBQAIAAAHgGIAIAWQgLAIgPAAQgSAAgLgLg");
	this.shape_6.setTransform(18.475,-5.05);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AALAnIgDgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAAEAHIgEgPIgEAPIAIAAg");
	this.shape_7.setTransform(10.95,-5.05);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgfAnIAAhNIAgAAQAOAAAHAHQAIAFAAAKQAAAKgIAFQAEACADAEQACAFAAAEQABAMgIAGQgIAGgNABgAgEASIAEAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAgBQABAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBgBAAAAIgEAAgAgEgJIAEAAQAFAAgBgDQABgFgFAAIgEAAg");
	this.shape_8.setTransform(3.55,-5.05);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAIAnIAAgcIgPAAIAAAcIgcAAIAAhNIAcAAIAAAcIAPAAIAAgcIAcAAIAABNg");
	this.shape_9.setTransform(-3.85,-5.05);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgUAhQgGgFgEgIIAVgLQAFAJAEgBQAAAAABAAQAAAAAAAAQABAAAAgBQAAAAAAgBQAAgCgDgCIgLgFQgPgHAAgNQAAgMAIgGQAIgHAMAAQAMAAAIAHQAIAFABAGIgTALQgEgGgFgBQAAAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAABAFADIAKAGQAPAGAAANQAAALgIAHQgIAHgOAAQgLAAgJgHg");
	this.shape_10.setTransform(-10.95,-5.05);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AALAnIgCgNIgRAAIgEANIgcAAIAbhNIAaAAIAbBNgAADAHIgDgPIgEAPIAHAAg");
	this.shape_11.setTransform(-18.15,-5.05);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgVAdQgMgLAAgSQAAgQALgMQAMgLASAAQAPAAAKAIIgIAVQgGgFgIAAQgHgBgEAFQgEAFAAAGQAAAHAEAFQAEAFAHgBQAIAAAHgGIAIAWQgLAIgPAAQgSAAgLgLg");
	this.shape_12.setTransform(-25.825,-5.05);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAEAnIgKgZIAAAZIgbAAIAAhNIAhAAQANAAAIAIQAIAGAAAMQgBAGgDAFQgDAFgGACIASAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQABAAAAgBQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_13.setTransform(11.8,8.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAMAnIgEgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAAEAHIgEgPIgEAPIAIAAg");
	this.shape_14.setTransform(-4.15,8.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgeAnIAAhNIAfAAQANAAAJAHQAHAFAAAKQAAAKgIAFQAEACADAEQADAFAAAEQAAAMgIAGQgIAGgMABgAgEASIAEAAQABAAAAAAQAAAAABAAQAAgBABAAQAAAAABgBQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQAAgBgBAAIgEAAgAgEgJIAEAAQAFAAAAgDQAAgFgFAAIgEAAg");
	this.shape_15.setTransform(-11.55,8.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgRAJIAAgSIAjAAIAAASg");
	this.shape_16.setTransform(25.875,-0.65);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgcAcQgLgKABgSQAAgQALgMQALgLASAAQAUAAAMAMIgPARQgGgFgIgBQgGAAgEAFQgEAFgBAGQAAAHAEAFQAFAEAGAAQAGAAAEgBIAAgDIgJAAIAAgSIAiAAIAAAhQgPANgXAAQgSAAgMgMg");
	this.shape_17.setTransform(19.5,-1.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgUAlIADgVIAGABQAFAAAAgEIAAgzIAbAAIAAA3QAAAWgZAAQgIAAgIgCg");
	this.shape_18.setTransform(13.05,-1.075);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_19.setTransform(9.15,-1.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAEAnIgKgZIAAAZIgbAAIAAhNIAhAAQANAAAIAIQAIAGgBAMQABAGgEAFQgEAEgFADIASAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_20.setTransform(4.2,-1.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAEAnIgKgZIAAAZIgbAAIAAhNIAhAAQANAAAIAIQAHAGAAAMQAAAGgDAFQgEAEgEADIARAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_21.setTransform(-10.4,-1.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgXAnIAAhNIAvAAIAAAWIgUAAIAAAGIASAAIAAAUIgSAAIAAAHIAUAAIAAAWg");
	this.shape_22.setTransform(-16.85,-1.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgNAnIgchNIAeAAIALAsIANgsIAdAAIgcBNg");
	this.shape_23.setTransform(-23.85,-1.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAhQgGgGgDgHIATgLQAFAIAFAAQABAAAAAAQAAAAABAAQAAAAAAgBQAAAAAAAAQAAgDgEgCIgKgFQgPgHAAgOQAAgKAIgIQAIgGAMAAQAMAAAJAGQAHAGACAGIgUALQgDgHgFAAQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAAAQAAADAFACIAKAGQAPAGAAANQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape_24.setTransform(20.5,-11.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgeAcQgLgLAAgRQAAgQALgMQALgLATAAQATAAAMALQALAMAAAQQAAARgLALQgLAMgUAAQgTAAgLgMgAgJgLQgDAFgBAGQABAHADAFQAEAFAFAAQAGAAAEgFQADgFAAgHQAAgGgDgFQgEgFgGAAQgFAAgEAFg");
	this.shape_25.setTransform(13.05,-11.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgYAmIAAhLIAbAAIAAA2IAWAAIAAAVg");
	this.shape_26.setTransform(6.15,-11.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAFAmIgMgfIAAAfIgcAAIAAhLIAcAAIAAAbIAMgbIAeAAIgTAkIAUAng");
	this.shape_27.setTransform(-2.45,-11.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgeAcQgLgLAAgRQAAgQALgMQAMgLASAAQATAAAMALQALAMAAAQQAAARgLALQgLAMgUAAQgSAAgMgMgAgJgLQgDAFgBAGQABAHADAFQAEAFAFAAQAGAAAEgFQADgFAAgHQAAgGgDgFQgEgFgGAAQgFAAgEAFg");
	this.shape_28.setTransform(-10.7,-11.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgeAcQgLgLAAgRQAAgQALgMQALgLATAAQATAAAMALQALAMAAAQQAAARgLALQgLAMgUAAQgTAAgLgMgAgJgLQgDAFgBAGQABAHADAFQAEAFAFAAQAGAAAEgFQADgFAAgHQAAgGgDgFQgEgFgGAAQgFAAgEAFg");
	this.shape_29.setTransform(-19.35,-11.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgOAIIAAgPIAdAAIAAAPg");
	this.shape_30.setTransform(21.275,10.375);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_31.setTransform(18.275,12.225);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgVAcIAHgPQAEACAEAAQAGAAAAgGQgCACgEAAQgIAAgEgGQgGgEAAgKQAAgJAHgHQAGgGALgBQAZABAAAcQAAAPgGAJQgIALgOAAQgLABgHgFgAgCgNIgBAEIABAEQAAAAABABQAAAAAAAAQABAAAAAAQAAAAAAAAIACgBQABgJgDAAQAAAAgBAAQAAAAAAAAQgBABAAAAQAAAAAAAAg");
	this.shape_32.setTransform(14.45,10);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgSAbQgGgFAAgKQAAgJAGgEQgFgEAAgIQAAgIAGgFQAHgFAKgBQALABAGAFQAHAFAAAIQAAAIgFAEQAGAEAAAJQAAAKgHAFQgGAGgMgBQgKABgIgGgAgCAIIAAADQgBAFADAAQAEAAAAgFIgBgDQAAAAgBgBQAAAAAAAAQgBgBAAAAQAAAAgBAAQAAAAAAAAQAAAAgBABQAAAAAAAAQgBABAAAAgAgCgLQAAAEACAAQABAAAAAAQABgBAAAAQABgBAAAAQAAgBAAgBQAAgFgDABQgCgBAAAFg");
	this.shape_33.setTransform(9.15,10);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_34.setTransform(3.625,12.225);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgLAfIgXg+IAZAAIAJAkIAKgkIAYAAIgWA+g");
	this.shape_35.setTransform(-1.1,10);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_36.setTransform(-5.775,12.225);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AAIAfIgIggIgHAgIgWAAIgRg+IAYAAIAGAiIAIgiIARAAIAIAiIAFgiIAZAAIgRA+g");
	this.shape_37.setTransform(-11.75,10);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_38.setTransform(-17.675,12.225);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgLAfIAAgsIgMAAIAAgSIAvAAIAAASIgNAAIAAAsg");
	this.shape_39.setTransform(-21.375,10);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAHAfIgNgdIAAAdIgXAAIAAg+IAYAAIAMAeIAAgeIAXAAIAAA+g");
	this.shape_40.setTransform(27.425,2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAnAAIAAASIgRAAIAAAGIAPAAIAAAPIgPAAIAAAGIARAAIAAARg");
	this.shape_41.setTransform(21.975,2);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgWAXQgKgJABgOQgBgNAKgJQAKgKANAAQASAAAJAKIgMAPQgFgGgHAAQgEAAgEAFQgDADgBAFQABAGADAEQADAEAGgBQAEABADgCIAAgCIgHAAIAAgPIAcAAIAAAbQgNAKgSAAQgOAAgKgJg");
	this.shape_42.setTransform(16.35,2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAHAfIgNgdIAAAdIgXAAIAAg+IAYAAIAMAeIAAgeIAXAAIAAA+g");
	this.shape_43.setTransform(9.825,2);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AAJAfIgCgJIgOAAIgCAJIgXAAIAWg+IAVAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_44.setTransform(3.3,2);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAHAfIAAgWIgMAAIAAAWIgYAAIAAg+IAYAAIAAAXIAMAAIAAgXIAXAAIAAA+g");
	this.shape_45.setTransform(-3.25,2);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgYAfIAAg+IAaAAQAKAAAHAHQAGAFAAAKQAAAJgGAFQgHAHgKgBIgEAAIAAAUgAgCgEIACAAIADgBQAAAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQgBAAAAgBQAAAAgBAAQAAAAgBAAQAAgBAAAAIgCAAg");
	this.shape_46.setTransform(-9,2);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgYAXQgKgJAAgOQAAgNAKgJQAJgKAPAAQAQAAAJAKQAKAJAAANQAAAOgKAJQgJAJgQAAQgPAAgJgJgAgHgJQgDAEAAAFQAAAGADAEQADAEAEAAQAFAAADgEQADgEAAgGQAAgFgDgEQgDgEgFAAQgEAAgDAEg");
	this.shape_47.setTransform(-15.3,2);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AAHAfIgNgdIAAAdIgXAAIAAg+IAYAAIAMAeIAAgeIAXAAIAAA+g");
	this.shape_48.setTransform(-23.775,2);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAnAAIAAASIgRAAIAAAGIAPAAIAAAPIgPAAIAAAGIARAAIAAARg");
	this.shape_49.setTransform(-29.225,2);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AAHAfIgNgdIAAAdIgXAAIAAg+IAYAAIAMAeIAAgeIAXAAIAAA+g");
	this.shape_50.setTransform(24.275,-6);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAnAAIAAASIgRAAIAAAGIAPAAIAAAPIgPAAIAAAGIARAAIAAARg");
	this.shape_51.setTransform(18.825,-6);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AADAfIgIgUIAAAUIgWAAIAAg+IAbAAQALABAHAFQAFAGABAJQAAAFgEAFQgCADgEACIAOAagAgFgFIADAAQAAAAABAAQAAAAABAAQAAAAAAAAQAAgBAAAAIABgDIgBgDQAAAAAAgBQAAAAAAAAQgBAAAAAAQgBgBAAAAIgDAAg");
	this.shape_52.setTransform(14,-6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAnAAIAAASIgRAAIAAAGIAPAAIAAAPIgPAAIAAAGIARAAIAAARg");
	this.shape_53.setTransform(8.725,-6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAWAAIAAAtIARAAIAAARg");
	this.shape_54.setTransform(4.375,-6);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAWAAIAAAtIARAAIAAARg");
	this.shape_55.setTransform(-0.025,-6);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AAJAfIgCgJIgOAAIgDAJIgWAAIAVg+IAWAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_56.setTransform(-5.6,-6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgLAfIAAgsIgMAAIAAgSIAvAAIAAASIgNAAIAAAsg");
	this.shape_57.setTransform(-11.425,-6);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgFIARgJQADAGAEAAQAAAAABAAQAAAAAAAAQAAgBABAAQAAAAAAgBQAAgBgDgCIgJgEQgMgGAAgLQAAgIAHgGQAGgFAKgBQAJAAAIAGQAFAEACAFIgQAKQgDgHgEAAQgBAAAAABQAAAAAAAAQgBAAAAAAQAAABAAAAQAAACAEACIAIAEQANAGAAAKQAAAKgHAFQgHAGgLgBQgJABgHgGg");
	this.shape_58.setTransform(-16.525,-6);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AAHAfIgNgdIAAAdIgXAAIAAg+IAYAAIAMAeIAAgeIAXAAIAAA+g");
	this.shape_59.setTransform(-22.325,-6);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AgKAfIAAg+IAVAAIAAA+g");
	this.shape_60.setTransform(-26.95,-6);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgFIARgJQADAGAEAAQAAAAABAAQAAAAAAAAQAAgBABAAQAAAAAAgBQAAgBgDgCIgJgEQgMgGAAgKQAAgJAHgGQAGgGAKAAQAJABAIAFQAFAEACAFIgQAJQgDgFgEgBQgBAAAAABQAAAAAAAAQgBAAAAAAQAAABAAAAQAAACAEACIAIAEQANAGAAAKQAAAKgHAFQgHAGgLAAQgJAAgHgGg");
	this.shape_61.setTransform(12.975,-14);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgLAfIAAg+IAXAAIAAA+g");
	this.shape_62.setTransform(9,-14);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgLAfIAAgsIgMAAIAAgSIAvAAIAAASIgNAAIAAAsg");
	this.shape_63.setTransform(5.125,-14);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AAKAfIgDgJIgOAAIgCAJIgXAAIAVg+IAWAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_64.setTransform(-0.7,-14);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AADAfIgIgUIAAAUIgWAAIAAg+IAbAAQALABAHAFQAFAGAAAJQAAAFgCAFQgDADgFACIAPAagAgFgFIADAAQABAAAAAAQAAAAABAAQAAAAAAAAQAAgBABAAIABgDIgBgDQgBAAAAgBQAAAAAAAAQgBAAAAAAQAAgBgBAAIgDAAg");
	this.shape_65.setTransform(-6.65,-14);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AgWAYQgKgKAAgOQAAgNAKgJQAKgKANAAQARABAKAJIgMAOQgGgEgGgBQgFABgDAEQgDADgBAFQABAGADAEQADADAFAAQAFAAADgBIAAgDIgHAAIAAgOIAcAAIAAAbQgMALgTAAQgOgBgKgIg");
	this.shape_66.setTransform(-13.1,-14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11,p:{x:-18.15,y:-5.05}},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5,p:{x:26,y:-5.05}},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_5,p:{x:-3.25,y:-1.1}},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_11,p:{x:4,y:8.9}},{t:this.shape_13}]},1).to({state:[{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33.5,-19.6,66.3,37.1);


(lib.dollar_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AkfD2IgigJIg9ldQATAGAPADQBMAQBXgZQA1gOBmgwQBlgvA1gPQBXgYBNAPQARADARAGIA9FdIgigKQhMgQhYAZQg1APhlAwQhmAvg0APQg1APgxAAQggAAgegGgAkkDWQBMAQBXgZQA1gOBlgwQBmgvA1gPQBYgZBMAQIgzkdQhMgQhXAZQg1APhlAvQhmAwg2AOQhXAZhMgQgAg+ByQgmgTgIgwQgIguAaguQAaguAtgVQArgWAnAVQAmATAIAwQAIAugaAuQgaAtgtAWQgWALgVAAQgUAAgTgKgAAHArIhOAkIACANQAAADADABQAEACAEgCQAYgJAugWIBGggQAEgBACgEQACgFgBgEIgCgMQgcAMg0AYgAA6hkIhFAgQgvAWgYAJQgEACgCAEQgCAFAAAEIAQBXQABAEADABQADACAEgCIBGgfIBGghQAEgBADgDQABgEAAgEIgQhZQgBgDgCgCIgEgBIgEABgAg+BYIgBgEIA4gaIABADIAggPIAAgDIA4gZIABAFQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAAAIhHAgIhFAgIgBABQgBAAAAgBQAAAAAAAAQgBAAAAAAQAAgBAAAAgAhDA+IgPhYQAAgBAAAAQAAgBAAAAQAAAAABgBQAAAAAAAAIBBgdIAAAAQABAIAGgEQAHgCgBgIIA/gdQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAAAIAPBZQABABAAAAQAAAAgBABQAAAAAAABQgBAAAAAAIhHAfQgtAWgYAJIgBABIgCgBgAkhAQIgHgBIgCgLIAGABIgBgEIgGAAIgCgLIAJABQACgKAHgGQAIgHANAAQALgBAIAEIgEAQQgFgDgHAAQgLAAgEAIIAVgBIABAKIgWAAIAAAFIAWAAIACALIgSAAQAGAJALgBQAGgBAFgDIAKANQgGAGgMABIgCAAQgaAAgNgZgAElAoQgdgCgLgVIgHAAIgCgKIAFgBIgBgEIgFAAIgCgKIAIgBQABgLAIgGQAJgGALAAQAMAAAJAFIgFAOQgHgDgFAAQgLgBgDAIIAUAAIABAKIgXAAIABAEIAWAAIABAMIgQgBQAFAHALABQAFABAGgDIAKAPQgEAEgKAAIgEgBg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AkkDWIgzkdQBMAQBXgZQA2gOBmgwQBlgvA1gPQBXgZBMAQIAzEdQhMgQhYAZQg1APhmAvQhlAwg1AOQg1APgwAAQggAAgegGgAgThwQgtAVgaAuQgaAuAIAuQAIAwAmATQAmAUAsgVQAtgWAagtQAagugIguQgIgwgmgTQgTgKgUAAQgVAAgWALgAkoAPIAHABQAOAaAbgBQAMgBAGgGIgKgNQgFADgGABQgLABgGgJIASAAIgCgLIgWAAIAAgFIAWAAIgBgKIgVABQAEgIALAAQAHAAAFADIAEgQQgIgEgLABQgNAAgIAHQgHAGgCAKIgJgBIACALIAGAAIABAEIgGgBgAElAoQANABAFgEIgKgPQgGADgFgBQgLgBgFgHIAQABIgBgMIgWAAIgBgEIAXAAIgBgKIgUAAQADgIALABQAFAAAHADIAFgOQgJgFgMAAQgLAAgJAGQgIAGgBALIgIABIACAKIAFAAIABAEIgFABIACAKIAHAAQALAVAdACgAhCBgQgDgBAAgDIgCgNIBOgkQA0gYAcgMIACAMQABAEgCAFQgCAEgEABIhGAgQguAWgYAJIgEABIgEgBgAg/BUIABAEQAAABAAAAQABAAAAAAQAAABABAAQAAAAABgBIBFggIBHggQAAAAABAAQAAAAAAgBQABAAAAAAQAAgBAAAAIgBgFIg4AZIAAADIggAPIgBgDgAhGBGQgDgBgBgEIgQhXQAAgEACgFQACgEAEgCQAYgJAvgWIBFggQAFgBADABQACACABADIAQBZQAAAEgBAEQgDADgEABIhGAhIhGAfIgDABIgEgBgAhRgdQAAAAAAAAQgBABAAAAQAAAAAAABQAAAAAAABIAPBYQABABAAAAQAAAAAAAAQABAAAAAAQABAAAAgBQAYgJAtgWIBHgfQAAAAABAAQAAgBAAAAQABgBAAAAQAAAAgBgBIgPhZQAAAAAAAAQAAgBAAAAQgBAAAAAAQAAAAgBAAIg/AdQABAIgHACQgGAEgBgIIAAAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_5, new cjs.Rectangle(-38.3,-25.2,76.6,50.4), null);


(lib.dollar_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AD7EIQg6g0hYgWQg1gNhvgLQgwgEglgEQgzgHgegJQhPgTg3gsIgMgKIgZgZIB5lMIAZAZQA6A0BZAVQA0APBwAJQBwALA1AOQBYAWA6AzQANALAMAOIh5FMIgZgZgAloAnQA6A0BYAWQA1AOBwAKQBvAKA1ANQBZAWA6A0IBjkQQg6gzhYgXQg1gNhwgLQhwgJg0gOQhZgWg6g0gADqC2QgYgQABgZIgHgCIAEgLIAFACIACgFIgFgCIADgKIAHADQAHgJAKgBQALgCAKAHQAKAFAFAKIgLAJQgEgGgFgEQgKgEgGAEIASALIgFAKIgTgMIgCAFIATALIgFAKIgOgKQABAKAJAGQAEADAHABIABARIgCABQgHAAgIgGgAgmBsQgxgDgXglQgXgkARgsQAQgtAugbIAFgDQArgYAuAEQAxAEAXAkQAMAVABAXQAAASgHATQgQAsguAbQgnAYgqAAIgNgBgAhEgxIgaBIQgDAHAIABIAcADIgBADQgBADABADQACACADABIBPAHQAIABADgIIABgDIAcADQADAAADgBQAEgCABgDIAbhJQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQgCgCgDAAIgxgGQgJgBgCAHIgSAwIgkgDIASgvQACgIgIAAIgxgGIgCAAQgGAAgDAHgAg2AmIAKgbIBPAHIgLAcgAgnATQgEADgCADQgBAEACADQACADAFAAQADABAEgDQAEgDACgEQABgEgCgCQgCgDgEAAIgBAAQgEAAgDACgAgOAcQAAAAAAABQAAAAAAABQAAAAABAAQAAABABAAIAZACQABAAABAAQAAAAABgBQAAAAABAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAAAQgBAAgBAAIgZgDQgBAAAAABQgBAAAAAAQgBAAAAABQAAAAgBABgABAAnIgbgEIACgHIAEABQAHAAAIgEQAHgFACgGQADgHgEgFQgEgGgHAAQgHAAgIAEQgHADgCAHIgHAAIASgxIAxAGIAAAAIgaBIgAAygVQgBAEACAEQACACAEABQALABADgKQACgDgCgEQgDgDgEgBIgCAAQgJAAgDAJgAgnAZQABgDAFABQAEgBgBAEQgCAEgEAAQgEAAABgFgAhWAXIAbhIQAYAEAZACIgSAvIgGAAQACgGgDgFQgEgGgIAAQgHgBgIAEQgGAFgDAFQgCAHADAGQAEAFAHABIAEAAIgDAHgAguglQgEADgCADQgBAEACAEQACADAEAAQAFABAEgDQAFgCABgFQABgDgCgEQgCgCgEgBIgCAAQgEAAgDACgAAtAXIgDgBIABgDQACgHgIgBIgBgBQACgEAFgDQAFgDAFAAQAFABACADQADAEgCAFQgCAFgEADQgEACgEAAIgCAAgAhAAJQgDgDACgFQACgEAEgDQAGgDAFAAQAEACADADQADAEgCADIgBABIgHAAQgDADgBADIgBADIgEABQgFgBgCgEgAA7gPQgEgBABgEQABgEAFAAQAFABgCAEQgBAEgEAAIgBAAgAgrgZQgEgBACgEQABgEAFABQAEgBgBAFQgCAEgEAAIgBAAgAjrhYQgYgNABgdIgFgDIADgLIAEADIACgFIgEgCIAEgKIAGADIAEgDQAFgFAHgBQALgBAKAGQALAGAFAHIgMAKQgCgFgHgDQgJgEgHAEIASAJIgFALIgTgLIgCAFIATAKIgFAKIgPgIQACAJAJAGQAHACAFgBIABARIgFABQgFAAgIgEg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("ABzCgQg1gNhvgKQhwgKg1gOQhYgWg6g0IBjkQQA6A0BZAWQA0AOBwAJQBwALA1ANQBYAXA6AzIhjEQQg6g0hZgWgADMCLIAHACQgBAZAYAQQAKAHAHgCIgBgRQgHgBgEgDQgJgGgBgKIAOAKIAFgKIgTgLIACgFIATAMIAFgKIgSgLQAGgEAKAEQAFAEAEAGIALgJQgFgKgKgFQgKgHgLACQgKABgHAJIgHgDIgDAKIAFACIgCAFIgFgCgAgxhXIgFADQguAbgQAtQgRAsAXAkQAXAlAxADQAxAEAtgbQAugbAQgsQAHgTAAgSQgBgXgMgVQgXgkgxgEIgOgBQgmAAglAVgAkHiFIAFADQgBAdAYANQAMAGAGgDIgBgRQgFABgHgCQgJgGgCgJIAPAIIAFgKIgTgKIACgFIATALIAFgLIgSgJQAHgEAJAEQAHADACAFIAMgKQgFgHgLgGQgKgGgLABQgHABgFAFIgEADIgGgDIgEAKIAEACIgCAFIgEgDgAg5AuQgDgBgCgCQgBgDABgDIABgDIgcgDQgIgBADgHIAahIQADgIAIABIAxAGQAIAAgCAIIgSAvIAkADIASgwQACgHAJABIAxAGQADAAACACQAAABABAAQAAABAAAAQAAABAAABQAAAAAAABIgbBJQgBADgEACQgDABgDAAIgcgDIgBADQgDAIgIgBgAg2AmIBOAIIALgcIhPgHgAAlAjIAbAEIABAAIAahIIAAAAIgxgGIgSAxIAHAAQACgHAHgDQAIgEAHAAQAHAAAEAGQAEAFgDAHQgCAGgHAFQgIAEgHAAIgEgBgAhWAXIAcADIADgHIgEAAQgHgBgEgFQgDgGACgHQADgFAGgFQAIgEAHABQAIAAAEAGQADAFgCAGIAGAAIASgvQgZgCgYgEgAArADQgFADgCAEIABABQAIABgCAHIgBADIADABQAFAAAFgCQAEgDACgFQACgFgDgEQgCgDgFgBQgFAAgFADgAg7gGQgEADgCAEQgCAFADADQACAEAFABIAEgBIABgDQABgDADgDIAHAAIABgBQACgDgDgEQgDgDgEgCQgFAAgGADgAglAjQgFAAgCgDQgCgDABgEQACgDAEgDQAEgCAEAAQAEAAACADQACACgBAEQgCAEgEADQgDACgCAAIgCAAgAgnAZQgBAFAEAAQAEAAACgEQABgEgEABIgBgBQgEAAgBADgAgMAfQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAAAABAAQAAgBABAAIAZADQABAAABAAQAAAAAAAAQABABAAAAQAAABgBABQAAAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAgAA5gKQgEgBgCgCQgCgEABgEQAEgKAKABQAEABADADQACAEgCADQgDAJgJAAIgCAAgAA4gUQgBAEAEABQAFAAABgEQACgEgFgBQgFAAgBAEgAgtgUQgEAAgCgDQgCgEABgEQACgDAEgDQAEgCAFAAQAEABACACQACAEgBADQgBAFgFACQgDADgEAAIgCgBgAgtgeQgCAEAEABQAFAAACgEQABgFgEABIgBgBQgEAAgBAEg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_4, new cjs.Rectangle(-39.7,-28.9,79.5,57.8), null);


(lib.dollar_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ADoEZQg2g5hXgdQgzgRhvgQQhwgQg0gRQhXgcg1g6QgMgNgLgPICRlCQAKAOAMANQA2A6BXAcQA0ARBvARQBvAQA0AQQBXAdA2A6QAMANAKAOIiQFCQgNgQgKgLgAlqAOQA1A5BXAdQA0ARBwAPQBvARAzARQBXAcA2A5IB3kIQg2g5hXgcQg0gRhvgQQhvgRg0gRQhXgcg2g5gADdDEQgXgQADgZIgGgEIAEgLIAFADIAAgCIACgCIgFgCIAFgLIAGAEQAIgIAKgBQALgBAJAIQAJAGAFAKIgMAIQgCgFgGgFQgJgGgHAEQAHAEAKAIIgFAKIgTgOIgCAFIASANIgFAJIgOgJQABAJAIAHQAHAEAEAAIAAASIgCAAQgGAAgJgIgAguBpQgxgGgUgmQgUgmATgrQAUgqAvgYQAwgYAwAGQAxAHAVAmQAUAlgUArQgTAqgwAYQglAUglAAQgLAAgLgCgAgShGIg8CEQgDAKAKABIBJAMQALABAEgJIA7iFQACgEgCgDQgCgDgEgBIhJgKIgDgBQgIAAgEAIgAhEBCQgBAAAAgBQAAAAgBAAQAAAAAAgBQAAAAABAAIA8iFQAAAAAAgBQAAAAAAAAQABAAAAAAQABAAAAAAIBKALQAAAAABAAQAAAAAAAAQABABAAAAQgBABAAAAIg8CFQAAAAAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAgAghA6QgCADACADQABACADABQAJAAACgGQAEgHgIgCIgCAAQgHAAgCAGgAAEg3IAoAGIADgHIgogFgAjlhnQgLgHgFgMQgFgMACgMIgGgEIAFgKIAEADIAAgCIACgDIgEgDIAEgKIAGAEQAQgPAWAOQAKAGAEAIIgMAKQgDgFgGgEQgJgGgGAFIARAKIgGAKIgSgMIgCACIAAACIASAMIgGAKIgOgJQABAJAJAGQAFADAGAAIAAAQIgDABQgGAAgJgFg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("ABoCmQgzgRhwgRQhvgPg0gRQhXgdg1g5IB2kIQA2A5BWAcQA0ARBwARQBvAQA0ARQBXAcA1A5Ih3EIQg1g5hXgcgADDCXIAFAEQgCAZAXAQQAKAJAGgBIABgSQgEAAgHgEQgJgHgBgJIAPAJIAFgJIgSgNIACgFIATAOIAEgKQgKgIgGgEQAHgEAIAGQAHAFABAFIAMgIQgEgKgJgGQgKgIgKABQgKABgIAIIgGgEIgFALIAFACIgCACIAAACIgFgDgAgxhWQgwAYgTAqQgTArATAmQAVAmAxAGQAxAHAvgZQAwgYASgqQAUgrgUglQgUgmgxgHIgVgBQgmAAglATgAj+iWIAFAEQgBAMAFAMQAFAMALAHQALAHAHgDIAAgQQgGAAgGgDQgIgGgBgJIAOAJIAFgKIgSgMIABgCIACgCIASAMIAFgKIgRgKQAHgFAJAGQAGAEACAFIANgKQgEgIgKgGQgXgOgPAPIgGgEIgFAKIAFADIgCADIAAACIgFgDgAACBVIhKgMQgJgBADgKIA8iEQAFgJAKACIBJAKQAEABACADQACADgCAEIg7CFQgEAIgJAAIgCAAgAgJhFIg9CFQAAAAAAAAQAAABAAAAQAAAAABAAQAAABAAAAIBLALQAAAAAAAAQABAAAAAAQAAgBABAAQAAAAAAAAIA8iFQAAAAAAgBQAAAAAAgBQAAAAAAAAQgBAAgBAAIhJgLQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAABAAAAgAgdBDQgDgBgCgCQgBgDABgDQADgHAIABQAJACgEAHQgCAGgHAAIgCAAgAADg3IAEgGIAoAFIgDAHg");
	this.shape_1.setTransform(0.05,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_3, new cjs.Rectangle(-39.9,-30.8,79.9,61.6), null);


(lib.dollar_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AmAhpIAdgSQBDgkBbgJQA2gHBvACQBuACA2gGQBagKBDgkIAegRIBCFaIgdASQhDAkhaAJQg2AHhvgCQhvgBg2AFQhaAKhDAkQgNAHgQAKgAi/iJQhbAKhDAjIA3EdQBDgkBagKQA2gGBvABQBvACA2gGQBagKBDgjIg3kdQhDAkhaAKQg2AGhugBIgqgBQhQAAgrAFgAAWBxQgwgBgpghQgogggJguQgJguAcgiQAcgiAwABQAwABApAhQAoAgAJAuQAJAvgcAhQgbAhguAAIgDAAgAhEhDQgFAGACAKIAVBsQABAIAIAHQAIAGAJAAIBQABQAJABAFgHQAGgGgCgJIgVhsQgCgKgHgGQgIgHgKAAIhPgBQgJABgGAGgAEYBXQgJgEgGgJIgHACIgCgLIAFgBIgBgGIgFACIgCgLIAHgCQABgLAIgJQAHgJAMgFQAMgEAHACIgEARQgFgCgHADQgKADgCAJIATgHIABAMIgVAHIABAFIAUgHIABALIgQAGQAHAGAJgEQAFgCAGgGIAJAMQgEAIgNAEQgHADgGAAQgFAAgFgCgAgZBGQgGAAgFgDQgFgFgBgFIgThhIBsACIATBgQABAFgDAFQgEADgGAAgAgigRQgLANADARQAEASAQANQAPAMASABQATAAALgNQALgMgEgSQgDgSgQgMQgPgNgSgBIgCAAQgSAAgKANgAAKAxQgPAAgNgKQgNgLgCgPQgDgOAJgLQAJgKAPAAQAPABAMAKQANALADAOQADAPgJAKQgIAKgPAAIgBAAgAkpgZIgHACIgCgLIAFgCIgBgEIgFABIgCgKIAHgDQABgMAIgKQAHgKAMgDQALgDAIADIgEAPQgHgBgFACQgJACgEAKIATgGIACALIgWAHIABAFIAVgHIABALIgQAFQAGAGAKgDQAIgCADgFIAJANQgEAHgNAEQgHACgGAAQgRAAgIgOgAg+gwIgBgDQgBgHADgDQAEgFAGAAIBPABQAGAAAFAFQAFAEABAGIABAEIhsgCgAgRg4QACAHAHAAQAIAAgCgHQgCgIgHAAQgHAAABAIgAgug9QgCACABACQABAIAIAAQADAAACgCQABgCAAgDQgBgIgIAAIgBAAQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAABg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AldhbQBDgkBbgKQA2gGBuACQBuABA2gGQBbgKBDgkIA3EdQhDAjhbAKQg2AHhvgCQhugCg2AGQhaAKhEAkgAhihPQgbAiAJAuQAIAuApAhQApAgAvABQAxABAcghQAcgigJgvQgJgugoggQgpghgwgBIgDAAQguAAgcAhgAEYBYQAKADANgEQAMgFAFgIIgJgLQgGAFgFACQgJAEgHgGIAQgGIgBgLIgVAHIAAgEIAUgIIgBgLIgTAHQADgJAKgEQAGgCAGABIADgRQgHgCgLAEQgMAFgHAJQgIAJgBALIgHACIACALIAFgBIABAFIgFABIACALIAGgCQAHAKAJAEgAkDgNQANgEADgHIgJgMQgCAEgIACQgKADgHgGIARgFIgBgLIgVAHIgBgFIAWgHIgCgLIgTAGQAEgKAJgCQAFgCAGACIAFgQQgJgDgLADQgLAEgIAKQgHAJgCAMIgGADIACALIAEgCIABAEIgEACIACALIAHgCQAMATAagHgAgYBPQgKAAgHgHQgIgGgCgJIgVhsQgCgKAGgGQAFgGAKAAIBPABQAJAAAJAGQAHAHACAJIAVBsQACAJgGAGQgFAHgKAAgAgrA5QACAFAFAFQAFAEAFAAIBQABQAGAAADgEQADgEgBgGIgShgIhsgBgAg9g9QgEAEACAGIAAAEIBsABIAAgEQgBgGgGgEQgEgEgHAAIhPgBQgFAAgEAEgAALA5QgSAAgQgNQgPgNgEgSQgDgRAKgNQAMgNASABQATAAAPANQAQANADARQAEASgLANQgLAMgSAAIgBAAgAgbgLQgKAKADAOQADAPANALQANAKAOAAQAQABAJgLQAIgKgCgPQgDgOgNgKQgNgLgOAAIgBAAQgPAAgIAKgAgSg4QAAgHAHAAQAHAAACAHQACAIgJAAQgHAAgCgIgAgvg4QgBgDACgCQABgDADABQAJAAAAAHQABADgCACQgCACgCAAQgJAAAAgHg");
	this.shape_1.setTransform(0.05,-0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_2, new cjs.Rectangle(-38.5,-24.1,77.1,48.3), null);


(lib.dollar_1_v2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjYDoIgdgEIhUkcIAcAFQBCAFBGgdQArgRBRgxQBPgxAqgSQBHgdBBAGQAQABAOADIBUEcIgdgFQhBgGhHAeQgqARhRAxQhPAygrARQg6AYg3AAIgXgBgAjfDPQBBAFBHgdQAqgRBQgxQBRgxAqgSQBGgdBBAGIhFjoQhBgHhHAeQgqARhPAyQhRAxgrARQhGAdhBgGgAgoBmQghgNgMgnQgLgmARgoQARgpAjgWQAjgXAhAOQAhANAMAnQALAlgRAoQgRApgjAXQgXANgUAAQgNAAgMgEgAgVBPQgDADACACQAAABABAAQAAAAABAAQAAAAABAAQAAAAABAAIAggoIArgGQAAAAAAAAQAAAAAAAAQAAAAAAAAQABgBAAAAQAAAAABgBQAAAAAAAAQABgBAAAAQAAgBgBAAQACgEgEAAIgfAFIAugcQADgBACgEQACgDgCgDIgWhMQgBgEgDgBQgDgBgEACIg5AjQgmAYgVAKIgEAGQgCAEABADIAWBLQABADAEACQAEABACgCIAugbgAgwBGIgWhKQgBgBAAAAQAAAAABgBQAAAAAAgBQAAAAABAAQATgLAngYIA6gjQAAAAAAAAQABAAAAAAQAAAAAAAAQABABAAAAIAWBMQABAAAAAAQAAAAgBABQAAAAAAAAQAAABgBAAIg6AjQgmAYgUAKIgBABIgBgCg");
	this.shape.setTransform(0,-0.0243);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AjfDPIhFjpQBBAGBGgdQArgRBRgxQBPgyAqgRQBHgeBBAHIBFDoQhAgGhHAdQgqAShRAxQhQAxgqARQg8AZg3AAIgVgBgAgahbQgkAWgRApQgRAoALAmQAMAnAhANQAiAMAigVQAjgXARgpQARgogLglQgLgngigNQgMgFgNAAQgUAAgWAOgAgWBUQgCgCADgDIAXgcIguAbQgCACgEgBQgDgCgBgDIgXhLQgBgDACgEIAFgGQAUgKAngYIA5gjQADgCADABQADABABAEIAXBMQABADgCADQgBAEgEABIguAcIAfgFQAEAAgBAEQAAAAAAABQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAABQAAAAAAAAQgBAAAAAAQAAAAAAAAIgqAGIghAoIgBAAIgDgBgAAvhNIg6AjQgmAYgUALQgBAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABIAWBKQABABAAAAQAAABAAAAQABAAAAAAQAAAAAAgBQAUgKAmgYIA6gjQABAAAAgBQAAAAABAAQAAgBAAAAQAAAAAAAAIgXhMIgBgBIgBAAg");
	this.shape_1.setTransform(-0.025,-0.0148);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1_v2, new cjs.Rectangle(-33,-23.3,66.1,46.6), null);


(lib.dollar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjzEuQgTgIgVgNIhwlQQAVANAUAJQBZAkBPguQAwgdBThfQBShfAwgdQBPgwBZAmQATAIAWANIBwFQQgVgOgUgIQhZglhQAvQgwAdhSBfQhSBfgwAdQgtAbgvAAQglAAgogRgAj9EQQBZAmBQgwQAwgdBShfQBShfAwgdQBQgvBZAlIhckTQhZgmhPAwQgwAdhSBfQhTBfgwAdQhPAvhZglgAgkCSQgmgFgPgvQgPgsAQg5QAQg6AlgrQAjgrAlAGQAlAFAQAuQAOAtgQA5QgQA5gkAsQgfAmghAAIgIgBgAAqh6QgVAUgnAuQgoAvgVAUQgIAHADALIAeBZQABAEAEAAQADABAEgDQAQgQAfgkIgXAtQgDAFADADQABAAAAAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAgBQAAAAABAAQAAAAAAAAQAAgBAAAAIAfg/IAtgYIABgBQADgCAAgEQAAgFgEACIghASQAYgcAYgYQADgDACgGQABgFgBgEIgehaQgBgEgDgBIgBAAQgDAAgDAEgAj9BtQgMgIgIgLIgIgCIgDgKIAGABIgBgCIgBgCIgGgCIgDgLIAIADQACgTAbgBQANAAAIADIgDAOQgFgCgHAAQgMABgCAGQALABAKgBIADAKQgMABgMgBIACAFIAXABIACAKIgSAAQAJAIAKgBQAGAAAFgEIALANQgEAHgNAAIgBAAQgNAAgMgHgAgvBwIgehaQAAgBgBAAQAAgBABAAQAAgBAAAAQAAgBABAAQAVgUAoguQAnguAUgVQABAAAAgBQAAAAABAAQAAAAAAABQABAAAAABIAeBZQAAABAAAAQAAABgBAAQAAABAAAAQAAABgBAAQgTAUgpAuQgmAtgWAWIgCABIAAgBgAEdgiQgfgFgPgUIgHABIgDgLIAFAAIgBgDIgBgCIgFAAIgDgKIAHgBQAAgKAIgHQAIgGANACQAMABAKAIIgDAMQgGgEgIgCQgLgBgDAHIAWACIACAKIgYgCIACAFIAXACIADAKIgTgBQAIAHALACQAHAAAGgBIAMAPQgDACgHAAIgJAAg");
	this.shape.setTransform(-0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("Aj8EQIhckTQBZAlBPgvQAxgdBThfQBRhfAwgdQBQgwBYAmIBcETQhZglhPAvQgwAdhTBfQhSBfgwAdQgsAbgwAAQglAAgngRgAgjhsQgkArgRA6QgQA5APAsQAQAvAlAFQAlAGAjgrQAlgsAQg5QAQg5gPgtQgPgugmgFIgIgBQghAAgfAmgAkYBYIAIACQAHALANAIQANAHANAAQAMAAAFgHIgMgNQgFAEgGAAQgKABgJgIIASAAIgCgKIgXgBIgCgFQAMABAMgBIgDgKQgKABgLgBQACgGAMgBQAIAAAFACIACgOQgHgDgNAAQgcABgBATIgIgDIACALIAHACIAAACIABACIgGgBgAEdgiQAPABAEgDIgMgPQgGABgHAAQgLgCgHgHIASABIgCgKIgYgCIgBgFIAYACIgDgKIgVgCQACgHALABQAIACAGAEIADgMQgKgIgMgBQgNgCgIAGQgIAHAAAKIgHABIADAKIAGAAIAAACIABADIgFAAIAEALIAHgBQAOAUAfAFgAgxB6QgDAAgBgEIgehZQgDgLAHgHQAWgUAogvQAnguAUgUQADgEAEAAQADABACAEIAdBaQACAEgCAFQgBAGgEADQgYAYgYAcIAhgSQAEgCAAAFQAAAEgDACIgBABIgtAYIgfA/QAAAAAAABQAAAAAAAAQAAAAgBAAQAAABAAAAQAAAAgBABQAAAAgBAAQAAAAgBAAQAAAAAAAAQgDgDADgFIAWgtQgfAkgQAQQgDADgDAAIgBgBgAAthzQgVAVgnAuQgoAugVAUQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABIAeBaQAAAAAAABQAAAAABAAQAAAAAAAAQABAAABgBQAVgWAmgtQApguATgUQABAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBIgehZIgBgCIgBABg");
	this.shape_1.setTransform(-0.05,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1, new cjs.Rectangle(-39.6,-31.9,79.2,63.8), null);


(lib.cta_rollover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E39126").s().p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	this.shape.setTransform(10,12);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta_rollover, new cjs.Rectangle(0,0,20,24), null);


(lib.cta_arrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag/AKIAAgTIB/AAIAAATg");
	this.shape.setTransform(1.9821,5.4827,0.3891,0.5,0,-50.0001,130.0012);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag/AKIAAgTIB/AAIAAATg");
	this.shape_1.setTransform(1.9679,2.2327,0.3891,0.5,50.0001);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta_arrow, new cjs.Rectangle(0,0,4,7.8), null);


(lib.cover1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AygE2IAAprMAlBAAAIAAJrg");
	this.shape.setTransform(-0.7,25.3,1.2658,2.2544,-4.9997,0,0,-119,-31.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cover1, new cjs.Rectangle(0,0,311.1,165.4), null);


(lib.arrowLeftRight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// hitArea
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,0,0,0.008)").s().p("AiuC5IAAlxIFdAAIAAFxg");
	this.shape.setTransform(6.4856,-0.0054,0.7142,1.081);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	// arrow
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#969696").s().p("AguAiIBEhQIAJALIANgLIACACIhMBbg");
	this.shape_1.setTransform(4.75,4.15,1,1,0,0,0,0.1,-0.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#969696").s().p("AgugnIAQgMIBMBbIgPAMg");
	this.shape_2.setTransform(4.65,-3.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AguAiIBEhQIAJALIANgLIACACIhMBbg");
	this.shape_3.setTransform(4.75,4.15,1,1,0,0,0,0.1,-0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgugnIAQgMIBMBbIgPAMg");
	this.shape_4.setTransform(4.65,-3.775);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).to({state:[{t:this.shape_4},{t:this.shape_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-20,25,40);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.greybg();
	this.instance.parent = this;
	this.instance.setTransform(150,0,1,1,0,0,0,150,103);

	this.instance_1 = new lib.greybg();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-150,0,1,1,0,0,0,150,103);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-300,-103,623,206);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.greybg();
	this.instance.parent = this;
	this.instance.setTransform(150,0,1,1,0,0,0,150,103);

	this.instance_1 = new lib.greybg();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-150,0,1,1,0,0,0,150,103);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-300,-103,623,206);


(lib.subsubtext = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer_1
	this.m02 = new lib.m03();
	this.m02.name = "m02";
	this.m02.parent = this;
	this.m02.setTransform(0,26);

	this.m01 = new lib.Symbol5();
	this.m01.name = "m01";
	this.m01.parent = this;
	this.m01.setTransform(0,13);

	this.m00 = new lib.Symbol1();
	this.m00.name = "m00";
	this.m00.parent = this;

	this.m12 = new lib.m12();
	this.m12.name = "m12";
	this.m12.parent = this;
	this.m12.setTransform(0,26);

	this.m11 = new lib.Symbol6();
	this.m11.name = "m11";
	this.m11.parent = this;
	this.m11.setTransform(0,13);

	this.m10 = new lib.Symbol2();
	this.m10.name = "m10";
	this.m10.parent = this;

	this.m21 = new lib.Symbol7();
	this.m21.name = "m21";
	this.m21.parent = this;
	this.m21.setTransform(0,13);

	this.m20 = new lib.Symbol3();
	this.m20.name = "m20";
	this.m20.parent = this;

	this.m30 = new lib.m33();
	this.m30.name = "m30";
	this.m30.parent = this;

	this.m32 = new lib.m31();
	this.m32.name = "m32";
	this.m32.parent = this;
	this.m32.setTransform(0,26);

	this.m31 = new lib.Symbol4();
	this.m31.name = "m31";
	this.m31.parent = this;
	this.m31.setTransform(0,13);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.m00},{t:this.m01},{t:this.m02}]}).to({state:[{t:this.m10},{t:this.m11},{t:this.m12}]},1).to({state:[{t:this.m20},{t:this.m21}]},1).to({state:[{t:this.m31},{t:this.m32},{t:this.m30}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,172.5,44);


(lib.shape_grey = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.instance = new lib.Symbol9();
	this.instance.parent = this;
	this.instance.setTransform(130,85,1,1,0,0,0,130,85);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("A0TLuIAA3bMAonAAAIAAXbg");
	this.shape.setTransform(150.0162,64.9951,1.1538,1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-40,300,210);


(lib.price_old_line_container = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.line = new lib.price_old_line();
	this.line.name = "line";
	this.line.parent = this;
	this.line.setTransform(0,24);

	this.timeline.addTween(cjs.Tween.get(this.line).wait(1));

}).prototype = getMCSymbolPrototype(lib.price_old_line_container, new cjs.Rectangle(0,0,92,26), null);


(lib.loopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(300,103);

	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,103);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},299).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,x:0},299).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-300,0,923,206);


(lib.label_green_circle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.l1 = new lib.label_green_circle_checl_2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(-4.65,5.2,1,1,-45);

	this.l0 = new lib.label_green_circle_check_1();
	this.l0.name = "l0";
	this.l0.parent = this;
	this.l0.setTransform(-6.4,-2.25,1,1,48.5006);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#30A945").s().p("AhiBjQgpgpAAg6QAAg5ApgpQApgpA5AAQA6AAApApQApApAAA5QAAA6gpApQgpApg6AAQg5AAgpgpg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.l0},{t:this.l1}]}).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,0,0,0)").s().p("AhZgFIAagYIArAvIBWhWIAYAbIhwBug");
	this.shape_1.setTransform(0,-0.125);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle, new cjs.Rectangle(-14,-14,28,28), null);


(lib.label_green = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.text = new lib.label_green_text();
	this.text.name = "text";
	this.text.parent = this;
	this.text.setTransform(32,4);

	this.gr = new lib.label_green_circle();
	this.gr.name = "gr";
	this.gr.parent = this;
	this.gr.setTransform(14,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.gr},{t:this.text}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green, new cjs.Rectangle(0,0,28,28), null);


(lib.label_cashback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("Helloblello", "11px 'Kokonor'", "#FFFFFF");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 11;
	this.dynText.lineWidth = 56;
	this.dynText.parent = this;
	this.dynText.setTransform(5,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

	// Layer_2
	this.bgr = new lib.emptyMovieClip();
	this.bgr.name = "bgr";
	this.bgr.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.bgr).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_cashback, new cjs.Rectangle(3,0,60,22.2), null);


(lib.endframe_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AyWGQIAAjIMAktAAAIAADIg");
	mask.setTransform(117.5001,39.9993);

	// line4
	this.l3 = new lib.endframe_video_line4();
	this.l3.name = "l3";
	this.l3.parent = this;
	this.l3.setTransform(0,60);

	var maskedShapeInstanceList = [this.l3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.l3).wait(1));

	// mask3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AyWEsIAAjIMAktAAAIAADIg");
	mask_1.setTransform(117.5001,29.9993);

	// line3
	this.l2 = new lib.endframe_video_line3();
	this.l2.name = "l2";
	this.l2.parent = this;
	this.l2.setTransform(0,40);

	var maskedShapeInstanceList = [this.l2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.l2).wait(1));

	// mask2 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("AyWDIIAAjIMAktAAAIAADIg");
	mask_2.setTransform(117.5001,19.9993);

	// line2
	this.l1 = new lib.endframe_video_line2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(0,20);

	var maskedShapeInstanceList = [this.l1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.l1).wait(1));

	// mask1 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("AyWBkIAAjHMAktAAAIAADHg");
	mask_3.setTransform(117.5001,9.9993);

	// line1
	this.l0 = new lib.endframe_video_line1();
	this.l0.name = "l0";
	this.l0.parent = this;

	var maskedShapeInstanceList = [this.l0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.l0).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_video, new cjs.Rectangle(0,0,235,80), null);


(lib.endframe_tv_audio = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AyWGLIAAi+MAktAAAIAAC+g");
	mask.setTransform(117.5001,39.4994);

	// line4
	this.l3 = new lib.endframe_tv_audio_line4();
	this.l3.name = "l3";
	this.l3.parent = this;
	this.l3.setTransform(0,60);

	var maskedShapeInstanceList = [this.l3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.l3).wait(1));

	// mask3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AyWEnIAAi+MAktAAAIAAC+g");
	mask_1.setTransform(117.5001,29.4994);

	// line3
	this.l2 = new lib.endframe_tv_audio_line3();
	this.l2.name = "l2";
	this.l2.parent = this;
	this.l2.setTransform(0,40);

	var maskedShapeInstanceList = [this.l2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.l2).wait(1));

	// mask2 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("AyWDDIAAjHMAktAAAIAADHg");
	mask_2.setTransform(117.5001,19.4993);

	// line2
	this.l1 = new lib.endframe_tv_audio_line2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(0,20);

	var maskedShapeInstanceList = [this.l1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.l1).wait(1));

	// mask1 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("AyWBfIAAi9MAktAAAIAAC9g");
	mask_3.setTransform(117.5001,9.4994);

	// line1
	this.l0 = new lib.endframe_tv_audio_line1();
	this.l0.name = "l0";
	this.l0.parent = this;

	var maskedShapeInstanceList = [this.l0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.l0).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_tv_audio, new cjs.Rectangle(0,0,225,79), null);


(lib.dollar_cashback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.text = new lib.dollar_cashback_text();
	this.text.name = "text";
	this.text.parent = this;
	this.text.setTransform(0,1.45,1,1,9.9999);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AkTETQhyhyAAihQAAihByhyQBzhyCgAAQCiAABxByQBzByAAChQAAChhzByQhxBziiAAQigAAhzhzg");
	this.shape.setTransform(-0.0216,-0.0194,0.8974,0.8974);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_cashback, new cjs.Rectangle(-35,-35,70,70), null);


(lib.cta = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// text
	this.dynText = new cjs.Text("", "11px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 9;
	this.dynText.lineWidth = 226;
	this.dynText.parent = this;
	this.dynText.setTransform(-248,5);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

	// arrowsMask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	mask.setTransform(-10,12);

	// arrows
	this.arrow2 = new lib.cta_arrow();
	this.arrow2.name = "arrow2";
	this.arrow2.parent = this;
	this.arrow2.setTransform(-13,8);

	this.arrow1 = new lib.cta_arrow();
	this.arrow1.name = "arrow1";
	this.arrow1.parent = this;
	this.arrow1.setTransform(-10,8);

	var maskedShapeInstanceList = [this.arrow2,this.arrow1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.arrow1},{t:this.arrow2}]}).wait(1));

	// rollover
	this.rollover1MC = new lib.emptyMovieClip();
	this.rollover1MC.name = "rollover1MC";
	this.rollover1MC.parent = this;
	this.rollover1MC.setTransform(-250,0);
	this.rollover1MC.alpha = 0;

	this.rollover2MC = new lib.cta_rollover();
	this.rollover2MC.name = "rollover2MC";
	this.rollover2MC.parent = this;
	this.rollover2MC.setTransform(-10,12,1,1,0,0,0,10,12);
	this.rollover2MC.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.rollover2MC},{t:this.rollover1MC}]}).wait(1));

	// bgr
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FAA332").s().p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	this.shape.setTransform(-10,12);

	this.shapeMC = new lib.emptyMovieClip();
	this.shapeMC.name = "shapeMC";
	this.shapeMC.parent = this;
	this.shapeMC.setTransform(-250,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shapeMC},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta, new cjs.Rectangle(-250,0,250,25.3), null);


// stage content:
(lib.index = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var self = this;
		
		//stage.enableMouseOver(6);
		
		var videoDiv = document.getElementById("content");
		var videoHolder = document.getElementById("video_holder");
		if (videoHolder != undefined) {
			var videoPlayer = videoHolder.getElementsByTagName("video")[0];
		}
		
		
		var muteDiv = document.getElementById("mute");
		var playPauseDiv = document.getElementById("playPause");
		var isVideoEnded = false;
		var mmMC = this.mediamarkt;
		var mm2MC = this.mediamarkt2;
		var greyMC = this.grey;
		var loopyMC = this.loopy;
		
		var logoMC = this.logo;
		var productMC = this.product;
		var priceMC = this.price;
		var labelMC = this.label;
		var leftMC = this.left;
		var rightMC = this.right;
		var ctaMC = this.cta;
		var cta2MC = this.cta2;
		var coverMC = this.cover;
		var hitAreaMC = this.hitAreaMC;
		var leftButtonDiv = document.getElementById("arrow_left");
		var rightButtonDiv = document.getElementById("arrow_right");
		var ctaButtonDiv = document.getElementById("cta");
		var lineMC = this.lineContainer;
		var campaignMC = this.campaign;
		var whiteMC = this.white;
		var servButtonMC = this.servButton;
		var vanafMC = this.vanaf;
		var cover2MC = this.cover2;
		var endMC = this.endframe;
		var endVideoMC = this.endframe_video;
		
		var text1MC = this.text1;
		var text2MC = this.text2;
		var text3MC = this.text3;
		
		var numberOfProducts = productID = numberOfServices = prevProductID = 0;
		dynamic.productID = productID;
		var productsObject = new Array();
		
		leftMC.mouseChildren = rightMC.mouseChildren = false;
		
		var bigJump = false;
		
		var productImgScale = 0.20; //1,552795031055901 //0.26 //0.28
		if (typeof VARIATION == "undefined") {
			VARIATION = "";
		}
		if (VARIATION == "dollar" || VARIATION == "tvmania") {
			productImgScale *= 1.25;
			leftMC.y -= 25;
			rightMC.y -= 25;
			leftMC.scaleX = leftMC.scaleY = rightMC.scaleX = rightMC.scaleY = 1.5;
			leftMC.x += 2;
			rightMC.x -= 2;
		}
		if (VARIATION == "tvmania") {
			productImgScale *= 2;
			//leftMC.y -= 25;
			//rightMC.y -= 25;
		}
		
		var numberImgScale = 0.33;
		var logoScale = 0.5;
		var campaignScale = 0.33;
		
		var autoRotateTime = 3.5;
		var ctaHover = false;
		
		var dollarCashbackToX = 140;
		var dollarCashbackToY = 180;
		
		var prijsToX = 26;
		var logoToY = -23;
		var productToX = productMC.x;
		var labelToX = 195;
		var priceToX = priceMC.x;
		var text1ToY = text1MC.y;
		var text2ToY = text2MC.y;
		var text3ToX = text3MC.x;
		var leftToX = leftMC.x;
		var rightToX = rightMC.x;
		var ctaToX = dynamic.width - 10;
		var mm2ToX = mm2MC.x;
		var lineToX = 24;
		var servButtonToX = dynamic.width;
		var vanafToX = 24;
		var lines = 4;
		for (var i = 0; i<lines; i++) {
			var lMC = endMC["l" + i];
			lMC.toY = lMC.y;
			lMC.y = lMC.toY + 21;
			lMC.rotation = 7;
			
			lMC = endVideoMC["l" + i];
			lMC.toY = lMC.y;
			lMC.y = lMC.toY + 18;
			lMC.rotation = 7;
		}
		var endToX = 22;
		var gToX = 25;
		var maxGreen = 3;
		for (i = 0; i<maxGreen; i++) {
			var a = self["g" + i];
			a.x = gToX;
			a.toY = a.y;
			a.y = a.toY + 25;
			a.alpha = 0.0;
			a.gr.l0.scaleX = a.gr.l1.scaleX = 0;
			a.gr.scaleX = a.gr.scaleY = 0.0;
		}
		var serviceLabelToX = 142;
		
		var isLeftClick = false;
		
		productMC.x = dynamic.width + 150;
		//priceMC.x = - dynamic.width / 2;
		text1MC.y = text1ToY + 27;
		text2MC.y = text2ToY + 27;
		leftMC.x = leftToX + 25;
		rightMC.x = rightToX - 25;
		ctaMC.x = ctaToX - 70;
		ctaMC.alpha = 0.0;
		mm2MC.y = 230;
		mm2MC.alpha = 0;
		mm2MC.x = mm2ToX + 50;
		lineMC.x = lineToX;
		lineMC.line.rotation = -14;
		lineMC.line.scaleX = 0.0;
		vanafMC.x = vanafToX + 20;
		cover2MC.y = dynamic.height;
		endMC.x = endToX;
		var maniaLabelToX = 22;
		
		// DCO 2.0 (!)
		this.getShotDuration = function (shot) {
			
			var duration = 3.0;
		
			return duration;
			
		}
		
		this.getShotData = function (i) {
		
			// IF NOT EMPTY SHOT
			if (dynamic.getDynamic("shot_type", i) == "") {
				return undefined;
			}
		
			return {
				
				"products": dynamic.getDynamic("shot_type", i).split("|"),
				"main_specs": dynamic.getDynamic("copy", i).split("|"),
				"sub_specs": dynamic.getDynamic("price_type", i).split("xxx")[0].split("|"),
				"label_new": dynamic.getDynamic("price_type", i).split("xxx")[1].split("|"),
				//"label_img": dynamic.getDynamic("price", i).split("|"),
				"price": dynamic.getDynamic("price", i).split("|"),
				"cta": dynamic.getDynamic("cta_copy", i),
				"colorPanel": dynamic.getDynamic("font_scale", i).split("|")[0]
				//"categoryType": dynamic.getDynamic("font_scale", i).split("|")[1]
				//"logo": dynamicContent.dcodata[0].Logo1.split("|")
		
			}
		
		}
		
		this.playShot = function (shot) {
		
			if (shot.index == 1) {
				
				numberOfProducts = shot.products.length;
				
				var h = 0;
				
				for (var i = 0; i<numberOfProducts; i++) {
					
					if (shot.products[i] != "" && shot.products[i] != "-") {
						
						var productName = shot.products[i];
						var checkProductName = productName.toLowerCase();
						var isService = false;
						if (shot.products[i][0] == "^") {
							isService = true;
							productName = productName.substring(1, productName.length);
							numberOfServices++;
						}
						
						var productObject = {
							name: productName,
							main_specs: shot.main_specs[i],
							sub_specs: shot.sub_specs[i],
							label_new: shot.label_new[i],
							label: !dynamic.isImageEmpty(LABEL + i),
							price: shot.price[i].split("^")[0],
							priceOld: shot.price[i].split("^")[1],
							service: isService
						}
						
						var imgMC = new createjs.MovieClip();
						productMC.addChild(imgMC);
						if (!(checkProductName == "tv" || checkProductName == "audio" || checkProductName == "video")) {
							dynamic.setDynamicImage(imgMC, (PRODUCT + i));
						}
						
						var toScale = productImgScale;
						var toX = 0;
						var toY = 0;
						imgMC.x = h * 300;
						h++;
						
						if (shot.sub_specs[i] != undefined && shot.sub_specs[i] != "" && shot.sub_specs[i] != " " && shot.sub_specs[i] != "-") {
							imgMC.green = true;
						}
						if (shot.label_new[i].length > 0 && shot.label_new[i].split("_")[0].toLowerCase() == "cashback") {
							imgMC.cashback = true;
						}
						if (shot.label_new[i].length > 0 && shot.label_new[i].split("_")[0].toLowerCase() == "service") {
							imgMC.service = true;
						}
						if (imgMC.green == true) {
							toScale *= 0.82;
							toX = 43;
							if (imgMC.cashback == true) {
								toScale *= 0.92;
								toY = -22;
							}
						} else if (imgMC.cashback == true) {
							toScale *= 0.76;
							toY -= 23;
						}
						if(VARIATION=="tvmania" && i==2  ){
							toX -= 15;
							toScale *= 0.9;
							
						}
						
						if(VARIATION=="tvmania" && i==0  ){
							
							toX -=15;
						}
						if(VARIATION=="tvmania" && i==1  ){
							
							toX -=15;
						}	
						
						if(VARIATION=="tvmania" && i==3  ){
							
							toX -=15;
						}
						
						imgMC.scaleX = imgMC.scaleY = toScale;
						imgMC.x += toX;
						if (VARIATION == "dollar") {
							toY -= 20;
						}
						imgMC.y += toY;
						
						var logoImgMC = new createjs.MovieClip();
						logoMC.addChild(logoImgMC);
						logoImgMC.alpha = 0;
						//dynamic.setDynamicImage(logoImgMC, (LOGO + i), 0, 0);
						logoImgMC.scaleX = logoImgMC.scaleY = logoScale;
						
						var labelImgMC = new createjs.MovieClip();
						labelMC.addChild(labelImgMC);
						if (!dynamic.isImageEmpty(LABEL + i)) {
							//dynamic.setDynamicImage(labelImgMC, (LABEL + i));
						}
						
						if (checkProductName == "tv" || checkProductName == "audio" || checkProductName == "video") {
							productObject = checkProductName;
							shot.main_specs[i] = shot.sub_specs[i] = shot.price[i] = "";
							if (checkProductName == "audio") {
								endMC.l3.gotoAndStop(1);
							}
						}
						
						productsObject.push(productObject);
						
					}
				}
				
				numberOfProducts = productsObject.length;
				
				if (shot.colorPanel == "red") {
					//greyMC.gotoAndStop(1);
					leftMC.gotoAndStop(1);
					rightMC.gotoAndStop(1);
					lineMC.line.gotoAndStop(1);
					whiteMC.visible = false;
					text1MC.dynText.color = text2MC.dynText.color = vanafMC.dynText.color = "#ffffff";
				}
				
				if (!dynamic.isImageEmpty(IMAGE_COUNT + 1)) {
					dynamic.setDynamicImage(campaignMC, (IMAGE_COUNT + 1));
					campaignMC.scaleX = campaignMC.scaleY = 0;
				}
				
				if (numberOfServices > 0) {
					buildServicesButton("BEKIJK ONZE SERVICES");
					rightToX = servButtonToX - 19;
					rightMC.x = rightToX - 25;
					var servButtonWidth = dynamic.width - servButtonToX;
					rightButtonDiv.style.right = servButtonWidth + "px";
					productToX -= Math.round((dynamic.width - servButtonToX) / 3);
					var div = document.createElement("div");
					div.style.width = servButtonWidth + "px";
					div.style.height = "80px";
					div.style.right = "0px";
					div.style.top = "95px";
					div.id = "services";
					document.getElementById("dom_overlay_container").appendChild(div);
					document.getElementById("services").addEventListener("click", doServices, false);
					for (i = 0; i<numberOfProducts; i++) {
						var m = productMC.children[i];
						if (m.green == true) {
							var sc = m.scaleX;
							sc *= 0.76;
							m.scaleX = m.scaleY = sc;
							m.x -= 7;
							m.y += 3;
							if (m.cashback == true) {
								m.y += 7;
							}
						}
					}
				}
				
				buildCta(shot.cta.split("|")[0], ctaMC);
				if (shot.cta.split("|")[1] != undefined) {
					buildCta(shot.cta.split("|")[1], cta2MC);
				}
				editCtaButtonDivWidth(ctaMC.ctaTextWidth);
				updateLogoTextsLabelPrice();
				animateInCreative();
				
				ctaButtonDiv.addEventListener("mouseover", doRollOver, false);
				ctaButtonDiv.addEventListener("mouseout", doRollOut, false);
				ctaButtonDiv.addEventListener("click", dynamic.onMouseClick.bind(dynamic));
				
				if (productsObject[productID] != "video" && VARIATION != "dollar") {
					
					//checkNumberOfProducts();
					
				}
				
			}
			
			//if (shot.isLastShot != true) {
		
		}
		
		function checkNumberOfProducts() {
						
			if (numberOfProducts > 1) {
				
				leftMC.visible = rightMC.visible = true;
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
				leftButtonDiv.addEventListener("click", doLeftRightClick, false);
				rightButtonDiv.addEventListener("click", doLeftRightClick, false);
				
				TweenMax.delayedCall(autoRotateTime + 1, doAutoRotate);
				
				enableSwipe();
				
			}
			
		}
		
		function enableSwipe() {
			swipedetect(document.getElementById("swipe_area"), function (swipedir) {
				self.cancelAutoRotate();
				var maxID = numberOfProducts - numberOfServices - 1;
				var minID = 0;
				if (productsObject[productID].service) {
					maxID = numberOfProducts - 1;
					minID = numberOfProducts - numberOfServices;
				} 
				if (swipedir == "right") {
					if (productID > minID) {
						productID--;
						bigJump = false;
					} else {
						bigJump = true;
						productID = maxID;
					}
					isLeftClick = true;
					changeProduct();
				}
				if (swipedir == "left") {
					if (productID < maxID) {
						productID++;
						bigJump = false;
					} else {
						bigJump = true;
						productID = minID;
					}
					isLeftClick = false;
					changeProduct();
				}
				if (swipedir == "click") {
					dynamic.onMouseClick();
				}
			});
		}
		
		function animateInVideo() {
			TweenMax.set(videoHolder, {display: "block"});
			TweenMax.set(videoDiv, {x: 50});
			//videoPlayer.currentTime = 0.1;
			dynamic.playVideo(1);
			TweenMax.to(videoDiv, 0.8, {x: 0, alpha: 1.0, ease: Expo.easeOut});
		}
		
		function checkVideoCurrentTime(event) {
			
			var time = event.target.currentTime;
			if (time >= event.target.duration - 0.6 && !isVideoEnded && videoHolder.style.display == "block") {
				document.getElementById("unmute").parentNode.removeChild(document.getElementById("unmute"));
				document.getElementById("pause").parentNode.removeChild(document.getElementById("pause"));
				isVideoEnded = true;
				TweenMax.to(videoDiv, 0.5, {alpha: 0.0});
				TweenMax.delayedCall(0.4, animateInEndframe, [endVideoMC]);
				checkNumberOfProducts();
				leftMC.alpha = rightMC.alpha = 0;
				leftMC.x = leftToX + 25;
				rightMC.x = rightToX - 25;
				TweenMax.to(leftMC, 0.9, {x: leftToX, ease: Expo.easeOut});
				TweenMax.to(leftMC, 0.8, {alpha: 1.0});
				TweenMax.to(rightMC, 0.9, {x: rightToX, ease: Expo.easeOut});
				TweenMax.to(rightMC, 0.8, {alpha: 1.0});
			}
			
		}
		
		function checkVideoMuted(event) {
			
			if (videoPlayer.muted) {
				TweenMax.to(muteDiv.children[0], 0.3, {alpha: 0.0});
				TweenMax.to(muteDiv.children[1], 0.3, {alpha: 1.0});
			} else {
				TweenMax.to(muteDiv.children[0], 0.3, {alpha: 1.0});
				TweenMax.to(muteDiv.children[1], 0.3, {alpha: 0.0});
			}
			
		}
		
		function doMuteUnmute(event) {
			
			if (videoPlayer.muted) {
				videoPlayer.muted = false;
			} else {
				videoPlayer.muted = true;
			}
			
		}
		
		function checkVideoPlayPause(event) {
			if (event.type == "play") {
				TweenLite.to(playPauseDiv.children[0], 0.3, {alpha: 0.0});
				TweenLite.to(playPauseDiv.children[1], 0.3, {alpha: 1.0});
			}
			if (event.type == "pause") {
				TweenLite.to(playPauseDiv.children[0], 0.3, {alpha: 1.0});
				TweenLite.to(playPauseDiv.children[1], 0.3, {alpha: 0.0});
			}
		}
		
		function doPlayPause(event) {
			if (videoPlayer.paused) {
				videoPlayer.play();
			} else {
				videoPlayer.pause();
			}
		}
		
		function buildCta(ctaText, whichCtaMC) {
			
			var rightMargin = 20;
			var buttonHeight = 24;
			var textPadding = 7;
			
			var ctaTextField = whichCtaMC.dynText;
			ctaTextField.text = ctaText;
			var ctaRect = new createjs.Shape();
			var ctaTextWidth = Math.round(ctaTextField.getMeasuredWidth()) + textPadding * 2;
			ctaTextField.x = - rightMargin - ctaTextWidth + textPadding - 1;
			whichCtaMC.shapeMC.x = whichCtaMC.rollover1MC.x = - rightMargin - ctaTextWidth - 1;
			ctaRect.graphics.beginFill("#FAA332").drawRoundRectComplex(0, 0, ctaTextWidth, buttonHeight, 3, 0, 0, 3);
			whichCtaMC.shapeMC.addChild(ctaRect);
			
			var rolloverRect = new createjs.Shape();
			rolloverRect.graphics.beginFill("#E39126").drawRoundRectComplex(0, 0, ctaTextWidth, buttonHeight, 3, 0, 0, 3);
			whichCtaMC.rollover1MC.addChild(rolloverRect);
			
			whichCtaMC.ctaTextWidth = ctaTextWidth;
			
		}
		
		function editCtaButtonDivWidth(toWidth) {
				
			var rightMargin = 20;
			ctaButtonDiv.style.width = (toWidth + 1 + rightMargin) + "px";
			
		}
		
		function doRollOver(event) {
			
			TweenMax.to([ctaMC.rollover1MC, ctaMC.rollover2MC, cta2MC.rollover1MC, cta2MC.rollover2MC], 0.4, {alpha: 1.0});
			ctaHover = true;
			ctaTL.play();
			
		}
		
		function doRollOut(event) {
			
			TweenMax.to([ctaMC.rollover1MC, ctaMC.rollover2MC, cta2MC.rollover1MC, cta2MC.rollover2MC], 0.4, {alpha: 0.0});
			ctaHover = false;
			
		}
		
		var ctaTL = new TimelineMax({paused:true, onComplete: function(){
			if(ctaHover) ctaTL.restart();
		}});
		
		ctaTL.fromTo([ctaMC.arrow1, cta2MC.arrow1], 0.3, {x: -10}, {x: 4, ease: Expo.easeIn}, 0.0)
			 .fromTo([ctaMC.arrow2, cta2MC.arrow2], 0.3, {x: -13}, {x: 1, ease: Expo.easeIn}, 0.05)
			 .set([ctaMC.arrow1, cta2MC.arrow1], {x:-25})
			 .set([ctaMC.arrow2, cta2MC.arrow2], {x:-28})
			 .to([ctaMC.arrow1, cta2MC.arrow1], 0.5, {x: -10, ease: Expo.easeOut}, 0.35)
			 .to([ctaMC.arrow2, cta2MC.arrow2], 0.5, {x: -13, ease: Expo.easeOut}, 0.45);
		
		function doAutoRotate() {
			productID++;
			bigJump = false;
			changeProduct();
			if (productID < numberOfProducts - 1) {
				TweenMax.delayedCall(autoRotateTime, doAutoRotate);
			}
		}
		self.cancelAutoRotate = function() {
			TweenMax.killDelayedCallsTo(doAutoRotate);
		}
		
		function animateInCreative() {
			
			var d = 0.1;
			
			/*
			if (VARIATION != "dollar" && VARIATION == "tvmania") {
				TweenMax.to(mmMC, 1.1, {y: 125, ease: Expo.easeInOut, delay: d});
				
				TweenMax.to(coverMC, 1.2, {y: 135, ease: Expo.easeInOut, delay: d, onComplete: function() {
					coverMC.visible = false;
				}});
				
				d += 1.05;
			} */
			
			/*
			TweenMax.to(mmMC, 0.6, {rotation: 3, ease: Quart.easeIn, delay: d});
			TweenMax.to(mmMC, 0.6, {rotation: 0, ease: Expo.easeOut, delay: d + 0.6});
			TweenMax.to(mmMC, 1.2, {scaleX: 0.15, scaleY: 0.15, x: 79, y: 228, ease: Expo.easeInOut, delay: d});
			*/
			
			//d += 0.35;
			var greyToX = 40;
			if (greyMC.currentFrame == 1) {
				greyToX = 0;
			}
			
			//TweenMax.to(greyMC, 0.4, {x: greyToX + 80, ease: Expo.easeIn, delay: d});
			//TweenMax.to(greyMC, 0.8, {x: greyToX, ease: Expo.easeOut, delay: d + 0.4});
			
			
			TweenMax.to(mm2MC, 0.9, {x: mm2ToX, ease: Expo.easeOut, delay: d + 0.6});
			TweenMax.to(mm2MC, 0.8, {alpha: 1.0, delay: d + 0.6});
			
			d += 0.1;
			/*
			TweenMax.to(logoMC, 0.8, {y: logoToY, ease: Expo.easeOut, delay: d});
			TweenMax.to(logoMC, 0.5, {alpha: 1.0, delay: d});
			*/
			
			TweenMax.to(campaignMC, 0.5, {scaleX: campaignScale * 0.5, scaleY: campaignScale * 0.5, ease: Expo.easeIn, delay: d});
			TweenMax.to(campaignMC, 0.5, {scaleX: campaignScale, scaleY: campaignScale, ease: Elastic.easeOut.config(2.0, 1.0), delay: d + 0.5});
			TweenMax.from(campaignMC, 0.9, {rotation: -30, ease: Expo.easeInOut, delay: d});
			
			d += 0.3;
			
			d += 0.2;
			
			if (VARIATION == "dollar") {
				self.d0.y = 92 + 30;
				self.d1.y = 115 + 30;
				self.d2.y = 139;
				self.d2.alpha = 0;
				
				self.doll0.rotation = -10;
				self.doll1.rotation = -30;
				self.doll2.rotation = 20;
				self.doll0.scaleX = self.doll0.scaleY = 0.6 * 0.75;
				self.doll1.scaleX = self.doll1.scaleY = 0.7 * 0.75;
				self.doll2.scaleX = self.doll2.scaleY = 0.65 * 0.75;
				self.doll0.x = 25 + 10;
				self.doll1.x = 145 + 20;
				self.doll2.x = 260 + 30;
				
				TweenMax.from(self.d0, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
				TweenMax.to(self.d0, 0.8, {y: 92, ease: Expo.easeOut, delay: d});
				
				d -= 0.2;
				TweenMax.to(self.doll0, 1.6, {x: 25, y: 90, scaleX: 0.6, scaleY: 0.6, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll0, 0.4, {rotation: 30, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll0, 0.6, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.2});
				
				d += 0.15;
				TweenMax.to(self.doll1, 2.1, {x: 145, y: 165, scaleX: 0.7, scaleY: 0.7, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll1, 0.5, {rotation: 20, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll1, 0.7, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.5});
			
				d += 0.15;
				TweenMax.to(self.doll2, 1.7, {x: 260, y: 105, scaleX: 0.65, scaleY: 0.65, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll2, 0.4, {rotation: -30, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll2, 0.7, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.2});
				d -= 0.3;
				
				d += 0.2 + 0.2;
				TweenMax.from(self.d1, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
				TweenMax.to(self.d1, 0.8, {y: 115, ease: Expo.easeOut, delay: d});
				
				d += 0.2;
				TweenMax.from(self.d2, 0.7, {x: 323, ease: Expo.easeOut, delay: d});
				TweenMax.to(self.d2, 0.0, {alpha: 1.0, delay: d});
				
				d += 2.5;
				TweenMax.to(self.doll1, 0.65, {y: dynamic.height - 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.doll0, 0.7, {y: dynamic.height - 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d + 0.05});
				TweenMax.to(self.doll2, 0.8, {y: dynamic.height - 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d + 0.07});
				TweenMax.to(self.doll1, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.doll2, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.doll0, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.d1, 0.4, {y: 115 + 27, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.d1, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				TweenMax.to(self.d2, 0.3, {alpha: 0.0, delay: d});
				TweenMax.to(self.d2, 0.3, {y: "+= 20", ease: Expo.easeIn, delay: d});
				TweenMax.to(self.d2, 0.7, {rotation: 4, ease: Expo.easeInOut, delay: d});
				
				d += 0.075;
				TweenMax.to(self.d0, 0.4, {y: 92 + 27, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.d0, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				d += 0.4;
			}
			
			if (VARIATION == "tvmania" && productID==0) {
				self.dollarcashback.scaleX=self.dollarcashback.scaleY=0.68;
				console.log(productID)
			
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:40,ease: Expo.easeIn, delay: 0.0});
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0, ease: Power3.easeIn, delay: 2.2});
				
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0.68, ease: Power3.easeOut, delay: 2.5});	
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
				TweenMax.killDelayedCallsTo(animateInManiaLabels);
				TweenMax.delayedCall(d, animateInManiaLabels);	
		}
			else if (VARIATION == "tvmania") {
				TweenMax.killDelayedCallsTo(animateInManiaLabels);
				TweenMax.delayedCall(d, animateInManiaLabels);
				
			}
			
			TweenMax.from(text1MC, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text1MC, 0.8, {y: text1ToY, ease: Expo.easeOut, delay: d});
			
			TweenMax.delayedCall(d - 1.5, checkNumberOfProducts);
			
			d += 0.2;
			TweenMax.from(text2MC, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text2MC, 0.8, {y: text2ToY, ease: Expo.easeOut, delay: d});
			
			if (productsObject[productID] == "video") {
				//dynamic.playVideo(1);
				videoPlayer.addEventListener("timeupdate", checkVideoCurrentTime, false);
				videoPlayer.addEventListener("volumechange", checkVideoMuted, false);
				videoPlayer.addEventListener("play", checkVideoPlayPause, false);
				videoPlayer.addEventListener("pause", checkVideoPlayPause, false);
				checkVideoMuted();
				
				var div = document.createElement("div");
				div.style.width = "30px";
				div.style.height = "30px";
				div.style.left = "0px";
				div.style.top = "180px";
				div.id = "unmute";
				document.getElementById("dom_overlay_container").appendChild(div);
				document.getElementById("unmute").addEventListener("click", doMuteUnmute, false);
				
				div = document.createElement("div");
				div.style.width = "20px";
				div.style.height = "30px";
				div.style.left = "30px";
				div.style.top = "180px";
				div.id = "pause";
				document.getElementById("dom_overlay_container").appendChild(div);
				document.getElementById("pause").addEventListener("click", doPlayPause, false);
				
				endVideoMC.x = endToX;
				TweenMax.delayedCall(d - 0.1, animateInVideo);
			}
			
			if (text3MC.dynText.text != "") {
				d += 0.3;
				TweenMax.to(text3MC, 0.7, {x: text3ToX, ease: Expo.easeOut, delay: d});
				TweenMax.to(text3MC, 1.0, {alpha: 1.0, delay: d});
				d -= 0.6;
			} else {
				d -= 0.3;
			}
			
			d += 0.2;
			TweenMax.to(productMC, 1.3, {x: productToX, ease: Expo.easeOut, delay: d});
			
			if (VARIATION == "dollar") {
				TweenMax.delayedCall(d + 0.1, animateInCash);
			}
			if (VARIATION == "tvmania") {
				TweenMax.delayedCall(d + 0.1, animateInDescription);
			}
			
			d += 0.2;
			TweenMax.to(leftMC, 0.9, {x: leftToX, ease: Expo.easeOut, delay: d});
			TweenMax.to(leftMC, 0.8, {alpha: 1.0, delay: d});
			TweenMax.to(rightMC, 0.9, {x: rightToX, ease: Expo.easeOut, delay: d});
			TweenMax.to(rightMC, 0.8, {alpha: 1.0, delay: d});
			
			if (servButtonToX < dynamic.width) {
				d += 0.3;
				TweenMax.to(servButtonMC, 0.6, {x: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: d});
				d -= 0.3;
			}
			
			d += 0.2;
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				TweenMax.delayedCall(d, animateInPrice);
			}
			
			TweenMax.delayedCall(d, animateInLabels);
			
			d += 0.2;
			TweenMax.delayedCall(d, animateInLabels2);
			
			//if (productsObject[productID].label == true) {
			if (self.cashback.dynText.text.length > 0) {
				d += 0.2;
				TweenMax.to(self.cashback, 0.6, {x: labelToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: d});
			}
			
			d += -0.2;
			TweenMax.to(ctaMC, 0.7, {alpha: 1.0, delay: d});
			TweenMax.to(ctaMC, 0.8, {x: ctaToX, ease: Expo.easeOut, delay: d});
			
		}
		
		function doLeftRightClick(event) {
			//console.log(event.target)
			var maxID = numberOfProducts - numberOfServices - 1;
			var minID = 0;
			if (productsObject[productID].service) {
				maxID = numberOfProducts - 1;
				minID = numberOfProducts - numberOfServices;
			} 
			if (event.target.name == "left" || event.target == leftButtonDiv) {
				if (productID > minID) {
					productID--;
					bigJump = false;
				} else {
					bigJump = true;
					productID = maxID;
				}
				isLeftClick = true;
			} else {
				if (productID < maxID) {
					productID++;
					bigJump = false;
				} else {
					bigJump = true;
					productID = minID;
				}
				isLeftClick = false;
			}
			self.cancelAutoRotate();
			changeProduct();
		}
		
		function adjustTextHeader() {
			
			var H1TextField = text1MC.dynText;
			var H2TextField = text2MC.dynText;
			
			var headerText1Width = H1TextField.getMeasuredWidth();
			var headerText2Width = H2TextField.getMeasuredWidth();
			/*
			if (H2TextField.getMeasuredWidth() > headerTextWidth) {
				headerTextWidth = H2TextField.getMeasuredWidth();
			}
			*/
			var idealHeaderTextWidth = 250;
			if (!dynamic.isImageEmpty(IMAGE_COUNT + 1)) {
				idealHeaderTextWidth = 170;
			}
			
			var headerFontScale = 1.0;
			if (headerText1Width > idealHeaderTextWidth) {
				headerFontScale = 1 / (headerText1Width / idealHeaderTextWidth);
			}
			text1MC.scaleX = text1MC.scaleY = headerFontScale;
			
			headerFontScale = 1.0;
			if (headerText2Width > idealHeaderTextWidth) {
				headerFontScale = 1 / (headerText2Width / idealHeaderTextWidth);
			}
			text2MC.scaleX = text2MC.scaleY = headerFontScale;
			
		}
		
		function updateLogoTextsLabelPrice() {
			
			text3MC.x = text3ToX + 30;
			text1MC.dynText.text = productsObject[productID].name;
			text2MC.dynText.text = productsObject[productID].main_specs;
			text3MC.dynText.text = ""; //productsObject[productID].sub_specs;
			adjustTextHeader();
			
			logoMC.y = - 70;
			logoMC.x += 20; 
			labelMC.x = dynamic.width + 40;
			self.cashback.x = labelMC.x;
			for (var i = 0; i<numberOfProducts; i++) {
				if (i == productID && logoMC.children[i] != undefined) {
					logoMC.children[i].alpha = labelMC.children[i].alpha = 1.0;
				} else if (logoMC.children[i] != undefined) {
					logoMC.children[i].alpha = labelMC.children[i].alpha = 0.0;
				}
			}
			
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf([a, a.gr.l0, a.gr.l1, a.gr]);
				self["g" + i].text.gotoAndStop(0);
			}
			if (productsObject[productID].sub_specs != undefined && productsObject[productID].sub_specs != "") {
				var s = productsObject[productID].sub_specs.split("^");
				if (s[0].split("_")[0].toLowerCase() == "bullet") {
					for (i = 0; i<maxGreen; i++) {
						var ID = 0;
						var a = self["g" + i];
						a.x = gToX;
						a.y = a.toY + 25;
						a.gr.l0.scaleX = a.gr.l1.scaleX = 0;
						a.gr.scaleX = a.gr.scaleY = 0.0;
						if (s[i] != undefined && s[i].split("_")[1] != undefined) {
							switch (s[i].split("_")[1].toLowerCase()) {
								case "bezorging":
								ID = 1;
								break;
								
								case "aansluiting":
								ID = 2;
								break;
								
								case "oud":
								ID = 3;
								break;
								
								case "retouneren":
								ID = 4;
								break;
								
								case "30dagen":
								ID = 5;
								break;
								
								case "plaatsing":
								ID = 5;
								break;
							}
						}
						a.text.gotoAndStop(ID);
					}
				}
			}
			
			self.servicelabel.gotoAndStop(0);
			self.cashback.dynText.text = "";
			while (self.cashback.bgr.numChildren > 0) {
				self.cashback.bgr.removeChild(self.cashback.bgr.children[0]);
			}
			var z = productsObject[productID].label_new;
			var lab2W = 0;
			if (z != undefined && z != "") {
				if (z.split("_")[0].toLowerCase() == "cashback") {
					self.cashback.dynText.text = z.split("_")[1];
					lab2W = Math.floor(self.cashback.dynText.getBounds().width) + 11;
					if (self.cashback.dynText.getMeasuredHeight() > 11) {
						//lab2W = 65;
					}
				} else if (z.split("_")[0].toLowerCase() == "service") {
					var b = z.split("_")[1].toLowerCase();
					var ID = 0;
					switch (b) {
						case "a": 
						ID = 1;
						break;
						
						case "b": 
						ID = 2;
						break;
						
						case "c": 
						ID = 3;
						break;
						
						case "d": 
						ID = 4;
						break;
						
						case "e": 
						ID = 5;
						break;
						
						case "f": 
						ID = 6;
						break;
						
						case "g": 
						ID = 7;
						break;
						
						case "h": 
						ID = 8;
						break;
						
						case "i": 
						ID = 9;
						break;
					}
					self.servicelabel.gotoAndStop(ID);
				}
			}
			if (self.servicelabel.currentFrame == 9) {
				self.servicelabel.y = 160;
			} else {
				self.servicelabel.y = 63;
			}
			if (self.cashback.dynText.text.length > 0) {
				var cashRect = new createjs.Shape();
				cashRect.graphics.beginFill("#e90000").drawRoundRectComplex(0, 0, lab2W, Math.floor(self.cashback.dynText.getMeasuredHeight()) + 7, 6, 6, 6, 6);
				self.cashback.bgr.addChild(cashRect);
			}
			
			lineMC.x = lineToX;
			TweenMax.killTweensOf(lineMC.line);
			lineMC.line.scaleX = 0;
			lineMC.alpha = 1.0;
			priceMC.x = priceToX;
			priceMC.alpha = 1.0;
			vanafMC.dynText.text = "";
			vanafMC.x = vanafToX + 15;
			self.prijs.x = prijsToX + 20;
			while (priceMC.numChildren > 0) {
				for (var k = 0; k<priceMC.children[0].numChildren; k++) {
					TweenMax.killTweensOf(priceMC.children[0].children[k]);
					for (var h = 0; h<priceMC.children[0].children[k].numChildren; h++) {
						TweenMax.killTweensOf(priceMC.children[0].children[k].children[h]);
					}
				}
				priceMC.removeChild(priceMC.children[0]);
			}
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				var newPriceMC = new createjs.MovieClip();
				priceMC.addChild(newPriceMC);
				var priceStr = productsObject[productID].price.split(" ");
				if (priceStr.length > 1) {
					vanafMC.dynText.text = priceStr[0].toUpperCase();
					priceStr = priceStr[1].split("");
				} else {
					priceStr = priceStr[0].split("");
				}
				for (i = 0; i<priceStr.length; i++) {
					var childMC = new createjs.MovieClip();
					newPriceMC.addChild(childMC);
					
					var charMC = new createjs.MovieClip();
					//var maskRect = new createjs.Shape();
					childMC.addChild(charMC);
					if (priceStr[i] != ".") {
						var numberID = Number(priceStr[i]) + 1;
						if (greyMC.currentFrame == 1) {
							numberID += 12;
						}
						dynamic.setDynamicImage(charMC, (NUMBERS + numberID), 0, 0);
					} else {
						var numbi = IMAGE_COUNT - 1;
						if (greyMC.currentFrame == 0) {
							numbi -= 12;
						}
						dynamic.setDynamicImage(charMC, (numbi), 0, 0);
					}
					if (i > 0) {
						childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale) - 6;
						if (priceStr[(i - 1)] != "." && newPriceMC.dot == true) {
							childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.7) - 4;
						}
					} else {
						childMC.toX = 0;
					}
					childMC.scaleX = childMC.scaleY = numberImgScale;
					if (newPriceMC.dot) {
						childMC.scaleX = childMC.scaleY = numberImgScale * 0.7;
						childMC.y += 1;
					}
					if (priceStr[i] == ".") {
						childMC.toX -= 0;
						newPriceMC.dot = true;
					}
					if (priceStr[(i - 1)] == ".") {
						childMC.toX -= 2;
					}
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					if (VARIATION == "dollar" && productID == 1) {
						childMC.y -= 10;
					}
				}
				if (!newPriceMC.dot) {
					var childMC = new createjs.MovieClip();
					newPriceMC.addChild(childMC);
					var charEndingMC = new createjs.MovieClip();
					childMC.addChild(charEndingMC);
					var numbi = IMAGE_COUNT;
					if (greyMC.currentFrame == 0) {
						numbi -= 12;
					}
					dynamic.setDynamicImage(charEndingMC, (numbi), 0, 0);
					childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale) - 6;
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					childMC.scaleX = childMC.scaleY = numberImgScale;
					if (VARIATION == "dollar" && productID == 1) {
						childMC.y -= 10;
					}
				}
			}
			
			if (productsObject[productID].priceOld != "" && productsObject[productID].priceOld != "-" && !isNaN(productsObject[productID].priceOld) && newPriceMC != undefined) {
				newPriceMC.oldPrice = true;
				var oldPriceMC = new createjs.MovieClip();
				newPriceMC.addChild(oldPriceMC);
				var oldPriceStr = productsObject[productID].priceOld.split("");
				oldPriceMC.y = -30;
				oldPriceMC.x += 2;
				for (i = 0; i<oldPriceStr.length; i++) {
					var childMC = new createjs.MovieClip();
					oldPriceMC.addChild(childMC);
					var charMC = new createjs.MovieClip();
					//var maskRect = new createjs.Shape();
					childMC.addChild(charMC);
					if (oldPriceStr[i] != ".") {
						var numberID = Number(oldPriceStr[i]) + 1;
						dynamic.setDynamicImage(charMC, (NUMBERS + numberID), 0, 0);
					} else {
						dynamic.setDynamicImage(charMC, (IMAGE_COUNT - 1 - 12), 0, 0);
					}
					if (i > 0) {
						childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.6) - 6 * 0.6;
						if (oldPriceStr[(i - 1)] != "." && oldPriceMC.dot == true) {
							childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.7 * 0.6) - 4 * 0.6;
						}
					} else {
						childMC.toX = 0;
					}
					childMC.scaleX = childMC.scaleY = numberImgScale * 0.6;
					if (oldPriceMC.dot) {
						childMC.scaleX = childMC.scaleY = numberImgScale * 0.7 * 0.6;
						childMC.y += 0;
					}
					if (oldPriceStr[i] == ".") {
						childMC.toX -= 0;
						oldPriceMC.dot = true;
					}
					if (oldPriceStr[(i - 1)] == ".") {
						childMC.toX -= 1;
					}
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
				}
				if (!oldPriceMC.dot) {
					var childMC = new createjs.MovieClip();
					oldPriceMC.addChild(childMC);
					var charEndingMC = new createjs.MovieClip();
					childMC.addChild(charEndingMC);
					dynamic.setDynamicImage(charEndingMC, (IMAGE_COUNT - 12), 0, 0);
					childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.6) - 6 * 0.6;
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					childMC.scaleX = childMC.scaleY = numberImgScale * 0.6;
				}
				oldPriceMC.priceWidth = oldPriceMC.children[oldPriceMC.numChildren - 1].toX + 9;
				if (oldPriceMC.dot == true) {
					oldPriceMC.priceWidth += 5;
				}
			}
		}
		
		function animateInPrice() {
			var currentPrice = priceMC.children[priceMC.numChildren - 1];
			var indexTill = currentPrice.numChildren;
			var delayPrice = 0.0;
			TweenMax.killTweensOf(lineMC.line);
			lineMC.line.scaleX = 0.0;
			lineMC.scaleY = 0.60;
			if (currentPrice.oldPrice == true) {
				indexTill--;
				lineMC.scaleX = 1 / (90 / currentPrice.children[currentPrice.numChildren - 1].priceWidth);
				for (var j = 0; j<currentPrice.children[currentPrice.numChildren - 1].numChildren; j++) {
					var oldNumberMC = currentPrice.children[currentPrice.numChildren - 1].children[j];
					TweenMax.to(oldNumberMC, 0.8, {x: oldNumberMC.toX, alpha: 1.0, ease: Expo.easeOut, delay: j * 0.03});
				}
				delayPrice = 0.3;
				TweenMax.to(lineMC.line, 0.7, {scaleX: 1.0, ease: Expo.easeOut, delay: 0.15});
			}
			for (var i = 0; i<indexTill; i++) {
				var numberMC = currentPrice.children[i];
				TweenMax.to(numberMC, 0.9, {x: numberMC.toX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + i * 0.03});
			}
			TweenMax.to(vanafMC, 0.9, {x: vanafToX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + 0});
			if (VARIATION == "dollar" && productID == 1) {
				TweenMax.to(self.prijs, 0.9, {x: prijsToX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + 0});
			}
		}
		
		function changeProduct() {
			
			dynamic.productID = productID;
			
			var d = 0.0;
			TweenMax.killDelayedCallsTo(updateLogoTextsLabelPrice);
			TweenMax.killDelayedCallsTo(animateInPrice);
			TweenMax.killDelayedCallsTo(editServicesButton);
			TweenMax.killDelayedCallsTo(animateInEndframe);
			TweenMax.killDelayedCallsTo(animateOutEndframe);
			TweenMax.killDelayedCallsTo(animateInLabels);
			TweenMax.killDelayedCallsTo(animateInLabels2);
			TweenMax.killDelayedCallsTo(animateInCheck);
			TweenMax.killTweensOf([text1MC, text2MC, text3MC, productMC, logoMC, labelMC, self.cashback, self.servicelabel, priceMC, lineMC, lineMC.line, ctaMC, cta2MC, vanafMC, servButtonMC, leftMC, rightMC, self.prijs, self.dollarcashback, self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3, self.subsubtext]);
			TweenMax.to([logoMC, self.cashback, priceMC, lineMC, vanafMC, self.subsubtext], 0.3, {alpha: 0.0});
			var moveBy = 100;
			if (isLeftClick == false || isLeftClick == undefined) {
				moveBy = -moveBy;
			}
			//TweenMax.to(labelMC, 0.4, {x: "+=" + (moveBy + 50), ease: Expo.easeIn});
			TweenMax.to(self.cashback, 0.4, {x: "+=" + (moveBy + 50), ease: Expo.easeIn});
			TweenMax.to(text3MC, 0.3, {x: text3ToX + moveBy, ease: Expo.easeIn, delay: 0.05});
			TweenMax.to(priceMC, 0.4, {x: priceToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			TweenMax.to(vanafMC, 0.4, {x: vanafToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			TweenMax.to(lineMC, 0.4, {x: lineToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			if (VARIATION == "dollar") {
				TweenMax.to([self.prijs, self.dollarcashback, self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.3, {alpha: 0.0});
				TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			
			if (VARIATION == "tvmania") {
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:40,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			if (VARIATION == "tvmania" && productID==0) {
				self.dollarcashback.scaleX=self.dollarcashback.scaleY=0.68;
				console.log(productID)
			
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:65,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0, ease: Power3.easeIn, delay: 2});
				
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0.68, ease: Power3.easeOut, delay: 2.3});	
				}
			 if (VARIATION == "tvmania" && productID==3) {
				
			console.log(productID)
				self.dollarcashback.scaleX=self.dollarcashback.scaleY=0.68;
			
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:40,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
				
			}
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf(a);
				if (a.text.currentFrame > 0) {
					TweenMax.to(a, 0.4, {x: gToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
					TweenMax.to(a, 0.3, {alpha: 0.0, delay: 0.0});
				}
			}
			if (productsObject[productID].service && servButtonMC.dynText.text == "BEKIJK ONZE SERVICES") {
				TweenMax.to(servButtonMC, 0.4, {x: dynamic.width, alpha: 0.0, ease: Expo.easeIn, delay: 0});
				TweenMax.to(servButtonMC, 0.6, {x: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0.4});
				TweenMax.to(servButtonMC, 0.4, {alpha: 1.0, delay: 0.4});
				TweenMax.delayedCall(0.4, editServicesButton, ["BEKIJK ONZE DEALS"]);
			}
			if (!productsObject[productID].service && servButtonMC.dynText.text == "BEKIJK ONZE DEALS") {
				TweenMax.to(servButtonMC, 0.4, {x: dynamic.width, ease: Expo.easeIn, alpha: 0.0, delay: 0});
				TweenMax.to(servButtonMC, 0.6, {x: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0.4});
				TweenMax.to(servButtonMC, 0.4, {alpha: 1.0, delay: 0.4});
				TweenMax.delayedCall(0.4, editServicesButton, ["BEKIJK ONZE SERVICES"]);
			}
			
			TweenMax.to(text3MC, 0.3, {alpha: 0.0});
			
			var productTargetX = productToX - productID * 300;
			if (bigJump == true) {
				var productTargetPrevX = productTargetX - 75;
				if (!(productID == 0) && prevProductID <= numberOfProducts - numberOfServices - 1) {
					productTargetPrevX += 150;
				} else if (productID == numberOfProducts - 1) {
					productTargetPrevX += 150;
				}
				TweenMax.to(productMC, 0.8, {x: productTargetPrevX, ease: Expo.easeIn, delay: d});
				TweenMax.to(productMC, 0.6, {x: productTargetX, ease: Expo.easeOut, delay: d + 0.8});
			} else {
				TweenMax.to(productMC, 1.2, {x: productTargetX, ease: Expo.easeInOut, delay: d});
			}
			prevProductID = productID;
			
			if (numberOfServices > 0) {
				var whichCtaMC = ctaMC;
				if (productsObject[productID].service) {
					TweenMax.to(ctaMC, 0.9, {x: ctaToX + dynamic.width / 2, ease: Expo.easeInOut, delay: d});
					TweenMax.to(ctaMC, 0.4, {alpha: 0.0, delay: d});
					TweenMax.to(cta2MC, 0.9, {x: ctaToX, ease: Expo.easeInOut, delay: d});
					TweenMax.to(cta2MC, 1.3, {alpha: 1.0, delay: d});
					whichCtaMC = cta2MC;
				} else {
					TweenMax.to(ctaMC, 0.9, {x: ctaToX, ease: Expo.easeInOut, delay: d});
					TweenMax.to(ctaMC, 1.3, {alpha: 1.0, delay: d});
					TweenMax.to(cta2MC, 0.9, {x: 0, ease: Expo.easeInOut, delay: d});
					TweenMax.to(cta2MC, 0.7, {alpha: 0.0, delay: d});
				}
				editCtaButtonDivWidth(whichCtaMC.ctaTextWidth);
			}
			
			//TweenMax.to(self.servicelabel, 0.4, {x: dynamic.width, ease: Expo.easeIn, delay: d});
			
			TweenMax.to(text2MC, 0.4, {y: text2ToY + 27, ease: Expo.easeInOut, delay: d});
			TweenMax.to(text2MC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			
			d += 0.075;
			TweenMax.to(text1MC, 0.4, {y: text1ToY + 27, ease: Expo.easeInOut, delay: d});
			TweenMax.to(text1MC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			
			d += 0.4;
			TweenMax.delayedCall(d, updateLogoTextsLabelPrice);
			
			if (typeof(productsObject[productID]) == "string") {
				TweenMax.to(productMC, 0.3, {alpha: 0.0, delay: d - 0.3});
				if (productsObject[productID] == "video") {
					TweenMax.delayedCall(d, animateInEndframe, [endVideoMC]);
				} else {
					TweenMax.delayedCall(d, animateInEndframe, [endMC]);
				}
			} else {
				TweenMax.to(productMC, 0.6, {alpha: 1.0, delay: d + 0.2});
				TweenMax.delayedCall(d - 0.5, animateOutEndframe);
			}
			if (productsObject[productID].service) {
				if (numberOfServices <= 1) {
					leftButtonDiv.style.display = rightButtonDiv.style.display = "none";
					TweenMax.to(leftMC, 0.8, {x: leftToX + 15, ease: Expo.easeOut, delay: d - 0.2});
					TweenMax.to(rightMC, 0.8, {x: rightToX - 15, ease: Expo.easeOut, delay: d - 0.2});
					TweenMax.to(leftMC, 0.4, {alpha: 0, delay: d - 0.4});
					TweenMax.to(rightMC, 0.4, {alpha: 0, delay: d - 0.4});
				} else {
					leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
					TweenMax.to(leftMC, 0.8, {x: leftToX, alpha: 1.0, ease: Expo.easeOut, delay: d});
					TweenMax.to(rightMC, 0.8, {x: rightToX, alpha: 1.0, ease: Expo.easeOut, delay: d});
				}
			} else if (numberOfProducts > 1) {
				TweenMax.to(leftMC, 0.8, {x: leftToX, alpha: 1, ease: Expo.easeOut, delay: d});
				TweenMax.to(rightMC, 0.8, {x: rightToX, alpha: 1, ease: Expo.easeOut, delay: d});
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
			}
			/*
			TweenMax.to(logoMC, 0.8, {y: logoToY, ease: Expo.easeOut, delay: d});
			TweenMax.to(logoMC, 0.5, {alpha: 1.0, delay: d});
			*/
			
			if (VARIATION == "dollar") {
				TweenMax.delayedCall(d + 0.1, animateInCash);
			}
			if (VARIATION == "tvmania") {
				//TweenMax.delayedCall(d + 0.1, animateInStripe);
				TweenMax.killDelayedCallsTo(animateInManiaLabels);
				TweenMax.delayedCall(d, animateInManiaLabels);
			}
			
			
			//d += 0.1;
			TweenMax.to(text1MC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text1MC, 0.8, {y: text1ToY, ease: Expo.easeOut, delay: d});
			
			d += 0.2;
			TweenMax.to(text2MC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text2MC, 0.8, {y: text2ToY, ease: Expo.easeOut, delay: d});
			
			if (text3MC.dynText.text != "") {
				d += 0.1;
				TweenMax.to(text3MC, 0.7, {x: text3ToX, ease: Expo.easeOut, delay: d});
				TweenMax.to(text3MC, 1.0, {alpha: 1.0, delay: d});
			}
			
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				d += 0.2;
				TweenMax.delayedCall(d, animateInPrice);
			}
			
			TweenMax.delayedCall(d, animateInLabels);
			
			//if (productsObject[productID].label == true) {
			d += 0.2;
			TweenMax.delayedCall(d, animateInLabels2);
			
		}
		
		function animateInManiaLabels() {
			var k = self.subsubtext;
			k.x = maniaLabelToX;
			k.alpha = 1.0;
			k.gotoAndStop(productID);
			var s = 0;
			while (k["m" + productID + s] != undefined) {
				s++;
			}
			var howMany = s;
			for (var i = 0; i<howMany; i++) {
				var n = k["m" + productID+i]
				console.log(n)
				TweenMax.killTweensOf(n);
				TweenMax.from(n, 0.8, {x: 50, alpha: 0.0, delay: i * 0.05});
			}
		}
		function animateInCash() {
			var cash = self["dollProd" + productID];
			cash.rotation = -10;
			var toX = [100, 100, 80, 85];
			var toY = [115, 110, 110, 125];
			var d = 0.0;
			cash.x = toX[productID];
			cash.y = toY[productID];
			cash.scaleX = cash.scaleY = 0.0;
			cash.alpha = 1.0;
			//cash.x = dynamic.width + 50;
			//TweenMax.to(cash, 1.0, {y: toY[productID], ease: Sine.easeOut, delay: d});
			TweenMax.to(cash, 0.2, {rotation: 10, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d + 0.5});
			TweenMax.to(cash, 0.8, {rotation: 0, ease: Elastic.easeOut.config(1.0, 1.0), delay: d + 1.1});
			TweenMax.to(cash, 1.0, {scaleX: 1.0, scaleY: 1.0, ease: Elastic.easeOut.config(1.0, 0.8), delay: d + 0.5});
			
			d += 0.1;
			var order = [1, 0, 2];
			for (var i = 0; i<3; i++) {
				var cash2 = self["dollRandom" + productID + i];
				cash2.rotation = Math.floor(Math.random() * 60) - 30;
				cash2.scaleX = cash2.scaleY = Math.floor(Math.random() * 25) / 100 + 0.7;
				var toRot = -cash2.rotation;
				cash2.y = -50;
				cash2.x = 40 + i * 100 + Math.floor(Math.random() * 40);
				var time = Math.floor(Math.random() * 10) / 10 + 1.2;
				TweenMax.to(cash2, time, {x: "-= 30", y: dynamic.height + 30, ease: Sine.easeOut, delay: d + order[i] * 0.3});
				TweenMax.to(cash2, 0.5, {rotation: toRot, ease: Sine.easeInOut, repeat: 6, yoyo: true, delay: d + order[i] * 0.3});
				//TweenMax.to(cash2, 0.6, {rotation: 0, ease: Cubic.easeOut, delay: d + i * 0.1 + 1.2});
			}
		}
		
		function animateInDescription() {
			var d = 0.0;
			d += 0.1;
			
		}
		function buildServicesButton(caption) {
			
			var servTextField = servButtonMC.dynText;
			editServicesButton(caption);
			var servButtonHeight = servTextField.getMeasuredHeight() + 30;
			var servRect = new createjs.Shape();
			servRect.graphics.beginFill("#ffffff").drawRoundRectComplex(3, 0, 74, servButtonHeight, 10, 10, 0, 0);
			servButtonMC.addChild(servRect);
			servButtonMC.swapChildren(servButtonMC.children[0], servButtonMC.children[1]);
			servButtonToX = dynamic.width - servButtonHeight + 12;
			
		}
		
		function editServicesButton(caption) {
			
			var servTextField = servButtonMC.dynText;
			servTextField.text = caption;
		
		}
		
		function doServices() {
			self.cancelAutoRotate();
			if (servButtonMC.dynText.text == "BEKIJK ONZE DEALS") {
				productID = 0;
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
			} else {
				productID = numberOfProducts - numberOfServices;
			}
			bigJump = true;
			changeProduct();
		}
		
		function animateInEndframe(whichEndMC) {
			var d = 0.0;
			for (var i = 0; i<lines; i++) {
				var lMC = whichEndMC["l" + i];
				d = i * 0.12;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
				TweenMax.to(lMC, 0.8, {y: lMC.toY, ease: Expo.easeOut, delay: d});
			}
		}
		
		function animateOutEndframe() {
			var d = 0.0;
			for (var i = 0; i<lines; i++) {
				var lMC = endMC["l" + i];
				d = (lines - i) * 0.05;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.4, {y: lMC.toY + 21, ease: Expo.easeInOut, delay: d});
				TweenMax.to(lMC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				lMC = endVideoMC["l" + i];
				d = (lines - i) * 0.05;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.4, {y: lMC.toY + 21, ease: Expo.easeInOut, delay: d});
				TweenMax.to(lMC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			}
		}
		
		function animateInLabels() {
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf([a, a.gr]);
				if (a.text.currentFrame > 0) {
					TweenMax.delayedCall(0 + i * 0.1, animateInCheck, [a]);
					TweenMax.to(a.gr, 0.6, {scaleX: 1.0, scaleY: 1.0, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0 + 0.1 + i * 0.075});
					TweenMax.to(a, 1.0, {y: a.toY, alpha: 1.0, ease: Expo.easeOut, delay: 0 + i * 0.075});
				}
			}
		}
		function animateInCheck(movieclip) {
			TweenMax.to(movieclip.gr.l0, 0.3, {scaleX: 1.0, ease: Expo.easeIn});
			TweenMax.to(movieclip.gr.l1, 0.5, {scaleX: 1.0, ease: Expo.easeOut, delay: 0.3});
		}
		function animateInLabels2() {
			if (self.cashback.dynText.text.length > 0) {
				TweenMax.to(self.cashback, 0, {alpha: 1.0});
				TweenMax.to(self.cashback, 0.6, {x: labelToX, ease: Elastic.easeOut.config(0.8, 1.2)});
			} else if (self.servicelabel.currentFrame > 0) {
				//TweenMax.to(self.servicelabel, 0.6, {x: serviceLabelToX, ease: Expo.easeOut});
			}
			if (VARIATION == "dollar") {
				var animIn = true;
				self.dollarcashback.alpha = 1.0;
				self.dollarcashback.x = dynamic.width + 150;
				if (productID == 1) {
					self.dollarcashback.text.gotoAndStop(0);
				} else if (productID == 2) {
					self.dollarcashback.text.gotoAndStop(1);
				} else {
					animIn = false;
				}
				if (animIn == true) {
					TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
			}
			
		if (VARIATION == "tvmania") {
				var animIn = true;
				self.dollarcashback.alpha = 1.0;
				self.dollarcashback.x = dynamic.width+150;
			self.dollarcashback.y= 90;
			if (productID == 0) {
					self.dollarcashback.text.gotoAndStop(0);
				TweenMax.delayedCall(1.1, gotoLastFrame);
				} 
				else if (productID == 3) {
					self.dollarcashback.y= 180;
				
					self.dollarcashback.text.gotoAndStop(1);
					
					
				} else {
					animIn = false;
				}
				if (animIn == true && productID == 0) {
					//TweenMax.to(self.dollarcashback, 0.6, {x: 130, y:112, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
				else if (animIn == true && productID == 3) {
					
				
					//TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
			}
			
		}
		function gotoLastFrame(){
			
			self.dollarcashback.text.gotoAndStop(2);
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// border
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape.setTransform(149.9898,249.4748,0.8988,0.04);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_1.setTransform(149.9898,0.5248,0.8988,0.04);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_2.setTransform(0.523,125.0111,0.0028,10.8002);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_3.setTransform(299.5231,125.0111,0.0028,10.8002);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// elements_over
	this.dollProd3 = new lib.dollar_4();
	this.dollProd3.name = "dollProd3";
	this.dollProd3.parent = this;
	this.dollProd3.setTransform(85.1,-40,1,1,0,0,0,0.1,0);

	this.dollProd2 = new lib.dollar_5();
	this.dollProd2.name = "dollProd2";
	this.dollProd2.parent = this;
	this.dollProd2.setTransform(80,-200);

	this.dollProd0 = new lib.dollar_1_v2();
	this.dollProd0.name = "dollProd0";
	this.dollProd0.parent = this;
	this.dollProd0.setTransform(100,-129.7);

	this.doll2 = new lib.dollar_3();
	this.doll2.name = "doll2";
	this.doll2.parent = this;
	this.doll2.setTransform(350,-80);

	this.doll1 = new lib.dollar_2();
	this.doll1.name = "doll1";
	this.doll1.parent = this;
	this.doll1.setTransform(200,-60);

	this.doll0 = new lib.dollar_1();
	this.doll0.name = "doll0";
	this.doll0.parent = this;
	this.doll0.setTransform(80,-70);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.doll0},{t:this.doll1},{t:this.doll2},{t:this.dollProd0},{t:this.dollProd2},{t:this.dollProd3}]}).wait(1));

	// text2Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3bFKIAAkiMAu3AAAIAAEig");
	mask.setTransform(149.9995,33.0206);

	// text2
	this.text2 = new lib.text2();
	this.text2.name = "text2";
	this.text2.parent = this;
	this.text2.setTransform(25.9,40.5,1,1,0,0,0,2.9,10.5);

	var maskedShapeInstanceList = [this.text2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.text2).wait(1));

	// text1Mask (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("A3bDIIAAj5MAu3AAAIAAD5g");
	mask_1.setTransform(149.9995,19.9932);

	// text1
	this.text1 = new lib.text1();
	this.text1.name = "text1";
	this.text1.parent = this;
	this.text1.setTransform(25.9,37,1,1,0,0,0,2.9,-2);

	var maskedShapeInstanceList = [this.text1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.text1).wait(1));

	// arrows
	this.left = new lib.arrowLeftRight();
	this.left.name = "left";
	this.left.parent = this;
	this.left.setTransform(19,149,1,1,0,0,180);
	this.left.alpha = 0;
	this.left.visible = false;

	this.right = new lib.arrowLeftRight();
	this.right.name = "right";
	this.right.parent = this;
	this.right.setTransform(281,149);
	this.right.alpha = 0;
	this.right.visible = false;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.right},{t:this.left}]}).wait(1));

	// text1_mask (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("A3WJEIAAkEMAutAAAIAAEEg");
	mask_2.setTransform(149.5,57.9931);

	// text1
	this.d0 = new lib.dollar_text_1();
	this.d0.name = "d0";
	this.d0.parent = this;
	this.d0.setTransform(23,400);

	var maskedShapeInstanceList = [this.d0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.d0).wait(1));

	// text2_mask (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("A3WK3IAAkEMAutAAAIAAEEg");
	mask_3.setTransform(149.5,69.4931);

	// text2
	this.d1 = new lib.dollar_text_2();
	this.d1.name = "d1";
	this.d1.parent = this;
	this.d1.setTransform(23,423);

	var maskedShapeInstanceList = [this.d1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.d1).wait(1));

	// rest
	this.d2 = new lib.dollar_text_3();
	this.d2.name = "d2";
	this.d2.parent = this;
	this.d2.setTransform(23,447);

	this.timeline.addTween(cjs.Tween.get(this.d2).wait(1));

	// elements
	this.subsubtext = new lib.subsubtext();
	this.subsubtext.name = "subsubtext";
	this.subsubtext.parent = this;
	this.subsubtext.setTransform(322.35,80);

	this.dollRandom32 = new lib.dollar_4();
	this.dollRandom32.name = "dollRandom32";
	this.dollRandom32.parent = this;
	this.dollRandom32.setTransform(210.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom30 = new lib.dollar_4();
	this.dollRandom30.name = "dollRandom30";
	this.dollRandom30.parent = this;
	this.dollRandom30.setTransform(130.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom31 = new lib.dollar_4();
	this.dollRandom31.name = "dollRandom31";
	this.dollRandom31.parent = this;
	this.dollRandom31.setTransform(30.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom22 = new lib.dollar_5();
	this.dollRandom22.name = "dollRandom22";
	this.dollRandom22.parent = this;
	this.dollRandom22.setTransform(235,-130);

	this.dollRandom20 = new lib.dollar_5();
	this.dollRandom20.name = "dollRandom20";
	this.dollRandom20.parent = this;
	this.dollRandom20.setTransform(130,-126);

	this.dollRandom21 = new lib.dollar_5();
	this.dollRandom21.name = "dollRandom21";
	this.dollRandom21.parent = this;
	this.dollRandom21.setTransform(30,-125);

	this.dollRandom12 = new lib.dollar_3();
	this.dollRandom12.name = "dollRandom12";
	this.dollRandom12.parent = this;
	this.dollRandom12.setTransform(235.05,-80);

	this.dollRandom10 = new lib.dollar_3();
	this.dollRandom10.name = "dollRandom10";
	this.dollRandom10.parent = this;
	this.dollRandom10.setTransform(125,-40);

	this.dollRandom11 = new lib.dollar_3();
	this.dollRandom11.name = "dollRandom11";
	this.dollRandom11.parent = this;
	this.dollRandom11.setTransform(45,-90);

	this.dollRandom00 = new lib.dollar_1_v2();
	this.dollRandom00.name = "dollRandom00";
	this.dollRandom00.parent = this;
	this.dollRandom00.setTransform(160,-40,0.8,0.8);

	this.dollRandom02 = new lib.dollar_1_v2();
	this.dollRandom02.name = "dollRandom02";
	this.dollRandom02.parent = this;
	this.dollRandom02.setTransform(265,-30,0.8,0.8);

	this.dollRandom01 = new lib.dollar_1_v2();
	this.dollRandom01.name = "dollRandom01";
	this.dollRandom01.parent = this;
	this.dollRandom01.setTransform(40,-30,0.8,0.8);

	this.dollarcashback = new lib.dollar_cashback();
	this.dollarcashback.name = "dollarcashback";
	this.dollarcashback.parent = this;
	this.dollarcashback.setTransform(465,-42.25);

	this.servicelabel = new lib.label_service();
	this.servicelabel.name = "servicelabel";
	this.servicelabel.parent = this;
	this.servicelabel.setTransform(700,63);

	this.cashback = new lib.label_cashback();
	this.cashback.name = "cashback";
	this.cashback.parent = this;
	this.cashback.setTransform(580,176);

	this.g2 = new lib.label_green();
	this.g2.name = "g2";
	this.g2.parent = this;
	this.g2.setTransform(600,101,0.5,0.5);

	this.g1 = new lib.label_green();
	this.g1.name = "g1";
	this.g1.parent = this;
	this.g1.setTransform(600,84,0.5,0.5);

	this.g0 = new lib.label_green();
	this.g0.name = "g0";
	this.g0.parent = this;
	this.g0.setTransform(600,67,0.5,0.5);

	this.lineContainer = new lib.price_old_line_container();
	this.lineContainer.name = "lineContainer";
	this.lineContainer.parent = this;
	this.lineContainer.setTransform(-130,137);

	this.price = new lib.emptyMovieClip();
	this.price.name = "price";
	this.price.parent = this;
	this.price.setTransform(23,165);

	this.servButton = new lib.services_button();
	this.servButton.name = "servButton";
	this.servButton.parent = this;
	this.servButton.setTransform(310,175,1,1,-89.9948);

	this.label = new lib.emptyMovieClip();
	this.label.name = "label";
	this.label.parent = this;
	this.label.setTransform(225,178);

	this.product = new lib.emptyMovieClip();
	this.product.name = "product";
	this.product.parent = this;
	this.product.setTransform(168,146);

	this.campaign = new lib.emptyMovieClip();
	this.campaign.name = "campaign";
	this.campaign.parent = this;
	this.campaign.setTransform(245,31);

	this.dollProd1 = new lib.dollar_3();
	this.dollProd1.name = "dollProd1";
	this.dollProd1.parent = this;
	this.dollProd1.setTransform(100,-38.05);

	this.prijs = new lib.dollar_prijs();
	this.prijs.name = "prijs";
	this.prijs.parent = this;
	this.prijs.setTransform(626,190);
	this.prijs.alpha = 0;

	this.endframe_video = new lib.endframe_video();
	this.endframe_video.name = "endframe_video";
	this.endframe_video.parent = this;
	this.endframe_video.setTransform(-700,84);

	this.endframe = new lib.endframe_tv_audio();
	this.endframe.name = "endframe";
	this.endframe.parent = this;
	this.endframe.setTransform(-422,85);

	this.mediamarkt2 = new lib.mediamarkt_logo();
	this.mediamarkt2.name = "mediamarkt2";
	this.mediamarkt2.parent = this;
	this.mediamarkt2.setTransform(75.05,330.1,0.14,0.14,0,0,0,0.4,0.7);

	this.cover2 = new lib.shape3();
	this.cover2.name = "cover2";
	this.cover2.parent = this;
	this.cover2.setTransform(0,350,1,1,0,0,0,0,40);

	this.cta2 = new lib.cta();
	this.cta2.name = "cta2";
	this.cta2.parent = this;
	this.cta2.setTransform(-10,218);

	this.vanaf = new lib.vanaf();
	this.vanaf.name = "vanaf";
	this.vanaf.parent = this;
	this.vanaf.setTransform(-224,150);
	this.vanaf.alpha = 0;

	this.text3 = new lib.text3();
	this.text3.name = "text3";
	this.text3.parent = this;
	this.text3.setTransform(69,147.1,1,1,0,0,0,45,23.1);
	this.text3.alpha = 0;

	this.white = new lib.shape2();
	this.white.name = "white";
	this.white.parent = this;
	this.white.setTransform(-50,0);

	this.cta = new lib.cta();
	this.cta.name = "cta";
	this.cta.parent = this;
	this.cta.setTransform(550,218);

	this.logo = new lib.emptyMovieClip();
	this.logo.name = "logo";
	this.logo.parent = this;
	this.logo.setTransform(25,0);
	this.logo.alpha = 0;

	this.grey = new lib.shape_grey();
	this.grey.name = "grey";
	this.grey.parent = this;
	this.grey.setTransform(300,40);

	this.cover = new lib.cover1();
	this.cover.name = "cover";
	this.cover.parent = this;
	this.cover.setTransform(-5.5,265);

	this.mediamarkt = new lib.mediamarkt_logo();
	this.mediamarkt.name = "mediamarkt";
	this.mediamarkt.parent = this;
	this.mediamarkt.setTransform(150.05,289.1,0.27,0.27,0,0,0,0.4,0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mediamarkt},{t:this.cover},{t:this.grey},{t:this.logo},{t:this.cta},{t:this.white},{t:this.text3},{t:this.vanaf},{t:this.cta2},{t:this.cover2},{t:this.mediamarkt2},{t:this.endframe},{t:this.endframe_video},{t:this.prijs},{t:this.dollProd1},{t:this.campaign},{t:this.product},{t:this.label},{t:this.servButton},{t:this.price},{t:this.lineContainer},{t:this.g0},{t:this.g1},{t:this.g2},{t:this.cashback},{t:this.servicelabel},{t:this.dollarcashback},{t:this.dollRandom01},{t:this.dollRandom02},{t:this.dollRandom00},{t:this.dollRandom11},{t:this.dollRandom10},{t:this.dollRandom12},{t:this.dollRandom21},{t:this.dollRandom20},{t:this.dollRandom22},{t:this.dollRandom31},{t:this.dollRandom30},{t:this.dollRandom32},{t:this.subsubtext}]}).wait(1));

	// bgmove
	this.instance = new lib.loopy();
	this.instance.parent = this;
	this.instance.setTransform(300,103,1,1,0,0,0,300,103);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// hitAreaMC
	this.hitAreaMC = new lib.shape1();
	this.hitAreaMC.name = "hitAreaMC";
	this.hitAreaMC.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.hitAreaMC).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-550,-103.9,1307.8,565.9);
// library properties:
lib.properties = {
	id: 'CB71CC1E45F7401086B269D4402BC0DC',
	width: 300,
	height: 250,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/grey_color.png", id:"grey_color"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['CB71CC1E45F7401086B269D4402BC0DC'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;