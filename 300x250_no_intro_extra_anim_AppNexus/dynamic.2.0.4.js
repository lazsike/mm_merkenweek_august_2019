// Last updated by Laszlo on 18 March 2019
var dynamic = {

    currentMovie: null,
    currentShot: 0,
    container: null,
    width: 0,
    height: 0,
    tzhgd: 0, //for Safari exitURL issue

    dynamicData: [],
    shots: [],

    currentDynamicBackground: undefined,
    bVideoWasPlayed: false,
    currentVideo: undefined,
    videoElements: [], //dom video elements
    max_shots: 5,
    onInit: function() {

        document.addEventListener("visibilitychange", this.checkVisibility, false);

        this.width = comp.getLibrary().properties.width;
        this.height = comp.getLibrary().properties.height;

        stage.update();

        this.currentMovie = exportRoot;

        this.container = document.getElementById("animation_container");
        this.container.style.cursor = "pointer";
        
        if (this.swipeCanvas == true) {
            // swipeable creative, click/mouseover/mouseout events set in creative (!) - except swipe_area
            document.getElementById("swipe_area").addEventListener("click", this.onMouseClick.bind(this));
        } else if (this.currentMovie.hitAreaMC != undefined) {
            // not swipeable but creative with more click/mouseover/mouseout events
            this.currentMovie.hitAreaMC.addEventListener("click", this.onMouseClick.bind(this));
        } else {
            // regular creative with 1 click/mouseover/mouseout event
            this.container.addEventListener("click", this.onMouseClick.bind(this));
            this.container.addEventListener("mouseover", this.onMouseOver.bind(this));
            this.container.addEventListener("mouseout", this.onMouseOut.bind(this));
            this.container.addEventListener("mousemove", this.onMouseMove.bind(this));
        }

        for (var i = 1; i <= this.max_shots; i++) {
            var shotData = exportRoot.getShotData(i);
            if (shotData != undefined)
                shotData.index = i;
            if (shotData != undefined)
                this.shots.push(shotData);
        }
        if (this.shots.length >= 1) {
            this.shots[this.shots.length - 1].isLastShot = true;
            this.playNextShot();
            //console.log(this.shots);
        }

        // SAFARI ISSUE FIX BEGIN
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf("safari") != -1) {
            if (ua.indexOf("chrome") > -1) {
                // Chrome
            } else {
                // Safari
                TweenMax.ticker.useRAF(false);
            }
        }
        // SAFARI ISSUE FIX END
    },
    getCurrentShot: function() {
        var shot = this.shots[this.currentShot - 1];
        return shot;
    },
    playNextShot: function() {
        this.currentShot++;

        var shot = this.shots[this.currentShot - 1];

        var timeout = exportRoot.getShotDuration(shot);

        if (this.shots[this.currentShot - 1].isLastShot != true) {
            TweenMax.delayedCall(timeout, function() {
                dynamic.playNextShot();
            });
        }
        console.log("Playing Shot #" + this.currentShot);
        exportRoot.playShot(shot);
    },

    getImageID: function(img) {
        for (var i = 0; i < comp.getLibrary().properties.manifest.length; i++)
            if (img.image.src == comp.getLibrary().properties.manifest[i].src) {
                return i;
            }
    },

    getDynamicImageIndex: function(indx, shotIndex) {
        indx = parseInt(indx);

        if (shotIndex == undefined)
            shotIndex = this.currentShot;
        var startFromIndex = 0;
        for (var i = 0; i < this.dynamicData.length; i++) {
            if (i < (shotIndex - 1)) startFromIndex += parseInt(this.dynamicData[i].image_count);
        }
        return indx + startFromIndex - 1;
    },

    copyDynamicImage: function(fromImage, toImage) {
        if (fromImage.children.length == 0) {

            return;

        }
        var bitmap = new createjs.Bitmap(fromImage.children[0].image);
        bitmap.regX = Math.round(bitmap.image.width * 0.5);
        bitmap.regY = Math.round(bitmap.image.height * 0.5);
        while (toImage.getNumChildren() > 0)
            toImage.removeChild(toImage.children[0]);

        toImage.addChild(bitmap);
    },

    setAbsoluteDynamicImage: function(clip, indx) {

        var theIndex = indx - 1;

        while (clip.getNumChildren() > 0)
            clip.removeChild(clip.children[0]);


        var img = this.getImageObject(theIndex);
        var bitmap = new createjs.Bitmap(img);
        bitmap.regX = Math.round(bitmap.image.width * 0.5);
        bitmap.regY = Math.round(bitmap.image.height * 0.5);

        clip.addChild(bitmap);

        clip.placeholderMC.visible = false;

    },
    setDynamicImage: function(copyTo, indx, regX, regY) {
        indx = parseInt(indx);

        var shotIndex = this.currentShot;

        if (regX == undefined) regX = 0.5;
        if (regY == undefined) regY = 0.5;
        var startFromIndex = 0;
        var theIndex = 0;

        for (var i = 0; i < this.dynamicData.length; i++) {
            if (i < (shotIndex - 1)) startFromIndex += parseInt(this.dynamicData[i].image_count);
        }
        theIndex = indx + startFromIndex - 1;

        if (copyTo != undefined)
            clip = copyTo;

        while (clip.getNumChildren() > 0)
            clip.removeChild(clip.children[0]);


        var img = this.getImageObject(theIndex);
        var bitmap = new createjs.Bitmap(img);
        bitmap.regX = Math.round(bitmap.image.width * regX);
        bitmap.regY = Math.round(bitmap.image.height * regY);
        var imageData = this.getDynamicImageData(theIndex);
        bitmap.scaleX = bitmap.scaleY = imageData.scale;
        bitmap.rotation = imageData.rotation;
        bitmap.x = Math.round(imageData.x);
        bitmap.y = Math.round(imageData.y);

        clip.addChild(bitmap);

        if (clip.placeholderMC != undefined)
            clip.placeholderMC.visible = false;

    },
    isImageEmpty: function(valueID, shotID) {
        if (shotID == undefined)
            shotID = this.currentShot;
        var imgIndex = this.getDynamicImageIndex(valueID, shotID);
        var theImage = this.getImageObject(imgIndex);


        if (theImage == undefined || theImage.width == 1 && theImage.height == 1) {
            //console.log(" image " + valueID + "is empty ");
            return true;
        }

        return false;
    },
    createVideoElement: function(videoURL) {

        var videoTagAttributes = " muted playsinline";

        if (dynamic.loopVideo == true) videoTagAttributes += " loop";

        document.getElementById("video_holder").innerHTML = "<video src='" + videoURL + "'" + videoTagAttributes + " preload='none'>";
        var newVideo = document.getElementsByTagName("video")[0];
        newVideo.load();

        this.videoElements.push(newVideo);

    },
    findVideoElement: function(theUrl) {
        for (var v = 0; v < dynamic.videoElements.length; v++) {

            if (dynamic.videoElements[v] == undefined) continue;

            var fileName1 = dynamic.videoElements[v].src.replace(/^.*[\\\/]/, '');
            var fileName2 = theUrl.replace(/^.*[\\\/]/, '');
            if (fileName1 == fileName2)
                return dynamic.videoElements[v];
        }

        return undefined;
    },
    loadDynamicVideo: function(clip) {

        var shotIndex = this.currentShot;
        var self = this;
        if (shotIndex == undefined || this.videoElements[shotIndex - 1] == undefined) return;

        var background = new createjs.Shape();
        clip.addChild(background);

        var video = new createjs.Bitmap(this.videoElements[shotIndex - 1]);

        video.x = -this.width / 2;
        video.y = -this.height / 2;

        clip.addChild(video);

        clip.playVideo = function() {
            console.log(this);
            self.playVideo(shotIndex);
        }
    },
    playVideo: function(shotIndex) {
        var self = dynamic;
        var newVideo = self.videoElements[shotIndex - 1];

        newVideo.play();
        self.currentVideo = newVideo;

        if (self.bVideoWasPlayed == false) {
            TweenMax.delayedCall(30.0, function() {
                for (var v = 0; v < self.videoElements.length; v++) {
                    if (self.videoElements[v] == undefined) continue;
                    self.videoElements[v].pause();
                }
            });
        }

        self.bVideoWasPlayed = true;
    },
    setDynamicLogo: function(clip, regX, regY) {

        if (clip == undefined) return;
        while (clip.getNumChildren() > 0)
            clip.removeChild(clip.children[0]);

        var img = this.getLogoImage();
        var bitmap = new createjs.Bitmap(img);
        bitmap.regX = Math.round(bitmap.image.width * regX);
        bitmap.regY = Math.round(bitmap.image.height * regY);

        clip.addChild(bitmap);

    },
    getLogoImage: function() {
        return this.getImageObject(20);
    },
    getImageObject: function(index) {
        var images = comp.getImages();

        return images['Image' + (index + 1)];
    },
    onMouseMove: function(e) {
        if (this.currentMovie.onMouseMove != undefined) {
            this.currentMovie.onMouseMove(e);
        }
    },
    onMouseOver: function() {

        if (this.currentMovie.onMouseOver != undefined) {
            this.currentMovie.onMouseOver();
        }
    },
    onMouseOut: function() {

        if (this.currentMovie.onMouseOut != undefined) {
            this.currentMovie.onMouseOut();
        }
    },
    onMouseClick: function() {

        var exitURL = dynamic.getDynamic("url");
        if (dynamic.isVideoPlaying == undefined || dynamic.isVideoPlaying == true) {
            Enabler.exitOverride("ExitEvent", exitURL);
        } else {
            this.playVideo(1);
        }

    },

    onAddEnded: function() {
        this.currentMovie.stop();
        console.log("Ad has ended.");
    },

    getMovieByName: function(theName) {


        for (var i = 0; i < exportRoot.children.length; i++) {

            if (exportRoot.children[i].name == theName)
                return exportRoot.children[i];
        }

        for (var i = 0; i < this.movieClipsToClone.length; i++) {
            var movieClipToClone = this.movieClipsToClone[i].clipName + "" + this.movieClipsToClone[i].index;

            if (movieClipToClone.toLowerCase() == theName)
                return this.cloneMovieClip(this.movieClipsToClone[i].clipName.toLowerCase(), this.movieClipsToClone[i].index);
        }

        console.log("ERROR movie `" + theName + "` not found!");

        return undefined;
    },
    getDynamicImageData: function(index) {
        for (var i = 1; i <= 5; i++) {
            if (dynamicContent.dcodata[0]['ImageData' + i] == undefined) dynamicContent.dcodata[0]['ImageData' + i] = "0x0x1x0;0x0x1x0;0x0x1x0";
        }
        var feedContent = dynamicContent.dcodata[0].ImageData1 + dynamicContent.dcodata[0].ImageData2 + dynamicContent.dcodata[0].ImageData3 + dynamicContent.dcodata[0].ImageData4 + dynamicContent.dcodata[0].ImageData5;
        var imagesArray = feedContent.split(';');

        if (imagesArray[index] == undefined) {
            return { x: 0, y: 0, scale: 1, rotation: 0 }
        }
        var valuesArray = imagesArray[index].split('x');

        var newObject = {
            x: valuesArray[0],
            y: valuesArray[1],
            scale: valuesArray[2],
            rotation: valuesArray[3]
        };

        return newObject;
    },
    getImageURL: function(index) {
        return comp.getLibrary().properties.manifest[index].src;
    },
    getDynamicImage: function(parentClip) {

        for (var i = 0; i < parentClip.children.length; i++) {
            var clip = parentClip.children[i];
            if (clip.image != undefined) return clip.image;
        }
        return undefined;
    },

    getTillDate: function function_name() {

        return dynamicContent.dcodata[0].Date_Till.RawValue;
    },
    getCounterDays: function() {
        var bIsCounter = false;

        var shotType = dynamic.getDynamic("shot_type", this.currentShot).split("|");

        for (var i = 0; i < shotType.length; i++)
            if (shotType[i] == "counter") bIsCounter = true;

        if (bIsCounter == true) {
            var maxDays = 7;
            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            var firstDate = new Date();
            var secondDate = new Date(this.getTillDate());

            var diffDays = Math.ceil((secondDate.getTime() - firstDate.getTime()) / (oneDay));

            return diffDays;
        } else {
            return 0;
        }


    },
    getDynamic: function(valueID, shotID) {
        var ctaOptions = dynamicContent.dcodata[0].CTA.split("|");
        if (this.dynamicData.length < shotID) return "";


        if (shotID == undefined) {
            if (valueID.indexOf("image") != -1) {
                return {
                    x: 0,
                    y: 0,
                    scale: 1,
                    rotation: 0
                };
            }
            shotID = this.currentShot;
        }
        if (valueID == "clip_name") return this.dynamicData[shotID - 1].clip_name;
        else if (valueID == "image1") return this.getDynamicImageData(0 + 3 * (shotID - 1));
        else if (valueID == "image2") return this.getDynamicImageData(1 + 3 * (shotID - 1));
        else if (valueID == "image3") return this.getDynamicImageData(2 + 3 * (shotID - 1));
        else if (valueID == "shot_type") {

            return this.dynamicData[shotID - 1].shot_type;
        } else if (valueID == "font_scale") {
            return this.dynamicData[shotID - 1].font_scale;


        } else if (valueID == "copy") {
            var line = this.dynamicData[shotID - 1].copy;
            if (line != undefined) return line.replace(/<br\s*[\/]?>/gi, "\n")
            return "";
        } else if (valueID == "geo_copy") {
            var line = dynamicContent.dcodata[0].GeoCopy;
            if (line != undefined) return line.replace(/<br\s*[\/]?>/gi, "\n")
            return "";
        } else if (valueID == "cta_copy") {
            var line = this.dynamicData[shotID - 1].cta_copy;
            if (line != undefined) return line.replace(/<br\s*[\/]?>/gi, "\n")
            return "";
        } else if (valueID == "price_type") {
            var line = this.dynamicData[shotID - 1].price_type;
            if (line != undefined) return line.replace(/<br\s*[\/]?>/gi, "\n")
            return "";
        } else if (valueID == "cta_copy") return this.dynamicData[shotID - 1].cta_copy;
        else if (valueID == "url") return this.dynamicData[shotID - 1].url.Url;
        else if (valueID == "price_type") return this.dynamicData[shotID - 1].price_type;

        else if (valueID == "shot_data") {

            return this.dynamicData[shotID - 1].price_copy;
        } else if (valueID == "price") {

            return this.dynamicData[shotID - 1].price_copy;

        } else if (valueID == "cta_options") {
            return ctaOptions[shotID - 1];
        } else {
            alert("Sorry, but there is no dynamic value of this name: " + valueID);
        }
    },


    setDynamicContent: function() {

        //console.log(dynamicContent)
        if (typeof(dynamicContent) == "undefined" || dynamicContent.dcodata == undefined) {
            //document.getElementById("backupImage").style.display = "block";
            return;
        }

        this.loadMovies(dynamicContent.dcodata[0]);
    },
    loadMovies: function(data) {

        for (var i = 1; i <= 5; i++)
            if (data['Shot' + i] != undefined && data['Shot' + i] != "") {
                this.dynamicData.push(this.parseDynamicData(data['Shot' + i], data['ExitURL' + i]));
            }


    },

    parseDynamicData: function(d, url) {
        var valuesArray = d.split(';');
        // if (valuesArray.length != 6) alert("Error - shot data is missing some values");

        var newObject = {
            shot_type: valuesArray[0],
            copy: valuesArray[1],
            cta_copy: valuesArray[2],
            price_type: valuesArray[3],
            price_copy: valuesArray[4],
            font_scale: valuesArray[5],
            image_count: valuesArray[6],
            url: url
        }
        return newObject;
    },

    checkVisibility: function() {
        if (document.visibilityState == "hidden") {
            clearTimeout(this.tzhgd);
            TweenMax.ticker.sleep();
        } else {
            this.tzhgd = setTimeout(function() { 
                TweenMax.ticker.wake();
            }, 40);
        }
    }
}

function swipedetect(el, callback) {

    var touchsurface = el,
        swipedir,
        startX,
        startY,
        distX,
        distY,
        threshold = 50, //required min distance traveled to be considered swipe
        restraint = 100, // maximum distance allowed at the same time in perpendicular direction
        allowedTime = 300, // maximum time allowed to travel that distance,
        clickTime = 100,
        elapsedTime,
        startTime,
        handleswipe = callback || function(swipedir){}

    touchsurface.addEventListener('touchstart', function(e){
        var touchobj = e.changedTouches[0]
        swipedir = 'none'
        dist = 0
        startX = touchobj.pageX
        startY = touchobj.pageY
        startTime = new Date().getTime() // record time when finger first makes contact with surface
        e.preventDefault()
    }, false)

    touchsurface.addEventListener('touchmove', function(e){
        e.preventDefault() // prevent scrolling when inside DIV
    }, false)

    touchsurface.addEventListener('touchend', function(e){
        var touchobj = e.changedTouches[0];
        distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime // get time elapsed
        if (elapsedTime <= allowedTime && elapsedTime > clickTime){ // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ // 2nd condition for horizontal swipe met
                swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
                swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
            }
        }
        if (elapsedTime <= clickTime && Math.abs(distX) <= 5 && Math.abs(distY) <= 5) {
            swipedir = 'click';
        }
        handleswipe(swipedir)
        e.preventDefault()
    }, false)

}