(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.grey_field = function() {
	this.initialize(img.grey_field);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2551,515);


(lib.intro_img = function() {
	this.initialize(img.intro_img);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,774,314);


(lib.logobalk = function() {
	this.initialize(img.logobalk);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,2544,226);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.vanaf = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "16px 'Geomanist'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 6;
	this.dynText.lineWidth = 176;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.vanaf, new cjs.Rectangle(0,0,180,23.9), null);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.grey_field();
	this.instance.parent = this;
	this.instance.setTransform(-1624.85,-164,0.6369,0.6369);

	this.instance_1 = new lib.grey_field();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0.15,-164,0.6369,0.6369);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1624.8,-164,3249.7,328);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.grey_field();
	this.instance.parent = this;
	this.instance.setTransform(-0.85,-164,0.6369,0.6369);

	this.instance_1 = new lib.grey_field();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-1623.85,-164,0.6369,0.6369);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1623.8,-164,3247.7,328);


(lib.text3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "13px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 12;
	this.dynText.lineWidth = 86;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text3, new cjs.Rectangle(0,0,90,25.6), null);


(lib.text2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "41px 'Geomanist'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 43;
	this.dynText.lineWidth = 996;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text2, new cjs.Rectangle(0,0,1000,45), null);


(lib.text1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "58px 'Geomanist'", "#E31E26");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 60;
	this.dynText.lineWidth = 996;
	this.dynText.parent = this;
	this.dynText.setTransform(2,-43);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text1, new cjs.Rectangle(0,-45,1000,62), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgtBIQgOgMgGgQIArgYQALARALAAQAEAAAAgDQAAgFgJgFIgWgLQghgQAAgeQAAgYARgOQASgPAbAAQAaAAATAPQAPALAEAOIgqAYQgIgPgLAAQgFgBAAAFQAAAFAKAFIAXAMQAhAQAAAcQAAAZgSAOQgSAPgeAAQgaAAgTgPg");
	this.shape.setTransform(274.525,52.15);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag5BDQgTgTAAggIAAhlIA9AAIAABnQAAARAPABQARgBAAgRIAAhnIA8AAIAABlQABAggUATQgVASglAAQgkAAgVgSg");
	this.shape_1.setTransform(259.2,52.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgeBUIAAh3IggAAIAAgwIB+AAIAAAwIgiAAIAAB3g");
	this.shape_2.setTransform(244.1,52.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgtBIQgOgMgGgQIArgYQALARALAAQAEAAAAgDQAAgFgJgFIgWgLQghgQAAgeQAAgYARgOQASgPAbAAQAaAAATAPQAPALAEAOIgqAYQgIgPgLAAQgFgBAAAFQAAAFAKAFIAXAMQAhAQAAAcQAAAZgSAOQgSAPgeAAQgaAAgTgPg");
	this.shape_3.setTransform(230.475,52.15);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("Ag4BDQgVgTAAggIAAhlIA9AAIAABnQAAARAQABQARgBAAgRIAAhnIA9AAIAABlQgBAggTATQgVASglAAQgkAAgUgSg");
	this.shape_4.setTransform(215.15,52.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("Ag9A+QgYgYAAgmQAAgkAZgZQAZgZAnAAQAtAAAbAbIgiAlQgOgMgRgBQgOAAgJAKQgKALAAAOQAAAQAJALQAKAKAOgBQANAAAIgEIAAgGIgTAAIAAgoIBJAAIAABKQggAbgxAAQgpAAgZgZg");
	this.shape_5.setTransform(197.625,52.15);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ag5BDQgTgTAAggIAAhlIA9AAIAABnQAAARAPABQARgBAAgRIAAhnIA8AAIAABlQABAggUATQgVASglAAQgkAAgVgSg");
	this.shape_6.setTransform(180.35,52.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAaBUIgIgaIgmAAIgIAaIg7AAIA6inIA7AAIA6CngAAIAQIgIgiIgKAiIASAAg");
	this.shape_7.setTransform(162.975,52.15);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgPBUIAAhzIgPAEIgLgsIAmgMIAtAAIAACng");
	this.shape_8.setTransform(144.55,52.15);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgPBUIAAhzIgPAEIgLgsIAmgMIAtAAIAACng");
	this.shape_9.setTransform(134.9,52.15);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAsBUIAAhRIgRBEIg1AAIgQhEIAABRIg8AAIAAinIBNAAIAZBeIAbheIBMAAIAACng");
	this.shape_10.setTransform(114.575,52.15);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag7BnIBHjNIAwAAIhHDNg");
	this.shape_11.setTransform(97.8,52.85);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgdBUIAAh3IgiAAIAAgwIB+AAIAAAwIggAAIAAB3g");
	this.shape_12.setTransform(85.3,52.15);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgvBFQgOgMgEgTIA0gLQABAKAKABQAJAAAAgMQAAgFgDgDQgDgEgEAAQgHAAgDAHIgwgIIAEhiIBwAAIAAAuIg+AAIAAANQAGgDAIAAQAZAAARAPQARAPAAAZQAAAagTAQQgTARgcAAQgcABgTgRg");
	this.shape_13.setTransform(67.175,52.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AASBUIgkhPIAABPIg8AAIAAinIA+AAIAjBPIAAhPIA8AAIAACng");
	this.shape_14.setTransform(47.125,52.15);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAaBUIgIgaIgmAAIgIAaIg7AAIA6inIA7AAIA6CngAAIAQIgIgiIgKAiIASAAg");
	this.shape_15.setTransform(29.575,52.15);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgeBUIg9inIBCAAIAaBiIAbhiIBAAAIg9Cng");
	this.shape_16.setTransform(11.325,52.15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AhmChIAAlBIB0AAIAADmIBZAAIAABbg");
	this.shape_17.setTransform(690.225,20.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AhkChIAAlBIDIAAIAABaIhUAAIAAAbIBLAAIAABVIhLAAIAAAcIBUAAIAABbg");
	this.shape_18.setTransform(667.15,20.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AhkChIAAlBIDJAAIAABaIhVAAIAAAbIBMAAIAABVIhMAAIAAAcIBVAAIAABbg");
	this.shape_19.setTransform(644.3,20.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("Ag6ChIh1lBIB/AAIAyC7IAzi7IB7AAIh1FBg");
	this.shape_20.setTransform(615.025,20.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AhmChIAAlBIB0AAIAADmIBZAAIAABbg");
	this.shape_21.setTransform(577.475,20.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AhkChIAAlBIDJAAIAABaIhVAAIAAAbIBMAAIAABVIhMAAIAAAcIBVAAIAABbg");
	this.shape_22.setTransform(554.4,20.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AhkChIAAlBIDJAAIAABaIhVAAIAAAbIBMAAIAABVIhMAAIAAAcIBVAAIAABbg");
	this.shape_23.setTransform(531.55,20.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAiChIAAh0IhDAAIAAB0Ih1AAIAAlBIB1AAIAAB0IBDAAIAAh0IB1AAIAAFBg");
	this.shape_24.setTransform(503.7,20.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("Ah+ChIAAlBICHAAQA2AAAhAfQAfAeAAAwQAAAyggAdQghAgg0AAIgYAAIAABlgAgOgYIALAAQAJAAAHgGQAHgHABgLQgBgLgGgHQgGgGgKAAIgMAAg");
	this.shape_25.setTransform(465.4,20.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AiBB2QgwguAAhIQAAhHAwguQAxgxBQAAQBRAAAxAxQAwAuAABHQAABIgwAuQgxAxhRAAQhQAAgxgxgAgpgyQgOATAAAfQAAAfAOAUQAPAUAaAAQAaAAAQgUQAOgUAAgfQAAgfgOgTQgQgUgaAAQgaAAgPAUg");
	this.shape_26.setTransform(433.225,20.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E90000").s().p("Ah1B3QgwguABhJQgBhGAxgvQAxgxBLAAQBXAAAyA0IhABGQgbgXghAAQgbAAgSATQgTATAAAdQAAAfASAUQASATAcAAQAaAAAOgJIAAgMIgkAAIAAhNICMAAIAACOQg8A1hgAAQhOAAgwgwg");
	this.shape_27.setTransform(388.75,20.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E90000").s().p("AAhChIhFiYIAACYIhzAAIAAlBIB3AAIBECXIAAiXIB0AAIAAFBg");
	this.shape_28.setTransform(355.3,20.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E90000").s().p("Ag5ChIAAlBIB0AAIAAFBg");
	this.shape_29.setTransform(331.55,20.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E90000").s().p("Ag5ChIAAjnIg/AAIAAhaIDyAAIAABaIhAAAIAADng");
	this.shape_30.setTransform(311.55,20.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E90000").s().p("AAQChIgshoIAABoIhyAAIAAlBICPAAQA3AAAhAeQAfAdAAAuQAAAagPAXQgOAUgWAMIBKCHgAgcgcIAOAAQAKAAAFgHQAGgGAAgJQAAgJgGgGQgFgIgKABIgOAAg");
	this.shape_31.setTransform(285.275,20.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E90000").s().p("AiBB2QgwguAAhIQAAhHAwguQAxgxBQAAQBRAAAxAxQAwAuAABHQAABIgwAuQgxAxhRAAQhQAAgxgxgAgpgyQgOATAAAfQAAAfAOAUQAPAUAaAAQAaAAAQgUQAOgUAAgfQAAgfgOgTQgQgUgaAAQgaAAgPAUg");
	this.shape_32.setTransform(251.525,20.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E90000").s().p("AAWChIg3iFIAACFIh1AAIAAlBIB1AAIAAB1IA2h1ICAAAIhQCZIBSCog");
	this.shape_33.setTransform(217.7,20.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E90000").s().p("AArCPQgZgYAAgjQAAgjAZgYQAagYAnAAQAnAAAaAYQAZAYAAAjQAAAjgZAYQgaAYgnAAQgnAAgagYgABhBUQAAAQALAAQALAAAAgQQAAgQgLAAQgLAAAAAQgAiiChIDslBIBZAAIjsFBgAisgZQgZgXAAgjQAAgjAZgYQAagYAnAAQAnAAAaAYQAZAYAAAjQAAAjgZAXQgaAYgnAAQgnAAgagYgAh2hTQAAAQALAAQALAAAAgQQAAgQgLAAQgLAAAAAQg");
	this.shape_34.setTransform(171.775,20.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E90000").s().p("AhaCEQgbgYgJgjIBjgWQADAUATAAQATABAAgXQAAgKgGgGQgGgHgIAAQgNAAgFANIhdgNIAHi9IDYAAIAABXIh3AAIgBAZQAMgEAQgBQAwAAAgAdQAhAeAAAuQAAAygkAgQglAhg2AAQg3AAgjggg");
	this.shape_35.setTransform(138.15,20.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E90000").s().p("Ah4CkIAAgoQAAggAUgdQAPgVAegZIAsgmQATgUAAgRQAAgOgNAAQgOAAgHAcIhigUQAIgrAbgZQAhgfA3AAQA3AAAhAbQAkAbAAAwQAAAyg1ApIglAbQgQANAAAHIBsAAIAABXg");
	this.shape_36.setTransform(111.775,20.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#E90000").s().p("Ag6ChIAAjnIg/AAIAAhaIDyAAIAABaIg/AAIAADng");
	this.shape_37.setTransform(76.8,20.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#E90000").s().p("AiBB2QgwguAAhIQAAhHAwguQAxgxBQAAQBRAAAxAxQAwAuAABHQAABIgwAuQgxAxhRAAQhQAAgxgxgAgpgyQgOATAAAfQAAAfAOAUQAPAUAaAAQAaAAAQgUQAOgUAAgfQAAgfgOgTQgQgUgaAAQgaAAgPAUg");
	this.shape_38.setTransform(45.825,20.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#E90000").s().p("Ag6ChIAAjnIg+AAIAAhaIDyAAIAABaIhAAAIAADng");
	this.shape_39.setTransform(14.85,20.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,703.3,68.6), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape.setTransform(236.725,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_1.setTransform(220.575,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_2.setTransform(204.1,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_3.setTransform(186.525,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgiBiIAAjDIBFAAIAADDg");
	this.shape_4.setTransform(171.9,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AhCBNQgXgVAAglIAAh2IBHAAIAAB4QAAAUASAAQATAAAAgUIAAh4IBHAAIAAB2QAAAlgXAVQgYAXgrAAQgqAAgYgXg");
	this.shape_5.setTransform(157.625,13.375);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhTBiIAAgkIBKhpIhIAAIAAg2ICkAAIAAAkIhLBoIBMAAIAAA3g");
	this.shape_6.setTransform(139.025,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAXIAuAAIAAAzIguAAIAABCg");
	this.shape_7.setTransform(123.775,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhOBIQgdgdgBgrQABgrAdgcQAegeAwABQAxgBAfAeQAcAcAAArQAAArgcAdQgfAdgxAAQgwAAgegdgAgZgeQgIAMAAASQAAAUAIALQAKAMAPAAQAQAAAKgMQAJgLAAgUQAAgSgJgMQgKgMgQAAQgPAAgKAMg");
	this.shape_8.setTransform(105.8,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_9.setTransform(86.975,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_10.setTransform(71.025,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_11.setTransform(46.975,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_12.setTransform(29.75,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_13.setTransform(12.425,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,247.1,32), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape.setTransform(375.875,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_1.setTransform(359.725,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_2.setTransform(343.25,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_3.setTransform(325.675,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AhOBIQgdgdgBgrQABgrAdgcQAegeAwABQAxgBAeAeQAdAcAAArQAAArgdAdQgeAdgxAAQgwAAgegdgAgZgeQgIAMAAASQAAAUAIALQAKAMAPAAQAQAAAJgMQAKgLgBgUQABgSgKgMQgJgMgQAAQgPAAgKAMg");
	this.shape_4.setTransform(304.35,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_5.setTransform(285.175,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhZBiIAAjDIBGAAQAxAAAdAaQAfAbAAAsQAAAtgeAaQgeAbgxAAgAgUAqIAEAAQAhAAAAgqQAAgpghAAIgEAAg");
	this.shape_6.setTransform(266.475,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_7.setTransform(241.175,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_8.setTransform(224.15,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_9.setTransform(203.675,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_10.setTransform(188.7,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_11.setTransform(171.675,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgiBiIAAjDIBGAAIAADDg");
	this.shape_12.setTransform(157.25,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAVBiIAAhHIgpAAIAABHIhHAAIAAjDIBHAAIAABGIApAAIAAhGIBHAAIAADDg");
	this.shape_13.setTransform(142.875,13.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("Ag3BJQgegcAAgtQAAgqAdgdQAegeAtABQAnAAAbASIgVA4QgQgOgVAAQgRgBgKAMQgLAMAAARQAAATALALQALAMAQAAQAVAAARgQIAVA4QgbAUgogBQgtAAgdgcg");
	this.shape_14.setTransform(123.775,13.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_15.setTransform(104.575,13.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAzBiIAAheIgUBPIg9AAIgUhPIAABeIhFAAIAAjDIBZAAIAeBvIAfhvIBZAAIAADDg");
	this.shape_16.setTransform(81.375,13.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_17.setTransform(60.425,13.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_18.setTransform(42.025,13.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AAYBiIgYhmIgWBmIhIAAIgzjDIBMAAIASBrIAYhrIA3AAIAYBrIAThrIBLAAIgyDDg");
	this.shape_19.setTransform(16.8,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,386.2,32), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape.setTransform(201.725,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AggAdQAOgJAHgNQgXgIAAgWQAAgOAKgJQAJgKAPAAQAPAAAKAKQAKAJAAARQAAAVgMAVQgLAVgTAOg");
	this.shape_1.setTransform(189,7.725);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgjBiIhHjDIBNAAIAeByIAfhyIBLAAIhHDDg");
	this.shape_2.setTransform(173.4,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_3.setTransform(154.725,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_4.setTransform(130.875,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_5.setTransform(110.525,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhCBNQgXgVAAglIAAh2IBHAAIAAB4QAAAUASAAQATAAAAgUIAAh4IBHAAIAAB2QAAAlgXAVQgYAXgrAAQgqAAgYgXg");
	this.shape_6.setTransform(90.575,13.375);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_7.setTransform(72.575,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAzBiIAAheIgUBPIg9AAIgUhPIAABeIhFAAIAAjDIBZAAIAeBvIAfhvIBZAAIAADDg");
	this.shape_8.setTransform(51.775,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_9.setTransform(28.575,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_10.setTransform(10.025,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,217.5,32), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape.setTransform(243.25,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhZBiIAAjDIBGAAQAxAAAdAaQAfAbAAAsQAAAtgeAaQgeAbgxAAgAgUAqIAEAAQAhAAAAgqQAAgpghAAIgEAAg");
	this.shape_1.setTransform(227.125,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_2.setTransform(209.175,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_3.setTransform(192.7,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_4.setTransform(178.8,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_5.setTransform(164.075,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ag3BJQgegcAAgtQAAgqAdgdQAegeAtABQAnAAAbASIgVA4QgQgOgVAAQgRgBgKAMQgLAMAAARQAAATALALQALAMAQAAQAVAAARgQIAVA4QgbAUgogBQgtAAgdgcg");
	this.shape_6.setTransform(147.175,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_7.setTransform(131.4,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag9BiIAAjDIBFAAIAACMIA2AAIAAA3g");
	this.shape_8.setTransform(117.9,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_9.setTransform(103.85,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_10.setTransform(88.775,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_11.setTransform(73.8,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_12.setTransform(56.225,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_13.setTransform(32.675,13.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhOBIQgegdAAgrQAAgrAegcQAegeAwABQAxgBAeAeQAdAcABArQgBArgdAdQgdAdgyAAQgwAAgegdgAgYgeQgJAMAAASQAAAUAJALQAJAMAPAAQAQAAAJgMQAJgLAAgUQAAgSgJgMQgJgMgQAAQgPAAgJAMg");
	this.shape_14.setTransform(13.05,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,257.8,32), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape.setTransform(243.25,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhZBiIAAjDIBGAAQAxAAAdAaQAfAbAAAsQAAAtgeAaQgeAbgxAAgAgUAqIAEAAQAhAAAAgqQAAgpghAAIgEAAg");
	this.shape_1.setTransform(227.125,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_2.setTransform(209.175,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_3.setTransform(192.7,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_4.setTransform(178.8,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_5.setTransform(164.075,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ag3BJQgegcAAgtQAAgqAdgdQAegeAtABQAnAAAbASIgVA4QgQgOgVAAQgRgBgKAMQgLAMAAARQAAATALALQALAMAQAAQAVAAARgQIAVA4QgbAUgogBQgtAAgdgcg");
	this.shape_6.setTransform(147.175,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_7.setTransform(131.4,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag9BiIAAjDIBFAAIAACMIA2AAIAAA3g");
	this.shape_8.setTransform(117.9,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_9.setTransform(103.85,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_10.setTransform(88.775,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_11.setTransform(73.8,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_12.setTransform(56.225,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_13.setTransform(32.675,13.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhOBIQgegdAAgrQAAgrAegcQAegeAwABQAxgBAeAeQAdAcABArQgBArgdAdQgdAdgyAAQgwAAgegdgAgYgeQgJAMAAASQAAAUAJALQAJAMAPAAQAQAAAJgMQAJgLAAgUQAAgSgJgMQgJgMgQAAQgPAAgJAMg");
	this.shape_14.setTransform(13.05,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,257.8,32), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape.setTransform(324.45,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag9BiIAAjDIBFAAIAACMIA2AAIAAA3g");
	this.shape_1.setTransform(310.95,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_2.setTransform(296.9,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgiBiIAAjDIBGAAIAADDg");
	this.shape_3.setTransform(285.6,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAzBiIAAheIgUBPIg9AAIgUhPIAABeIhFAAIAAjDIBZAAIAeBvIAfhvIBZAAIAADDg");
	this.shape_4.setTransform(268.425,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_5.setTransform(243.25,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhZBiIAAjDIBGAAQAxAAAdAaQAfAbAAAsQAAAtgeAaQgeAbgxAAgAgUAqIAEAAQAhAAAAgqQAAgpghAAIgEAAg");
	this.shape_6.setTransform(227.125,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_7.setTransform(209.175,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_8.setTransform(192.7,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_9.setTransform(178.8,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_10.setTransform(164.075,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag3BJQgegcAAgtQAAgqAdgdQAegeAtABQAnAAAbASIgVA4QgQgOgVAAQgRgBgKAMQgLAMAAARQAAATALALQALAMAQAAQAVAAARgQIAVA4QgbAUgogBQgtAAgdgcg");
	this.shape_11.setTransform(147.175,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_12.setTransform(131.4,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("Ag9BiIAAjDIBFAAIAACMIA2AAIAAA3g");
	this.shape_13.setTransform(117.9,13.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_14.setTransform(103.85,13.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_15.setTransform(88.775,13.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_16.setTransform(73.8,13.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_17.setTransform(56.225,13.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_18.setTransform(32.675,13.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AhOBIQgegdAAgrQAAgrAegcQAegeAwABQAxgBAeAeQAdAcABArQgBArgdAdQgdAdgyAAQgwAAgegdgAgYgeQgJAMAAASQAAAUAJALQAJAMAPAAQAQAAAJgMQAJgLAAgUQAAgSgJgMQgJgMgQAAQgPAAgJAMg");
	this.shape_19.setTransform(13.05,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,339,32), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape.setTransform(243.25,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhZBiIAAjDIBGAAQAxAAAdAaQAfAbAAAsQAAAtgeAaQgeAbgxAAgAgUAqIAEAAQAhAAAAgqQAAgpghAAIgEAAg");
	this.shape_1.setTransform(227.125,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_2.setTransform(209.175,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_3.setTransform(192.7,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_4.setTransform(178.8,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_5.setTransform(164.075,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ag3BJQgegcAAgtQAAgqAdgdQAegeAtABQAnAAAbASIgVA4QgQgOgVAAQgRgBgKAMQgLAMAAARQAAATALALQALAMAQAAQAVAAARgQIAVA4QgbAUgogBQgtAAgdgcg");
	this.shape_6.setTransform(147.175,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_7.setTransform(131.4,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag9BiIAAjDIBFAAIAACMIA2AAIAAA3g");
	this.shape_8.setTransform(117.9,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_9.setTransform(103.85,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_10.setTransform(88.775,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_11.setTransform(73.8,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AhHBIQgdgcAAgsQAAgqAegdQAdgeAuABQA1AAAfAfIgoArQgQgOgUAAQgQgBgLAMQgLAMAAARQAAATAKAMQALALARABQAQAAAIgGIAAgIIgWAAIAAguIBWAAIAABXQglAfg6AAQgwAAgdgdg");
	this.shape_12.setTransform(56.225,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_13.setTransform(32.675,13.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhOBIQgegdAAgrQAAgrAegcQAegeAwABQAxgBAeAeQAdAcABArQgBArgdAdQgdAdgyAAQgwAAgegdgAgYgeQgJAMAAASQAAAUAJALQAJAMAPAAQAQAAAJgMQAJgLAAgUQAAgSgJgMQgJgMgQAAQgPAAgJAMg");
	this.shape_14.setTransform(13.05,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,252.4,32), null);


(lib.shape3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhjC5IAAlxIDHAAIAAFxg");
	this.shape.setTransform(-74.5013,24.0083,12.5499,0.8647);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-72.5,0,72.5,0).s().p("ArUDIIAAmPIWpAAIAAGPg");
	this.shape_1.setTransform(97.5051,24.0161,0.6552,0.7999);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape3, new cjs.Rectangle(-200,8,345,32), null);


(lib.shape2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjHLuIAA3bIGPAAIAAXbg");
	this.shape.setTransform(20,124.9945,1,1.6666);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape2, new cjs.Rectangle(0,0,40,250), null);


(lib.shape1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");
	this.shape.setTransform(485.0006,125,3.2333,1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape1, new cjs.Rectangle(0,0,970,250), null);


(lib.shape_grey = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E6E7E8").s().p("A0TLuIAA3bMAonAAAIAAXbg");
	this.shape.setTransform(464.9897,84.9997,3.5769,1.1333);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("A0TLuIAA3bMAonAAAIAAXbg");
	this.shape_1.setTransform(485.0059,64.9951,3.7308,1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-40,970,210);


(lib.services_button = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "12px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.textAlign = "center";
	this.dynText.lineHeight = 12;
	this.dynText.lineWidth = 86;
	this.dynText.parent = this;
	this.dynText.setTransform(49.95,7);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.services_button, new cjs.Rectangle(5,5,90,23.9), null);


(lib.price_old_line = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E31E26").s().p("AjxAyIAAhjIHjAAIAABjg");
	this.shape.setTransform(46.0198,1,1.9008,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AjxAyIAAhjIHjAAIAABjg");
	this.shape_1.setTransform(46.0198,1,1.9008,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,92,2);


(lib.mo2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape.setTransform(202.475,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_1.setTransform(186.325,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_2.setTransform(166.425,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhQBiIAAjDIBUAAQAhAAAVAPQASAPABAZQgBAZgTAOQAKAFAHALQAHALgBAMQABAegVAQQgTAQggAAgAgMAtIAMAAQAFAAAEgDQAFgFAAgFQAAgGgFgDQgEgEgFAAIgMAAgAgMgXIAMAAQALAAAAgLQAAgLgLAAIgMAAg");
	this.shape_3.setTransform(147.5,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AhZBiIAAjDIBGAAQAxAAAdAaQAfAbAAAsQAAAtgeAaQgeAbgxAAgAgUAqIAEAAQAhAAAAgqQAAgpghAAIgEAAg");
	this.shape_4.setTransform(129.425,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_5.setTransform(109.525,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhCBNQgXgVAAglIAAh2IBHAAIAAB4QAAAUASAAQATAAAAgUIAAh4IBHAAIAAB2QAAAlgXAVQgYAXgrAAQgqAAgYgXg");
	this.shape_6.setTransform(89.575,13.375);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhOBIQgdgdgBgrQABgrAdgcQAegeAwABQAxgBAeAeQAdAcAAArQAAArgdAdQgeAdgxAAQgwAAgegdgAgZgeQgIAMAAASQAAAUAIALQAKAMAPAAQAQAAAJgMQAKgLgBgUQABgSgKgMQgJgMgQAAQgPAAgKAMg");
	this.shape_7.setTransform(68.65,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_8.setTransform(49.475,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_9.setTransform(25.975,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_10.setTransform(8.95,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mo2, new cjs.Rectangle(0,0,212.8,32), null);


(lib.merken_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AA5GrIiTlgIAAFgIk3AAIAAtVIE3AAIAAE2ICRk2IFSAAIjTGXIDbG+g");
	this.shape.setTransform(779.15,50.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AkJGrIAAtVIIUAAIAADwIjhAAIAABHIDIAAIAADhIjIAAIAABNIDhAAIAADwg");
	this.shape_1.setTransform(705.05,50.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AkKGrIAAtVIIVAAIAADwIjgAAIAABHIDHAAIAADhIjHAAIAABNIDgAAIAADwg");
	this.shape_2.setTransform(644.45,50.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("ABoGrIhom/IhnG/Ik3AAIjetVIFLAAIBPHVIBpnVIDzAAIBpHVIBPnVIFLAAIjeNVg");
	this.shape_3.setTransform(549.625,50.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("ABaGrIi6mRIAAGRIkyAAIAAtVIE7AAIC3GRIAAmRIEyAAIAANVg");
	this.shape_4.setTransform(441.2,50.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AkJGrIAAtVIIUAAIAADwIjgAAIAABHIDHAAIAADhIjHAAIAABNIDgAAIAADwg");
	this.shape_5.setTransform(367,50.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AA5GrIiTlgIAAFgIk3AAIAAtVIE3AAIAAE2ICQk2IFTAAIjUGXIDdG+g");
	this.shape_6.setTransform(295.25,50.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AArGrIh4kUIAAEUIksAAIAAtVIF6AAQCTAABXBQQBUBNAAB4QAABGgqA+QgkA2g6AhIDDFlgAhNhLIAlAAQAbAAAQgTQARgQAAgYQAAgYgRgSQgQgSgbAAIglAAg");
	this.shape_7.setTransform(215.825,50.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AkKGrIAAtVIIVAAIAADwIjgAAIAABHIDHAAIAADhIjHAAIAABNIDgAAIAADwg");
	this.shape_8.setTransform(144.05,50.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("ADdGrIAAmbIhVFaIkPAAIhVlaIAAGbIktAAIAAtVIGEAAICFHlICGnlIGEAAIAANVg");
	this.shape_9.setTransform(57.875,50.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.merken_text, new cjs.Rectangle(0,0,822.8,126), null);


(lib.mediamarkt_logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EB1D24").s().p("AgzA2QgWgWAAggQAAgeAWgWQAVgXAeABQAfgBAVAXQAWAWAAAeQAAAfgWAXQgVAWgfgBQgeABgVgWgAgqgrQgSATAAAYQAAAaASASQASASAYAAQAZAAASgSQASgSAAgaQAAgZgSgSQgSgRgZgBQgYABgSARgAASAoIgNggIgSAAIAAAgIgRAAIAAhQIAkAAQAeAAAAAYQAAAPgPAHIAPAigAgNgFIAQAAQAPAAAAgLQAAgLgNABIgSAAg");
	this.shape.setTransform(392.5029,-32.1916,0.9958,0.9957);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AhIBeQgKAAgGgIQgHgIACgNIAaiMQABgHAGgGQAGgFAHAAIB4AAQAKAAAGAIQAGAHgBAKIgaCNQgCAIgFAHQgHAGgHAAg");
	this.shape_1.setTransform(-100.5552,-29.8518,0.9958,0.9957);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EB1D24").s().p("AN5IUQhHgPg7g+Qg6g/gOhMQgHgnABgqQgHAGgIAAIn0AAIgBAGQAAAHAHAXIACAHQARAsAoAaQAnAaAxgBQBhgCBQhbQAGgHAJAAIBwAAQALAAAGAKQAHAKgFAKQgwBmhhA+QhiA+hwAAQhNgDg/gkQg+glgkhAQglhFAChcIgxEOQgBAGgFADQgEAEgFAAIiDAAQgHAAgFgHQgDgDAAgGIAAgEQAgiGApj/QAskNAKgrQhEBQnLJ6QgFAGgHABIiGAAQgHAAgFgHQgEgEAAgGIAAgDQAeiMArj6IA2k2QhBBPooJ7QgFAFgIABIivAAQgJAAgFgLQgEgKAHgHINmv6QAFgGAHAAICJgBQAIAAAFAGQADAGAAAFIAAADQheIhgUBjQBFhPG3pDQAFgFAHAAICMAAQAIAAAEAGQAFAGgCAIIhnI8IADgHQAxhfBehCQBchABngNIACAAQBcgFBGAjQBGAjArBHIAAACQAjBGAEBUQAkhpBWhQQBIhFBXgaQBWgaBYAUIAEABIALAFQA3AYAYAWIAyj1QACgIAGgEQAFgFAIAAIBsAAQAKAAAHAIQAGAHgCAKIieNlQgDAPgSgBIhsABQgMAAgEgFQgFgFAAgIIAFgdQhpBDhkAAQgcAAgbgFgAQBgZQgqAIghAXQg6ArgfA4QgfA3AAA7QAAAXADAPQAGAeAQAZQAQAaAYARQAzAiBXgSQA8gOA0g2QA0g2ARhBQAGgXAAgZQACg0gXglQgjg5g6gOQgVgFgWAAQgTAAgTAEgAIZBTQgEglgkglQgYgUgXgIQgegKgtAEQgwAGgrAbQgvAegXAtIFDAAIAAAAg");
	this.shape_2.setTransform(-255.3734,-0.0609,0.9958,0.9957);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EB1D24").s().p("ADVHHIgKgEQgEAEgDAAIiCAAQgMAAgGgJQh+jWgrhDIgeCrIgVBpQgFAOgPAAIh1AAQgJAAgHgHQgGgIACgJIBYoWIgJABIgIABQh4ASggB5IhPGQQgCAIgFAFQgGAEgIAAIhoAAQgKAAgGgHQgHgHACgKIB3qVQABgHAGgEQAGgFAIAAIBsAAQALAAAFAHQAHAIgCAJIgDAQQAggUAkgNQAogOAkgBQAKAAAHAJIAAAAQAQhhAUheQABgIAGgFQAFgFAIAAIB3AAQAKAAAGAIQAFAGAAAHIAAAEQg0EXgRBkQA6hABhh9QAFgHAKAAICLAAQALAAAGAJQAGgJAMAAIBDAAIAji+QABgIAGgFQAGgFAIAAIB5AAQAJAAAHAIQAFAGAAAIIgjC6IB7AAQAIAAAIAHQAEAGAAAIIAAAEIgSBrQgBAIgGAFQgGAFgIAAIiDAAIhhIWQgBAHgGAFQgGAFgHAAgAAPBEIDGFJQAKhAAnjTIAojaIg7AAQgKAAgGgIQgGgHABgLIAQhSIjfEQg");
	this.shape_3.setTransform(329.7063,6.0663,0.9958,0.9957);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EB1D24").s().p("Aq8IDQBKgdBFguQAjgXAUgRQB6hrAKiQQALiNhmhHQAJALAFAOQBOBgguCEQgvCHiQBHQgXALgnANQhNAahNAJQg0gigtgwQBLALBOgFQAmgCAYgEQCegfBQh3QBOh1g0hxIAAAGQAAAHgBAIQAYB9hqBeQhrBfiigMQgYgCgmgIQhMgQhGgdQggg8gRhDIgCgLQA8AvBFAkQAjASAWAIQCYA1B/g9QB8g8ANh8QgGAOgIAIQgsB1iIAZQiKAZiEhbQgXgSghgdQhAg6gsg0IhfIIQgDAPgSgBIhrAAQgMAAgFgGQgFgFAAgGIAFgdQiFBVh/gXQhHgPg7g+Qg6g/gNhMQgDgQgDgjIgCghIgwEOQgDAPgSgBIh4ABQgMAAgEgFQgGgHABgKIB4qWQABgIAGgFQAGgFAIAAIB4AAQAKAAAGAHQAGAIgBAKIgyENIACgFQAlhrBUhOQBJhFBWgaQBXgbBYAWIAPAFQA2AYAYAWQADgQAEgNIADgJQAIgRAPAAIBtAAQAKAAAHAGQAEAFAEAKQAYBFAtBLQAXAlASAYQBrB+CQAJQCNAJBFhtQgIAGgHAEIgEACQheBZiHgtQiJguhJiWQgKgWgMgmQgYhLgKhMQAmhBA2g0QgKBNAEBRQACAoAEAZQAfCiB3BQQB0BPBxg4IgQABIgKAAQh7AYhchrQhehtAMikQACgZAIgoQAQhPAehIQA6ggA9gQQguA9gjBGIgaA7Qg1CdBACCQA/CAB+AHIgRgKIgBgBQh7gmgdiMQgdiQBciLIAogzQA1g8A9gvQBNABBIAWQhJAdhEAtIg2AnQh8BtgJCSQgICPBqBFQgJgKgFgLQhVheAtiIQAuiLCThJIA+gYQBOgZBPgKQA7AlAwA0QhOgMhRAFIhDAGQigAghPB5QhPB3A5ByIgBgOIAAgJQgah/BqheQBshhCjAMIBCALQBRARBJAgQAgA9APA+IAEAUQg+gzhLgnIg+gdQiXg1h/A9Qh9A8gNB8QAHgOAJgLQAthzCHgYQCKgZCDBbIA1ArQA9A5AvBBQgCBBgTBAQgdhKgshFIgmg2Qhph6iNgMQiJgLhIBlQANgHAHgCQBfhSCDAvQCGAvBHCSIAfBUQAgBjAIBOILYtXQAGgHAGAAICZABQAHAAAFAFQADAEAAAIIAAADQhaIjgZBhQBEhOG0pFQAGgFAGAAICRgBQAHAAAFAGQAFAGgCAJIhsJMQAlhWBIhEQBJhFBWgaQBWgaBZAUIAEABIALAFQAyAUAgAeIAJgqQABgIAGgEQAGgEAHAAIBtAAQAKAAAGAGQAFAGAAAJIh5KZQgCAPgTAAIhrAAQgNAAgEgFQgEgGAAgHIAAgEIAEgZQiFBVh/gXQhHgPg7g+Qg6g/gNhMQgDgPADgZQAEgiAAgHIAAgSIgwEfQgEANgMAAIiEAAQgHAAgFgHQgEgFAAgFIABgDQAYh6AskBQApjvANg+QgqAyj1FGIjsE+QgEAFgHABIiHAAQgHAAgFgGQgDgEAAgGIAAgEQAaiHAqj1IA1ktQhBBPo0JoQgFAGgIAAIirAAQgKAAgEgKQgEgIAFgIIAAAAIAAgBIAQgaQASgjAHgjQALhMgIhRIgKhCQgeifhzhQQhxhQhwAyIAFAAQALAAAJACQB5gUBZBrQBbBsgLCiIgLBDQgRBRgfBKQg5Adg6APIgTAFQAyhAAnhLQATgmAJgZQA1idhAiDQg/h/h/gHQALAEAJAHQB6AmAdCMQAdCQhcCLQgPAVgcAgQg3A+hBAxQhIgEhHgYgAZHgaQgqAIghAXQg5ArgeA3QgfA3AAA8QAAATADATQASBDAsAeQAxAhBWgPQA7gOA1g3QA0g2AUhBQAGgUAAgeQAAgygVgmQgeg3hBgPQgVgFgVAAQgTAAgUAEgA2hgaQgqAIghAXQg5ArgeA3QgfA3AAA8QAAAXAEAPQATBEAqAdQAXAQAlAEQAmAEAsgIQA6gPAzg1QAxg1AThBQAHgWAAgcQAAgxgWgnQgeg3hBgPQgVgFgVAAQgTAAgUAEg");
	this.shape_4.setTransform(75.8329,0.039,0.9958,0.9957);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mediamarkt_logo, new cjs.Rectangle(-400,-54,799.9,108.1), null);


(lib.m32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape.setTransform(331.025,13.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_1.setTransform(314.875,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_2.setTransform(298.4,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_3.setTransform(283.675,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_4.setTransform(265.775,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgjBiIAAjDIBGAAIAADDg");
	this.shape_5.setTransform(251.35,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_6.setTransform(238.825,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_7.setTransform(221.425,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_8.setTransform(197.375,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAuAAIAAA0IguAAIAAARIAzAAIAAA3g");
	this.shape_9.setTransform(180.35,13.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_10.setTransform(157.925,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_11.setTransform(140.9,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAKBiIgbhAIAABAIhFAAIAAjDIBWAAQAiAAAUASQATASAAAbQAAARgJAOQgJAMgNAHIAtBSgAgRgRIAIAAQAHABADgFQADgDAAgGQAAgFgDgEQgDgFgHAAIgIAAg");
	this.shape_12.setTransform(125.825,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AhOBIQgegdABgrQgBgrAegcQAegeAwABQAxgBAfAeQAdAcgBArQABArgdAdQgeAdgyAAQgwAAgegdgAgZgeQgIAMAAASQAAAUAIALQAKAMAPAAQAQAAAKgMQAIgLABgUQgBgSgIgMQgKgMgQAAQgPAAgKAMg");
	this.shape_13.setTransform(105.25,13.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_14.setTransform(86.425,13.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgiBiIAAjDIBFAAIAADDg");
	this.shape_15.setTransform(74.25,13.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAVBiIgqhcIAABcIhGAAIAAjDIBIAAIApBcIAAhcIBGAAIAADDg");
	this.shape_16.setTransform(59.775,13.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AhOBIQgegdABgrQgBgrAegcQAegeAwABQAxgBAfAeQAcAcABArQgBArgcAdQgeAdgyAAQgwAAgegdgAgZgeQgIAMAAASQAAAUAIALQAKAMAPAAQAQAAAKgMQAIgLABgUQgBgSgIgMQgKgMgQAAQgPAAgKAMg");
	this.shape_17.setTransform(38.65,13.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAzBiIAAheIgUBPIg9AAIgUhPIAABeIhFAAIAAjDIBZAAIAeBvIAfhvIBZAAIAADDg");
	this.shape_18.setTransform(14.825,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m32, new cjs.Rectangle(0,0,341.4,32), null);


(lib.m31 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AghAdQAPgJAHgNQgXgIAAgWQAAgOAKgJQAJgKAPAAQAPAAAKAKQAKAJAAARQAAAVgLAVQgMAVgTAOg");
	this.shape.setTransform(320.6,22.525);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_1.setTransform(308.725,13.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_2.setTransform(292.825,13.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhOBIQgdgdAAgrQAAgrAdgcQAegeAwABQAxgBAfAeQAcAcAAArQAAArgcAdQgfAdgxAAQgwAAgegdgAgZgeQgIAMAAASQAAAUAIALQAKAMAPAAQAQAAAKgMQAJgLAAgUQAAgSgJgMQgKgMgQAAQgPAAgKAMg");
	this.shape_3.setTransform(273.2,13.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_4.setTransform(254.375,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AANBiIghhRIAABRIhHAAIAAjDIBHAAIAABHIAhhHIBNAAIgxBeIAzBlg");
	this.shape_5.setTransform(237.05,13.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_6.setTransform(218.875,13.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag8BiIAAjDIB5AAIAAA3IgzAAIAAAQIAtAAIAAA0IgtAAIAAARIAzAAIAAA3g");
	this.shape_7.setTransform(203.9,13.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhZBiIAAjDIBGAAQAxAAAdAaQAfAbAAAsQAAAtgeAaQgeAbgxAAgAgUAqIAEAAQAhAAAAgqQAAgpghAAIgEAAg");
	this.shape_8.setTransform(187.775,13.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AghAdQAQgJAGgNQgXgIAAgWQAAgOAKgJQAJgKAPAAQAQAAAJAKQAKAJAAARQAAAVgLAVQgLAVgVAOg");
	this.shape_9.setTransform(168.8,22.525);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag0BUQgRgOgHgTIAygcQANAVANAAQAFAAAAgFQAAgFgLgHIgagMQgmgTAAgiQAAgcAUgRQAUgRAgAAQAeAAAXAQQARAOAFAQIgyAcQgIgSgNAAQgGABAAAEQAAAGAMAGIAaAOQAnATAAAgQAAAegVARQgVAQgjAAQgeAAgWgRg");
	this.shape_10.setTransform(156.925,13.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_11.setTransform(141.025,13.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AhOBIQgegdAAgrQAAgrAegcQAegeAwABQAxgBAeAeQAdAcAAArQAAArgdAdQgdAdgyAAQgwAAgegdgAgYgeQgJAMAAASQAAAUAJALQAJAMAPAAQAQAAAJgMQAKgLgBgUQABgSgKgMQgJgMgQAAQgPAAgJAMg");
	this.shape_12.setTransform(121.4,13.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgjBiIAAiMIgmAAIAAg3ICTAAIAAA3IgnAAIAACMg");
	this.shape_13.setTransform(102.575,13.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_14.setTransform(86.875,13.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAeBiIgJgeIgtAAIgIAeIhGAAIBEjDIBFAAIBEDDgAAJASIgKgnIgLAnIAVAAg");
	this.shape_15.setTransform(67.925,13.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("Ag9BiIAAjDIBGAAIAACMIA2AAIAAA3g");
	this.shape_16.setTransform(50.95,13.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AhMBiIAAjDIBRAAQAhAAAUATQATASAAAeQAAAegTARQgUAUgggBIgOAAIAAA+gAgIgOIAGAAQAGAAAEgEQAEgEAAgHQAAgGgEgEQgEgFgFAAIgHAAg");
	this.shape_17.setTransform(30.575,13.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAVBiIAAhHIgpAAIAABHIhHAAIAAjDIBHAAIAABGIApAAIAAhGIBHAAIAADDg");
	this.shape_18.setTransform(12.025,13.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m31, new cjs.Rectangle(0,0,332,32), null);


(lib.logobalk_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logobalk();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.logobalk_1, new cjs.Rectangle(0,0,1272,113), null);


(lib.label_service = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeAwQgJgIgEgLIAcgQQAIAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQAMgJASAAQASAAAMAJQAKAIADAJIgdAQQgEgKgIABQAAAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAADAGAEIAQAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape.setTransform(143.75,43.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_1.setTransform(135.175,43.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_2.setTransform(125.475,43.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_3.setTransform(113.425,43.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AALA4IAAgoIgWAAIAAAoIgpAAIAAhvIApAAIAAAoIAWAAIAAgoIAqAAIAABvg");
	this.shape_4.setTransform(101.4,43.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgLALgSAAIgIAAIAAAjgAgEgHIADAAQADgBADgCQACgCAAgEQAAgEgCgCQgDgDgCABIgEAAg");
	this.shape_5.setTransform(91.15,43.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_6.setTransform(81.825,43.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAGA4IgQgkIAAAkIgmAAIAAhvIAwAAQATAAAMALQAKAJABARQAAAIgGAIQgEAHgIAEIAaAvgAgKgJIAGAAQADAAABgDQACgCABgCQgBgEgCgCQgBgDgDABIgGAAg");
	this.shape_7.setTransform(72.7,43.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_8.setTransform(61.3,43.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAdA4IAAg2IgMAtIgiAAIgLgtIAAA2IgnAAIAAhvIAyAAIARA/IARg/IAzAAIAABvg");
	this.shape_9.setTransform(48.05,43.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgQQAGAMAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQASAAAMAJQAKAIADAJIgdAQQgEgKgHABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAADAGAEIAQAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape_10.setTransform(36.1,43.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_11.setTransform(22.725,43.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_12.setTransform(12.975,43.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgPQAGALAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQAAgEgGgDIgPgIQgWgKAAgTQAAgRALgJQANgKARAAQASAAAMAKQAKAHADAKIgdAPQgEgJgIgBQAAAAgBABQgBAAAAAAQgBABAAAAQAAABAAABQAAADAGADIAQAHQAWAMAAASQAAAQgMALQgMAJgUAAQgRAAgNgKg");
	this.shape_13.setTransform(218.8,29.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_14.setTransform(209.775,29.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAdIgaAAIAAAKIAdAAIAAAfg");
	this.shape_15.setTransform(201.325,29.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgiA4IAAhvIAnAAIAABQIAfAAIAAAfg");
	this.shape_16.setTransform(193.6,29.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgtA4IAAhvIAvAAQAUAAAKAJQAMAJAAANQAAAOgMAJQAGACAEAGQAEAHAAAHQAAAQgLAKQgMAJgSAAgAgGAaIAGAAQADAAACgCQACgDABgDQgBgDgCgCQgCgCgDAAIgGAAgAgGgMIAGAAQAHAAgBgHQABgGgHAAIgGAAg");
	this.shape_17.setTransform(184.7,29.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAGAKIgGgVIgHAVIANAAg");
	this.shape_18.setTransform(173.65,29.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_19.setTransform(163.275,29.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAMA4IgYg0IAAA0IgoAAIAAhvIApAAIAXA1IAAg1IApAAIAABvg");
	this.shape_20.setTransform(150.025,29.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAnBvgAAGAKIgGgVIgHAVIANAAg");
	this.shape_21.setTransform(138.3,29.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgTA4IgphvIAsAAIARBAIARhAIArAAIgpBvg");
	this.shape_22.setTransform(126.175,29.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAdIgaAAIAAAKIAdAAIAAAfg");
	this.shape_23.setTransform(112.925,29.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_24.setTransform(106.475,29.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_25.setTransform(99.525,29.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAmBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_26.setTransform(89.1,29.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKAKQALALAAAPQAAAKgEAIQgGAGgHAEIAZAvgAgJgJIAEAAQAEAAACgCQABgDABgDQgBgDgBgCQgCgDgEAAIgEAAg");
	this.shape_27.setTransform(78.5,29.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAnBvgAAGAKIgGgVIgHAVIANAAg");
	this.shape_28.setTransform(67.1,29.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgLALgTAAIgHAAIAAAjgAgEgIIAEAAQACABACgDQADgCAAgEQAAgEgCgCQgCgDgDAAIgEAAg");
	this.shape_29.setTransform(56.65,29.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAdIgaAAIAAAKIAdAAIAAAfg");
	this.shape_30.setTransform(47.775,29.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AAFA4IgPgkIAAAkIgmAAIAAhvIAwAAQATAAAMAKQALALgBAPQAAAKgEAIQgGAGgHAEIAZAvgAgKgJIAGAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgDgDAAIgGAAg");
	this.shape_31.setTransform(39.15,29.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgdA2IAEgfIAIABQAHAAAAgGIAAhKIAoAAIAABQQAAAhglgBQgLAAgLgCg");
	this.shape_32.setTransform(27.175,29.45);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_33.setTransform(21.475,29.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgtA4IAAhvIAvAAQAUAAALAJQALAJgBANQAAAOgLAJQAHACAEAGQADAHAAAHQAAAQgMAKQgKAJgTAAgAgGAaIAGAAQADAAADgCQABgDAAgDQAAgDgBgCQgDgCgDAAIgGAAgAgGgMIAGAAQAGAAABgHQgBgGgGAAIgGAAg");
	this.shape_34.setTransform(14.1,29.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E31E26").s().p("AAGA4IgQgkIAAAkIgmAAIAAhvIAwAAQATAAAMALQAKAJABAQQAAAKgGAHQgEAHgIAEIAaAvgAgKgJIAGAAQADAAABgDQACgCABgCQgBgEgCgCQgBgDgDABIgGAAg");
	this.shape_35.setTransform(212.2,15.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E31E26").s().p("AgsApQgRgQAAgZQAAgYARgQQARgRAbAAQAcAAARARQARAQAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_36.setTransform(200.475,15.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#E31E26").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_37.setTransform(189.725,15.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#E31E26").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgIgMAAQgJAAgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMAAAKgIIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_38.setTransform(180.075,15.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#E31E26").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_39.setTransform(171.025,15.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#E31E26").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_40.setTransform(162.625,15.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#E31E26").s().p("AgsApQgRgQAAgZQAAgYARgQQARgRAbAAQAcAAARARQARAQAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_41.setTransform(151.875,15.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#E31E26").s().p("AAGA4IgPgkIAAAkIgnAAIAAhvIAwAAQAUAAAKALQAMAJAAAQQgBAKgFAHQgEAHgIAEIAaAvgAgJgJIAEAAQAEAAABgDQACgCAAgCQAAgEgCgCQgBgDgEABIgEAAg");
	this.shape_42.setTransform(140.9,15.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#E31E26").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgHIADAAQADAAACgDQADgCAAgEQAAgEgCgCQgDgDgCABIgEAAg");
	this.shape_43.setTransform(130.95,15.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#E31E26").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_44.setTransform(120.325,15.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#E31E26").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_45.setTransform(110.575,15.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#E31E26").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_46.setTransform(102.625,15.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#E31E26").s().p("AAGA4IgPgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQAMAJAAAQQgBAKgEAHQgFAHgIAEIAZAvgAgJgJIAEAAQAEAAABgDQACgCAAgCQAAgEgCgCQgBgDgEABIgEAAg");
	this.shape_47.setTransform(94,15.4);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#E31E26").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgIgMAAQgJAAgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMAAAKgIIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_48.setTransform(83.375,15.4);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#E31E26").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQARAAANAJQAKAIADAJIgdARQgFgLgGABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMALQgMAJgUAAQgRAAgMgKg");
	this.shape_49.setTransform(73.7,15.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#E31E26").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPAMgLQAMgJARAAQARAAANAJQAKAIADAJIgcARQgFgLgHABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMALQgMAJgUAAQgRAAgMgKg");
	this.shape_50.setTransform(61.45,15.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#E31E26").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_51.setTransform(54.375,15.4);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#E31E26").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_52.setTransform(47.425,15.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#E31E26").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_53.setTransform(37,15.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#E31E26").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQATAAALALQALAJAAAQQAAAKgEAHQgFAHgIAEIAZAvgAgJgJIAEAAQAEAAACgDQABgCAAgCQAAgEgBgCQgCgDgEABIgEAAg");
	this.shape_54.setTransform(26.4,15.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#E31E26").s().p("AgpApQgPgQAAgZQAAgYAQgQQARgRAZAAQAfAAASASIgYAZQgJgJgLABQgIAAgHAGQgHAHAAAJQAAALAHAHQAGAGAKAAQAIAAAFgDIAAgEIgMAAIAAgaIAwAAIAAAxQgVASghAAQgaAAgSgRg");
	this.shape_55.setTransform(14.9,15.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("AgaAOIAAgbIA1AAIAAAbg");
	this.shape_56.setTransform(181.525,30.025);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AgOANQgFgFAAgIQAAgGAFgGQAGgFAIgBQAJABAFAFQAGAGAAAGQAAAIgGAFQgFAFgJAAQgIAAgGgFg");
	this.shape_57.setTransform(176.15,33.35);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AgmAyIANgcQAIAEAGAAQALAAABgLQgEAEgIgBQgOAAgIgIQgKgJAAgSQAAgRAMgMQAMgLATAAQAsAAAAA0QAAAagKAQQgNAVgcAAQgSAAgNgIgAgEgXQgCACAAAEQAAAEACADQACADACAAQADAAACgCQAAgQgFgBQgBAAAAABQgBAAgBAAQAAAAAAABQgBAAAAABg");
	this.shape_58.setTransform(169.275,29.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AgfAuQgJgJgDgMIAjgIQAAAIAHAAQAFAAAAgIQAAgDgCgCQgCgDgCAAQgEAAgCAFIgggFIADhBIBKAAIAAAeIgpAAIgBAJQAFgBAFgBQAQAAALAKQAMAKAAAQQAAARgNALQgMAMgTAAQgSAAgNgLg");
	this.shape_59.setTransform(160.05,29.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AghA4IAAhvIBEAAIAAAfIgdAAIAAAOIAaAAIAAAcIgaAAIAAAmg");
	this.shape_60.setTransform(148.7,29.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_61.setTransform(138.8,29.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_62.setTransform(115.4,29.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#E31E26").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_63.setTransform(89.575,29.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#E31E26").s().p("AAFA4IgPgkIAAAkIgmAAIAAhvIAwAAQATAAAMAKQALALgBAPQABAKgGAIQgFAGgHAEIAaAvgAgKgJIAGAAQADAAABgCQADgDAAgDQAAgDgDgCQgBgDgDAAIgGAAg");
	this.shape_64.setTransform(80.45,29.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#E31E26").s().p("AgsApQgRgQAAgZQAAgYARgQQARgRAbAAQAcAAARARQARAQAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgRQgFAHAAAKQAAALAFAGQAGAIAIAAQAJAAAFgIQAFgGAAgLQAAgKgFgHQgFgGgJgBQgIABgGAGg");
	this.shape_65.setTransform(68.725,29.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#E31E26").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgLALgTAAIgHAAIAAAjgAgEgIIAEAAQACABACgDQADgCAAgEQAAgEgCgCQgDgDgCAAIgEAAg");
	this.shape_66.setTransform(57.9,29.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#E31E26").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgIIADAAQADABADgDQACgCAAgEQAAgEgCgCQgDgDgCAAIgEAAg");
	this.shape_67.setTransform(48.5,29.4);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#E31E26").s().p("AgmAsQgNgMAAgVIAAhDIApAAIAABEQAAAMAKAAQALAAAAgMIAAhEIAoAAIAABDQAAAVgNAMQgNANgZAAQgYAAgOgNg");
	this.shape_68.setTransform(38,29.5);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#E31E26").s().p("AgeAwQgJgIgEgLIAcgPQAIALAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQAAgEgGgDIgPgIQgWgKAAgTQAAgRAMgJQALgKASAAQASAAAMAKQAKAHADAKIgcAPQgGgJgHgBQAAAAgBABQgBAAAAAAQgBABAAAAQAAABAAABQAAADAHADIAPAHQAWAMAAASQAAAQgMALQgMAJgUAAQgRAAgNgKg");
	this.shape_69.setTransform(27.75,29.4);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#E31E26").s().p("AATAxQgKAHgSAAQgUAAgNgLQgLgKAAgQQAAgSASgLQgGgJgBgKQAAgNALgJQAJgIARAAQARAAAKAJQAKAIAAAOQAAAQgRALIAGAFIABgEIAeAAQABAPgKANIALAKIgXAWgAgRASQAAAEADADQADACADAAQAEAAADgCIgOgNQgCADAAADgAgJgbQABAFAEAEQAEgDAAgGQAAgFgEAAQgEAAgBAFg");
	this.shape_70.setTransform(14.55,29.625);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#E31E26").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_71.setTransform(156.075,15.4);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#E31E26").s().p("AglAsQgNgMgBgVIAAhDIApAAIAABEQAAALAKAAQALAAAAgLIAAhEIApAAIAABDQAAAVgOAMQgOANgYAAQgXAAgOgNg");
	this.shape_72.setTransform(144.85,15.5);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#E31E26").s().p("AALA4IAAgoIgWAAIAAAoIgoAAIAAhvIAoAAIAAApIAWAAIAAgpIAqAAIAABvg");
	this.shape_73.setTransform(120.9,15.4);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#E31E26").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_74.setTransform(91.875,15.4);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#000000").s().p("AAFA4IgPgkIAAAkIgnAAIAAhvIAxAAQATAAAMALQALAJgBAQQAAAKgEAHQgGAHgHAEIAZAvgAgKgJIAGAAQADAAACgDQACgCAAgCQAAgEgCgCQgCgDgDABIgGAAg");
	this.shape_75.setTransform(54.45,15.4);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFALIgFgXIgHAXIAMAAg");
	this.shape_76.setTransform(43.05,15.4);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#000000").s().p("AgdA1IAEgeIAIABQAHAAAAgGIAAhJIAoAAIAABQQAAAfglAAQgLAAgLgDg");
	this.shape_77.setTransform(21.725,15.45);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#000000").s().p("AgKA4IAAhNIgKAEIgIgeIAagIIAfAAIAABvg");
	this.shape_78.setTransform(12.15,15.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#E31E26").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAdIgaAAIAAAKIAdAAIAAAfg");
	this.shape_79.setTransform(129.675,29.4);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#E31E26").s().p("AAMA4IgYg0IAAA0IgoAAIAAhvIApAAIAXA1IAAg1IApAAIAABvg");
	this.shape_80.setTransform(111.075,29.4);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#E31E26").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAdIgaAAIAAAKIAdAAIAAAfg");
	this.shape_81.setTransform(101.325,29.4);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#E31E26").s().p("AAFA4IgPgkIAAAkIgnAAIAAhvIAxAAQATAAAMAKQALALgBAPQAAAKgEAIQgGAGgHAEIAaAvgAgKgJIAGAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgDgDAAIgGAAg");
	this.shape_82.setTransform(92.7,29.4);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#E31E26").s().p("AAPAyQgJgJAAgMQAAgMAJgIQAJgJAOAAQANAAAJAJQAJAIAAAMQAAAMgJAJQgJAIgNAAQgOAAgJgIgAAhAdQAAAGAFAAQADAAAAgGQAAgFgDAAQgFAAAAAFgAg4A4IBShvIAfAAIhSBvgAg7gIQgJgIAAgMQAAgNAJgHQAJgJANAAQAOAAAJAJQAJAHAAANQAAAMgJAIQgJAIgOAAQgNAAgJgIgAgogcQgBAFAEABQAEgBAAgFQAAgGgEAAQgEAAABAGg");
	this.shape_83.setTransform(77.05,29.4);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#E31E26").s().p("AgsAAQAAg5AsAAQAtAAAAA5QAAA6gtAAQgsAAAAg6gAgFAAQAAAaAFAAQAFAAAAgaQAAgZgFgBQgFABAAAZg");
	this.shape_84.setTransform(65.125,29.4);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#E31E26").s().p("AAMA4IgYg0IAAA0IgoAAIAAhvIApAAIAXA1IAAg1IApAAIAABvg");
	this.shape_85.setTransform(51.525,29.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#E31E26").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAdIgaAAIAAAKIAdAAIAAAfg");
	this.shape_86.setTransform(41.775,29.4);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#E31E26").s().p("AgoAqQgRgRABgZQAAgYAQgQQARgRAaAAQAeAAASASIgYAYQgJgHgLAAQgJgBgGAHQgHAGABAKQgBALAHAGQAGAIAJAAQAJAAAFgDIAAgFIgNAAIAAgaIAyAAIAAAxQgWASghAAQgbAAgQgQg");
	this.shape_87.setTransform(31.75,29.4);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#E31E26").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAdIgaAAIAAAKIAdAAIAAAfg");
	this.shape_88.setTransform(21.875,29.4);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#E31E26").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_89.setTransform(13.475,29.4);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#000000").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_90.setTransform(190.475,15.4);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAmBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_91.setTransform(167.4,15.4);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAnBvgAAGALIgGgXIgHAXIANAAg");
	this.shape_92.setTransform(155.5,15.4);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#000000").s().p("AAdA4IAAg2IgLAuIgjAAIgLguIAAA2IgoAAIAAhvIAzAAIARA+IASg+IAzAAIAABvg");
	this.shape_93.setTransform(142.25,15.4);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#000000").s().p("AgsAAQAAg5AsAAQAtAAAAA5QAAA6gtAAQgsAAAAg6gAgFAAQAAAaAFAAQAFAAAAgaQAAgZgFAAQgFAAAAAZg");
	this.shape_94.setTransform(127.075,15.4);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#000000").s().p("AgJA4IAAhNIgKAEIgJgeIAbgIIAdAAIAABvg");
	this.shape_95.setTransform(118.95,15.4);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_96.setTransform(107.025,15.4);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#000000").s().p("AglAsQgNgMgBgVIAAhDIApAAIAABEQAAALAKAAQALAAAAgLIAAhEIApAAIAABDQAAAVgOAMQgOANgYAAQgXAAgOgNg");
	this.shape_97.setTransform(87.6,15.5);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_98.setTransform(76.225,15.4);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#000000").s().p("AgiA4IAAhvIAnAAIAABPIAfAAIAAAgg");
	this.shape_99.setTransform(63.65,15.4);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAnBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_100.setTransform(41.75,15.4);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#000000").s().p("AgtA4IAAhvIAvAAQAUAAALAIQALAKgBAOQAAAOgLAHQAHADAEAGQADAHAAAHQAAARgMAJQgKAJgTAAgAgGAaIAGAAQADAAADgDQABgBAAgDQAAgEgBgCQgDgCgDAAIgGAAgAgGgNIAGAAQAGAAABgGQgBgGgGAAIgGAAg");
	this.shape_101.setTransform(14.1,15.4);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#E31E26").s().p("AgaAOIAAgbIA1AAIAAAbg");
	this.shape_102.setTransform(210.525,30.025);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#E31E26").s().p("AgSAQQAIgFAEgIQgNgDAAgNQAAgHAFgGQAGgFAIgBQAIAAAHAGQAFAGAAAIQAAAMgHANQgGAMgLAHg");
	this.shape_103.setTransform(205.15,34.75);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#E31E26").s().p("AgsAAQAAg5AsAAQAtAAAAA5QAAA6gtAAQgsAAAAg6gAgFAAQAAAaAFAAQAFAAAAgaQAAgZgFgBQgFABAAAZg");
	this.shape_104.setTransform(188.625,29.4);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#E31E26").s().p("AgpA5IAAgOQAAgLAHgKQAFgIAKgIIAQgNQAGgGAAgGQAAgGgEAAQgFAAgCAKIgjgHQAEgOAJgJQALgLATAAQATAAALAJQANAJAAASQAAAQgTAOIgMAKQgGAEAAADIAmAAIAAAeg");
	this.shape_105.setTransform(179.175,29.3);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#E31E26").s().p("AgQAwQgPgKgGgQIgMAAIAAgUIAIAAIAAgDIgIAAIAAgUIAMAAQAGgPAPgKQAPgLASAAQATAAANAIIgNAbQgIgFgIAAQgKAAgHAGIAaAAIgCAUIgeAAIAAABIAAACIAdAAIgCAUIgUAAQAFAFALAAQAKAAAIgFIAMAaQgLAKgUAAQgUAAgPgKg");
	this.shape_106.setTransform(168.85,29.4);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#E31E26").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_107.setTransform(122.625,29.4);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#E31E26").s().p("AAFA4IgPgkIAAAkIgnAAIAAhvIAxAAQAUAAAKAKQAMALgBAPQAAAKgEAIQgGAGgHAEIAZAvgAgKgJIAFAAQAEAAACgCQACgDAAgDQAAgDgCgCQgCgDgEAAIgFAAg");
	this.shape_108.setTransform(113.5,29.4);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#E31E26").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAGAKIgGgVIgHAVIANAAg");
	this.shape_109.setTransform(102.1,29.4);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#E31E26").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_110.setTransform(90.2,29.4);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#E31E26").s().p("AAHA4IgSguIAAAuIgoAAIAAhvIAoAAIAAAoIASgoIAsAAIgbA2IAcA5g");
	this.shape_111.setTransform(78.85,29.4);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#E31E26").s().p("AgmAsQgNgMABgVIAAhDIAoAAIAABEQAAAMAKAAQALAAAAgMIAAhEIAoAAIAABDQABAVgNAMQgOANgZAAQgYAAgOgNg");
	this.shape_112.setTransform(67.5,29.5);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#E31E26").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_113.setTransform(55.9,29.4);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#E31E26").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAmBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_114.setTransform(25.2,29.4);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#E31E26").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgJgMABQgJgBgGAHQgGAGAAAKQAAAKAGAHQAGAHAJAAQAMAAAKgJIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_115.setTransform(14.075,29.4);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#000000").s().p("AgoApQgQgQgBgZQAAgYARgQQARgRAaAAQAeAAARASIgWAZQgJgJgMABQgJAAgGAGQgGAHAAAJQAAALAGAHQAGAGAJAAQAJAAAFgDIAAgEIgNAAIAAgaIAyAAIAAAxQgWASghAAQgbAAgQgRg");
	this.shape_116.setTransform(83.7,15.4);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#000000").s().p("AgTA4IgphvIAsAAIARBBIARhBIArAAIgpBvg");
	this.shape_117.setTransform(48.225,15.4);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgQQARgRAbAAQAcAAARARQARAQAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_118.setTransform(15.325,15.4);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#000000").s().p("AgfAuQgKgJgCgMIAigIQABAIAHAAQAFAAABgIQAAgDgDgCQgBgDgDAAQgEAAgCAFIgggFIADhBIBKAAIAAAeIgpAAIAAAJQAEgBAFgBQAQAAAMAKQALAKAAAQQAAARgMALQgNAMgTAAQgTAAgMgLg");
	this.shape_119.setTransform(100.05,29.5);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#000000").s().p("AgmAyIANgcQAIAEAGAAQALAAABgLQgEAEgIgBQgOAAgIgIQgKgJAAgSQAAgRAMgMQAMgLATAAQAsAAAAA0QAAAagKAQQgNAVgcAAQgSAAgNgIgAgEgXQgCACAAAEQAAAEACADQACADACAAQADAAACgCQAAgQgFgBQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAABg");
	this.shape_120.setTransform(90.775,29.4);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#000000").s().p("AgSAQQAIgFAEgIQgNgDAAgNQAAgHAGgGQAFgFAIgBQAIAAAHAGQAFAGAAAIQAAAMgGANQgHAMgLAHg");
	this.shape_121.setTransform(83.95,34.75);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#000000").s().p("AgmAyIANgcQAIAEAGAAQALAAABgLQgEAEgIgBQgOAAgIgIQgKgJAAgSQAAgRAMgMQAMgLATAAQAsAAAAA0QAAAagKAQQgNAVgcAAQgSAAgNgIgAgEgXQgCACAAAEQAAAEACADQACADACAAQADAAACgCQAAgQgFgBQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAABg");
	this.shape_122.setTransform(77.075,29.4);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#000000").s().p("AgpA5IAAgOQAAgLAHgKQAFgIAKgIIAQgNQAGgGAAgGQAAgGgEAAQgFAAgCAKIgjgHQAEgOAJgJQALgLATAAQATAAALAJQANAJAAASQAAAQgTAOIgMAKQgGAEAAADIAmAAIAAAeg");
	this.shape_123.setTransform(67.775,29.3);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#000000").s().p("AgNANQgGgFAAgIQAAgGAGgGQAFgFAIgBQAJABAFAFQAGAGAAAGQAAAIgGAFQgFAFgJAAQgIAAgFgFg");
	this.shape_124.setTransform(57.95,33.35);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#000000").s().p("AAOA4IgOg5IgMA5IgqAAIgchvIArAAIALA9IANg9IAfAAIAOA9IALg9IAqAAIgdBvg");
	this.shape_125.setTransform(30.65,29.4);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#000000").s().p("AgNANQgGgFAAgIQAAgGAGgGQAFgFAIgBQAJABAGAFQAFAGAAAGQAAAIgFAFQgHAFgIAAQgIAAgFgFg");
	this.shape_126.setTransform(20.05,33.35);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#E31E26").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQALAJAAAQQAAAKgEAHQgGAHgHAEIAZAvgAgJgJIAEAAQAEAAACgDQABgCAAgCQAAgEgBgCQgCgDgEABIgEAAg");
	this.shape_127.setTransform(200.45,15.4);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#E31E26").s().p("AAFA4IgPgkIAAAkIgnAAIAAhvIAxAAQATAAAMALQALAJgBAQQAAAKgEAHQgGAHgHAEIAZAvgAgKgJIAGAAQADAAACgDQACgCAAgCQAAgEgCgCQgCgDgDABIgGAAg");
	this.shape_128.setTransform(129.15,15.4);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#E31E26").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgLALgTAAIgHAAIAAAjgAgEgHIAEAAQACAAACgDQADgCAAgEQAAgEgCgCQgCgDgDABIgEAAg");
	this.shape_129.setTransform(119.2,15.4);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#E31E26").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQALAJAAAQQAAAKgEAHQgGAHgHAEIAZAvgAgJgJIAEAAQAEAAACgDQABgCAAgCQAAgEgBgCQgCgDgEABIgEAAg");
	this.shape_130.setTransform(75.8,15.4);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#E31E26").s().p("AgdAwQgLgIgDgLIAcgQQAIAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPAMgLQALgJASAAQARAAANAJQAKAIADAJIgcARQgFgLgHABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHAEIAPAHQAWAKAAATQAAAQgMALQgMAJgUAAQgRAAgMgKg");
	this.shape_131.setTransform(55.5,15.4);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#E31E26").s().p("AgNANQgGgFAAgIQAAgHAGgFQAFgGAIAAQAJAAAGAGQAFAFAAAHQAAAIgFAFQgHAGgIAAQgIAAgFgGg");
	this.shape_132.setTransform(45.7,19.35);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#E31E26").s().p("AgiA4IAAhvIAnAAIAABPIAfAAIAAAgg");
	this.shape_133.setTransform(39.8,15.4);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#E31E26").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgIgMAAQgJAAgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMAAAKgIIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_134.setTransform(30.575,15.4);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#E31E26").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_135.setTransform(19.775,15.4);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#000000").s().p("AggAwQgMgKAAgSQAAgPALgHQgIgJAAgMQAAgPALgJQALgKATAAQAUAAALAKQALAJAAAPQAAAMgIAJQALAHAAAPQAAASgMAKQgMAKgVAAQgUAAgMgKgAgEAOQgCACAAAEQAAAJAGAAQAGAAAAgJQAAgEgBgCQgCgDgDAAQgCAAgCADgAgEgUQAAAHAEAAQAFAAAAgHQAAgIgFAAQgEAAAAAIg");
	this.shape_136.setTransform(166.225,29.4);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#000000").s().p("AANA4IgNg5IgNA5IgoAAIgehvIAsAAIAKA9IAOg9IAfAAIAOA9IAKg9IAsAAIgdBvg");
	this.shape_137.setTransform(133.3,29.4);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#000000").s().p("AgOANQgFgFAAgIQAAgGAFgGQAGgFAIgBQAJABAFAFQAGAGAAAGQAAAIgGAFQgFAFgJAAQgIAAgGgFg");
	this.shape_138.setTransform(122.7,33.35);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#E31E26").s().p("AAFA4IgPgkIAAAkIgmAAIAAhvIAwAAQATAAAMAKQALALAAAPQAAAKgGAIQgFAGgHAEIAaAvgAgKgJIAGAAQADAAABgCQADgDAAgDQAAgDgDgCQgBgDgDAAIgGAAg");
	this.shape_139.setTransform(84.5,29.4);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#E31E26").s().p("AgiA4IAAhvIAnAAIAABQIAfAAIAAAfg");
	this.shape_140.setTransform(67.35,29.4);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#E31E26").s().p("AgjA4IAAhvIAoAAIAABQIAeAAIAAAfg");
	this.shape_141.setTransform(59.55,29.4);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#E31E26").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAnBvgAAGAKIgGgVIgHAVIANAAg");
	this.shape_142.setTransform(49.55,29.4);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#E31E26").s().p("AgdAwQgKgIgEgLIAcgPQAHALAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQAAgEgGgDIgPgIQgWgKAAgTQAAgRAMgJQALgKASAAQARAAANAKQAKAHADAKIgcAPQgGgJgHgBQAAAAgBABQgBAAAAAAQgBABAAAAQAAABAAABQAAADAHADIAPAHQAWAMAAASQAAAQgMALQgMAJgUAAQgRAAgMgKg");
	this.shape_143.setTransform(30.1,29.4);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#E31E26").s().p("AgoApQgRgQABgZQAAgYAQgQQARgRAZAAQAeAAATASIgYAZQgIgJgMABQgIAAgHAGQgHAHAAAJQAAALAHAHQAGAGAJAAQAJAAAFgDIAAgEIgNAAIAAgaIAyAAIAAAxQgWASghAAQgaAAgRgRg");
	this.shape_144.setTransform(150.45,15.4);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#E31E26").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_145.setTransform(138.825,15.4);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#E31E26").s().p("AAMA4IAAgoIgXAAIAAAoIgpAAIAAhvIApAAIAAApIAXAAIAAgpIAoAAIAABvg");
	this.shape_146.setTransform(115.45,15.4);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#E31E26").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgHIADAAQADAAADgDQACgCAAgEQAAgEgCgCQgDgDgCABIgEAAg");
	this.shape_147.setTransform(105.2,15.4);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#000000").s().p("AghA4IAAhvIBDAAIAAAgIgcAAIAAAMIAaAAIAAAeIgaAAIAAAlg");
	this.shape_148.setTransform(80.9,15.4);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgQQAGAMAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQASAAAMAJQAKAIADAJIgdARQgFgLgGABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMALQgMAJgUAAQgRAAgNgKg");
	this.shape_149.setTransform(59.4,15.4);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#000000").s().p("AglAsQgOgMAAgVIAAhDIApAAIAABEQAAALAKAAQALAAAAgLIAAhEIApAAIAABDQgBAVgNAMQgOANgYAAQgXAAgOgNg");
	this.shape_150.setTransform(49.2,15.5);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#000000").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgIgMAAQgJAAgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMAAAKgIIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_151.setTransform(30.575,15.4);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#E31E26").s().p("AgNANQgGgFAAgIQAAgGAGgGQAFgFAIgBQAJABAGAFQAFAGAAAGQAAAIgFAFQgHAFgIAAQgIAAgFgFg");
	this.shape_152.setTransform(193.65,33.35);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#E31E26").s().p("AgfAuQgKgJgCgMIAigIQABAIAHAAQAFAAABgIQgBgDgCgCQgCgDgCAAQgEAAgCAFIgggFIADhBIBKAAIAAAeIgpAAIgBAJQAFgBAFgBQAQAAAMAKQALAKAAAQQAAARgMALQgNAMgTAAQgTAAgMgLg");
	this.shape_153.setTransform(177.35,29.5);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#000000").s().p("AgoApQgQgQgBgZQAAgYARgQQARgRAaAAQAeAAASASIgYAZQgIgJgMABQgJAAgGAGQgHAHABAJQgBALAHAHQAGAGAJAAQAJAAAFgDIAAgEIgNAAIAAgaIAyAAIAAAxQgWASghAAQgbAAgQgRg");
	this.shape_154.setTransform(194.75,15.4);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAGALIgGgXIgHAXIANAAg");
	this.shape_155.setTransform(171.4,15.4);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_156.setTransform(101.525,15.4);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQATAAALALQALAJAAAQQAAAKgEAHQgFAHgIAEIAZAvgAgJgJIAEAAQAEAAACgDQABgCAAgCQAAgEgBgCQgCgDgEABIgEAAg");
	this.shape_157.setTransform(89.85,15.4);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_158.setTransform(80.425,15.4);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#000000").s().p("AgTA4IgphvIAsAAIARBBIARhBIArAAIgpBvg");
	this.shape_159.setTransform(70.275,15.4);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgQQARgRAbAAQAcAAARARQARAQAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_160.setTransform(57.775,15.4);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgLALgSAAIgIAAIAAAjgAgEgHIADAAQADAAADgDQACgCAAgEQAAgEgCgCQgDgDgCABIgEAAg");
	this.shape_161.setTransform(43.9,15.4);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#000000").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_162.setTransform(22.675,15.4);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgQQAGAMAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQASAAAMAJQAKAIADAJIgdARQgEgLgIABQAAAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMALQgMAJgUAAQgRAAgNgKg");
	this.shape_163.setTransform(13.6,15.4);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#000000").s().p("AgOANQgFgFAAgIQAAgGAFgGQAGgGAIABQAJgBAFAGQAGAGAAAGQAAAIgGAFQgFAGgJAAQgIAAgGgGg");
	this.shape_164.setTransform(90.55,47.35);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#000000").s().p("AgmAyIANgcQAIAEAGAAQALAAABgLQgEADgIABQgOAAgIgJQgKgJAAgRQAAgSAMgLQAMgMATAAQAsAAAAA0QAAAagKAQQgNAVgcAAQgSAAgNgIgAgEgXQgCADAAADQAAAEACADQACADACAAQADAAACgBQAAgSgFABQgBAAAAAAQgBAAgBAAQAAAAAAABQgBAAAAABg");
	this.shape_165.setTransform(83.675,43.4);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#000000").s().p("AgrAGQAAgaAKgQQAOgVAbAAQATAAANAIIgNAcQgIgEgHAAQgLAAgBALQAEgDAIAAQAOAAAJAIQAJAJAAASQAAARgMAMQgMALgTAAQgsAAAAg0gAgDAKQgBARAGAAQAAAAABgBQAAAAABAAQABAAAAgBQAAAAABgBQABgDAAgDQAAgEgCgDQgCgDgDAAQAAAAAAAAQgBAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_166.setTransform(74.275,43.4);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#000000").s().p("AgKA4IAAhNIgJAEIgIgeIAagIIAdAAIAABvg");
	this.shape_167.setTransform(66.3,43.4);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#000000").s().p("AgNANQgGgFAAgIQAAgGAGgGQAFgGAIABQAJgBAFAGQAGAGAAAGQAAAIgGAFQgFAGgJAAQgIAAgFgGg");
	this.shape_168.setTransform(57.95,47.35);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#000000").s().p("AgOANQgFgFAAgIQAAgGAFgGQAGgGAIABQAJgBAFAGQAGAGAAAGQAAAIgGAFQgFAGgJAAQgIAAgGgGg");
	this.shape_169.setTransform(41.25,47.35);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#000000").s().p("AAOA4IgOg6IgMA6IgqAAIgchvIArAAIALA8IANg8IAfAAIAOA8IALg8IAqAAIgdBvg");
	this.shape_170.setTransform(30.65,43.4);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#000000").s().p("AgNANQgGgFAAgIQAAgGAGgGQAFgGAIABQAJgBAGAGQAFAGAAAGQAAAIgFAFQgHAGgIAAQgIAAgFgGg");
	this.shape_171.setTransform(20.05,47.35);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#E31E26").s().p("AgoAqQgQgRgBgZQAAgYARgQQARgRAaAAQAeAAASASIgYAYQgIgHgMAAQgJgBgGAHQgHAGABAKQgBALAHAGQAGAIAJAAQAJAAAFgDIAAgFIgNAAIAAgaIAyAAIAAAxQgWASghAAQgbAAgQgQg");
	this.shape_172.setTransform(190.7,29.4);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#E31E26").s().p("AAMA4IAAgoIgXAAIAAAoIgoAAIAAhvIAoAAIAAAoIAXAAIAAgoIAoAAIAABvg");
	this.shape_173.setTransform(155.7,29.4);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#E31E26").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgIIADAAQADABACgDQADgCAAgEQAAgEgCgCQgDgDgCAAIgEAAg");
	this.shape_174.setTransform(145.45,29.4);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#E31E26").s().p("AAMA4IgYg0IAAA0IgoAAIAAhvIApAAIAXA1IAAg1IApAAIAABvg");
	this.shape_175.setTransform(119.175,29.4);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#E31E26").s().p("AAMA4IgYg0IAAA0IgoAAIAAhvIApAAIAXA1IAAg1IApAAIAABvg");
	this.shape_176.setTransform(96.675,29.4);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#E31E26").s().p("AAGA4IgPgkIAAAkIgoAAIAAhvIAxAAQAUAAAKAKQAMALAAAPQgBAKgEAIQgFAGgIAEIAZAvgAgJgJIAEAAQAEAAABgCQACgDAAgDQAAgDgCgCQgBgDgEAAIgEAAg");
	this.shape_177.setTransform(60.25,29.4);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#E31E26").s().p("AgtA4IAAhvIAwAAQASAAALAJQALAJAAANQAAAOgLAJQAGACAEAGQAEAHAAAHQAAAQgMAKQgKAJgTAAgAgGAaIAGAAQADAAACgCQACgDAAgDQAAgDgCgCQgCgCgDAAIgGAAgAgGgMIAGAAQAGAAAAgHQAAgGgGAAIgGAAg");
	this.shape_178.setTransform(49.95,29.4);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#E31E26").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAnBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_179.setTransform(26.1,29.4);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#E31E26").s().p("AgSARQAJgGADgIQgNgDAAgNQAAgIAGgFQAFgGAIAAQAJABAFAEQAGAGAAAKQAAAMgGALQgHAMgLAJg");
	this.shape_180.setTransform(189.45,20.75);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#E31E26").s().p("AgjA4IAAhvIAoAAIAABPIAeAAIAAAgg");
	this.shape_181.setTransform(146.05,15.4);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#E31E26").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAmBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_182.setTransform(128.25,15.4);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#E31E26").s().p("AgeAwQgKgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQARAAANAJQAKAIADAJIgdARQgFgLgGABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMALQgMAJgUAAQgRAAgNgKg");
	this.shape_183.setTransform(108.8,15.4);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#E31E26").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_184.setTransform(90.225,15.4);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#000000").s().p("AghA1QgKgJgFgMIAggSQAIAOAIAAQABAAAAAAQABgBAAAAQABAAAAgBQAAAAAAgBQAAgEgGgEIgRgIQgYgMAAgVQAAgSANgLQANgKATAAQATAAAPAKQALAJADAKIggASQgFgLgIAAQgBAAgBAAQAAAAgBABQAAAAgBAAQAAABAAABQAAADAIAEIAQAJQAZAMAAAUQAAASgNALQgOALgWAAQgTAAgOgLg");
	this.shape_185.setTransform(228.825,32.875);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#000000").s().p("AgnA+IAAh7IAsAAIAABYIAjAAIAAAjg");
	this.shape_186.setTransform(219.6,32.85);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#000000").s().p("AglA+IAAh7IBLAAIAAAjIggAAIAAALIAdAAIAAAfIgdAAIAAALIAgAAIAAAjg");
	this.shape_187.setTransform(210.75,32.85);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#000000").s().p("AAIA+IgUgzIAAAzIgtAAIAAh7IAtAAIAAAtIAUgtIAxAAIgfA7IAgBAg");
	this.shape_188.setTransform(200.375,32.85);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#000000").s().p("AANA+Igag6IAAA6IgsAAIAAh7IAtAAIAaA6IAAg6IAsAAIAAB7g");
	this.shape_189.setTransform(187.675,32.85);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#000000").s().p("AgVA+IAAh7IAsAAIAAB7g");
	this.shape_190.setTransform(178.55,32.85);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#000000").s().p("AAPA+IgPhAIgOBAIgtAAIghh7IAwAAIAMBDIAPhDIAjAAIAPBDIALhDIAxAAIghB7g");
	this.shape_191.setTransform(166.4,32.85);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#000000").s().p("AgmA+IAAh7IBNAAIAAAjIghAAIAAALIAdAAIAAAfIgdAAIAAALIAhAAIAAAjg");
	this.shape_192.setTransform(149.3,32.85);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#000000").s().p("Ag0A+IAAgXIAvhCIguAAIAAgiIBnAAIAAAXIgvBBIAwAAIAAAjg");
	this.shape_193.setTransform(139.425,32.85);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#000000").s().p("AANA+Igag6IAAA6IgsAAIAAh7IAtAAIAaA6IAAg6IAsAAIAAB7g");
	this.shape_194.setTransform(127.575,32.85);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#000000").s().p("AgxAtQgSgSgBgbQABgbASgSQATgSAeAAQAfAAATASQASASABAbQgBAbgSASQgTATgfAAQgeAAgTgTgAgPgTQgGAIABALQgBAMAGAHQAGAIAJAAQAKAAAGgIQAGgHgBgMQABgLgGgIQgGgHgKAAQgJAAgGAHg");
	this.shape_195.setTransform(114.25,32.875);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#000000").s().p("AANA+Igag6IAAA6IgsAAIAAh7IAtAAIAaA6IAAg6IAsAAIAAB7g");
	this.shape_196.setTransform(97.525,32.85);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#000000").s().p("AATA+IgGgTIgbAAIgGATIgsAAIAqh7IAsAAIArB7gAAFAMIgFgZIgHAZIAMAAg");
	this.shape_197.setTransform(84.6,32.85);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#000000").s().p("AgWA+Igth7IAxAAIATBIIAThIIAwAAIgtB7g");
	this.shape_198.setTransform(71.125,32.85);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#000000").s().p("AANA+Igag6IAAA6IgsAAIAAh7IAtAAIAaA6IAAg6IAsAAIAAB7g");
	this.shape_199.setTransform(54.525,32.85);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#000000").s().p("AglBOIAAh7IBLAAIAAAjIggAAIAAAKIAdAAIAAAgIgdAAIAAALIAgAAIAAAjgAgTgyIAMgbIApAAIgVAbg");
	this.shape_200.setTransform(43.8,31.25);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#000000").s().p("AglBOIAAh7IBLAAIAAAjIggAAIAAAKIAdAAIAAAgIgdAAIAAALIAgAAIAAAjgAgTgyIAMgbIApAAIgVAbg");
	this.shape_201.setTransform(35.05,31.25);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#000000").s().p("AANA+Igag6IAAA6IgsAAIAAh7IAtAAIAaA6IAAg6IAsAAIAAB7g");
	this.shape_202.setTransform(20.925,32.85);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#000000").s().p("AgWA+IAAh7IAsAAIAAB7g");
	this.shape_203.setTransform(11.8,32.85);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#000000").s().p("AghA1QgKgJgFgMIAggSQAIAOAIAAQABAAAAAAQABgBAAAAQABAAAAgBQAAAAAAgBQAAgEgGgEIgRgIQgYgMAAgVQAAgSANgLQANgKATAAQATAAAPAKQALAJADAKIggASQgFgLgIAAQgBAAgBAAQAAAAgBABQAAAAgBAAQAAABAAABQAAADAIAEIAQAJQAZAMAAAUQAAASgNALQgOALgWAAQgTAAgOgLg");
	this.shape_204.setTransform(260.025,17.125);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#000000").s().p("AgsAuQgTgSABgcQAAgbASgSQASgSAdAAQAhAAAUAUIgZAbQgLgJgMAAQgKAAgGAHQgIAHAAALQABAMAGAHQAHAIALAAQAJAAAFgEIAAgEIgNAAIAAgdIA2AAIAAA2QgYAUglAAQgdAAgSgSg");
	this.shape_205.setTransform(248.25,17.125);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#000000").s().p("AANA+Igag6IAAA6IgsAAIAAh7IAtAAIAaA6IAAg6IAsAAIAAB7g");
	this.shape_206.setTransform(235.425,17.1);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#000000").s().p("AATA+IgGgTIgcAAIgFATIgsAAIAqh7IAsAAIArB7gAAFAMIgFgZIgHAZIAMAAg");
	this.shape_207.setTransform(222.5,17.1);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#000000").s().p("AgmA+IAAh7IAsAAIAABYIAiAAIAAAjg");
	this.shape_208.setTransform(211.75,17.1);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#000000").s().p("AAgA+IAAg7IgMAxIgnAAIgMgxIAAA7IgsAAIAAh7IA4AAIATBFIAThFIA5AAIAAB7g");
	this.shape_209.setTransform(195.775,17.1);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#000000").s().p("AgxAtQgSgSAAgbQAAgbASgSQATgSAeAAQAfAAATASQATASAAAbQAAAbgTASQgTATgfAAQgeAAgTgTgAgPgTQgFAIAAALQAAAMAFAHQAFAIAKAAQAKAAAGgIQAGgHAAgMQAAgLgGgIQgGgHgKAAQgKAAgFAHg");
	this.shape_210.setTransform(180.7,17.125);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#000000").s().p("AAIA+IgUgzIAAAzIgtAAIAAh7IAtAAIAAAtIAUgtIAxAAIgfA7IAgBAg");
	this.shape_211.setTransform(167.725,17.1);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#000000").s().p("AglA+IAAh7IBLAAIAAAjIggAAIAAAOIAdAAIAAAhIgdAAIAAApg");
	this.shape_212.setTransform(153.825,17.1);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#000000").s().p("AgxAtQgTgSAAgbQAAgbATgSQATgSAeAAQAfAAATASQASASAAAbQAAAbgSASQgTATgfAAQgeAAgTgTgAgPgTQgGAIAAALQAAAMAGAHQAGAIAJAAQAKAAAGgIQAGgHgBgMQABgLgGgIQgGgHgKAAQgJAAgGAHg");
	this.shape_213.setTransform(142.5,17.125);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#000000").s().p("AglA+IAAh7IBLAAIAAAjIggAAIAAALIAdAAIAAAfIgdAAIAAALIAgAAIAAAjg");
	this.shape_214.setTransform(127.75,17.1);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#000000").s().p("AANA+Igag6IAAA6IgsAAIAAh7IAtAAIAaA6IAAg6IAsAAIAAB7g");
	this.shape_215.setTransform(117.025,17.1);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#000000").s().p("AgVA+IAAh7IAsAAIAAB7g");
	this.shape_216.setTransform(107.9,17.1);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#000000").s().p("AgmA+IAAh7IAsAAIAABYIAiAAIAAAjg");
	this.shape_217.setTransform(100.95,17.1);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#000000").s().p("AANA+Igag6IAAA6IgsAAIAAh7IAtAAIAaA6IAAg6IAsAAIAAB7g");
	this.shape_218.setTransform(90.125,17.1);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#000000").s().p("AgxAtQgTgSAAgbQAAgbATgSQATgSAeAAQAfAAATASQASASAAAbQAAAbgSASQgTATgfAAQgeAAgTgTgAgPgTQgGAIAAALQAAAMAGAHQAGAIAJAAQAKAAAGgIQAGgHgBgMQABgLgGgIQgGgHgKAAQgJAAgGAHg");
	this.shape_219.setTransform(76.8,17.125);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#000000").s().p("AAIA+IgUgzIAAAzIgtAAIAAh7IAtAAIAAAtIAUgtIAxAAIgfA7IAgBAg");
	this.shape_220.setTransform(60.425,17.1);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#000000").s().p("AggA8IAEgiIAJABQAJAAgBgHIAAhSIAsAAIAABZQAAAjgpAAQgMABgMgDg");
	this.shape_221.setTransform(50.2,17.15);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#000000").s().p("AgVA+IAAh7IAsAAIAAB7g");
	this.shape_222.setTransform(43.9,17.1);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#000000").s().p("AAIA+IgUgzIAAAzIgtAAIAAh7IAtAAIAAAtIAUgtIAxAAIgfA7IAgBAg");
	this.shape_223.setTransform(35.125,17.1);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#000000").s().p("AgmA+IAAh7IBNAAIAAAjIghAAIAAALIAdAAIAAAfIgdAAIAAALIAhAAIAAAjg");
	this.shape_224.setTransform(24.4,17.1);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#000000").s().p("AgyA+IAAh7IA1AAQAUAAANAKQAMAJAAAQQAAAPgNAJQAIADADAHQAFAIAAAHQAAASgNALQgMAKgUAAgAgHAdIAHAAQAEAAACgDQACgCAAgDQAAgEgCgDQgCgCgEAAIgHAAgAgHgOIAHAAQAHAAAAgHQAAgHgHAAIgHAAg");
	this.shape_225.setTransform(14.65,17.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_55},{t:this.shape_54,p:{x:26.4}},{t:this.shape_53,p:{x:37}},{t:this.shape_52},{t:this.shape_51,p:{x:54.375,y:15.4}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48,p:{x:83.375}},{t:this.shape_47},{t:this.shape_46,p:{x:102.625}},{t:this.shape_45,p:{x:110.575}},{t:this.shape_44,p:{x:120.325}},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41,p:{x:151.875}},{t:this.shape_40,p:{x:162.625}},{t:this.shape_39,p:{x:171.025}},{t:this.shape_38,p:{x:180.075}},{t:this.shape_37,p:{x:189.725}},{t:this.shape_36,p:{x:200.475}},{t:this.shape_35,p:{x:212.2}},{t:this.shape_34},{t:this.shape_33,p:{x:21.475,y:29.4}},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24,p:{x:106.475,y:29.4}},{t:this.shape_23},{t:this.shape_22,p:{x:126.175}},{t:this.shape_21},{t:this.shape_20,p:{x:150.025}},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14,p:{x:209.775}},{t:this.shape_13},{t:this.shape_12,p:{x:12.975,y:43.4}},{t:this.shape_11,p:{x:22.725,y:43.4}},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8,p:{x:61.3,y:43.4}},{t:this.shape_7},{t:this.shape_6,p:{x:81.825,y:43.4}},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2,p:{x:125.475,y:43.4}},{t:this.shape_1,p:{x:135.175,y:43.4}},{t:this.shape}]},1).to({state:[{t:this.shape_78},{t:this.shape_77},{t:this.shape_8,p:{x:31.15,y:15.4}},{t:this.shape_76},{t:this.shape_75},{t:this.shape_41,p:{x:68.475}},{t:this.shape_44,p:{x:80.525}},{t:this.shape_74},{t:this.shape_39,p:{x:101.075}},{t:this.shape_35,p:{x:110.5}},{t:this.shape_73},{t:this.shape_36,p:{x:132.925}},{t:this.shape_72},{t:this.shape_71,p:{x:156.075,y:15.4}},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65,p:{x:68.725}},{t:this.shape_64},{t:this.shape_63,p:{x:89.575}},{t:this.shape_22,p:{x:103.275}},{t:this.shape_62},{t:this.shape_20,p:{x:127.125}},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58,p:{x:169.275}},{t:this.shape_57,p:{x:176.15}},{t:this.shape_56,p:{x:181.525,y:30.025}}]},1).to({state:[{t:this.shape_101},{t:this.shape_12,p:{x:22.925,y:15.4}},{t:this.shape_6,p:{x:31.375,y:15.4}},{t:this.shape_100},{t:this.shape_8,p:{x:53.65,y:15.4}},{t:this.shape_99,p:{x:63.65}},{t:this.shape_98,p:{x:76.225}},{t:this.shape_97},{t:this.shape_24,p:{x:98.775,y:15.4}},{t:this.shape_96,p:{x:107.025}},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92,p:{x:155.5}},{t:this.shape_91},{t:this.shape_11,p:{x:179.125,y:15.4}},{t:this.shape_90},{t:this.shape_1,p:{x:199.675,y:15.4}},{t:this.shape_2,p:{x:209.425,y:15.4}},{t:this.shape_89,p:{x:13.475}},{t:this.shape_88,p:{x:21.875}},{t:this.shape_87},{t:this.shape_86,p:{x:41.775}},{t:this.shape_85,p:{x:51.525}},{t:this.shape_84,p:{x:65.125}},{t:this.shape_83},{t:this.shape_82,p:{x:92.7}},{t:this.shape_81,p:{x:101.325}},{t:this.shape_80,p:{x:111.075}},{t:this.shape_63,p:{x:121.275}},{t:this.shape_79,p:{x:129.675}}]},1).to({state:[{t:this.shape_118,p:{x:15.325}},{t:this.shape_96,p:{x:27.375}},{t:this.shape_6,p:{x:37.575,y:15.4}},{t:this.shape_117,p:{x:48.225,y:15.4}},{t:this.shape_92,p:{x:60.35}},{t:this.shape_11,p:{x:72.075,y:15.4}},{t:this.shape_116},{t:this.shape_12,p:{x:96.775,y:15.4}},{t:this.shape_1,p:{x:104.725,y:15.4}},{t:this.shape_2,p:{x:114.475,y:15.4}},{t:this.shape_115},{t:this.shape_114},{t:this.shape_71,p:{x:36.775,y:29.4}},{t:this.shape_79,p:{x:45.975}},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111,p:{x:78.85}},{t:this.shape_110},{t:this.shape_109,p:{x:102.1}},{t:this.shape_108},{t:this.shape_107},{t:this.shape_89,p:{x:134.575}},{t:this.shape_65,p:{x:145.325}},{t:this.shape_63,p:{x:156.075}},{t:this.shape_106},{t:this.shape_105,p:{x:179.175}},{t:this.shape_104},{t:this.shape_84,p:{x:198.225}},{t:this.shape_103},{t:this.shape_102,p:{x:210.525,y:30.025}}]},1).to({state:[{t:this.shape_51,p:{x:11.525,y:15.4}},{t:this.shape_135,p:{x:19.775}},{t:this.shape_134},{t:this.shape_133,p:{x:39.8}},{t:this.shape_132},{t:this.shape_131},{t:this.shape_48,p:{x:65.175}},{t:this.shape_130},{t:this.shape_46,p:{x:84.425}},{t:this.shape_45,p:{x:92.375}},{t:this.shape_44,p:{x:102.125}},{t:this.shape_102,p:{x:111.075,y:16.025}},{t:this.shape_129},{t:this.shape_128},{t:this.shape_41,p:{x:140.125}},{t:this.shape_40,p:{x:150.875}},{t:this.shape_39,p:{x:159.275}},{t:this.shape_38,p:{x:168.325}},{t:this.shape_37,p:{x:177.975}},{t:this.shape_36,p:{x:188.725}},{t:this.shape_127},{t:this.shape_14,p:{x:13.475}},{t:this.shape_126},{t:this.shape_125},{t:this.shape_57,p:{x:41.25}},{t:this.shape_22,p:{x:49.625}},{t:this.shape_124,p:{x:57.95}},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119}]},1).to({state:[{t:this.shape_33,p:{x:11.525,y:15.4}},{t:this.shape_2,p:{x:19.775,y:15.4}},{t:this.shape_151},{t:this.shape_99,p:{x:39.8}},{t:this.shape_150},{t:this.shape_149},{t:this.shape_24,p:{x:66.525,y:15.4}},{t:this.shape_1,p:{x:72.975,y:15.4}},{t:this.shape_148},{t:this.shape_36,p:{x:94.025}},{t:this.shape_147},{t:this.shape_146},{t:this.shape_53,p:{x:127.1}},{t:this.shape_145},{t:this.shape_144},{t:this.shape_45,p:{x:160.475}},{t:this.shape_135,p:{x:170.225}},{t:this.shape_39,p:{x:182.975}},{t:this.shape_44,p:{x:192.725}},{t:this.shape_51,p:{x:11.525,y:29.4}},{t:this.shape_85,p:{x:19.775}},{t:this.shape_143},{t:this.shape_63,p:{x:39.175}},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140,p:{x:67.35}},{t:this.shape_81,p:{x:75.075}},{t:this.shape_139},{t:this.shape_79,p:{x:93.125}},{t:this.shape_80,p:{x:102.875}},{t:this.shape_14,p:{x:116.125}},{t:this.shape_138},{t:this.shape_137},{t:this.shape_124,p:{x:143.9}},{t:this.shape_22,p:{x:152.275}},{t:this.shape_136},{t:this.shape_58,p:{x:175.575}},{t:this.shape_57,p:{x:182.45}},{t:this.shape_56,p:{x:187.825,y:30.025}}]},1).to({state:[{t:this.shape_163},{t:this.shape_162},{t:this.shape_8,p:{x:33.05,y:15.4}},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_98,p:{x:111.275}},{t:this.shape_118,p:{x:126.375}},{t:this.shape_96,p:{x:138.425}},{t:this.shape_6,p:{x:148.625,y:15.4}},{t:this.shape_117,p:{x:159.275,y:15.4}},{t:this.shape_155},{t:this.shape_11,p:{x:183.125,y:15.4}},{t:this.shape_154},{t:this.shape_12,p:{x:207.825,y:15.4}},{t:this.shape_1,p:{x:215.775,y:15.4}},{t:this.shape_2,p:{x:225.525,y:15.4}},{t:this.shape_115},{t:this.shape_114},{t:this.shape_71,p:{x:36.775,y:29.4}},{t:this.shape_79,p:{x:45.975}},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111,p:{x:78.85}},{t:this.shape_110},{t:this.shape_109,p:{x:102.1}},{t:this.shape_108},{t:this.shape_107},{t:this.shape_89,p:{x:134.575}},{t:this.shape_65,p:{x:145.325}},{t:this.shape_63,p:{x:156.075}},{t:this.shape_105,p:{x:168.175}},{t:this.shape_153},{t:this.shape_84,p:{x:186.725}},{t:this.shape_152},{t:this.shape_102,p:{x:199.025,y:30.025}}]},1).to({state:[{t:this.shape_33,p:{x:11.525,y:15.4}},{t:this.shape_2,p:{x:19.775,y:15.4}},{t:this.shape_151},{t:this.shape_99,p:{x:39.8}},{t:this.shape_150},{t:this.shape_149},{t:this.shape_24,p:{x:66.525,y:15.4}},{t:this.shape_1,p:{x:72.975,y:15.4}},{t:this.shape_148},{t:this.shape_184},{t:this.shape_135,p:{x:98.475}},{t:this.shape_183},{t:this.shape_37,p:{x:117.875}},{t:this.shape_182},{t:this.shape_133,p:{x:138.25}},{t:this.shape_181},{t:this.shape_45,p:{x:153.775}},{t:this.shape_54,p:{x:163.2}},{t:this.shape_39,p:{x:171.825}},{t:this.shape_44,p:{x:181.575}},{t:this.shape_180},{t:this.shape_111,p:{x:14.75}},{t:this.shape_179},{t:this.shape_140,p:{x:36.1}},{t:this.shape_51,p:{x:42.375,y:29.4}},{t:this.shape_178},{t:this.shape_177},{t:this.shape_88,p:{x:68.875}},{t:this.shape_82,p:{x:78.3}},{t:this.shape_86,p:{x:86.925}},{t:this.shape_176},{t:this.shape_81,p:{x:109.425}},{t:this.shape_175},{t:this.shape_65,p:{x:134.275}},{t:this.shape_174},{t:this.shape_173},{t:this.shape_109,p:{x:167.35}},{t:this.shape_85,p:{x:179.075}},{t:this.shape_172},{t:this.shape_79,p:{x:200.725}},{t:this.shape_80,p:{x:210.475}},{t:this.shape_6,p:{x:13.475,y:43.4}},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_117,p:{x:49.625,y:43.4}},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_56,p:{x:95.925,y:44.025}}]},1).to({state:[{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185}]},1).wait(1));

	// Layer_1
	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#FFFFFF").s().p("AtMCTIAAklIaZAAIAAElg");
	this.shape_226.setTransform(116.5106,29.5008,1.3786,1.9999);
	this.shape_226._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_226).wait(1).to({_off:false},0).wait(1).to({scaleY:1.5253,y:22.5052},0).wait(6).to({scaleY:1.9999,y:29.5008},0).wait(1).to({scaleX:1.6274,scaleY:1.6609,x:137.5161,y:24.5114},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,275.1,59);


(lib.label_green_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(7));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgoBBIAHgNQAPAJARAAQARAAAJgHQAMgJAAgRIAAgJQgNAUgYAAQgXAAgPgRQgOgPAAgZQAAgZAOgQQAPgQAXAAQAOAAALAIQAIAFAFAHIAAgRIAQAAIAABdQAAA3g2AAQgWAAgSgLgAgZgwQgLAMAAASQAAASALAMQAKALAQAAQAQAAALgMQAJgLABgSQgBgTgKgMQgLgLgQAAQgPAAgKAMg");
	this.shape.setTransform(148.15,14.175);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgGgIgMAAQgMAAgHAJQgIAIAAANIAABCIgRAAIAAhsIAQAAIAAAPQAFgHAIgFQAJgGAKAAQATAAAKANQAKAMAAASIAABEg");
	this.shape_1.setTransform(136.4,12.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgHBKIAAhsIAQAAIAABsgAgHg2QgEgDAAgFQAAgFAEgDQADgDAEAAQAFAAADADQADADABAFQgBAFgDADQgDADgFAAQgEAAgDgDg");
	this.shape_2.setTransform(128.35,10.325);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgoBBIAHgNQAOAJATAAQAQAAAKgHQALgJAAgRIAAgJQgNAUgYAAQgXAAgPgRQgOgPAAgZQAAgZAOgQQAPgQAXAAQAOAAAMAIQAIAFAEAHIAAgRIAQAAIAABdQAAA3g2AAQgWAAgSgLgAgZgwQgLAMAAASQAAASALAMQALALAPAAQAQAAALgMQAKgLAAgSQgBgTgKgMQgLgLgQAAQgPAAgKAMg");
	this.shape_3.setTransform(119.45,14.175);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_4.setTransform(110.575,12.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYAQgQQAQgQAYAAQAYAAARAQQAQAQAAAYQAAAYgQARQgQAQgZABQgXgBgRgQgAgcgdQgLAMAAARQAAASALAMQAMAMAQAAQARAAAMgMQALgMAAgSQAAgSgLgLQgMgNgRAAQgQAAgMANg");
	this.shape_5.setTransform(100.35,12.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgoA2IAAgLIA9hSIg8AAIAAgPIBQAAIAAAMIg+BTIA+AAIAAANg");
	this.shape_6.setTransform(89.1,12.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgkAqQgPgQgBgaQABgZAPgPQAPgRAVABQAYAAAOAPQAOAPABAZIAAAFIhYAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgJAHQgNAHgSABQgXAAgQgQgAAkgIQgBgQgKgKQgKgIgPAAQgMAAgKAJQgLAKgBAPIBGAAIAAAAg");
	this.shape_7.setTransform(78.35,12.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AglA5IAAARIgPAAIAAiXIAQAAIAAA9QAEgHAIgFQAMgJAOABQAWgBAPARQAPAQAAAYQAAAZgQAQQgPAQgWABQgYAAgOgVgAgagKQgLALAAATQAAASAKAMQALANAQAAQAPAAALgMQAKgMABgTQAAgSgLgLQgKgNgQAAQgQAAgKAMg");
	this.shape_8.setTransform(66.35,10.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AglAiIAMgIQAJARARAAQAJAAAFgFQAGgEAAgJQAAgIgIgFQgEgCgOgGQgNgFgGgGQgIgIAAgMQAAgOAKgHQAJgJAPABQANAAAKAHQAIAGACAHIgMAIQgHgPgPAAQgHAAgFAFQgFADAAAHQAAAJAIAFIASAIQANAGAGAEQAJAIAAANQAAAOgLAKQgLAIgQABQgaAAgLgYg");
	this.shape_9.setTransform(49.875,12.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgHBKIAAhsIAPAAIAABsgAgHg2QgEgDABgFQgBgFAEgDQADgDAEAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_10.setTransform(43.25,10.325);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFAMAAIAEgBIAAAPIgKABQgfAAAAgng");
	this.shape_11.setTransform(37.85,11.225);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_12.setTransform(29.225,12.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_13.setTransform(21.325,12.15);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgtA3QgVgWAAghQAAggAVgVQAWgWAfAAQAhAAAVAVIgLALQgSgRgZAAQgYAAgQASQgQARAAAZQAAAaAQARQAQARAYAAQAbAAAQgPIAAgfIghAAIAAgOIAxAAIAAA0QgYAYgkAAQgeAAgWgVg");
	this.shape_14.setTransform(9.5,10.425);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgoBBIAGgNQAPAJATAAQAPAAALgHQALgJAAgRIAAgJQgMAUgZAAQgXAAgPgRQgPgPAAgZQAAgZAPgQQAPgQAXAAQANAAANAIQAHAFAEAHIAAgRIARAAIAABdQgBA3g1AAQgWAAgSgLgAgagwQgKAMAAASQAAASALAMQAKALAPAAQARAAALgMQAKgLgBgSQABgTgLgMQgLgLgQAAQgPAAgLAMg");
	this.shape_15.setTransform(149.8,14.175);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgNAAgHAJQgIAIAAANIAABCIgQAAIAAhsIAPAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_16.setTransform(138.05,12.15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgHBKIAAhsIAQAAIAABsgAgHg2QgEgDABgFQgBgFAEgDQADgDAEAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_17.setTransform(130,10.325);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFALAAIAFgBIAAAPIgKABQgfAAAAgng");
	this.shape_18.setTransform(124.6,11.225);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAHAIALgBQAMABAIgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgJAGgKAAQgSAAgLgNg");
	this.shape_19.setTransform(111.4,12.45);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgHBMIAAiXIAPAAIAACXg");
	this.shape_20.setTransform(103.475,10.15);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgNAAgHAJQgIAIAAANIAABCIgQAAIAAhsIAPAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_21.setTransform(86.75,12.15);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_22.setTransform(64.625,12.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AglAiIAMgIQAJARARAAQAJAAAFgFQAGgEAAgJQAAgIgIgFQgEgCgOgGQgNgFgGgGQgIgIAAgMQAAgOAKgHQAJgJAPABQANAAAKAHQAIAGACAHIgMAIQgHgPgPAAQgHAAgFAFQgFADAAAHQAAAJAIAFIASAIQANAGAGAEQAJAIAAANQAAAOgLAKQgLAIgQABQgaAAgLgYg");
	this.shape_23.setTransform(49.875,12.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_24.setTransform(29.225,12.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgIAHgHQAKgGALAAIAAARQgfAAAAApIAAA1g");
	this.shape_25.setTransform(109.525,34.15);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AghArQgKgLAAgUIAAhDIARAAIAABEQAAANAHAHQAIAIALAAQALAAAIgJQAIgIAAgOIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgJAGgKAAQgSAAgLgNg");
	this.shape_26.setTransform(99.95,34.45);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYARgQQAQgRAXAAQAYAAARARQAQAQAAAYQAAAYgQARQgRAQgYAAQgYAAgQgQgAgcgdQgLAMAAARQAAATALALQALAMARAAQARAAAMgMQALgMAAgSQAAgRgLgNQgMgMgRABQgRAAgLAMg");
	this.shape_27.setTransform(87.9,34.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgkApQgQgPAAgZQAAgaAQgQQAQgPAUgBQAYAAAOAQQAOAQAAAYIAAAEIhXAAQABASALALQALAKAQAAQAWABAMgSIALAIQgGAKgJAGQgNAJgRgBQgYAAgQgQgAAjgIQAAgQgKgJQgKgJgPAAQgMAAgKAJQgLAKgBAPIBFAAIAAAAg");
	this.shape_28.setTransform(69.45,34.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgIAHgHQAKgGALAAIAAARQgfAAAAApIAAA1g");
	this.shape_29.setTransform(60.675,34.15);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AglAiIAMgHQAJAQARAAQAJAAAFgEQAGgFAAgJQAAgHgIgGQgEgDgOgGQgNgEgGgFQgIgJAAgMQAAgNAKgJQAJgHAPgBQANAAAKAIQAIAGACAIIgMAGQgHgOgPABQgHAAgFADQgFAFAAAGQAAAJAIAFIASAIQANAGAGAEQAJAIAAANQAAAOgLAJQgLAKgQgBQgaABgLgYg");
	this.shape_30.setTransform(47.125,34.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgHBKIAAhsIAPAAIAABsgAgHg2QgDgDgBgFQABgFADgDQADgDAEAAQAFAAADADQADADABAFQgBAFgDADQgDADgFAAQgEAAgDgDg");
	this.shape_31.setTransform(40.5,32.325);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgNAcIAAg5IgNAAIAAgOIANAAIAAgXIAPAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFALAAIAFgBIAAAPIgKABQgeAAAAgng");
	this.shape_32.setTransform(35.1,33.225);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgQAMgJQAMgKASAAQATAAAJAKIAAgNQAAgMgHgGQgHgHgMABQgQgBgPAKIgHgMQATgMAUgBQAqABAAAoIAABHIgPAAIgBgJQgLAMgSgBQgSAAgMgKgAgSAEQgIAHAAAKQAAAKAIAGQAHAHAMgBQAMABAHgHQAIgGAAgKQAAgKgIgHQgHgFgMAAQgMAAgHAFg");
	this.shape_33.setTransform(26.475,34.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgIAHgHQAKgGALAAIAAARQgfAAAAApIAAA1g");
	this.shape_34.setTransform(18.575,34.15);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgoBBIAGgNQAPAJATAAQAPAAALgHQALgJAAgRIAAgJQgMAUgZAAQgXAAgPgRQgOgPAAgZQAAgZAOgQQAPgQAXAAQANAAANAIQAHAFAEAHIAAgRIARAAIAABdQgBA3g1AAQgWAAgSgLgAgZgwQgLAMAAASQAAASALAMQAKALAPAAQARAAALgMQAKgLgBgSQABgTgLgMQgLgLgQAAQgPAAgKAMg");
	this.shape_35.setTransform(8.05,36.175);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFAMAAIAEgBIAAAPIgKABQgfAAAAgng");
	this.shape_36.setTransform(138.2,11.225);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("Ag1BMIAAiUIAQAAIAAARQAOgUAXAAQAYAAAPAQQAOAQAAAZQAAAZgOAQQgPAQgWAAQgPAAgLgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQALALAPAAQAQAAAKgMQALgMgBgSQAAgSgLgMQgKgMgPAAQgQAAgLANg");
	this.shape_37.setTransform(90.35,14.125);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("Ag0BMIAAiUIAPAAIAAARQAOgUAYAAQAWAAAQAQQAPAQAAAZQAAAZgPAQQgPAQgWAAQgOAAgMgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQAKALAQAAQAQAAAKgMQALgMAAgSQAAgSgMgMQgJgMgQAAQgQAAgLANg");
	this.shape_38.setTransform(77.5,14.125);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_39.setTransform(65.075,12.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgjAqQgRgQABgaQgBgZARgPQAOgRAWABQAYAAANAPQAPAPAAAZIAAAFIhXAAQAAARALALQAKALARAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgRABQgXAAgPgQgAAkgIQgBgQgLgKQgJgIgOAAQgNAAgKAJQgLAKAAAPIBFAAIAAAAg");
	this.shape_40.setTransform(48.8,12.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AglA9QgPgQgBgZQAAgYAPgQQAOgRAYABQANgBAMAJQAIAFAEAHIAAg9IAQAAIAACXIgQAAIAAgRQgNAVgYAAQgWgBgPgQgAgZgJQgLALAAASQAAATALAMQAKAMAPAAQARAAAKgNQALgMgBgSQABgTgLgLQgLgMgQAAQgPAAgKANg");
	this.shape_41.setTransform(36.05,10.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAIAIAKgBQANABAHgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgKAGgJAAQgSAAgLgNg");
	this.shape_42.setTransform(24.3,12.45);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("Ag1A2QgVgWAAggQAAggAVgWQAWgVAfAAQAgAAAWAWQAVAWAAAfQAAAggVAWQgWAWggAAQgfAAgWgWgAgogqQgQARAAAZQAAAaAQARQARARAXAAQAZAAAQgRQAQgRAAgaQAAgZgQgRQgQgSgZAAQgXAAgRASg");
	this.shape_43.setTransform(10.3,10.425);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgkAqQgPgQAAgaQAAgZAPgPQAQgRAUABQAZAAAOAPQAOAPgBAZIAAAFIhXAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgQABQgYAAgQgQgAAjgIQAAgQgLgKQgJgIgPAAQgMAAgKAJQgKAKgBAPIBEAAIAAAAg");
	this.shape_44.setTransform(151.1,12.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AgkAqQgQgQAAgaQAAgZAQgPQAQgRAUABQAZAAAOAPQAOAPgBAZIAAAFIhXAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgQABQgYAAgQgQgAAjgIQAAgQgLgKQgJgIgPAAQgMAAgKAJQgKAKgCAPIBFAAIAAAAg");
	this.shape_45.setTransform(132.55,12.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgHgIgLAAQgLAAgIAJQgIAIAAANIAABCIgRAAIAAhsIARAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMAAASIAABEg");
	this.shape_46.setTransform(120.9,12.15);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAIAIAKgBQANABAHgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgKAGgJAAQgTAAgKgNg");
	this.shape_47.setTransform(102.7,12.45);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYAQgQQAQgQAYAAQAZAAAQAQQAQAQAAAYQAAAYgRARQgPAQgZABQgYgBgQgQgAgcgdQgLAMAAARQAAASALAMQAMAMAQAAQASAAALgMQALgMAAgSQAAgSgLgLQgLgNgSAAQgQAAgMANg");
	this.shape_48.setTransform(90.65,12.3);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_49.setTransform(63.425,12.15);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_50.setTransform(21.325,12.15);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AglA9QgPgQgBgZQAAgYAPgQQAOgRAXABQAPgBALAJQAIAFAEAHIAAg9IAQAAIAACXIgQAAIAAgRQgNAVgYAAQgXgBgOgQgAgZgJQgLALAAASQAAATALAMQALAMAOAAQARAAAKgNQAKgMAAgSQAAgTgLgLQgKgMgQAAQgOAAgLANg");
	this.shape_51.setTransform(186.9,10.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AgVBeIAAgOIAFAAQASAAAAgXIAAhvIARAAIAABwQAAAkgiAAIgGAAgAAChKQgDgDABgFQgBgFADgDQADgDAFAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgFAAgDgDg");
	this.shape_52.setTransform(177.6,12.325);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AAVBMIgug6IAAA6IgQAAIAAiXIAQAAIAABZIAsguIAVAAIguAvIAwA9g");
	this.shape_53.setTransform(161.375,10.15);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgGgIgMAAQgMAAgHAJQgIAIAAANIAABCIgRAAIAAhsIAQAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_54.setTransform(150,12.15);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#000000").s().p("AgkAqQgQgQAAgaQAAgZAQgPQAQgRAUABQAYAAAOAPQAOAPABAZIAAAFIhYAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgJAHQgNAHgRABQgYAAgQgQgAAjgIQAAgQgKgKQgKgIgPAAQgMAAgKAJQgLAKgBAPIBFAAIAAAAg");
	this.shape_55.setTransform(138.3,12.3);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("AgmA9QgOgQAAgZQAAgYAOgQQAPgRAWABQAOgBAMAJQAIAFAEAHIAAg9IAQAAIAACXIgPAAIAAgRQgOAVgYAAQgXgBgPgQgAgZgJQgLALAAASQAAATALAMQAKAMAQAAQARAAAKgNQAJgMABgSQAAgTgMgLQgJgMgQAAQgQAAgKANg");
	this.shape_56.setTransform(125.55,10.3);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AglA5IAAARIgQAAIAAiXIARAAIAAA9QAEgHAIgFQALgJAPABQAWgBAPARQAOAQAAAYQAAAZgOAQQgPAQgYABQgXAAgOgVgAgagKQgLALAAATQAAASAKAMQALANAQAAQAPAAAKgMQALgMAAgTQABgSgLgLQgKgNgQAAQgPAAgLAMg");
	this.shape_57.setTransform(101.6,10.3);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgMAAgIAJQgIAIAAANIAABCIgQAAIAAhsIAQAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMgBASIAABEg");
	this.shape_58.setTransform(83.8,12.15);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AgjAqQgRgQABgaQgBgZARgPQAPgRAVABQAXAAAPAPQANAPAAAZIAAAFIhWAAQAAARALALQAKALARAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgRABQgXAAgPgQgAAkgIQgCgQgKgKQgJgIgOAAQgNAAgKAJQgKAKgBAPIBFAAIAAAAg");
	this.shape_59.setTransform(72.1,12.3);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AglA9QgPgQAAgZQgBgYAPgQQAOgRAYABQANgBAMAJQAIAFAEAHIAAg9IARAAIAACXIgRAAIAAgRQgNAVgYAAQgWgBgPgQgAgZgJQgKALAAASQAAATAKAMQAKAMAPAAQARAAALgNQAKgMgBgSQABgTgLgLQgLgMgQAAQgPAAgKANg");
	this.shape_60.setTransform(35.95,10.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AglA4QgKgSAAgmQAAgmAKgRQAMgUAZAAQAbAAALAUQAJARABAmQgBAmgJASQgLAUgbAAQgZAAgMgUgAgYgqQgFAMAAAeQAAAeAFANQAGARASAAQATAAAGgRQAFgNAAgeQAAgegFgMQgGgSgTAAQgSAAgGASg");
	this.shape_61.setTransform(18.8,10.425);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AggBAQgJgIgEgNIAOgFQAHAVAYAAQAMAAAJgIQAIgIAAgNQAAgNgJgIQgJgIgQAAIgLAAIAAgNIAmguIg+AAIAAgQIBRAAIAAAQIgmAtQAQAAANAKQAOALAAAVQAAAUgNANQgNANgUAAQgTAAgNgLg");
	this.shape_62.setTransform(7.425,10.525);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AgnBBIAFgNQAQAJARAAQAQAAAKgHQAMgJAAgRIAAgJQgMAUgZAAQgXAAgPgRQgPgPAAgZQAAgZAPgQQAPgQAXAAQANAAAMAIQAJAFADAHIAAgRIAQAAIAABdQABA3g2AAQgWAAgRgLgAgagwQgKAMAAASQAAASALAMQALALAOAAQARAAAKgMQAKgLAAgSQAAgTgLgMQgKgLgQAAQgPAAgLAMg");
	this.shape_63.setTransform(135.25,14.175);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgMAAgIAJQgIAIAAANIAABCIgQAAIAAhsIAQAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMAAASIAABEg");
	this.shape_64.setTransform(123.5,12.15);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#000000").s().p("AgHBKIAAhsIAPAAIAABsgAgHg2QgDgDgBgFQABgFADgDQADgDAEAAQAFAAADADQAEADAAAFQAAAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_65.setTransform(115.45,10.325);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#000000").s().p("Ag0BMIAAiUIAPAAIAAARQAOgUAYAAQAWAAAPAQQAQAQAAAZQAAAZgPAQQgPAQgWAAQgOAAgMgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQAKALAQAAQAQAAAKgMQALgMAAgSQgBgSgKgMQgLgMgPAAQgQAAgLANg");
	this.shape_66.setTransform(66.35,14.125);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_14},{t:this.shape_13,p:{x:21.325}},{t:this.shape_12,p:{x:29.225}},{t:this.shape_11,p:{x:37.85,y:11.225}},{t:this.shape_10},{t:this.shape_9,p:{x:49.875}},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4,p:{x:110.575}},{t:this.shape_3},{t:this.shape_2,p:{x:128.35}},{t:this.shape_1},{t:this.shape,p:{x:148.15}}]},1).to({state:[{t:this.shape_14},{t:this.shape_4,p:{x:21.325}},{t:this.shape_24,p:{x:29.225}},{t:this.shape_11,p:{x:37.85,y:11.225}},{t:this.shape_10},{t:this.shape_23},{t:this.shape_22,p:{x:64.625}},{t:this.shape_12,p:{x:75.325}},{t:this.shape_21},{t:this.shape_9,p:{x:96.825}},{t:this.shape_20,p:{x:103.475}},{t:this.shape_19},{t:this.shape_2,p:{x:119.35}},{t:this.shape_18,p:{x:124.6}},{t:this.shape_17,p:{x:130}},{t:this.shape_16,p:{x:138.05}},{t:this.shape_15}]},1).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40,p:{x:48.8}},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_24,p:{x:101.475}},{t:this.shape_4,p:{x:110.975}},{t:this.shape_22,p:{x:118.875}},{t:this.shape_12,p:{x:129.575}},{t:this.shape_36,p:{x:138.2}},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32,p:{x:35.1,y:33.225}},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_11,p:{x:78.35,y:33.225}},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25}]},1).to({state:[{t:this.shape_14},{t:this.shape_50},{t:this.shape_12,p:{x:29.225}},{t:this.shape_11,p:{x:37.85,y:11.225}},{t:this.shape_10},{t:this.shape_9,p:{x:49.875}},{t:this.shape_49},{t:this.shape_40,p:{x:72.2}},{t:this.shape_32,p:{x:81.1,y:11.225}},{t:this.shape_48},{t:this.shape_47},{t:this.shape_13,p:{x:112.275}},{t:this.shape_46},{t:this.shape_45},{t:this.shape_4,p:{x:142.325}},{t:this.shape_44},{t:this.shape_16,p:{x:162.8}}]},1).to({state:[{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_12,p:{x:47.825}},{t:this.shape,p:{x:59.5}},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_40,p:{x:113.6}},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_18,p:{x:168.7}},{t:this.shape_17,p:{x:174.1}},{t:this.shape_52},{t:this.shape_51}]},1).to({state:[{t:this.shape_14},{t:this.shape_4,p:{x:21.325}},{t:this.shape_24,p:{x:29.225}},{t:this.shape_36,p:{x:37.85}},{t:this.shape_10},{t:this.shape_23},{t:this.shape_66},{t:this.shape_20,p:{x:74.675}},{t:this.shape_22,p:{x:81.975}},{t:this.shape_12,p:{x:92.675}},{t:this.shape_11,p:{x:101.3,y:11.225}},{t:this.shape_9,p:{x:108.825}},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,195.9,47);


(lib.label_green_circle_checl_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhOASIADgjICaABIAAAig");
	this.shape.setTransform(7.9,1.725);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle_checl_2, new cjs.Rectangle(0,0,15.8,3.5), null);


(lib.label_green_circle_check_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgygRIBjAAIACAhIhlACg");
	this.shape.setTransform(5.075,1.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle_check_1, new cjs.Rectangle(0,0,10.2,3.5), null);


(lib.intro_img_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.intro_img();
	this.instance.parent = this;
	this.instance.setTransform(0,-40,0.9786,0.9785);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.intro_img_1, new cjs.Rectangle(0,-40,757.5,307.3), null);


(lib.endframe_video_line4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape.setTransform(153.075,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_1.setTransform(139.125,11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAIBRIgWg1IAAA1Ig4AAIAAihIBGAAQAcABAQAPQAQAOAAAXQAAANgIAMQgHAJgKAGIAkBEgAgOgOIAHAAQAFABADgEQADgDAAgFQAAgEgDgEQgDgCgFAAIgHAAg");
	this.shape_2.setTransform(126.725,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhAA8QgYgYAAgkQAAgjAYgYQAZgXAnAAQAoAAAZAXQAYAYAAAjQAAAkgYAYQgYAXgpAAQgnAAgZgXgAgUgZQgHAKAAAPQAAAQAHAKQAIAKAMAAQANAAAHgKQAIgKAAgQQAAgPgIgKQgHgKgNABQgMgBgIAKg");
	this.shape_3.setTransform(109.85,11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgrBFQgOgMgFgPIApgXQAKARALAAQAEAAAAgEQAAgEgJgGIgWgKQgegQAAgcQAAgWAQgPQARgNAaAAQAZAAASANQAPALADAOIgoAXQgIgOgKAAQgFAAAAADQAAAFAKAFIAWALQAfAQAAAaQAAAZgRAOQgRAOgdgBQgZAAgSgOg");
	this.shape_4.setTransform(94.1,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgrBFQgOgMgFgPIApgXQAKARALAAQAEAAAAgEQAAgEgJgGIgVgKQgggQAAgcQAAgWARgPQARgNAaAAQAYAAATANQAPALAEAOIgqAXQgGgOgLAAQgFAAAAADQAAAFAKAFIAVALQAgAQAAAaQAAAZgRAOQgRAOgcgBQgZAAgTgOg");
	this.shape_5.setTransform(80.85,11.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_6.setTransform(68.575,11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgtA9QgZgYAAglQAAgiAYgYQAYgYAmAAQAfAAAXAPIgSAvQgMgNgSAAQgOAAgIAJQgJAKAAAOQAAAPAJAKQAIAJAOAAQASAAANgNIASAuQgXARgggBQglAAgYgWg");
	this.shape_7.setTransform(55.425,11.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhAA8QgYgYAAgkQAAgjAYgYQAZgXAnAAQAoAAAZAXQAYAYAAAjQAAAkgYAYQgZAXgoAAQgnAAgZgXgAgUgZQgHAKAAAPQAAAQAHAKQAIAKAMAAQANAAAHgKQAIgKAAgQQAAgPgIgKQgHgKgNABQgMgBgIAKg");
	this.shape_8.setTransform(39.1,11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAIBRIgWg1IAAA1Ig4AAIAAihIBGAAQAcABAQAPQAQAOAAAXQAAANgIAMQgHAJgKAGIAkBEgAgOgOIAHAAQAFABADgEQADgDAAgFQAAgEgDgEQgDgCgFAAIgHAAg");
	this.shape_9.setTransform(23.325,11.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag+BRIAAihIBCAAQAbAAAQAQQARAPgBAYQAAAZgQAOQgQAQgaAAIgMAAIAAAzgAgHgLIAGAAQAEgBAEgDQADgDAAgGQAAgFgDgEQgDgCgFAAIgGAAg");
	this.shape_10.setTransform(9.05,11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAOBDIgcg/IAAA/IgwAAIAAiEIAxAAIAcA+IAAg+IAwAAIAACEg");
	this.shape_11.setTransform(208.075,9.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_12.setTransform(196.65,9.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAHBDIgSgrIAAArIgvAAIAAiEIA7AAQAWAAAOAMQANAMgBATQAAAKgFAKQgGAHgJAGIAeA4gAgLgLIAFAAQAFAAACgDQACgCAAgEQAAgEgCgDQgCgCgFAAIgFAAg");
	this.shape_13.setTransform(186.5,9.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAUBDIgGgVIgeAAIgGAVIgvAAIAuiEIAvAAIAuCEgAAGANIgGgbIgIAbIAOAAg");
	this.shape_14.setTransform(173.125,9.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgYBDIgviEIA0AAIATBMIAVhMIA0AAIgxCEg");
	this.shape_15.setTransform(158.8,9.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAHBDIgSgrIAAArIgvAAIAAiEIA6AAQAXAAANAMQAOAMAAATQgBAKgFAKQgGAHgKAGIAfA4gAgLgLIAFAAQAFAAABgDQADgCAAgEQAAgEgDgDQgBgCgFAAIgFAAg");
	this.shape_16.setTransform(145.95,9.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgoBDIAAiEIBSAAIAAAlIgjAAIAAALIAfAAIAAAiIgfAAIAAAMIAjAAIAAAmg");
	this.shape_17.setTransform(134.9,9.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_18.setTransform(122.05,9.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgXBDIAAhfIgaAAIAAglIBjAAIAAAlIgaAAIAABfg");
	this.shape_19.setTransform(112.175,9.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("Ag8BDIAAiEIAvAAQAiAAATARQAVASAAAeQAAAegVATQgTASgiAAgAgNAcIADAAQAWAAAAgcQAAgbgWAAIgDAAg");
	this.shape_20.setTransform(97.15,9.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgXBDIAAiEIAvAAIAACEg");
	this.shape_21.setTransform(87.625,9.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgsA0QgRgOABgZIAAhQIAwAAIAABRQAAAOAMAAQANAAAAgOIAAhRIAxAAIAABQQAAAZgQAOQgRAQgdAAQgcAAgQgQg");
	this.shape_22.setTransform(78.05,9.725);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgqBDIAAiEIAwAAIAABeIAlAAIAAAmg");
	this.shape_23.setTransform(67,9.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgoBDIAAiEIBRAAIAAAlIgiAAIAAALIAfAAIAAAiIgfAAIAAAMIAiAAIAAAmg");
	this.shape_24.setTransform(57.6,9.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgwAxQgTgTAAgeQAAgdATgTQAVgUAeAAQAkAAAVAWIgbAcQgLgJgNAAQgLAAgHAIQgIAHAAAMQAAANAHAIQAIAIALAAQAKAAAGgEIAAgFIgPAAIAAgfIA6AAIAAA6QgZAWgnAAQggAAgUgUg");
	this.shape_25.setTransform(45.825,9.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgXBDIAAhfIgaAAIAAglIBjAAIAAAlIgaAAIAABfg");
	this.shape_26.setTransform(30.125,9.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgpBDIAAiEIBTAAIAAAlIgjAAIAAALIAfAAIAAAiIgfAAIAAAMIAjAAIAAAmg");
	this.shape_27.setTransform(20.2,9.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AAOBDIAAgwIgbAAIAAAwIgxAAIAAiEIAxAAIAAAvIAbAAIAAgvIAwAAIAACEg");
	this.shape_28.setTransform(8.8,9.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,261.1,27);


(lib.endframe_video_line3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AgyBRIAAihIA5AAIAABzIAsAAIAAAug");
	this.shape.setTransform(209.025,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_1.setTransform(197.525,11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E90000").s().p("AgcBRIAAhzIggAAIAAguIB5AAIAAAuIggAAIAABzg");
	this.shape_2.setTransform(185.425,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E90000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape_3.setTransform(170.725,11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("AgcBRIAAihIA5AAIAAChg");
	this.shape_4.setTransform(158.875,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_5.setTransform(145.175,11.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AgcBRIAAihIA5AAIAAChg");
	this.shape_6.setTransform(135.875,11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AgcBRIAAhzIggAAIAAguIB5AAIAAAuIggAAIAABzg");
	this.shape_7.setTransform(125.875,11.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AAYBRIgGgZIglAAIgHAZIg6AAIA5ihIA4AAIA3ChgAAIAPIgIggIgJAgIARAAg");
	this.shape_8.setTransform(110.9,11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AAIBRIgWg1IAAA1Ig4AAIAAihIBGAAQAcABAQAPQAQAOAAAXQAAANgIAMQgHAJgKAGIAkBEgAgOgOIAHAAQAFABADgEQADgDAAgFQAAgEgDgEQgDgCgFAAIgHAAg");
	this.shape_9.setTransform(95.675,11.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_10.setTransform(82.175,11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E90000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape_11.setTransform(68.175,11.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_12.setTransform(54.225,11.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E90000").s().p("Ag7A8QgXgYABgkQAAgiAXgYQAYgYAmAAQArAAAaAaIghAiQgNgLgQAAQgNAAgKAJQgIAKgBAOQAAAQAJAJQAJAKAOAAQANAAAGgFIAAgFIgSAAIAAgnIBHAAIAABHQgeAbgxgBQgmAAgZgXg");
	this.shape_13.setTransform(39.8,11.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#E90000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_14.setTransform(21.275,11.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#E90000").s().p("AguBFQgRgPAAgZQAAgVAQgNQgNgLAAgSQAAgUAQgOQARgOAbAAQAcAAARAOQAQAOAAAUQAAASgNALQAQANAAAVQAAAZgRAPQgRAOgeAAQgdAAgRgOgAgGAUQgCAEAAAEQAAAOAIAAQAJAAAAgOQAAgEgCgEQgCgEgFAAQgEAAgCAEgAgGgeQAAALAGAAQAHAAAAgLQAAgLgHAAQgGAAAAALg");
	this.shape_15.setTransform(8.775,11.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_video_line3, new cjs.Rectangle(0,0,243.1,27), null);


(lib.endframe_video_line2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgcBRIAAhzIggAAIAAguIB5AAIAAAuIggAAIAABzg");
	this.shape.setTransform(178.675,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_1.setTransform(166.575,11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAqBRIAAhOIgQBCIgzAAIgQhCIAABOIg5AAIAAihIBJAAIAZBbIAZhbIBJAAIAAChg");
	this.shape_2.setTransform(150.35,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape_3.setTransform(127.125,11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_4.setTransform(113.175,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgcBRIAAhzIggAAIAAguIB5AAIAAAuIggAAIAABzg");
	this.shape_5.setTransform(101.075,11.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgtA9QgZgYAAglQAAgiAYgYQAYgYAmAAQAfAAAXAPIgSAvQgMgNgSAAQgOAAgIAJQgJAKAAAOQAAAPAJAKQAIAJAOAAQASAAANgNIASAuQgXARgggBQglAAgYgWg");
	this.shape_6.setTransform(87.225,11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("Ag2BAQgTgTAAgdIAAhhIA6AAIAABiQAAARAPAAQAQAAAAgRIAAhiIA6AAIAABhQAAAdgTATQgUASgjAAQgjAAgTgSg");
	this.shape_7.setTransform(71.875,11.35);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhJBRIAAihIA6AAQAnAAAZAWQAZAVAAAlQAAAlgZAVQgZAXgnAAgAgQAjIADAAQAbAAAAgjQAAghgbgBIgDAAg");
	this.shape_8.setTransform(56.225,11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AhAA8QgYgYAAgkQAAgjAYgYQAZgXAnAAQAoAAAZAXQAYAYAAAjQAAAkgYAYQgZAXgoAAQgnAAgZgXgAgUgZQgHAKAAAPQAAAQAHAKQAIAKAMAAQANAAAHgKQAIgKAAgQQAAgPgIgKQgHgKgNABQgMgBgIAKg");
	this.shape_9.setTransform(39.1,11.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAIBRIgWg1IAAA1Ig4AAIAAihIBGAAQAcABAQAPQAQAOAAAXQAAANgIAMQgHAJgKAGIAkBEgAgOgOIAHAAQAFABADgEQADgDAAgFQAAgEgDgEQgDgCgFAAIgHAAg");
	this.shape_10.setTransform(23.325,11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("Ag+BRIAAihIBCAAQAbAAAQAQQARAPgBAYQAAAZgQAOQgQAQgaAAIgMAAIAAAzgAgHgLIAGAAQAEgBAEgDQADgDAAgGQAAgFgDgEQgDgCgFAAIgGAAg");
	this.shape_11.setTransform(9.05,11.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_video_line2, new cjs.Rectangle(0,0,244.1,27), null);


(lib.endframe_video_line1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape.setTransform(168.475,11.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhEBRIAAgeIA9hVIg7AAIAAguICGAAIAAAeIg9BVIA+AAIAAAug");
	this.shape_1.setTransform(155.575,11.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AARBRIgihMIAABMIg6AAIAAihIA7AAIAiBMIAAhMIA6AAIAAChg");
	this.shape_2.setTransform(140.125,11.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhAA8QgYgYAAgkQAAgjAYgYQAZgXAnAAQAoAAAZAXQAYAYAAAjQAAAkgYAYQgZAXgoAAQgnAAgZgXgAgUgZQgHAKAAAPQAAAQAHAKQAIAKAMAAQANAAAHgKQAIgKAAgQQAAgPgIgKQgHgKgNABQgMgBgIAKg");
	this.shape_3.setTransform(122.8,11.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgyBRIAAihIA5AAIAABzIAsAAIAAAug");
	this.shape_4.setTransform(103.925,11.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAYBRIgHgZIgkAAIgIAZIg5AAIA4ihIA5AAIA4ChgAAHAPIgIggIgJAgIARAAg");
	this.shape_5.setTransform(89.55,11.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AALBRIgbhDIAABDIg6AAIAAihIA6AAIAAA7IAag7IBAAAIgnBOIAoBTg");
	this.shape_6.setTransform(68.8,11.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgqBNIAGgsIAMACQAKAAAAgJIAAhqIA6AAIAABzQAAAug2AAQgQAAgQgEg");
	this.shape_7.setTransform(55.55,11.275);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgcBRIAAihIA5AAIAAChg");
	this.shape_8.setTransform(47.375,11.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AALBRIgbhDIAABDIg6AAIAAihIA6AAIAAA7IAag7IBAAAIgnBOIAoBTg");
	this.shape_9.setTransform(35.95,11.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgxBRIAAihIBjAAIAAAuIgqAAIAAANIAmAAIAAAqIgmAAIAAAOIAqAAIAAAug");
	this.shape_10.setTransform(22.025,11.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AhBBRIAAihIBEAAQAcAAAQANQAQAMAAAVQAAAUgRAMQAJADAGAKQAFAJAAAKQAAAYgQANQgQANgbABgAgJAlIAJAAQAEAAAEgDQADgDAAgEQAAgFgDgDQgEgEgEABIgJAAgAgJgSIAJAAQAJgBAAgIQAAgKgJABIgJAAg");
	this.shape_11.setTransform(9.325,11.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_video_line1, new cjs.Rectangle(0,0,225,27), null);


(lib.endframe_static_line4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape.setTransform(363.975,15.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_1.setTransform(343.9,15.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAMBzIgghKIAABKIhRAAIAAjlIBlAAQAoAAAYAVQAWAVAAAhQAAASgLARQgKAOgPAJIA0BggAgUgUIAKAAQAHAAAEgEQAEgFAAgHQAAgGgEgFQgEgEgHAAIgKAAg");
	this.shape_2.setTransform(326.075,15.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAjBzIgKgjIg1AAIgKAjIhSAAIBQjlIBRAAIBQDlgAALAWIgMgvIgNAvIAZAAg");
	this.shape_3.setTransform(302.575,15.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgqBzIhTjlIBbAAIAjCGIAliGIBYAAIhUDlg");
	this.shape_4.setTransform(277.5,15.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAMBzIgghKIAABKIhRAAIAAjlIBlAAQAoAAAYAVQAWAVAAAhQAAASgLARQgKAOgPAJIA0BggAgUgUIAKAAQAHAAAEgEQAEgFAAgHQAAgGgEgFQgEgEgHAAIgKAAg");
	this.shape_5.setTransform(255.025,15.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_6.setTransform(235.6,15.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_7.setTransform(212.85,15.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgpBzIAAikIgtAAIAAhBICtAAIAABBIgtAAIAACkg");
	this.shape_8.setTransform(195.475,15.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AhpBzIAAjlIBTAAQA5AAAjAeQAkAfAAA1QAAA1gkAfQgjAgg5gBgAgYAyIAFAAQAnAAAAgyQAAgwgnAAIgFAAg");
	this.shape_9.setTransform(169.075,15.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgpBzIAAjlIBTAAIAADlg");
	this.shape_10.setTransform(152.35,15.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AhOBbQgbgaAAgqIAAiMIBTAAIAACNQAAAZAWAAQAWAAAAgZIAAiNIBUAAIAACMQAAAqgbAaQgdAbgyAAQgyAAgcgbg");
	this.shape_11.setTransform(135.55,15.425);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AhIBzIAAjlIBSAAIAAClIA/AAIAABAg");
	this.shape_12.setTransform(116.15,15.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg9AAIAAATIA2AAIAAA8Ig2AAIAAAVIA9AAIAABAg");
	this.shape_13.setTransform(99.6,15.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhUBWQgigiAAg0QAAgyAjgiQAjgjA2AAQA+AAAkAmIguAyQgTgRgYAAQgTAAgNANQgNAPAAAUQAAAXAMANQANAOAUAAQATAAAKgGIAAgJIgaAAIAAg3IBlAAIAABmQgsAmhEAAQg5AAgigig");
	this.shape_14.setTransform(78.875,15.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgpBzIAAikIgtAAIAAhBICtAAIAABBIgtAAIAACkg");
	this.shape_15.setTransform(51.225,15.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg9AAIAAATIA2AAIAAA8Ig2AAIAAAVIA9AAIAABAg");
	this.shape_16.setTransform(33.8,15.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAYBzIAAhTIgvAAIAABTIhUAAIAAjlIBUAAIAABTIAvAAIAAhTIBUAAIAADlg");
	this.shape_17.setTransform(13.825,15.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,378,37);


(lib.endframe_static_line3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape.setTransform(368.025,15.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_1.setTransform(347.95,15.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAMBzIgghKIAABKIhRAAIAAjlIBlAAQAoAAAYAVQAWAVAAAhQAAASgLARQgKAOgPAJIA0BggAgUgUIAKAAQAHAAAEgEQAEgFAAgHQAAgGgEgFQgEgEgHAAIgKAAg");
	this.shape_2.setTransform(330.125,15.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAjBzIgKgjIg1AAIgKAjIhSAAIBQjlIBRAAIBQDlgAALAWIgMgvIgNAvIAZAAg");
	this.shape_3.setTransform(306.625,15.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgqBzIhTjlIBbAAIAjCGIAliGIBYAAIhUDlg");
	this.shape_4.setTransform(281.55,15.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAMBzIgghKIAABKIhRAAIAAjlIBlAAQAoAAAYAVQAWAVAAAhQAAASgLARQgKAOgPAJIA0BggAgUgUIAKAAQAHAAAEgEQAEgFAAgHQAAgGgEgFQgEgEgHAAIgKAAg");
	this.shape_5.setTransform(259.075,15.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_6.setTransform(239.65,15.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_7.setTransform(216.9,15.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgpBzIAAikIgtAAIAAhBICtAAIAABBIgtAAIAACkg");
	this.shape_8.setTransform(199.525,15.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Ag+BiQgUgPgIgYIA8ggQAOAZAQAAQAGAAAAgGQAAgGgNgIIgfgPQgtgWAAgpQAAghAYgUQAYgUAmAAQAjAAAaAUQAVAQAGASIg7AjQgKgWgPABQgHgBAAAGQAAAHAOAIIAfAPQAtAYAAAlQAAAigYAUQgYAVgqAAQgkAAgagWg");
	this.shape_9.setTransform(174.425,15.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgnAiQASgLAHgPQgbgJAAgaQAAgRAMgKQAMgMARAAQASAAAMALQAMALgBAUQAAAZgNAZQgNAZgYAQg");
	this.shape_10.setTransform(159.4,8.75);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgqBzIhTjlIBaAAIAkCGIAliGIBYAAIhUDlg");
	this.shape_11.setTransform(141,15.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgpBzIAAikIgtAAIAAhBICtAAIAABBIgtAAIAACkg");
	this.shape_12.setTransform(118.975,15.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_13.setTransform(95.2,15.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhpBzIAAjlIBTAAQA5AAAjAeQAkAfAAA1QAAA1gkAfQgjAgg5gBgAgYAyIAFAAQAnAAAAgyQAAgwgnAAIgFAAg");
	this.shape_14.setTransform(76.175,15.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AA8BzIAAhuIgXBdIhJAAIgXhdIAABuIhRAAIAAjlIBpAAIAjCCIAkiCIBpAAIAADlg");
	this.shape_15.setTransform(43.175,15.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AhcBVQgjghAAg0QAAgyAjgiQAjgjA5AAQA6AAAkAjQAiAiAAAyQAAA0giAhQgkAjg6AAQg5AAgjgjgAgdgkQgKAOAAAWQAAAWAKAOQALAPASAAQATAAALgPQAKgOAAgWQAAgWgKgOQgLgOgTAAQgSAAgLAOg");
	this.shape_16.setTransform(15.025,15.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_static_line3, new cjs.Rectangle(0,0,414,37), null);


(lib.endframe_static_line2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag+BiQgUgPgIgYIA8ggQAOAZAQAAQAGAAAAgGQAAgGgNgIIgfgPQgtgWAAgpQAAghAYgUQAYgUAmAAQAjAAAaAUQAVAQAGASIg7AjQgKgWgPABQgHgBAAAGQAAAHAOAIIAfAPQAtAYAAAlQAAAigYAUQgYAVgqAAQgkAAgagWg");
	this.shape.setTransform(372.325,15.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhIBzIAAjlIBSAAIAAClIA/AAIAABAg");
	this.shape_1.setTransform(355.1,15.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg9AAIAAATIA2AAIAAA8Ig2AAIAAAVIA9AAIAABAg");
	this.shape_2.setTransform(338.55,15.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAPBzIgmheIAABeIhUAAIAAjlIBUAAIAABUIAmhUIBbAAIg5BuIA8B3g");
	this.shape_3.setTransform(319.15,15.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape_4.setTransform(295.425,15.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgpBzIAAjlIBTAAIAADlg");
	this.shape_5.setTransform(278.4,15.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAcBzIgch3IgbB3IhUAAIg8jlIBZAAIAVB+IAdh+IBBAAIAcB+IAWh+IBZAAIg8Dlg");
	this.shape_6.setTransform(255.775,15.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_7.setTransform(223.75,15.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhhBzIAAgrIBWh6IhUAAIAAhAIDAAAIAAAqIhXB7IBZAAIAABAg");
	this.shape_8.setTransform(205.3,15.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape_9.setTransform(183.125,15.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AhcBVQgjghAAg0QAAgyAjgiQAjgjA5AAQA6AAAkAjQAiAiAAAyQAAA0giAhQgkAjg6AAQg5AAgjgjgAgdgkQgKAOAAAWQAAAWAKAOQALAPASAAQATAAALgPQAKgOAAgWQAAgWgKgOQgLgOgTAAQgSAAgLAOg");
	this.shape_10.setTransform(158.225,15.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape_11.setTransform(126.975,15.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAjBzIgKgjIg1AAIgKAjIhSAAIBQjlIBRAAIBQDlgAALAWIgMgvIgNAvIAZAAg");
	this.shape_12.setTransform(102.825,15.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgpBzIhUjlIBaAAIAkCGIAliGIBYAAIhUDlg");
	this.shape_13.setTransform(77.75,15.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape_14.setTransform(46.675,15.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AhHCRIAAjlICPAAIAABBIg9AAIAAATIA2AAIAAA8Ig2AAIAAAVIA9AAIAABAgAgkhdIAWgzIBNAAIgnAzg");
	this.shape_15.setTransform(26.6,12.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AhHCRIAAjlICPAAIAABBIg9AAIAAATIA2AAIAAA8Ig2AAIAAAVIA9AAIAABAgAgkhdIAWgzIBOAAIgoAzg");
	this.shape_16.setTransform(10.2,12.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_static_line2, new cjs.Rectangle(0,0,426,37), null);


(lib.endframe_static_line1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E31E26").s().p("Ag+BiQgUgPgIgYIA8ggQAOAZAQAAQAGAAAAgGQAAgGgNgIIgfgPQgtgWAAgpQAAghAYgUQAYgUAmAAQAjAAAaAUQAVAQAGASIg7AjQgKgWgPABQgHgBAAAGQAAAHAOAIIAfAPQAtAYAAAlQAAAigYAUQgYAVgqAAQgkAAgagWg");
	this.shape.setTransform(470.375,15.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E31E26").s().p("AhUBWQgigiAAg0QAAgyAjgiQAjgjA2AAQA+AAAkAmIguAyQgTgRgYAAQgTAAgNANQgNAPAAAUQAAAXAMANQANAOAUAAQATAAAKgGIAAgJIgaAAIAAg3IBlAAIAABmQgsAmhEAAQg5AAgigig");
	this.shape_1.setTransform(448.375,15.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E31E26").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape_2.setTransform(424.375,15.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E31E26").s().p("AAjBzIgKgjIg1AAIgKAjIhSAAIBQjlIBRAAIBQDlgAALAWIgMgvIgNAvIAZAAg");
	this.shape_3.setTransform(400.225,15.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E31E26").s().p("AhIBzIAAjlIBSAAIAAClIA/AAIAABAg");
	this.shape_4.setTransform(380.2,15.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E31E26").s().p("AA8BzIAAhuIgXBdIhJAAIgXhdIAABuIhRAAIAAjlIBpAAIAjCCIAkiCIBpAAIAADlg");
	this.shape_5.setTransform(350.425,15.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E31E26").s().p("AhcBVQgjghAAg0QAAgyAjgiQAjgjA5AAQA6AAAkAjQAiAiAAAyQAAA0giAhQgkAjg6AAQg5AAgjgjgAgdgkQgKAOAAAWQAAAWAKAOQALAPASAAQATAAALgPQAKgOAAgWQAAgWgKgOQgLgOgTAAQgSAAgLAOg");
	this.shape_6.setTransform(322.275,15.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E31E26").s().p("AAQBzIgoheIAABeIhUAAIAAjlIBUAAIAABUIAnhUIBbAAIg5BuIA7B3g");
	this.shape_7.setTransform(298.05,15.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AhGBzIAAjlICOAAIAABBIg8AAIAAAbIA2AAIAAA8Ig2AAIAABNg");
	this.shape_8.setTransform(272.1,15.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AhcBVQgjghAAg0QAAgyAjgiQAjgjA5AAQA6AAAkAjQAiAiAAAyQAAA0giAhQgkAjg6AAQg5AAgjgjgAgdgkQgKAOAAAWQAAAWAKAOQALAPASAAQATAAALgPQAKgOAAgWQAAgWgKgOQgLgOgTAAQgSAAgLAOg");
	this.shape_9.setTransform(250.925,15.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg9AAIAAATIA2AAIAAA8Ig2AAIAAAVIA9AAIAABAg");
	this.shape_10.setTransform(223.35,15.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape_11.setTransform(203.275,15.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgpBzIAAjlIBTAAIAADlg");
	this.shape_12.setTransform(186.25,15.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AhIBzIAAjlIBSAAIAAClIA/AAIAABAg");
	this.shape_13.setTransform(173.3,15.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAYBzIgxhsIAABsIhTAAIAAjlIBVAAIAxBsIAAhsIBTAAIAADlg");
	this.shape_14.setTransform(153.075,15.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AhcBVQgjghAAg0QAAgyAjgiQAjgjA5AAQA6AAAkAjQAiAiAAAyQAAA0giAhQgkAjg6AAQg5AAgjgjgAgdgkQgKAOAAAWQAAAWAKAOQALAPASAAQATAAALgPQAKgOAAgWQAAgWgKgOQgLgOgTAAQgSAAgLAOg");
	this.shape_15.setTransform(128.175,15.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AhJBzIAAjlIBTAAIAAClIA/AAIAABAg");
	this.shape_16.setTransform(101.05,15.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_17.setTransform(84.5,15.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgpBzIAAikIgtAAIAAhBICtAAIAABBIgtAAIAACkg");
	this.shape_18.setTransform(67.125,15.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("Ag+BiQgUgPgIgYIA8ggQAOAZAQAAQAGAAAAgGQAAgGgNgIIgfgPQgtgWAAgpQAAghAYgUQAYgUAmAAQAjAAAaAUQAVAQAGASIg7AjQgKgWgPABQgHgBAAAGQAAAHAOAIIAfAPQAtAYAAAlQAAAigYAUQgYAVgqAAQgkAAgagWg");
	this.shape_19.setTransform(48.375,15.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AhHBzIAAjlICPAAIAABBIg8AAIAAATIA2AAIAAA8Ig2AAIAAAVIA8AAIAABAg");
	this.shape_20.setTransform(30.7,15.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AheBzIAAjlIBjAAQAngBAYATQAXARgBAeQABAdgYAQQANAGAHANQAIAOAAAOQAAAigYAUQgWASgnAAgAgNA1IANAAQAGABAFgFQAEgEAAgHQAAgGgEgFQgFgEgGAAIgNAAgAgNgbIANAAQANAAAAgNQAAgNgNAAIgNAAg");
	this.shape_21.setTransform(12.5,15.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_static_line1, new cjs.Rectangle(0,0,503,37), null);


(lib.emptyMovieClip = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.dollar_text_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgaBJIAAiRIA1AAIAACRg");
	this.shape.setTransform(491.35,10.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_1.setTransform(480.525,10.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgyA6QgRgRAAgbIAAhYIA1AAIAABZQAAAQAOAAQAOAAAAgQIAAhZIA2AAIAABYQAAAbgSARQgSARggAAQggAAgSgRg");
	this.shape_2.setTransform(465.575,10.55);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgnBGIAGgoIALACQAJAAAAgIIAAhhIA1AAIAABpQAAAqgxAAQgPAAgPgEg");
	this.shape_3.setTransform(453.6,10.45);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("Ag2BLIAAgTQAAgOAJgNQAHgKANgMIAVgQQAIgJAAgIQAAgGgGAAQgGAAgDAMIgtgJQAEgTAMgMQAPgOAZAAQAZAAAPAMQAQANAAAWQAAAWgYATIgRAMQgHAGAAADIAxAAIAAAog");
	this.shape_4.setTransform(439.275,10.275);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("Ag1A2QgWgVAAghQAAggAXgVQAWgWAiAAQAnAAAYAXIgeAgQgMgKgPAAQgMAAgJAIQgHAJgBANQABAOAHAJQAJAJAMAAQAMAAAGgEIAAgGIgQAAIAAgiIBAAAIAABAQgcAYgsAAQgjAAgWgWg");
	this.shape_5.setTransform(421.2,10.425);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAXBJIgHgWIgiAAIgGAWIg0AAIAziRIAzAAIAzCRgAAHAOIgHgeIgJAeIAQAAg");
	this.shape_6.setTransform(405.7,10.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AhCBJIAAiRIA0AAQAkgBAWAUQAXATAAAiQAAAigWAUQgXATgkAAgAgPAgIADAAQAZAAAAggQAAgfgZABIgDAAg");
	this.shape_7.setTransform(391.025,10.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_8.setTransform(376.125,10.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("Ag7A2QgVgVAAghQAAggAVgVQAXgWAkAAQAlAAAWAWQAXAVAAAgQAAAhgXAVQgWAWglAAQgkAAgXgWgAgSgWQgGAIAAAOQAAAOAGAJQAHAJALAAQAMAAAHgJQAHgJAAgOQAAgOgHgIQgHgKgMAAQgLAAgHAKg");
	this.shape_9.setTransform(360.3,10.425);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("Ag+BJIAAgbIA3hOIg2AAIAAgoIB7AAIAAAbIg4BNIA5AAIAAApg");
	this.shape_10.setTransform(345.475,10.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAmBJIAAhHIgPA8IguAAIgOg8IAABHIg0AAIAAiRIBDAAIAWBSIAXhSIBDAAIAACRg");
	this.shape_11.setTransform(325.275,10.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgzBaIA9izIAqAAIg+Czg");
	this.shape_12.setTransform(310.6,11);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgaBJIAAhoIgdAAIAAgpIBvAAIAAApIgeAAIAABog");
	this.shape_13.setTransform(299.6,10.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgaBJIAAiRIA0AAIAACRg");
	this.shape_14.setTransform(286.45,10.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_15.setTransform(277.975,10.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAmBJIAAhHIgPA8IguAAIgOg8IAABHIg0AAIAAiRIBDAAIAWBSIAXhSIBDAAIAACRg");
	this.shape_16.setTransform(263.175,10.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgBBJIAAgdIg9AAIAAgeIA2hWIA4AAIAABOIAPAAIAAAmIgPAAIAAAdgAgRAGIAQAAIAAgbg");
	this.shape_17.setTransform(242.95,10.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("Ag2BLIAAgTQAAgOAJgNQAHgKANgMIAVgQQAIgJAAgIQAAgGgGAAQgGAAgDAMIgtgJQAEgTAMgMQAPgOAZAAQAZAAAPAMQAQANAAAWQAAAWgYATIgRAMQgHAGAAADIAxAAIAAAog");
	this.shape_18.setTransform(230.325,10.275);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("Ag1A2QgVgVgBghQAAggAXgVQAWgWAiAAQAoAAAWAXIgdAgQgMgKgPAAQgMAAgIAIQgIAJAAANQAAAOAIAJQAHAJANAAQAMAAAGgEIAAgGIgRAAIAAgiIBAAAIAABAQgbAYgsAAQgjAAgWgWg");
	this.shape_19.setTransform(212.25,10.425);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAWBJIgGgWIghAAIgHAWIg0AAIAziRIA0AAIAyCRgAAHAOIgHgeIgIAeIAPAAg");
	this.shape_20.setTransform(196.75,10.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AhCBJIAAiRIA0AAQAkgBAWAUQAXATAAAiQAAAigWAUQgXATgkAAgAgPAgIADAAQAZAAAAggQAAgfgZABIgDAAg");
	this.shape_21.setTransform(182.075,10.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgmBGIAEgoIAMACQAKAAAAgIIAAhhIAzAAIAABpQAAAqgwAAQgPAAgOgEg");
	this.shape_22.setTransform(170.15,10.45);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgaBJIAAiRIA0AAIAACRg");
	this.shape_23.setTransform(162.7,10.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAIBJIgVgvIAAAvIgzAAIAAiRIBAAAQAZgBAPAOQAOANABAVQAAAMgIALQgFAIgLAGIAiA9gAgNgMIAHAAQAFAAACgEQACgCAAgFQAAgEgCgCQgCgEgFAAIgHAAg");
	this.shape_24.setTransform(153.3,10.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgaBJIg1iRIA6AAIAWBUIAXhUIA4AAIg1CRg");
	this.shape_25.setTransform(138.025,10.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_26.setTransform(118.275,10.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AAWBJIgGgWIghAAIgHAWIg0AAIAziRIA0AAIAyCRgAAHAOIgHgeIgIAeIAPAAg");
	this.shape_27.setTransform(102.9,10.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgaBJIg1iRIA6AAIAWBUIAXhUIA4AAIg1CRg");
	this.shape_28.setTransform(86.975,10.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("Ag1A2QgVgVgBghQAAggAXgVQAWgWAiAAQAnAAAXAXIgdAgQgNgKgOAAQgMAAgJAIQgHAJAAANQAAAOAHAJQAIAJANAAQAMAAAGgEIAAgGIgRAAIAAgiIBAAAIAABAQgbAYgsAAQgjAAgWgWg");
	this.shape_29.setTransform(66.8,10.425);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgaBJIAAiRIA0AAIAACRg");
	this.shape_30.setTransform(55.85,10.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AhCBJIAAiRIA0AAQAkgBAWAUQAXATAAAiQAAAigWAUQgXATgkAAgAgPAgIADAAQAZAAAAggQAAgfgZABIgDAAg");
	this.shape_31.setTransform(45.675,10.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_32.setTransform(33.375,10.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_33.setTransform(22.875,10.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("Ag1A2QgWgVABghQAAggAVgVQAXgWAiAAQAnAAAYAXIgeAgQgNgKgPAAQgLAAgIAIQgJAJAAANQAAAOAJAJQAIAJAMAAQALAAAHgEIAAgGIgQAAIAAgiIBAAAIAABAQgcAYgrAAQgkAAgWgWg");
	this.shape_34.setTransform(9.7,10.425);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_3, new cjs.Rectangle(0,0,496.8,25), null);


(lib.dollar_text_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AivDBIAAmAICJAAQBhAAA6AyQA7A0AABZQAABYg7A1Qg6A0hhAAgAgoBSIAIAAQBCAAAAhSQAAhShCAAIgIAAg");
	this.shape.setTransform(465.625,24);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("Ah6DBIAAmAICLAAIAAEUIBqAAIAABsg");
	this.shape_1.setTransform(433.425,24);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E90000").s().p("Ag7ClQg0ghgTg6IgpAAIAAhCIAaAAIAAgOIgaAAIAAhFIAqAAQAVg1AxgiQA1glBAAAQBBAAAuAbIgtBbQgbgOgfAAQggAAgZAUIBZAAIgIBFIhnAAIAAAHIAAAHIBlAAIgFBCIhGAAQARAUAnAAQAhgBAbgRIAqBaQglAihFAAQhHAAg1gjg");
	this.shape_2.setTransform(399.95,24);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E90000").s().p("AiNCOQg4g4AAhWQAAhUA5g5QA7g6BaAAQBoAAA8A/IhNBUQgggdgnAAQggAAgWAXQgWAXAAAjQAAAlAVAXQAVAXAiAAQAfAAARgKIAAgPIgsAAIAAhcICpAAIAACqQhJBAhyAAQheAAg6g6g");
	this.shape_3.setTransform(360.875,24.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("Ah3DBIAAmAIDvAAIAABsIhlAAIAAAgIBaAAIAABlIhaAAIAAAjIBlAAIAABsg");
	this.shape_4.setTransform(327,24);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AhFDBIAAmAICLAAIAAGAg");
	this.shape_5.setTransform(304.75,24);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AhFDBIAAkUIhLAAIAAhsIEhAAIAABsIhMAAIAAEUg");
	this.shape_6.setTransform(280.825,24);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AAoDBIhTi2IAAC2IiJAAIAAmAICNAAIBTC1IAAi1ICJAAIAAGAg");
	this.shape_7.setTransform(245.65,24);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AA7DBIgRg8IhZAAIgSA8IiIAAICFmAICJAAICFGAgAASAjIgUhOIgWBOIAqAAg");
	this.shape_8.setTransform(205.4,24);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AAZDBIhBifIAACfIiMAAIAAmAICMAAIAACLIBAiLICZAAIhfC3IBjDJg");
	this.shape_9.setTransform(166.3,24);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AA7DBIgRg8IhZAAIgSA8IiIAAICFmAICJAAICFGAgAASAjIgUhOIgWBOIAqAAg");
	this.shape_10.setTransform(126.1,24);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E90000").s().p("AhGDBIiLmAICXAAIA8DgIA9jgICTAAIiMGAg");
	this.shape_11.setTransform(84.3,24);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("Ah3DBIAAmAIDvAAIAABsIhlAAIAAAgIBaAAIAABlIhaAAIAAAjIBlAAIAABsg");
	this.shape_12.setTransform(38.65,24);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E90000").s().p("AhnC4IAOhoQAPADAOAAQAZAAAAgUIAAkAICLAAIAAEVQAABuiBAAQgnAAgngKg");
	this.shape_13.setTransform(13,24.15);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_2, new cjs.Rectangle(0,0,485.9,59), null);


(lib.dollar_text_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AhFDBIAAkUIhLAAIAAhsIEhAAIAABsIhMAAIAAEUg");
	this.shape.setTransform(396.225,24);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhFDBIAAmAICLAAIAAGAg");
	this.shape_1.setTransform(372.25,24);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AiDCYQgugrAAhHIAAjpICNAAIAADsQgBAoAlAAQAmAAAAgoIAAjsICMAAIAADpQgBBHgtArQgwAshUAAQhUAAgvgsg");
	this.shape_2.setTransform(344.2,24.375);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AhoCkQghgbgOgmIBkg2QAYApAbAAQAKAAAAgJQAAgLgXgMIgzgZQhLgnAAhCQAAg4AogiQAnghBAAAQA8AAArAhQAjAaAKAgIhiA4QgRgjgZAAQgMAAAAAKQAAALAYANIAzAZQBMAoAAA/QAAA5gpAiQgpAihEAAQg9AAgsgkg");
	this.shape_3.setTransform(298.275,24.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("Ah3DBIAAmAIDvAAIAABsIhkAAIAAAgIBZAAIAABlIhZAAIAAAjIBkAAIAABsg");
	this.shape_4.setTransform(268.85,24);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("Ah6DBIAAmAICLAAIAAEUIBqAAIAABsg");
	this.shape_5.setTransform(242.275,24);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("Ah6DBIAAmAICLAAIAAEUIBqAAIAABsg");
	this.shape_6.setTransform(215.425,24);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AA7DBIgRg8IhZAAIgRA8IiJAAICGmAICIAAICFGAgAASAjIgUhOIgWBOIAqAAg");
	this.shape_7.setTransform(181.05,24);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("Ah6DBIAAmAICLAAIAAEUIBqAAIAABsg");
	this.shape_8.setTransform(137.075,24);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AA7DBIgRg8IhZAAIgSA8IiIAAICFmAICJAAICFGAgAASAjIgUhOIgWBOIAqAAg");
	this.shape_9.setTransform(102.7,24);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AA7DBIgRg8IhZAAIgRA8IiJAAICGmAICIAAICFGAgAASAjIgUhOIgWBOIAqAAg");
	this.shape_10.setTransform(61.8,24);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAoDBIAAiLIhQAAIAACLIiLAAIAAmAICLAAIAACKIBQAAIAAiKICNAAIAAGAg");
	this.shape_11.setTransform(21.7,24);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_1, new cjs.Rectangle(0,0,413.7,59), null);


(lib.dollar_prijs = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgHAcIAAgOIgMAHIgIgPIALgGIgLgFIAIgPIAMAHIAAgNIAPAAIAAANIAMgHIAIAPIgMAFIAMAGIgIAPIgMgHIAAAOg");
	this.shape.setTransform(5.275,5.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgVALIAAgVIArAAIAAAVg");
	this.shape_1.setTransform(38.625,36.675);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgJALQgFgFAAgGQAAgFAFgFQAEgEAFAAQAGAAAEAEQAFAFAAAFQAAAGgFAFQgEAEgGAAQgFAAgEgEg");
	this.shape_2.setTransform(33.8,39.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAFA0IAAgXIgtAAIAAgSIAog+IAeAAIAAA6IALAAIAAAWIgLAAIAAAXgAgOAHIATAAIAAgfg");
	this.shape_3.setTransform(27.35,36);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAFA0IAAgXIgtAAIAAgSIAog+IAeAAIAAA6IALAAIAAAWIgLAAIAAAXgAgOAHIATAAIAAgfg");
	this.shape_4.setTransform(18.35,36);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgYA0IAfhRIgqAAIAAgWIBHAAIAAASIghBVg");
	this.shape_5.setTransform(9.925,36);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgKAkQgEgFAAgGQAAgGAEgEQAFgFAFAAQAGAAAFAFQAEAEAAAGQAAAGgEAFQgFAEgGAAQgFAAgFgEgAgKgOQgEgEAAgGQAAgHAEgEQAFgEAFAAQAGAAAFAEQAEAEAAAHQAAAGgEAEQgFAEgGAAQgFAAgFgEg");
	this.shape_6.setTransform(71.1,23.425);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAJA2IgVglIAAAlIgZAAIAAhrIAZAAIAAA8IAUgeIAcAAIgZAiIAbArg");
	this.shape_7.setTransform(65.325,21.825);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgUAeQgMgMAAgSQAAgRAMgMQANgLARAAQANAAAKAHIgGAVQgIgGgIAAQgJAAgEAGQgFAFAAAHQAAAIAFAGQAFAFAJAAQAHAAAIgFIAGAUQgLAHgMAAQgSAAgMgLg");
	this.shape_8.setTransform(57.25,23.325);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgZAiQgJgHAAgMQgBgMAJgGQAIgHAOAAQAHAAAIADIAAgDQAAgJgMAAQgJAAgLAFIgJgRQAPgJARAAQASAAAJAJQAGAHAAAPIAAAwIgWAAIgBgEQgHAGgKAAQgNAAgHgHgAgGAIQgEACAAAEQAAAEAEACQADACADAAQALAAAAgIQAAgEgEgCQgDgDgEAAQgDAAgDADg");
	this.shape_9.setTransform(49.5,23.325);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgKA0IgHgFIAAAGIgYAAIAAhrIAZAAIAAAkIAHgFQAHgDAGAAQARAAALALQAKALAAASQAAASgLAMQgLALgRAAQgGAAgHgDgAgLAAQgFAFAAAJQAAAIAEAGQAFAFAHAAQAHAAAFgFQAFgFAAgJQAAgJgFgFQgFgEgHAAQgHAAgEAEg");
	this.shape_10.setTransform(41.175,21.925);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AALA2IAAgvQAAgKgKAAQgEAAgEADQgDACAAAFIAAAvIgZAAIAAhrIAZAAIAAAmQAHgKAMAAQAQAAAHAMQAEAIAAAQIAAArg");
	this.shape_11.setTransform(32.075,21.825);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgTAlQgHgEgEgFIAOgOQAIAHAIAAQAHAAAAgEQAAgDgFgCIgMgFQgSgHAAgQQAAgLAJgHQAJgGAMAAQASAAAKANIgQANQgGgGgHAAQgFAAAAAEQAAADAFACIAMAEQASAHAAAPQAAAMgJAHQgJAHgNAAQgKAAgJgEg");
	this.shape_12.setTransform(24.475,23.325);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgaAiQgIgHgBgMQAAgMAJgGQAIgHAOAAQAHAAAIADIAAgDQAAgJgMAAQgKAAgKAFIgJgRQAPgJARAAQASAAAJAJQAGAHAAAPIAAAwIgWAAIgBgEQgGAGgLAAQgMAAgJgHgAgGAIQgEACAAAEQAAAEAEACQADACADAAQALAAAAgIQgBgEgDgCQgDgDgEAAQgDAAgDADg");
	this.shape_13.setTransform(17,23.325);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgUAeQgMgMAAgSQAAgRANgMQALgLASAAQANAAAKAHIgHAVQgGgGgKAAQgIAAgEAGQgFAFAAAHQAAAIAGAGQAEAFAIAAQAJAAAGgFIAHAUQgKAHgNAAQgSAAgMgLg");
	this.shape_14.setTransform(9.6,23.325);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgaAiQgIgHgBgMQAAgMAJgGQAIgHAOAAQAHAAAIADIAAgDQAAgJgMAAQgKAAgKAFIgJgRQAPgJARAAQASAAAJAJQAGAHAAAPIAAAwIgWAAIgBgEQgHAGgKAAQgMAAgJgHgAgGAIQgEACAAAEQAAAEAEACQADACADAAQALAAAAgIQgBgEgDgCQgDgDgEAAQgDAAgDADg");
	this.shape_15.setTransform(53.45,9.325);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AALAoIAAguQAAgLgLAAQgEAAgDADQgDADAAAFIAAAuIgZAAIAAhNIAZAAIAAAIQAGgKANAAQAQAAAGAMQAFAIAAARIAAAqg");
	this.shape_16.setTransform(45.375,9.225);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgTAlQgHgEgEgFIAOgOQAIAHAIAAQAHAAAAgEQAAgDgFgCIgMgFQgSgHAAgQQAAgLAJgHQAJgGAMAAQASAAAKANIgQANQgGgGgHAAQgFAAAAAEQAAADAFACIAMAEQASAHAAAPQAAAMgJAHQgJAHgNAAQgKAAgJgEg");
	this.shape_17.setTransform(34.875,9.325);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgTBFIAAgVIAEAAQAJAAABgIIAAhNIAYAAIAABOQgBAdgfAAIgGgBgAgDguQgEgDAAgGQAAgGAEgEQADgEAGAAQAGAAAEAEQAEAEAAAGQAAAFgEAEQgEAEgGAAQgGAAgDgEg");
	this.shape_18.setTransform(29,9.175);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgLA3IAAhNIAXAAIAABNgAgJgfQgEgEAAgGQAAgGAEgEQAEgDAFAAQAGAAAEADQAEAEAAAGQAAAGgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_19.setTransform(25.7,7.725);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgWAoIAAhNIAYAAIAAAKQAGgMAPAAIAAAZIgEAAQgRAAAAATIAAAjg");
	this.shape_20.setTransform(21.225,9.225);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgpA2IAAhpIAYAAIAAAGQACgDAFgCQAHgDAGAAQARAAALALQALAMAAASQAAARgKAMQgLALgRAAQgGAAgHgDIgHgFIAAAigAgMgaQgEAFAAAJQAAAJAFAEQAEAFAHAAQAHAAAFgFQAFgFAAgIQAAgJgFgFQgFgGgHAAQgHAAgFAGg");
	this.shape_21.setTransform(13.775,10.625);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_prijs, new cjs.Rectangle(0,0,75.3,47), null);


(lib.dollar_cashback_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAEAmIgKgZIAAAZIgbAAIAAhLIAhAAQANAAAIAGQAIAIgBAKQAAAHgDAGQgEADgFAEIASAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape.setTransform(11.05,9.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAMAmIgDgLIgRAAIgEALIgcAAIAbhLIAaAAIAbBLgAADAHIgDgOIgEAOIAHAAg");
	this.shape_1.setTransform(3.25,9.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AALAmIgDgLIgRAAIgDALIgbAAIAahLIAbAAIAbBLgAADAHIgDgOIgEAOIAHAAg");
	this.shape_2.setTransform(-4.9,9.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgfAmIAAhLIAgAAQAOAAAHAFQAIAHAAAJQAAAKgIAFQAEABADAFQACAEAAAGQABALgIAHQgIAFgNAAgAgEASIAEAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAAAQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBAAAAAAIgEAAgAgEgIIAEAAQAFAAgBgFQABgEgFAAIgEAAg");
	this.shape_3.setTransform(-12.3,9.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgRAKIAAgSIAjAAIAAASg");
	this.shape_4.setTransform(25.125,-0.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbAcQgMgKAAgSQAAgQAMgLQALgMASAAQAUAAANANIgQARQgGgHgIAAQgGABgEAEQgFAFABAGQAAAHADAFQAFAFAGAAQAGAAADgDIAAgDIgIAAIAAgRIAhAAIAAAiQgOAMgXAAQgSAAgLgMg");
	this.shape_5.setTransform(18.75,-0.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgUAlIADgVIAGABQAFAAAAgEIAAgzIAbAAIAAA3QAAAWgZAAQgIAAgIgCg");
	this.shape_6.setTransform(12.3,-0.575);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgNAmIAAhLIAbAAIAABLg");
	this.shape_7.setTransform(8.4,-0.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAEAmIgKgZIAAAZIgbAAIAAhLIAhAAQANAAAIAGQAHAIAAAKQAAAHgDAGQgDADgFAEIARAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape_8.setTransform(3.45,-0.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAFAmIgMgfIAAAfIgcAAIAAhLIAcAAIAAAcIAMgcIAeAAIgSAkIATAng");
	this.shape_9.setTransform(-4,-0.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAEAmIgKgZIAAAZIgbAAIAAhLIAhAAQANAAAIAGQAIAIAAAKQgBAHgDAGQgDADgGAEIASAfgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape_10.setTransform(-11.15,-0.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgXAmIAAhLIAvAAIAAAVIgVAAIAAAHIASAAIAAATIgSAAIAAAHIAVAAIAAAVg");
	this.shape_11.setTransform(-17.6,-0.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgNAmIgchLIAeAAIALAsIANgsIAdAAIgcBLg");
	this.shape_12.setTransform(-24.6,-0.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgUAhQgGgGgDgHIAUgLQAEAJAFAAQAAAAABgBQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgDgCIgLgFQgPgHAAgOQAAgKAIgIQAIgGAMAAQAMAAAIAGQAIAGABAGIgTAMQgEgIgFABQAAAAgBAAQAAAAAAAAQgBAAAAABQAAAAAAAAQAAADAFACIAKAFQAPAIAAAMQAAALgIAHQgIAHgOAAQgLAAgJgHg");
	this.shape_13.setTransform(19.75,-10.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgeAdQgMgMAAgRQAAgQAMgLQALgMATAAQATAAAMAMQAMALAAAQQAAARgMAMQgMALgTAAQgTAAgLgLgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQAEgFAAgHQAAgGgEgFQgEgFgGAAQgGAAgDAFg");
	this.shape_14.setTransform(12.3,-10.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgXAmIAAhMIAbAAIAAA3IAUAAIAAAVg");
	this.shape_15.setTransform(5.4,-10.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAFAmIgMgfIAAAfIgcAAIAAhMIAcAAIAAAdIAMgdIAfAAIgTAlIATAng");
	this.shape_16.setTransform(-3.2,-10.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgeAdQgMgMABgRQgBgQAMgLQALgMATAAQATAAAMAMQALALAAAQQAAARgLAMQgMALgTAAQgTAAgLgLgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQADgFABgHQgBgGgDgFQgEgFgGAAQgGAAgDAFg");
	this.shape_17.setTransform(-11.45,-10.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgeAdQgMgMAAgRQAAgQAMgLQALgMATAAQATAAAMAMQAMALAAAQQAAARgMAMQgMALgTAAQgTAAgLgLgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQAEgFAAgHQAAgGgEgFQgEgFgGAAQgGAAgDAFg");
	this.shape_18.setTransform(-20.1,-10.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgJAJQgEgEAAgFQAAgEAEgEQAEgEAFAAQAGAAAEAEQAEAEAAAEQAAAFgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_19.setTransform(8.325,6.425);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgeAAQAAgnAeAAQAfAAAAAnQAAAogfAAQgeAAAAgogAgDAAQAAASADAAQAEAAAAgSQAAgRgEAAQgDAAAAARg");
	this.shape_20.setTransform(3.575,3.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgeAAQAAgnAeAAQAfAAAAAnQAAAogfAAQgeAAAAgogAgDAAQAAASADAAQAEAAAAgSQAAgRgEAAQgDAAAAARg");
	this.shape_21.setTransform(-3.025,3.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgVAgQgGgGgCgJIAXgFQABAFAEAAQAEAAAAgFQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAgBAAAAQAAAAgBAAQgDAAgBADIgWgDIACgsIAyAAIAAAVIgcAAIAAAGIAGgBQAMAAAHAGQAIAHAAALQAAALgIAIQgJAIgNAAQgMAAgJgHg");
	this.shape_22.setTransform(-9.475,3.775);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAFAnIgMggIAAAgIgcAAIAAhNIAcAAIAAAcIAMgcIAfAAIgUAlIAUAog");
	this.shape_23.setTransform(26.65,-6.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgVAdQgMgLAAgSQAAgQALgMQAMgLASAAQAPAAAKAIIgIAVQgGgFgIAAQgHgBgEAFQgEAFAAAGQAAAHAEAFQAEAFAHgBQAIABAHgHIAIAWQgLAIgPAAQgSAAgLgLg");
	this.shape_24.setTransform(19.125,-6.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAMAnIgEgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAAEAHIgEgOIgEAOIAIAAg");
	this.shape_25.setTransform(11.6,-6.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgeAnIAAhNIAfAAQANAAAJAHQAHAFAAAKQAAAKgIAFQAEABADAFQADAFAAAEQAAAMgIAGQgIAHgMAAgAgEASIAEAAQABAAAAAAQAAAAABAAQAAgBABAAQAAAAABgBQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQAAgBgBAAIgEAAgAgEgJIAEAAQAFAAAAgEQAAgEgFAAIgEAAg");
	this.shape_26.setTransform(4.2,-6.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAIAnIAAgcIgPAAIAAAcIgcAAIAAhNIAcAAIAAAcIAPAAIAAgcIAcAAIAABNg");
	this.shape_27.setTransform(-3.2,-6.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgUAhQgGgGgDgHIAUgLQAEAJAFgBQABAAAAAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgDgCIgLgFQgPgHAAgNQAAgMAIgGQAIgHAMAAQAMAAAIAHQAIAFABAGIgTALQgDgGgGAAQAAAAgBAAQAAAAAAAAQgBAAAAABQAAAAAAABQAAACAFACIAKAGQAPAGAAANQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape_28.setTransform(-10.3,-6.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AALAnIgDgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAADAHIgDgOIgEAOIAHAAg");
	this.shape_29.setTransform(-17.5,-6.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgVAdQgMgLAAgSQAAgQALgMQAMgLASAAQAPAAAKAIIgIAVQgGgFgIAAQgHgBgEAFQgEAFAAAGQAAAHAEAFQAEAFAHgBQAIABAHgHIAIAWQgLAIgPAAQgSAAgLgLg");
	this.shape_30.setTransform(-25.175,-6.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgOAIIAAgPIAdAAIAAAPg");
	this.shape_31.setTransform(21.425,10.975);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_32.setTransform(18.425,12.825);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgVAcIAHgPQAFACADAAQAGAAABgHQgCACgFAAQgHAAgFgEQgGgGAAgJQAAgKAHgGQAHgHAKABQAZAAAAAdQAAANgGAKQgHAMgQAAQgKgBgHgEgAgCgMIgBADIABAEQAAAAABABQAAAAAAAAQABAAAAAAQAAABAAAAIACgBQABgKgDAAQAAAAgBAAQAAAAAAAAQAAABgBAAQAAAAAAABg");
	this.shape_33.setTransform(14.6,10.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgRAbQgHgGAAgJQAAgJAGgEQgFgFAAgGQAAgJAHgFQAGgGAKABQALgBAHAGQAGAFAAAJQAAAGgFAFQAGAEAAAJQAAAJgGAGQgIAFgLABQgLgBgGgFgAgCAIIgBADQAAAFADAAQAEAAgBgFIAAgDQAAAAgBgBQAAAAAAgBQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAAAgBAAQAAABAAAAQgBABAAAAgAgCgLQAAAEACAAQADAAAAgEQAAgFgDAAQgCAAAAAFg");
	this.shape_34.setTransform(9.3,10.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_35.setTransform(3.775,12.825);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgKAgIgYg+IAaAAIAIAkIAKgkIAZAAIgYA+g");
	this.shape_36.setTransform(-0.95,10.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_37.setTransform(-5.625,12.825);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AAHAgIgHghIgGAhIgYAAIgQg+IAYAAIAHAhIAHghIARAAIAIAhIAFghIAZAAIgRA+g");
	this.shape_38.setTransform(-11.6,10.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_39.setTransform(-17.525,12.825);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgLAgIAAgtIgMAAIAAgRIAvAAIAAARIgNAAIAAAtg");
	this.shape_40.setTransform(-21.225,10.6);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_41.setTransform(27.575,2.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_42.setTransform(22.125,2.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgWAXQgKgJAAgOQAAgNAKgJQAJgJAOAAQARgBALALIgNANQgGgEgGAAQgFAAgDADQgDAEAAAFQAAAGADAEQAEADAEABQAFgBADgBIAAgDIgHAAIAAgNIAbAAIAAAbQgLAKgTgBQgPABgJgKg");
	this.shape_43.setTransform(16.5,2.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_44.setTransform(9.975,2.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAKAgIgDgKIgOAAIgCAKIgXAAIAWg+IAVAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_45.setTransform(3.45,2.6);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AAHAgIAAgXIgNAAIAAAXIgWAAIAAg+IAWAAIAAAWIANAAIAAgWIAWAAIAAA+g");
	this.shape_46.setTransform(-3.1,2.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgYAgIAAg+IAZAAQAMgBAFAHQAHAFAAAKQAAAJgHAFQgGAGgKABIgEAAIAAAUgAgCgEIACAAIACgBQABAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAAAAAIgCAAg");
	this.shape_47.setTransform(-8.85,2.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgZAXQgJgJAAgOQAAgNAJgJQALgJAOAAQAQAAAJAJQAKAJAAANQAAAOgKAJQgJAJgQAAQgPAAgKgJgAgHgJQgDAEAAAFQAAAGADAEQADAEAEAAQAFAAADgEQADgEAAgGQAAgFgDgEQgDgEgFAAQgEAAgDAEg");
	this.shape_48.setTransform(-15.15,2.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_49.setTransform(-23.625,2.6);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_50.setTransform(-29.075,2.6);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AAHAfIgNgdIAAAdIgXAAIAAg+IAYAAIAMAeIAAgeIAXAAIAAA+g");
	this.shape_51.setTransform(24.425,-5.4);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAnAAIAAASIgRAAIAAAGIAPAAIAAAPIgPAAIAAAGIARAAIAAARg");
	this.shape_52.setTransform(18.975,-5.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AADAfIgIgUIAAAUIgWAAIAAg+IAbAAQALAAAHAHQAFAFAAAJQAAAFgDAEQgCADgFADIAPAagAgFgEIADAAQABAAAAgBQAAAAABAAQAAAAAAAAQAAgBABAAIABgDIgBgDQgBAAAAgBQAAAAAAAAQgBAAAAAAQAAAAgBAAIgDAAg");
	this.shape_53.setTransform(14.15,-5.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAnAAIAAASIgRAAIAAAGIAPAAIAAAPIgPAAIAAAGIARAAIAAARg");
	this.shape_54.setTransform(8.875,-5.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAWAAIAAAtIARAAIAAARg");
	this.shape_55.setTransform(4.525,-5.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgTAfIAAg+IAWAAIAAAtIARAAIAAARg");
	this.shape_56.setTransform(0.125,-5.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AAJAfIgCgJIgOAAIgCAJIgXAAIAWg+IAVAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_57.setTransform(-5.45,-5.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgLAfIAAgsIgMAAIAAgSIAvAAIAAASIgNAAIAAAsg");
	this.shape_58.setTransform(-11.275,-5.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgQAbQgGgEgCgHIARgJQADAHAEAAQAAAAABAAQAAAAAAAAQABgBAAAAQAAAAAAAAQAAgCgDgCIgJgEQgMgGAAgLQAAgJAHgFQAGgFAKAAQAJAAAIAEQAFAFACAFIgQAKQgDgHgEABQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAABAAAAQAAACAEACIAIAEQANAGAAALQAAAJgHAFQgHAFgLAAQgJAAgHgFg");
	this.shape_59.setTransform(-16.375,-5.4);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AAHAfIgNgdIAAAdIgXAAIAAg+IAYAAIAMAeIAAgeIAXAAIAAA+g");
	this.shape_60.setTransform(-22.175,-5.4);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgLAfIAAg+IAWAAIAAA+g");
	this.shape_61.setTransform(-26.8,-5.4);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgQAbQgGgEgCgHIARgJQADAHAEAAQAAAAABAAQAAAAAAAAQABgBAAAAQAAAAAAgBQAAgBgDgCIgJgEQgMgGAAgLQAAgIAHgGQAGgFAKgBQAJAAAIAGQAFAEACAFIgQAKQgDgHgEAAQgBABAAAAQAAAAAAAAQgBAAAAAAQAAABAAAAQAAACAEACIAIAEQANAGAAAKQAAAKgHAFQgHAGgLgBQgJABgHgGg");
	this.shape_62.setTransform(13.125,-13.4);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgKAfIAAg+IAVAAIAAA+g");
	this.shape_63.setTransform(9.15,-13.4);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgLAfIAAgsIgMAAIAAgSIAvAAIAAASIgNAAIAAAsg");
	this.shape_64.setTransform(5.275,-13.4);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AAJAfIgCgJIgOAAIgDAJIgWAAIAVg+IAWAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_65.setTransform(-0.55,-13.4);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AAEAfIgIgUIAAAUIgXAAIAAg+IAbAAQALAAAGAHQAHAFAAAJQAAAFgDAEQgDADgEADIAOAagAgEgFIACAAQAAAAABAAQAAAAABAAQAAAAAAAAQAAgBAAAAIABgDIgBgDQAAAAAAgBQAAAAAAAAQgBAAAAAAQgBAAAAgBIgCAAg");
	this.shape_66.setTransform(-6.5,-13.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgWAXQgKgJAAgOQAAgNAKgJQAJgKAPAAQAQAAALAKIgNAPQgGgGgGAAQgFAAgDAFQgEADABAFQgBAGAEAEQAEADAEAAQAFABADgCIAAgCIgHAAIAAgPIAbAAIAAAbQgLALgTgBQgPABgJgKg");
	this.shape_67.setTransform(-12.95,-13.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4,p:{x:25.125,y:-0.15}},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_4,p:{x:12.025,y:4.15}}]},1).to({state:[{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33.4,-19,66.3,37);


(lib.dollar_5_v2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AnGELIgtgUIABnuQAXAOAVAHQBlAoB+gMQBMgHCYgqQCWgnBNgIQB+gNBlApQAXAIAWAMIgBHuIgsgWQhlgoh/ANQhMAHiXAqQiXAnhNAIQgeADgcAAQhbAAhNgfgAnGDfQBlApB9gNQBNgICXgoQCXgpBNgHQB+gOBlApIAAmUQhlgph+ANQhMAIiXApQiYAphMAGQh+AOhlgpgAhxCOQgvgjABhEQgBhBAvg6QAvg6BCgRQBDgTAvAlQAvAjgBBEQABBBgvA5QgvA6hDASQgVAGgVABQgoAAgfgZgAAAA9Ih1AfIAAARQAAAGADABQAFADAGgBQAjgHBFgSQBEgUAjgHQAHgBADgFQAEgHABgFIAAgRQgqAJhNAVgABoh7IhoAaQhFASgiAIQgHACgEAEQgEAGAAAGIABB8QAAAFAEACQAEAFAFgDIBpgYQBEgUAjgIQAHgBADgEQAEgGABgGIAAh9QAAgFgFgCQgCgDgEAAIgEABgAGIB/QgmgKgKghIgKgBIAAgOIAHgBIgBgGIgHgBIAAgPIAMABQAEgPAMgHQAOgHAQAEQAPADAMAKIgKASQgJgHgIgCQgPgCgFAJIAbAGIgBAOIgfgFIAAAGIAeAGIgCAQIgWgHQAGANAOADQAIACAKgCIAIAXQgEACgHAAQgGAAgJgBgAhrBpIABgEIBSgXIAAAFIAxgOIAAgDIBTgWIAAAGQAAAEgEABQgkAHhEASIhnAdQgBAAgBgBQgBAAAAAAQgBgBAAAAQAAgBAAgBgAhrBFIABh+QAAAAAAgBQAAgBAAAAQABgBAAAAQABAAABAAIBegZIAAACQAAAJAJgCQAKgCAAgLIBfgZQABAAAAAAQABAAAAABQABAAAAABQAAAAAAABIgBB8QAAADgDACIhoAZQhEAUgjAHIgCABQgBAAAAAAQAAAAgBgBQAAAAAAAAQAAAAAAgBgAlggDQgkgGgMgmIgKgDIAAgPIAHACIAAgGIgHgCIAAgQIAKADQAFgNAMgGQANgIASADQAOACAKAHIgJAVQgGgGgKgBQgOgCgHAJIAcAEIAAAPQgLAAgVgGIAAAIIAfAFIgCAPIgYgDQAHAMAOADQAJAAAIgEIAJAUQgHAGgNAAIgGAAg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AnGDfIAAmUQBlApB+gOQBMgGCYgpQCXgpBMgIQB+gNBlApIAAGUQhlgph+AOQhNAHiXApQiXAohNAIQgdADgcAAQhcAAhNgfgAAAifQhCARgvA6QgvA6ABBBQgBBEAvAjQAvAlBCgTQBDgSAvg6QAvg5gBhBQABhEgvgjQgfgZgoAAQgVAAgWAHgAFNA9IAHABIABAGIgHABIAAAOIAKABQAKAhAmAKQASADAIgEIgIgXQgKACgIgCQgOgDgGgNIAWAHIACgQIgegGIAAgGIAfAFIABgOIgbgGQAFgJAPACQAIACAJAHIAKgSQgMgKgPgDQgQgEgOAHQgMAHgEAPIgMgBgAmagyIAKADQAMAmAkAGQASACAIgIIgJgUQgIAEgJAAQgOgDgHgMIAYADIACgPIgfgFIAAgIQAVAGALAAIAAgPIgcgEQAHgJAOACQAKABAGAGIAJgVQgKgHgOgCQgSgDgNAIQgMAGgFANIgKgDIAAAQIAHACIAAAGIgHgCgAhyB0QgDgBAAgGIAAgRIB1gfQBNgVAqgJIAAARQgBAFgEAHQgDAFgHABQgjAHhEAUQhFASgjAHIgDAAQgFAAgDgCgAhqBlIgBAEQAAABAAABQAAAAABABQAAAAABAAQABABABAAIBngdQBEgSAkgHQAEgBAAgEIAAgGIhTAWIAAADIgxAOIAAgFgAhxBOQgEgCAAgFIgBh8QAAgGAEgGQAEgEAHgCQAigIBFgSIBogaQAHgBADADQAFACAAAFIAAB9QgBAGgEAGQgDAEgHABQgjAIhEAUIhpAYIgDABQgEAAgCgDgAhng8QgBAAgBAAQAAAAgBABQAAAAAAABQAAABAAAAIgBB+QAAABAAAAQAAABABAAQAAAAABAAQABAAABgBQAjgHBEgUIBogZQADgCAAgDIABh8QAAgBAAAAQAAgBgBAAQAAgBgBAAQAAAAgBAAIhfAZQAAALgKACQgJACAAgJIAAgCg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_5_v2, new cjs.Rectangle(-50,-29.8,100,59.6), null);


(lib.dollar_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlZEmIgogKIhKmiQAXAHASAEQBbATBpgeQA/gSB7g5QB5g5BAgRQBpgeBcAUQAUADAUAHIBKGiIgpgMQhcgShpAdQg/ATh6A5Qh6A5g/ARQg/ASg7AAQgmAAglgIgAlfEBQBcATBogdQA/gSB7g5QB5g5BAgSQBpgeBcATIg9lWQhbgThpAdQhAASh5A5Qh7A6hAARQhoAehcgTgAhLCKQgtgYgJg5QgKg4Afg3QAfg3A2gaQA0gaAvAYQAtAYAJA5QAKA4gfA3QgfA3g2AaQgbANgZAAQgYAAgXgLgAAJA0IheArIACAPQABAFADABQAEABAFgBQAdgLA3gbQA4gaAcgMQAFgBADgGQACgFgBgFIgCgPQgiAPg+AdgABGh4IhTAlQg5AbgdAMQgFACgCAEQgCAGAAAFIAUBoQAAAFAEACQAEADAEgCIBUgmQA4gbAdgMQAFgBACgFQACgEAAgFIgShrQgCgEgEgCIgDgBIgFABgAhLBpIAAgEIBCgfIABAEIAogTIgBgDIBDgeIABAFQAAABAAAAQAAABAAAAQAAABgBAAQAAAAgBABQgeAMg3AZIhUAnIAAABQgBAAAAgBQAAAAgBAAQAAAAAAgBQAAAAgBgBgAhQBLIgThqQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAIBNgjIAAABQABAIAIgEQAJgCgCgJIBMgkQABAAAAAAQABAAAAAAQAAAAAAABQABAAAAABIASBqQAAABAAAAQAAABAAAAQgBABAAAAQAAABgBAAIhVAlQg2AagdAMIgCABIgBgBgAlaAUIgJgBIgCgOIAGABIgCgFIgFAAIgDgNIAKABQACgMAJgHQAKgJAPAAQAOAAAJAFIgFASQgGgEgIAAQgNABgEAJIAYgBIACAMQgJABgSgBIABAGIAaAAIABAOIgVAAQAIAJANAAQAHgBAGgEIALAQQgGAHgPABIgDAAQgeAAgPgdgAFgAxQgjgDgOgZIgIAAIgCgNIAGgBIgBgFIgGABIgDgNIAKgBQABgNAKgHQALgIANAAQANABAMAGIgGARQgIgFgHAAQgNAAgEAJIAZAAIABAMIgbAAIABAFIAaAAIABAPIgUgCQAIAJAMABQAGABAIgEIALASQgFAFgLAAIgFAAg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AlfEBIg9lWQBcATBogeQBAgRB7g6QB5g5BAgSQBpgdBbATIA9FWQhcgThpAeQhAASh5A5Qh7A5g/ASQhAARg6AAQgmAAgkgHgAgXiHQg2AagfA3QgfA3AKA4QAJA5AtAYQAuAXA1gZQA2gaAfg3QAfg3gKg4QgJg5gtgYQgXgLgYAAQgZAAgbANgAljATIAJABQAQAfAggCQAPgBAGgHIgLgQQgGAEgHABQgNAAgIgJIAVAAIgBgOIgaAAIgBgGQASABAJgBIgCgMIgYABQAEgJANgBQAIAAAGAEIAFgSQgJgFgOAAQgPAAgKAJQgJAHgCAMIgKgBIADANIAFAAIACAFIgGgBgAFgAxQAPABAGgGIgLgSQgIAEgGgBQgMgBgIgJIAUACIgBgPIgaAAIgBgFIAbAAIgBgMIgZAAQAEgJANAAQAHAAAIAFIAGgRQgMgGgNgBQgNAAgLAIQgKAHgBANIgKABIADANIAGgBIABAFIgGABIACANIAIAAQAOAZAjADgAhPB0QgDgBgBgFIgCgPIBegrQA+gdAigPIACAPQABAFgCAFQgDAGgFABQgcAMg4AaQg3AbgdALIgFABIgEgBgAhLBlIAAAEQABABAAABQAAAAAAAAQABAAAAABQABAAAAgBIBUgnQA3gZAegMQABgBAAAAQABAAAAgBQAAAAAAgBQAAAAAAgBIgBgFIhDAeIABADIgoATIgBgEgAhUBUQgEgCAAgFIgUhoQAAgFACgGQACgEAFgCQAdgMA5gbIBTglQAFgCADACQAEACACAEIASBrQAAAFgCAEQgCAFgFABQgdAMg4AbIhUAmIgEAAIgEgBgAhhgjQgBAAAAAAQAAABAAAAQgBABAAAAQAAABAAABIATBqQAAAAAAAAQABABAAAAQAAAAABAAQAAgBABAAQAdgMA2gaIBVglQABAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBIgShqQAAgBgBAAQAAgBAAAAQAAAAgBAAQAAAAgBAAIhMAkQACAJgJACQgIAEgBgIIAAgBg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_5, new cjs.Rectangle(-45.9,-30.2,91.9,60.5), null);


(lib.dollar_4_v2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AmxEUIgVgIIgtgUIAAnuIAtAVQBlAoB+gNQBMgHCXgpQCYgnBMgIQB+gOBlAoQAWAJAWAMIABHtQgXgMgWgKQhlgnh+ANQhMAIiXAoQhBASgyALQhGAPgrADQggAEgeAAQhOAAhCgWgAnGDfQBmAoB8gNQBNgGCXgpQCYgnBMgJQB+gNBlAoIAAmUQhlgoh+ANQhLAIiZAnQiXAqhMAGQh+ANhlgogAhwCPQguglgBhDQABhBAug6IAGgGQArg0A/gRQBCgTAwAlQAaAVALAdQAKAYgBAdQABBCgvA5QgvA5hDASQgVAHgVAAQgnAAgfgYgAhBBPQAAAFAFADQADACADgCIBsgbQALgFAAgLIAAgEIAmgKQAFAAADgEQAEgEgBgFIAAhsQAAgFgCAAQgEgDgFABIhEAQQgKADAAALIgBBIIgwANIAAhIQAAgLgLADQghAKgjAHQgLADABALIAABrQAAALAKgCIAngJgAGJCAQgmgLgMggIgJgBIgBgQIAHABIABgHIgHAAIgBgPIALAAQAEgPAOgFQAMgIARADQAPADALAKIgJARQgIgGgJgCQgPgCgFAJIAbAGIAAAPIgggGIAAAHIAfAFIgDAQIgWgGQAFAMAPADQAIADAJgDIAKAXQgFADgIAAIgNgBgAg1AkIBrgdIgBApIhqAcgAggAmQgHABgDAFQgFAGAAAEQABAHADADQAEACAHgCQAEAAAEgGQAEgFAAgGQAAgGgEgCIgFgCIgDABgAAhAdIgiAKQgDABgBADQgBAFAEgBIAigKQAGgCgBgDQAAgBAAgBQAAAAAAgBQAAAAgBgBQAAAAgBAAIgCABgAhmgjQAhgHAjgKIgCBIIgHACQAAgKgHgGQgIgGgKAEQgLACgHALQgHAIgBAKQABAKAHAGQAIAEAKgCIAEgBIAAAMIgmAIgAhEghQgHACgEAFQgEAFAAAGQgBAGAFADQAEAEAHgCQAFgBAFgGQAFgFgBgHQAAgFgEgEQgEgCgDAAIgDABgAgoA1QABgGAHgBQAEgDAAAHQABAFgGACIgCAAQgEAAgBgEgAhRArQgEgEAAgHQAAgGAEgHQAGgGAHgCQAGAAAFADQAGADgBAHIgBABQgEABgDADQgFAEABAGIAAAEIgEACIgFAAQgEAAgEgCgABBASIAEgBQAKgCAJgKQAHgIAAgJQAAgLgIgFQgIgGgKADQgJADgJAJQgHAJABAKIgJABIAAhIIBDgQIAAAAIAABqIAAAAIgmAKgABGhGQgOAEAAAPQAAAGADADQAEADAHgBQAOgFABgOQgBgFgFgEQgDgCgEAAIgCAAgABBAFQAAgKgLACIgCAAQAAgHAFgGQAGgFAFgDQAIgBAFADQAFADAAAHQAAAHgEAGQgGAGgIABIgDABgAlfgDQglgFgMgmIgJgDIAAgPIAFACIABgIIgGAAIAAgQIAJACIADgHQAGgHAIgFQANgHARADQAQABAKAIIgKATQgGgFgKgBQgNgCgHAKIAcADIgCAPIgdgEIgBAGIAeAFIgCAQIgYgEQAHAMAOACQAJABAJgEIAIAWQgFAFgLAAIgJgBgAhKgRQgBgGAHgCQAFgDAAAHQAAAIgFABIgCAAQgEAAAAgFgABAg1QgBgGAGgDQAGgBABAHQAAAGgIABIgBAAQgDAAAAgEg");
	this.shape.setTransform(0,-0.0138);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AnGDfIAAmUQBlAoB/gNQBLgGCXgqQCZgnBLgIQB+gNBlAoIAAGUQhlgoh+ANQhMAJiYAnQiXAphMAGQgfADgdAAQhaAAhNgegAAAifQg+ARgsA0IgGAGQguA6gBBBQABBDAuAlQAwAkBAgTQBDgSAwg5QAug5AAhCQAAgdgJgYQgMgdgagVQgfgZgoAAQgUAAgXAHgAFOBUIAJABQAMAgAmALQASAEAIgGIgKgXQgJADgHgDQgQgDgFgMIAXAGIACgQIgfgFIAAgHIAgAGIAAgPIgbgGQAFgJAPACQAJACAIAGIAJgRQgLgKgPgDQgQgDgNAIQgNAFgFAPIgLAAIABAPIAHAAIAAAHIgIgBgAmZgxIAJADQANAmAkAFQASADAIgHIgJgWQgIAEgJgBQgPgCgHgMIAYAEIACgQIgegFIABgGIAdAEIACgPIgcgDQAHgKANACQALABAGAFIAKgTQgKgIgRgBQgQgDgOAHQgIAFgGAHIgDAHIgJgCIAAAQIAGAAIgBAIIgFgCgAg8BXQgEgDAAgFIAAgEIgmAJQgLACAAgLIAAhrQAAgLALgDQAigHAhgKQALgDAAALIAABIIAxgNIAAhIQAAgLALgDIBDgQQAFgBAEADQACAAABAFIgBBsQABAFgEAEQgDAEgFAAIgmAKIAAAEQAAALgLAFIhrAbIgEABIgDgBgAg1AkIAAAoIBrgcIAAgpgAhmgjIAABrIAmgIIABgMIgFABQgKACgIgEQgHgGAAgKQAAgKAHgIQAIgLAKgCQAKgEAIAGQAHAGAAAKIAIgCIABhIQgiAKgiAHgAhRATQgEAHAAAGQAAAHAFAEQAFAEAHgCIAEgCIAAgEQAAgGAEgEQADgDAEgBIABgBQABgHgFgDQgGgDgGAAQgHACgGAGgABXgcQAIAFAAALQAAAJgHAIQgIAKgLACIgEABIAAALIAmgKIAAAAIABhqIgBAAIhDAQIAABIIAJgBQgBgKAHgJQAJgJAJgDIAHgBQAGAAAFAEgABBAFIAAAEIAEgBQAHgBAGgGQAEgGAAgHQAAgHgFgDQgFgDgHABQgGADgGAFQgFAGABAHIABAAIADgBQAIAAAAAJgAgrBAQgDgDAAgHQgBgEAFgGQAEgFAGgBQAEgCAEADQAEACABAGQgBAGgEAFQgEAGgEAAIgFABIgGgBgAggAuQgHABAAAGQAAAGAGgCQAGgCAAgFQgBgFgCAAIgCABgAgFArQABgDADgBIAigKQAFgCgBAFQABADgFACIgjAKIgBAAQAAAAgBgBQAAAAgBAAQAAgBAAAAQAAgBAAgBgAhPgGQgFgDABgGQAAgGAEgFQAFgFAGgCQAFgBAFACQAEAEABAFQAAAHgFAFQgEAGgGABIgEAAQgEAAgDgCgAhEgZQgHACABAGQAAAGAGgBQAFgBAAgIQAAgFgCAAIgDABgAA8gqQgEgDAAgGQAAgPAOgEQAFgBAFADQAEAEABAFQgBAOgOAFIgDAAQgFAAgCgCgABFg+QgGADABAGQAAAFAFgBQAHgBAAgGQAAgGgFAAIgCAAg");
	this.shape_1.setTransform(-0.025,-0.0015);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_4_v2, new cjs.Rectangle(-50,-29.7,100,59.5), null);


(lib.dollar_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AEuE8QhHg9hpgbQhAgQiFgMQg6gFgsgGQg9gIgkgKQhfgYhCg0IgPgMIgegeICSmPIAeAeQBGA+BqAaQA/ASCGALQCGANBAAQQBqAaBGA+QAPAOAPAQIiRGPIgegfgAmwAvQBGA+BpAaQBAARCGAMQCGANA/AQQBrAaBGA+IB3lHQhHg+hpgaQhAgQiGgNQiGgLg/gSQhqgahHg+gAEZDbQgdgUABgdIgIgEIAEgMIAGACIADgGIgGgCIAEgMIAIADQAJgKAMgBQAMgDANAHQALAIAGALIgNALQgFgHgGgFQgLgFgHAFIAVANIgGAMIgXgOIgCAGIAXANIgGAMIgRgLQABAKALAIQAFAEAIABIACAVIgEAAQgHAAgKgGgAguCCQg7gEgbgsQgbgrATg1QAUg2A3ghIAGgDQAzgdA4AEQA7AEAbAsQAPAZABAbQAAAWgJAXQgTA2g2AgQgwAcgyAAIgQAAgAhRg7IggBWQgDAJAJABIAiAEIgCADQgBAFACADQACADAEAAIBeAJQAKAAADgJIACgEIAhAEQAEABAEgCQAEgDACgDIAghYQABgEgCgBQgCgDgEgBIg7gHQgKgBgDAJIgWA6IgrgEIAVg5QADgJgJgBQgegCgdgEIgCgBQgIAAgDAJgAhBAuIAMghIBfAJIgNAhgAgvAXQgFADgCAEQgBAFACAEQADADAFAAQAEABAFgDQAFgEACgEQACgFgDgDQgDgEgEABIgCgBQgEAAgEADgAgRAhQAAABAAABQAAAAAAABQABAAAAAAQABABAAAAIAfADQAEAAABgDQACgEgFAAIgegCQgDgBgCADgABNAuIghgEIADgJIAEABQAJABAJgFQAJgGADgHQADgIgEgHQgFgGgJgBQgJgBgJAGQgIADgDAJIgIAAIAVg7IA7AHIABAAIggBWgAA8gZQgBAEACAFQACACAGACQAMABAFgMQABgEgCgEQgDgEgFgBIgCAAQgLAAgEALgAgvAfQACgEAGAAQAEgBgBAGQgCAEgFAAQgFgBABgEgAhnAcIAghWQAdAEAeACIgWA5IgHgBQADgHgEgGQgFgHgJAAQgJgCgJAGQgIAFgDAHQgDAJAEAGQAFAHAIAAIAFABIgEAJgAg4gtQgEADgCAFQgCAFACAEQADAEAFABQAGAAAFgDQAFgDACgFQABgFgCgEQgDgDgFgBIgBAAQgFAAgFACgAA2AbIgEgBIABgDQADgJgJgBIgBgBQABgFAGgEQAGgDAGAAQAGABADAEQAEAEgCAGQgDAGgFADQgFADgFAAIgCAAgAhNALQgDgEACgGQACgFAFgDQAHgEAGAAQAFACAEAEQADAEgCAFIgCAAQgEAAgDABQgEADgCAEIgBADIgEABQgGAAgDgFgABHgTQgFAAACgFQABgFAGgBQAFACgCAFQgBAEgEAAIgCAAgAgzgeQgFgBACgFQABgFAGABQAFgBgCAGQgCAFgEAAIgBAAgAkahqQgdgPACgjIgHgEIAEgNIAFAEIACgGIgFgDIAFgNIAHAFIAFgFQAGgEAIgBQANgCANAHQANAGAGAKIgOAMQgDgGgIgEQgLgGgIAGIAVALIgGANIgWgNIgDAFIAXANIgGAMIgSgKQACALALAHQAIACAHAAIAAAUIgFABQgHAAgJgFg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("ACKDBQhAgQiFgNQiGgMhAgRQhqgahFg+IB2lHQBHA+BqAaQA+ASCHALQCGANBAAQQBpAaBGA+Ih2FHQhGg+hrgagAD1CmIAHAEQAAAdAcAUQANAIAJgCIgCgVQgIgBgFgEQgLgIgBgKIAQALIAGgMIgWgNIACgGIAXAOIAGgMIgVgNQAGgFAMAFQAGAFAFAHIANgLQgGgLgMgIQgMgHgMADQgMABgJAKIgJgDIgEAMIAHACIgDAGIgGgCgAg7hoIgGADQg4AhgTA2QgUA1AbArQAcAsA7AEQA6AFA4ghQA2ggATg2QAJgXAAgWQgBgbgPgZQgbgsg8gEIgQAAQguAAgsAZgAk8igIAHAEQgCAjAdAPQAOAIAHgEIAAgUQgIAAgHgCQgMgHgBgLIASAKIAFgMIgWgNIACgFIAXANIAGgNIgWgLQAJgGAKAGQAJAEACAGIAPgMQgHgKgMgGQgNgHgNACQgIABgHAEIgEAFIgIgFIgFANIAGADIgCAGIgFgEgAhFA3QgDAAgCgDQgCgDABgFIACgDIgigEQgKgBADgJIAhhWQADgKAJACQAeAEAeACQAJABgDAJIgVA5IAqAEIAXg6QACgJALABIA7AHQAEABABADQACABgBAEIgfBYQgCADgFADQgDACgEgBIghgEIgCAEQgDAJgKAAgAhBAuIBeAJIANghIhfgJgAAsAqIAhAEIABAAIAfhWIgBAAIg6gHIgVA7IAHAAQAEgJAHgDQAKgGAIABQAKABAFAGQADAHgDAIQgCAHgJAGQgJAFgJgBIgEgBgAhoAcIAhAEIAFgJIgFgBQgJAAgEgHQgFgGADgJQAEgHAIgFQAJgGAJACQAJAAAEAHQAFAGgEAHIAHABIAXg5QgfgCgdgEgAAzADQgGAEgBAFIAAABQAKABgDAJIgBADIAEABQAFABAGgEQAGgDACgGQACgGgDgEQgDgEgGgBQgGAAgGADgAhHgHQgGADgCAFQgCAGADAEQAEAFAFAAIAFgBIABgDQACgEAEgDQADgBAEAAIACAAQACgFgEgEQgDgEgGgCQgGAAgGAEgAgtAqQgFAAgDgDQgDgEACgFQACgEAFgDQAEgDAGABQAEgBADAEQADADgCAFQgCAEgFAEQgEACgDAAIgCAAgAgvAfQgBAEAEABQAFAAADgEQABgGgEABIgCAAQgFAAgBAEgAgPAlQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAgBQABgDAEABIAeACQAEAAgCAEQAAADgEAAgABFgMQgGgCgCgCQgCgFABgEQAFgMAMABQAEABADAEQADAEgBAEQgFALgLAAIgBAAgABEgYQgDAFAGAAQAFACACgGQACgFgGgCQgFABgBAFgAg2gXQgFgBgDgEQgDgEACgFQADgFAEgDQAGgCAFAAQAFABADADQACAEgBAFQgCAFgGADQgDADgFAAIgCAAgAg2gkQgCAFAEABQAFAAADgFQACgGgFABIgBgBQgFAAgBAFg");
	this.shape_1.setTransform(0.05,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_4, new cjs.Rectangle(-47.6,-34.7,95.30000000000001,69.4), null);


(lib.dollar_3_v3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AFcGlQhQhViDgrQhOgZingZQingZhOgZQiDgqhQhWQgRgTgSgXIDZnlQAPAVAUAVIARARQBMBJB2AmQBOAaCnAZQCoAYBOAZQCDArBQBWQATAUAPAVIjZHlQgTgYgQgSgAohAUQBQBXCDAqQBOAaCoAYQCnAZBOAZQCDArBPBWICzmNQhQhWiDgrQhOgZiogZQingYhOgZQiCgshRhWgAFLEnQgigZAEgmIgIgFIAHgQIAGAEIABgEIACgCIgHgEIAHgPIAKAFQALgMAPgBQAQgCAPAMQANALAIAOIgTAMQgCgHgKgJQgNgJgKAHQAJAGAPAMIgHAOIgcgUIgDAGIAbAUIgIAPIgVgQQACAOAMALQALAGAFAAIgBAcIgCAAQgIAAgPgMgAhGCeQhJgKgfg4Qgdg5AchBQAehABGgkQBIglBKAKQBJAKAfA5QAfA4gfBBQgdBAhGAkQg4Adg4AAQgRAAgRgCgAgbhpIhbDHQgFAOAPACIBwARQAPABAGgNIBZjIQADgEgDgFQgCgGgHgBIhugQIgEAAQgNAAgFAMgAhnBiQgBAAgBAAQAAAAAAgBQAAAAAAgBQAAAAAAgBIBajHQAAgBAAAAQAAgBABAAQAAAAABAAQABAAABAAIBwARQAAAAABAAQAAAAABABQAAAAAAAAQgBABAAABIhaDHQAAABAAAAQgBABAAAAQgBAAAAAAQgBAAgBAAgAgzBYQgCAEACAEIAIAFQANAAADgJQAFgKgMgDIgDAAQgKAAgEAJgAAFhTIA+AIIAEgJIg8gJgAlZicQgPgKgJgSQgHgRADgTIgJgHIAHgOIAGAEIAAgDIADgEIgGgEIAHgRIAIAIQAXgXAiAWQAQAJAFALIgSAPQgEgJgJgFQgOgJgKAIIAaAQIgIAOIgcgTIgCAEIAAADIAbASIgJAPIgUgPQAAAQANAIQAIAFAJAAIAAAZIgEAAQgJAAgOgIg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("ACcD5QhNgZiogZQingYhOgaQiDgqhQhXICymNQBRBWCCAsQBOAZCnAYQCoAZBOAZQCCArBRBWIizGNQhQhWiDgrgAEkDjIAJAFQgEAmAiAZQAQANAJgBIABgcQgFAAgLgGQgMgLgDgOIAWAQIAIgPIgbgUIADgGIAcAUIAHgOQgPgMgKgGQALgHANAJQAKAJACAHIASgMQgHgOgNgLQgPgMgRACQgOABgMAMIgKgFIgGAPIAGAEIgCACIAAAEIgHgEgAhLiCQhGAkgeBAQgdBBAeA5QAfA4BJAKQBKALBHgmQBHgkAdhAQAehBgeg4Qgfg5hJgKQgRgCgRAAQg4AAg4AdgAl+jjIAIAHQgCATAHARQAIASAQAKQARALAKgDIAAgZQgJAAgIgFQgNgIgBgQIAVAPIAJgPIgcgSIABgDIACgEIAcATIAIgOIgagQQAKgIAOAJQAJAFAEAJIASgPQgGgLgPgJQgigWgXAXIgIgIIgHARIAFAEIgCAEIAAADIgHgEgAADB/IhvgRQgPgCAFgOIBbjHQAGgOAPACIBvAQQAGABADAGQADAFgDAEIhZDIQgGAMgOAAIgCAAgAgPhoIhaDHQgBABAAAAQAAABABAAQAAABAAAAQABAAAAAAIBwARQABAAAAAAQABAAAAAAQABAAAAgBQABAAAAgBIBajHQAAgBAAgBQABAAgBAAQAAgBAAAAQgBAAgBAAIhvgRQgBAAgBAAQgBAAAAAAQgBAAAAABQAAAAAAABgAgsBlIgHgFQgCgEACgEQAEgKANABQAMADgFAKQgDAJgLAAIgDAAgAAFhTIAFgKIA9AJIgFAJg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_3_v3, new cjs.Rectangle(-60,-46.2,120,92.5), null);


(lib.dollar_3_v2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Am/ESQgWgJgXgMIgHnqQAVALAXAKQAoAQAsAHQBEALBKgJQBMgJCWgtQCVgtBLgJQB+gPBlAoQAYAKAUAMIAIHpQgYgNgUgHQhkgph/APQhKAKiXAtQiVAshLAJQgjAFgiAAQhVAAhJgegAnADlQBlAqB/gQQBKgJCVgtQCWgtBLgJQB+gQBlApIgGmRQhlgph+AQQhLAJiVAsQiWAuhMAJQhvANhdgfIgWgIgAhtCRQgwgkAAhDQgBg/Atg7QAug6BBgVQBCgVAvAkQAvAjAABDQADBAgvA6QgtA6hCAVQgYAIgWAAQglAAgdgWgAAxh9IhjAdQgOAFgBANIAEDKQAAAOANgEIBkgeQAOgEAAgOIgEjKQAAgFgEgDQgCgCgDAAIgEABgAgyB4IgDjKQAAAAAAgBQAAAAAAAAQAAgBABAAQAAgBABAAIBjgdQABAAABgBQAAAAABABQAAAAAAAAQAAABAAABIAEDKQAAAAAAABQgBAAAAABQAAAAgBABQAAAAgBAAIhkAdIgBABQgBAAAAAAQAAAAAAgBQAAAAAAAAQAAgBAAgBgAABBPQgKAEAAAKQABAFADACQAEACADgBQAKgEAAgJQAAgJgIAAIgDAAgAgbhSIAAAKIA1gRIAAgJgAGIB1QgmgIgLghIgKgBIAAgQIAHABIgBgDIAAgDIgHgBIgBgQIAMACQAFgPALgHQANgIARAEQAPAEANAJIgLARQgFgFgMgDQgOgCgGAKQALAAARAFIgBAPQgIgDgXgDIAAAGIAeAGIgBAPIgXgEQAGALAOAEQAMABAFgDIAKAYQgEACgGAAQgHAAgJgCgAldADQgSgCgOgLQgNgLgFgRIgJgDIgBgPIAIACIgBgDIABgEIgIgBIAAgQIAKACQALgdAjAFQASACAJAIIgKATQgHgFgJgBQgPgBgFAJIAbADIgBAPIgegEIgBAEIABADIAeAEIgBAQIgYgEQAIAMAOABQAIACAIgEIAJAVQgFAEgMAAIgIgBg");
	this.shape.setTransform(0,0.0067);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AnADmIgGmRIAXAIQBdAeBvgNQBLgJCXgtQCUgtBLgJQB+gQBmApIAFGSQhlgph+AQQhKAIiXAtQiVAthKAJQgjAEgiAAQhWAAhJgdgAgBieQhCAVguA5QgtA7ABBAQAABDAwAjQAvAkBBgWQBCgUAug6QAug7gChAQgBhDgvgjQgdgXglAAQgXAAgXAJgAFNBMIAKABQAMAgAmAIQASAFAHgFIgKgXQgFADgLgCQgPgEgGgKIAXAEIABgPIgegHIAAgGQAXAEAIACIABgPQgRgFgLAAQAHgJANACQAMACAFAGIALgRQgMgJgQgFQgQgDgNAIQgMAHgFAOIgLgBIABAQIAHAAIAAAEIAAADIgHgBgAmYgoIAKACQAFARAMAMQAOALASACQASACAIgGIgKgUQgHADgJgBQgOgCgHgLIAXADIABgQIgegEIgBgDIABgEIAfAFIAAgPIgbgEQAFgJAPACQAJABAHAEIALgTQgKgHgRgCQgkgFgLAdIgJgDIAAAQIAHACIgBAEIABACIgHgCgAg9B8IgDjJQAAgOAOgEIBjgeQAGgBAEACQAEADAAAGIADDKQAAANgOAFIhjAdIgGABQgIAAAAgLgAAwhyIhjAdQAAAAgBABQAAAAgBABQAAAAAAABQAAAAAAABIADDKQgBADADgBIBkgdQABgBABAAQAAgBAAAAQABgBAAAAQAAAAAAgBIgDjKQAAgBAAAAQAAgBgBAAQAAAAAAAAQgBAAAAAAIgCAAgAgFBlQgDgDAAgEQAAgLAJgEQAMgBgBAKQABAJgLAEIgCAAIgFAAgAgbhRIA1gQIABAJIg2AQg");
	this.shape_1.setTransform(-0.025,-0.002);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_3_v2, new cjs.Rectangle(-50,-30.4,100,60.8), null);


(lib.dollar_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AEWFRQhAhFhogiQg+gUiGgUQiFgTg/gVQhoghhAhGQgOgPgOgRICumEQAMARAPAQIANAOQA+A7BeAeQA+AVCFATQCGAUA/AUQBoAiBABFQAPAQAMARIitGDQgPgUgNgNgAmzAQQBABFBpAjQA+AUCGATQCFAUA+AUQBpAiBABFICPk9QhBhFhogiQg+gUiGgUQiGgTg+gVQhogihBhFgAEJDsQgbgUADgfIgHgEIAFgNIAGAEIAAgDIACgCIgFgDIAFgNIAIAFQAJgJAMgCQANgBALAJQALAJAGALIgPAKQgCgGgHgHQgLgHgIAFQAIAFAMAKIgGALIgWgQIgDAGIAWAPIgHAMIgRgMQACAKAKAJQAIAFAFAAIgBAWIgBAAQgHAAgMgJgAg3B+Qg7gIgYgtQgYgtAXgzQAXgzA5geQA5gdA7AIQA7AIAYAtQAYAtgYAzQgXA0g5AdQgsAXgtAAQgNAAgNgCgAgVhUIhICfQgFALAMABIBZAPQAMABAFgLIBIifQACgEgCgEQgDgEgFgBIhYgNIgEAAQgJAAgEAJgAhSBOQgBAAAAAAQAAAAAAAAQgBAAAAgBQABAAAAgBIBIifQAAAAAAgBQAAAAABgBQAAAAAAAAQABAAABAAIBYAOQABAAAAAAQABAAAAABQAAAAAAAAQAAABAAABIhICfQAAAAgBAAQAAABAAAAQgBAAAAAAQgBABAAAAgAgoBGQgCAEACADQACACADABQALABADgIQAEgHgKgDIgCAAQgJAAgCAHgAAFhCIAwAHIAEgIIgxgHgAkTh8QgNgIgGgPQgGgOACgOIgHgGIAGgMIAFAEIAAgDIACgCIgFgEIAGgNIAGAGQATgTAbARQAMAIAFAJIgPAMQgDgHgHgEQgLgHgIAFIAVAOIgHALIgWgPIgCADIAAADIAVAOIgGAMIgRgLQABALAKAHQAHAEAHAAIAAATIgEABQgHAAgLgGg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AB9DHQg+gUiGgUQiFgTg/gUQhogjhAhFICOk9QBBBFBnAiQA/AVCFATQCGAUA+AUQBoAiBBBFIiPE9QhAhFhogigADpC1IAHAEQgDAfAbAUQANAKAIgBIAAgWQgEAAgJgFQgKgJgBgKIARAMIAGgMIgVgPIACgGIAWAQIAGgLQgMgKgIgFQAJgFAKAHQAIAHACAGIAOgKQgFgLgLgJQgMgJgNABQgMACgJAJIgIgFIgFANIAFADIgBACIgBADIgFgEgAg7hoQg5AegYAzQgXAzAYAtQAZAtA6AIQA7AJA5geQA5gdAXg0QAYgzgYgtQgZgtg7gIQgNgCgMAAQguAAgsAXgAkxi1IAGAGQgBAOAFAOQAHAPANAIQANAIAIgDIAAgTQgHAAgGgEQgLgHgBgLIARALIAHgMIgWgOIABgDIABgDIAXAPIAGgLIgUgOQAHgFALAHQAHAEAEAHIAPgMQgFgJgNgIQgagRgTATIgHgGIgFANIAEAEIgBACIgBADIgFgEgAACBmIhYgPQgMgBAEgLIBIifQAFgLAMACIBZANQAFABACAEQACAEgCAEIhHCfQgFAKgLAAIgCAAgAgMhTIhICfQAAABAAAAQAAABAAAAQAAAAAAAAQABAAAAAAIBZAPQABAAAAgBQABAAAAAAQABAAAAgBQAAAAAAAAIBIifQABgBAAgBQAAAAAAAAQgBgBAAAAQgBAAAAAAIhZgOQAAAAgBAAQgBAAAAAAQAAABgBAAQAAABAAAAgAgjBQQgEgBgCgCQgBgDABgEQADgIALABQAJADgEAHQgCAHgJAAIgCAAgAAEhCIAEgIIAwAHIgDAIg");
	this.shape_1.setTransform(0.075,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_3, new cjs.Rectangle(-47.9,-36.9,95.9,73.9), null);


(lib.dollar_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AoliWQASgNAYgNQBfgyCBgPQBOgICeACQCdADBOgKQCBgOBfgyIArgaIBfHvIgqAaQhgAyiAAPQhNAJifgDQiegChNAJQiBAOhgAyQgSAKgYAQgAkSjDQiBAOhgAyIBOGVQBggyCBgOQBNgJCfACQCeADBNgKQCBgOBggyIhPmWQhfA0iCANQhNAJidgCIg3gBQh1AAhAAIgAAfChQhFgBg6gvQg5gvgNhBQgNhDAogvQAogxBEACQBFABA7AvQA6AuAMBCQANBCgoAxQgnAuhCAAIgEAAgAhhhfQgIAHACAOIAfCbQACAMALAKQALAJANAAIByABQAOAAAHgJQAIgJgDgNIgdibQgDgMgLgJQgLgKgNAAIhygBQgOAAgHAKgAGQB8QgOgFgIgOIgKACIgCgPIAGgBIgBgHIgHACIgCgQIAKgDQABgQAKgMQAMgOAQgGQARgHAKADIgGAYQgHgBgJADQgPAFgDANIAbgKIACARIgeALIABAGIAdgKIABAPIgWAIQAIAKAOgGQAIgEAIgHIAMAQQgGALgRAHQgKAEgKAAQgHAAgGgDgAglBlQgIAAgHgGQgIgGgBgIIgaiKICaACIAbCJQABAIgEAGQgFAFgJABgAgxgZQgQATAFAXQAGAbAWASQAWATAaABQAbAAAQgTQAPgRgFgaQgFgZgWgSQgWgTgaAAIgCgBQgaAAgPASgAAOBGQgWAAgSgPQgSgQgEgUQgFgUANgPQANgQAXABQAVAAARAQQATAPAEAUQAEAVgMAPQgNAOgVAAIgBAAgAmqglIgIAEIgDgQIAGgCIgBgHIgGADIgDgQIAJgEQACgRALgNQAKgPARgEQAQgFAMADIgHAXQgJgBgIACQgMADgGAOIAbgIIADAQIgfAKIABAGIAegJIACAPIgYAHQAIAJAQgEQAKgDAEgGIAOASQgGAJgTAFQgKADgJAAQgXAAgNgUgAhahEIgBgGQgCgIAGgGQAEgGAJAAIByABQAIABAHAFQAHAHACAHIABAHIibgCgAgZhRQADAMAKAAQALAAgCgMQgDgKgKAAQgLAAACAKgAhDhYQgCADABAEQABALAMAAQAEAAADgDQACgDgBgFQgBgKgLAAIgCAAQgEAAgCADg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AnziCQBggzCBgOQBNgJCfACQCdADBNgJQCBgOBgg0IBPGXQhgAyiBAOQhNAKifgDQiegChNAIQiBAOhgAzgAiMhxQgnAwANBCQAMBCA6AvQA6AvBFABQBFABAogvQAogxgNhDQgNhBg5guQg7gvhFgBIgEAAQhCAAgnAugAGQB9QAOAFATgGQARgHAGgLIgNgQQgIAHgHAEQgOAFgJgJIAXgIIgBgQIgeAKIgBgGIAegLIgBgQIgcAKQAEgNAPgFQAJgDAHABIAGgYQgKgDgRAGQgRAGgLAOQgKANgCAQIgKADIADAPIAGgCIACAHIgGACIACAPIAKgCQAIAOAOAFgAlzgTQATgFAFgKIgNgSQgEAGgKAEQgQAEgIgJIAYgHIgDgQIgdAKIgBgHIAegKIgCgPIgbAIQAFgOANgDQAIgDAJACIAHgXQgNgDgQAEQgQAFgKAPQgLANgCARIgJAEIACAQIAHgEIABAHIgHADIAEAQIAIgEQASAcAlgLgAgjBxQgNAAgLgKQgLgJgCgMIgfibQgCgOAHgIQAIgJAOAAIBxABQANAAAMAJQALAJADANIAdCbQADANgIAJQgHAJgOAAgAg9BSQABAHAHAHQAIAFAIAAIByABQAJAAAFgFQAEgGgCgIIgaiJIibgCgAhYhXQgFAFACAJIABAGICbACIgBgHQgCgIgHgGQgHgGgJAAIhxgBQgJAAgFAGgAAQBSQgaAAgXgTQgWgSgFgbQgFgYAPgSQAQgSAbAAQAaABAXATQAWARAFAaQAFAZgPASQgQASgaAAIgBAAgAgogQQgNAPAFAUQADAVATAQQASAPAWAAQAWAAANgPQAMgPgEgUQgEgVgTgPQgSgPgVAAIgCAAQgVAAgMAOgAgahQQgBgKALAAQAKAAADAKQACALgLAAQgLAAgDgLgAhFhQQAAgFACgCQADgEAEABQAMAAABAKQABAFgCADQgDACgEAAQgMAAgCgKg");
	this.shape_1.setTransform(0.025,-0.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_2, new cjs.Rectangle(-55,-34.4,110,68.9), null);


(lib.dollar_1_v3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AnHEHIgsgVIADnrIAsAVQBmAoB9gLQBMgHCXgnQBlgZBFgNIA5gIQBOgIBEANQAqAHAnAQQAXAKAVAMIgDHrQgUgKgYgLQhlgqiAANQhKAHiXAmQiWAohOAIQgcACgbAAQhdAAhOgggAnHDcQBlAnB+gMQBMgGCYgmQCWgoBMgIQB/gLBjAoIAEmSQhlgqh/ANQhKAHiYAoQiXAnhNAGQh9AMhkgogAhxCNQgwglABhDQABhBAvg4QAwg5BCgSQBCgTAuAmQAwAmgBBDQgBA/gvA5QgvA5hDASQgUAFgSAAQgpAAghgYgAhJBzQgHAEAEAEQAAACAGABIBIgwIBEALQABAAAAAAQABABAAgBQAAAAAAAAQABAAAAgBQAFgCAAgCQAEgFgGgDIg1gGIBYgXQAGAAADgFQAFgGAAgGIACiCQAAgGgFgDQgEgEgGACIhtAcQhJASglAHQgFACgFAFQgFAGABAFIgBCDQgBAEAEAFQAHADADgDQAfgFA5gOgAhuBXIAAiBQAAgCADgDQAlgGBIgTIBtgbQAEgCAAAFIgBCBQgBAEgCAAIhuAaQhHAUgmAHIgBAAQAAAAAAAAQgBAAAAgBQAAAAAAgBQAAAAAAgBg");
	this.shape.setTransform(0,-0.0103);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AnHDcIAEmTQBkApB9gNQBNgGCXgnQCYgnBKgIQB/gMBlApIgEGTQhjgph/AMQhMAHiWApQiYAmhMAGQgdADgcAAQhdAAhNgfgAACieQhCARgwA5QgvA5gBBAQgBBDAwAmQAwAiBAgPQBDgTAvg5QAvg4ABhAQABhDgwglQgfgagnAAQgUAAgWAHgAhMB7QgEgEAHgEIA0giQg5APgfAFQgDACgHgCQgEgFABgFIABiDQgBgFAFgFQAFgFAFgDQAlgHBJgSIBtgbQAGgDAEAFQAFACAAAHIgCCCQAAAFgFAHQgDAFgGAAIhYAWIA1AHQAGACgEAFQAAADgFACQAAAAgBABQAAAAAAAAQAAAAgBAAQAAAAgBgBIhEgKIhIAwQgGgBAAgDgABvhiIhtAaQhIATglAHQgDACAAADIAACBQAAAEACgCQAmgGBHgUIBugbQACAAABgEIABiBQAAAAAAgBQAAgBAAAAQgBAAAAgBQgBAAAAAAIgCABg");
	this.shape_1.setTransform(0,-0.0799);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1_v3, new cjs.Rectangle(-50,-29.5,100,59.1), null);


(lib.dollar_1_v2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AklE8IgpgGIhzmCQAVAEATACQBYAHBggnQA6gXBuhEQBthCA6gYQBggoBYAIQAVABAUAFIBzGCIgogGQhYgIhiAoQg4AYhuBDQhtBDg6AYQhPAhhLAAIgegCgAkwEZQBZAIBfgoQA6gXBuhDQBthDA5gYQBhgoBYAIIhek8QhZgJhgAoQg5AYhtBEQhuBCg6AXQhfAohagIgAg3CKQgtgRgQg1QgPg0AXg3QAXg4AwgeQAwgeAtASQAtASAQA1QAPAzgXA3QgXA4gwAeQgfASgcAAQgRAAgRgGgAgdBsQgEAEACACQACADAFgBIAsg3IA5gJQAAAAABABQAAAAAAAAQAAAAAAgBQAAAAAAAAQAFgCgCgDQACgFgGAAIgqAGIA/glQAEgCADgFQACgFgCgEIgehnQgBgFgFgBQgEgDgFADIhOAwQg0AhgbAOQgEADgDAFQgCAFABAEIAeBmQACAFAFADQAEAAAEgDQAWgLAogZgAhCBgIgehmQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBABAAQAbgOA1ghIBPgwQAAAAAAAAQABgBAAAAQAAABABAAQAAAAABABIAeBnQAAABAAAAQAAAAAAABQAAAAAAABQgBAAgBAAIhPAwQg0AhgbAOIgBABIgCgCg");
	this.shape.setTransform(0,0.0257);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AkwEZIhek9QBZAIBfgoQA7gXBthCQBthEA5gYQBhgoBYAJIBeE8QhYgIhgAoQg6AYhtBDQhtBDg6AXQhRAihLAAIgdgCgAgkh9QgxAegXA4QgXA3APA0QAQA1AtARQAuARAvgdQAwgeAXg4QAXg3gPgzQgQg1gtgSQgRgHgRAAQgcAAgeATgAgeByQgDgCAEgEIAfgnQgoAZgWALQgEADgEAAQgFgDgBgFIgfhmQgBgEACgFQADgFAEgDQAbgOA1ghIBOgwQAEgDAFADQAEABABAFIAfBnQABAEgCAFQgCAFgFACIg+AlIAqgGQAFAAgCAFQACADgEACQAAAAAAAAQAAABgBAAQAAAAAAAAQAAAAgBgBIg5AJIgsA3IgDAAQAAAAgBAAQAAAAgBAAQAAAAgBgBQAAAAAAgBgABAhpIhPAwQg1AhgbAOQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABIAfBmQAAABAAAAQABABAAAAQAAAAABAAQAAgBAAAAQAbgOA0ghIBQgwQAAAAABAAQAAAAAAgBQABgBAAAAQAAAAgBgBIgehnIgCgBIgBAAg");
	this.shape_1.setTransform(-0.025,0.0104);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1_v2, new cjs.Rectangle(-45,-31.7,90,63.5), null);


(lib.dollar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AlSGkQgbgLgdgSIibnTQAdASAcALQB8A0BuhBQBCgoB0iEQBxiFBDgoQBvhDB7A1QAaALAfASICbHTQgdgSgcgLQh8g0huBBQhCAoh0CEQhxCFhDAoQg+AlhCAAQg0AAg3gXgAlgF6QB8A1BuhCQBEgoByiFQByiEBEgpQBuhCB8A0IiAl+Qh7g1huBCQhEAohyCFQhzCEhDApQhuBCh8g0gAgyDLQg1gHgVhBQgUg9AWhRQAXhPAyg8QAxg8AzAIQA0AHAWBBQAUA8gXBRQgWBPgyA9QgrA1guAAIgLgBgAA6irQgdAeg2BAQg4A/gdAdQgLALAEAOIAqB9QACAFAEAAQAFABAFgFQAXgWAqgyIgfBAQgEAHAEACQACADADgEQAAAAAAAAQABAAAAAAQAAgBAAAAQAAAAAAAAQAQgdAcg8IA/giIABgBQAEgCAAgGQAAgGgFACIguAYQAhglAigiQAFgFABgIQACgHgCgFIgoh+QgCgGgFAAIgBAAQgEAAgFAEgAlgCWQgQgIgLgRIgMgDIgDgNIAHABIgCgDIAAgEIgIgBIgEgPIAKACQACgZAngCQASAAALAEIgEAUQgHgCgKAAQgQABgEAIQAPABAPgBIAEAOQgRADgRgDIAEAHIAeABIAEAOIgZAAQANAMAOgCQAHAAAIgGIAPASQgFAKgTAAIgCAAQgRAAgRgLgAhCCbIgqh8QgBgDADgDQAdgdA3g/QA3hBAcgcQABgBABAAQAAAAABAAQAAAAAAABQABAAAAABIApB7QAAAEgBACQgbAdg5A/Qg1BAgfAeIgCABQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAgBgAGMgxQgrgFgUgdIgKABIgFgOIAHgBIgCgDIAAgDIgGAAIgGgOIAJgCQACgOAKgIQALgKATADQAQADAOAJIgEAQQgJgFgLgCQgPgCgEAKIAfAEIADAOQgXgDgLAAIADAGQAKAAAWADIAEAOIgagCQAKAKARADQAIABAJgDIAQAXQgDABgJAAIgOgBg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AlgF6Ih/l+QB8A0BuhCQBDgpBziEQByiFBEgoQBuhCB7A1ICAF+Qh8g0huBCQhEAphyCEQhyCFhEAoQg9AlhCAAQg0AAg3gYgAgxiWQgyA8gXBPQgWBRAUA9QAVBBA1AHQAzAIAxg8QAyg9AWhPQAXhRgUg8QgWhBg0gHIgLgBQguAAgrA1gAmHB6IAMADQALARAQAIQATAMARgBQATAAAFgKIgPgSQgIAGgHAAQgOACgNgMIAZAAIgEgOIgegBIgEgHQARADARgDIgEgOQgPABgPgBQAEgIAQgBQAKAAAHACIAEgUQgLgEgSAAQgnACgCAZIgKgCIAEAPIAIABIAAAEIACADIgHgBgAGMgxQAVACAFgCIgQgXQgJADgIgBQgRgDgKgKIAaACIgEgOQgWgDgKAAIgDgGQALAAAXADIgDgOIgfgEQAEgKAPACQALACAJAFIAEgQQgOgJgQgDQgTgDgLAKQgKAIgCAOIgJACIAGAOIAGAAIAAADIACADIgHABIAFAOIAKgBQAUAdArAFgAhFCqQgEAAgCgFIgqh9QgEgOALgLQAdgdA4g/QA2hAAdgeQAFgFAFABQAFAAACAGIAoB+QACAFgCAHQgBAIgFAFQgiAighAlIAugYQAFgCAAAGQAAAGgEACIgBABIg/AiQgcA8gQAdQAAAAAAAAQAAAAAAABQAAAAgBAAQAAAAAAAAQgDAEgCgDQgEgCAEgHIAfhAQgqAygXAWQgFAFgEAAIgBgBgAA9igQgcAcg3BBQg3A/gdAdQgDADABADIAqB8QAAAFADgEQAfgeA1hAQA5g/AbgdQABgCAAgEIgph7IgCgCIgCABg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1, new cjs.Rectangle(-55,-44.3,110,88.6), null);


(lib.cta_rollover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E39126").s().p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	this.shape.setTransform(10,12);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta_rollover, new cjs.Rectangle(0,0,20,24), null);


(lib.cta_arrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag/AKIAAgTIB/AAIAAATg");
	this.shape.setTransform(1.9821,5.4827,0.3891,0.5,0,-50.0001,130.0012);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag/AKIAAgTIB/AAIAAATg");
	this.shape_1.setTransform(1.9679,2.2327,0.3891,0.5,50.0001);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta_arrow, new cjs.Rectangle(0,0,4,7.8), null);


(lib.cover1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AygE2IAAprMAlBAAAIAAJrg");
	this.shape.setTransform(-0.75,26,1.3023,2.3198,-4.9985,0,0,-119,-31.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cover1, new cjs.Rectangle(0,0,320,170.2), null);


(lib.cover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A8MQQMAAAggfMA4ZAAAMAAAAgfg");
	this.shape.setTransform(1491.1968,-75.0388,0.4098,0.3363,0,0,180);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-180.5,0,180.5,0).s().p("A8MQQMAAAggfMA4ZAAAMAAAAgfg");
	this.shape_1.setTransform(1252.1487,-75.0388,0.9231,0.3363,0,0,180);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("A8MQQMAAAggfMA4ZAAAMAAAAgfg");
	this.shape_2.setTransform(99.8688,-75.0226,0.5534,0.336);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-180.5,0,180.5,0).s().p("A8MQQMAAAggfMA4ZAAAMAAAAgfg");
	this.shape_3.setTransform(444.9974,-75.0226,1.37,0.336);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E6E7E8").s().p("A8MQQMAAAggfMA4ZAAAMAAAAgfg");
	this.shape_4.setTransform(1491.1968,73.9501,0.4098,1.0961,0,0,180);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#E6E7E8","rgba(230,231,232,0)"],[0,1],-180.5,0,180.5,0).s().p("A8MQQMAAAggfMA4ZAAAMAAAAgfg");
	this.shape_5.setTransform(1252.1487,73.9501,0.9231,1.0961,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E6E7E8").s().p("AvnR0MAAAgjnIfOAAMAAAAjng");
	this.shape_6.setTransform(99.95,73.85);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#E6E7E8","rgba(230,231,232,0)"],[0,1],-180.5,0,180.5,0).s().p("A8MQQMAAAggfMA4ZAAAMAAAAgfg");
	this.shape_7.setTransform(445.1583,73.8501,1.3704,1.0961);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AqONSIAA6jIUdAAIAAajg");
	this.shape_8.setTransform(914.5218,64.9997,0.8473,1.2353);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#E90000","rgba(233,0,0,0)"],[0,1],125,0,-124.9,0).s().p("AzhNSIAA6jMAnDAAAIAAajg");
	this.shape_9.setTransform(735,64.9997,1,1.2353);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AnGNSIAA6jIONAAIAAajg");
	this.shape_10.setTransform(70.4982,64.9997,1.5503,1.2353);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#E90000","rgba(233,0,0,0)"],[0,1],-125,0,125,0).s().p("AzhNSIAA6jMAnDAAAIAAajg");
	this.shape_11.setTransform(265,64.9997,1,1.2353);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-110,1565.2,298);


(lib.arrowLeftRight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// hitArea
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,0,0,0.008)").s().p("AiuC5IAAlxIFdAAIAAFxg");
	this.shape.setTransform(6.4856,-0.0054,0.7142,1.081);

	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(1));

	// arrow
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#969696").s().p("AguAiIBEhQIAJALIANgLIACACIhMBbg");
	this.shape_1.setTransform(6.65,5.5,1.4084,1.4088,0,0,0,0.1,-0.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#969696").s().p("AgugnIAQgMIBMBbIgPAMg");
	this.shape_2.setTransform(6.5491,-5.6183,1.4084,1.4088);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AguAiIBEhQIAJALIANgLIACACIhMBbg");
	this.shape_3.setTransform(6.65,5.5,1.4086,1.4091,0,0,0,0.1,-0.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgugnIAQgMIBMBbIgPAMg");
	this.shape_4.setTransform(6.5498,-5.6195,1.4086,1.4091);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).to({state:[{t:this.shape_4},{t:this.shape_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-6,-20,25,40);


(lib.subsubtext = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer_1
	this.m02 = new lib.mo2();
	this.m02.name = "m02";
	this.m02.parent = this;
	this.m02.setTransform(0,48);

	this.m01 = new lib.Symbol5();
	this.m01.name = "m01";
	this.m01.parent = this;
	this.m01.setTransform(0,24);

	this.m00 = new lib.Symbol1();
	this.m00.name = "m00";
	this.m00.parent = this;

	this.m11 = new lib.Symbol6();
	this.m11.name = "m11";
	this.m11.parent = this;
	this.m11.setTransform(0,23);

	this.m10 = new lib.Symbol2();
	this.m10.name = "m10";
	this.m10.parent = this;

	this.m21 = new lib.Symbol7();
	this.m21.name = "m21";
	this.m21.parent = this;
	this.m21.setTransform(0,24);

	this.m20 = new lib.Symbol3();
	this.m20.name = "m20";
	this.m20.parent = this;

	this.m32 = new lib.m32();
	this.m32.name = "m32";
	this.m32.parent = this;
	this.m32.setTransform(0,48);

	this.m31 = new lib.m31();
	this.m31.name = "m31";
	this.m31.parent = this;
	this.m31.setTransform(0,24);

	this.m30 = new lib.Symbol4();
	this.m30.name = "m30";
	this.m30.parent = this;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.m00},{t:this.m01},{t:this.m02}]}).to({state:[{t:this.m10},{t:this.m11}]},1).to({state:[{t:this.m20},{t:this.m21}]},1).to({state:[{t:this.m30},{t:this.m31},{t:this.m32}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,386.2,80);


(lib.price_old_line_container = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.line = new lib.price_old_line();
	this.line.name = "line";
	this.line.parent = this;
	this.line.setTransform(0,24);

	this.timeline.addTween(cjs.Tween.get(this.line).wait(1));

}).prototype = getMCSymbolPrototype(lib.price_old_line_container, new cjs.Rectangle(0,0,92,26), null);


(lib.label_green_circle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.l1 = new lib.label_green_circle_checl_2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(-4.65,5.2,1,1,-45);

	this.l0 = new lib.label_green_circle_check_1();
	this.l0.name = "l0";
	this.l0.parent = this;
	this.l0.setTransform(-6.4,-2.25,1,1,48.5012);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#30A945").s().p("AhiBjQgpgpAAg6QAAg5ApgpQApgpA5AAQA6AAApApQApApAAA5QAAA6gpApQgpApg6AAQg5AAgpgpg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.l0},{t:this.l1}]}).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,0,0,0)").s().p("AhZgFIAagYIArAvIBWhWIAYAbIhwBug");
	this.shape_1.setTransform(0,-0.125);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle, new cjs.Rectangle(-14,-14,28,28), null);


(lib.label_green = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.text = new lib.label_green_text();
	this.text.name = "text";
	this.text.parent = this;
	this.text.setTransform(32,4);

	this.gr = new lib.label_green_circle();
	this.gr.name = "gr";
	this.gr.parent = this;
	this.gr.setTransform(14,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.gr},{t:this.text}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green, new cjs.Rectangle(0,0,28,28), null);


(lib.label_cashback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("Helloblello", "15px 'Kokonor'", "#FFFFFF");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 16;
	this.dynText.lineWidth = 86;
	this.dynText.parent = this;
	this.dynText.setTransform(10,6);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

	// Layer_2
	this.bgr = new lib.emptyMovieClip();
	this.bgr.name = "bgr";
	this.bgr.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.bgr).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_cashback, new cjs.Rectangle(8,4,90.1,30), null);


(lib.greys = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween3("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(1623.85,164);

	this.instance_1 = new lib.Tween4("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-0.15,164);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},550).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,x:-0.15},550).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1625,0,4872.7,328);


(lib.endframe_video = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AyWGQIAAjIMAktAAAIAADIg");
	mask.setTransform(117.5001,39.9993);

	// line4
	this.l3 = new lib.endframe_video_line4();
	this.l3.name = "l3";
	this.l3.parent = this;
	this.l3.setTransform(0,60);

	var maskedShapeInstanceList = [this.l3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.l3).wait(1));

	// mask3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AyWEsIAAjIMAktAAAIAADIg");
	mask_1.setTransform(117.5001,29.9993);

	// line3
	this.l2 = new lib.endframe_video_line3();
	this.l2.name = "l2";
	this.l2.parent = this;
	this.l2.setTransform(0,40);

	var maskedShapeInstanceList = [this.l2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.l2).wait(1));

	// mask2 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("AyWDIIAAjIMAktAAAIAADIg");
	mask_2.setTransform(117.5001,19.9993);

	// line2
	this.l1 = new lib.endframe_video_line2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(0,20);

	var maskedShapeInstanceList = [this.l1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.l1).wait(1));

	// mask1 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("AyWBkIAAjHMAktAAAIAADHg");
	mask_3.setTransform(117.5001,9.9993);

	// line1
	this.l0 = new lib.endframe_video_line1();
	this.l0.name = "l0";
	this.l0.parent = this;

	var maskedShapeInstanceList = [this.l0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.l0).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_video, new cjs.Rectangle(0,0,235,80), null);


(lib.endframe_static = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("Egn1AJsIAAkYMBPrAAAIAAEYg");
	mask.setTransform(254.9976,61.9985);

	// line4
	this.l3 = new lib.endframe_static_line4();
	this.l3.name = "l3";
	this.l3.parent = this;
	this.l3.setTransform(0,96);

	var maskedShapeInstanceList = [this.l3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.l3).wait(1));

	// mask3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("Egn1AHMIAAkYMBPrAAAIAAEYg");
	mask_1.setTransform(254.9976,45.999);

	// line3
	this.l2 = new lib.endframe_static_line3();
	this.l2.name = "l2";
	this.l2.parent = this;
	this.l2.setTransform(0,64);

	var maskedShapeInstanceList = [this.l2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.l2).wait(1));

	// mask2 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("Egn1AEsIAAk1MBPrAAAIAAE1g");
	mask_2.setTransform(254.9976,29.9986);

	// line2
	this.l1 = new lib.endframe_static_line2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(0,32);

	var maskedShapeInstanceList = [this.l1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.l1).wait(1));

	// mask1 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("Egn1ACMIAAkXMBPrAAAIAAEXg");
	mask_3.setTransform(254.9976,13.999);

	// line1
	this.l0 = new lib.endframe_static_line1();
	this.l0.name = "l0";
	this.l0.parent = this;

	var maskedShapeInstanceList = [this.l0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.l0).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_static, new cjs.Rectangle(0,0,503,92), null);


(lib.dollar_cashback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_2
	this.text = new lib.dollar_cashback_text();
	this.text.name = "text";
	this.text.parent = this;
	this.text.setTransform(0,1.45,1,1,9.9999);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AkTETQhyhyAAihQAAihByhyQBzhyCgAAQCiAABxByQBzByAAChQAAChhzByQhxBziiAAQigAAhzhzg");
	this.shape.setTransform(-0.0216,-0.0194,0.8974,0.8974);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_cashback, new cjs.Rectangle(-35,-35,70,70), null);


(lib.cta = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// text
	this.dynText = new cjs.Text("", "11px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 9;
	this.dynText.lineWidth = 226;
	this.dynText.parent = this;
	this.dynText.setTransform(-248,5);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

	// arrowsMask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	mask.setTransform(-10,12);

	// arrows
	this.arrow2 = new lib.cta_arrow();
	this.arrow2.name = "arrow2";
	this.arrow2.parent = this;
	this.arrow2.setTransform(-13,8);

	this.arrow1 = new lib.cta_arrow();
	this.arrow1.name = "arrow1";
	this.arrow1.parent = this;
	this.arrow1.setTransform(-10,8);

	var maskedShapeInstanceList = [this.arrow2,this.arrow1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.arrow1},{t:this.arrow2}]}).wait(1));

	// rollover
	this.rollover1MC = new lib.emptyMovieClip();
	this.rollover1MC.name = "rollover1MC";
	this.rollover1MC.parent = this;
	this.rollover1MC.setTransform(-250,0);
	this.rollover1MC.alpha = 0;

	this.rollover2MC = new lib.cta_rollover();
	this.rollover2MC.name = "rollover2MC";
	this.rollover2MC.parent = this;
	this.rollover2MC.setTransform(-10,12,1,1,0,0,0,10,12);
	this.rollover2MC.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.rollover2MC},{t:this.rollover1MC}]}).wait(1));

	// bgr
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FAA332").s().p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	this.shape.setTransform(-10,12);

	this.shapeMC = new lib.emptyMovieClip();
	this.shapeMC.name = "shapeMC";
	this.shapeMC.parent = this;
	this.shapeMC.setTransform(-250,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shapeMC},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta, new cjs.Rectangle(-250,0,250,25.3), null);


// stage content:
(lib.index = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var self = this;
		
		
		
		//stage.enableMouseOver(6);
		
		var videoDiv = document.getElementById("content");
		var videoHolder = document.getElementById("video_holder");
		if (videoHolder != undefined) {
			var videoPlayer = videoHolder.getElementsByTagName("video")[0];
		}
		var muteDiv = document.getElementById("mute");
		var playPauseDiv = document.getElementById("playPause");
		var isVideoEnded = false;
		var mmMC = this.mediamarkt;
		var mm2MC = this.mediamarkt2;
		var greyMC = this.grey;
		
		//logo original _x: 725
		var logoMC = this.logo;
		var productMC = this.product;
		var priceMC = this.price;
		var labelMC = this.label;
		var leftMC = this.left;
		var rightMC = this.right;
		var ctaMC = this.cta;
		var cta2MC = this.cta2;
		var coverMC = this.cover;
		var hitAreaMC = this.hitAreaMC;
		var leftButtonDiv = document.getElementById("arrow_left");
		var rightButtonDiv = document.getElementById("arrow_right");
		var ctaButtonDiv = document.getElementById("cta");
		var cover2MC = this.cover2;
		var lineMC = this.lineContainer;
		var campaignMC = this.campaign;
		var whiteMC = this.white;
		var servButtonMC = this.servButton;
		var vanafMC = this.vanaf;
		var cover3MC = this.cover3;
		var endMC = this.endframe;
		var endVideoMC = this.endframe_video;
		
		var text1MC = this.text1;
		var text2MC = this.text2;
		var text3MC = this.text3;
		
		var numberOfProducts = productID = numberOfServices = prevProductID = 0;
		dynamic.productID = productID;
		var productsObject = new Array();
		
		leftMC.mouseChildren = rightMC.mouseChildren = false;
		
		var bigJump = false;
		
		var productImgScale = 0.80; //1,552795031055901 //0.39 //0.42
		if (typeof VARIATION == "undefined") {
			VARIATION = "";
		}
		if (VARIATION == "dollar") {
			productImgScale *= 1.25;
			//leftMC.y -= 25;
			//rightMC.y -= 25;
		}
		if (VARIATION == "merkenweek") {
			productImgScale *= 0.85;
			leftMC.x += 125;
			rightMC.x += 275;
		}
		var numberImgScale = 0.5;
		var logoScale = 0.5;
		var campaignScale = 0.5;
		
		var autoRotateTime = 3.0;
		var ctaHover = false;
		
		var dollarCashbackToX = 590;
		var dollarCashbackToY = 70;
		var prijsToX = 830;
		var logoToY = -23;
		var productToX = productMC.x;
		var labelToX = 565;
		var priceToX = priceMC.x;
		var text1ToY = text1MC.y +30;
		var text2ToY = text2MC.y + 30;
		var text3ToX = text3MC.x;
		var leftToX = leftMC.x;
		var rightToX = rightMC.x;
		var ctaToX = dynamic.width - 15;
		var mm2ToX = mm2MC.x;
		var lineToX = 686;
		var servButtonToX = dynamic.width;
		var vanafToX = 688;
		var maniaLabelToX = 15;
		var lines = 4;
		
		for (var i = 0; i<lines; i++) {
			var lMC = endMC["l" + i];
			lMC.toY = lMC.y;
			lMC.y = lMC.toY + 29;
			lMC.rotation = 7;
			
			lMC = endVideoMC["l" + i];
			lMC.toY = lMC.y;
			lMC.y = lMC.toY + 18;
			lMC.rotation = 7;
		}
		var endToX = 17;
		var gToX = 20;
		var maxGreen = 3;
		for (i = 0; i<maxGreen; i++) {
			var a = self["g" + i];
			a.x = gToX;
			a.toY = a.y;
			a.y = a.toY + 25;
			a.alpha = 0.0;
			a.gr.l0.scaleX = a.gr.l1.scaleX = 0;
			a.gr.scaleX = a.gr.scaleY = 0.0;
		}
		var serviceLabelToX = 158;
		
		var isLeftClick = false;
		
		productMC.x = dynamic.width + 150;
		//priceMC.x = - dynamic.width / 2;
		text1MC.y = text1ToY +45;
		text2MC.y = text2ToY +45;
		leftMC.x = leftToX + 25;
		rightMC.x = rightToX - 25;
		ctaMC.x = ctaToX - 70;
		ctaMC.alpha = 0.0;
		mm2MC.y = 308;
		mm2MC.alpha = 0;
		mm2MC.x = mm2ToX + 50;
		cover2MC.y = 40;
		cover2MC.x = 3000;
		lineMC.x = lineToX;
		lineMC.line.rotation = -14;
		lineMC.line.scaleX = 0.0;
		vanafMC.x = vanafToX + 20;
		cover3MC.y = dynamic.height+300;
		endMC.x = endToX;
		cta2MC.x = ctaToX - 200;
		cta2MC.alpha = 0.0;
		
		// DCO 2.0 (!)
		this.getShotDuration = function (shot) {
			
			var duration = 3.0;
		
			return duration;
			
		}
		
		this.getShotData = function (i) {
		
			// IF NOT EMPTY SHOT
			if (dynamic.getDynamic("shot_type", i) == "") {
				return undefined;
			}
		
			return {
				
				"products": dynamic.getDynamic("shot_type", i).split("|"),
				"main_specs": dynamic.getDynamic("copy", i).split("|"),
				"sub_specs": dynamic.getDynamic("price_type", i).split("xxx")[0].split("|"),
				"label_new": dynamic.getDynamic("price_type", i).split("xxx")[1].split("|"),
				//"label_img": dynamic.getDynamic("price", i).split("|"),
				"price": dynamic.getDynamic("price", i).split("|"),
				"cta": dynamic.getDynamic("cta_copy", i),
				"colorPanel": dynamic.getDynamic("font_scale", i).split("|")[0]
				//"categoryType": dynamic.getDynamic("font_scale", i).split("|")[1]
				//"logo": dynamicContent.dcodata[0].Logo1.split("|")
		
			}
		
		}
		
		this.playShot = function (shot) {
		
			if (shot.index == 1) {
				
				numberOfProducts = shot.products.length;
				
				var h = 0;
				
				for (var i = 0; i<numberOfProducts; i++) {
					
					if (shot.products[i] != "" && shot.products[i] != "-") {
						
						var productName = shot.products[i];
						var checkProductName = productName.toLowerCase();
						var isService = false;
						if (shot.products[i][0] == "^") {
							isService = true;
							productName = productName.substring(1, productName.length);
							numberOfServices++;
						}
						
						var productObject = {
							name: productName,
							main_specs: shot.main_specs[i],
							sub_specs: shot.sub_specs[i],
							label_new: shot.label_new[i],
							label: !dynamic.isImageEmpty(LABEL + i),
							price: shot.price[i].split("^")[0],
							priceOld: shot.price[i].split("^")[1],
							service: isService
						}
						
						var imgMC = new createjs.MovieClip();
						productMC.addChild(imgMC);
						if (!(checkProductName == "tv" || checkProductName == "audio" || checkProductName == "video")) {
							dynamic.setDynamicImage(imgMC, (PRODUCT + i));
						}
						
						var toScale = productImgScale;
						var toX = 0;
						var toY = 0;
						imgMC.x = h * 1272;
						h++;
						
						if (shot.sub_specs[i] != undefined && shot.sub_specs[i] != "" && shot.sub_specs[i] != " " && shot.sub_specs[i] != "-") {
							imgMC.green = true;
						}
						if (shot.label_new[i].length > 0 && shot.label_new[i].split("_")[0].toLowerCase() == "cashback") {
							imgMC.cashback = true;
						}
						if (shot.label_new[i].length > 0 && shot.label_new[i].split("_")[0].toLowerCase() == "service") {
							imgMC.service = true;
						}
						/*
						if (imgMC.green == true) {
							toScale *= 0.82;
							toX = 43;
							if (imgMC.cashback == true) {
								toScale *= 0.92;
								toY = -22;
							}
						} else if (imgMC.cashback == true) {
							toScale *= 0.76;
							toY -= 23;
						}
						*/
						if (imgMC.cashback == true) {
							toScale *= 0.9;
							toX += 45;
						}
						if (VARIATION == "dollar" && i == 0) {
							toScale *= 1.1;
						}
						if (VARIATION == "dollar" && i == 1) {
							toScale *= 0.86;
						}
						if (VARIATION == "dollar" && i == 2) {
							//toX -= 40;
						}
						if (VARIATION == "merkenweek" && i == 0) {
							//toScale *= 1;
							toY += 20;
							toX += 435;
						}
						if (VARIATION == "merkenweek" && i == 1) {
							//toScale *= 1.1;
							toY += 20;
							toX += 395;
						}
						if (VARIATION == "merkenweek" && i == 2) {
							//toScale *= 1;
							toY += 20;
							toX += 395;
						}
						if (VARIATION == "merkenweek" && i == 3) {
							//toScale *= 1.1;
							toY += 20;
							toX += 395;
						}
						//toScale *= 2.9;
						//toX += 200;
						imgMC.scaleX = imgMC.scaleY = toScale;
						imgMC.x += toX;
						imgMC.y += toY;
						
						var logoImgMC = new createjs.MovieClip();
						logoMC.addChild(logoImgMC);
						logoImgMC.alpha = 0;
						//dynamic.setDynamicImage(logoImgMC, (LOGO + i), 0, 0);
						logoImgMC.scaleX = logoImgMC.scaleY = logoScale;
						
						var labelImgMC = new createjs.MovieClip();
						labelMC.addChild(labelImgMC);
						if (!dynamic.isImageEmpty(LABEL + i)) {
							//dynamic.setDynamicImage(labelImgMC, (LABEL + i));
						}
						
						if (checkProductName == "tv" || checkProductName == "audio" || checkProductName == "video") {
							productObject = checkProductName;
							shot.main_specs[i] = shot.sub_specs[i] = shot.price[i] = "";
							if (checkProductName == "audio") {
								endMC.l3.gotoAndStop(1);
							}
						}
						
						productsObject.push(productObject);
						
					}
				}
				
				numberOfProducts = productsObject.length;
				
				if (shot.colorPanel == "red") {
					greyMC.gotoAndStop(1);
					cover2MC.gotoAndStop(1);
					leftMC.gotoAndStop(1);
					rightMC.gotoAndStop(1);
					lineMC.line.gotoAndStop(1);
					whiteMC.visible = false;
					text1MC.dynText.color = text2MC.dynText.color = vanafMC.dynText.color = "#ffffff";
				}
				
				if (!dynamic.isImageEmpty(IMAGE_COUNT + 1)) {
					dynamic.setDynamicImage(campaignMC, (IMAGE_COUNT + 1));
					campaignMC.scaleX = campaignMC.scaleY = 0;
				}
				
				if (numberOfServices > 0) {
					buildServicesButton("BEKIJK ONZE SERVICES");
					//rightToX = servButtonToX - 19;
					//rightMC.x = rightToX - 25;
					var servButtonWidth = dynamic.width - servButtonToX;
					//rightButtonDiv.style.right = servButtonWidth + "px";
					//productToX -= Math.round((dynamic.width - servButtonToX) / 3);
					var div = document.createElement("div");
					div.style.width = servButtonWidth + "px";
					div.style.height = "90px";
					div.style.right = "0px";
					div.style.top = "75px";
					div.id = "services";
					document.getElementById("dom_overlay_container").appendChild(div);
					document.getElementById("services").addEventListener("click", doServices, false);
					/*
					for (i = 0; i<numberOfProducts; i++) {
						var m = productMC.children[i];
						if (m.green == true) {
							var sc = m.scaleX;
							sc *= 0.76;
							m.scaleX = m.scaleY = sc;
							m.x -= 7;
							m.y += 3;
							if (m.cashback == true) {
								m.y += 7;
							}
						}
					}
					*/
				}
				
				buildCta(shot.cta.split("|")[0], ctaMC);
				if (shot.cta.split("|")[1] != undefined) {
					buildCta(shot.cta.split("|")[1], cta2MC);
				}
				editCtaButtonDivWidth(ctaMC.ctaTextWidth);
				updateLogoTextsLabelPrice();
				animateInCreative();
				
				ctaButtonDiv.addEventListener("mouseover", doRollOver, false);
				ctaButtonDiv.addEventListener("mouseout", doRollOut, false);
				ctaButtonDiv.addEventListener("click", dynamic.onMouseClick.bind(dynamic));
				
				if (productsObject[productID] != "video" && (VARIATION != "dollar"|| VARIATION !="merkenweek")) {
					
					//checkNumberOfProducts();
					
				}
				
			}
			
			//if (shot.isLastShot != true) {
		
		}
		
		function checkNumberOfProducts() {
						
			if (numberOfProducts > 1) {
				
				leftMC.visible = rightMC.visible = true;
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
				leftButtonDiv.addEventListener("click", doLeftRightClick, false);
				rightButtonDiv.addEventListener("click", doLeftRightClick, false);
				
				TweenMax.delayedCall(autoRotateTime + 1, doAutoRotate);
				
				enableSwipe();
				
			}
			
		}
		
		function enableSwipe() {
			swipedetect(document.getElementById("swipe_area"), function (swipedir) {
				self.cancelAutoRotate();
				var maxID = numberOfProducts - numberOfServices - 1;
				var minID = 0;
				if (productsObject[productID].service) {
					maxID = numberOfProducts - 1;
					minID = numberOfProducts - numberOfServices;
				} 
				if (swipedir == "right") {
					if (productID > minID) {
						productID--;
						bigJump = false;
					} else {
						bigJump = true;
						productID = maxID;
					}
					isLeftClick = true;
					changeProduct();
				}
				if (swipedir == "left") {
					if (productID < maxID) {
						productID++;
						bigJump = false;
					} else {
						bigJump = true;
						productID = minID;
					}
					isLeftClick = false;
					changeProduct();
				}
				if (swipedir == "click") {
					dynamic.onMouseClick();
				}
			});
		}
		
		function animateInVideo() {
			TweenMax.set(videoHolder, {display: "block"});
			TweenMax.set(videoDiv, {x: 50});
			//videoPlayer.currentTime = 0.1;
			dynamic.playVideo(1);
			TweenMax.to(videoDiv, 0.8, {x: 0, alpha: 1.0, ease: Expo.easeOut});
		}
		
		function checkVideoCurrentTime(event) {
			
			var time = event.target.currentTime;
			if (time >= event.target.duration - 0.6 && !isVideoEnded && videoHolder.style.display == "block") {
				document.getElementById("unmute").parentNode.removeChild(document.getElementById("unmute"));
				document.getElementById("pause").parentNode.removeChild(document.getElementById("pause"));
				isVideoEnded = true;
				TweenMax.to(videoDiv, 0.5, {alpha: 0.0});
				TweenMax.delayedCall(0.4, animateInEndframe, [endVideoMC]);
				checkNumberOfProducts();
				leftMC.alpha = rightMC.alpha = 0;
				leftMC.x = leftToX + 25;
				rightMC.x = rightToX - 25;
				TweenMax.to(leftMC, 0.9, {x: leftToX, ease: Expo.easeOut});
				TweenMax.to(leftMC, 0.8, {alpha: 1.0});
				TweenMax.to(rightMC, 0.9, {x: rightToX, ease: Expo.easeOut});
				TweenMax.to(rightMC, 0.8, {alpha: 1.0});
			}
			
		}
		
		function checkVideoMuted(event) {
			
			if (videoPlayer.muted) {
				TweenMax.to(muteDiv.children[0], 0.3, {alpha: 0.0});
				TweenMax.to(muteDiv.children[1], 0.3, {alpha: 1.0});
			} else {
				TweenMax.to(muteDiv.children[0], 0.3, {alpha: 1.0});
				TweenMax.to(muteDiv.children[1], 0.3, {alpha: 0.0});
			}
			
		}
		
		function doMuteUnmute(event) {
			
			if (videoPlayer.muted) {
				videoPlayer.muted = false;
			} else {
				videoPlayer.muted = true;
			}
			
		}
		
		function checkVideoPlayPause(event) {
			if (event.type == "play") {
				TweenLite.to(playPauseDiv.children[0], 0.3, {alpha: 0.0});
				TweenLite.to(playPauseDiv.children[1], 0.3, {alpha: 1.0});
			}
			if (event.type == "pause") {
				TweenLite.to(playPauseDiv.children[0], 0.3, {alpha: 1.0});
				TweenLite.to(playPauseDiv.children[1], 0.3, {alpha: 0.0});
			}
		}
		
		function doPlayPause(event) {
			if (videoPlayer.paused) {
				videoPlayer.play();
			} else {
				videoPlayer.pause();
			}
		}
		
		function buildCta(ctaText, whichCtaMC) {
			
			var rightMargin = 20;
			var buttonHeight = 24;
			var textPadding = 7;
			
			var ctaTextField = whichCtaMC.dynText;
			ctaTextField.text = ctaText;
			var ctaRect = new createjs.Shape();
			var ctaTextWidth = Math.round(ctaTextField.getMeasuredWidth()) + textPadding * 2;
			ctaTextField.x = - rightMargin - ctaTextWidth + textPadding - 1;
			whichCtaMC.shapeMC.x = whichCtaMC.rollover1MC.x = - rightMargin - ctaTextWidth - 1;
			ctaRect.graphics.beginFill("#FAA332").drawRoundRectComplex(0, 0, ctaTextWidth, buttonHeight, 3, 0, 0, 3);
			whichCtaMC.shapeMC.addChild(ctaRect);
			
			var rolloverRect = new createjs.Shape();
			rolloverRect.graphics.beginFill("#E39126").drawRoundRectComplex(0, 0, ctaTextWidth, buttonHeight, 3, 0, 0, 3);
			whichCtaMC.rollover1MC.addChild(rolloverRect);
			
			whichCtaMC.ctaTextWidth = ctaTextWidth;
			
		}
		
		function editCtaButtonDivWidth(toWidth) {
				
			var rightMargin = 20;
			ctaButtonDiv.style.width = (toWidth + 1 + rightMargin) + "px";
			
		}
		
		function doRollOver(event) {
			
			TweenMax.to([ctaMC.rollover1MC, ctaMC.rollover2MC, cta2MC.rollover1MC, cta2MC.rollover2MC], 0.4, {alpha: 1.0});
			ctaHover = true;
			ctaTL.play();
			
		}
		
		function doRollOut(event) {
			
			TweenMax.to([ctaMC.rollover1MC, ctaMC.rollover2MC, cta2MC.rollover1MC, cta2MC.rollover2MC], 0.4, {alpha: 0.0});
			ctaHover = false;
			
		}
		
		var ctaTL = new TimelineMax({paused:true, onComplete: function(){
			if(ctaHover) ctaTL.restart();
		}});
		
		ctaTL.fromTo([ctaMC.arrow1, cta2MC.arrow1], 0.3, {x: -10}, {x: 4, ease: Expo.easeIn}, 0.0)
			 .fromTo([ctaMC.arrow2, cta2MC.arrow2], 0.3, {x: -13}, {x: 1, ease: Expo.easeIn}, 0.05)
			 .set([ctaMC.arrow1, cta2MC.arrow1], {x:-25})
			 .set([ctaMC.arrow2, cta2MC.arrow2], {x:-28})
			 .to([ctaMC.arrow1, cta2MC.arrow1], 0.5, {x: -10, ease: Expo.easeOut}, 0.35)
			 .to([ctaMC.arrow2, cta2MC.arrow2], 0.5, {x: -13, ease: Expo.easeOut}, 0.45);
		
		function doAutoRotate() {
			productID++;
			bigJump = false;
			changeProduct();
			if (productID < numberOfProducts - 1) {
				TweenMax.delayedCall(autoRotateTime, doAutoRotate);
			}
		}
		self.cancelAutoRotate = function() {
			TweenMax.killDelayedCallsTo(doAutoRotate);
		}
		
		function animateInCreative() {
			
			var d = 0.1;
			
			if (VARIATION != "dollar" && VARIATION !="merkenweek") {
				TweenMax.to(mmMC, 1.1, {y: 203, ease: Expo.easeInOut, delay: d});
				
				TweenMax.to(coverMC, 1.2, {y: 143, ease: Expo.easeInOut, delay: d, onComplete: function() {
					coverMC.visible = false;
				}});
				
				d += 1.05;
			}
			/*
			TweenMax.to(mmMC, 0.6, {rotation: 3, ease: Quart.easeIn, delay: d});
			TweenMax.to(mmMC, 0.6, {rotation: 0, ease: Expo.easeOut, delay: d + 0.6});
			TweenMax.to(mmMC, 1.2, {scaleX: 0.15, scaleY: 0.15, x: 79, y: 228, ease: Expo.easeInOut, delay: d});
			*/
			
			//d += 0.35;
			var greyToX = 0;
			if (greyMC.currentFrame == 1) {
				greyToX = 0;
			}
			//TweenMax.to(greyMC, 0.4, {x: greyToX + 100, ease: Expo.easeIn, delay: d});
			//TweenMax.to(greyMC, 0.8, {x: greyToX, ease: Expo.easeOut, delay: d + 0.4});
			//TweenMax.to(cover2MC, 0.4, {x: 0, ease: Expo.easeIn, delay: d});
			//TweenMax.to(cover2MC, 0.8, {x: 0, ease: Expo.easeOut, delay: d + 0.4});
			
			TweenMax.to(mm2MC, 0.9, {x: mm2ToX, ease: Expo.easeOut, delay: d + 0.6});
			TweenMax.to(mm2MC, 0.8, {alpha: 1.0, delay: d + 0.6});
			
			
			//self.logobalk.scaleX=self.logobalk.scaleY=0.9;
			
			d += 0.1;
			TweenMax.to(self.merkentext, 0.5, {x:130, alpha:1.0, ease:Sine.easeInOut, delay:d});
			d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:580, alpha:1.0,ease:Sine.easeInOut, delay:d});
			
			
			d += 2.1;
			
			//TweenMax.to(self.merkentext, 0.5, {y:60, ease:Sine.easeInOut, delay:d});
			//d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:-300, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			self.intro_img.scaleX=self.intro_img.scaleY=0.9;
			d += 0.5;
			
			TweenMax.to(self.intro_text, 0.5, {x:85,ease:Sine.easeInOut, delay:d});
			TweenMax.to(self.intro_img, 0.5, {x:850,ease:Sine.easeInOut, delay:d});
			
			d += 3.1;
			TweenMax.delayedCall(d, animateOutMerken);
			
			/*
			TweenMax.to(logoMC, 0.8, {y: logoToY, ease: Expo.easeOut, delay: d});
			TweenMax.to(logoMC, 0.5, {alpha: 1.0, delay: d});
			*/
			TweenMax.to(campaignMC, 0.5, {scaleX: campaignScale * 0.5, scaleY: campaignScale * 0.5, ease: Expo.easeIn, delay: d});
			TweenMax.to(campaignMC, 0.5, {scaleX: campaignScale, scaleY: campaignScale, ease: Elastic.easeOut.config(2.0, 1.0), delay: d + 0.5});
			TweenMax.from(campaignMC, 0.9, {rotation: -30, ease: Expo.easeInOut, delay: d});
			
			d += 0.3;
			
			d += 0.2;
			if (VARIATION == "dollar") {
				var d0ToY = 69;
				self.d0.y = d0ToY + 50;
				self.d1.y = d0ToY + 43 + 50;
				self.d2.y = d0ToY + 89;
				self.d2.alpha = 0;
				
				self.doll0.rotation = -10;
				self.doll1.rotation = -30;
				self.doll2.rotation = 20;
				self.doll0.scaleX = self.doll0.scaleY = 1 * 0.75;
				self.doll1.scaleX = self.doll1.scaleY = 1 * 0.75;
				self.doll2.scaleX = self.doll2.scaleY = 1 * 0.75;
				self.doll0.x = 880 + 30;
				self.doll1.x = 570 + 40;
				self.doll2.x = 60 + 30;
				
				TweenMax.from(self.d0, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
				TweenMax.to(self.d0, 0.8, {y: d0ToY, ease: Expo.easeOut, delay: d});
				
				d -= 0.2;
				TweenMax.to(self.doll0, 1.6, {x: 880, y: 50, scaleX: 1, scaleY: 1, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll0, 0.4, {rotation: 30, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll0, 0.6, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.2});
				
				d += 0.15;
				TweenMax.to(self.doll1, 2.1, {x: 570, y: 215, scaleX: 1, scaleY: 1, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll1, 0.5, {rotation: 20, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll1, 0.7, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.5});
			
				d += 0.15;
				TweenMax.to(self.doll2, 1.7, {x: 60, y: 35, scaleX: 1, scaleY: 1, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll2, 0.4, {rotation: -30, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll2, 0.7, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.2});
				d -= 0.3;
				
				d += 0.2 + 0.2;
				TweenMax.from(self.d1, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
				TweenMax.to(self.d1, 0.8, {y: d0ToY + 43, ease: Expo.easeOut, delay: d});
				
				d += 0.2;
				TweenMax.from(self.d2, 0.7, {x: 323, ease: Expo.easeOut, delay: d});
				TweenMax.to(self.d2, 0.0, {alpha: 1.0, delay: d});
				
				d += 2.5;
				TweenMax.to(self.doll1, 0.65, {y: dynamic.height + 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.doll0, 0.7, {y: dynamic.height + 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d + 0.05});
				TweenMax.to(self.doll2, 0.8, {y: dynamic.height + 60, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d + 0.07});
				TweenMax.to(self.doll1, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.doll2, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.doll0, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.d1, 0.4, {y: 115 + 50, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.d1, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				TweenMax.to(self.d2, 0.3, {alpha: 0.0, delay: d});
				TweenMax.to(self.d2, 0.3, {y: "+= 20", ease: Expo.easeIn, delay: d});
				TweenMax.to(self.d2, 0.7, {rotation: 4, ease: Expo.easeInOut, delay: d});
				
				d += 0.075;
				TweenMax.to(self.d0, 0.4, {y: 92 + 50, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.d0, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				d += 0.4;
			}
			if (VARIATION == "merkenweek" && productID==0) {
				self.dollarcashback.scaleX = self.dollarcashback.scaleY= 1.3;
			
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: 650,y:50, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:75,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, .3, { scaleX: 0, ease: Power3.easeIn, delay: 2});
				
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 1.3, scaleY: 1.3,  ease: Power3.easeOut, delay: 2.3});
		TweenMax.delayedCall(d, animateInManiaLabels);		
		}
			else if (VARIATION == "merkenweek") {
				
				
				TweenMax.delayedCall(d, animateInManiaLabels);
				
				d += 0.5;
			}
			TweenMax.from(text1MC, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text1MC, 0.8, {y: text1ToY, ease: Expo.easeOut, delay: d});
			
			TweenMax.delayedCall(d - 1.5, checkNumberOfProducts);
			
			d += 0.2;
			TweenMax.from(text2MC, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text2MC, 0.8, {y: text2ToY, ease: Expo.easeOut, delay: d});
			
			if (productsObject[productID] == "video") {
				//dynamic.playVideo(1);
				//videoPlayer.addEventListener("timeupdate", checkVideoCurrentTime, false);
				videoPlayer.addEventListener("volumechange", checkVideoMuted, false);
				videoPlayer.addEventListener("play", checkVideoPlayPause, false);
				videoPlayer.addEventListener("pause", checkVideoPlayPause, false);
				checkVideoMuted();
				
				var div = document.createElement("div");
				div.style.width = "30px";
				div.style.height = "30px";
				div.style.left = "50px";
				div.style.top = "220px";
				div.id = "unmute";
				document.getElementById("dom_overlay_container").appendChild(div);
				document.getElementById("unmute").addEventListener("click", doMuteUnmute, false);
				
				div = document.createElement("div");
				div.style.width = "20px";
				div.style.height = "30px";
				div.style.left = "80px";
				div.style.top = "220px";
				div.id = "pause";
				document.getElementById("dom_overlay_container").appendChild(div);
				document.getElementById("pause").addEventListener("click", doPlayPause, false);
				
				endVideoMC.x = 620; //endToX;
				TweenMax.delayedCall(d - 0.1, animateInVideo);
				
				TweenMax.delayedCall(d + 0.2, animateInEndframe, [endVideoMC]);
			}
			
			if (text3MC.dynText.text != "") {
				d += 0.3;
				TweenMax.to(text3MC, 0.7, {x: text3ToX, ease: Expo.easeOut, delay: d});
				TweenMax.to(text3MC, 1.0, {alpha: 1.0, delay: d});
				d -= 0.6;
			} else {
				d -= 0.3;
			}
			
			d += 0.2;
			TweenMax.to(productMC, 1.3, {x: productToX, ease: Expo.easeOut, delay: d});
			
			if (VARIATION == "dollar") {
				TweenMax.delayedCall(d + 0.1, animateInCash);
			}
			
			d += 0.2;
			TweenMax.to(leftMC, 0.9, {x: leftToX, ease: Expo.easeOut, delay: d});
			TweenMax.to(leftMC, 0.8, {alpha: 1.0, delay: d});
			TweenMax.to(rightMC, 0.9, {x: rightToX, ease: Expo.easeOut, delay: d});
			TweenMax.to(rightMC, 0.8, {alpha: 1.0, delay: d});
			
			if (servButtonToX < dynamic.width) {
				d += 0.3;
				TweenMax.to(servButtonMC, 0.6, {x: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: d});
				d -= 0.3;
			}
			
			d += 0.2;
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				TweenMax.delayedCall(d, animateInPrice);
			}
			
			TweenMax.delayedCall(d, animateInLabels);
			
			d += 0.2;
			TweenMax.delayedCall(d, animateInLabels2);
			
			//if (productsObject[productID].label == true) {
			if (self.cashback.dynText.text.length > 0) {
				d += 0.2;
				TweenMax.to(self.cashback, 0.6, {x: labelToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: d});
			}
			
			d += -0.2;
			TweenMax.to(ctaMC, 0.7, {alpha: 1.0, delay: d});
			TweenMax.to(ctaMC, 0.8, {x: ctaToX, ease: Expo.easeOut, delay: d});
			
		}
		
		function doLeftRightClick(event) {
			//console.log(event.target)
			var maxID = numberOfProducts - numberOfServices - 1;
			var minID = 0;
			if (productsObject[productID].service) {
				maxID = numberOfProducts - 1;
				minID = numberOfProducts - numberOfServices;
			} 
			if (event.target.name == "left" || event.target == leftButtonDiv) {
				if (productID > minID) {
					productID--;
					bigJump = false;
				} else {
					bigJump = true;
					productID = maxID;
				}
				isLeftClick = true;
			} else {
				if (productID < maxID) {
					productID++;
					bigJump = false;
				} else {
					bigJump = true;
					productID = minID;
				}
				isLeftClick = false;
			}
			self.cancelAutoRotate();
			changeProduct();
		}
		
		function adjustTextHeader() {
			
			var H1TextField = text1MC.dynText;
			var H2TextField = text2MC.dynText;
			
			var headerText1Width = H1TextField.getMeasuredWidth();
			var headerText2Width = H2TextField.getMeasuredWidth();
			/*
			if (H2TextField.getMeasuredWidth() > headerTextWidth) {
				headerTextWidth = H2TextField.getMeasuredWidth();
			}
			*/
			var idealHeaderTextWidth = 285;
			
			var headerFontScale = 1.0;
			if (headerText1Width > idealHeaderTextWidth) {
				headerFontScale = 1 / (headerText1Width / idealHeaderTextWidth);
			}
			text1MC.scaleX = text1MC.scaleY = headerFontScale;
			
			headerFontScale = 1.0;
			if (headerText2Width > idealHeaderTextWidth) {
				headerFontScale = 1 / (headerText2Width / idealHeaderTextWidth);
			}
			text2MC.scaleX = text2MC.scaleY = headerFontScale;
			
		}
		
		function updateLogoTextsLabelPrice() {
			
			text3MC.x = text3ToX + 30;
			text1MC.dynText.text = productsObject[productID].name;
			text2MC.dynText.text = productsObject[productID].main_specs;
			//text3MC.dynText.text = productsObject[productID].sub_specs;
			adjustTextHeader();
			
			logoMC.y = - 70;
			labelMC.x = dynamic.width + 50;
			self.cashback.x = labelMC.x;
			for (var i = 0; i<numberOfProducts; i++) {
				if (i == productID && logoMC.children[i] != undefined) {
					logoMC.children[i].alpha = labelMC.children[i].alpha = 1.0;
				} else if (logoMC.children[i] != undefined) {
					logoMC.children[i].alpha = labelMC.children[i].alpha = 0.0;
				}
			}
			
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf([a, a.gr.l0, a.gr.l1, a.gr]);
				self["g" + i].text.gotoAndStop(0);
			}
			if (productsObject[productID].sub_specs != undefined && productsObject[productID].sub_specs != "") {
				var s = productsObject[productID].sub_specs.split("^");
				if (s[0].split("_")[0].toLowerCase() == "bullet") {
					for (i = 0; i<maxGreen; i++) {
						var ID = 0;
						var a = self["g" + i];
						a.x = gToX;
						a.y = a.toY + 25;
						a.gr.l0.scaleX = a.gr.l1.scaleX = 0;
						a.gr.scaleX = a.gr.scaleY = 0.0;
						if (s[i] != undefined && s[i].split("_")[1] != undefined) {
							switch (s[i].split("_")[1].toLowerCase()) {
								case "bezorging":
								ID = 1;
								break;
								
								case "aansluiting":
								ID = 2;
								break;
								
								case "oud":
								ID = 3;
								break;
								
								case "retouneren":
								ID = 4;
								break;
								
								case "30dagen":
								ID = 5;
								break;
								
								case "plaatsing":
								ID = 5;
								break;
							}
						}
						a.text.gotoAndStop(ID);
					}
				}
			}
			
			var mu = 0;
			if (productMC.children[productID].green == true) {
				mu = 25;
			}
			mask.y = 16 - mu;
			mask_1.y = 0.5 - mu;
			text1ToY = 53 - mu;
			text2ToY = 57.5 - mu;
			text1MC.y = text1ToY + 35;
			text2MC.y = text2ToY + 35;
			
			self.servicelabel.gotoAndStop(0);
			self.cashback.dynText.text = "";
			while (self.cashback.bgr.numChildren > 0) {
				self.cashback.bgr.removeChild(self.cashback.bgr.children[0]);
			}
			var z = productsObject[productID].label_new;
			var lab2W = 0;
			if (z != undefined && z != "") {
				if (z.split("_")[0].toLowerCase() == "cashback") {
					self.cashback.dynText.text = z.split("_")[1];
					lab2W = Math.floor(self.cashback.dynText.getBounds().width) + 21;
					if (self.cashback.dynText.getMeasuredHeight() > 17) {
						//lab2W = 95;
					}
				} else if (z.split("_")[0].toLowerCase() == "service") {
					var b = z.split("_")[1].toLowerCase();
					var ID = 0;
					switch (b) {
						case "a": 
						ID = 1;
						break;
						
						case "b": 
						ID = 2;
						break;
						
						case "c": 
						ID = 3;
						break;
						
						case "d": 
						ID = 4;
						break;
						
						case "e": 
						ID = 5;
						break;
						
						case "f": 
						ID = 6;
						break;
						
						case "g": 
						ID = 7;
						break;
						
						case "h": 
						ID = 8;
						break;
						
						case "i": 
						ID = 9;
						break;
					}
					self.servicelabel.gotoAndStop(ID);
				}
			}
			/*
			if (self.servicelabel.currentFrame == 9) {
				self.servicelabel.y = 154;
			} else {
				self.servicelabel.y = 63;
			}
			*/
			if (self.cashback.dynText.text.length > 0) {
				var cashRect = new createjs.Shape();
				cashRect.graphics.beginFill("#e90000").drawRoundRectComplex(0, 0, lab2W, Math.floor(self.cashback.dynText.getMeasuredHeight()) + 15, 6, 6, 6, 6);
				self.cashback.bgr.addChild(cashRect);
			}
			
			lineMC.x = lineToX;
			TweenMax.killTweensOf(lineMC.line);
			lineMC.line.scaleX = 0;
			lineMC.alpha = 1.0;
			priceMC.x = priceToX;
			priceMC.alpha = 1.0;
			vanafMC.dynText.text = "";
			vanafMC.x = vanafToX + 15;
			self.prijs.x = prijsToX + 20;
			while (priceMC.numChildren > 0) {
				for (var k = 0; k<priceMC.children[0].numChildren; k++) {
					TweenMax.killTweensOf(priceMC.children[0].children[k]);
					for (var h = 0; h<priceMC.children[0].children[k].numChildren; h++) {
						TweenMax.killTweensOf(priceMC.children[0].children[k].children[h]);
					}
				}
				priceMC.removeChild(priceMC.children[0]);
			}
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				var newPriceMC = new createjs.MovieClip();
				priceMC.addChild(newPriceMC);
				var priceStr = productsObject[productID].price.split(" ");
				if (priceStr.length > 1) {
					vanafMC.dynText.text = priceStr[0].toUpperCase();
					priceStr = priceStr[1].split("");
				} else {
					priceStr = priceStr[0].split("");
				}
				for (i = 0; i<priceStr.length; i++) {
					var childMC = new createjs.MovieClip();
					newPriceMC.addChild(childMC);
					
					var charMC = new createjs.MovieClip();
					//var maskRect = new createjs.Shape();
					childMC.addChild(charMC);
					if (priceStr[i] != ".") {
						var numberID = Number(priceStr[i]) + 1;
						if (greyMC.currentFrame == 1) {
							numberID += 12;
						}
						dynamic.setDynamicImage(charMC, (NUMBERS + numberID), 0, 0);
					} else {
						var numbi = IMAGE_COUNT - 1;
						if (greyMC.currentFrame == 0) {
							numbi -= 12;
						}
						dynamic.setDynamicImage(charMC, (numbi), 0, 0);
					}
					if (i > 0) {
						childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale) - 9;
						if (priceStr[(i - 1)] != "." && newPriceMC.dot == true) {
							childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.7) - 6;
						}
					} else {
						childMC.toX = 0;
					}
					childMC.scaleX = childMC.scaleY = numberImgScale;
					if (newPriceMC.dot) {
						childMC.scaleX = childMC.scaleY = numberImgScale * 0.7;
						childMC.y += 1;
					}
					if (priceStr[i] == ".") {
						childMC.toX -= 2;
						newPriceMC.dot = true;
					}
					if (priceStr[(i - 1)] == ".") {
						childMC.toX -= 4;
					}
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					
				}
				if (!newPriceMC.dot) {
					var childMC = new createjs.MovieClip();
					newPriceMC.addChild(childMC);
					var charEndingMC = new createjs.MovieClip();
					childMC.addChild(charEndingMC);
					var numbi = IMAGE_COUNT;
					if (greyMC.currentFrame == 0) {
						numbi -= 12;
					}
					dynamic.setDynamicImage(charEndingMC, (numbi), 0, 0);
					childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale) - 10;
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					childMC.scaleX = childMC.scaleY = numberImgScale;
				}
			}
			
			if (productsObject[productID].priceOld != "" && productsObject[productID].priceOld != "-" && !isNaN(productsObject[productID].priceOld) && newPriceMC != undefined) {
				newPriceMC.oldPrice = true;
				var oldPriceMC = new createjs.MovieClip();
				newPriceMC.addChild(oldPriceMC);
				var oldPriceStr = productsObject[productID].priceOld.split("");
				oldPriceMC.y = -40;
				oldPriceMC.x += 2;
				for (i = 0; i<oldPriceStr.length; i++) {
					var childMC = new createjs.MovieClip();
					oldPriceMC.addChild(childMC);
					var charMC = new createjs.MovieClip();
					//var maskRect = new createjs.Shape();
					childMC.addChild(charMC);
					if (oldPriceStr[i] != ".") {
						var numberID = Number(oldPriceStr[i]) + 1;
						dynamic.setDynamicImage(charMC, (NUMBERS + numberID), 0, 0);
					} else {
						dynamic.setDynamicImage(charMC, (IMAGE_COUNT - 1 - 12), 0, 0);
					}
					if (i > 0) {
						childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.6) - 9 * 0.6;
						if (oldPriceStr[(i - 1)] != "." && oldPriceMC.dot == true) {
							childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.7 * 0.6) - 6 * 0.6;
						}
					} else {
						childMC.toX = 0;
					}
					childMC.scaleX = childMC.scaleY = numberImgScale * 0.6;
					if (oldPriceMC.dot) {
						childMC.scaleX = childMC.scaleY = numberImgScale * 0.7 * 0.6;
						childMC.y += 1;
					}
					if (oldPriceStr[i] == ".") {
						childMC.toX -= 2;
						oldPriceMC.dot = true;
					}
					if (oldPriceStr[(i - 1)] == ".") {
						childMC.toX -= 3;
					}
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
				}
				if (!oldPriceMC.dot) {
					var childMC = new createjs.MovieClip();
					oldPriceMC.addChild(childMC);
					var charEndingMC = new createjs.MovieClip();
					childMC.addChild(charEndingMC);
					dynamic.setDynamicImage(charEndingMC, (IMAGE_COUNT - 12), 0, 0);
					childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.6) - 10 * 0.6;
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					childMC.scaleX = childMC.scaleY = numberImgScale * 0.6;
				}
				oldPriceMC.priceWidth = oldPriceMC.children[oldPriceMC.numChildren - 1].toX + 9;
				if (oldPriceMC.dot == true) {
					oldPriceMC.priceWidth += 10;
				}
			}
		}
		
		
		function animateInPrice() {
			var currentPrice = priceMC.children[priceMC.numChildren - 1];
			var indexTill = currentPrice.numChildren;
			var delayPrice = 0.0;
			TweenMax.killTweensOf(lineMC.line);
			lineMC.line.scaleX = 0.0;
			if (currentPrice.oldPrice == true) {
				indexTill--;
				lineMC.scaleX = 1 / (90 / currentPrice.children[currentPrice.numChildren - 1].priceWidth);
				for (var j = 0; j<currentPrice.children[currentPrice.numChildren - 1].numChildren; j++) {
					var oldNumberMC = currentPrice.children[currentPrice.numChildren - 1].children[j];
					TweenMax.to(oldNumberMC, 0.8, {x: oldNumberMC.toX, alpha: 1.0, ease: Expo.easeOut, delay: j * 0.03});
				}
				delayPrice = 0.3;
				TweenMax.to(lineMC.line, 0.7, {scaleX: 1.0, ease: Expo.easeOut, delay: 0.15});
			}
			for (var i = 0; i<indexTill; i++) {
				var numberMC = currentPrice.children[i];
				TweenMax.to(numberMC, 0.9, {x: numberMC.toX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + i * 0.03});
			}
			TweenMax.to(vanafMC, 0.9, {x: vanafToX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + 0});
			if (VARIATION == "dollar" && productID == 1) {
				TweenMax.to(self.prijs, 0.9, {x: prijsToX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + 0});
			}
		}
		
		function changeProduct() {
			
			dynamic.productID = productID;
			
			var d = 0.0;
			TweenMax.killDelayedCallsTo(animateInManiaLabels);
			TweenMax.killDelayedCallsTo(updateLogoTextsLabelPrice);
			TweenMax.killDelayedCallsTo(animateInPrice);
			TweenMax.killDelayedCallsTo(editServicesButton);
			TweenMax.killDelayedCallsTo(animateInEndframe);
			TweenMax.killDelayedCallsTo(animateOutEndframe);
			TweenMax.killDelayedCallsTo(animateInLabels);
			TweenMax.killDelayedCallsTo(animateInLabels2);
			TweenMax.killDelayedCallsTo(animateInCheck);
			TweenMax.killTweensOf([text1MC, text2MC, text3MC, productMC, logoMC, labelMC, self.cashback, self.servicelabel, priceMC, lineMC, lineMC.line, ctaMC, cta2MC, vanafMC, servButtonMC, leftMC, rightMC, self.prijs, self.dollarcashback, self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3, self.subsubtext]);
			TweenMax.to([logoMC, self.cashback, priceMC, lineMC, vanafMC, self.subsubtext], 0.3, {alpha: 0.0});
			var moveBy = 100;
			if (isLeftClick == false || isLeftClick == undefined) {
				moveBy = -moveBy;
			}
			//TweenMax.to(labelMC, 0.4, {x: "+=" + (moveBy + 50), ease: Expo.easeIn});
			TweenMax.to(self.cashback, 0.4, {x: "+=" + (moveBy + 50), ease: Expo.easeIn});
			TweenMax.to(text3MC, 0.3, {x: text3ToX + moveBy, ease: Expo.easeIn, delay: 0.05});
			TweenMax.to(priceMC, 0.4, {x: priceToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			TweenMax.to(vanafMC, 0.4, {x: vanafToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			TweenMax.to(lineMC, 0.4, {x: lineToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			if (VARIATION == "dollar") {
				TweenMax.to([self.prijs, self.dollarcashback, self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.3, {alpha: 0.0});
				TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			if (VARIATION == "merkenweek") {
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:75,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			if (VARIATION == "merkenweek" && productID==0) {
				self.dollarcashback.scaleX = self.dollarcashback.scaleY= 1.3;
			
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: 650,y:50, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:75,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, .3, { scaleX: 0, ease: Power3.easeIn, delay: 1.5});
				
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 1.3, scaleY:1.3, ease: Power3.easeOut, delay: 1.8});
				}
			if (VARIATION == "merkenweek" && productID==3) {
				self.dollarcashback.scaleX = self.dollarcashback.scaleY=1.5;
			
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:75,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf(a);
				if (a.text.currentFrame > 0) {
					TweenMax.to(a, 0.4, {x: gToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
					TweenMax.to(a, 0.3, {alpha: 0.0, delay: 0.0});
				}
			}
			if (productsObject[productID].service && servButtonMC.dynText.text == "BEKIJK ONZE SERVICES") {
				TweenMax.to(servButtonMC, 0.4, {x: dynamic.width, alpha: 0.0, ease: Expo.easeIn, delay: 0});
				TweenMax.to(servButtonMC, 0.6, {x: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0.4});
				TweenMax.to(servButtonMC, 0.4, {alpha: 1.0, delay: 0.4});
				TweenMax.delayedCall(0.4, editServicesButton, ["BEKIJK ONZE DEALS"]);
			}
			if (!productsObject[productID].service && servButtonMC.dynText.text == "BEKIJK ONZE DEALS") {
				TweenMax.to(servButtonMC, 0.4, {x: dynamic.width, ease: Expo.easeIn, alpha: 0.0, delay: 0});
				TweenMax.to(servButtonMC, 0.6, {x: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0.4});
				TweenMax.to(servButtonMC, 0.4, {alpha: 1.0, delay: 0.4});
				TweenMax.delayedCall(0.4, editServicesButton, ["BEKIJK ONZE SERVICES"]);
			}
			
			TweenMax.to(text3MC, 0.3, {alpha: 0.0});
			
			var productTargetX = productToX - productID * 1272;
			if (bigJump == true) {
				var productTargetPrevX = productTargetX - 75;
				if (!(productID == 0) && prevProductID <= numberOfProducts - numberOfServices - 1) {
					productTargetPrevX += 150;
				} else if (productID == numberOfProducts - 1) {
					productTargetPrevX += 150;
				}
				TweenMax.to(productMC, 0.8, {x: productTargetPrevX, ease: Expo.easeIn, delay: d});
				TweenMax.to(productMC, 0.6, {x: productTargetX, ease: Expo.easeOut, delay: d + 0.8});
			} else {
				TweenMax.to(productMC, 1.2, {x: productTargetX, ease: Expo.easeInOut, delay: d});
			}
			prevProductID = productID;
			
			if (numberOfServices > 0) {
				var whichCtaMC = ctaMC;
				if (productsObject[productID].service) {
					TweenMax.to(ctaMC, 0.9, {x: ctaToX + dynamic.width / 2, ease: Expo.easeInOut, delay: d});
					TweenMax.to(ctaMC, 0.4, {alpha: 0.0, delay: d});
					TweenMax.to(cta2MC, 0.9, {x: ctaToX, ease: Expo.easeInOut, delay: d});
					TweenMax.to(cta2MC, 1.3, {alpha: 1.0, delay: d});
					whichCtaMC = cta2MC;
				} else {
					TweenMax.to(ctaMC, 0.9, {x: ctaToX, ease: Expo.easeInOut, delay: d});
					TweenMax.to(ctaMC, 1.3, {alpha: 1.0, delay: d});
					TweenMax.to(cta2MC, 0.9, {x: ctaToX - 200, ease: Expo.easeInOut, delay: d});
					TweenMax.to(cta2MC, 0.7, {alpha: 0.0, delay: d});
				}
				editCtaButtonDivWidth(whichCtaMC.ctaTextWidth);
			}
			
			TweenMax.to(self.servicelabel, 0.4, {y: -100, ease: Expo.easeIn, delay: d});
			
			TweenMax.to(text2MC, 0.4, {y: text2ToY + 35, ease: Expo.easeInOut, delay: d});
			TweenMax.to(text2MC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			
			d += 0.075;
			TweenMax.to(text1MC, 0.4, {y: text1ToY + 35, ease: Expo.easeInOut, delay: d});
			TweenMax.to(text1MC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			
			d += 0.4;
			TweenMax.delayedCall(d, updateLogoTextsLabelPrice);
			
			if (typeof(productsObject[productID]) == "string") {
				TweenMax.to(productMC, 0.3, {alpha: 0.0, delay: d - 0.3});
				if (productsObject[productID] == "video") {
					TweenMax.delayedCall(d, animateInEndframe, [endVideoMC]);
				} else {
					TweenMax.delayedCall(d, animateInEndframe, [endMC]);
				}
				leftToX = 17;
			} else {
				leftToX = 559;
				TweenMax.to(productMC, 0.6, {alpha: 1.0, delay: d + 0.2});
				TweenMax.delayedCall(d - 0.5, animateOutEndframe);
			}
			if (productsObject[productID].service) {
				if (numberOfServices <= 1) {
					leftButtonDiv.style.display = rightButtonDiv.style.display = "none";
					TweenMax.to(leftMC, 0.8, {x: leftToX + 15, ease: Expo.easeOut, delay: d - 0.2});
					TweenMax.to(rightMC, 0.8, {x: rightToX - 15, ease: Expo.easeOut, delay: d - 0.2});
					TweenMax.to(leftMC, 0.4, {alpha: 0, delay: d - 0.4});
					TweenMax.to(rightMC, 0.4, {alpha: 0, delay: d - 0.4});
				} else {
					leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
					TweenMax.to(leftMC, 0.8, {x: leftToX, alpha: 1.0, ease: Expo.easeOut, delay: d});
					TweenMax.to(rightMC, 0.8, {x: rightToX, alpha: 1.0, ease: Expo.easeOut, delay: d});
				}
			} else if (numberOfProducts > 1) {
				TweenMax.to(leftMC, 0.8, {x: leftToX, alpha: 1, ease: Expo.easeOut, delay: d});
				TweenMax.to(rightMC, 0.8, {x: rightToX, alpha: 1, ease: Expo.easeOut, delay: d});
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
			}
			//leftButtonDiv.style.left = (leftToX + 125) + "px";
			/*
			TweenMax.to(logoMC, 0.8, {y: logoToY, ease: Expo.easeOut, delay: d});
			TweenMax.to(logoMC, 0.5, {alpha: 1.0, delay: d});
			*/
			
			if (VARIATION == "dollar") {
				TweenMax.delayedCall(d + 0.1, animateInCash);
			}
			if (VARIATION == "merkenweek") {
				//TweenMax.delayedCall(d + 0.1, animateInStripe);
				TweenMax.delayedCall(d, animateInManiaLabels);
			}
			//d += 0.1;
			TweenMax.delayedCall(d, animateInTexts);
			
			if (text3MC.dynText.text != "") {
				d += 0.1;
				TweenMax.to(text3MC, 0.7, {x: text3ToX, ease: Expo.easeOut, delay: d});
				TweenMax.to(text3MC, 1.0, {alpha: 1.0, delay: d});
			}
			
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				d += 0.2;
				TweenMax.delayedCall(d, animateInPrice);
			}
			
			TweenMax.delayedCall(d, animateInLabels);
			
			//if (productsObject[productID].label == true) {
			d += 0.2;
			TweenMax.delayedCall(d, animateInLabels2);
			
		}
		
		function animateInManiaLabels() {
			var k = self.subsubtext;
			k.x = maniaLabelToX;
			k.alpha = 1.0;
			k.gotoAndStop(productID);
			var s = 0;
			while (k["m" + productID + s] != undefined) {
				s++;
			}
			var howMany = s;
			for (var i = 0; i<howMany; i++) {
				var n = k["m" + productID + i]
				console.log(n)
				TweenMax.from(n, 0.8, {x: 50, alpha: 0.0, delay: i * 0.05});
			}
		}
		function animateInCash() {
			var cash = self["dollProd" + productID];
			cash.rotation = -10;
			var toX = [610, 425, 365, 570];
			var toY = [50, 165, 160, 105];
			var d = 0.0;
			cash.x = toX[productID];
			cash.y = toY[productID];
			cash.scaleX = cash.scaleY = 0.0;
			cash.alpha = 1.0;
			//cash.x = dynamic.width + 50;
			//TweenMax.to(cash, 1.0, {y: toY[productID], ease: Sine.easeOut, delay: d});
			TweenMax.to(cash, 0.2, {rotation: 10, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d + 0.5});
			TweenMax.to(cash, 0.8, {rotation: 0, ease: Elastic.easeOut.config(1.0, 1.0), delay: d + 1.1});
			TweenMax.to(cash, 1.0, {scaleX: 1.0, scaleY: 1.0, ease: Elastic.easeOut.config(1.0, 0.8), delay: d + 0.5});
			
			d += 0.1;
			var order = [1, 0, 2];
			for (var i = 0; i<3; i++) {
				var cash2 = self["dollRandom" + productID + i];
				cash2.rotation = Math.floor(Math.random() * 60) - 30;
				cash2.scaleX = cash2.scaleY = Math.floor(Math.random() * 30) / 100 + 0.7;
				var toRot = -cash2.rotation;
				cash2.y = -50;
				cash2.x = 350 + i * 140 + Math.floor(Math.random() * 50);
				var time = Math.floor(Math.random() * 10) / 10 + 1.2;
				TweenMax.to(cash2, time, {x: "-= 30", y: dynamic.height + 60, ease: Sine.easeOut, delay: d + order[i] * 0.3});
				TweenMax.to(cash2, 0.5, {rotation: toRot, ease: Sine.easeInOut, repeat: 6, yoyo: true, delay: d + order[i] * 0.3});
				//TweenMax.to(cash2, 0.6, {rotation: 0, ease: Cubic.easeOut, delay: d + i * 0.1 + 1.2});
			}
		}
		
		function animateInTexts() {
			var d = 0.0;
			TweenMax.to(text1MC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text1MC, 0.8, {y: text1ToY, ease: Expo.easeOut, delay: d});
			
			d += 0.2;
			TweenMax.to(text2MC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text2MC, 0.8, {y: text2ToY, ease: Expo.easeOut, delay: d});
		}
		
		function animateOutMerken() {
			
		
			var d = 0.0;
			TweenMax.to(self.merkentext, 0.5, {x:-1000, ease:Sine.easeInOut, delay:d});
			d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:-900, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			d += 0.1;
			TweenMax.to(self.intro_img, 0.5, {x:1500, alpha:0.0,ease:Sine.easeInOut, delay:d});
			TweenMax.to(self.intro_text, 0.5, {x:-1000, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			d += 0.5;
			
			
		}
		
		function buildServicesButton(caption) {
			
			var servTextField = servButtonMC.dynText;
			editServicesButton(caption);
			var servButtonHeight = servTextField.getMeasuredHeight() + 30;
			var servRect = new createjs.Shape();
			servRect.graphics.beginFill("#ffffff").drawRoundRectComplex(0, 0, 100, servButtonHeight, 10, 10, 0, 0);
			servButtonMC.addChild(servRect);
			servButtonMC.swapChildren(servButtonMC.children[0], servButtonMC.children[1]);
			servButtonToX = dynamic.width - servButtonHeight + 12;
			
		}
		
		function editServicesButton(caption) {
			
			var servTextField = servButtonMC.dynText;
			servTextField.text = caption;
		
		}
		
		function doServices() {
			self.cancelAutoRotate();
			if (servButtonMC.dynText.text == "BEKIJK ONZE DEALS") {
				productID = 0;
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
			} else {
				productID = numberOfProducts - numberOfServices;
			}
			bigJump = true;
			changeProduct();
		}
		
		function animateInEndframe(whichEndMC) {
			var d = 0.0;
			for (var i = 0; i<lines; i++) {
				var lMC = whichEndMC["l" + i];
				d = i * 0.12;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
				TweenMax.to(lMC, 0.8, {y: lMC.toY, ease: Expo.easeOut, delay: d});
			}
		}
		
		function animateOutEndframe() {
			var d = 0.0;
			for (var i = 0; i<lines; i++) {
				var lMC = endMC["l" + i];
				d = (lines - i) * 0.05;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.4, {y: lMC.toY + 29, ease: Expo.easeInOut, delay: d});
				TweenMax.to(lMC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				lMC = endVideoMC["l" + i];
				d = (lines - i) * 0.05;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.4, {y: lMC.toY + 21, ease: Expo.easeInOut, delay: d});
				TweenMax.to(lMC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			}
		}
		
		function animateInLabels() {
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf([a, a.gr]);
				if (a.text.currentFrame > 0) {
					TweenMax.delayedCall(0 + i * 0.1, animateInCheck, [a]);
					TweenMax.to(a.gr, 0.6, {scaleX: 1.0, scaleY: 1.0, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0 + 0.1 + i * 0.075});
					TweenMax.to(a, 1.0, {y: a.toY, alpha: 1.0, ease: Expo.easeOut, delay: 0 + i * 0.075});
				}
			}
		}
		
		function animateInCheck(movieclip) {
			TweenMax.to(movieclip.gr.l0, 0.3, {scaleX: 1.0, ease: Expo.easeIn});
			TweenMax.to(movieclip.gr.l1, 0.5, {scaleX: 1.0, ease: Expo.easeOut, delay: 0.3});
		}
		function animateInLabels2() {
			if (self.cashback.dynText.text.length > 0) {
				TweenMax.to(self.cashback, 0, {alpha: 0.0, x: labelToX + 200});
				TweenMax.to(self.cashback, 0.6, {x: labelToX, ease: Elastic.easeOut.config(0.8, 1.2)});
				TweenMax.to(self.cashback, 0.7, {alpha: 1.0});
			} else if (self.servicelabel.currentFrame > 0) {
				//TweenMax.to(self.servicelabel, 0.6, {y: 30, ease: Expo.easeOut});
			}
			if (VARIATION == "dollar") {
				var animIn = true;
				self.dollarcashback.alpha = 1.0;
				self.dollarcashback.x = dynamic.width + 150;
				if (productID == 1) {
					self.dollarcashback.text.gotoAndStop(0);
				} else if (productID == 2) {
					self.dollarcashback.text.gotoAndStop(1);
				} else {
					animIn = false;
				}
				if (animIn == true) {
					TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
			}
			
			if (VARIATION == "merkenweek") {
				var animIn = true;
				self.dollarcashback.alpha = 1.0;
				self.dollarcashback.x = dynamic.width+150;
				if (productID == 0) {
					self.dollarcashback.text.gotoAndStop(1);
				TweenMax.delayedCall(0.9, gotoLastFrame);
				} 
				else if (productID == 3) {
					self.dollarcashback.text.gotoAndStop(0);
				} else {
					animIn = false;
				}
				if (animIn == true) {
					//TweenMax.to(self.dollarcashback, 0.6, {x: 620, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
			}
			
		}
		function gotoLastFrame(){
			
			self.dollarcashback.text.gotoAndStop(2);
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// border
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EhNVAAFIAAgJMCarAAAIAAAJg");
	this.shape.setTransform(635.9967,327.6552,1.305,1.2857);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_1.setTransform(635.5432,0.6507,3.6264,0.0553);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_2.setTransform(0.523,125.0111,0.0028,10.8002);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgEVGMAAAgqLIAJAAMAAAAqLg");
	this.shape_3.setTransform(1271.6382,164.0118,1.3,1.2889);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// elements_over
	this.dollProd3 = new lib.dollar_4_v2();
	this.dollProd3.name = "dollProd3";
	this.dollProd3.parent = this;
	this.dollProd3.setTransform(280.1,-40,1,1,0,0,0,0.1,0);

	this.dollProd2 = new lib.dollar_5_v2();
	this.dollProd2.name = "dollProd2";
	this.dollProd2.parent = this;
	this.dollProd2.setTransform(80,-200);

	this.dollProd0 = new lib.dollar_1_v3();
	this.dollProd0.name = "dollProd0";
	this.dollProd0.parent = this;
	this.dollProd0.setTransform(100,-129.7);

	this.doll2 = new lib.dollar_3_v3();
	this.doll2.name = "doll2";
	this.doll2.parent = this;
	this.doll2.setTransform(397.9,-229.55);

	this.doll1 = new lib.dollar_2();
	this.doll1.name = "doll1";
	this.doll1.parent = this;
	this.doll1.setTransform(200,-60);

	this.doll0 = new lib.dollar_1();
	this.doll0.name = "doll0";
	this.doll0.parent = this;
	this.doll0.setTransform(80,-255.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.doll0},{t:this.doll1},{t:this.doll2},{t:this.dollProd0},{t:this.dollProd2},{t:this.dollProd3}]}).wait(1));

	// text2Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A7VMqIAAl8MA2rAAAIAAF8g");
	mask.setTransform(174.9957,80.9904);

	// text2
	this.text2 = new lib.text2();
	this.text2.name = "text2";
	this.text2.parent = this;
	this.text2.setTransform(19.9,131.5,1,1,0,0,0,2.9,14.5);

	var maskedShapeInstanceList = [this.text2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.text2).wait(1));

	// text1Mask (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("A7VK8IAAqyMA2rAAAIAAKyg");
	mask_1.setTransform(174.9957,69.9983);

	// text1
	this.text1 = new lib.text1();
	this.text1.name = "text1";
	this.text1.parent = this;
	this.text1.setTransform(19.9,107,1,1,0,0,0,2.9,-2);

	var maskedShapeInstanceList = [this.text1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.text1).wait(1));

	// random_cash
	this.dollRandom00 = new lib.dollar_1_v2();
	this.dollRandom00.name = "dollRandom00";
	this.dollRandom00.parent = this;
	this.dollRandom00.setTransform(756.45,-51.25);

	this.dollRandom02 = new lib.dollar_1_v2();
	this.dollRandom02.name = "dollRandom02";
	this.dollRandom02.parent = this;
	this.dollRandom02.setTransform(861.45,-41.25);

	this.dollRandom01 = new lib.dollar_1_v2();
	this.dollRandom01.name = "dollRandom01";
	this.dollRandom01.parent = this;
	this.dollRandom01.setTransform(636.45,-41.25);

	this.dollRandom12 = new lib.dollar_3();
	this.dollRandom12.name = "dollRandom12";
	this.dollRandom12.parent = this;
	this.dollRandom12.setTransform(577.85,-83.05);

	this.dollRandom10 = new lib.dollar_3();
	this.dollRandom10.name = "dollRandom10";
	this.dollRandom10.parent = this;
	this.dollRandom10.setTransform(467.8,-43.05);

	this.dollRandom11 = new lib.dollar_3();
	this.dollRandom11.name = "dollRandom11";
	this.dollRandom11.parent = this;
	this.dollRandom11.setTransform(339.9,-99.2);

	this.dollRandom32 = new lib.dollar_4();
	this.dollRandom32.name = "dollRandom32";
	this.dollRandom32.parent = this;
	this.dollRandom32.setTransform(590.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom30 = new lib.dollar_4();
	this.dollRandom30.name = "dollRandom30";
	this.dollRandom30.parent = this;
	this.dollRandom30.setTransform(510.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom31 = new lib.dollar_4();
	this.dollRandom31.name = "dollRandom31";
	this.dollRandom31.parent = this;
	this.dollRandom31.setTransform(410.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom22 = new lib.dollar_5();
	this.dollRandom22.name = "dollRandom22";
	this.dollRandom22.parent = this;
	this.dollRandom22.setTransform(615,-130);

	this.dollRandom20 = new lib.dollar_5();
	this.dollRandom20.name = "dollRandom20";
	this.dollRandom20.parent = this;
	this.dollRandom20.setTransform(510,-126);

	this.dollRandom21 = new lib.dollar_5();
	this.dollRandom21.name = "dollRandom21";
	this.dollRandom21.parent = this;
	this.dollRandom21.setTransform(410,-125);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.dollRandom21},{t:this.dollRandom20},{t:this.dollRandom22},{t:this.dollRandom31},{t:this.dollRandom30},{t:this.dollRandom32},{t:this.dollRandom11},{t:this.dollRandom10},{t:this.dollRandom12},{t:this.dollRandom01},{t:this.dollRandom02},{t:this.dollRandom00}]}).wait(1));

	// arrows + logo + cta
	this.cashback = new lib.label_cashback();
	this.cashback.name = "cashback";
	this.cashback.parent = this;
	this.cashback.setTransform(1290,150);

	this.mediamarkt2 = new lib.mediamarkt_logo();
	this.mediamarkt2.name = "mediamarkt2";
	this.mediamarkt2.parent = this;
	this.mediamarkt2.setTransform(79.05,328.1,0.15,0.15,0,0,0,0.4,0.7);

	this.cover3 = new lib.shape3();
	this.cover3.name = "cover3";
	this.cover3.parent = this;
	this.cover3.setTransform(650,350,1,1,0,0,0,0,40);

	this.cta2 = new lib.cta();
	this.cta2.name = "cta2";
	this.cta2.parent = this;
	this.cta2.setTransform(-10,218);

	this.left = new lib.arrowLeftRight();
	this.left.name = "left";
	this.left.parent = this;
	this.left.setTransform(540,125,1,1,0,0,180);
	this.left.alpha = 0;
	this.left.visible = false;

	this.right = new lib.arrowLeftRight();
	this.right.name = "right";
	this.right.parent = this;
	this.right.setTransform(968,125);
	this.right.alpha = 0;
	this.right.visible = false;

	this.cta = new lib.cta();
	this.cta.name = "cta";
	this.cta.parent = this;
	this.cta.setTransform(1550,296);

	this.campaign = new lib.emptyMovieClip();
	this.campaign.name = "campaign";
	this.campaign.parent = this;
	this.campaign.setTransform(893,40);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.campaign},{t:this.cta},{t:this.right},{t:this.left},{t:this.cta2},{t:this.cover3},{t:this.mediamarkt2},{t:this.cashback}]}).wait(1));

	// svl_mask (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("A1eEsIAApXMAq9AAAIAAJXg");
	mask_2.setTransform(817.5,69);

	// servicelabel_lyr
	this.servicelabel = new lib.label_service();
	this.servicelabel.name = "servicelabel";
	this.servicelabel.parent = this;
	this.servicelabel.setTransform(680,-100);

	var maskedShapeInstanceList = [this.servicelabel];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.servicelabel).wait(1));

	// text1_mask (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("Egu3AEdIAAnfMBdvAAAIAAHfg");
	mask_3.setTransform(300.0025,28.4982);

	// text1
	this.d0 = new lib.dollar_text_1();
	this.d0.name = "d0";
	this.d0.parent = this;
	this.d0.setTransform(23,400);

	var maskedShapeInstanceList = [this.d0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.d0).wait(1));

	// text2_mask (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	mask_4.graphics.p("Egu3AH0IAAngMBdvAAAIAAHgg");
	mask_4.setTransform(300.0025,49.9982);

	// text2
	this.d1 = new lib.dollar_text_2();
	this.d1.name = "d1";
	this.d1.parent = this;
	this.d1.setTransform(23,444);

	var maskedShapeInstanceList = [this.d1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.d1).wait(1));

	// rest
	this.d2 = new lib.dollar_text_3();
	this.d2.name = "d2";
	this.d2.parent = this;
	this.d2.setTransform(23,489);

	this.timeline.addTween(cjs.Tween.get(this.d2).wait(1));

	// elements
	this.logobalk = new lib.logobalk_1();
	this.logobalk.name = "logobalk";
	this.logobalk.parent = this;
	this.logobalk.setTransform(2024.5,220,1,1,0,0,0,579.5,43);

	this.intro_img = new lib.intro_img_1();
	this.intro_img.name = "intro_img";
	this.intro_img.parent = this;
	this.intro_img.setTransform(1481.85,140.5,1,1,0,0,0,67.5,42.5);

	this.merkentext = new lib.merken_text();
	this.merkentext.name = "merkentext";
	this.merkentext.parent = this;
	this.merkentext.setTransform(1458.4,96,1,1,0,0,0,116.9,55);

	this.intro_text = new lib.Symbol10();
	this.intro_text.name = "intro_text";
	this.intro_text.parent = this;
	this.intro_text.setTransform(-645.95,183,1,1,0,0,0,70.4,35);

	this.subsubtext = new lib.subsubtext();
	this.subsubtext.name = "subsubtext";
	this.subsubtext.parent = this;
	this.subsubtext.setTransform(1384.8,120);

	this.dollarcashback = new lib.dollar_cashback();
	this.dollarcashback.name = "dollarcashback";
	this.dollarcashback.parent = this;
	this.dollarcashback.setTransform(1590,125,1.7143,1.7143);

	this.dollProd1 = new lib.dollar_3_v2();
	this.dollProd1.name = "dollProd1";
	this.dollProd1.parent = this;
	this.dollProd1.setTransform(100,-38.05);

	this.prijs = new lib.dollar_prijs();
	this.prijs.name = "prijs";
	this.prijs.parent = this;
	this.prijs.setTransform(1000,142);
	this.prijs.alpha = 0;

	this.text3 = new lib.text3();
	this.text3.name = "text3";
	this.text3.parent = this;
	this.text3.setTransform(685,372);
	this.text3.alpha = 0;

	this.label = new lib.emptyMovieClip();
	this.label.name = "label";
	this.label.parent = this;
	this.label.setTransform(580,180);

	this.servButton = new lib.services_button();
	this.servButton.name = "servButton";
	this.servButton.parent = this;
	this.servButton.setTransform(1280,175,1,1,-89.9948);

	this.g2 = new lib.label_green();
	this.g2.name = "g2";
	this.g2.parent = this;
	this.g2.setTransform(1280,170,0.6875,0.6875);

	this.g1 = new lib.label_green();
	this.g1.name = "g1";
	this.g1.parent = this;
	this.g1.setTransform(1280,146,0.6875,0.6875);

	this.g0 = new lib.label_green();
	this.g0.name = "g0";
	this.g0.parent = this;
	this.g0.setTransform(1460,122.05,0.6875,0.6875,0,0,0,0,0.1);

	this.endframe_video = new lib.endframe_video();
	this.endframe_video.name = "endframe_video";
	this.endframe_video.parent = this;
	this.endframe_video.setTransform(-750,84);

	this.vanaf = new lib.vanaf();
	this.vanaf.name = "vanaf";
	this.vanaf.parent = this;
	this.vanaf.setTransform(-624,120);
	this.vanaf.alpha = 0;

	this.lineContainer = new lib.price_old_line_container();
	this.lineContainer.name = "lineContainer";
	this.lineContainer.parent = this;
	this.lineContainer.setTransform(-200,102);

	this.endframe = new lib.endframe_static();
	this.endframe.name = "endframe";
	this.endframe.parent = this;
	this.endframe.setTransform(-527,78);

	this.price = new lib.emptyMovieClip();
	this.price.name = "price";
	this.price.parent = this;
	this.price.setTransform(685,140);

	this.cover2 = new lib.cover();
	this.cover2.name = "cover2";
	this.cover2.parent = this;
	this.cover2.setTransform(1113,-250.05,1,1.3353);

	this.white = new lib.shape2();
	this.white.name = "white";
	this.white.parent = this;
	this.white.setTransform(-51.9,126.05,1,1,0,0,0,20,125);

	this.logo = new lib.emptyMovieClip();
	this.logo.name = "logo";
	this.logo.parent = this;
	this.logo.setTransform(20,0);
	this.logo.alpha = 0;

	this.product = new lib.emptyMovieClip();
	this.product.name = "product";
	this.product.parent = this;
	this.product.setTransform(500,125);

	this.grey = new lib.shape_grey();
	this.grey.name = "grey";
	this.grey.parent = this;
	this.grey.setTransform(1310,40);

	this.cover = new lib.cover1();
	this.cover.name = "cover";
	this.cover.parent = this;
	this.cover.setTransform(325,338);

	this.mediamarkt = new lib.mediamarkt_logo();
	this.mediamarkt.name = "mediamarkt";
	this.mediamarkt.parent = this;
	this.mediamarkt.setTransform(485.15,358.15,0.3698,0.3698,0,0,0,0.4,0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mediamarkt},{t:this.cover},{t:this.grey},{t:this.product},{t:this.logo},{t:this.white},{t:this.cover2},{t:this.price},{t:this.endframe},{t:this.lineContainer},{t:this.vanaf},{t:this.endframe_video},{t:this.g0},{t:this.g1},{t:this.g2},{t:this.servButton},{t:this.label},{t:this.text3},{t:this.prijs},{t:this.dollProd1},{t:this.dollarcashback},{t:this.subsubtext},{t:this.intro_text},{t:this.merkentext},{t:this.intro_img},{t:this.logobalk}]}).wait(1));

	// Layer_2 (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	mask_5.graphics.p("EiBOAWqMAAAgtTMECdAAAMAAAAtTg");
	mask_5.setTransform(827.064,144.9963);

	// Layer_1
	this.instance = new lib.greys();
	this.instance.parent = this;
	this.instance.setTransform(812.4,124,1,1,0,0,0,812.4,164);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_5;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// hitAreaMC
	this.hitAreaMC = new lib.shape1();
	this.hitAreaMC.name = "hitAreaMC";
	this.hitAreaMC.parent = this;
	this.hitAreaMC.setTransform(0,0,1.3113,1.312);

	this.timeline.addTween(cjs.Tween.get(this.hitAreaMC).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-114,-232.9,2831,746.9);
// library properties:
lib.properties = {
	id: 'CB71CC1E45F7401086B269D4402BC0DC',
	width: 1272,
	height: 328,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/grey_field.png", id:"grey_field"},
		{src:"images/intro_img.png", id:"intro_img"},
		{src:"images/logobalk.png", id:"logobalk"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['CB71CC1E45F7401086B269D4402BC0DC'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;