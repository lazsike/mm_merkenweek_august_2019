(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.grey_field = function() {
	this.initialize(img.grey_field);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,728,76);


(lib.intro_img = function() {
	this.initialize(img.intro_img);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,562,228);


(lib.logobalk = function() {
	this.initialize(img.logobalk);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,341,96);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.vanaf = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "13px 'Geomanist'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 3;
	this.dynText.lineWidth = 86;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.vanaf, new cjs.Rectangle(0,0,90,23.9), null);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.grey_field();
	this.instance.parent = this;
	this.instance.setTransform(-728,-38);

	this.instance_1 = new lib.grey_field();
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,-38);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-728,-38,1456,76);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.grey_field();
	this.instance.parent = this;
	this.instance.setTransform(0,-38);

	this.instance_1 = new lib.grey_field();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-728,-38);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-728,-38,1456,76);


(lib.text3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "13px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 12;
	this.dynText.lineWidth = 141;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text3, new cjs.Rectangle(0,0,145,25.6), null);


(lib.text2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "28px 'Geomanist'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 30;
	this.dynText.lineWidth = 796;
	this.dynText.parent = this;
	this.dynText.setTransform(2,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text2, new cjs.Rectangle(0,0,800,32), null);


(lib.text1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "28px 'Geomanist'", "#E31E26");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 30;
	this.dynText.lineWidth = 796;
	this.dynText.parent = this;
	this.dynText.setTransform(2,-30);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.text1, new cjs.Rectangle(0,-32,800,32), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgPQAGALAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQAAgEgGgDIgPgIQgWgKAAgTQAAgRALgJQANgKARAAQASAAAMAKQAKAHADAKIgdAPQgFgJgGgBQgBAAgBABQgBAAAAAAQgBABAAAAQAAABAAABQAAACAGAEIAQAHQAWAMAAASQAAAQgMALQgMAJgUAAQgRAAgNgKg");
	this.shape.setTransform(183.6,37.95);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AglAsQgOgMAAgVIAAhDIApAAIAABEQAAAMAKAAQALAAAAgMIAAhEIApAAIAABDQgBAVgNAMQgOANgYAAQgXAAgOgNg");
	this.shape_1.setTransform(173.4,38.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_2.setTransform(163.325,37.95);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgPQAHALAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAAAQAAgEgGgDIgPgIQgWgKAAgTQAAgRALgJQANgKARAAQARAAANAKQAKAHADAKIgdAPQgFgJgGgBQgBAAgBABQgBAAAAAAQgBABAAAAQAAABAAABQAAACAGAEIAQAHQAWAMAAASQAAAQgMALQgMAJgUAAQgRAAgNgKg");
	this.shape_3.setTransform(154.25,37.95);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AglAsQgOgMAAgVIAAhDIApAAIAABEQAAAMAKAAQALAAAAgMIAAhEIApAAIAABDQgBAVgNAMQgOANgYAAQgXAAgOgNg");
	this.shape_4.setTransform(144.05,38.05);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgpAqQgPgRAAgZQAAgYAQgQQARgRAZAAQAfAAASASIgYAYQgJgHgLAAQgIgBgHAHQgHAGAAAKQAAALAHAGQAGAIAKAAQAIAAAFgDIAAgFIgNAAIAAgaIAxAAIAAAxQgVASghAAQgaAAgSgQg");
	this.shape_5.setTransform(132.35,37.95);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AglAsQgNgMAAgVIAAhDIAoAAIAABEQAAAMAKAAQALAAAAgMIAAhEIApAAIAABDQAAAVgNAMQgOANgZAAQgYAAgNgNg");
	this.shape_6.setTransform(120.85,38.05);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAnBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_7.setTransform(109.25,37.95);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgJA4IAAhMIgLACIgHgdIAZgIIAfAAIAABvg");
	this.shape_8.setTransform(96.95,37.95);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgJA4IAAhMIgKACIgIgdIAagIIAdAAIAABvg");
	this.shape_9.setTransform(90.5,37.95);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAdA4IAAg2IgLAtIgjAAIgLgtIAAA2IgoAAIAAhvIAzAAIARA/IASg/IAzAAIAABvg");
	this.shape_10.setTransform(77,37.95);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgnBFIAviJIAgAAIgvCJg");
	this.shape_11.setTransform(65.8,38.425);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_12.setTransform(57.475,37.95);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgfAuQgKgJgCgMIAjgIQAAAIAHAAQAFAAAAgIQAAgDgCgCQgCgDgCAAQgEAAgCAFIgggFIADhBIBKAAIAAAeIgpAAIgBAJQAFgBAFgBQAQAAAMAKQALAKAAAQQAAARgNALQgNAMgSAAQgSAAgNgLg");
	this.shape_13.setTransform(45.4,38.05);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAMA4IgYg0IAAA0IgoAAIAAhvIApAAIAXA1IAAg1IApAAIAABvg");
	this.shape_14.setTransform(32.075,37.95);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFAKIgFgVIgGAVIALAAg");
	this.shape_15.setTransform(20.35,37.95);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgTA4IgphvIAsAAIARBAIARhAIArAAIgpBvg");
	this.shape_16.setTransform(8.225,37.95);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AhGBwIAAjfIBQAAIAACgIA+AAIAAA/g");
	this.shape_17.setTransform(480.7,14.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA0AAIAAA6Ig0AAIAAAUIA7AAIAAA/g");
	this.shape_18.setTransform(464.65,14.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA1AAIAAA6Ig1AAIAAAUIA7AAIAAA/g");
	this.shape_19.setTransform(448.75,14.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgoBwIhRjfIBYAAIAjCCIAjiCIBVAAIhRDfg");
	this.shape_20.setTransform(428.375,14.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AhGBwIAAjfIBQAAIAACgIA9AAIAAA/g");
	this.shape_21.setTransform(402.3,14.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA0AAIAAA6Ig0AAIAAAUIA7AAIAAA/g");
	this.shape_22.setTransform(386.25,14.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA1AAIAAA6Ig1AAIAAAUIA7AAIAAA/g");
	this.shape_23.setTransform(370.35,14.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAYBwIAAhRIgvAAIAABRIhRAAIAAjfIBRAAIAABQIAvAAIAAhQIBRAAIAADfg");
	this.shape_24.setTransform(350.95,14.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AhXBwIAAjfIBdAAQAmAAAXAWQAVAUAAAiQABAigXAVQgXAVglABIgQAAIAABGgAgKgQIAIAAQAGAAAFgEQAFgGAAgHQAAgIgFgEQgEgFgHAAIgIAAg");
	this.shape_25.setTransform(324.35,14.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AhaBSQghggAAgyQAAgxAhggQAjgiA3AAQA4AAAjAiQAhAgAAAxQAAAyghAgQgjAig4AAQg3AAgjgigAgcgiQgKANAAAVQAAAWAKAOQALANARAAQASAAALgNQAKgOAAgWQAAgVgKgNQgLgOgSAAQgRAAgLAOg");
	this.shape_26.setTransform(301.95,14.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E90000").s().p("AhSBTQggghAAgyQAAgxAiggQAhgiA0AAQA9AAAjAkIgtAxQgSgQgYAAQgRAAgNANQgNANAAAUQAAAWAMANQAMANAUAAQASAAAJgFIAAgJIgZAAIAAg1IBiAAIAABjQgqAkhDAAQg2AAgighg");
	this.shape_27.setTransform(271,14.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E90000").s().p("AAXBwIgvhpIAABpIhRAAIAAjfIBTAAIAvBpIAAhpIBRAAIAADfg");
	this.shape_28.setTransform(247.775,14.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E90000").s().p("AgoBwIAAjfIBRAAIAADfg");
	this.shape_29.setTransform(231.275,14.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E90000").s().p("AgoBwIAAigIgsAAIAAg/ICpAAIAAA/IgtAAIAACgg");
	this.shape_30.setTransform(217.35,14.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E90000").s().p("AALBwIgehJIAABJIhPAAIAAjfIBiAAQAnAAAXAVQAWAUAAAgQAAASgLAQQgKANgPAJIAzBegAgTgTIAJAAQAHAAAEgFQAEgEAAgHQAAgFgEgFQgEgFgHAAIgJAAg");
	this.shape_31.setTransform(199.075,14.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E90000").s().p("AhZBSQgiggAAgyQAAgxAiggQAigiA3AAQA4AAAjAiQAhAgAAAxQAAAyghAgQgjAig4AAQg4AAghgigAgcgiQgKANAAAVQAAAWAKAOQALANARAAQASAAALgNQAKgOAAgWQAAgVgKgNQgLgOgSAAQgRAAgLAOg");
	this.shape_32.setTransform(175.6,14.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E90000").s().p("AAPBwIgmhcIAABcIhRAAIAAjfIBRAAIAABSIAlhSIBZAAIg3BrIA5B0g");
	this.shape_33.setTransform(152.075,14.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E90000").s().p("AAeBkQgSgRAAgZQAAgYASgQQASgRAbAAQAbAAASARQASAQAAAYQAAAZgSARQgSAQgbAAQgbAAgSgQgABEA6QAAAMAHAAQAIAAAAgMQAAgKgIgBQgHABAAAKgAhwBwICkjfIA9AAIijDfgAh3gQQgSgRAAgZQAAgYASgQQASgRAbAAQAbAAASARQASAQAAAYQAAAZgSARQgSAQgbAAQgbAAgSgQgAhSg6QAAAMAIAAQAHAAAAgMQAAgKgHgBQgIABAAAKg");
	this.shape_34.setTransform(120.125,14.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E90000").s().p("Ag+BcQgTgRgGgYIBFgQQACAPANAAQANAAAAgQQAAgGgEgFQgFgEgFgBQgJAAgEAJIhAgJIAFiDICWAAIAAA8IhTAAIgBASQAJgDALAAQAhAAAWAUQAXAUAAAgQAAAjgZAWQgZAXgmAAQgmAAgYgWg");
	this.shape_35.setTransform(96.725,15);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E90000").s().p("AhTByIAAgcQAAgWAOgUQAKgPAVgRIAegaQAOgPAAgLQAAgKgJAAQgKAAgFATIhEgNQAGgdASgTQAXgVAnAAQAlAAAYASQAYAUAAAhQAAAiglAdIgZASQgLAJAAAGIBLAAIAAA8g");
	this.shape_36.setTransform(78.375,14.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#E90000").s().p("AgoBwIAAigIgrAAIAAg/ICnAAIAAA/IgrAAIAACgg");
	this.shape_37.setTransform(54.05,14.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#E90000").s().p("AhZBSQgiggAAgyQAAgxAiggQAigiA3AAQA4AAAjAiQAhAgAAAxQAAAyghAgQgjAig4AAQg4AAghgigAgcgiQgKANAAAVQAAAWAKAOQALANARAAQASAAALgNQAKgOAAgWQAAgVgKgNQgLgOgSAAQgRAAgLAOg");
	this.shape_38.setTransform(32.5,14.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#E90000").s().p("AgoBwIAAigIgrAAIAAg/ICoAAIAAA/IgsAAIAACgg");
	this.shape_39.setTransform(10.95,14.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,490.4,49.6), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AhSBTQggghAAgyQAAgxAhggQAjgiA0AAQA8AAAjAkIgtAxQgSgQgXAAQgTAAgNANQgMANAAAUQAAAWAMANQANANATAAQASAAAJgFIAAgJIgZAAIAAg1IBiAAIAABjQgqAkhDAAQg2AAgighg");
	this.shape.setTransform(272.05,14.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("AAXBwIgvhpIAABpIhRAAIAAjfIBTAAIAvBpIAAhpIBRAAIAADfg");
	this.shape_1.setTransform(248.825,14.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E90000").s().p("AgoBwIAAjfIBRAAIAADfg");
	this.shape_2.setTransform(232.325,14.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E90000").s().p("AgoBwIAAigIgrAAIAAg/ICoAAIAAA/IgsAAIAACgg");
	this.shape_3.setTransform(218.4,14.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("AALBwIgehJIAABJIhPAAIAAjfIBiAAQAnAAAXAVQAWAUAAAgQAAASgLAQQgKANgPAJIAzBegAgTgTIAJAAQAHAAAEgFQAEgEAAgHQAAgFgEgFQgEgFgHAAIgJAAg");
	this.shape_4.setTransform(200.125,14.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AhaBSQghggAAgyQAAgxAhggQAjgiA3AAQA4AAAiAiQAiAgAAAxQAAAygiAgQgiAig4AAQg3AAgjgigAgcgiQgKANAAAVQAAAWAKAOQAKANASAAQASAAALgNQAKgOAAgWQAAgVgKgNQgLgOgSAAQgSAAgKAOg");
	this.shape_5.setTransform(176.65,14.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AAPBwIgmhcIAABcIhRAAIAAjfIBRAAIAABSIAlhSIBZAAIg3BrIA5B0g");
	this.shape_6.setTransform(153.125,14.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AAeBkQgSgRAAgZQAAgYASgQQASgRAbAAQAbAAASARQASAQAAAYQAAAZgSARQgSAQgbAAQgbAAgSgQgABEA6QAAAMAHAAQAIAAAAgMQAAgKgIgBQgHABAAAKgAhwBwICkjfIA9AAIijDfgAh3gQQgSgRAAgZQAAgYASgQQASgRAbAAQAbAAASARQASAQAAAYQAAAZgSARQgSAQgbAAQgbAAgSgQgAhSg6QAAAMAIAAQAHAAAAgMQAAgKgHgBQgIABAAAKg");
	this.shape_7.setTransform(121.175,14.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AhYAAQAAhzBYAAQBZAAAABzQAAB0hZAAQhYAAAAh0gAgKAAQAAA0AKAAQALAAAAg0QAAg0gLAAQgKAAAAA0g");
	this.shape_8.setTransform(97.275,14.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AhTByIAAgcQAAgWAOgUQAKgPAVgRIAegaQAOgPAAgLQAAgKgJAAQgKAAgFATIhEgNQAGgdASgTQAXgVAnAAQAlAAAYASQAYAUAAAhQAAAiglAdIgZASQgLAJAAAGIBLAAIAAA8g");
	this.shape_9.setTransform(78.375,14.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AgoBwIAAigIgrAAIAAg/ICnAAIAAA/IgrAAIAACgg");
	this.shape_10.setTransform(54.05,14.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E90000").s().p("AhZBSQgiggAAgyQAAgxAiggQAigiA3AAQA4AAAjAiQAhAgAAAxQAAAyghAgQgjAig4AAQg4AAghgigAgcgiQgKANAAAVQAAAWAKAOQALANARAAQASAAALgNQAKgOAAgWQAAgVgKgNQgLgOgSAAQgRAAgLAOg");
	this.shape_11.setTransform(32.5,14.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("AgoBwIAAigIgrAAIAAg/ICoAAIAAA/IgsAAIAACgg");
	this.shape_12.setTransform(10.95,14.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgSARQAJgGADgIQgNgDAAgNQAAgIAFgFQAGgGAIAAQAJABAFAEQAGAGAAAKQAAAMgHALQgGAMgLAJg");
	this.shape_13.setTransform(328.7,42.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgeAwQgJgIgEgLIAcgQQAIAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQAMgJASAAQASAAAMAJQAKAIADAJIgdARQgEgLgIABQAAAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape_14.setTransform(321.95,37.45);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgSAAIgHAAIAAAjgAgEgHIADAAQADAAACgDQADgCAAgEQAAgEgCgCQgDgDgCABIgEAAg");
	this.shape_15.setTransform(312.85,37.45);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_16.setTransform(301.675,37.45);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_17.setTransform(290.925,37.45);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAHA4IgSguIAAAuIgpAAIAAhvIApAAIAAApIASgpIAsAAIgbA1IAcA6g");
	this.shape_18.setTransform(281.05,37.45);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQAMgJASAAQARAAANAJQAKAIADAJIgdARQgEgLgHABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHAEIAPAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgMgKg");
	this.shape_19.setTransform(270.7,37.45);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_20.setTransform(262.125,37.45);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_21.setTransform(252.925,37.45);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgSARQAJgGADgIQgNgDAAgNQAAgIAGgFQAFgGAIAAQAJABAFAEQAGAGAAAKQAAAMgHALQgGAMgLAJg");
	this.shape_22.setTransform(242.1,42.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgQQAGAMAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQASAAAMAJQAKAIADAJIgdARQgFgLgGABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape_23.setTransform(235.35,37.45);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgHIADAAQADAAADgDQACgCAAgEQAAgEgCgCQgDgDgCABIgEAAg");
	this.shape_24.setTransform(226.25,37.45);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_25.setTransform(215.075,37.45);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_26.setTransform(204.325,37.45);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgLALgTAAIgHAAIAAAjgAgEgHIAEAAQACAAADgDQACgCAAgEQAAgEgCgCQgCgDgDABIgEAAg");
	this.shape_27.setTransform(195.35,37.45);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_28.setTransform(184.5,37.45);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgiA4IAAhvIAnAAIAABPIAfAAIAAAgg");
	this.shape_29.setTransform(174.8,37.45);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgLALgTAAIgHAAIAAAjgAgEgHIAEAAQACAAACgDQADgCAAgEQAAgEgCgCQgDgDgCABIgEAAg");
	this.shape_30.setTransform(163.2,37.45);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AALA4IAAgoIgWAAIAAAoIgpAAIAAhvIApAAIAAApIAWAAIAAgpIAqAAIAABvg");
	this.shape_31.setTransform(152.6,37.45);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_32.setTransform(139.825,37.45);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_33.setTransform(130.625,37.45);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQALAJAAAQQAAAKgEAHQgGAHgHAEIAZAvgAgJgJIAEAAQAEAAACgDQACgBgBgDQABgEgCgCQgCgDgEABIgEAAg");
	this.shape_34.setTransform(120.35,37.45);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_35.setTransform(110.925,37.45);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_36.setTransform(102.975,37.45);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_37.setTransform(94.575,37.45);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgIgMgBQgJABgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMAAAKgIIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_38.setTransform(84.925,37.45);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_39.setTransform(75.875,37.45);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgiA4IAAhvIAnAAIAABPIAfAAIAAAgg");
	this.shape_40.setTransform(68.15,37.45);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_41.setTransform(60.125,37.45);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQARAAANAJQAKAIADAJIgdARQgEgLgHABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHAEIAPAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgMgKg");
	this.shape_42.setTransform(51.55,37.45);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_43.setTransform(42.975,37.45);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgpApQgPgQgBgZQAAgYARgQQARgRAaAAQAeAAARASIgWAZQgJgJgMAAQgIABgHAGQgGAHAAAJQAAALAGAHQAGAGAJAAQAJAAAFgDIAAgEIgMAAIAAgaIAxAAIAAAwQgWATghAAQgbAAgRgRg");
	this.shape_44.setTransform(32.95,37.45);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgHIAEAAQACAAADgDQACgCAAgEQAAgEgCgCQgCgDgDABIgEAAg");
	this.shape_45.setTransform(19.5,37.45);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_46.setTransform(8.325,37.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,336.1,49.1), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AhRBTQghghAAgyQAAgxAiggQAhgiA0AAQA9AAAjAkIgtAxQgSgQgYAAQgRAAgNANQgNANAAAUQAAAWAMANQAMANAUAAQASAAAJgFIAAgJIgZAAIAAg1IBiAAIAABjQgqAkhDAAQg2AAghghg");
	this.shape.setTransform(204.95,14.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("AAXBwIgvhpIAABpIhRAAIAAjfIBTAAIAvBpIAAhpIBRAAIAADfg");
	this.shape_1.setTransform(181.725,14.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E90000").s().p("AgoBwIAAjfIBRAAIAADfg");
	this.shape_2.setTransform(165.225,14.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E90000").s().p("AgoBwIAAigIgsAAIAAg/ICoAAIAAA/IgsAAIAACgg");
	this.shape_3.setTransform(151.3,14.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("AALBwIgehJIAABJIhPAAIAAjfIBiAAQAnAAAXAVQAWAUAAAgQAAASgLAQQgKANgPAJIAzBegAgTgTIAJAAQAHAAAEgFQAEgEAAgHQAAgFgEgFQgEgFgHAAIgJAAg");
	this.shape_4.setTransform(133.025,14.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AhZBSQgiggAAgyQAAgxAiggQAigiA3AAQA4AAAjAiQAhAgAAAxQAAAyghAgQgjAig4AAQg4AAghgigAgcgiQgKANAAAVQAAAWAKAOQALANARAAQASAAALgNQAKgOAAgWQAAgVgKgNQgLgOgSAAQgRAAgLAOg");
	this.shape_5.setTransform(109.55,14.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AAPBwIgmhcIAABcIhRAAIAAjfIBRAAIAABSIAlhSIBZAAIg3BrIA5B0g");
	this.shape_6.setTransform(86.025,14.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AAeBkQgSgRAAgZQAAgYASgQQASgRAbAAQAbAAASARQASAQAAAYQAAAZgSARQgSAQgbAAQgbAAgSgQgABEA6QAAAMAHAAQAIAAAAgMQAAgKgIgBQgHABAAAKgAhwBwICkjfIA9AAIijDfgAh3gQQgSgRAAgZQAAgYASgQQASgRAbAAQAbAAASARQASAQAAAYQAAAZgSARQgSAQgbAAQgbAAgSgQgAhSg6QAAAMAIAAQAHAAAAgMQAAgKgHgBQgIABAAAKg");
	this.shape_7.setTransform(54.075,14.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AhYAAQAAhzBYAAQBZAAAABzQAAB0hZAAQhYAAAAh0gAgKAAQAAA0AKAAQALAAAAg0QAAg0gLAAQgKAAAAA0g");
	this.shape_8.setTransform(30.175,14.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AhTByIAAgcQAAgWAOgUQAKgPAVgRIAegaQAOgPAAgLQAAgKgJAAQgKAAgFATIhEgNQAGgdASgTQAXgVAnAAQAlAAAYASQAYAUAAAhQAAAiglAdIgZASQgLAJAAAGIBLAAIAAA8g");
	this.shape_9.setTransform(11.275,14.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgQQAGAMAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgQALgKQANgJARAAQARAAANAJQAKAIADAJIgdAQQgEgKgHAAQgBAAgBABQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGADIAQAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape_10.setTransform(280.9,35.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQATAAALALQALAJAAARQAAAIgEAJQgFAGgIAEIAZAvgAgJgJIAEAAQAEAAACgDQABgCAAgCQAAgEgBgCQgCgCgEgBIgEAAg");
	this.shape_11.setTransform(271.65,35.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_12.setTransform(262.225,35.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgpApQgQgQAAgZQAAgXARgRQARgRAZAAQAeAAASASIgWAYQgKgHgLgBQgIABgHAGQgGAHgBAJQABALAGAHQAGAGAKABQAIgBAFgDIAAgEIgMAAIAAgaIAwAAIAAAwQgVATghAAQgaAAgSgRg");
	this.shape_13.setTransform(252.2,35.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_14.setTransform(243.825,35.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgmAsQgMgMAAgVIAAhDIAoAAIAABEQAAAMAKAAQALAAAAgMIAAhEIAoAAIAABDQABAVgNAMQgOANgZAAQgYAAgOgNg");
	this.shape_15.setTransform(235.7,35.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgvA4IAAgVIAqg7IgpAAIAAgfIBeAAIAAAVIgrA7IArAAIAAAfg");
	this.shape_16.setTransform(225.075,35.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AghA4IAAhvIBDAAIAAAfIgcAAIAAAOIAaAAIAAAdIgaAAIAAAlg");
	this.shape_17.setTransform(216.35,35.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIABQAJgBAFgGQAFgHAAgLQAAgKgFgGQgFgIgJAAQgIAAgGAIg");
	this.shape_18.setTransform(206.125,35.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_19.setTransform(195.375,35.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgQQAGAMAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgQALgKQANgJARAAQASAAAMAJQAKAIADAJIgdAQQgFgKgGAAQgBAAgBABQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGADIAQAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape_20.setTransform(186.3,35.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgoApQgRgQABgZQAAgXAQgRQARgRAaAAQAeAAASASIgYAYQgJgHgLgBQgJABgGAGQgHAHABAJQgBALAHAHQAGAGAJABQAJgBAFgDIAAgEIgNAAIAAgaIAyAAIAAAwQgWATghAAQgbAAgQgRg");
	this.shape_21.setTransform(172.6,35.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_22.setTransform(162.725,35.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgnAAIAmhvIAnAAIAnBvgAAGALIgGgXIgHAXIANAAg");
	this.shape_23.setTransform(152.8,35.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_24.setTransform(139.825,35.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_25.setTransform(130.625,35.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQALAJAAARQAAAIgEAJQgGAGgHAEIAZAvgAgJgJIAEAAQAEAAACgDQACgCgBgCQABgEgCgCQgCgCgEgBIgEAAg");
	this.shape_26.setTransform(120.35,35.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_27.setTransform(110.925,35.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_28.setTransform(102.975,35.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_29.setTransform(94.575,35.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgJgMAAQgJABgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMABAKgJIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_30.setTransform(84.925,35.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_31.setTransform(75.875,35.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgiA4IAAhvIAnAAIAABQIAfAAIAAAfg");
	this.shape_32.setTransform(68.15,35.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_33.setTransform(60.125,35.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgQALgKQANgJARAAQARAAANAJQAKAIADAJIgdAQQgEgKgHAAQgBAAgBABQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHADIAPAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgMgKg");
	this.shape_34.setTransform(51.55,35.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_35.setTransform(42.975,35.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgpApQgPgQgBgZQAAgXARgRQARgRAaAAQAeAAARASIgWAYQgJgHgMgBQgIABgHAGQgGAHAAAJQAAALAGAHQAGAGAJABQAJgBAFgDIAAgEIgMAAIAAgaIAxAAIAAAwQgWATghAAQgbAAgRgRg");
	this.shape_36.setTransform(32.95,35.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgHIAEAAQACgBADgCQACgCAAgEQAAgEgCgCQgCgCgDgBIgEAAg");
	this.shape_37.setTransform(19.5,35.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIABQAJgBAFgGQAFgHAAgLQAAgKgFgGQgFgIgJAAQgIAAgGAIg");
	this.shape_38.setTransform(8.325,35.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,287.7,47), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeAwQgJgIgEgLIAcgQQAIAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPAMgLQALgJASAAQASAAAMAJQAKAIADAJIgcARQgGgLgHABQAAAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHAEIAPAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape.setTransform(308.55,37.45);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_1.setTransform(299.975,37.45);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_2.setTransform(290.275,37.45);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_3.setTransform(282.025,37.45);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AALA4IAAgoIgWAAIAAAoIgoAAIAAhvIAoAAIAAApIAWAAIAAgpIAqAAIAABvg");
	this.shape_4.setTransform(273.8,37.45);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgIgMgBQgJABgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMAAAKgIIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_5.setTransform(262.875,37.45);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_6.setTransform(251.85,37.45);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAdA4IAAg2IgLAuIgjAAIgLguIAAA2IgnAAIAAhvIAyAAIARA+IASg+IAyAAIAABvg");
	this.shape_7.setTransform(238.6,37.45);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgeAwQgJgIgEgLIAcgQQAIAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPAMgLQALgJASAAQASAAAMAJQAKAIADAJIgcARQgFgLgIABQAAAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHAEIAPAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape_8.setTransform(226.65,37.45);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_9.setTransform(216.1,37.45);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AANA4IgNg6IgNA6IgoAAIgehvIAsAAIAKA8IAOg8IAfAAIAOA8IAKg8IAsAAIgdBvg");
	this.shape_10.setTransform(201.7,37.45);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_11.setTransform(186.225,37.45);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgiA4IAAhvIAnAAIAABPIAfAAIAAAgg");
	this.shape_12.setTransform(178.5,37.45);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_13.setTransform(170.475,37.45);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_14.setTransform(164.025,37.45);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAdA4IAAg2IgLAuIgjAAIgLguIAAA2IgoAAIAAhvIAzAAIARA+IASg+IAyAAIAABvg");
	this.shape_15.setTransform(154.2,37.45);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_16.setTransform(139.825,37.45);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_17.setTransform(130.625,37.45);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQALAJAAAQQAAAKgEAHQgGAHgHAEIAZAvgAgJgJIAEAAQAEAAACgDQACgBgBgDQABgEgCgCQgCgDgEABIgEAAg");
	this.shape_18.setTransform(120.35,37.45);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_19.setTransform(110.925,37.45);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_20.setTransform(102.975,37.45);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_21.setTransform(94.575,37.45);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgIgMgBQgJABgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMAAAKgIIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_22.setTransform(84.925,37.45);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_23.setTransform(75.875,37.45);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgiA4IAAhvIAnAAIAABPIAfAAIAAAgg");
	this.shape_24.setTransform(68.15,37.45);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_25.setTransform(60.125,37.45);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQARAAANAJQAKAIADAJIgdARQgEgLgHABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHAEIAPAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgMgKg");
	this.shape_26.setTransform(51.55,37.45);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_27.setTransform(42.975,37.45);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgpApQgPgQgBgZQAAgYARgQQARgRAaAAQAeAAARASIgWAZQgJgJgMAAQgIABgHAGQgGAHAAAJQAAALAGAHQAGAGAJAAQAJAAAFgDIAAgEIgMAAIAAgaIAxAAIAAAwQgWATghAAQgbAAgRgRg");
	this.shape_28.setTransform(32.95,37.45);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgHIAEAAQACAAADgDQACgCAAgEQAAgEgCgCQgCgDgDABIgEAAg");
	this.shape_29.setTransform(19.5,37.45);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_30.setTransform(8.325,37.45);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E90000").s().p("AhRBTQghghAAgyQAAgxAiggQAhgiA1AAQA8AAAjAkIgtAxQgTgQgWAAQgTAAgMANQgNANAAAUQAAAWAMANQANANATAAQARAAALgFIAAgJIgaAAIAAg1IBiAAIAABjQgqAkhCAAQg3AAghghg");
	this.shape_31.setTransform(199.3,14.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E90000").s().p("AAXBwIgvhpIAABpIhRAAIAAjfIBTAAIAvBpIAAhpIBRAAIAADfg");
	this.shape_32.setTransform(176.075,14.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E90000").s().p("AgoBwIAAjfIBRAAIAADfg");
	this.shape_33.setTransform(159.575,14.8);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E90000").s().p("AgoBwIAAigIgsAAIAAg/ICoAAIAAA/IgsAAIAACgg");
	this.shape_34.setTransform(145.65,14.8);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E90000").s().p("AALBwIgehJIAABJIhPAAIAAjfIBiAAQAnAAAXAVQAWAUAAAgQAAASgLAQQgKANgPAJIAzBegAgTgTIAJAAQAHAAAEgFQAEgEAAgHQAAgFgEgFQgEgFgHAAIgJAAg");
	this.shape_35.setTransform(127.375,14.8);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#E90000").s().p("AhZBSQgiggAAgyQAAgxAiggQAigiA3AAQA4AAAiAiQAiAgAAAxQAAAygiAgQgiAig4AAQg4AAghgigAgcgiQgKANAAAVQAAAWAKAOQALANARAAQASAAALgNQAKgOAAgWQAAgVgKgNQgLgOgSAAQgRAAgLAOg");
	this.shape_36.setTransform(103.9,14.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#E90000").s().p("AAPBwIgmhcIAABcIhRAAIAAjfIBRAAIAABSIAlhSIBZAAIg3BrIA5B0g");
	this.shape_37.setTransform(80.375,14.8);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#E90000").s().p("AAeBkQgSgRAAgZQAAgYASgQQASgRAbAAQAbAAASARQASAQAAAYQAAAZgSARQgSAQgbAAQgbAAgSgQgABEA6QAAAMAHAAQAIAAAAgMQAAgKgIgBQgHABAAAKgAhwBwICkjfIA9AAIijDfgAh3gQQgSgRAAgZQAAgYASgQQASgRAbAAQAbAAASARQASAQAAAYQAAAZgSARQgSAQgbAAQgbAAgSgQgAhSg6QAAAMAIAAQAHAAAAgMQAAgKgHgBQgIABAAAKg");
	this.shape_38.setTransform(48.425,14.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#E90000").s().p("AhYAAQAAhzBYAAQBZAAAABzQAAB0hZAAQhYAAAAh0gAgKAAQAAA0AKAAQALAAAAg0QAAg0gLAAQgKAAAAA0g");
	this.shape_39.setTransform(24.525,14.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#E90000").s().p("AgVBwIAAiaIgTAGIgQg7IA0gQIA8AAIAADfg");
	this.shape_40.setTransform(8.3,14.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,318.4,49.1), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeAwQgJgIgEgLIAcgQQAIAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPAMgLQALgJASAAQASAAAMAJQAKAIADAJIgcARQgFgLgIABQAAAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHAEIAPAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape.setTransform(260.85,35.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgSAQQAJgFADgHQgNgEAAgNQAAgIAFgFQAHgFAHAAQAJAAAFAFQAGAFAAAKQAAAMgHALQgGANgLAHg");
	this.shape_1.setTransform(253.55,32.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgTA4IgphvIAsAAIARBBIARhBIArAAIgpBvg");
	this.shape_2.setTransform(244.675,35.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_3.setTransform(234.025,35.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgpApQgPgQAAgZQAAgYAQgQQARgRAZAAQAeAAATASIgYAZQgJgJgLAAQgIABgHAGQgGAHgBAJQABALAGAHQAGAGAKAAQAIAAAFgDIAAgEIgMAAIAAgaIAwAAIAAAwQgVATghAAQgaAAgSgRg");
	this.shape_4.setTransform(220.45,35.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_5.setTransform(208.825,35.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgmAsQgMgMAAgVIAAhDIAoAAIAABEQAAALAKAAQALAAAAgLIAAhEIAoAAIAABDQABAVgNAMQgOANgZAAQgYAAgOgNg");
	this.shape_6.setTransform(197.45,35.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgQQAGAMAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQASAAAMAJQAKAIADAJIgdARQgFgLgGABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape_7.setTransform(187.2,35.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAdA4IAAg2IgMAuIgiAAIgLguIAAA2IgnAAIAAhvIAyAAIARA+IARg+IA0AAIAABvg");
	this.shape_8.setTransform(175.3,35.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAFALIgFgXIgGAXIALAAg");
	this.shape_9.setTransform(162,35.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPAMgLQAMgJARAAQARAAANAJQAKAIADAJIgcARQgFgLgHABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGAEIAQAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgMgKg");
	this.shape_10.setTransform(151.45,35.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_11.setTransform(139.825,35.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_12.setTransform(130.625,35.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQALAJAAAQQAAAKgEAHQgGAHgHAEIAZAvgAgJgJIAEAAQAEAAACgDQACgBgBgDQABgEgCgCQgCgDgEABIgEAAg");
	this.shape_13.setTransform(120.35,35.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_14.setTransform(110.925,35.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_15.setTransform(102.975,35.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgTA4IAAhPIgWAAIAAggIBTAAIAAAgIgWAAIAABPg");
	this.shape_16.setTransform(94.575,35.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgfAqQgRgQAAgaQAAgYAQgQQARgRAaAAQAWAAAPALIgMAgQgJgIgMgBQgJABgGAGQgGAHAAAJQAAAKAGAIQAGAFAJAAQAMAAAKgIIAMAgQgQALgWAAQgaAAgQgQg");
	this.shape_17.setTransform(84.925,35.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_18.setTransform(75.875,35.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgiA4IAAhvIAnAAIAABPIAfAAIAAAgg");
	this.shape_19.setTransform(68.15,35.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_20.setTransform(60.125,35.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgPALgLQANgJARAAQARAAANAJQAKAIADAJIgdARQgEgLgHABQgBAAgBAAQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHAEIAPAHQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgMgKg");
	this.shape_21.setTransform(51.55,35.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAgIgdAAIAAAJIAaAAIAAAcIgaAAIAAAKIAdAAIAAAgg");
	this.shape_22.setTransform(42.975,35.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgpApQgPgQgBgZQAAgYARgQQARgRAaAAQAeAAARASIgWAZQgJgJgMAAQgIABgHAGQgGAHAAAJQAAALAGAHQAGAGAJAAQAJAAAFgDIAAgEIgMAAIAAgaIAxAAIAAAwQgWATghAAQgbAAgRgRg");
	this.shape_23.setTransform(32.95,35.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgHIAEAAQACAAADgDQACgCAAgEQAAgEgCgCQgCgDgDABIgEAAg");
	this.shape_24.setTransform(19.5,35.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIAAQAJAAAFgGQAFgHAAgLQAAgKgFgGQgFgIgJABQgIgBgGAIg");
	this.shape_25.setTransform(8.325,35.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#E90000").s().p("AhPBQQgfgfAAgxQAAgvAfggQAiggAzgBQA5ABAjAjIgsAvQgSgQgWAAQgRAAgNANQgNANAAATQAAAVAMANQAMANAUAAQARAAAJgGIAAgIIgZAAIAAg0IBgAAIAABgQgpAjhBAAQg1AAggggg");
	this.shape_26.setTransform(186.1,14.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#E90000").s().p("AAXBtIgvhnIAABnIhOAAIAAjZIBQAAIAuBnIAAhnIBOAAIAADZg");
	this.shape_27.setTransform(163.55,14.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#E90000").s().p("AgmBtIAAjZIBNAAIAADZg");
	this.shape_28.setTransform(147.55,14.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#E90000").s().p("AgnBtIAAicIgqAAIAAg9ICjAAIAAA9IgqAAIAACcg");
	this.shape_29.setTransform(134.1,14.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E90000").s().p("AALBtIgehGIAABGIhMAAIAAjZIBfAAQAmABAWAUQAVAUAAAeQAAASgKAPQgKAOgOAHIAxBcgAgTgTIAKAAQAHABADgFQAEgFAAgFQAAgHgEgEQgDgEgHgBIgKAAg");
	this.shape_30.setTransform(116.425,14.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#E90000").s().p("AhXBQQgggfAAgxQAAgwAggfQAiggA1gBQA3ABAgAgQAhAfAAAwQAAAxghAfQggAgg3AAQg1AAgigggAgbghQgKANAAAUQAAAVAKANQAKAOARAAQARAAALgOQAKgNAAgVQAAgUgKgNQgLgOgRAAQgRAAgKAOg");
	this.shape_31.setTransform(93.7,14.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#E90000").s().p("AAPBtIglhaIAABaIhPAAIAAjZIBPAAIAABPIAkhPIBWAAIg2BoIA4Bxg");
	this.shape_32.setTransform(70.925,14.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E90000").s().p("AAdBgQgRgPAAgYQAAgYARgQQARgQAbAAQAaAAASAQQARAQAAAYQAAAYgRAPQgSAQgaAAQgbAAgRgQgABBA5QAAALAIgBQAHABAAgLQAAgLgHAAQgIAAAAALgAhsBtICejZIA8AAIifDZgAh0gQQgRgQAAgYQAAgYARgPQASgQAagBQAbABARAQQARAPAAAYQAAAYgRAQQgRAQgbAAQgaAAgSgQgAhPg4QAAALAHAAQAIAAAAgLQAAgKgIAAQgHAAAAAKg");
	this.shape_33.setTransform(45.925,14.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E90000").s().p("Ag8BaQgTgRgFgYIBCgOQACANANAAQAMAAAAgPQAAgGgDgFQgEgEgGAAQgJAAgEAJIg9gJIAEiAICRAAIAAA7IhQAAIgBARQAJgDAKAAQAgAAAWAUQAXATAAAfQgBAhgYAXQgYAVglAAQglAAgXgUg");
	this.shape_34.setTransform(23.3,14.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E90000").s().p("AgUBtIAAiVIgTAFIgPg5IAzgQIA6AAIAADZg");
	this.shape_35.setTransform(8.1,14.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,270.7,46.8), null);


(lib.shape3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("At+GBIAAsBIb9AAIAAMBg");
	this.shape.setTransform(63.9947,45.0164,0.7151,1.1688);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape3, new cjs.Rectangle(0,0,128,90), null);


(lib.shape2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjHLuIAA3bIGPAAIAAXbg");
	this.shape.setTransform(7.9999,45.0194,0.4,0.6001);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape2, new cjs.Rectangle(0,0,16,90.1), null);


(lib.shape1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");
	this.shape.setTransform(363.9931,44.9982,2.4266,0.36);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.shape1, new cjs.Rectangle(0,0,728,90), null);


(lib.shape_grey = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E6E7E8").s().p("A0TLuIAA3bMAonAAAIAAXbg");
	this.shape.setTransform(356.5153,34.9929,2.7424,0.4666);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("A0TLuIAA3bMAonAAAIAAXbg");
	this.shape_1.setTransform(356.5087,34.9905,2.7424,0.5999);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-10,713.1,90);


(lib.services_button = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("", "11px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.textAlign = "center";
	this.dynText.lineHeight = 10;
	this.dynText.lineWidth = 136;
	this.dynText.parent = this;
	this.dynText.setTransform(70,7);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

}).prototype = getMCSymbolPrototype(lib.services_button, new cjs.Rectangle(0,5,140.1,22.3), null);


(lib.price_old_line = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E31E26").s().p("AjxAyIAAhjIHjAAIAABjg");
	this.shape.setTransform(46.0198,1,1.9008,0.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AjxAyIAAhjIHjAAIAABjg");
	this.shape_1.setTransform(46.0198,1,1.9008,0.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,92,2);


(lib.merken_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAbDLIhFinIAACnIiUAAIAAmVICUAAIAACTIBEiTIChAAIhlDBIBpDUg");
	this.shape.setTransform(371.375,25.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ah+DLIAAmVID9AAIAABzIhqAAIAAAhIBeAAIAABqIheAAIAAAlIBqAAIAAByg");
	this.shape_1.setTransform(336.15,25.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ah+DLIAAmVID9AAIAABzIhqAAIAAAhIBeAAIAABqIheAAIAAAlIBqAAIAAByg");
	this.shape_2.setTransform(307.35,25.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAxDLIgxjUIgwDUIiUAAIhqmVICdAAIAmDfIAyjfIBzAAIAyDfIAmjfICdAAIhqGVg");
	this.shape_3.setTransform(262.275,25.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("AAqDLIhXi/IAAC/IiRAAIAAmVICVAAIBXC/IAAi/ICRAAIAAGVg");
	this.shape_4.setTransform(210.725,25.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("Ah9DLIAAmVID7AAIAABzIhqAAIAAAhIBfAAIAABqIhfAAIAAAlIBqAAIAAByg");
	this.shape_5.setTransform(175.45,25.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AAbDLIhFinIAACnIiUAAIAAmVICUAAIAACTIBEiTIChAAIhlDBIBpDUg");
	this.shape_6.setTransform(141.375,25.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AAUDLIg4iDIAACDIiPAAIAAmVICzAAQBGAAAqAmQAnAkAAA6QAAAhgTAdQgRAagcAPIBdCqgAgkgjIARAAQANAAAIgJQAHgIAAgMQAAgKgHgJQgIgJgNAAIgRAAg");
	this.shape_7.setTransform(103.625,25.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("Ah9DLIAAmVID8AAIAABzIhrAAIAAAhIBfAAIAABqIhfAAIAAAlIBrAAIAAByg");
	this.shape_8.setTransform(69.5,25.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("ABpDLIAAjEIgoClIiBAAIgoilIAADEIiPAAIAAmVIC4AAIA/DmIBAjmIC4AAIAAGVg");
	this.shape_9.setTransform(28.575,25.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.merken_text, new cjs.Rectangle(0,0,393.3,62), null);


(lib.mediamarkt_logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EB1D24").s().p("AgzA2QgWgWAAggQAAgeAWgWQAVgXAeABQAfgBAVAXQAWAWAAAeQAAAfgWAXQgVAWgfgBQgeABgVgWgAgqgrQgSATAAAYQAAAaASASQASASAYAAQAZAAASgSQASgSAAgaQAAgZgSgSQgSgRgZgBQgYABgSARgAASAoIgNggIgSAAIAAAgIgRAAIAAhQIAkAAQAeAAAAAYQAAAPgPAHIAPAigAgNgFIAQAAQAPAAAAgLQAAgLgNABIgSAAg");
	this.shape.setTransform(392.5653,-32.1875,0.9958,0.9957);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AhIBeQgKAAgGgIQgHgIACgNIAaiMQABgHAGgGQAGgFAHAAIB4AAQAKAAAGAIQAGAHgBAKIgaCNQgCAIgFAHQgHAGgHAAg");
	this.shape_1.setTransform(-100.5306,-29.8475,0.9958,0.9957);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EB1D24").s().p("AN5IUQhHgPg7g+Qg6g/gOhMQgHgnABgqQgHAGgIAAIn0AAIgBAGQAAAHAHAXIACAHQARAsAoAaQAnAaAxgBQBhgCBQhbQAGgHAJAAIBwAAQALAAAGAKQAHAKgFAKQgwBmhhA+QhiA+hwAAQhNgDg/gkQg+glgkhAQglhFAChcIgxEOQgBAGgFADQgEAEgFAAIiDAAQgHAAgFgHQgDgDAAgGIAAgEQAgiGApj/QAskNAKgrQhEBQnLJ6QgFAGgHABIiGAAQgHAAgFgHQgEgEAAgGIAAgDQAeiMArj6IA2k2QhBBPooJ7QgFAFgIABIivAAQgJAAgFgLQgEgKAHgHINmv6QAFgGAHAAICJgBQAIAAAFAGQADAGAAAFIAAADQheIhgUBjQBFhPG3pDQAFgFAHAAICMAAQAIAAAEAGQAFAGgCAIIhnI8IADgHQAxhfBehCQBchABngNIACAAQBcgFBGAjQBGAjArBHIAAACQAjBGAEBUQAkhpBWhQQBIhFBXgaQBWgaBYAUIAEABIALAFQA3AYAYAWIAyj1QACgIAGgEQAFgFAIAAIBsAAQAKAAAHAIQAGAHgCAKIieNlQgDAPgSgBIhsABQgMAAgEgFQgFgFAAgIIAFgdQhpBDhkAAQgcAAgbgFgAQBgZQgqAIghAXQg6ArgfA4QgfA3AAA7QAAAXADAPQAGAeAQAZQAQAaAYARQAzAiBXgSQA8gOA0g2QA0g2ARhBQAGgXAAgZQACg0gXglQgjg5g6gOQgVgFgWAAQgTAAgTAEgAIZBTQgEglgkglQgYgUgXgIQgegKgtAEQgwAGgrAbQgvAegXAtIFDAAIAAAAg");
	this.shape_2.setTransform(-255.3606,-0.0539,0.9958,0.9957);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EB1D24").s().p("ADVHHIgKgEQgEAEgDAAIiCAAQgMAAgGgJQh+jWgrhDIgeCrIgVBpQgFAOgPAAIh1AAQgJAAgHgHQgGgIACgJIBYoWIgJABIgIABQh4ASggB5IhPGQQgCAIgFAFQgGAEgIAAIhoAAQgKAAgGgHQgHgHACgKIB3qVQABgHAGgEQAGgFAIAAIBsAAQALAAAFAHQAHAIgCAJIgDAQQAggUAkgNQAogOAkgBQAKAAAHAJIAAAAQAQhhAUheQABgIAGgFQAFgFAIAAIB3AAQAKAAAGAIQAFAGAAAHIAAAEQg0EXgRBkQA6hABhh9QAFgHAKAAICLAAQALAAAGAJQAGgJAMAAIBDAAIAji+QABgIAGgFQAGgFAIAAIB5AAQAJAAAHAIQAFAGAAAIIgjC6IB7AAQAIAAAIAHQAEAGAAAIIAAAEIgSBrQgBAIgGAFQgGAFgIAAIiDAAIhhIWQgBAHgGAFQgGAFgHAAgAAPBEIDGFJQAKhAAnjTIAojaIg7AAQgKAAgGgIQgGgHABgLIAQhSIjfEQg");
	this.shape_3.setTransform(329.764,6.0739,0.9958,0.9957);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EB1D24").s().p("Aq8IDQBKgdBFguQAjgXAUgRQB6hrAKiQQALiNhmhHQAJALAFAOQBOBgguCEQgvCHiQBHQgXALgnANQhNAahNAJQg0gigtgwQBLALBOgFQAmgCAYgEQCegfBQh3QBOh1g0hxIAAAGQAAAHgBAIQAYB9hqBeQhrBfiigMQgYgCgmgIQhMgQhGgdQggg8gRhDIgCgLQA8AvBFAkQAjASAWAIQCYA1B/g9QB8g8ANh8QgGAOgIAIQgsB1iIAZQiKAZiEhbQgXgSghgdQhAg6gsg0IhfIIQgDAPgSgBIhrAAQgMAAgFgGQgFgFAAgGIAFgdQiFBVh/gXQhHgPg7g+Qg6g/gNhMQgDgQgDgjIgCghIgwEOQgDAPgSgBIh4ABQgMAAgEgFQgGgHABgKIB4qWQABgIAGgFQAGgFAIAAIB4AAQAKAAAGAHQAGAIgBAKIgyENIACgFQAlhrBUhOQBJhFBWgaQBXgbBYAWIAPAFQA2AYAYAWQADgQAEgNIADgJQAIgRAPAAIBtAAQAKAAAHAGQAEAFAEAKQAYBFAtBLQAXAlASAYQBrB+CQAJQCNAJBFhtQgIAGgHAEIgEACQheBZiHgtQiJguhJiWQgKgWgMgmQgYhLgKhMQAmhBA2g0QgKBNAEBRQACAoAEAZQAfCiB3BQQB0BPBxg4IgQABIgKAAQh7AYhchrQhehtAMikQACgZAIgoQAQhPAehIQA6ggA9gQQguA9gjBGIgaA7Qg1CdBACCQA/CAB+AHIgRgKIgBgBQh7gmgdiMQgdiQBciLIAogzQA1g8A9gvQBNABBIAWQhJAdhEAtIg2AnQh8BtgJCSQgICPBqBFQgJgKgFgLQhVheAtiIQAuiLCThJIA+gYQBOgZBPgKQA7AlAwA0QhOgMhRAFIhDAGQigAghPB5QhPB3A5ByIgBgOIAAgJQgah/BqheQBshhCjAMIBCALQBRARBJAgQAgA9APA+IAEAUQg+gzhLgnIg+gdQiXg1h/A9Qh9A8gNB8QAHgOAJgLQAthzCHgYQCKgZCDBbIA1ArQA9A5AvBBQgCBBgTBAQgdhKgshFIgmg2Qhph6iNgMQiJgLhIBlQANgHAHgCQBfhSCDAvQCGAvBHCSIAfBUQAgBjAIBOILYtXQAGgHAGAAICZABQAHAAAFAFQADAEAAAIIAAADQhaIjgZBhQBEhOG0pFQAGgFAGAAICRgBQAHAAAFAGQAFAGgCAJIhsJMQAlhWBIhEQBJhFBWgaQBWgaBZAUIAEABIALAFQAyAUAgAeIAJgqQABgIAGgEQAGgEAHAAIBtAAQAKAAAGAGQAFAGAAAJIh5KZQgCAPgTAAIhrAAQgNAAgEgFQgEgGAAgHIAAgEIAEgZQiFBVh/gXQhHgPg7g+Qg6g/gNhMQgDgPADgZQAEgiAAgHIAAgSIgwEfQgEANgMAAIiEAAQgHAAgFgHQgEgFAAgFIABgDQAYh6AskBQApjvANg+QgqAyj1FGIjsE+QgEAFgHABIiHAAQgHAAgFgGQgDgEAAgGIAAgEQAaiHAqj1IA1ktQhBBPo0JoQgFAGgIAAIirAAQgKAAgEgKQgEgIAFgIIAAAAIAAgBIAQgaQASgjAHgjQALhMgIhRIgKhCQgeifhzhQQhxhQhwAyIAFAAQALAAAJACQB5gUBZBrQBbBsgLCiIgLBDQgRBRgfBKQg5Adg6APIgTAFQAyhAAnhLQATgmAJgZQA1idhAiDQg/h/h/gHQALAEAJAHQB6AmAdCMQAdCQhcCLQgPAVgcAgQg3A+hBAxQhIgEhHgYgAZHgaQgqAIghAXQg5ArgeA3QgfA3AAA8QAAATADATQASBDAsAeQAxAhBWgPQA7gOA1g3QA0g2AUhBQAGgUAAgeQAAgygVgmQgeg3hBgPQgVgFgVAAQgTAAgUAEgA2hgaQgqAIghAXQg5ArgeA3QgfA3AAA8QAAAXAEAPQATBEAqAdQAXAQAlAEQAmAEAsgIQA6gPAzg1QAxg1AThBQAHgWAAgcQAAgxgWgnQgeg3hBgPQgVgFgVAAQgTAAgUAEg");
	this.shape_4.setTransform(75.8694,0.0439,0.9958,0.9957);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mediamarkt_logo, new cjs.Rectangle(-400,-54,799.9,108.1), null);


(lib.m32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgeAwQgKgIgDgLIAdgQQAGAMAIAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgQALgKQANgJARAAQASAAAMAJQAKAIADAJIgdAQQgFgKgGAAQgBAAgBABQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGADIAQAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape.setTransform(189.9,8.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQALAJAAARQAAAIgEAJQgGAGgHAEIAZAvgAgJgJIAEAAQAEAAACgDQABgCAAgCQAAgEgBgCQgCgCgEgBIgEAAg");
	this.shape_1.setTransform(180.65,8.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_2.setTransform(171.225,8.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_3.setTransform(162.825,8.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_4.setTransform(152.625,8.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_5.setTransform(144.375,8.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAGA4IgPgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQAMAJAAARQgBAIgFAJQgEAGgIAEIAZAvgAgJgJIAEAAQAEAAACgDQABgCAAgCQAAgEgBgCQgCgCgEgBIgEAAg");
	this.shape_6.setTransform(137.2,8.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgrA4IAAhvIAuAAQATAAALALQALAKAAARQAAARgLAKQgMALgRAAIgIAAIAAAjgAgEgHIADAAQADgBADgCQACgCAAgEQAAgEgCgCQgDgCgCgBIgEAAg");
	this.shape_7.setTransform(127.25,8.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_8.setTransform(113.575,8.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_9.setTransform(103.825,8.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_10.setTransform(91.075,8.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_11.setTransform(81.325,8.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAGA4IgQgkIAAAkIgmAAIAAhvIAwAAQATAAAMALQAKAJABARQAAAIgGAJQgEAGgIAEIAaAvgAgKgJIAGAAQADAAABgDQACgCABgCQgBgEgCgCQgBgCgDgBIgGAAg");
	this.shape_12.setTransform(72.7,8.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIABQAJgBAFgGQAFgHAAgLQAAgKgFgGQgFgIgJAAQgIAAgGAIg");
	this.shape_13.setTransform(60.975,8.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgTA4IAAhQIgWAAIAAgfIBTAAIAAAfIgWAAIAABQg");
	this.shape_14.setTransform(50.225,8.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgTA4IAAhvIAnAAIAABvg");
	this.shape_15.setTransform(43.275,8.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_16.setTransform(35.025,8.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIABQAJgBAFgGQAFgHAAgLQAAgKgFgGQgFgIgJAAQgIAAgGAIg");
	this.shape_17.setTransform(22.975,8.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAdA4IAAg2IgMAtIgiAAIgLgtIAAA2IgnAAIAAhvIAyAAIARA/IARg/IAzAAIAABvg");
	this.shape_18.setTransform(9.35,8.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m32, new cjs.Rectangle(0,0,196.7,20), null);


(lib.m11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgdAwQgKgIgEgLIAcgQQAIAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgQAMgKQALgJASAAQASAAAMAJQAKAIADAJIgcAQQgGgKgHAAQAAAAgBABQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHADIAPAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgMgKg");
	this.shape.setTransform(92.65,8.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAGA4IgQgkIAAAkIgmAAIAAhvIAwAAQATAAAMALQAKAJABARQAAAIgGAJQgEAGgIAEIAaAvgAgKgJIAGAAQADAAABgDQACgCAAgCQAAgEgCgCQgBgCgDgBIgGAAg");
	this.shape_1.setTransform(83.4,8.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_2.setTransform(73.975,8.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgpApQgPgQAAgZQAAgXAQgRQARgRAZAAQAfAAASASIgYAYQgJgHgLgBQgIABgHAGQgHAHAAAJQAAALAHAHQAGAGAKABQAIgBAFgDIAAgEIgNAAIAAgaIAxAAIAAAwQgVATghAAQgaAAgSgRg");
	this.shape_3.setTransform(63.95,8.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIABQAJgBAFgGQAFgHAAgLQAAgKgFgGQgFgIgJAAQgIAAgGAIg");
	this.shape_4.setTransform(51.775,8.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAFA4IgOgkIAAAkIgoAAIAAhvIAxAAQAUAAAKALQALAJAAARQAAAIgEAJQgFAGgIAEIAZAvgAgJgJIAEAAQAEAAACgDQABgCAAgCQAAgEgBgCQgCgCgEgBIgEAAg");
	this.shape_5.setTransform(40.8,8.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_6.setTransform(30.125,8.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_7.setTransform(15.725,8.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_8.setTransform(5.975,8.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m11, new cjs.Rectangle(0,0,99.5,20), null);


(lib.m01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgdAwQgLgIgDgLIAdgQQAHAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgQALgKQANgJARAAQARAAANAJQAKAIADAJIgdAQQgFgKgGAAQgBAAgBABQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAGADIAQAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgMgKg");
	this.shape.setTransform(116.45,8.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAFA4IgPgkIAAAkIgnAAIAAhvIAxAAQAUAAAKALQAMAJgBARQAAAIgEAJQgGAGgHAEIAZAvgAgKgJIAGAAQADAAACgDQACgCAAgCQAAgEgCgCQgCgCgDgBIgGAAg");
	this.shape_1.setTransform(107.2,8.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AARA4IgFgRIgZAAIgFARIgoAAIAnhvIAnAAIAmBvgAAGAKIgGgWIgHAWIANAAg");
	this.shape_2.setTransform(95.8,8.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgtA4IAAhvIAvAAQAUAAALAJQAKAIABAOQAAAOgMAJQAGACAFAHQADAGAAAHQAAAQgLAKQgMAJgSAAgAgGAaIAGAAQADAAADgDQACgCAAgCQAAgEgCgCQgDgCgDAAIgGAAgAgGgNIAGAAQAHABAAgHQAAgGgHAAIgGAAg");
	this.shape_3.setTransform(85,8.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgyA4IAAhvIAoAAQAbAAARAPQARAPAAAZQAAAagRAPQgRAPgbAAgAgLAYIACAAQATAAAAgYQAAgXgTAAIgCAAg");
	this.shape_4.setTransform(74.675,8.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_5.setTransform(63.325,8.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgmAsQgNgMAAgVIAAhDIApAAIAABEQAAAMAKAAQALAAAAgMIAAhEIAoAAIAABDQAAAVgNAMQgNANgZAAQgYAAgOgNg");
	this.shape_6.setTransform(51.95,8.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgsApQgRgQAAgZQAAgYARgRQARgQAbAAQAcAAARAQQARARAAAYQAAAZgRAQQgRARgcAAQgbAAgRgRgAgOgQQgFAGAAAKQAAALAFAHQAGAGAIABQAJgBAFgGQAFgHAAgLQAAgKgFgGQgFgIgJAAQgIAAgGAIg");
	this.shape_7.setTransform(40.025,8.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgeAwQgJgIgEgLIAcgQQAIAMAHAAQAAAAABAAQABAAAAgBQABAAAAAAQAAgBAAgBQAAgDgGgDIgPgHQgWgLAAgUQAAgQAMgKQALgJASAAQASAAAMAJQAKAIADAJIgcAQQgFgKgIAAQAAAAgBABQgBAAAAAAQgBABAAAAQAAABAAAAQAAAEAHADIAPAIQAWAKAAATQAAAQgMAKQgMAKgUAAQgRAAgNgKg");
	this.shape_8.setTransform(29.1,8.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAMA4IgYg1IAAA1IgoAAIAAhvIApAAIAXA0IAAg0IApAAIAABvg");
	this.shape_9.setTransform(15.725,8.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgiA4IAAhvIBFAAIAAAfIgdAAIAAAKIAaAAIAAAcIgaAAIAAALIAdAAIAAAfg");
	this.shape_10.setTransform(5.975,8.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.m01, new cjs.Rectangle(0,0,123.3,20), null);


(lib.logobalk_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logobalk();
	this.instance.parent = this;
	this.instance.setTransform(0,-10,0.8886,0.8885);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.logobalk_1, new cjs.Rectangle(0,-10,303,85.3), null);


(lib.label_service = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(10));

	// Layer_2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgUAhQgGgGgDgHIATgLQAFAJAFgBQABAAAAAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgDgCIgLgFQgPgHAAgNQAAgMAIgGQAIgHAMAAQAMAAAJAHQAHAFACAGIgUALQgDgGgFAAQgBAAgBAAQAAAAAAAAQgBAAAAABQAAAAAAABQAAACAFACIAKAGQAPAGAAANQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape.setTransform(141.55,14.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgYAnIAAhNIAbAAIAAA3IAWAAIAAAWg");
	this.shape_1.setTransform(135.85,14.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgXAnIAAhNIAvAAIAAAWIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAWg");
	this.shape_2.setTransform(130.35,14.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAFAnIgMggIAAAgIgcAAIAAhNIAcAAIAAAcIAMgcIAeAAIgSAlIATAog");
	this.shape_3.setTransform(123.9,14.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_4.setTransform(116.025,14.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_5.setTransform(110.35,14.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAJAnIgJgoIgIAoIgcAAIgUhNIAdAAIAHAqIAKgqIAVAAIAJAqIAIgqIAdAAIgUBNg");
	this.shape_6.setTransform(102.825,14.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgXAnIAAhNIAvAAIAAAWIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAWg");
	this.shape_7.setTransform(92.2,14.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AggAnIAAgPIAdgoIgcAAIAAgWIBAAAIAAAPIgeAoIAeAAIAAAWg");
	this.shape_8.setTransform(86.025,14.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_9.setTransform(78.675,14.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgeAdQgLgMAAgRQAAgQALgMQAMgLASAAQATAAAMALQAMAMgBAQQABARgMAMQgLALgUAAQgSAAgMgLgAgJgLQgDAEgBAHQABAIADAEQADAFAGAAQAGAAAEgFQAEgEgBgIQABgHgEgEQgEgFgGAAQgGAAgDAFg");
	this.shape_10.setTransform(70.4,14.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_11.setTransform(60.025,14.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAMAnIgEgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAAEAHIgEgOIgEAOIAIAAg");
	this.shape_12.setTransform(52,14.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgNAnIgchNIAeAAIALAsIANgsIAdAAIgcBNg");
	this.shape_13.setTransform(43.65,14.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_14.setTransform(33.325,14.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgXAxIAAhNIAvAAIAAAWIgUAAIAAAGIARAAIAAAUIgRAAIAAAHIAUAAIAAAWgAgMgeIAIgRIAZAAIgNARg");
	this.shape_15.setTransform(26.65,13.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgXAxIAAhNIAvAAIAAAWIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAWgAgLgeIAHgRIAZAAIgNARg");
	this.shape_16.setTransform(21.2,13.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_17.setTransform(12.425,14.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_18.setTransform(6.75,14.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgUAhQgHgFgDgIIAUgLQAGAJAEgBQAAAAABAAQAAAAAAAAQABAAAAgBQAAAAAAgBQAAgCgEgCIgKgFQgPgHAAgNQAAgMAIgGQAIgHAMAAQAMAAAIAHQAIAFACAGIgUALQgEgGgEgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAABAFADIAKAGQAPAGAAANQAAALgIAHQgIAHgOAAQgLAAgJgHg");
	this.shape_19.setTransform(160.75,4.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgcAdQgKgMgBgRQAAgQAMgMQAMgLARAAQAUAAAMAMIgPARQgGgFgIAAQgFgBgFAFQgFAFAAAGQAAAHAFAFQAEAFAGgBQAGAAADgCIAAgCIgIAAIAAgSIAiAAIAAAhQgPANgXAAQgSAAgMgLg");
	this.shape_20.setTransform(153.45,4.4);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_21.setTransform(145.475,4.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AALAnIgCgNIgRAAIgEANIgcAAIAbhNIAaAAIAbBNgAADAHIgDgPIgEAPIAHAAg");
	this.shape_22.setTransform(137.45,4.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgYAnIAAhNIAbAAIAAA3IAWAAIAAAWg");
	this.shape_23.setTransform(130.8,4.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAUAnIAAglIgIAfIgYAAIgHgfIAAAlIgbAAIAAhNIAjAAIALArIAMgrIAjAAIAABNg");
	this.shape_24.setTransform(120.9,4.4);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgeAdQgMgMAAgRQAAgQAMgMQAMgLASAAQATAAAMALQAMAMAAAQQAAARgMAMQgLALgUAAQgSAAgMgLgAgJgLQgEAEAAAHQAAAHAEAFQAEAFAFAAQAGAAAEgFQADgFAAgHQAAgHgDgEQgEgFgGAAQgFAAgEAFg");
	this.shape_25.setTransform(111.55,4.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAFAnIgMggIAAAgIgcAAIAAhNIAcAAIAAAcIAMgcIAeAAIgTAlIAUAog");
	this.shape_26.setTransform(103.5,4.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgXAnIAAhNIAvAAIAAAWIgUAAIAAAJIASAAIAAAUIgSAAIAAAag");
	this.shape_27.setTransform(94.875,4.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgeAdQgLgMAAgRQAAgQALgMQAMgLASAAQATAAAMALQALAMAAAQQAAARgLAMQgLALgUAAQgSAAgMgLgAgJgLQgDAEgBAHQABAHADAFQAEAFAFAAQAGAAAEgFQADgFAAgHQAAgHgDgEQgEgFgGAAQgFAAgEAFg");
	this.shape_28.setTransform(87.85,4.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgXAnIAAhNIAvAAIAAAWIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAWg");
	this.shape_29.setTransform(78.7,4.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_30.setTransform(72.025,4.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_31.setTransform(66.35,4.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgYAnIAAhNIAbAAIAAA3IAWAAIAAAWg");
	this.shape_32.setTransform(62.05,4.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AAIAnIgQgkIAAAkIgbAAIAAhNIAcAAIAQAkIAAgkIAbAAIAABNg");
	this.shape_33.setTransform(55.325,4.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgeAdQgMgMABgRQgBgQAMgMQALgLATAAQATAAAMALQALAMAAAQQAAARgLAMQgMALgTAAQgTAAgLgLgAgJgLQgEAEABAHQgBAHAEAFQADAFAGAAQAGAAAEgFQADgFABgHQgBgHgDgEQgEgFgGAAQgGAAgDAFg");
	this.shape_34.setTransform(47.05,4.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AAFAnIgMggIAAAgIgcAAIAAhNIAcAAIAAAcIAMgcIAeAAIgTAlIAUAog");
	this.shape_35.setTransform(36.9,4.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgUAlIADgVIAGABQAFAAAAgEIAAgzIAbAAIAAA3QAAAWgZAAQgIAAgIgCg");
	this.shape_36.setTransform(30.55,4.425);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_37.setTransform(26.65,4.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AAFAnIgMggIAAAgIgcAAIAAhNIAcAAIAAAcIAMgcIAeAAIgSAlIATAog");
	this.shape_38.setTransform(21.2,4.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgXAnIAAhNIAvAAIAAAWIgUAAIAAAGIASAAIAAAUIgSAAIAAAHIAUAAIAAAWg");
	this.shape_39.setTransform(14.55,4.4);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgfAnIAAhNIAgAAQAOAAAHAHQAIAFAAAKQAAAKgIAFQAEACADAEQACAFAAAEQABAMgIAHQgIAFgNABgAgEASIAEAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAgBQABAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBgBAAAAIgEAAgAgEgJIAEAAQAFAAgBgDQABgFgFAAIgEAAg");
	this.shape_40.setTransform(8.5,4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},9).wait(1));

	// Layer_1
	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AtMCTIAAklIaZAAIAAElg");
	this.shape_41.setTransform(84.9902,11.4472,1.0059,0.7796);
	this.shape_41._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape_41).wait(9).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-2,171,25);


(lib.label_green_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(7));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgoBBIAHgNQAPAJARAAQARAAAJgHQAMgJAAgRIAAgJQgNAUgYAAQgXAAgPgRQgOgPAAgZQAAgZAOgQQAPgQAXAAQAOAAALAIQAIAFAFAHIAAgRIAQAAIAABdQAAA3g2AAQgWAAgSgLgAgZgwQgLAMAAASQAAASALAMQAKALAQAAQAQAAALgMQAJgLABgSQgBgTgKgMQgLgLgQAAQgPAAgKAMg");
	this.shape.setTransform(148.15,14.175);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgGgIgMAAQgMAAgHAJQgIAIAAANIAABCIgRAAIAAhsIAQAAIAAAPQAFgHAIgFQAJgGAKAAQATAAAKANQAKAMAAASIAABEg");
	this.shape_1.setTransform(136.4,12.15);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgHBKIAAhsIAQAAIAABsgAgHg2QgEgDAAgFQAAgFAEgDQADgDAEAAQAFAAADADQADADABAFQgBAFgDADQgDADgFAAQgEAAgDgDg");
	this.shape_2.setTransform(128.35,10.325);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgoBBIAHgNQAOAJATAAQAQAAAKgHQALgJAAgRIAAgJQgNAUgYAAQgXAAgPgRQgOgPAAgZQAAgZAOgQQAPgQAXAAQAOAAAMAIQAIAFAEAHIAAgRIAQAAIAABdQAAA3g2AAQgWAAgSgLgAgZgwQgLAMAAASQAAASALAMQALALAPAAQAQAAALgMQAKgLAAgSQgBgTgKgMQgLgLgQAAQgPAAgKAMg");
	this.shape_3.setTransform(119.45,14.175);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_4.setTransform(110.575,12.15);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYAQgQQAQgQAYAAQAYAAARAQQAQAQAAAYQAAAYgQARQgQAQgZABQgXgBgRgQgAgcgdQgLAMAAARQAAASALAMQAMAMAQAAQARAAAMgMQALgMAAgSQAAgSgLgLQgMgNgRAAQgQAAgMANg");
	this.shape_5.setTransform(100.35,12.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgoA2IAAgLIA9hSIg8AAIAAgPIBQAAIAAAMIg+BTIA+AAIAAANg");
	this.shape_6.setTransform(89.1,12.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgkAqQgPgQgBgaQABgZAPgPQAPgRAVABQAYAAAOAPQAOAPABAZIAAAFIhYAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgJAHQgNAHgSABQgXAAgQgQgAAkgIQgBgQgKgKQgKgIgPAAQgMAAgKAJQgLAKgBAPIBGAAIAAAAg");
	this.shape_7.setTransform(78.35,12.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AglA5IAAARIgPAAIAAiXIAQAAIAAA9QAEgHAIgFQAMgJAOABQAWgBAPARQAPAQAAAYQAAAZgQAQQgPAQgWABQgYAAgOgVgAgagKQgLALAAATQAAASAKAMQALANAQAAQAPAAALgMQAKgMABgTQAAgSgLgLQgKgNgQAAQgQAAgKAMg");
	this.shape_8.setTransform(66.35,10.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AglAiIAMgIQAJARARAAQAJAAAFgFQAGgEAAgJQAAgIgIgFQgEgCgOgGQgNgFgGgGQgIgIAAgMQAAgOAKgHQAJgJAPABQANAAAKAHQAIAGACAHIgMAIQgHgPgPAAQgHAAgFAFQgFADAAAHQAAAJAIAFIASAIQANAGAGAEQAJAIAAANQAAAOgLAKQgLAIgQABQgaAAgLgYg");
	this.shape_9.setTransform(49.875,12.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgHBKIAAhsIAPAAIAABsgAgHg2QgEgDABgFQgBgFAEgDQADgDAEAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_10.setTransform(43.25,10.325);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFAMAAIAEgBIAAAPIgKABQgfAAAAgng");
	this.shape_11.setTransform(37.85,11.225);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_12.setTransform(29.225,12.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_13.setTransform(21.325,12.15);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgtA3QgVgWAAghQAAggAVgVQAWgWAfAAQAhAAAVAVIgLALQgSgRgZAAQgYAAgQASQgQARAAAZQAAAaAQARQAQARAYAAQAbAAAQgPIAAgfIghAAIAAgOIAxAAIAAA0QgYAYgkAAQgeAAgWgVg");
	this.shape_14.setTransform(9.5,10.425);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgoBBIAGgNQAPAJATAAQAPAAALgHQALgJAAgRIAAgJQgMAUgZAAQgXAAgPgRQgPgPAAgZQAAgZAPgQQAPgQAXAAQANAAANAIQAHAFAEAHIAAgRIARAAIAABdQgBA3g1AAQgWAAgSgLgAgagwQgKAMAAASQAAASALAMQAKALAPAAQARAAALgMQAKgLgBgSQABgTgLgMQgLgLgQAAQgPAAgLAMg");
	this.shape_15.setTransform(149.8,14.175);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgNAAgHAJQgIAIAAANIAABCIgQAAIAAhsIAPAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_16.setTransform(138.05,12.15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgHBKIAAhsIAQAAIAABsgAgHg2QgEgDABgFQgBgFAEgDQADgDAEAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_17.setTransform(130,10.325);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFALAAIAFgBIAAAPIgKABQgfAAAAgng");
	this.shape_18.setTransform(124.6,11.225);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAHAIALgBQAMABAIgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgJAGgKAAQgSAAgLgNg");
	this.shape_19.setTransform(111.4,12.45);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgHBMIAAiXIAPAAIAACXg");
	this.shape_20.setTransform(103.475,10.15);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgNAAgHAJQgIAIAAANIAABCIgQAAIAAhsIAPAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_21.setTransform(86.75,12.15);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_22.setTransform(64.625,12.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AglAiIAMgIQAJARARAAQAJAAAFgFQAGgEAAgJQAAgIgIgFQgEgCgOgGQgNgFgGgGQgIgIAAgMQAAgOAKgHQAJgJAPABQANAAAKAHQAIAGACAHIgMAIQgHgPgPAAQgHAAgFAFQgFADAAAHQAAAJAIAFIASAIQANAGAGAEQAJAIAAANQAAAOgLAKQgLAIgQABQgaAAgLgYg");
	this.shape_23.setTransform(49.875,12.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_24.setTransform(29.225,12.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYARgQQAPgQAYAAQAZAAAQAQQAQAQAAAYQAAAYgQARQgQAQgZABQgXgBgRgQgAgcgdQgLAMAAARQAAASALAMQAMAMAQAAQARAAAMgMQALgMAAgSQAAgSgLgLQgMgNgRAAQgQAAgMANg");
	this.shape_25.setTransform(232.55,12.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgNAcIAAg5IgNAAIAAgOIANAAIAAgXIAPAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFAMAAIAEgBIAAAPIgKABQgfAAABgng");
	this.shape_26.setTransform(223,11.225);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgkAqQgPgQAAgaQAAgZAPgPQAQgRAUABQAZAAAOAPQAOAPgBAZIAAAFIhXAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgQABQgYAAgQgQgAAjgIQAAgQgLgKQgJgIgPAAQgMAAgKAJQgKAKgBAPIBEAAIAAAAg");
	this.shape_27.setTransform(214.1,12.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFALAAIAFgBIAAAPIgKABQgeAAgBgng");
	this.shape_28.setTransform(179.75,11.225);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_29.setTransform(163.225,12.15);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgnBBIAFgNQAQAJASAAQAPAAALgHQALgJAAgRIAAgJQgNAUgYAAQgXAAgPgRQgPgPAAgZQAAgZAPgQQAPgQAXAAQAOAAAMAIQAIAFADAHIAAgRIAQAAIAABdQABA3g2AAQgWAAgRgLgAgagwQgKAMAAASQAAASALAMQALALAOAAQARAAAKgMQAKgLAAgSQAAgTgLgMQgKgLgQAAQgPAAgLAMg");
	this.shape_30.setTransform(152.7,14.175);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgXA4IAAhsIAQAAIAAASQADgJAHgGQAKgGALAAIAAAQQgfABAAAqIAAA0g");
	this.shape_31.setTransform(110.975,12.15);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_32.setTransform(101.475,12.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("Ag1BMIAAiUIAQAAIAAARQAOgUAXAAQAYAAAPAQQAOAQAAAZQAAAZgOAQQgPAQgWAAQgPAAgLgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQALALAPAAQAQAAAKgMQALgMgBgSQAAgSgLgMQgKgMgPAAQgQAAgLANg");
	this.shape_33.setTransform(90.35,14.125);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("Ag0BMIAAiUIAPAAIAAARQAOgUAYAAQAWAAAQAQQAPAQAAAZQAAAZgPAQQgPAQgWAAQgOAAgMgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQAKALAQAAQAQAAAKgMQALgMAAgSQAAgSgMgMQgJgMgQAAQgQAAgLANg");
	this.shape_34.setTransform(77.5,14.125);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgfAvQgMgKAAgQQAAgPAMgKQAMgKASAAQATAAAJAKIAAgOQAAgLgHgHQgHgFgMgBQgQABgPAIIgHgMQATgMAUABQAqgBAAAoIAABHIgPAAIgBgJQgLANgSAAQgSAAgMgLgAgSAFQgIAGAAAJQAAAKAIAHQAHAGAMAAQAMAAAHgGQAIgHAAgKQAAgJgIgGQgHgGgMAAQgMAAgHAGg");
	this.shape_35.setTransform(65.075,12.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgjAqQgRgQABgaQgBgZARgPQAOgRAWABQAYAAANAPQAPAPAAAZIAAAFIhXAAQAAARALALQAKALARAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgRABQgXAAgPgQgAAkgIQgBgQgLgKQgJgIgOAAQgNAAgKAJQgLAKAAAPIBFAAIAAAAg");
	this.shape_36.setTransform(48.8,12.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AglA9QgPgQgBgZQAAgYAPgQQAOgRAYABQANgBAMAJQAIAFAEAHIAAg9IAQAAIAACXIgQAAIAAgRQgNAVgYAAQgWgBgPgQgAgZgJQgLALAAASQAAATALAMQAKAMAPAAQARAAAKgNQALgMgBgSQABgTgLgLQgLgMgQAAQgPAAgKANg");
	this.shape_37.setTransform(36.05,10.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAIAIAKgBQANABAHgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgKAGgJAAQgSAAgLgNg");
	this.shape_38.setTransform(24.3,12.45);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("Ag1A2QgVgWAAggQAAggAVgWQAWgVAfAAQAgAAAWAWQAVAWAAAfQAAAggVAWQgWAWggAAQgfAAgWgWgAgogqQgQARAAAZQAAAaAQARQARARAXAAQAZAAAQgRQAQgRAAgaQAAgZgQgRQgQgSgZAAQgXAAgRASg");
	this.shape_39.setTransform(10.3,10.425);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgkAqQgQgQAAgaQAAgZAQgPQAQgRAUABQAZAAAOAPQAOAPgBAZIAAAFIhXAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgQABQgYAAgQgQgAAjgIQAAgQgLgKQgJgIgPAAQgMAAgKAJQgKAKgCAPIBFAAIAAAAg");
	this.shape_40.setTransform(132.55,12.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgHgIgLAAQgLAAgIAJQgIAIAAANIAABCIgRAAIAAhsIARAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMAAASIAABEg");
	this.shape_41.setTransform(120.9,12.15);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AghArQgKgMAAgTIAAhDIARAAIAABDQAAANAHAIQAIAIAKgBQANABAHgJQAIgJAAgNIAAhBIARAAIAABsIgRAAIAAgQQgEAIgIAFQgKAGgJAAQgTAAgKgNg");
	this.shape_42.setTransform(102.7,12.45);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgoApQgQgQAAgZQAAgYAQgQQAQgQAYAAQAZAAAQAQQAQAQAAAYQAAAYgRARQgPAQgZABQgYgBgQgQgAgcgdQgLAMAAARQAAASALAMQAMAMAQAAQASAAALgMQALgMAAgSQAAgSgLgLQgLgNgSAAQgQAAgMANg");
	this.shape_43.setTransform(90.65,12.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgNAcIAAg5IgNAAIAAgOIANAAIAAgXIAPAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFALAAIAFgBIAAAPIgKABQgeAAAAgng");
	this.shape_44.setTransform(81.1,11.225);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AglA9QgPgQgBgZQAAgYAPgQQAOgRAXABQAPgBALAJQAIAFAEAHIAAg9IAQAAIAACXIgQAAIAAgRQgNAVgYAAQgXgBgOgQgAgZgJQgLALAAASQAAATALAMQALAMAOAAQARAAAKgNQAKgMAAgSQAAgTgLgLQgKgMgQAAQgOAAgLANg");
	this.shape_45.setTransform(186.9,10.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AgVBeIAAgOIAFAAQASAAAAgXIAAhvIARAAIAABwQAAAkgiAAIgGAAgAAChKQgDgDABgFQgBgFADgDQADgDAFAAQAFAAADADQAEADgBAFQABAFgEADQgDADgFAAQgFAAgDgDg");
	this.shape_46.setTransform(177.6,12.325);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AAVBMIgug6IAAA6IgQAAIAAiXIAQAAIAABZIAsguIAVAAIguAvIAwA9g");
	this.shape_47.setTransform(161.375,10.15);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgIgHQgGgIgMAAQgMAAgHAJQgIAIAAANIAABCIgRAAIAAhsIAQAAIAAAPQAFgHAJgFQAIgGAKAAQATAAAKANQAJAMAAASIAABEg");
	this.shape_48.setTransform(150,12.15);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AgkAqQgQgQAAgaQAAgZAQgPQAQgRAUABQAYAAAOAPQAOAPABAZIAAAFIhYAAQABARALALQALALAQAAQAWgBAMgRIALAJQgGAJgJAHQgNAHgRABQgYAAgQgQgAAjgIQAAgQgKgKQgKgIgPAAQgMAAgKAJQgLAKgBAPIBFAAIAAAAg");
	this.shape_49.setTransform(138.3,12.3);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AgmA9QgOgQAAgZQAAgYAOgQQAPgRAWABQAOgBAMAJQAIAFAEAHIAAg9IAQAAIAACXIgPAAIAAgRQgOAVgYAAQgXgBgPgQgAgZgJQgLALAAASQAAATALAMQAKAMAQAAQARAAAKgNQAJgMABgSQAAgTgMgLQgJgMgQAAQgQAAgKANg");
	this.shape_50.setTransform(125.55,10.3);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AglA5IAAARIgQAAIAAiXIARAAIAAA9QAEgHAIgFQALgJAPABQAWgBAPARQAOAQAAAYQAAAZgOAQQgPAQgYABQgXAAgOgVgAgagKQgLALAAATQAAASAKAMQALANAQAAQAPAAAKgMQALgMAAgTQABgSgLgLQgKgNgQAAQgPAAgLAMg");
	this.shape_51.setTransform(101.6,10.3);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgMAAgIAJQgIAIAAANIAABCIgQAAIAAhsIAQAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMgBASIAABEg");
	this.shape_52.setTransform(83.8,12.15);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AgjAqQgRgQABgaQgBgZARgPQAPgRAVABQAXAAAPAPQANAPAAAZIAAAFIhWAAQAAARALALQAKALARAAQAWgBAMgRIALAJQgGAJgKAHQgNAHgRABQgXAAgPgQgAAkgIQgCgQgKgKQgJgIgOAAQgNAAgKAJQgKAKgBAPIBFAAIAAAAg");
	this.shape_53.setTransform(72.1,12.3);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#000000").s().p("AglA9QgPgQAAgZQgBgYAPgQQAOgRAYABQANgBAMAJQAIAFAEAHIAAg9IARAAIAACXIgRAAIAAgRQgNAVgYAAQgWgBgPgQgAgZgJQgKALAAASQAAATAKAMQAKAMAPAAQARAAALgNQAKgMgBgSQABgTgLgLQgLgMgQAAQgPAAgKANg");
	this.shape_54.setTransform(35.95,10.3);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#000000").s().p("AglA4QgKgSAAgmQAAgmAKgRQAMgUAZAAQAbAAALAUQAJARABAmQgBAmgJASQgLAUgbAAQgZAAgMgUgAgYgqQgFAMAAAeQAAAeAFANQAGARASAAQATAAAGgRQAFgNAAgeQAAgegFgMQgGgSgTAAQgSAAgGASg");
	this.shape_55.setTransform(18.8,10.425);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("AggBAQgJgIgEgNIAOgFQAHAVAYAAQAMAAAJgIQAIgIAAgNQAAgNgJgIQgJgIgQAAIgLAAIAAgNIAmguIg+AAIAAgQIBRAAIAAAQIgmAtQAQAAANAKQAOALAAAVQAAAUgNANQgNANgUAAQgTAAgNgLg");
	this.shape_56.setTransform(7.425,10.525);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AgnBBIAFgNQAQAJARAAQAQAAAKgHQAMgJAAgRIAAgJQgMAUgZAAQgXAAgPgRQgPgPAAgZQAAgZAPgQQAPgQAXAAQANAAAMAIQAJAFADAHIAAgRIAQAAIAABdQABA3g2AAQgWAAgRgLgAgagwQgKAMAAASQAAASALAMQALALAOAAQARAAAKgMQAKgLAAgSQAAgTgLgMQgKgLgQAAQgPAAgLAMg");
	this.shape_57.setTransform(135.25,14.175);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AAbA4IAAhEQAAgNgHgHQgIgIgKAAQgMAAgIAJQgIAIAAANIAABCIgQAAIAAhsIAQAAIAAAPQAEgHAIgFQAKgGAJAAQASAAALANQAKAMAAASIAABEg");
	this.shape_58.setTransform(123.5,12.15);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AgHBKIAAhsIAPAAIAABsgAgHg2QgDgDgBgFQABgFADgDQADgDAEAAQAFAAADADQAEADAAAFQAAAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_59.setTransform(115.45,10.325);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("Ag0BMIAAiUIAPAAIAAARQAOgUAYAAQAWAAAPAQQAQAQAAAZQAAAZgPAQQgPAQgWAAQgOAAgMgIQgIgGgEgGIAAA5gAgbgvQgKAMAAARQAAATALAMQAKALAQAAQAQAAAKgMQALgMAAgSQgBgSgKgMQgLgMgPAAQgQAAgLANg");
	this.shape_60.setTransform(66.35,14.125);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AgOAcIAAg5IgMAAIAAgOIAMAAIAAgXIAQAAIAAAXIAYAAIAAAOIgYAAIAAA3QAAAPAFAGQAEAFAMAAIAEgBIAAAPIgKABQgfAAAAgng");
	this.shape_61.setTransform(37.85,11.225);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_14},{t:this.shape_13,p:{x:21.325}},{t:this.shape_12,p:{x:29.225}},{t:this.shape_11,p:{x:37.85}},{t:this.shape_10},{t:this.shape_9,p:{x:49.875}},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4,p:{x:110.575}},{t:this.shape_3},{t:this.shape_2,p:{x:128.35}},{t:this.shape_1},{t:this.shape,p:{x:148.15}}]},1).to({state:[{t:this.shape_14},{t:this.shape_4,p:{x:21.325}},{t:this.shape_24,p:{x:29.225}},{t:this.shape_11,p:{x:37.85}},{t:this.shape_10},{t:this.shape_23},{t:this.shape_22,p:{x:64.625}},{t:this.shape_12,p:{x:75.325}},{t:this.shape_21},{t:this.shape_9,p:{x:96.825}},{t:this.shape_20,p:{x:103.475}},{t:this.shape_19,p:{x:111.4}},{t:this.shape_2,p:{x:119.35}},{t:this.shape_18,p:{x:124.6}},{t:this.shape_17,p:{x:130}},{t:this.shape_16,p:{x:138.05}},{t:this.shape_15}]},1).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36,p:{x:48.8}},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31,p:{x:110.975}},{t:this.shape_24,p:{x:118.875}},{t:this.shape_22,p:{x:129.575}},{t:this.shape_11,p:{x:138.2}},{t:this.shape_30},{t:this.shape_29,p:{x:163.225}},{t:this.shape_12,p:{x:171.125}},{t:this.shape_28},{t:this.shape_2,p:{x:185.15}},{t:this.shape_9,p:{x:191.775}},{t:this.shape_13,p:{x:205.325}},{t:this.shape_27,p:{x:214.1}},{t:this.shape_26},{t:this.shape_25},{t:this.shape_19,p:{x:244.6}},{t:this.shape_4,p:{x:254.175}}]},1).to({state:[{t:this.shape_14},{t:this.shape_31,p:{x:21.325}},{t:this.shape_12,p:{x:29.225}},{t:this.shape_11,p:{x:37.85}},{t:this.shape_10},{t:this.shape_9,p:{x:49.875}},{t:this.shape_29,p:{x:63.425}},{t:this.shape_36,p:{x:72.2}},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_13,p:{x:112.275}},{t:this.shape_41},{t:this.shape_40},{t:this.shape_4,p:{x:142.325}},{t:this.shape_27,p:{x:151.1}},{t:this.shape_16,p:{x:162.8}}]},1).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_12,p:{x:47.825}},{t:this.shape,p:{x:59.5}},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_36,p:{x:113.6}},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_18,p:{x:168.7}},{t:this.shape_17,p:{x:174.1}},{t:this.shape_46},{t:this.shape_45}]},1).to({state:[{t:this.shape_14},{t:this.shape_4,p:{x:21.325}},{t:this.shape_24,p:{x:29.225}},{t:this.shape_61},{t:this.shape_10},{t:this.shape_23},{t:this.shape_60},{t:this.shape_20,p:{x:74.675}},{t:this.shape_22,p:{x:81.975}},{t:this.shape_12,p:{x:92.675}},{t:this.shape_11,p:{x:101.3}},{t:this.shape_9,p:{x:108.825}},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,297.3,25);


(lib.label_green_circle_checl_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhOASIADgjICaABIAAAig");
	this.shape.setTransform(7.9,1.725);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle_checl_2, new cjs.Rectangle(0,0,15.8,3.5), null);


(lib.label_green_circle_check_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgygRIBjAAIACAhIhlACg");
	this.shape.setTransform(5.075,1.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle_check_1, new cjs.Rectangle(0,0,10.2,3.5), null);


(lib.intro_img_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.intro_img();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.intro_img_1, new cjs.Rectangle(0,0,281,114), null);


(lib.endframe_static_line4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAQBNIgghJIAABJIg4AAIAAiZIA5AAIAgBIIAAhIIA4AAIAACZg");
	this.shape.setTransform(242.975,10.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_1.setTransform(229.6,10.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAIBNIgVgxIAAAxIg2AAIAAiZIBDAAQAbAAAPAPQAPANAAAWQAAAMgHALQgGAJgLAHIAjBAgAgNgNIAHAAQAFAAACgDQADgDAAgEQAAgFgDgDQgCgDgFAAIgHAAg");
	this.shape_2.setTransform(217.725,10.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAYBNIgHgYIgjAAIgHAYIg3AAIA2iZIA2AAIA1CZgAAHAOIgHgeIgJAeIAQAAg");
	this.shape_3.setTransform(202.075,10.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgbBNIg4iZIA8AAIAYBZIAZhZIA6AAIg4CZg");
	this.shape_4.setTransform(185.35,10.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAIBNIgVgxIAAAxIg2AAIAAiZIBDAAQAbAAAPAPQAPANAAAWQAAAMgHALQgGAJgLAHIAjBAgAgNgNIAHAAQAFAAACgDQADgDAAgEQAAgFgDgDQgCgDgFAAIgHAAg");
	this.shape_5.setTransform(170.375,10.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_6.setTransform(157.45,10.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_7.setTransform(142.35,10.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgbBNIAAhtIgeAAIAAgsIBzAAIAAAsIgeAAIAABtg");
	this.shape_8.setTransform(130.8,10.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AhGBNIAAiZIA3AAQAnAAAXAUQAXAVABAjQgBAjgXAVQgXAVgnAAgAgQAhIAEAAQAZAAAAghQAAgggZAAIgEAAg");
	this.shape_9.setTransform(113.25,10.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgbBNIAAiZIA3AAIAACZg");
	this.shape_10.setTransform(102.1,10.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgzA9QgTgSAAgcIAAhdIA4AAIAABeQAAAQAOABQAPgBAAgQIAAheIA4AAIAABdQAAAcgSASQgTARgiABQghgBgSgRg");
	this.shape_11.setTransform(90.9,10.95);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgwBNIAAiZIA3AAIAABtIAqAAIAAAsg");
	this.shape_12.setTransform(77.95,10.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_13.setTransform(66.95,10.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("Ag4A5QgWgWAAgjQAAghAXgXQAXgXAkAAQApAAAZAZIggAhQgMgLgQABQgMgBgJAKQgJAJAAANQAAAPAJAJQAIAJANAAQAMAAAHgDIAAgHIgRAAIAAgkIBDAAIAABEQgdAZguAAQglAAgXgXg");
	this.shape_14.setTransform(53.175,10.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgbBNIAAhtIgeAAIAAgsIBzAAIAAAsIgeAAIAABtg");
	this.shape_15.setTransform(34.8,10.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_16.setTransform(23.2,10.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAQBNIAAg3IgfAAIAAA3Ig4AAIAAiZIA4AAIAAA3IAfAAIAAg3IA4AAIAACZg");
	this.shape_17.setTransform(9.875,10.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,256,26);


(lib.endframe_static_line3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.endframe_static_line2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAQBNIgghJIAABJIg4AAIAAiZIA5AAIAgBIIAAhIIA4AAIAACZg");
	this.shape.setTransform(407.425,10.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_1.setTransform(394.05,10.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAIBNIgVgxIAAAxIg2AAIAAiZIBDAAQAbAAAPAPQAPANAAAWQAAAMgHALQgGAJgLAHIAjBAgAgNgNIAHAAQAFAAACgDQADgDAAgEQAAgFgDgDQgCgDgFAAIgHAAg");
	this.shape_2.setTransform(382.175,10.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAYBNIgHgYIgjAAIgHAYIg3AAIA2iZIA2AAIA1CZgAAHAOIgHgeIgJAeIAQAAg");
	this.shape_3.setTransform(366.525,10.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgbBNIg4iZIA9AAIAXBZIAZhZIA6AAIg4CZg");
	this.shape_4.setTransform(349.8,10.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AAIBNIgVgxIAAAxIg2AAIAAiZIBDAAQAbAAAPAPQAPANAAAWQAAAMgHALQgGAJgLAHIAjBAgAgNgNIAHAAQAFAAACgDQADgDAAgEQAAgFgDgDQgCgDgFAAIgHAAg");
	this.shape_5.setTransform(334.825,10.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_6.setTransform(321.9,10.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_7.setTransform(306.8,10.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgbBNIAAhtIgeAAIAAgsIBzAAIAAAsIgeAAIAABtg");
	this.shape_8.setTransform(295.25,10.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgpBCQgNgLgGgPIAogWQAKAQAKAAQAEABAAgEQAAgFgJgEIgUgKQgegPAAgbQAAgWAQgOQAQgNAZAAQAXAAASANQAOALAEANIgoAWQgGgOgKAAQgFAAAAAEQAAAEAJAFIAVAKQAeAQAAAZQAAAXgQAOQgQANgcAAQgYAAgRgOg");
	this.shape_9.setTransform(278.575,10.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgZAWQAMgGAEgLQgSgGAAgRQAAgKAIgIQAIgHALAAQAMgBAIAIQAIAHAAANQAAARgJAQQgJARgQALg");
	this.shape_10.setTransform(268.525,6.5);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgbBNIg4iZIA8AAIAYBZIAZhZIA6AAIg3CZg");
	this.shape_11.setTransform(256.25,10.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgbBNIAAhtIgeAAIAAgsIBzAAIAAAsIgeAAIAABtg");
	this.shape_12.setTransform(241.6,10.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_13.setTransform(225.8,10.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AhGBNIAAiZIA3AAQAnAAAXAUQAXAVABAjQgBAjgXAVQgXAVgnAAgAgPAhIADAAQAZAAAAghQAAgggZAAIgDAAg");
	this.shape_14.setTransform(213.15,10.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAoBNIAAhKIgPA/IgxAAIgPg/IAABKIg2AAIAAiZIBGAAIAXBXIAYhXIBGAAIAACZg");
	this.shape_15.setTransform(191.175,10.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("Ag9A5QgXgWAAgjQAAgiAXgWQAYgXAlAAQAnAAAXAXQAXAWAAAiQAAAjgXAWQgXAXgnAAQgmAAgXgXgAgTgXQgHAJAAAOQAAAPAHAJQAHAKAMAAQANAAAHgKQAHgJAAgPQAAgOgHgJQgHgKgNAAQgMAAgHAKg");
	this.shape_16.setTransform(172.425,10.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgpBCQgNgLgGgPIAogWQAKAQAKAAQAEABAAgEQAAgFgJgEIgUgKQgegPAAgbQAAgWAQgOQAQgNAZAAQAXAAASANQAOALAEANIgoAWQgGgOgKAAQgFAAAAAEQAAAEAJAFIAVAKQAeAQAAAZQAAAXgQAOQgQANgcAAQgYAAgRgOg");
	this.shape_17.setTransform(153.225,10.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgwBNIAAiZIA3AAIAABtIAqAAIAAAsg");
	this.shape_18.setTransform(141.7,10.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_19.setTransform(130.7,10.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AALBNIgahAIAABAIg5AAIAAiZIA5AAIAAA4IAZg4IA9AAIgnBJIAoBQg");
	this.shape_20.setTransform(117.8,10.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAQBNIgghJIAABJIg4AAIAAiZIA5AAIAgBIIAAhIIA4AAIAACZg");
	this.shape_21.setTransform(102.025,10.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgbBNIAAiZIA3AAIAACZg");
	this.shape_22.setTransform(90.65,10.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AATBNIgThQIgSBQIg4AAIgoiZIA8AAIAOBTIAThTIArAAIATBTIAOhTIA8AAIgoCZg");
	this.shape_23.setTransform(75.575,10.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgvBNIAAiZIBfAAIAAAsIgoAAIAAAMIAkAAIAAAoIgkAAIAAANIAoAAIAAAsg");
	this.shape_24.setTransform(54.3,10.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AhBBNIAAgdIA6hRIg4AAIAAgrICAAAIAAAcIg7BRIA8AAIAAAsg");
	this.shape_25.setTransform(42.025,10.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAQBNIgghJIAABJIg4AAIAAiZIA5AAIAgBIIAAhIIA4AAIAACZg");
	this.shape_26.setTransform(27.275,10.8);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("Ag9A5QgXgWAAgjQAAgiAXgWQAYgXAlAAQAnAAAXAXQAXAWAAAiQAAAjgXAWQgXAXgnAAQgmAAgXgXgAgTgXQgHAJAAAOQAAAPAHAJQAHAKAMAAQANAAAHgKQAHgJAAgPQAAgOgHgJQgHgKgNAAQgMAAgHAKg");
	this.shape_27.setTransform(10.675,10.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_28.setTransform(426.325,10.4);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_29.setTransform(413.575,10.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AAIBJIgVgvIAAAvIgzAAIAAiRIBAAAQAZgBAPAOQAOANAAAVQABAMgIALQgFAIgLAGIAiA9gAgNgMIAHAAQAFAAABgEQADgCAAgFQAAgEgDgCQgBgEgFAAIgHAAg");
	this.shape_30.setTransform(402.25,10.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AAWBJIgGgWIghAAIgHAWIg0AAIAziRIA0AAIAyCRgAAHAOIgHgeIgIAeIAPAAg");
	this.shape_31.setTransform(387.3,10.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgaBJIg1iRIA6AAIAWBUIAXhUIA4AAIg1CRg");
	this.shape_32.setTransform(371.375,10.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AAHBJIgTgvIAAAvIg0AAIAAiRIBAAAQAagBAPAOQANANAAAVQAAAMgGALQgHAIgJAGIAhA9gAgMgMIAGAAQAEAAACgEQAEgCAAgFQAAgEgEgCQgCgEgEAAIgGAAg");
	this.shape_33.setTransform(357.1,10.4);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_34.setTransform(344.775,10.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_35.setTransform(330.325,10.4);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgZBJIAAhoIgdAAIAAgpIBuAAIAAApIgdAAIAABog");
	this.shape_36.setTransform(319.25,10.4);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AhCBJIAAiRIA0AAQAkgBAWAUQAXATAAAiQAAAigWAUQgXATgkAAgAgPAgIADAAQAZAAAAggQAAgfgZABIgDAAg");
	this.shape_37.setTransform(302.475,10.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AgaBJIAAiRIA0AAIAACRg");
	this.shape_38.setTransform(291.85,10.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgyA6QgRgRAAgbIAAhYIA1AAIAABZQAAAQAOAAQAOAAAAgQIAAhZIA2AAIAABYQAAAbgSARQgSARggAAQggAAgSgRg");
	this.shape_39.setTransform(281.175,10.55);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_40.setTransform(268.825,10.4);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_41.setTransform(258.325,10.4);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("Ag1A2QgWgVAAghQABggAVgVQAXgWAiAAQAnAAAYAXIgeAgQgMgKgQAAQgLAAgJAIQgHAJgBANQABAOAHAJQAJAJAMAAQAMAAAGgEIAAgGIgQAAIAAgiIBAAAIAABAQgcAYgrAAQgkAAgWgWg");
	this.shape_42.setTransform(245.15,10.425);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgZBJIAAhoIgdAAIAAgpIBtAAIAAApIgcAAIAABog");
	this.shape_43.setTransform(227.55,10.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_44.setTransform(216.525,10.4);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AAPBJIAAg0IgeAAIAAA0Ig1AAIAAiRIA1AAIAAA1IAeAAIAAg1IA2AAIAACRg");
	this.shape_45.setTransform(203.825,10.4);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AAmBJIAAhHIgPA8IguAAIgOg8IAABHIg0AAIAAiRIBDAAIAWBSIAXhSIBDAAIAACRg");
	this.shape_46.setTransform(182.675,10.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("Ag7A2QgWgVABghQgBggAWgVQAXgWAkAAQAlAAAWAWQAXAVAAAgQAAAhgXAVQgWAWglAAQgkAAgXgWgAgSgWQgGAIAAAOQAAAOAGAJQAHAJALAAQAMAAAHgJQAHgJgBgOQABgOgHgIQgHgKgMAAQgLAAgHAKg");
	this.shape_47.setTransform(164.8,10.425);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgnA/QgNgLgFgOIAmgVQAJAQAKAAQAEAAAAgEQAAgEgIgFIgUgJQgcgOAAgaQAAgVAPgNQAPgMAYAAQAWAAARAMQANAKAEAMIglAWQgHgOgKAAQgEAAAAAEQAAAEAJAFIAUAKQAdAOAAAZQAAAVgQANQgQANgaAAQgWAAgRgNg");
	this.shape_48.setTransform(146.375,10.425);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AguBJIAAiRIA0AAIAABoIApAAIAAApg");
	this.shape_49.setTransform(135.425,10.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_50.setTransform(124.925,10.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AAKBJIgZg8IAAA8Ig1AAIAAiRIA1AAIAAA1IAZg1IA5AAIgkBGIAmBLg");
	this.shape_51.setTransform(112.6,10.4);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_52.setTransform(97.525,10.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AgZBJIAAiRIA0AAIAACRg");
	this.shape_53.setTransform(86.7,10.4);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#000000").s().p("AASBJIgShLIgRBLIg1AAIgmiRIA4AAIAOBQIAShQIApAAIASBQIAOhQIA5AAIgmCRg");
	this.shape_54.setTransform(72.3,10.4);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#000000").s().p("AgtBJIAAiRIBbAAIAAApIgnAAIAAAMIAjAAIAAAmIgjAAIAAANIAnAAIAAApg");
	this.shape_55.setTransform(51.975,10.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("Ag+BJIAAgbIA3hOIg2AAIAAgoIB7AAIAAAbIg4BNIA5AAIAAApg");
	this.shape_56.setTransform(40.225,10.4);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AAPBJIgfhEIAABEIg0AAIAAiRIA2AAIAeBFIAAhFIA1AAIAACRg");
	this.shape_57.setTransform(26.125,10.4);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("Ag7A2QgVgVAAghQAAggAVgVQAXgWAkAAQAlAAAXAWQAVAVAAAgQAAAhgVAVQgXAWglAAQgkAAgXgWgAgSgWQgGAIgBAOQABAOAGAJQAHAJALAAQAMAAAHgJQAGgJABgOQgBgOgGgIQgHgKgMAAQgLAAgHAKg");
	this.shape_58.setTransform(10.3,10.425);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,473,26);


(lib.endframe_static_line1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAQBNIgghIIAABIIg4AAIAAiZIA5AAIAgBJIAAhJIA4AAIAACZg");
	this.shape.setTransform(424.775,12.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAYBNIgHgYIgjAAIgHAYIg3AAIA2iZIA2AAIA1CZgAAHAPIgHggIgJAgIAQAAg");
	this.shape_1.setTransform(408.675,12.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgbBNIg4iZIA9AAIAXBZIAYhZIA7AAIg3CZg");
	this.shape_2.setTransform(391.95,12.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAQBNIgghIIAABIIg4AAIAAiZIA5AAIAgBJIAAhJIA4AAIAACZg");
	this.shape_3.setTransform(371.325,12.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgvBhIAAiZIBfAAIAAArIgoAAIAAANIAkAAIAAAoIgkAAIAAAOIAoAAIAAArgAgYg+IAQgiIAyAAIgZAig");
	this.shape_4.setTransform(357.95,10.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgvBhIAAiZIBfAAIAAArIgoAAIAAANIAkAAIAAAoIgkAAIAAAOIAoAAIAAArgAgYg+IAQgiIAyAAIgaAig");
	this.shape_5.setTransform(347.05,10.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E31E26").s().p("AgpBCQgNgLgGgPIAogWQAKAQAKAAQAEAAAAgDQAAgEgJgFIgUgKQgegPAAgbQAAgWAQgNQAQgOAZAAQAXAAASAOQAOAKAEAMIgoAXQgGgOgKAAQgFAAAAAEQAAAEAJAGIAVAKQAeAPAAAZQAAAXgQAOQgQANgcAAQgYAAgRgOg");
	this.shape_6.setTransform(331.075,12.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E31E26").s().p("Ag4A5QgWgXAAgiQAAghAXgWQAXgYAkAAQApAAAZAZIggAiQgMgLgQAAQgMAAgJAJQgJAIAAAOQAAAPAJAJQAIAKANAAQAMAAAHgFIAAgFIgRAAIAAglIBDAAIAABDQgdAaguAAQglAAgXgXg");
	this.shape_7.setTransform(316.425,12.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E31E26").s().p("AAQBNIgghIIAABIIg4AAIAAiZIA5AAIAgBJIAAhJIA4AAIAACZg");
	this.shape_8.setTransform(300.475,12.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E31E26").s().p("AAYBNIgHgYIgjAAIgHAYIg3AAIA2iZIA2AAIA1CZgAAHAPIgHggIgJAgIAQAAg");
	this.shape_9.setTransform(284.375,12.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E31E26").s().p("AgwBNIAAiZIA3AAIAABuIAqAAIAAArg");
	this.shape_10.setTransform(271,12.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E31E26").s().p("AAoBNIAAhKIgPA+IgxAAIgPg+IAABKIg2AAIAAiZIBGAAIAXBWIAYhWIBGAAIAACZg");
	this.shape_11.setTransform(251.175,12.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E31E26").s().p("Ag9A4QgXgWAAgiQAAghAXgWQAYgYAlAAQAnAAAXAYQAXAWAAAhQAAAigXAWQgXAYgnAAQgmAAgXgYgAgTgYQgHAKAAAOQAAAPAHAKQAHAJAMAAQANAAAHgJQAHgKAAgPQAAgOgHgKQgHgJgNAAQgMAAgHAJg");
	this.shape_12.setTransform(232.425,12.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E31E26").s().p("AAKBNIgag/IAAA/Ig3AAIAAiZIA3AAIAAA4IAag4IA9AAIgnBKIApBPg");
	this.shape_13.setTransform(216.3,12.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgzBUIAAinIBnAAIAAAvIgrAAIAAAUIAnAAIAAArIgnAAIAAA5g");
	this.shape_14.setTransform(198.175,11.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AhDA+QgZgZAAglQAAgkAZgZQAagZApAAQAqAAAaAZQAZAZAAAkQAAAlgZAZQgaAZgqAAQgpAAgagZgAgVgaQgHAKAAAQQAAARAHAJQAIALANAAQAOAAAIgLQAHgJAAgRQAAgQgHgKQgIgKgOAAQgNAAgIAKg");
	this.shape_15.setTransform(182.775,11.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("Ag0BUIAAinIBpAAIAAAvIgsAAIAAAOIAmAAIAAAsIgmAAIAAAPIAsAAIAAAvg");
	this.shape_16.setTransform(162.75,11.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AASBUIgkhPIAABPIg8AAIAAinIA+AAIAjBPIAAhPIA8AAIAACng");
	this.shape_17.setTransform(148.175,11.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgeBUIAAinIA9AAIAACng");
	this.shape_18.setTransform(135.8,11.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("Ag1BUIAAinIA8AAIAAB4IAvAAIAAAvg");
	this.shape_19.setTransform(126.425,11.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AASBUIgkhPIAABPIg8AAIAAinIA+AAIAjBPIAAhPIA8AAIAACng");
	this.shape_20.setTransform(111.725,11.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AhDA+QgZgZAAglQAAgkAZgZQAagZApAAQAqAAAaAZQAZAZAAAkQAAAlgZAZQgaAZgqAAQgpAAgagZgAgVgaQgHAKAAAQQAAARAHAJQAIALANAAQAOAAAIgLQAHgJAAgRQAAgQgHgKQgIgKgOAAQgNAAgIAKg");
	this.shape_21.setTransform(93.625,11.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("Ag1BUIAAinIA8AAIAAB4IAvAAIAAAvg");
	this.shape_22.setTransform(73.925,11.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("Ag0BUIAAinIBoAAIAAAvIgsAAIAAAOIAoAAIAAAsIgoAAIAAAPIAsAAIAAAvg");
	this.shape_23.setTransform(61.9,11.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgdBUIAAh4IgiAAIAAgvIB+AAIAAAvIggAAIAAB4g");
	this.shape_24.setTransform(49.3,11.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgtBIQgOgMgGgRIArgXQALARALABQAEgBAAgEQAAgEgJgGIgWgKQghgQAAgdQAAgZARgPQASgOAbAAQAaAAATAOQAPALAEAOIgqAZQgIgQgLAAQgFABAAAEQAAAFAKAGIAXAKQAhARAAAcQAAAZgSAOQgSAPgeAAQgaAAgTgPg");
	this.shape_25.setTransform(35.675,11.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgzBUIAAinIBnAAIAAAvIgsAAIAAAOIAoAAIAAAsIgoAAIAAAPIAsAAIAAAvg");
	this.shape_26.setTransform(22.85,11.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AhEBUIAAinIBIAAQAcAAARANQARANAAAVQAAAWgRALQAJAFAGAKQAFAJAAALQAAAZgRANQgQAOgcAAgAgKAnIAKAAQAFAAADgEQADgCAAgGQAAgEgDgDQgDgEgFAAIgKAAgAgKgUIAKAAQAKAAAAgJQAAgJgKAAIgKAAg");
	this.shape_27.setTransform(9.625,11.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_static_line1, new cjs.Rectangle(0,0,532,28), null);


(lib.emptyMovieClip = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.dollar_text_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgRAxIAAhhIAjAAIAABhg");
	this.shape.setTransform(328.15,7.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_1.setTransform(320.925,7.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AggAmQgMgKAAgTIAAg6IAkAAIAAA7QAAAKAIAAQAKAAAAgKIAAg7IAjAAIAAA6QAAATgLAKQgMAMgWAAQgUAAgMgMg");
	this.shape_2.setTransform(310.975,7.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgaAvIAEgbIAHABQAHAAAAgFIAAhAIAiAAIAABFQABAcghAAQgJAAgLgCg");
	this.shape_3.setTransform(303,7.65);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgkAyIAAgMQAAgKAHgJQAEgGAJgIIAOgLQAEgFAAgFQAAgFgCAAQgFAAgCAIIgegGQADgMAHgJQALgJAQAAQAQAAALAIQALAIgBAQQAAAPgQAMIgLAIQgFADAAACIAhAAIAAAbg");
	this.shape_4.setTransform(293.45,7.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgjAkQgOgOAAgWQAAgUAOgPQAPgPAWAAQAaAAAQAQIgTAVQgJgGgJAAQgIgBgGAGQgFAGAAAIQgBAJAGAHQAFAFAJAAQAHAAAEgDIAAgDIgLAAIAAgXIAsAAIAAArQgTAQgeAAQgWAAgPgPg");
	this.shape_5.setTransform(281.4,7.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAPAxIgFgPIgVAAIgFAPIgjAAIAihhIAiAAIAjBhgAAFAJIgFgTIgGATIALAAg");
	this.shape_6.setTransform(271.05,7.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgsAxIAAhhIAjAAQAYAAAPANQAPANAAAWQAAAXgPANQgPANgYAAgAgKAVIACAAQARAAgBgVQABgUgRAAIgCAAg");
	this.shape_7.setTransform(261.25,7.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_8.setTransform(251.325,7.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgnAkQgOgOAAgWQAAgVAOgOQAQgPAXAAQAZAAAPAPQAOAOAAAVQAAAWgOAOQgPAPgZAAQgYAAgPgPgAgMgPQgEAHAAAIQAAAJAEAHQAFAFAHAAQAIAAAFgFQAEgHAAgJQAAgIgEgHQgFgFgIAAQgHAAgFAFg");
	this.shape_9.setTransform(240.775,7.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgpAxIAAgTIAlgyIgkAAIAAgcIBRAAIAAASIglAzIAmAAIAAAcg");
	this.shape_10.setTransform(230.925,7.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAZAxIAAgvIgJAoIgeAAIgKgoIAAAvIgjAAIAAhhIAtAAIAOA3IAPg3IAtAAIAABhg");
	this.shape_11.setTransform(217.45,7.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgiA8IAph3IAcAAIgpB3g");
	this.shape_12.setTransform(207.65,8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgQAxIAAhFIgUAAIAAgcIBJAAIAAAcIgTAAIAABFg");
	this.shape_13.setTransform(200.35,7.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgRAxIAAhhIAjAAIAABhg");
	this.shape_14.setTransform(191.6,7.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_15.setTransform(185.925,7.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAZAxIAAgvIgJAoIgeAAIgKgoIAAAvIgjAAIAAhhIAtAAIAOA3IAPg3IAtAAIAABhg");
	this.shape_16.setTransform(176.05,7.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgBAxIAAgUIgoAAIAAgUIAkg5IAlAAIAAAzIAKAAIAAAaIgKAAIAAAUgAgLADIAKAAIAAgRg");
	this.shape_17.setTransform(162.575,7.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgkAyIAAgMQAAgKAHgJQAEgGAJgIIAOgLQAEgFABgFQgBgFgDAAQgEAAgCAIIgegGQADgMAHgJQALgJAQAAQAQAAAKAIQALAIAAAQQAAAPgQAMIgLAIQgFADAAACIAhAAIAAAbg");
	this.shape_18.setTransform(154.15,7.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgjAkQgOgOAAgWQAAgUAOgPQAPgPAXAAQAaAAAPAQIgUAVQgHgGgLAAQgHgBgFAGQgHAGABAIQAAAJAFAHQAFAFAIAAQAIAAAEgDIAAgDIgLAAIAAgXIArAAIAAArQgSAQgdAAQgYAAgOgPg");
	this.shape_19.setTransform(142.1,7.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AAPAxIgEgPIgWAAIgEAPIgjAAIAhhhIAiAAIAiBhgAAFAJIgFgTIgFATIAKAAg");
	this.shape_20.setTransform(131.75,7.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgsAxIAAhhIAjAAQAYAAAPANQAPANAAAWQAAAXgPANQgPANgYAAgAgKAVIADAAQAPAAAAgVQAAgUgPAAIgDAAg");
	this.shape_21.setTransform(121.95,7.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgaAvIAEgbIAHABQAHAAAAgFIAAhAIAiAAIAABFQABAcghAAQgJAAgLgCg");
	this.shape_22.setTransform(114,7.65);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgRAxIAAhhIAjAAIAABhg");
	this.shape_23.setTransform(109.05,7.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAFAxIgNgfIAAAfIgiAAIAAhhIAqAAQARAAAKAJQAKAJAAAOQgBAIgEAHQgEAFgHAEIAXApgAgIgHIAEAAQADgBACgCQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBQAAAAAAgBQAAgBAAAAQAAgBgBAAQAAgBAAAAQgCgDgDAAIgEAAg");
	this.shape_24.setTransform(102.8,7.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgRAxIgkhhIAnAAIAOA5IAQg5IAmAAIgkBhg");
	this.shape_25.setTransform(92.65,7.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAKAxIgUguIAAAuIgjAAIAAhhIAkAAIAUAuIAAguIAjAAIAABhg");
	this.shape_26.setTransform(79.475,7.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AAPAxIgFgPIgVAAIgFAPIgjAAIAihhIAjAAIAiBhgAAEAJIgEgTIgGATIAKAAg");
	this.shape_27.setTransform(69.25,7.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgRAxIgkhhIAnAAIAOA5IAQg5IAlAAIgjBhg");
	this.shape_28.setTransform(58.65,7.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAXAAQAaAAAPAQIgUAVQgHgGgLAAQgHgBgFAGQgHAGAAAIQABAJAFAHQAGAFAHAAQAIAAAFgDIAAgDIgLAAIAAgXIAqAAIAAArQgSAQgdAAQgYAAgOgPg");
	this.shape_29.setTransform(45.2,7.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgRAxIAAhhIAjAAIAABhg");
	this.shape_30.setTransform(37.9,7.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgsAxIAAhhIAjAAQAYAAAPANQAPANAAAWQAAAXgPANQgPANgYAAgAgJAVIABAAQAQAAABgVQgBgUgQAAIgBAAg");
	this.shape_31.setTransform(31.1,7.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgeAxIAAhhIAjAAIAABFIAaAAIAAAcg");
	this.shape_32.setTransform(22.925,7.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgeAxIAAhhIA9AAIAAAcIgaAAIAAAHIAXAAIAAAZIgXAAIAAAJIAaAAIAAAcg");
	this.shape_33.setTransform(15.925,7.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgjAkQgPgOAAgWQAAgUAPgPQAPgPAWAAQAaAAAQAQIgTAVQgJgGgJAAQgIgBgGAGQgFAGgBAIQAAAJAGAHQAFAFAJAAQAHAAAFgDIAAgDIgLAAIAAgXIArAAIAAArQgTAQgeAAQgWAAgPgPg");
	this.shape_34.setTransform(7.15,7.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_3, new cjs.Rectangle(0,0,332.5,18), null);


(lib.dollar_text_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,0,0);


(lib.dollar_text_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AhmBwIAAjfIBQAAQA5AAAhAeQAiAeAAAzQABAzgjAeQghAfg5AAgAgXAwIAFAAQAmAAAAgwQAAgvgmAAIgFAAg");
	this.shape.setTransform(516,14.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E90000").s().p("AhHBwIAAjfIBRAAIAACgIA+AAIAAA/g");
	this.shape_1.setTransform(497.3,14.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E90000").s().p("AgiBgQgegTgMghIgXAAIAAgnIAPAAIAAgIIgPAAIAAgoIAYAAQANgfAcgUQAfgVAkAAQAnAAAaAQIgaA1QgQgJgSAAQgSAAgPAMIA0AAIgEAoIg8AAIgBAEIABAEIA7AAIgEAnIgpAAQALALAWAAQAUAAAPgLIAZA1QgWATgoAAQgpAAgfgUg");
	this.shape_2.setTransform(477.825,14.775);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E90000").s().p("AhSBTQggghAAgyQAAgxAiggQAhgiA0AAQA9AAAjAkIgtAxQgSgQgYAAQgRAAgNANQgNANAAAUQAAAWAMANQAMANAUAAQASAAAJgFIAAgJIgZAAIAAg1IBiAAIAABjQgqAkhDAAQg2AAgighg");
	this.shape_3.setTransform(455.05,14.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E90000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA1AAIAAA6Ig1AAIAAAUIA7AAIAAA/g");
	this.shape_4.setTransform(435.35,14.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E90000").s().p("AgoBwIAAjfIBRAAIAADfg");
	this.shape_5.setTransform(422.425,14.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E90000").s().p("AgoBwIAAigIgsAAIAAg/ICpAAIAAA/IgsAAIAACgg");
	this.shape_6.setTransform(408.5,14.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E90000").s().p("AAXBwIgvhpIAABpIhRAAIAAjfIBTAAIAvBpIAAhpIBRAAIAADfg");
	this.shape_7.setTransform(388.075,14.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E90000").s().p("AAjBwIgKgiIg0AAIgKAiIhQAAIBOjfIBPAAIBNDfgAAKAVIgLguIgMAuIAXAAg");
	this.shape_8.setTransform(364.65,14.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E90000").s().p("AAPBwIgmhcIAABcIhRAAIAAjfIBRAAIAABSIAlhSIBZAAIg3BrIA5B0g");
	this.shape_9.setTransform(341.875,14.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E90000").s().p("AAiBwIgKgiIgzAAIgKAiIhQAAIBOjfIBPAAIBNDfgAALAVIgMguIgMAuIAYAAg");
	this.shape_10.setTransform(318.5,14.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#E90000").s().p("AgoBwIhRjfIBYAAIAjCCIAjiCIBVAAIhRDfg");
	this.shape_11.setTransform(294.175,14.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA0AAIAAA6Ig0AAIAAAUIA7AAIAAA/g");
	this.shape_12.setTransform(267.65,14.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E90000").s().p("Ag7BrIAHg9QAJACAJAAQAOAAAAgMIAAiUIBRAAIAACgQAABBhLAAQgXAAgWgGg");
	this.shape_13.setTransform(252.7,14.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgoBwIAAigIgrAAIAAg/ICnAAIAAA/IgsAAIAACgg");
	this.shape_14.setTransform(231.25,14.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgoBwIAAjfIBRAAIAADfg");
	this.shape_15.setTransform(217.325,14.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AhMBZQgbgaABgpIAAiHIBRAAIAACJQAAAXAVAAQAWAAAAgXIAAiJIBSAAIAACHQAAApgbAaQgcAZgxAAQgwAAgcgZg");
	this.shape_16.setTransform(201.05,15);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("Ag8BgQgTgQgIgWIA6gfQAOAYAPAAQAGAAAAgGQAAgGgNgIIgegOQgrgWAAgnQAAggAXgUQAXgTAlAAQAiAAAZATQAVAQAFASIg5AhQgJgVgPAAQgHAAAAAGQAAAGAOAHIAeAPQAsAXAAAlQAAAhgYAUQgYATgoAAQgiAAgagUg");
	this.shape_17.setTransform(174.325,14.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AhFBwIAAjfICLAAIAAA/Ig7AAIAAATIA0AAIAAA6Ig0AAIAAAUIA7AAIAAA/g");
	this.shape_18.setTransform(157.2,14.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AhGBwIAAjfIBQAAIAACgIA9AAIAAA/g");
	this.shape_19.setTransform(141.75,14.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AhHBwIAAjfIBRAAIAACgIA9AAIAAA/g");
	this.shape_20.setTransform(126.15,14.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAjBwIgKgiIg0AAIgKAiIhPAAIBNjfIBPAAIBODfgAAKAVIgLguIgNAuIAYAAg");
	this.shape_21.setTransform(106.15,14.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AhHBwIAAjfIBRAAIAACgIA+AAIAAA/g");
	this.shape_22.setTransform(80.6,14.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AAjBwIgKgiIg0AAIgKAiIhPAAIBNjfIBPAAIBODfgAAKAVIgLguIgNAuIAYAAg");
	this.shape_23.setTransform(60.6,14.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAiBwIgKgiIgzAAIgKAiIhQAAIBOjfIBPAAIBNDfgAALAVIgMguIgMAuIAYAAg");
	this.shape_24.setTransform(36.8,14.8);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AAYBwIAAhRIgvAAIAABRIhRAAIAAjfIBRAAIAABQIAvAAIAAhQIBRAAIAADfg");
	this.shape_25.setTransform(13.45,14.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_text_1, new cjs.Rectangle(0,0,528.7,36), null);


(lib.dollar_prijs = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgPAJIAAgRIAfAAIAAARg");
	this.shape.setTransform(27,28.225);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgHAIQgDgDgBgFQABgEADgDQADgDAEAAQAFAAADADQAEADAAAEQAAAFgEADQgDADgFAAQgEAAgDgDg");
	this.shape_1.setTransform(23.3,30.75);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AADAoIAAgSIgiAAIAAgOIAfgvIAXAAIAAAtIAJAAIAAAQIgJAAIAAASgAgLAGIAOAAIAAgYg");
	this.shape_2.setTransform(18.375,27.725);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AADAoIAAgSIgiAAIAAgOIAfgvIAXAAIAAAtIAJAAIAAAQIgJAAIAAASgAgLAGIAOAAIAAgYg");
	this.shape_3.setTransform(11.475,27.725);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgSAoIAYg+IghAAIAAgRIA2AAIAAAOIgZBBg");
	this.shape_4.setTransform(5,27.725);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgHAbQgEgCAAgGQAAgEAEgDQADgEAEAAQAEAAAEAEQADADABAEQgBAGgDACQgEAEgEAAQgEAAgDgEgAgHgKQgEgEAAgFQAAgEAEgEQADgDAEAAQAEAAAEADQADAEABAEQgBAFgDAEQgEADgEAAQgEAAgDgDg");
	this.shape_5.setTransform(51.85,18.25);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AAHAqIgQgdIAAAdIgTAAIAAhTIATAAIAAAvIAPgYIAWAAIgUAbIAVAhg");
	this.shape_6.setTransform(47.425,17.05);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgPAXQgJgJAAgOQAAgNAJgJQAJgJANAAQAKAAAIAGIgFAQQgFgEgHAAQgHAAgCAEQgEAEAAAFQAAAHAEAEQADAEAHAAQAGAAAFgEIAFAPQgIAGgKAAQgNAAgJgJg");
	this.shape_7.setTransform(41.225,18.175);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgUAbQgGgGAAgKQAAgJAGgEQAHgFAKAAQAGAAAFACIAAgCQAAgHgIAAQgIAAgHAEIgIgNQALgIANAAQAOAAAHAHQAFAGAAAMIAAAkIgRAAIgBgDQgFAFgIAAQgJAAgHgFgAgFAGQgCACAAADQAAADACABQACACADAAQAIAAAAgGQAAgBAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQgDgCgDAAQgDAAgCACg");
	this.shape_8.setTransform(35.3,18.175);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgIAoIgFgEIAAAFIgSAAIAAhSIATAAIAAAbIAFgEQAGgCAFAAQANAAAHAJQAIAIAAAOQAAAOgIAIQgJAJgNAAQgEAAgGgCgAgJAAQgDAEAAAHQgBAGAEAEQAEAEAFAAQAFAAAEgEQAEgEAAgGQAAgHgEgEQgEgDgFAAQgFAAgEADg");
	this.shape_9.setTransform(28.95,17.125);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AAIAqIAAgkQAAgIgIAAQgDAAgCACQgCACgBAEIAAAkIgSAAIAAhTIASAAIAAAeQAFgIAKAAQAMAAAFAJQADAHABALIAAAig");
	this.shape_10.setTransform(22,17.05);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgOAcQgGgCgDgFIALgKQAGAFAGAAQAFAAABgDQgBgCgEgCIgIgDQgOgGAAgMQAAgJAHgFQAGgFAJAAQAOAAAIALIgNAKQgEgFgGAAQgDAAAAADQAAACADACIAKADQAOAFAAAMQAAAJgHAFQgHAGgKAAQgIAAgGgEg");
	this.shape_11.setTransform(16.15,18.175);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgUAbQgGgGAAgKQAAgJAGgEQAGgFALAAQAGAAAFACIAAgCQAAgHgJAAQgHAAgHAEIgIgNQAMgIAMAAQAOAAAHAHQAFAGAAAMIAAAkIgRAAIgBgDQgFAFgIAAQgJAAgHgFgAgFAGQgCACAAADQAAADACABQADACACAAQAIAAAAgGQAAgBAAgBQAAAAAAgBQgBAAAAgBQAAAAgBgBQgDgCgDAAQgCAAgDACg");
	this.shape_12.setTransform(10.45,18.175);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgPAXQgJgJAAgOQAAgNAJgJQAJgJANAAQAKAAAIAGIgFAQQgFgEgHAAQgHAAgCAEQgEAEAAAFQAAAHAEAEQADAEAHAAQAGAAAFgEIAFAPQgIAGgKAAQgNAAgJgJg");
	this.shape_13.setTransform(4.775,18.175);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgUAbQgGgGAAgKQAAgJAGgEQAHgFAKAAQAGAAAFACIAAgCQAAgHgIAAQgIAAgHAEIgIgNQALgIANAAQAOAAAHAHQAFAGAAAMIAAAkIgRAAIgBgDQgFAFgIAAQgJAAgHgFgAgFAGQgCACAAADQAAADACABQACACADAAQAIAAAAgGQAAgBAAgBQAAAAgBgBQAAAAAAgBQAAAAgBgBQgDgCgDAAQgDAAgCACg");
	this.shape_14.setTransform(43.4,7.625);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AAIAfIAAgjQAAgIgIAAQgDgBgCADQgCACAAAEIAAAjIgTAAIAAg8IATAAIAAAHQAFgIAJAAQAMAAAFAKQADAGAAAMIAAAhg");
	this.shape_15.setTransform(37.225,7.55);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgOAcQgGgCgDgFIALgKQAGAFAGAAQAGAAgBgDQAAgCgDgCIgJgDQgOgGAAgMQAAgJAHgFQAGgFAKAAQAOAAAHALIgMAKQgFgFgGAAQgDAAAAADQAAACADACIAKADQAOAFAAAMQAAAJgHAFQgHAGgKAAQgHAAgHgEg");
	this.shape_16.setTransform(29.15,7.625);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgOA1IAAgQIACAAQAIAAAAgGIAAg7IASAAIAAA7QAAAXgYAAIgEgBgAgCgiQgDgDAAgFQAAgFADgDQACgCAFAAQAEAAADACQADADAAAFQAAAFgDADQgDACgEAAQgFAAgCgCg");
	this.shape_17.setTransform(24.65,7.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgJAqIAAg7IASAAIAAA7gAgHgXQgDgDAAgFQAAgFADgDQADgCAEAAQAEAAADACQAEADAAAFQAAAFgEADQgDACgEAAQgEAAgDgCg");
	this.shape_18.setTransform(22.125,6.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgRAfIAAg8IASAAIAAAJQAFgKALAAIAAATIgDAAQgNABAAAPIAAAag");
	this.shape_19.setTransform(18.7,7.55);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgfAqIAAhRIASAAIAAAFQACgDADgBQAFgDAFAAQANAAAJAJQAIAJAAAOQAAANgIAJQgHAJgOAAQgFAAgFgDIgFgDIAAAagAgJgTQgEAEAAAGQABAHAEADQADAEAFAAQAFAAAEgEQAEgDgBgHQABgHgEgEQgEgEgFAAQgFAAgEAFg");
	this.shape_20.setTransform(13,8.625);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgFAVIAAgKIgKAFIgGgLIAJgFIgJgEIAGgLIAKAFIAAgKIALAAIAAAKIAJgFIAGALIgJAEIAKAFIgHALIgJgFIAAAKg");
	this.shape_21.setTransform(4.525,4.725);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_prijs, new cjs.Rectangle(0,0,78.1,36.7), null);


(lib.dollar_cashback_text = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(3));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAEAnIgKgaIAAAaIgbAAIAAhNIAhAAQANAAAIAHQAIAIgBALQAAAGgDAFQgEAFgFACIASAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAAAgBAAIgDAAg");
	this.shape.setTransform(11.05,9.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAMAnIgDgNIgRAAIgEANIgcAAIAbhNIAaAAIAbBNgAADAHIgDgOIgEAOIAHAAg");
	this.shape_1.setTransform(3.25,9.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AALAnIgDgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAADAHIgDgOIgEAOIAHAAg");
	this.shape_2.setTransform(-4.9,9.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgfAnIAAhNIAgAAQAOAAAHAHQAIAFAAAKQAAAKgIAFQAEABADAFQACAFAAAEQABAMgIAGQgIAHgNAAgAgEASIAEAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAgBQABAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQgBgBAAAAIgEAAgAgEgJIAEAAQAFAAgBgDQABgFgFAAIgEAAg");
	this.shape_3.setTransform(-12.3,9.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgRAJIAAgSIAjAAIAAASg");
	this.shape_4.setTransform(25.125,0.35);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbAdQgMgMAAgRQAAgQAMgMQALgLASAAQAUAAANAMIgQARQgGgFgIAAQgGgBgEAFQgFAFABAGQAAAHADAFQAFAFAGgBQAGAAADgBIAAgDIgIAAIAAgSIAhAAIAAAhQgOANgXAAQgSAAgLgLg");
	this.shape_5.setTransform(18.75,-0.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgUAlIADgVIAGABQAFAAAAgEIAAgzIAbAAIAAA3QAAAWgZAAQgIAAgIgCg");
	this.shape_6.setTransform(12.3,-0.075);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgNAnIAAhNIAbAAIAABNg");
	this.shape_7.setTransform(8.4,-0.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAEAnIgKgZIAAAZIgbAAIAAhNIAhAAQANAAAIAIQAHAGAAAMQAAAGgDAFQgDAFgFACIARAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQAAAAAAgBQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_8.setTransform(3.45,-0.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAFAnIgMggIAAAgIgcAAIAAhNIAcAAIAAAcIAMgcIAeAAIgSAlIATAog");
	this.shape_9.setTransform(-4,-0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAEAnIgKgZIAAAZIgbAAIAAhNIAhAAQANAAAIAIQAIAGAAAMQgBAGgDAFQgDAFgGACIASAhgAgGgGIADAAQABAAAAAAQABAAAAAAQABgBAAAAQAAAAAAgBQAAAAABAAQAAgBAAAAQAAgBAAAAQABAAAAgBQAAgBgBAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAAAAAgBQAAAAgBAAQAAAAgBAAQAAgBgBAAIgDAAg");
	this.shape_10.setTransform(-11.15,-0.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgXAnIAAhNIAvAAIAAAWIgVAAIAAAGIASAAIAAAUIgSAAIAAAHIAVAAIAAAWg");
	this.shape_11.setTransform(-17.6,-0.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgNAnIgchNIAeAAIALAsIANgsIAdAAIgcBNg");
	this.shape_12.setTransform(-24.6,-0.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgUAhQgGgFgDgIIAUgLQAEAIAFAAQAAAAABAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgDgCIgLgFQgPgHAAgNQAAgMAIgGQAIgHAMAAQAMAAAIAHQAIAFABAGIgTALQgEgGgFgBQAAAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAABAFADIAKAGQAPAHAAAMQAAAMgIAGQgIAHgOAAQgLAAgJgHg");
	this.shape_13.setTransform(19.75,-10.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgeAcQgMgLAAgRQAAgQAMgMQALgLATAAQATAAAMALQAMAMAAAQQAAARgMALQgMAMgTAAQgTAAgLgMgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQAEgFAAgHQAAgGgEgFQgEgFgGAAQgGAAgDAFg");
	this.shape_14.setTransform(12.3,-10.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgXAnIAAhNIAbAAIAAA3IAUAAIAAAWg");
	this.shape_15.setTransform(5.4,-10.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAFAnIgMggIAAAgIgcAAIAAhNIAcAAIAAAcIAMgcIAfAAIgTAlIATAog");
	this.shape_16.setTransform(-3.2,-10.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgeAcQgMgLABgRQgBgQAMgMQALgLATAAQATAAAMALQALAMAAAQQAAARgLALQgMAMgTAAQgTAAgLgMgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQADgFABgHQgBgGgDgFQgEgFgGAAQgGAAgDAFg");
	this.shape_17.setTransform(-11.45,-10.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgeAcQgMgLAAgRQAAgQAMgMQALgLATAAQATAAAMALQAMAMAAAQQAAARgMALQgMAMgTAAQgTAAgLgMgAgJgLQgEAFABAGQgBAHAEAFQADAFAGAAQAGAAAEgFQAEgFAAgHQAAgGgEgFQgEgFgGAAQgGAAgDAFg");
	this.shape_18.setTransform(-20.1,-10.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgRAKIAAgTIAjAAIAAATg");
	this.shape_19.setTransform(12.225,4.95);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgJAJQgEgEAAgFQAAgEAEgEQAEgEAFAAQAGAAAEAEQAEAEAAAEQAAAFgEAEQgEAEgGAAQgFAAgEgEg");
	this.shape_20.setTransform(8.525,7.225);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgeAAQAAgnAeAAQAfAAAAAnQAAAogfAAQgeAAAAgogAgDAAQAAASADAAQAEAAAAgSQAAgRgEAAQgDAAAAARg");
	this.shape_21.setTransform(3.775,4.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgeAAQAAgnAeAAQAfAAAAAnQAAAogfAAQgeAAAAgogAgDAAQAAASADAAQAEAAAAgSQAAgRgEAAQgDAAAAARg");
	this.shape_22.setTransform(-2.825,4.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgVAgQgGgGgCgJIAXgFQABAFAEAAQAEAAAAgFQAAgBAAAAQAAgBAAAAQAAgBgBAAQAAgBAAAAQgBgBAAAAQAAAAgBAAQAAgBAAAAQAAAAgBAAQgDAAgBADIgWgDIACgsIAyAAIAAAVIgcAAIAAAGIAGgBQAMAAAHAGQAIAHAAALQAAALgIAIQgJAIgNAAQgMAAgJgHg");
	this.shape_23.setTransform(-9.275,4.575);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgVAdQgMgLAAgSQAAgQALgMQAMgLASAAQAPAAAKAIIgIAVQgGgFgIAAQgHgBgEAFQgEAFAAAGQAAAHAEAFQAEAFAHgBQAIAAAHgGIAIAWQgLAIgPAAQgSAAgLgLg");
	this.shape_24.setTransform(19.325,-5.5);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAMAnIgDgNIgRAAIgEANIgcAAIAbhNIAaAAIAbBNgAADAHIgDgPIgEAPIAHAAg");
	this.shape_25.setTransform(11.8,-5.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgeAnIAAhNIAgAAQAMAAAJAHQAHAFAAAKQAAAKgIAFQAFACACAEQACAFABAEQgBAMgHAGQgIAGgMABgAgEASIAEAAQABAAAAAAQAAAAABAAQAAgBABAAQAAAAABgBQAAAAAAAAQAAgBABAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQgBAAAAgBQgBAAAAAAQgBAAAAAAQAAgBgBAAIgEAAgAgEgJIAEAAQAEAAABgDQgBgFgEAAIgEAAg");
	this.shape_26.setTransform(4.4,-5.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAIAnIAAgcIgPAAIAAAcIgcAAIAAhNIAcAAIAAAcIAPAAIAAgcIAcAAIAABNg");
	this.shape_27.setTransform(-3,-5.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgUAhQgGgFgDgIIATgLQAFAJAFgBQABAAAAAAQAAAAABAAQAAAAAAgBQAAAAAAgBQAAgCgEgCIgKgFQgPgHAAgNQAAgMAIgGQAIgHAMAAQAMAAAJAHQAHAFACAGIgUALQgDgGgFgBQgBAAgBABQAAAAAAAAQgBAAAAABQAAAAAAABQAAABAFADIAKAGQAPAGAAANQAAALgIAHQgIAHgOAAQgLAAgJgHg");
	this.shape_28.setTransform(-10.1,-5.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAMAnIgEgNIgRAAIgDANIgbAAIAahNIAbAAIAbBNgAAEAHIgEgPIgEAPIAIAAg");
	this.shape_29.setTransform(-17.3,-5.5);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgVAdQgMgLAAgSQAAgQALgMQAMgLASAAQAPAAAKAIIgIAVQgGgFgIAAQgHgBgEAFQgEAFAAAGQAAAHAEAFQAEAFAHgBQAIAAAHgGIAIAWQgLAIgPAAQgSAAgLgLg");
	this.shape_30.setTransform(-24.975,-5.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgOAIIAAgPIAdAAIAAAPg");
	this.shape_31.setTransform(21.525,11.675);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_32.setTransform(18.525,13.525);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgVAcIAHgQQAFADADAAQAGAAABgHQgCACgFAAQgIAAgEgEQgGgGAAgJQAAgJAHgHQAHgHAKAAQAZABAAAdQAAANgGAKQgHAMgQAAQgKAAgHgFgAgCgMIgBADIABAEQAAAAABABQAAAAAAAAQABAAAAAAQAAABAAAAIACgCQABgJgDAAQAAAAgBAAQAAAAAAAAQgBABAAAAQAAAAAAABg");
	this.shape_33.setTransform(14.7,11.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgRAbQgHgGAAgKQAAgIAGgEQgFgFAAgGQAAgJAHgFQAGgGAKAAQALAAAHAGQAGAFAAAJQAAAGgFAFQAGAEAAAIQAAAKgHAGQgHAGgLAAQgKAAgHgGgAgCAIIAAADQAAAFACAAQAEAAgBgFIAAgDQAAgBgBAAQAAAAAAgBQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAAAgBAAQAAABAAAAQgBAAAAABgAgCgLQAAAEACAAQADAAAAgEQAAgEgDgBQgCABAAAEg");
	this.shape_34.setTransform(9.4,11.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_35.setTransform(3.875,13.525);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgKAgIgYg+IAZAAIAJAkIAKgkIAZAAIgYA+g");
	this.shape_36.setTransform(-0.85,11.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_37.setTransform(-5.525,13.525);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AAIAgIgIggIgGAgIgYAAIgQg+IAYAAIAHAhIAHghIARAAIAIAhIAFghIAZAAIgRA+g");
	this.shape_38.setTransform(-11.5,11.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgHAHQgDgDAAgEQAAgDADgDQADgDAEAAQAFAAADADQADADAAADQAAAEgDADQgDADgFAAQgEAAgDgDg");
	this.shape_39.setTransform(-17.425,13.525);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgLAgIAAgtIgMAAIAAgRIAvAAIAAARIgNAAIAAAtg");
	this.shape_40.setTransform(-21.125,11.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_41.setTransform(27.675,3.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_42.setTransform(22.225,3.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgWAYQgKgKAAgOQAAgNAKgJQAKgKANABQARAAAKAKIgMANQgGgEgGAAQgFAAgDADQgDAEgBAFQABAGADAEQADAEAFAAQAFAAADgCIAAgDIgHAAIAAgNIAcAAIAAAbQgMAJgTABQgOgBgKgIg");
	this.shape_43.setTransform(16.6,3.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_44.setTransform(10.075,3.3);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAKAgIgDgKIgOAAIgCAKIgXAAIAWg+IAVAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_45.setTransform(3.55,3.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AAHAgIAAgXIgNAAIAAAXIgWAAIAAg+IAWAAIAAAWIANAAIAAgWIAWAAIAAA+g");
	this.shape_46.setTransform(-3,3.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgYAgIAAg+IAZAAQAMAAAFAFQAHAHAAAJQAAAJgHAGQgGAFgKAAIgEAAIAAAVgAgCgEIACAAIADgBQAAAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBQAAAAAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQgBAAAAgBQAAAAgBAAQAAAAgBAAQAAAAAAAAIgCAAg");
	this.shape_47.setTransform(-8.75,3.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AgZAXQgJgJAAgOQAAgNAJgJQALgKAOABQAQgBAJAKQAKAJAAANQAAAOgKAJQgJAKgQAAQgPAAgKgKgAgHgJQgDAEAAAFQAAAGADAEQADAEAEAAQAFAAADgEQADgEAAgGQAAgFgDgEQgDgEgFAAQgEAAgDAEg");
	this.shape_48.setTransform(-15.05,3.3);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_49.setTransform(-23.525,3.3);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_50.setTransform(-28.975,3.3);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_51.setTransform(24.525,-4.7);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_52.setTransform(19.075,-4.7);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AADAgIgIgVIAAAVIgWAAIAAg+IAbAAQALAAAHAFQAFAGAAAJQAAAFgDAEQgCADgFADIAPAbgAgFgEIADAAQAAAAABgBQAAAAABAAQAAAAAAAAQAAgBABAAIAAgDIAAgDQgBAAAAgBQAAAAAAAAQgBAAAAAAQgBAAAAAAIgDAAg");
	this.shape_53.setTransform(14.25,-4.7);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAnAAIAAARIgRAAIAAAFIAPAAIAAAQIgPAAIAAAGIARAAIAAASg");
	this.shape_54.setTransform(8.975,-4.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAWAAIAAAsIARAAIAAASg");
	this.shape_55.setTransform(4.625,-4.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AgTAgIAAg+IAWAAIAAAsIARAAIAAASg");
	this.shape_56.setTransform(0.225,-4.7);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AAJAgIgCgKIgOAAIgCAKIgXAAIAWg+IAVAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_57.setTransform(-5.35,-4.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FFFFFF").s().p("AgLAgIAAgtIgMAAIAAgRIAvAAIAAARIgNAAIAAAtg");
	this.shape_58.setTransform(-11.175,-4.7);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FFFFFF").s().p("AgQAbQgGgFgCgGIARgJQADAHAEAAQAAAAABAAQAAAAAAAAQAAgBABAAQAAAAAAAAQAAgCgDgCIgJgEQgMgFAAgLQAAgKAHgFQAGgGAKABQAJAAAIAEQAFAFACAFIgQAJQgDgFgEAAQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAABAAAAQAAACAEACIAIAFQANAFAAALQAAAIgHAGQgHAFgLABQgJgBgHgFg");
	this.shape_59.setTransform(-16.275,-4.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FFFFFF").s().p("AAHAgIgNgeIAAAeIgXAAIAAg+IAYAAIAMAdIAAgdIAXAAIAAA+g");
	this.shape_60.setTransform(-22.075,-4.7);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FFFFFF").s().p("AgKAgIAAg+IAVAAIAAA+g");
	this.shape_61.setTransform(-26.7,-4.7);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FFFFFF").s().p("AgQAbQgGgEgCgHIARgJQADAHAEAAQAAAAABAAQAAAAAAAAQABgBAAAAQAAAAAAAAQAAgCgDgCIgJgEQgMgFAAgMQAAgJAHgFQAGgGAKABQAJAAAIAEQAFAFACAFIgQAJQgDgFgEAAQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAABAAAAQAAACAEACIAIAFQANAFAAALQAAAIgHAGQgHAFgLAAQgJAAgHgFg");
	this.shape_62.setTransform(13.225,-12.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FFFFFF").s().p("AgLAgIAAg+IAXAAIAAA+g");
	this.shape_63.setTransform(9.25,-12.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FFFFFF").s().p("AgLAgIAAgtIgMAAIAAgRIAvAAIAAARIgNAAIAAAtg");
	this.shape_64.setTransform(5.375,-12.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FFFFFF").s().p("AAKAgIgDgKIgOAAIgDAKIgWAAIAVg+IAWAAIAWA+gAADAGIgDgMIgDAMIAGAAg");
	this.shape_65.setTransform(-0.45,-12.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FFFFFF").s().p("AAEAgIgIgVIAAAVIgXAAIAAg+IAbAAQALgBAGAHQAHAFgBAJQAAAFgCAEQgDAEgEACIAOAbgAgEgEIACAAQABAAAAgBQAAAAABAAQAAAAAAAAQAAgBAAAAIACgDIgCgDQAAAAAAgBQAAAAAAAAQgBAAAAAAQAAAAgBAAIgCAAg");
	this.shape_66.setTransform(-6.4,-12.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FFFFFF").s().p("AgWAXQgKgJAAgOQAAgNAKgJQAJgJAPAAQAQgBALALIgNANQgFgEgHAAQgFAAgDADQgEAEABAFQgBAGAEAEQAEADAEABQAFgBADgBIAAgDIgHAAIAAgNIAbAAIAAAbQgMAKgSgBQgPABgJgKg");
	this.shape_67.setTransform(-12.85,-12.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16,p:{x:-3.2,y:-10.1}},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_16,p:{x:26.85,y:-5.5}},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19}]},1).to({state:[{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33.3,-18.3,66.3,37);


(lib.dollar_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AkfD2IgigJIg9ldQATAGAPADQBMAQBXgZQA1gOBmgwQBlgvA1gPQBXgYBNAPQARADARAGIA9FdIgigKQhMgQhYAZQg1APhlAwQhmAvg0APQg1APgxAAQggAAgegGgAkkDWQBMAQBXgZQA1gOBlgwQBmgvA1gPQBYgZBMAQIgzkdQhMgQhXAZQg1APhlAvQhmAwg2AOQhXAZhMgQgAg+ByQgmgTgIgwQgIguAaguQAaguAtgVQArgWAnAVQAmATAIAwQAIAugaAuQgaAtgtAWQgWALgVAAQgUAAgTgKgAAHArIhOAkIACANQAAADADABQAEACAEgCQAYgJAugWIBGggQAEgBACgEQACgFgBgEIgCgMQgcAMg0AYgAA6hkIhFAgQgvAWgYAJQgEACgCAEQgCAFAAAEIAQBXQABAEADABQADACAEgCIBGgfIBGghQAEgBADgDQABgEAAgEIgQhZQgBgDgCgCIgEgBIgEABgAg+BYIgBgEIA4gaIABADIAggPIAAgDIA4gZIABAFQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAAAIhHAgIhFAgIgBABQgBAAAAgBQAAAAAAAAQgBAAAAAAQAAgBAAAAgAhDA+IgPhYQAAgBAAAAQAAgBAAAAQAAAAABgBQAAAAAAAAIBBgdIAAAAQABAIAGgEQAHgCgBgIIA/gdQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAAAIAPBZQABABAAAAQAAAAgBABQAAAAAAABQgBAAAAAAIhHAfQgtAWgYAJIgBABIgCgBgAkhAQIgHgBIgCgLIAGABIgBgEIgGAAIgCgLIAJABQACgKAHgGQAIgHANAAQALgBAIAEIgEAQQgFgDgHAAQgLAAgEAIIAVgBIABAKIgWAAIAAAFIAWAAIACALIgSAAQAGAJALgBQAGgBAFgDIAKANQgGAGgMABIgCAAQgaAAgNgZgAElAoQgdgCgLgVIgHAAIgCgKIAFgBIgBgEIgFAAIgCgKIAIgBQABgLAIgGQAJgGALAAQAMAAAJAFIgFAOQgHgDgFAAQgLgBgDAIIAUAAIABAKIgXAAIABAEIAWAAIABAMIgQgBQAFAHALABQAFABAGgDIAKAPQgEAEgKAAIgEgBg");

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AkkDWIgzkdQBMAQBXgZQA2gOBmgwQBlgvA1gPQBXgZBMAQIAzEdQhMgQhYAZQg1APhmAvQhlAwg1AOQg1APgwAAQggAAgegGgAgThwQgtAVgaAuQgaAuAIAuQAIAwAmATQAmAUAsgVQAtgWAagtQAagugIguQgIgwgmgTQgTgKgUAAQgVAAgWALgAkoAPIAHABQAOAaAbgBQAMgBAGgGIgKgNQgFADgGABQgLABgGgJIASAAIgCgLIgWAAIAAgFIAWAAIgBgKIgVABQAEgIALAAQAHAAAFADIAEgQQgIgEgLABQgNAAgIAHQgHAGgCAKIgJgBIACALIAGAAIABAEIgGgBgAElAoQANABAFgEIgKgPQgGADgFgBQgLgBgFgHIAQABIgBgMIgWAAIgBgEIAXAAIgBgKIgUAAQADgIALABQAFAAAHADIAFgOQgJgFgMAAQgLAAgJAGQgIAGgBALIgIABIACAKIAFAAIABAEIgFABIACAKIAHAAQALAVAdACgAhCBgQgDgBAAgDIgCgNIBOgkQA0gYAcgMIACAMQABAEgCAFQgCAEgEABIhGAgQguAWgYAJIgEABIgEgBgAg/BUIABAEQAAABAAAAQABAAAAAAQAAABABAAQAAAAABgBIBFggIBHggQAAAAABAAQAAAAAAgBQABAAAAAAQAAgBAAAAIgBgFIg4AZIAAADIggAPIgBgDgAhGBGQgDgBgBgEIgQhXQAAgEACgFQACgEAEgCQAYgJAvgWIBFggQAFgBADABQACACABADIAQBZQAAAEgBAEQgDADgEABIhGAhIhGAfIgDABIgEgBgAhRgdQAAAAAAAAQgBABAAAAQAAAAAAABQAAAAAAABIAPBYQABABAAAAQAAAAAAAAQABAAAAAAQABAAAAgBQAYgJAtgWIBHgfQAAAAABAAQAAgBAAAAQABgBAAAAQAAAAgBgBIgPhZQAAAAAAAAQAAgBAAAAQgBAAAAAAQAAAAgBAAIg/AdQABAIgHACQgGAEgBgIIAAAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_5, new cjs.Rectangle(-38.3,-25.2,76.6,50.4), null);


(lib.dollar_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AD7EIQg6g0hYgWQg1gNhvgLQgwgEglgEQgzgHgegJQhPgTg3gsIgMgKIgZgZIB5lMIAZAZQA6A0BZAVQA0APBwAJQBwALA1AOQBYAWA6AzQANALAMAOIh5FMIgZgZgAloAnQA6A0BYAWQA1AOBwAKQBvAKA1ANQBZAWA6A0IBjkQQg6gzhYgXQg1gNhwgLQhwgJg0gOQhZgWg6g0gADqC2QgYgQABgZIgHgCIAEgLIAFACIACgFIgFgCIADgKIAHADQAHgJAKgBQALgCAKAHQAKAFAFAKIgLAJQgEgGgFgEQgKgEgGAEIASALIgFAKIgTgMIgCAFIATALIgFAKIgOgKQABAKAJAGQAEADAHABIABARIgCABQgHAAgIgGgAgmBsQgxgDgXglQgXgkARgsQAQgtAugbIAFgDQArgYAuAEQAxAEAXAkQAMAVABAXQAAASgHATQgQAsguAbQgnAYgqAAIgNgBgAhEgxIgaBIQgDAHAIABIAcADIgBADQgBADABADQACACADABIBPAHQAIABADgIIABgDIAcADQADAAADgBQAEgCABgDIAbhJQAAgBAAAAQAAgBAAgBQAAAAAAgBQgBAAAAgBQgCgCgDAAIgxgGQgJgBgCAHIgSAwIgkgDIASgvQACgIgIAAIgxgGIgCAAQgGAAgDAHgAg2AmIAKgbIBPAHIgLAcgAgnATQgEADgCADQgBAEACADQACADAFAAQADABAEgDQAEgDACgEQABgEgCgCQgCgDgEAAIgBAAQgEAAgDACgAgOAcQAAAAAAABQAAAAAAABQAAAAABAAQAAABABAAIAZACQABAAABAAQAAAAABgBQAAAAABAAQAAgBAAAAQABgBAAgBQAAAAgBgBQAAAAAAAAQgBAAgBAAIgZgDQgBAAAAABQgBAAAAAAQgBAAAAABQAAAAgBABgABAAnIgbgEIACgHIAEABQAHAAAIgEQAHgFACgGQADgHgEgFQgEgGgHAAQgHAAgIAEQgHADgCAHIgHAAIASgxIAxAGIAAAAIgaBIgAAygVQgBAEACAEQACACAEABQALABADgKQACgDgCgEQgDgDgEgBIgCAAQgJAAgDAJgAgnAZQABgDAFABQAEgBgBAEQgCAEgEAAQgEAAABgFgAhWAXIAbhIQAYAEAZACIgSAvIgGAAQACgGgDgFQgEgGgIAAQgHgBgIAEQgGAFgDAFQgCAHADAGQAEAFAHABIAEAAIgDAHgAguglQgEADgCADQgBAEACAEQACADAEAAQAFABAEgDQAFgCABgFQABgDgCgEQgCgCgEgBIgCAAQgEAAgDACgAAtAXIgDgBIABgDQACgHgIgBIgBgBQACgEAFgDQAFgDAFAAQAFABACADQADAEgCAFQgCAFgEADQgEACgEAAIgCAAgAhAAJQgDgDACgFQACgEAEgDQAGgDAFAAQAEACADADQADAEgCADIgBABIgHAAQgDADgBADIgBADIgEABQgFgBgCgEgAA7gPQgEgBABgEQABgEAFAAQAFABgCAEQgBAEgEAAIgBAAgAgrgZQgEgBACgEQABgEAFABQAEgBgBAFQgCAEgEAAIgBAAgAjrhYQgYgNABgdIgFgDIADgLIAEADIACgFIgEgCIAEgKIAGADIAEgDQAFgFAHgBQALgBAKAGQALAGAFAHIgMAKQgCgFgHgDQgJgEgHAEIASAJIgFALIgTgLIgCAFIATAKIgFAKIgPgIQACAJAJAGQAHACAFgBIABARIgFABQgFAAgIgEg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("ABzCgQg1gNhvgKQhwgKg1gOQhYgWg6g0IBjkQQA6A0BZAWQA0AOBwAJQBwALA1ANQBYAXA6AzIhjEQQg6g0hZgWgADMCLIAHACQgBAZAYAQQAKAHAHgCIgBgRQgHgBgEgDQgJgGgBgKIAOAKIAFgKIgTgLIACgFIATAMIAFgKIgSgLQAGgEAKAEQAFAEAEAGIALgJQgFgKgKgFQgKgHgLACQgKABgHAJIgHgDIgDAKIAFACIgCAFIgFgCgAgxhXIgFADQguAbgQAtQgRAsAXAkQAXAlAxADQAxAEAtgbQAugbAQgsQAHgTAAgSQgBgXgMgVQgXgkgxgEIgOgBQgmAAglAVgAkHiFIAFADQgBAdAYANQAMAGAGgDIgBgRQgFABgHgCQgJgGgCgJIAPAIIAFgKIgTgKIACgFIATALIAFgLIgSgJQAHgEAJAEQAHADACAFIAMgKQgFgHgLgGQgKgGgLABQgHABgFAFIgEADIgGgDIgEAKIAEACIgCAFIgEgDgAg5AuQgDgBgCgCQgBgDABgDIABgDIgcgDQgIgBADgHIAahIQADgIAIABIAxAGQAIAAgCAIIgSAvIAkADIASgwQACgHAJABIAxAGQADAAACACQAAABABAAQAAABAAAAQAAABAAABQAAAAAAABIgbBJQgBADgEACQgDABgDAAIgcgDIgBADQgDAIgIgBgAg2AmIBOAIIALgcIhPgHgAAlAjIAbAEIABAAIAahIIAAAAIgxgGIgSAxIAHAAQACgHAHgDQAIgEAHAAQAHAAAEAGQAEAFgDAHQgCAGgHAFQgIAEgHAAIgEgBgAhWAXIAcADIADgHIgEAAQgHgBgEgFQgDgGACgHQADgFAGgFQAIgEAHABQAIAAAEAGQADAFgCAGIAGAAIASgvQgZgCgYgEgAArADQgFADgCAEIABABQAIABgCAHIgBADIADABQAFAAAFgCQAEgDACgFQACgFgDgEQgCgDgFgBQgFAAgFADgAg7gGQgEADgCAEQgCAFADADQACAEAFABIAEgBIABgDQABgDADgDIAHAAIABgBQACgDgDgEQgDgDgEgCQgFAAgGADgAglAjQgFAAgCgDQgCgDABgEQACgDAEgDQAEgCAEAAQAEAAACADQACACgBAEQgCAEgEADQgDACgCAAIgCAAgAgnAZQgBAFAEAAQAEAAACgEQABgEgEABIgBgBQgEAAgBADgAgMAfQgBAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAAAQABgBAAAAQAAgBABAAQAAAAABAAQAAgBABAAIAZADQABAAABAAQAAAAAAAAQABABAAAAQAAABgBABQAAAAAAABQgBAAAAAAQgBABAAAAQgBAAgBAAgAA5gKQgEgBgCgCQgCgEABgEQAEgKAKABQAEABADADQACAEgCADQgDAJgJAAIgCAAgAA4gUQgBAEAEABQAFAAABgEQACgEgFgBQgFAAgBAEgAgtgUQgEAAgCgDQgCgEABgEQACgDAEgDQAEgCAFAAQAEABACACQACAEgBADQgBAFgFACQgDADgEAAIgCgBgAgtgeQgCAEAEABQAFAAACgEQABgFgEABIgBgBQgEAAgBAEg");
	this.shape_1.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_4, new cjs.Rectangle(-39.7,-28.9,79.5,57.8), null);


(lib.dollar_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ADoEZQg2g5hXgdQgzgRhvgQQhwgQg0gRQhXgcg1g6QgMgNgLgPICRlCQAKAOAMANQA2A6BXAcQA0ARBvARQBvAQA0AQQBXAdA2A6QAMANAKAOIiQFCQgNgQgKgLgAlqAOQA1A5BXAdQA0ARBwAPQBvARAzARQBXAcA2A5IB3kIQg2g5hXgcQg0gRhvgQQhvgRg0gRQhXgcg2g5gADdDEQgXgQADgZIgGgEIAEgLIAFADIAAgCIACgCIgFgCIAFgLIAGAEQAIgIAKgBQALgBAJAIQAJAGAFAKIgMAIQgCgFgGgFQgJgGgHAEQAHAEAKAIIgFAKIgTgOIgCAFIASANIgFAJIgOgJQABAJAIAHQAHAEAEAAIAAASIgCAAQgGAAgJgIgAguBpQgxgGgUgmQgUgmATgrQAUgqAvgYQAwgYAwAGQAxAHAVAmQAUAlgUArQgTAqgwAYQglAUglAAQgLAAgLgCgAgShGIg8CEQgDAKAKABIBJAMQALABAEgJIA7iFQACgEgCgDQgCgDgEgBIhJgKIgDgBQgIAAgEAIgAhEBCQgBAAAAgBQAAAAgBAAQAAAAAAgBQAAAAABAAIA8iFQAAAAAAgBQAAAAAAAAQABAAAAAAQABAAAAAAIBKALQAAAAABAAQAAAAAAAAQABABAAAAQgBABAAAAIg8CFQAAAAAAAAQAAAAgBABQAAAAAAAAQgBAAAAAAgAghA6QgCADACADQABACADABQAJAAACgGQAEgHgIgCIgCAAQgHAAgCAGgAAEg3IAoAGIADgHIgogFgAjlhnQgLgHgFgMQgFgMACgMIgGgEIAFgKIAEADIAAgCIACgDIgEgDIAEgKIAGAEQAQgPAWAOQAKAGAEAIIgMAKQgDgFgGgEQgJgGgGAFIARAKIgGAKIgSgMIgCACIAAACIASAMIgGAKIgOgJQABAJAJAGQAFADAGAAIAAAQIgDABQgGAAgJgFg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("ABoCmQgzgRhwgRQhvgPg0gRQhXgdg1g5IB2kIQA2A5BWAcQA0ARBwARQBvAQA0ARQBXAcA1A5Ih3EIQg1g5hXgcgADDCXIAFAEQgCAZAXAQQAKAJAGgBIABgSQgEAAgHgEQgJgHgBgJIAPAJIAFgJIgSgNIACgFIATAOIAEgKQgKgIgGgEQAHgEAIAGQAHAFABAFIAMgIQgEgKgJgGQgKgIgKABQgKABgIAIIgGgEIgFALIAFACIgCACIAAACIgFgDgAgxhWQgwAYgTAqQgTArATAmQAVAmAxAGQAxAHAvgZQAwgYASgqQAUgrgUglQgUgmgxgHIgVgBQgmAAglATgAj+iWIAFAEQgBAMAFAMQAFAMALAHQALAHAHgDIAAgQQgGAAgGgDQgIgGgBgJIAOAJIAFgKIgSgMIABgCIACgCIASAMIAFgKIgRgKQAHgFAJAGQAGAEACAFIANgKQgEgIgKgGQgXgOgPAPIgGgEIgFAKIAFADIgCADIAAACIgFgDgAACBVIhKgMQgJgBADgKIA8iEQAFgJAKACIBJAKQAEABACADQACADgCAEIg7CFQgEAIgJAAIgCAAgAgJhFIg9CFQAAAAAAAAQAAABAAAAQAAAAABAAQAAABAAAAIBLALQAAAAAAAAQABAAAAAAQAAgBABAAQAAAAAAAAIA8iFQAAAAAAgBQAAAAAAgBQAAAAAAAAQgBAAgBAAIhJgLQgBAAAAAAQgBAAAAAAQAAAAAAAAQAAABAAAAgAgdBDQgDgBgCgCQgBgDABgDQADgHAIABQAJACgEAHQgCAGgHAAIgCAAgAADg3IAEgGIAoAFIgDAHg");
	this.shape_1.setTransform(0.05,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_3, new cjs.Rectangle(-39.9,-30.8,79.9,61.6), null);


(lib.dollar_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AmAhpIAdgSQBDgkBbgJQA2gHBvACQBuACA2gGQBagKBDgkIAegRIBCFaIgdASQhDAkhaAJQg2AHhvgCQhvgBg2AFQhaAKhDAkQgNAHgQAKgAi/iJQhbAKhDAjIA3EdQBDgkBagKQA2gGBvABQBvACA2gGQBagKBDgjIg3kdQhDAkhaAKQg2AGhugBIgqgBQhQAAgrAFgAAWBxQgwgBgpghQgogggJguQgJguAcgiQAcgiAwABQAwABApAhQAoAgAJAuQAJAvgcAhQgbAhguAAIgDAAgAhEhDQgFAGACAKIAVBsQABAIAIAHQAIAGAJAAIBQABQAJABAFgHQAGgGgCgJIgVhsQgCgKgHgGQgIgHgKAAIhPgBQgJABgGAGgAEYBXQgJgEgGgJIgHACIgCgLIAFgBIgBgGIgFACIgCgLIAHgCQABgLAIgJQAHgJAMgFQAMgEAHACIgEARQgFgCgHADQgKADgCAJIATgHIABAMIgVAHIABAFIAUgHIABALIgQAGQAHAGAJgEQAFgCAGgGIAJAMQgEAIgNAEQgHADgGAAQgFAAgFgCgAgZBGQgGAAgFgDQgFgFgBgFIgThhIBsACIATBgQABAFgDAFQgEADgGAAgAgigRQgLANADARQAEASAQANQAPAMASABQATAAALgNQALgMgEgSQgDgSgQgMQgPgNgSgBIgCAAQgSAAgKANgAAKAxQgPAAgNgKQgNgLgCgPQgDgOAJgLQAJgKAPAAQAPABAMAKQANALADAOQADAPgJAKQgIAKgPAAIgBAAgAkpgZIgHACIgCgLIAFgCIgBgEIgFABIgCgKIAHgDQABgMAIgKQAHgKAMgDQALgDAIADIgEAPQgHgBgFACQgJACgEAKIATgGIACALIgWAHIABAFIAVgHIABALIgQAFQAGAGAKgDQAIgCADgFIAJANQgEAHgNAEQgHACgGAAQgRAAgIgOgAg+gwIgBgDQgBgHADgDQAEgFAGAAIBPABQAGAAAFAFQAFAEABAGIABAEIhsgCgAgRg4QACAHAHAAQAIAAgCgHQgCgIgHAAQgHAAABAIgAgug9QgCACABACQABAIAIAAQADAAACgCQABgCAAgDQgBgIgIAAIgBAAQgBAAAAABQgBAAAAAAQgBAAAAABQgBAAAAABg");
	this.shape.setTransform(0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AldhbQBDgkBbgKQA2gGBuACQBuABA2gGQBbgKBDgkIA3EdQhDAjhbAKQg2AHhvgCQhugCg2AGQhaAKhEAkgAhihPQgbAiAJAuQAIAuApAhQApAgAvABQAxABAcghQAcgigJgvQgJgugoggQgpghgwgBIgDAAQguAAgcAhgAEYBYQAKADANgEQAMgFAFgIIgJgLQgGAFgFACQgJAEgHgGIAQgGIgBgLIgVAHIAAgEIAUgIIgBgLIgTAHQADgJAKgEQAGgCAGABIADgRQgHgCgLAEQgMAFgHAJQgIAJgBALIgHACIACALIAFgBIABAFIgFABIACALIAGgCQAHAKAJAEgAkDgNQANgEADgHIgJgMQgCAEgIACQgKADgHgGIARgFIgBgLIgVAHIgBgFIAWgHIgCgLIgTAGQAEgKAJgCQAFgCAGACIAFgQQgJgDgLADQgLAEgIAKQgHAJgCAMIgGADIACALIAEgCIABAEIgEACIACALIAHgCQAMATAagHgAgYBPQgKAAgHgHQgIgGgCgJIgVhsQgCgKAGgGQAFgGAKAAIBPABQAJAAAJAGQAHAHACAJIAVBsQACAJgGAGQgFAHgKAAgAgrA5QACAFAFAFQAFAEAFAAIBQABQAGAAADgEQADgEgBgGIgShgIhsgBgAg9g9QgEAEACAGIAAAEIBsABIAAgEQgBgGgGgEQgEgEgHAAIhPgBQgFAAgEAEgAALA5QgSAAgQgNQgPgNgEgSQgDgRAKgNQAMgNASABQATAAAPANQAQANADARQAEASgLANQgLAMgSAAIgBAAgAgbgLQgKAKADAOQADAPANALQANAKAOAAQAQABAJgLQAIgKgCgPQgDgOgNgKQgNgLgOAAIgBAAQgPAAgIAKgAgSg4QAAgHAHAAQAHAAACAHQACAIgJAAQgHAAgCgIgAgvg4QgBgDACgCQABgDADABQAJAAAAAHQABADgCACQgCACgCAAQgJAAAAgHg");
	this.shape_1.setTransform(0.05,-0.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_2, new cjs.Rectangle(-38.5,-24.1,77.1,48.3), null);


(lib.dollar_1_v2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjYDoIgdgEIhUkcIAcAFQBCAFBGgdQArgRBRgxQBPgxAqgSQBHgdBBAGQAQABAOADIBUEcIgdgFQhBgGhHAeQgqARhRAxQhPAygrARQg6AYg3AAIgXgBgAjfDPQBBAFBHgdQAqgRBQgxQBRgxAqgSQBGgdBBAGIhFjoQhBgHhHAeQgqARhPAyQhRAxgrARQhGAdhBgGgAgoBmQghgNgMgnQgLgmARgoQARgpAjgWQAjgXAhAOQAhANAMAnQALAlgRAoQgRApgjAXQgXANgUAAQgNAAgMgEgAgVBPQgDADACACQAAABABAAQAAAAABAAQAAAAABAAQAAAAABAAIAggoIArgGQAAAAAAAAQAAAAAAAAQAAAAAAAAQABgBAAAAQAAAAABgBQAAAAAAAAQABgBAAAAQAAgBgBAAQACgEgEAAIgfAFIAugcQADgBACgEQACgDgCgDIgWhMQgBgEgDgBQgDgBgEACIg5AjQgmAYgVAKIgEAGQgCAEABADIAWBLQABADAEACQAEABACgCIAugbgAgwBGIgWhKQgBgBAAAAQAAAAABgBQAAAAAAgBQAAAAABAAQATgLAngYIA6gjQAAAAAAAAQABAAAAAAQAAAAAAAAQABABAAAAIAWBMQABAAAAAAQAAAAgBABQAAAAAAAAQAAABgBAAIg6AjQgmAYgUAKIgBABIgBgCg");
	this.shape.setTransform(0,-0.0243);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("AjfDPIhFjpQBBAGBGgdQArgRBRgxQBPgyAqgRQBHgeBBAHIBFDoQhAgGhHAdQgqAShRAxQhQAxgqARQg8AZg3AAIgVgBgAgahbQgkAWgRApQgRAoALAmQAMAnAhANQAiAMAigVQAjgXARgpQARgogLglQgLgngigNQgMgFgNAAQgUAAgWAOgAgWBUQgCgCADgDIAXgcIguAbQgCACgEgBQgDgCgBgDIgXhLQgBgDACgEIAFgGQAUgKAngYIA5gjQADgCADABQADABABAEIAXBMQABADgCADQgBAEgEABIguAcIAfgFQAEAAgBAEQAAAAAAABQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAABQAAAAAAAAQgBAAAAAAQAAAAAAAAIgqAGIghAoIgBAAIgDgBgAAvhNIg6AjQgmAYgUALQgBAAAAAAQAAABAAAAQAAABAAAAQAAAAAAABIAWBKQABABAAAAQAAABAAAAQABAAAAAAQAAAAAAgBQAUgKAmgYIA6gjQABAAAAgBQAAAAABAAQAAgBAAAAQAAAAAAAAIgXhMIgBgBIgBAAg");
	this.shape_1.setTransform(-0.025,-0.0148);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1_v2, new cjs.Rectangle(-33,-23.3,66.1,46.6), null);


(lib.dollar_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AjzEuQgTgIgVgNIhwlQQAVANAUAJQBZAkBPguQAwgdBThfQBShfAwgdQBPgwBZAmQATAIAWANIBwFQQgVgOgUgIQhZglhQAvQgwAdhSBfQhSBfgwAdQgtAbgvAAQglAAgogRgAj9EQQBZAmBQgwQAwgdBShfQBShfAwgdQBQgvBZAlIhckTQhZgmhPAwQgwAdhSBfQhTBfgwAdQhPAvhZglgAgkCSQgmgFgPgvQgPgsAQg5QAQg6AlgrQAjgrAlAGQAlAFAQAuQAOAtgQA5QgQA5gkAsQgfAmghAAIgIgBgAAqh6QgVAUgnAuQgoAvgVAUQgIAHADALIAeBZQABAEAEAAQADABAEgDQAQgQAfgkIgXAtQgDAFADADQABAAAAAAQAAAAABAAQAAAAABAAQAAgBABAAQAAAAAAgBQAAAAABAAQAAAAAAAAQAAgBAAAAIAfg/IAtgYIABgBQADgCAAgEQAAgFgEACIghASQAYgcAYgYQADgDACgGQABgFgBgEIgehaQgBgEgDgBIgBAAQgDAAgDAEgAj9BtQgMgIgIgLIgIgCIgDgKIAGABIgBgCIgBgCIgGgCIgDgLIAIADQACgTAbgBQANAAAIADIgDAOQgFgCgHAAQgMABgCAGQALABAKgBIADAKQgMABgMgBIACAFIAXABIACAKIgSAAQAJAIAKgBQAGAAAFgEIALANQgEAHgNAAIgBAAQgNAAgMgHgAgvBwIgehaQAAgBgBAAQAAgBABAAQAAgBAAAAQAAgBABAAQAVgUAoguQAnguAUgVQABAAAAgBQAAAAABAAQAAAAAAABQABAAAAABIAeBZQAAABAAAAQAAABgBAAQAAABAAAAQAAABgBAAQgTAUgpAuQgmAtgWAWIgCABIAAgBgAEdgiQgfgFgPgUIgHABIgDgLIAFAAIgBgDIgBgCIgFAAIgDgKIAHgBQAAgKAIgHQAIgGANACQAMABAKAIIgDAMQgGgEgIgCQgLgBgDAHIAWACIACAKIgYgCIACAFIAXACIADAKIgTgBQAIAHALACQAHAAAGgBIAMAPQgDACgHAAIgJAAg");
	this.shape.setTransform(-0.025,0);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EB1D24").s().p("Aj8EQIhckTQBZAlBPgvQAxgdBThfQBRhfAwgdQBQgwBYAmIBcETQhZglhPAvQgwAdhTBfQhSBfgwAdQgsAbgwAAQglAAgngRgAgjhsQgkArgRA6QgQA5APAsQAQAvAlAFQAlAGAjgrQAlgsAQg5QAQg5gPgtQgPgugmgFIgIgBQghAAgfAmgAkYBYIAIACQAHALANAIQANAHANAAQAMAAAFgHIgMgNQgFAEgGAAQgKABgJgIIASAAIgCgKIgXgBIgCgFQAMABAMgBIgDgKQgKABgLgBQACgGAMgBQAIAAAFACIACgOQgHgDgNAAQgcABgBATIgIgDIACALIAHACIAAACIABACIgGgBgAEdgiQAPABAEgDIgMgPQgGABgHAAQgLgCgHgHIASABIgCgKIgYgCIgBgFIAYACIgDgKIgVgCQACgHALABQAIACAGAEIADgMQgKgIgMgBQgNgCgIAGQgIAHAAAKIgHABIADAKIAGAAIAAACIABADIgFAAIAEALIAHgBQAOAUAfAFgAgxB6QgDAAgBgEIgehZQgDgLAHgHQAWgUAogvQAnguAUgUQADgEAEAAQADABACAEIAdBaQACAEgCAFQgBAGgEADQgYAYgYAcIAhgSQAEgCAAAFQAAAEgDACIgBABIgtAYIgfA/QAAAAAAABQAAAAAAAAQAAAAgBAAQAAABAAAAQAAAAgBABQAAAAgBAAQAAAAgBAAQAAAAAAAAQgDgDADgFIAWgtQgfAkgQAQQgDADgDAAIgBgBgAAthzQgVAVgnAuQgoAugVAUQAAAAgBABQAAAAAAABQAAAAAAABQAAAAAAABIAeBaQAAAAAAABQAAAAABAAQAAAAAAAAQABAAABgBQAVgWAmgtQApguATgUQABAAAAgBQAAAAABgBQAAAAAAgBQAAAAAAgBIgehZIgBgCIgBABg");
	this.shape_1.setTransform(-0.05,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_1, new cjs.Rectangle(-39.6,-31.9,79.2,63.8), null);


(lib.cta_rollover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E39126").s().p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	this.shape.setTransform(10,12);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta_rollover, new cjs.Rectangle(0,0,20,24), null);


(lib.cta_arrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("Ag/AKIAAgTIB/AAIAAATg");
	this.shape.setTransform(1.9821,5.4827,0.3891,0.5,0,-50.0001,130.0012);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("Ag/AKIAAgTIB/AAIAAATg");
	this.shape_1.setTransform(1.9679,2.2327,0.3891,0.5,50.0001);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta_arrow, new cjs.Rectangle(0,0,4,7.8), null);


(lib.cover1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AygE2IAAprMAlBAAAIAAJrg");
	this.shape.setTransform(-0.7,25.3,1.2658,2.2544,-4.9997,0,0,-119,-31.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cover1, new cjs.Rectangle(0,0,311.1,165.4), null);


(lib.cover = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AnuCqIAAlTIPdAAIAAFTg");
	this.shape.setTransform(40.6521,85.0044,0.8153,0.2946);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AnuCqIAAlTIPdAAIAAFTg");
	this.shape_1.setTransform(40.6521,5.0044,0.8153,0.2946);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E6E7E8").s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_2.setTransform(40.3284,44.9833,0.208,1.0294,0,0,180);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AnuCqIAAlTIPdAAIAAFTg");
	this.shape_3.setTransform(634.1746,85.0044,1.8958,0.2946);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AnuCqIAAlTIPdAAIAAFTg");
	this.shape_4.setTransform(634.1746,5.0044,1.8958,0.2946);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E6E7E8").s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_5.setTransform(633.8521,44.9833,0.4838,1.0294,0,0,180);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-193.9,0,194,0).s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_6.setTransform(471.9777,85.0209,0.3557,0.147,0,0,180);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-193.9,0,194,0).s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_7.setTransform(471.9777,5.0209,0.3557,0.147,0,0,180);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-193.9,0,194,0).s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_8.setTransform(141.0097,85.0209,0.3144,0.147);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-193.9,0,194,0).s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_9.setTransform(141.0097,5.0209,0.3144,0.147);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#E6E7E8","rgba(230,231,232,0)"],[0,1],-193.9,0,194,0).s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_10.setTransform(472.0277,44.9833,0.3557,1.0294,0,0,180);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["#E6E7E8","rgba(230,231,232,0)"],[0,1],-193.9,0,194,0).s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_11.setTransform(141.0097,44.9833,0.3144,1.0294);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#E90000").s().p("ApcHRIAAuhIS5AAIAAOhg");
	this.shape_12.setTransform(590.0198,44.9774,0.1653,0.9677);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#E90000").s().p("ApcHRIAAuhIS5AAIAAOhg");
	this.shape_13.setTransform(40.5114,44.9774,0.6694,0.9677);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#E90000","rgba(233,0,0,0)"],[0,1],-193.9,0,194,0).s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_14.setTransform(499.4426,45.0219,0.4202,1.3235,0,0,180);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#E90000","rgba(233,0,0,0)"],[0,1],-193.9,0,194,0).s().p("A+TFUIAAqnMA8nAAAIAAKng");
	this.shape_15.setTransform(147.512,45.0219,0.3479,1.3235);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,728,90);


(lib.counterMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.counter_days = new cjs.Text("", "12px 'Kokonor'", "#29A945");
	this.counter_days.name = "counter_days";
	this.counter_days.lineHeight = 22;
	this.counter_days.lineWidth = 169;
	this.counter_days.parent = this;
	this.counter_days.setTransform(2,2);

	this.counter_txt = new cjs.Text("", "12px 'Kokonor'", "#29A945");
	this.counter_txt.name = "counter_txt";
	this.counter_txt.lineHeight = 22;
	this.counter_txt.lineWidth = 177;
	this.counter_txt.parent = this;
	this.counter_txt.setTransform(2,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.counter_txt},{t:this.counter_days}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.counterMC, new cjs.Rectangle(0,0,181,35.9), null);


(lib.arrowLeftRight = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// hitArea
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,0,0,0.008)").s().p("AiuC5IAAlxIFdAAIAAFxg");
	this.shape.setTransform(3.0142,-0.0027,0.2857,0.5405);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2));

	// arrow
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#969696").s().p("AguAiIBEhQIAJALIANgLIACACIhMBbg");
	this.shape_1.setTransform(4.1,3.2,0.8654,0.8671,0,0,0,0.1,-0.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#969696").s().p("AgugnIAQgMIBMBbIgPAMg");
	this.shape_2.setTransform(4.0243,-3.4731,0.8654,0.8671);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AguAiIBEhQIAJALIANgLIACACIhMBbg");
	this.shape_3.setTransform(4.1,3.3,0.8655,0.8671,0,0,0,0.1,-0.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgugnIAQgMIBMBbIgPAMg");
	this.shape_4.setTransform(4.0247,-3.4733,0.8655,0.8671);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1}]}).to({state:[{t:this.shape_4},{t:this.shape_3}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2,-10,10.1,20);


(lib.subsubtext = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(4));

	// Layer_1
	this.m01 = new lib.m01();
	this.m01.name = "m01";
	this.m01.parent = this;
	this.m01.setTransform(0,42);

	this.m00 = new lib.Symbol1();
	this.m00.name = "m00";
	this.m00.parent = this;

	this.m11 = new lib.m11();
	this.m11.name = "m11";
	this.m11.parent = this;
	this.m11.setTransform(0,45);

	this.m10 = new lib.Symbol2();
	this.m10.name = "m10";
	this.m10.parent = this;

	this.m20 = new lib.Symbol3();
	this.m20.name = "m20";
	this.m20.parent = this;

	this.m31 = new lib.m32();
	this.m31.name = "m31";
	this.m31.parent = this;
	this.m31.setTransform(0,45);

	this.m30 = new lib.Symbol4();
	this.m30.name = "m30";
	this.m30.parent = this;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.m00},{t:this.m01}]}).to({state:[{t:this.m10},{t:this.m11}]},1).to({state:[{t:this.m20}]},1).to({state:[{t:this.m30},{t:this.m31}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,336.1,65);


(lib.price_old_line_container = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.line = new lib.price_old_line();
	this.line.name = "line";
	this.line.parent = this;
	this.line.setTransform(0,24);

	this.timeline.addTween(cjs.Tween.get(this.line).wait(1));

}).prototype = getMCSymbolPrototype(lib.price_old_line_container, new cjs.Rectangle(0,0,92,26), null);


(lib.loopy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.Tween1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(728,38);

	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(0,38);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},359).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true,x:0},359).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-728,0,2184,76);


(lib.label_green_circle = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.l1 = new lib.label_green_circle_checl_2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(-4.65,5.2,1,1,-45);

	this.l0 = new lib.label_green_circle_check_1();
	this.l0.name = "l0";
	this.l0.parent = this;
	this.l0.setTransform(-6.4,-2.25,1,1,48.5012);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#30A945").s().p("AhiBjQgpgpAAg6QAAg5ApgpQApgpA5AAQA6AAApApQApApAAA5QAAA6gpApQgpApg6AAQg5AAgpgpg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.l0},{t:this.l1}]}).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,0,0,0)").s().p("AhZgFIAagYIArAvIBWhWIAYAbIhwBug");
	this.shape_1.setTransform(0,-0.125);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green_circle, new cjs.Rectangle(-14,-14,28,28), null);


(lib.label_green = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.text = new lib.label_green_text();
	this.text.name = "text";
	this.text.parent = this;
	this.text.setTransform(32,4);

	this.gr = new lib.label_green_circle();
	this.gr.name = "gr";
	this.gr.parent = this;
	this.gr.setTransform(14,14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.gr},{t:this.text}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_green, new cjs.Rectangle(0,0,28,28), null);


(lib.label_cashback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.dynText = new cjs.Text("Helloblello", "11px 'Kokonor'", "#FFFFFF");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 11;
	this.dynText.lineWidth = 56;
	this.dynText.parent = this;
	this.dynText.setTransform(5,2);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

	// Layer_2
	this.bgr = new lib.emptyMovieClip();
	this.bgr.name = "bgr";
	this.bgr.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.bgr).wait(1));

}).prototype = getMCSymbolPrototype(lib.label_cashback, new cjs.Rectangle(3,0,60,22.2), null);


(lib.endframe_static = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// mask4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AyWGuIAAjIMAktAAAIAADIg");
	mask.setTransform(117.5001,42.9993);

	// line4
	this.l3 = new lib.endframe_static_line4();
	this.l3.name = "l3";
	this.l3.parent = this;
	this.l3.setTransform(0,66);

	var maskedShapeInstanceList = [this.l3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.l3).wait(1));

	// mask3 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AyWFAIAAjIMAktAAAIAADIg");
	mask_1.setTransform(117.5001,31.9993);

	// line3
	this.l2 = new lib.endframe_static_line3();
	this.l2.name = "l2";
	this.l2.parent = this;
	this.l2.setTransform(0,44);

	var maskedShapeInstanceList = [this.l2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.l2).wait(1));

	// mask2 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("EgjJADcIAAjSMBGTAAAIAADSg");
	mask_2.setTransform(224.9979,21.9992);

	// line2
	this.l1 = new lib.endframe_static_line2();
	this.l1.name = "l1";
	this.l1.parent = this;
	this.l1.setTransform(0,23);

	var maskedShapeInstanceList = [this.l1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.l1).wait(1));

	// mask1 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("EgjJABpIAAjRMBGTAAAIAADRg");
	mask_3.setTransform(224.9979,10.4992);

	// line1
	this.l0 = new lib.endframe_static_line1();
	this.l0.name = "l0";
	this.l0.parent = this;

	var maskedShapeInstanceList = [this.l0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.l0).wait(1));

}).prototype = getMCSymbolPrototype(lib.endframe_static, new cjs.Rectangle(0,0,450,44), null);


(lib.dollar_cashback = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_2
	this.text = new lib.dollar_cashback_text();
	this.text.name = "text";
	this.text.parent = this;
	this.text.setTransform(0,1.45,1,1,9.9999);

	this.timeline.addTween(cjs.Tween.get(this.text).wait(1));

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E90000").s().p("AkTETQhyhyAAihQAAihByhyQBzhyCgAAQCiAABxByQBzByAAChQAAChhzByQhxBziiAAQigAAhzhzg");
	this.shape.setTransform(-0.0216,-0.0194,0.8974,0.8974);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.dollar_cashback, new cjs.Rectangle(-35,-35,70,70), null);


(lib.cta = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// text
	this.dynText = new cjs.Text("", "11px 'Kokonor'");
	this.dynText.name = "dynText";
	this.dynText.lineHeight = 9;
	this.dynText.lineWidth = 226;
	this.dynText.parent = this;
	this.dynText.setTransform(-248,5);

	this.timeline.addTween(cjs.Tween.get(this.dynText).wait(1));

	// arrowsMask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	mask.setTransform(-10,12);

	// arrows
	this.arrow2 = new lib.cta_arrow();
	this.arrow2.name = "arrow2";
	this.arrow2.parent = this;
	this.arrow2.setTransform(-13,8);

	this.arrow1 = new lib.cta_arrow();
	this.arrow1.name = "arrow1";
	this.arrow1.parent = this;
	this.arrow1.setTransform(-10,8);

	var maskedShapeInstanceList = [this.arrow2,this.arrow1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.arrow1},{t:this.arrow2}]}).wait(1));

	// rollover
	this.rollover1MC = new lib.emptyMovieClip();
	this.rollover1MC.name = "rollover1MC";
	this.rollover1MC.parent = this;
	this.rollover1MC.setTransform(-250,0);
	this.rollover1MC.alpha = 0;

	this.rollover2MC = new lib.cta_rollover();
	this.rollover2MC.name = "rollover2MC";
	this.rollover2MC.parent = this;
	this.rollover2MC.setTransform(-10,12,1,1,0,0,0,10,12);
	this.rollover2MC.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.rollover2MC},{t:this.rollover1MC}]}).wait(1));

	// bgr
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FAA332").s().p("AhjB4IAAjvICpAAQAeAAAAAeIAACzQAAAegeAAg");
	this.shape.setTransform(-10,12);

	this.shapeMC = new lib.emptyMovieClip();
	this.shapeMC.name = "shapeMC";
	this.shapeMC.parent = this;
	this.shapeMC.setTransform(-250,0);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shapeMC},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta, new cjs.Rectangle(-250,0,250,25.3), null);


// stage content:
(lib.index = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var self = this;
		
		
		var countDownDate = new Date("August 11, 2019 23:59:59").getTime();
		
		var x = setInterval(function() {
		  var now = new Date().getTime();
		
		  var distance = countDownDate - now;
		  function padder(n) {
		         return (n < 10 ? "0" + n : n);
		       }
		  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		
		  self.counterMC.counter_days.text = "nog " + padder(days) + " dagen"
		  self.counterMC.counter_txt.text = "en " + padder(hours) + ":"
		  + padder(minutes) + ":" + padder(seconds);
		  if (distance < 0) {
		    clearInterval(x);
		    self.counterMC.counter_days.text = "alleen vandaag nog";
			  self.counterMC.counter_txt.text = "";
		  }
		}, 1000);
		//stage.enableMouseOver(6);
		
		var mmMC = this.mediamarkt;
		var mm2MC = this.mediamarkt2;
		var greyMC = this.grey;
		
		var logoMC = this.logo;
		var productMC = this.product;
		var priceMC = this.price;
		var labelMC = this.label;
		var leftMC = this.left;
		var rightMC = this.right;
		var ctaMC = this.cta;
		var cta2MC = this.cta2;
		var coverMC = this.cover;
		var hitAreaMC = this.hitAreaMC;
		var leftButtonDiv = document.getElementById("arrow_left");
		var rightButtonDiv = document.getElementById("arrow_right");
		var ctaButtonDiv = document.getElementById("cta");
		var lineMC = this.lineContainer;
		var campaignMC = this.campaign;
		var whiteMC = this.white;
		var white2MC = this.white2;
		var servButtonMC = this.servButton;
		var vanafMC = this.vanaf;
		var cover2MC = this.cover2;
		var endMC = this.endframe;
		
		var text1MC = this.text1;
		var text2MC = this.text2;
		var text3MC = this.text3;
		
		var numberOfProducts = productID = numberOfServices = prevProductID = 0;
		dynamic.productID = productID;
		var productsObject = new Array();
		
		leftMC.mouseChildren = rightMC.mouseChildren = false;
		
		var bigJump = false;
		
		var productImgScale = 0.141; //1,552795031055901 //0.19 //0.2
		if (typeof VARIATION == "undefined") {
			VARIATION = "";
		}
		if (VARIATION == "dollar") {
			productImgScale *= 1.2;
			//leftMC.y -= 25;
			//rightMC.y -= 25;
		}
		var numberImgScale = 0.4;
		var logoScale = 0.5;
		var campaignScale = 0.27;
		
		var autoRotateTime = 3.0;
		var ctaHover = false;
		self.counterMC.x = 1000;
		var dollarCashbackToX = 175;
		var dollarCashbackToX0 = 265;
		var dollarCashbackToY = 25;
		var prijsToX = 548;
		var logoToY = -24;
		var productToX = 480; //productMC.x;
		var labelToX = 800;
		var priceToX = priceMC.x;
		var text1ToY = text1MC.y;
		var text2ToY = text2MC.y;
		var text3ToX = text3MC.x;
		var leftToX = leftMC.x;
		var rightToX = rightMC.x;
		var ctaToX = dynamic.width - 10;
		var mm2ToX = mm2MC.x;
		var lineToX = 451;
		var servButtonToX = servButtonMC.y;
		var vanafToX = 451;
		var lines = 2;
		for (var i = 0; i<lines; i++) {
			var lMC = endMC["l" + i];
			lMC.toY = lMC.y;
			lMC.y = lMC.toY + 21;
			lMC.rotation = 7;
		}
		var endToX = 22;
		var maniaLabelToX = 5;
		var gToX = 25;
		var maxGreen = 3;
		for (i = 0; i<maxGreen; i++) {
			var a = self["g" + i];
			a.x = gToX;
			a.toY = a.y;
			a.y = a.toY + 25;
			a.alpha = 0.0;
			a.gr.l0.scaleX = a.gr.l1.scaleX = 0;
			a.gr.scaleX = a.gr.scaleY = 0.0;
		}
		var serviceLabelToX = 548;
		
		var isLeftClick = false;
		
		productMC.x = dynamic.width + 150;
		//priceMC.x = - dynamic.width / 2;
		text1MC.y = text1ToY + 22;
		text2MC.y = text2ToY + 22;
		leftMC.x = leftToX + 25;
		rightMC.x = rightToX - 25;
		ctaMC.x = ctaToX - 70;
		ctaMC.alpha = 0.0;
		mm2MC.y = 78;
		mm2MC.alpha = 0;
		mm2MC.x = mm2ToX + 50;
		lineMC.x = lineToX;
		lineMC.line.rotation = -14;
		lineMC.line.scaleX = 0.0;
		vanafMC.x = vanafToX + 20;
		cover2MC.y = 0;
		endMC.x = endToX;
		cta2MC.alpha = 0.0;
		if (VARIATION == "dollar") {
			self.servicelabel.x = serviceLabelToX;
			priceToX -= 25;
			lineToX -= 25;
			text1MC.x -= 15;
			text2MC.x -= 15;
		}
		if (VARIATION == "merkenweek") {
			self.servicelabel.x = serviceLabelToX;
			priceToX -= 25;
			lineToX -= 25;
			text1MC.x -= 17;
			text2MC.x -= 17;
			productImgScale = 0.4;
			leftToX = 370;
			rightToX = 590;
			leftMC.x = leftToX + 25;
			leftMC.scaleX = leftMC.scaleY = rightMC.scaleX = rightMC.scaleY = 1.5;
			rightMC.x = rightToX - 25;
		}
		
		// DCO 2.0 (!)
		this.getShotDuration = function (shot) {
			
			var duration = 3.0;
		
			return duration;
			
		}
		
		this.getShotData = function (i) {
		
			// IF NOT EMPTY SHOT
			if (dynamic.getDynamic("shot_type", i) == "") {
				return undefined;
			}
		
			return {
				
				"products": dynamic.getDynamic("shot_type", i).split("|"),
				"main_specs": dynamic.getDynamic("copy", i).split("|"),
				"sub_specs": dynamic.getDynamic("price_type", i).split("xxx")[0].split("|"),
				"label_new": dynamic.getDynamic("price_type", i).split("xxx")[1].split("|"),
				//"label_img": dynamic.getDynamic("price", i).split("|"),
				"price": dynamic.getDynamic("price", i).split("|"),
				"cta": dynamic.getDynamic("cta_copy", i),
				"colorPanel": dynamic.getDynamic("font_scale", i).split("|")[0]
				//"categoryType": dynamic.getDynamic("font_scale", i).split("|")[1]
				//"logo": dynamicContent.dcodata[0].Logo1.split("|")
		
			}
		
		}
		
		this.playShot = function (shot) {
		
			if (shot.index == 1) {
				
				numberOfProducts = shot.products.length;
				
				var h = 0;
				
				var isGreenLabel = false;
				
				for (var i = 0; i<numberOfProducts; i++) {
					
					if (shot.products[i] != "" && shot.products[i] != "-") {
						
						var productName = shot.products[i];
						var checkProductName = productName.toLowerCase();
						var isService = false;
						if (shot.products[i][0] == "^") {
							isService = true;
							productName = productName.substring(1, productName.length);
							numberOfServices++;
						}
						
						var productObject = {
							name: productName,
							main_specs: shot.main_specs[i],
							sub_specs: shot.sub_specs[i],
							label_new: shot.label_new[i],
							label: !dynamic.isImageEmpty(LABEL + i),
							price: shot.price[i].split("^")[0],
							priceOld: shot.price[i].split("^")[1],
							service: isService
						}
						
						var imgMC = new createjs.MovieClip();
						productMC.addChild(imgMC);
						if (!(checkProductName == "tv" || checkProductName == "audio")) {
							dynamic.setDynamicImage(imgMC, (PRODUCT + i));
						}
						var toScale = productImgScale;
						var toX = 0;
						var toY = 0;
						imgMC.x = h * 728;
						
						if (VARIATION == "dollar" && i == 1) {
							imgMC.x += 40;
						}
						if (VARIATION == "dollar" && i == 2) {
							imgMC.x += 40;
						}
						if (VARIATION == "merkenweek" && i == 0) {
							//toX += 190;
							
						}
						if (VARIATION == "merkenweek" && i == 3) {
							//imgMC.x += 140;
							//toScale *= 0.68;
							//toY = 15;
						}
						h++;
						
						if (shot.sub_specs[i] != undefined && shot.sub_specs[i] != "" && shot.sub_specs[i] != " " && shot.sub_specs[i] != "-") {
							imgMC.green = true;
							isGreenLabel = true;
						}
						if (shot.label_new[i].length > 0 && shot.label_new[i].split("_")[0].toLowerCase() == "cashback") {
							imgMC.cashback = true;
						}
						if (shot.label_new[i].length > 0 && shot.label_new[i].split("_")[0].toLowerCase() == "service") {
							imgMC.service = true;
						}
						/*
						if (imgMC.green == true) {
							toScale *= 0.82;
							toX = 43;
							if (imgMC.cashback == true) {
								toScale *= 0.92;
								toY = -22;
							}
						} else if (imgMC.cashback == true) {
							toScale *= 0.76;
							toY -= 23;
						}
						*/
						imgMC.scaleX = imgMC.scaleY = toScale;
						imgMC.x += toX;
						imgMC.y += toY;
						
						var logoImgMC = new createjs.MovieClip();
						logoMC.addChild(logoImgMC);
						logoImgMC.alpha = 0;
						//dynamic.setDynamicImage(logoImgMC, (LOGO + i), 0, 0);
						logoImgMC.scaleX = logoImgMC.scaleY = logoScale;
						
						var labelImgMC = new createjs.MovieClip();
						labelMC.addChild(labelImgMC);
						if (!dynamic.isImageEmpty(LABEL + i)) {
							//dynamic.setDynamicImage(labelImgMC, (LABEL + i));
						}
						
						if (checkProductName == "tv" || checkProductName == "audio") {
							productObject = checkProductName;
							shot.main_specs[i] = shot.sub_specs[i] = shot.price[i] = "";
							if (checkProductName == "audio") {
								endMC.l1.gotoAndStop(1);
							}
						}
						
						productsObject.push(productObject);
						
					}
				}
				
				numberOfProducts = productsObject.length;
				
				if (shot.colorPanel == "red") {
					greyMC.gotoAndStop(1);
					cover2MC.gotoAndStop(1);
					leftMC.gotoAndStop(1);
					rightMC.gotoAndStop(1);
					lineMC.line.gotoAndStop(1);
					whiteMC.visible = false;
					text1MC.dynText.color = text2MC.dynText.color = vanafMC.dynText.color = "#ffffff";
					greyMC.x = cover2MC.x = 601;
					white2MC.visible = true;
				}
				
				if (!dynamic.isImageEmpty(IMAGE_COUNT + 1) && !isGreenLabel) {
					dynamic.setDynamicImage(campaignMC, (IMAGE_COUNT + 1));
					campaignMC.scaleX = campaignMC.scaleY = 0;
					var moveLowerBy = 17;
					text1ToY += moveLowerBy;
					text2ToY += moveLowerBy;
					mask.y += moveLowerBy;
					mask_1.y += moveLowerBy;
					text1MC.y = text1ToY + 22;
					text2MC.y = text2ToY + 22;
					endMC.y += 19;
				}
				
				if (numberOfServices > 0) {
					buildServicesButton("BEKIJK ONZE SERVICES");
					//rightToX = servButtonToX - 19;
					//rightMC.x = rightToX - 25;
					var servButtonWidth = 140;
					servButtonMC.x = dynamic.width - servButtonWidth - 10;
					if (greyMC.currentFrame == 0) {
						//servButtonWidth -= 50;
					}
					//rightButtonDiv.style.right = servButtonWidth + "px";
					//productToX -= Math.round((dynamic.width - servButtonToX) / 3);
					var div = document.createElement("div");
					div.style.width = servButtonWidth + "px";
					div.style.height = "28px";
					div.style.right = "10px";
					div.style.top = "0px";
					div.id = "services";
					document.getElementById("dom_overlay_container").appendChild(div);
					document.getElementById("services").addEventListener("click", doServices, false);
					if (greyMC.currentFrame == 0) {
						//div.style.top = "10px";
					}
					ctaMC.y = cta2MC.y = 36;
					ctaButtonDiv.style.bottom = "30px";
					cta2MC.x = ctaToX - 200;
					/*
					for (i = 0; i<numberOfProducts; i++) {
						var m = productMC.children[i];
						if (m.green == true) {
							var sc = m.scaleX;
							sc *= 0.76;
							m.scaleX = m.scaleY = sc;
							m.x -= 7;
							m.y += 3;
							if (m.cashback == true) {
								m.y += 7;
							}
						}
					}
					*/
				}
				
				buildCta(shot.cta.split("|")[0], ctaMC);
				if (shot.cta.split("|")[1] != undefined) {
					buildCta(shot.cta.split("|")[1], cta2MC);
				}
				if (VARIATION == "dollar") {
					ctaMC.y += 23;
					ctaButtonDiv.style.bottom = "10px";
				}
				if (VARIATION == "merkenweek") {
					ctaMC.y += 28;
					ctaButtonDiv.style.bottom = "10px";
				}
				editCtaButtonDivWidth(ctaMC.ctaTextWidth);
				updateLogoTextsLabelPrice();
				animateInCreative();
				
				ctaButtonDiv.addEventListener("mouseover", doRollOver, false);
				ctaButtonDiv.addEventListener("mouseout", doRollOut, false);
				ctaButtonDiv.addEventListener("click", dynamic.onMouseClick.bind(dynamic));
				
				if (productsObject[productID] != "video" && VARIATION != "dollar") {
					
					//checkNumberOfProducts();
					
				}
				
			}
			
			//if (shot.isLastShot != true) {
		
		}
		
		function checkNumberOfProducts() {
						
			if (numberOfProducts > 1) {
				
				leftMC.visible = rightMC.visible = true;
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
				leftButtonDiv.addEventListener("click", doLeftRightClick, false);
				rightButtonDiv.addEventListener("click", doLeftRightClick, false);
				
				TweenMax.delayedCall(autoRotateTime + 1, doAutoRotate);
				
				enableSwipe();
				
			}
			
		}
		
		function enableSwipe() {
			swipedetect(document.getElementById("swipe_area"), function (swipedir) {
				self.cancelAutoRotate();
				var maxID = numberOfProducts - numberOfServices - 1;
				var minID = 0;
				if (productsObject[productID].service) {
					maxID = numberOfProducts - 1;
					minID = numberOfProducts - numberOfServices;
				} 
				if (swipedir == "right") {
					if (productID > minID) {
						productID--;
						bigJump = false;
					} else {
						bigJump = true;
						productID = maxID;
					}
					isLeftClick = true;
					changeProduct();
				}
				if (swipedir == "left") {
					if (productID < maxID) {
						productID++;
						bigJump = false;
					} else {
						bigJump = true;
						productID = minID;
					}
					isLeftClick = false;
					changeProduct();
				}
				if (swipedir == "click") {
					dynamic.onMouseClick();
				}
			});
		}
		
		function buildCta(ctaText, whichCtaMC) {
			
			var rightMargin = 20;
			var buttonHeight = 24;
			var textPadding = 7;
			
			var ctaTextField = whichCtaMC.dynText;
			ctaTextField.text = ctaText;
			var ctaRect = new createjs.Shape();
			var ctaTextWidth = Math.round(ctaTextField.getMeasuredWidth()) + textPadding * 2;
			ctaTextField.x = - rightMargin - ctaTextWidth + textPadding - 1;
			whichCtaMC.shapeMC.x = whichCtaMC.rollover1MC.x = - rightMargin - ctaTextWidth - 1;
			ctaRect.graphics.beginFill("#FAA332").drawRoundRectComplex(0, 0, ctaTextWidth, buttonHeight, 3, 0, 0, 3);
			whichCtaMC.shapeMC.addChild(ctaRect);
			
			var rolloverRect = new createjs.Shape();
			rolloverRect.graphics.beginFill("#E39126").drawRoundRectComplex(0, 0, ctaTextWidth, buttonHeight, 3, 0, 0, 3);
			whichCtaMC.rollover1MC.addChild(rolloverRect);
			
			whichCtaMC.ctaTextWidth = ctaTextWidth;
			
		}
		
		function editCtaButtonDivWidth(toWidth) {
				
			var rightMargin = 20;
			ctaButtonDiv.style.width = (toWidth + 1 + rightMargin) + "px";
			
		}
		
		function doRollOver(event) {
			
			TweenMax.to([ctaMC.rollover1MC, ctaMC.rollover2MC, cta2MC.rollover1MC, cta2MC.rollover2MC], 0.4, {alpha: 1.0});
			ctaHover = true;
			ctaTL.play();
			
		}
		
		function doRollOut(event) {
			
			TweenMax.to([ctaMC.rollover1MC, ctaMC.rollover2MC, cta2MC.rollover1MC, cta2MC.rollover2MC], 0.4, {alpha: 0.0});
			ctaHover = false;
			
		}
		
		var ctaTL = new TimelineMax({paused:true, onComplete: function(){
			if(ctaHover) ctaTL.restart();
		}});
		
		ctaTL.fromTo([ctaMC.arrow1, cta2MC.arrow1], 0.3, {x: -10}, {x: 4, ease: Expo.easeIn}, 0.0)
			 .fromTo([ctaMC.arrow2, cta2MC.arrow2], 0.3, {x: -13}, {x: 1, ease: Expo.easeIn}, 0.05)
			 .set([ctaMC.arrow1, cta2MC.arrow1], {x:-25})
			 .set([ctaMC.arrow2, cta2MC.arrow2], {x:-28})
			 .to([ctaMC.arrow1, cta2MC.arrow1], 0.5, {x: -10, ease: Expo.easeOut}, 0.35)
			 .to([ctaMC.arrow2, cta2MC.arrow2], 0.5, {x: -13, ease: Expo.easeOut}, 0.45);
		
		function doAutoRotate() {
			productID++;
			bigJump = false;
			changeProduct();
			if (productID < numberOfProducts - 1) {
				TweenMax.delayedCall(autoRotateTime, doAutoRotate);
			}
		}
		self.cancelAutoRotate = function() {
			TweenMax.killDelayedCallsTo(doAutoRotate);
		}
		
		function animateInCreative() {
			
			var d = 0.0;
			
			if (VARIATION != "dollar" && VARIATION !="merkenweek") {
				TweenMax.to(mmMC, 1.1, {y: 45, ease: Expo.easeInOut, delay: d});
				
				TweenMax.to(coverMC, 1.2, {y: 52, ease: Expo.easeInOut, delay: d, onComplete: function() {
					coverMC.visible = false;
				}});
				
				d += 1.05;
			}
			/*
			TweenMax.to(mmMC, 0.6, {rotation: 3, ease: Quart.easeIn, delay: d});
			TweenMax.to(mmMC, 0.6, {rotation: 0, ease: Expo.easeOut, delay: d + 0.6});
			TweenMax.to(mmMC, 1.2, {scaleX: 0.15, scaleY: 0.15, x: 79, y: 228, ease: Expo.easeInOut, delay: d});
			*/
			
			//d += 0.35;
			var greyToX = 15;
			if (greyMC.currentFrame == 1) {
				greyToX = 0;
			}
			//TweenMax.to(greyMC, 0.4, {x: greyToX + 80, ease: Expo.easeIn, delay: d});
			//TweenMax.to(greyMC, 0.8, {x: greyToX, ease: Expo.easeOut, delay: d + 0.4});
			//TweenMax.to(cover2MC, 0.4, {x: greyToX + 80, ease: Expo.easeIn, delay: d});
			//TweenMax.to(cover2MC, 0.8, {x: greyToX, ease: Expo.easeOut, delay: d + 0.4});
			
			TweenMax.to(mm2MC, 0.9, {x: mm2ToX, ease: Expo.easeOut, delay: d + 0.6});
			TweenMax.to(mm2MC, 0.8, {alpha: 1.0, delay: d + 0.6});
			TweenMax.to(self.counterMC, 0.5, {x: 645, ease: Expo.easeOut, delay: d + 5.6});
			
			
			d += 0.1;
			
			TweenMax.to(self.merkentext, 0.5, {x:130, alpha:1.0, ease:Sine.easeInOut, delay:d});
			d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:580, alpha:1.0,ease:Sine.easeInOut, delay:d});
			
			
			d += 2.1;
			
			TweenMax.to(self.merkentext, 0.5, {x:-1000, ease:Sine.easeInOut, delay:d});
			d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:1000, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			self.intro_img.scaleX=self.intro_img.scaleY=0.65;
			d += 0.5;
			
			TweenMax.to(self.intro_text, 0.5, {x:85,ease:Sine.easeInOut, delay:d});
			TweenMax.to(self.intro_img, 0.5, {x:560,ease:Sine.easeInOut, delay:d});
			
			d += 2.1;
			TweenMax.delayedCall(d, animateOutMerken);
			/*
			TweenMax.to(logoMC, 0.8, {y: logoToY, ease: Expo.easeOut, delay: d});
			TweenMax.to(logoMC, 0.5, {alpha: 1.0, delay: d});
			*/
			
			TweenMax.to(campaignMC, 0.5, {scaleX: campaignScale * 0.5, scaleY: campaignScale * 0.5, ease: Expo.easeIn, delay: d});
			TweenMax.to(campaignMC, 0.5, {scaleX: campaignScale, scaleY: campaignScale, ease: Elastic.easeOut.config(2.0, 1.0), delay: d + 0.5});
			TweenMax.from(campaignMC, 0.9, {rotation: -30, ease: Expo.easeInOut, delay: d});
			
			d += 0.3;
			
			d += 0.2;
			if (VARIATION == "dollar") {
				var d0ToY = 24;
				self.d0.y = d0ToY + 30;
				self.d1.y = 115 + 30;
				self.d2.y = d0ToY + 27;
				self.d2.alpha = 0;
				
				self.doll0.rotation = -10;
				self.doll1.rotation = -30;
				self.doll2.rotation = 20;
				self.doll0.scaleX = self.doll0.scaleY = 0.6 * 0.75;
				self.doll1.scaleX = self.doll1.scaleY = 0.7 * 0.75;
				self.doll2.scaleX = self.doll2.scaleY = 0.65 * 0.75;
				self.doll0.x = 340 + 10;
				self.doll1.x = 60 + 20;
				self.doll2.x = 605 + 30;
				
				TweenMax.from(self.d0, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
				TweenMax.to(self.d0, 0.8, {y: d0ToY, ease: Expo.easeOut, delay: d});
				
				d -= 0.2;
				TweenMax.to(self.doll0, 1.6, {x: 340, y: 80, scaleX: 0.6, scaleY: 0.6, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll0, 0.4, {rotation: 30, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll0, 0.6, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.2});
				
				d += 0.15;
				TweenMax.to(self.doll1, 2.1, {x: 60, y: 165, scaleX: 0.7, scaleY: 0.7, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll1, 0.5, {rotation: 20, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll1, 0.7, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.5});
			
				d += 0.15;
				TweenMax.to(self.doll2, 1.7, {x: 605, y: 25, scaleX: 0.65, scaleY: 0.65, ease: Sine.easeOut, delay: d});
				TweenMax.to(self.doll2, 0.4, {rotation: -30, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d});
				TweenMax.to(self.doll2, 0.7, {rotation: 0, ease: Cubic.easeOut, delay: d + 1.2});
				d -= 0.3;
				
				d += 0.2 + 0.2;
				TweenMax.from(self.d1, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
				TweenMax.to(self.d1, 0.8, {y: 115, ease: Expo.easeOut, delay: d});
				
				d += 0.2;
				TweenMax.from(self.d2, 0.7, {x: 323, ease: Expo.easeOut, delay: d});
				TweenMax.to(self.d2, 0.0, {alpha: 1.0, delay: d});
				
				d += 2.5;
				TweenMax.to(self.doll1, 0.65, {y: dynamic.height + 50, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.doll0, 0.7, {y: dynamic.height + 50, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d + 0.05});
				TweenMax.to(self.doll2, 0.8, {y: dynamic.height + 50, scaleX: 0.0, scaleY: 0.0, rotation: Math.floor(Math.random() * 60) - 30, ease: Expo.easeInOut, delay: d + 0.07});
				TweenMax.to(self.doll1, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.doll2, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.doll0, 0.5, {alpha: 0.0, delay: d});
				TweenMax.to(self.d1, 0.4, {y: 115 + 27, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.d1, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				TweenMax.to(self.d2, 0.3, {alpha: 0.0, delay: d});
				TweenMax.to(self.d2, 0.3, {y: "+= 20", ease: Expo.easeIn, delay: d});
				TweenMax.to(self.d2, 0.7, {rotation: 4, ease: Expo.easeInOut, delay: d});
				
				d += 0.075;
				TweenMax.to(self.d0, 0.4, {y: 92 + 27, ease: Expo.easeInOut, delay: d});
				TweenMax.to(self.d0, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
				
				d += 0.4;
			}
			
			if (VARIATION == "merkenweek" &&productID==0) {
			//leftMC.y -= 25;
			//rightMC.y -= 25;
				self.dollarcashback.scaleX=self.dollarcashback.scaleY=0.86;
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX0,y:45, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:5,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			//TweenMax.to(self.dollarcashback, .3, { scaleX: 0, ease: Power3.easeIn, delay: 2});
				
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0.86, scaleY: 0.86,  ease: Power3.easeOut, delay: 2.3});
		TweenMax.delayedCall(d, animateInManiaLabels);		
		}
			else if (VARIATION == "merkenweek") {
				
				TweenMax.delayedCall(d, animateInManiaLabels);
				
			}
			TweenMax.from(text1MC, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text1MC, 0.8, {y: text1ToY, ease: Expo.easeOut, delay: d});
			
			TweenMax.delayedCall(d - 1.5, checkNumberOfProducts);
			
			d += 0.2;
			TweenMax.from(text2MC, 0.9, {rotation: 7, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text2MC, 0.8, {y: text2ToY, ease: Expo.easeOut, delay: d});
			
			if (text3MC.dynText.text != "") {
				d += 0.3;
				TweenMax.to(text3MC, 0.7, {x: text3ToX, ease: Expo.easeOut, delay: d});
				TweenMax.to(text3MC, 1.0, {alpha: 1.0, delay: d});
				d -= 0.6;
			} else {
				d -= 0.3;
			}
			
			d += 0.2;
			TweenMax.to(productMC, 1.3, {x: productToX, ease: Expo.easeOut, delay: d});
			
			if (VARIATION == "dollar") {
				TweenMax.delayedCall(d + 0.1, animateInCash);
			}
			
			d += 0.2;
			TweenMax.to(leftMC, 0.9, {x: leftToX, ease: Expo.easeOut, delay: d});
			TweenMax.to(leftMC, 0.8, {alpha: 1.0, delay: d});
			TweenMax.to(rightMC, 0.9, {x: rightToX, ease: Expo.easeOut, delay: d});
			TweenMax.to(rightMC, 0.8, {alpha: 1.0, delay: d});
			TweenMax.delayedCall(d, animateInLabels);
			
			if (servButtonToX >= 0) {
				d += 0.3;
				TweenMax.to(servButtonMC, 0.6, {y: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: d});
				d -= 0.3;
			}
			
			d += 0.2;
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				TweenMax.delayedCall(d, animateInPrice);
			}
			
			TweenMax.delayedCall(d, animateInLabels);
			
			d += 0.2;
			TweenMax.delayedCall(d, animateInLabels2);
			
			//if (productsObject[productID].label == true) {
			if (self.cashback.dynText.text.length > 0) {
				d += 0.2;
				TweenMax.to(self.cashback, 0.6, {x: labelToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: d});
			}
			
			d += -0.2;
			TweenMax.to(ctaMC, 0.7, {alpha: 1.0, delay: d});
			TweenMax.to(ctaMC, 0.8, {x: ctaToX, ease: Expo.easeOut, delay: d});
			
		}
		
		function doLeftRightClick(event) {
			//console.log(event.target)
			var maxID = numberOfProducts - numberOfServices - 1;
			var minID = 0;
			if (productsObject[productID].service) {
				maxID = numberOfProducts - 1;
				minID = numberOfProducts - numberOfServices;
			} 
			if (event.target.name == "left" || event.target == leftButtonDiv) {
				if (productID > minID) {
					productID--;
					bigJump = false;
				} else {
					bigJump = true;
					productID = maxID;
				}
				isLeftClick = true;
			} else {
				if (productID < maxID) {
					productID++;
					bigJump = false;
				} else {
					bigJump = true;
					productID = minID;
				}
				isLeftClick = false;
			}
			self.cancelAutoRotate();
			changeProduct();
		}
		
		function adjustTextHeader() {
			
			var H1TextField = text1MC.dynText;
			var H2TextField = text2MC.dynText;
			
			var headerText1Width = H1TextField.getMeasuredWidth();
			var headerText2Width = H2TextField.getMeasuredWidth();
			/*
			if (H2TextField.getMeasuredWidth() > headerTextWidth) {
				headerTextWidth = H2TextField.getMeasuredWidth();
			}
			*/
			var idealHeaderTextWidth = 180;
			
			var headerFontScale = 1.0;
			if (headerText1Width > idealHeaderTextWidth) {
				headerFontScale = 1 / (headerText1Width / idealHeaderTextWidth);
			}
			text1MC.scaleX = text1MC.scaleY = headerFontScale;
			
			headerFontScale = 1.0;
			if (headerText2Width > idealHeaderTextWidth) {
				headerFontScale = 1 / (headerText2Width / idealHeaderTextWidth);
			}
			text2MC.scaleX = text2MC.scaleY = headerFontScale;
			
		}
		
		function updateLogoTextsLabelPrice() {
			
			text3MC.x = text3ToX + 30;
			text1MC.dynText.text = productsObject[productID].name;
			text2MC.dynText.text = productsObject[productID].main_specs;
			//text3MC.dynText.text = productsObject[productID].sub_specs;
			adjustTextHeader();
			
			logoMC.y = - 70;
			labelMC.x = dynamic.width + 50;
			self.cashback.x = labelMC.x;
			for (var i = 0; i<numberOfProducts; i++) {
				if (i == productID && logoMC.children[i] != undefined) {
					logoMC.children[i].alpha = labelMC.children[i].alpha = 1.0;
				} else if (logoMC.children[i] != undefined) {
					logoMC.children[i].alpha = labelMC.children[i].alpha = 0.0;
				}
			}
			
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf([a, a.gr.l0, a.gr.l1, a.gr]);
				self["g" + i].text.gotoAndStop(0);
			}
			if (productsObject[productID].sub_specs != undefined && productsObject[productID].sub_specs != "") {
				var s = productsObject[productID].sub_specs.split("^");
				if (s[0].split("_")[0].toLowerCase() == "bullet") {
					for (i = 0; i<maxGreen; i++) {
						var ID = 0;
						var a = self["g" + i];
						a.x = gToX;
						a.y = a.toY + 25;
						a.gr.l0.scaleX = a.gr.l1.scaleX = 0;
						a.gr.scaleX = a.gr.scaleY = 0.0;
						if (s[i] != undefined && s[i].split("_")[1] != undefined) {
							switch (s[i].split("_")[1].toLowerCase()) {
								case "bezorging":
								ID = 1;
								break;
								
								case "aansluiting":
								ID = 2;
								break;
								
								case "oud":
								ID = 3;
								break;
								
								case "retouneren":
								ID = 4;
								break;
								
								case "30dagen":
								ID = 5;
								break;
								
								case "plaatsing":
								ID = 5;
								break;
							}
						}
						a.text.gotoAndStop(ID);
					}
				}
			}
			
			var mu = 0;
			if (productMC.children[productID].green == true) {
				mu = 20;
			}
			mask.y = 14 - mu;
			mask_1.y = 2 - mu;
			text1ToY = 23 - mu;
			text2ToY = 26.5 - mu;
			text1MC.y = text1ToY + 22;
			text2MC.y = text2ToY + 22;
			
			self.servicelabel.gotoAndStop(0);
			self.cashback.dynText.text = "";
			while (self.cashback.bgr.numChildren > 0) {
				self.cashback.bgr.removeChild(self.cashback.bgr.children[0]);
			}
			var z = productsObject[productID].label_new;
			var lab2W = 0;
			if (z != undefined && z != "") {
				if (z.split("_")[0].toLowerCase() == "cashback") {
					self.cashback.dynText.text = z.split("_")[1];
					lab2W = Math.floor(self.cashback.dynText.getMeasuredWidth()) + 11;
					if (self.cashback.dynText.getMeasuredHeight() > 11) {
						lab2W = 65;
					}
				} else if (z.split("_")[0].toLowerCase() == "service") {
					var b = z.split("_")[1].toLowerCase();
					var ID = 0;
					switch (b) {
						case "a": 
						ID = 1;
						break;
						
						case "b": 
						ID = 2;
						break;
						
						case "c": 
						ID = 3;
						break;
						
						case "d": 
						ID = 4;
						break;
						
						case "e": 
						ID = 5;
						break;
						
						case "f": 
						ID = 6;
						break;
						
						case "g": 
						ID = 7;
						break;
						
						case "h": 
						ID = 8;
						break;
						
						case "i": 
						ID = 9;
						break;
					}
					self.servicelabel.gotoAndStop(ID);
				}
			}
			/*
			if (self.servicelabel.currentFrame == 9) {
				self.servicelabel.y = 154;
			} else {
				self.servicelabel.y = 63;
			}
			*/
			/*
			if (self.cashback.dynText.text.length > 0) {
				var cashRect = new createjs.Shape();
				cashRect.graphics.beginFill("#e90000").drawRoundRectComplex(0, 0, lab2W, Math.floor(self.cashback.dynText.getMeasuredHeight()) + 7, 6, 6, 6, 6);
				self.cashback.bgr.addChild(cashRect);
			}
			*/
			
			lineMC.x = lineToX;
			TweenMax.killTweensOf(lineMC.line);
			lineMC.line.scaleX = 0;
			lineMC.alpha = 1.0;
			priceMC.x = priceToX;
			priceMC.alpha = 1.0;
			vanafMC.dynText.text = "";
			vanafMC.x = vanafToX + 15;
			self.prijs.x = prijsToX + 20;
			while (priceMC.numChildren > 0) {
				for (var k = 0; k<priceMC.children[0].numChildren; k++) {
					TweenMax.killTweensOf(priceMC.children[0].children[k]);
					for (var h = 0; h<priceMC.children[0].children[k].numChildren; h++) {
						TweenMax.killTweensOf(priceMC.children[0].children[k].children[h]);
					}
				}
				priceMC.removeChild(priceMC.children[0]);
			}
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				var newPriceMC = new createjs.MovieClip();
				priceMC.addChild(newPriceMC);
				var priceStr = productsObject[productID].price.split(" ");
				if (priceStr.length > 1) {
					vanafMC.dynText.text = priceStr[0].toUpperCase();
					priceStr = priceStr[1].split("");
				} else {
					priceStr = priceStr[0].split("");
				}
				for (i = 0; i<priceStr.length; i++) {
					var childMC = new createjs.MovieClip();
					newPriceMC.addChild(childMC);
					
					var charMC = new createjs.MovieClip();
					//var maskRect = new createjs.Shape();
					childMC.addChild(charMC);
					if (priceStr[i] != ".") {
						var numberID = Number(priceStr[i]) + 1;
						if (greyMC.currentFrame == 1) {
							numberID += 12;
						}
						dynamic.setDynamicImage(charMC, (NUMBERS + numberID), 0, 0);
					} else {
						var numbi = IMAGE_COUNT - 1;
						if (greyMC.currentFrame == 0) {
							numbi -= 12;
						}
						dynamic.setDynamicImage(charMC, (numbi), 0, 0);
					}
					if (i > 0) {
						childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale) - 7;
						if (priceStr[(i - 1)] != "." && newPriceMC.dot == true) {
							childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.7) - 5;
						}
					} else {
						childMC.toX = 0;
					}
					childMC.scaleX = childMC.scaleY = numberImgScale;
					if (newPriceMC.dot) {
						childMC.scaleX = childMC.scaleY = numberImgScale * 0.7;
						childMC.y += 0;
					}
					if (priceStr[i] == ".") {
						childMC.toX -= 0;
						newPriceMC.dot = true;
					}
					if (priceStr[(i - 1)] == ".") {
						childMC.toX -= 3;
					}
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					
				}
				if (!newPriceMC.dot) {
					var childMC = new createjs.MovieClip();
					newPriceMC.addChild(childMC);
					var charEndingMC = new createjs.MovieClip();
					childMC.addChild(charEndingMC);
					var numbi = IMAGE_COUNT;
					if (greyMC.currentFrame == 0) {
						numbi -= 12;
					}
					dynamic.setDynamicImage(charEndingMC, (numbi), 0, 0);
					childMC.toX = newPriceMC.children[i - 1].toX + Math.round(newPriceMC.children[i - 1].getBounds().width * numberImgScale) - 8;
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					childMC.scaleX = childMC.scaleY = numberImgScale;
				}
			}
			
			if (productsObject[productID].priceOld != "" && productsObject[productID].priceOld != "-" && !isNaN(productsObject[productID].priceOld) && newPriceMC != undefined) {
				newPriceMC.oldPrice = true;
				var oldPriceMC = new createjs.MovieClip();
				newPriceMC.addChild(oldPriceMC);
				var oldPriceStr = productsObject[productID].priceOld.split("");
				oldPriceMC.y = -30;
				oldPriceMC.x += 2;
				for (i = 0; i<oldPriceStr.length; i++) {
					var childMC = new createjs.MovieClip();
					oldPriceMC.addChild(childMC);
					var charMC = new createjs.MovieClip();
					//var maskRect = new createjs.Shape();
					childMC.addChild(charMC);
					if (oldPriceStr[i] != ".") {
						var numberID = Number(oldPriceStr[i]) + 1;
						dynamic.setDynamicImage(charMC, (NUMBERS + numberID), 0, 0);
					} else {
						dynamic.setDynamicImage(charMC, (IMAGE_COUNT - 1 - 12), 0, 0);
					}
					if (i > 0) {
						childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.6) - 7 * 0.6;
						if (oldPriceStr[(i - 1)] != "." && oldPriceMC.dot == true) {
							childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.7 * 0.6) - 5 * 0.6;
						}
					} else {
						childMC.toX = 0;
					}
					childMC.scaleX = childMC.scaleY = numberImgScale * 0.6;
					if (oldPriceMC.dot) {
						childMC.scaleX = childMC.scaleY = numberImgScale * 0.7 * 0.6;
						childMC.y += 1;
					}
					if (oldPriceStr[i] == ".") {
						childMC.toX -= 1;
						oldPriceMC.dot = true;
					}
					if (oldPriceStr[(i - 1)] == ".") {
						childMC.toX -= 2;
					}
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
				}
				if (!oldPriceMC.dot) {
					var childMC = new createjs.MovieClip();
					oldPriceMC.addChild(childMC);
					var charEndingMC = new createjs.MovieClip();
					childMC.addChild(charEndingMC);
					dynamic.setDynamicImage(charEndingMC, (IMAGE_COUNT - 12), 0, 0);
					childMC.toX = oldPriceMC.children[i - 1].toX + Math.round(oldPriceMC.children[i - 1].getBounds().width * numberImgScale * 0.6) - 8 * 0.6;
					childMC.x = childMC.toX + 20;
					childMC.alpha = 0;
					childMC.scaleX = childMC.scaleY = numberImgScale * 0.6;
				}
				oldPriceMC.priceWidth = oldPriceMC.children[oldPriceMC.numChildren - 1].toX + 9;
				if (oldPriceMC.dot == true) {
					oldPriceMC.priceWidth += 7;
				}
			}
		}
		
		
		function animateInPrice() {
			var currentPrice = priceMC.children[priceMC.numChildren - 1];
			var indexTill = currentPrice.numChildren;
			var delayPrice = 0.0;
			TweenMax.killTweensOf(lineMC.line);
			lineMC.line.scaleX = 0.0;
			lineMC.scaleY = 0.8;
			if (currentPrice.oldPrice == true) {
				indexTill--;
				lineMC.scaleX = 1 / (90 / currentPrice.children[currentPrice.numChildren - 1].priceWidth);
				for (var j = 0; j<currentPrice.children[currentPrice.numChildren - 1].numChildren; j++) {
					var oldNumberMC = currentPrice.children[currentPrice.numChildren - 1].children[j];
					TweenMax.to(oldNumberMC, 0.8, {x: oldNumberMC.toX, alpha: 1.0, ease: Expo.easeOut, delay: j * 0.03});
				}
				delayPrice = 0.3;
				TweenMax.to(lineMC.line, 0.7, {scaleX: 1.0, ease: Expo.easeOut, delay: 0.15});
			}
			for (var i = 0; i<indexTill; i++) {
				var numberMC = currentPrice.children[i];
				TweenMax.to(numberMC, 0.9, {x: numberMC.toX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + i * 0.03});
			}
			TweenMax.to(vanafMC, 0.9, {x: vanafToX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + 0});
			if (VARIATION == "dollar" && productID == 1) {
				TweenMax.to(self.prijs, 0.9, {x: prijsToX, alpha: 1.0, ease: Expo.easeOut, delay: delayPrice + 0});
			}
		}
		
		function changeProduct() {
			
			dynamic.productID = productID;
			
			var d = 0.0;
			TweenMax.killDelayedCallsTo(animateInManiaLabels);
			TweenMax.killDelayedCallsTo(updateLogoTextsLabelPrice);
			TweenMax.killDelayedCallsTo(animateInPrice);
			TweenMax.killDelayedCallsTo(editServicesButton);
			TweenMax.killDelayedCallsTo(animateInEndframe);
			TweenMax.killDelayedCallsTo(animateOutEndframe);
			TweenMax.killDelayedCallsTo(animateInLabels);
			TweenMax.killDelayedCallsTo(animateInLabels2);
			TweenMax.killDelayedCallsTo(animateInCheck);
			TweenMax.killTweensOf([text1MC, text2MC, text3MC, productMC, logoMC, labelMC, self.cashback, self.servicelabel, priceMC, lineMC, lineMC.line, ctaMC, cta2MC, vanafMC, servButtonMC, leftMC, rightMC, self.prijs, self.dollarcashback, self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3, self.subsubtext]);
			TweenMax.to([logoMC, self.cashback, priceMC, lineMC, vanafMC, self.subsubtext], 0.3, {alpha: 0.0});
			var moveBy = 100;
			if (isLeftClick == false || isLeftClick == undefined) {
				moveBy = -moveBy;
			}
			//TweenMax.to(labelMC, 0.4, {x: "+=" + (moveBy + 50), ease: Expo.easeIn});
			//TweenMax.to(self.cashback, 0.4, {x: "+=" + (moveBy + 50), ease: Expo.easeIn});
			TweenMax.to(text3MC, 0.3, {x: text3ToX + moveBy, ease: Expo.easeIn, delay: 0.05});
			TweenMax.to(priceMC, 0.4, {x: priceToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			TweenMax.to(vanafMC, 0.4, {x: vanafToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			TweenMax.to(lineMC, 0.4, {x: lineToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
			if (VARIATION == "dollar") {
				TweenMax.to([self.prijs, self.dollarcashback, self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.3, {alpha: 0.0});
				TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			if (VARIATION == "merkenweek") {
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:5,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			if (VARIATION == "merkenweek" &&productID==0) {
			//leftMC.y -= 25;
			//rightMC.y -= 25;
			//self.dollarcashback.scaleX=self.dollarcashback.scaleY=0.86;
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: 150,y:45, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:5,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			//TweenMax.to(self.dollarcashback, .3, { scaleX: 0, ease: Power3.easeIn, delay: 1.5});
				
		//TweenMax.to(self.dollarcashback, .3, { scaleX: 0.86, scaleY:0.86, ease: Power3.easeOut, delay: 1.8});
				}
			if (VARIATION == "merkenweek" &&productID==3) {
			//leftMC.y -= 25;
			//rightMC.y -= 25;
		
				//TweenMax.to(self.dollarcashback, 0.3, {alpha: 0.0});
				//TweenMax.to(self.prijs, 0.4, {x: prijsToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to(self.dollarcashback, 0.4, {x: dollarCashbackToX + moveBy - 20,y:dollarCashbackToY, ease: Expo.easeIn, delay: 0.0});
				TweenMax.to(self.subsubtext, 0.4, {x: maniaLabelToX + moveBy - 20,y:5,ease: Expo.easeIn, delay: 0.0});
				//TweenMax.to([self.dollProd0, self.dollProd1, self.dollProd2, self.dollProd3], 0.4, {x: 100 + moveBy - 20, ease: Expo.easeIn, delay: 0.0});
			}
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf(a);
				if (a.text.currentFrame > 0) {
					TweenMax.to(a, 0.4, {x: gToX + moveBy - 50, ease: Expo.easeIn, delay: 0.0});
					TweenMax.to(a, 0.3, {alpha: 0.0, delay: 0.0});
				}
			}
			if (productsObject[productID].service && servButtonMC.dynText.text == "BEKIJK ONZE SERVICES") {
				TweenMax.to(servButtonMC, 0.4, {y: -40, alpha: 0.0, ease: Expo.easeIn, delay: 0});
				TweenMax.to(servButtonMC, 0.6, {y: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0.4});
				TweenMax.to(servButtonMC, 0.4, {alpha: 1.0, delay: 0.4});
				TweenMax.delayedCall(0.4, editServicesButton, ["BEKIJK ONZE DEALS"]);
			}
			if (!productsObject[productID].service && servButtonMC.dynText.text == "BEKIJK ONZE DEALS") {
				TweenMax.to(servButtonMC, 0.4, {y: -40, ease: Expo.easeIn, alpha: 0.0, delay: 0});
				TweenMax.to(servButtonMC, 0.6, {y: servButtonToX, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0.4});
				TweenMax.to(servButtonMC, 0.4, {alpha: 1.0, delay: 0.4});
				TweenMax.delayedCall(0.4, editServicesButton, ["BEKIJK ONZE SERVICES"]);
			}
			
			TweenMax.to(text3MC, 0.3, {alpha: 0.0});
			
			var productTargetX = productToX - productID * dynamic.width;
			if (bigJump == true) {
				var productTargetPrevX = productTargetX - 75;
				if (!(productID == 0) && prevProductID <= numberOfProducts - numberOfServices - 1) {
					productTargetPrevX += 150;
				} else if (productID == numberOfProducts - 1) {
					productTargetPrevX += 150;
				}
				TweenMax.to(productMC, 0.8, {x: productTargetPrevX, ease: Expo.easeIn, delay: d});
				TweenMax.to(productMC, 0.6, {x: productTargetX, ease: Expo.easeOut, delay: d + 0.8});
			} else {
				TweenMax.to(productMC, 1.2, {x: productTargetX, ease: Expo.easeInOut, delay: d});
			}
			prevProductID = productID;
			
			if (numberOfServices > 0) {
				var whichCtaMC = ctaMC;
				if (productsObject[productID].service) {
					TweenMax.to(ctaMC, 0.9, {x: ctaToX + dynamic.width / 2, ease: Expo.easeInOut, delay: d});
					TweenMax.to(ctaMC, 0.4, {alpha: 0.0, delay: d});
					TweenMax.to(cta2MC, 0.9, {x: ctaToX, ease: Expo.easeInOut, delay: d});
					TweenMax.to(cta2MC, 0.9, {alpha: 1.0, delay: d + 0.4});
					whichCtaMC = cta2MC;
				} else {
					TweenMax.to(ctaMC, 0.9, {x: ctaToX, ease: Expo.easeInOut, delay: d});
					TweenMax.to(ctaMC, 1.3, {alpha: 1.0, delay: d});
					TweenMax.to(cta2MC, 0.9, {x: ctaToX - 200, ease: Expo.easeInOut, delay: d});
					TweenMax.to(cta2MC, 0.5, {alpha: 0.0, delay: d});
				}
				editCtaButtonDivWidth(whichCtaMC.ctaTextWidth);
			}
			
			TweenMax.to(self.servicelabel, 0.4, {y: -100, ease: Expo.easeIn, delay: d});
			
			TweenMax.to(text2MC, 0.4, {y: text2ToY + 22, ease: Expo.easeInOut, delay: d});
			TweenMax.to(text2MC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			
			d += 0.075;
			TweenMax.to(text1MC, 0.4, {y: text1ToY + 22, ease: Expo.easeInOut, delay: d});
			TweenMax.to(text1MC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			
			d += 0.4;
			TweenMax.delayedCall(d, updateLogoTextsLabelPrice);
			
			if (typeof(productsObject[productID]) == "string") {
				TweenMax.to(productMC, 0.3, {alpha: 0.0, delay: d - 0.3});
				TweenMax.delayedCall(d, animateInEndframe);
				//leftToX = 17;
				//rightToX = 465;
			} else {
				TweenMax.to(productMC, 0.6, {alpha: 1.0, delay: d + 0.2});
				TweenMax.delayedCall(d - 0.5, animateOutEndframe);
				//leftToX = 225;
				//rightToX = 410;
			}
			if (productsObject[productID].service) {
				if (numberOfServices <= 1) {
					leftButtonDiv.style.display = rightButtonDiv.style.display = "none";
					TweenMax.to(leftMC, 0.8, {x: leftToX + 15, ease: Expo.easeOut, delay: d - 0.2});
					TweenMax.to(rightMC, 0.8, {x: rightToX - 15, ease: Expo.easeOut, delay: d - 0.2});
					TweenMax.to(leftMC, 0.4, {alpha: 0, delay: d - 0.4});
					TweenMax.to(rightMC, 0.4, {alpha: 0, delay: d - 0.4});
				} else {
					leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
					TweenMax.to(leftMC, 0.8, {x: leftToX, alpha: 1.0, ease: Expo.easeOut, delay: d});
					TweenMax.to(rightMC, 0.8, {x: rightToX, alpha: 1.0, ease: Expo.easeOut, delay: d});
				}
			} else if (numberOfProducts > 1) {
				TweenMax.to(leftMC, 0.8, {x: leftToX, alpha: 1, ease: Expo.easeOut, delay: d});
				TweenMax.to(rightMC, 0.8, {x: rightToX, alpha: 1, ease: Expo.easeOut, delay: d});
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
			}
			leftButtonDiv.style.left = (leftToX - 11) + "px";
			rightButtonDiv.style.left = (rightToX - 4) + "px";
			/*
			TweenMax.to(logoMC, 0.8, {y: logoToY, ease: Expo.easeOut, delay: d});
			TweenMax.to(logoMC, 0.5, {alpha: 1.0, delay: d});
			*/
			
			if (VARIATION == "dollar") {
				TweenMax.delayedCall(d + 0.1, animateInCash);
			}
			if (VARIATION == "merkenweek") {
				//TweenMax.delayedCall(d + 0.1, animateInStripe);
				TweenMax.delayedCall(d, animateInManiaLabels);
			}
			
			//d += 0.1;
			TweenMax.delayedCall(d, animateInTexts);
			
			if (text3MC.dynText.text != "") {
				d += 0.1;
				TweenMax.to(text3MC, 0.7, {x: text3ToX, ease: Expo.easeOut, delay: d});
				TweenMax.to(text3MC, 1.0, {alpha: 1.0, delay: d});
			}
			
			if (productsObject[productID].price != "" && productsObject[productID].price != "-" && productsObject[productID].price != undefined) {
				d += 0.2;
				TweenMax.delayedCall(d, animateInPrice);
			}
			
			TweenMax.delayedCall(d, animateInLabels);
			
			//if (productsObject[productID].label == true) {
			d += 0.2;
			TweenMax.delayedCall(d, animateInLabels2);
			
		}
		function animateInManiaLabels() {
			var k = self.subsubtext;
			k.x = maniaLabelToX;
			k.alpha = 1.0;
			k.gotoAndStop(productID);
			var s = 0;
			while (k["m" + productID + s] != undefined) {
				s++;
			}
			var howMany = s;
			for (var i = 0; i<howMany; i++) {
				var n = k["m" + productID+i]
				console.log(n)
				TweenMax.from(n, 0.8, {x: 50, alpha: 0.0, delay: i * 0.05});
			}
		}
		function animateInCash() {
			var cash = self["dollProd" + productID];
			cash.rotation = -10;
			var toX = [260, 235, 225, 365];
			var toY = [60, 70, 70, 40];
			var d = 0.0;
			cash.x = toX[productID];
			cash.y = toY[productID];
			cash.scaleX = cash.scaleY = 0.0;
			cash.alpha = 1.0;
			//cash.x = dynamic.width + 50;
			//TweenMax.to(cash, 1.0, {y: toY[productID], ease: Sine.easeOut, delay: d});
			TweenMax.to(cash, 0.2, {rotation: 10, ease: Sine.easeInOut, repeat: 5, yoyo: true, delay: d + 0.5});
			TweenMax.to(cash, 0.8, {rotation: 0, ease: Elastic.easeOut.config(1.0, 1.0), delay: d + 1.1});
			TweenMax.to(cash, 1.0, {scaleX: 0.7, scaleY: 0.7, ease: Elastic.easeOut.config(1.0, 0.8), delay: d + 0.5});
			
			d += 0.1;
			var order = [1, 0, 2];
			for (var i = 0; i<3; i++) {
				var cash2 = self["dollRandom" + productID + i];
				cash2.rotation = Math.floor(Math.random() * 60) - 30;
				cash2.scaleX = cash2.scaleY = Math.floor(Math.random() * 40) / 100 + 0.5;
				var toRot = -cash2.rotation;
				cash2.y = -50;
				cash2.x = 220 + i * 100 + Math.floor(Math.random() * 50);
				var time = Math.floor(Math.random() * 10) / 10 + 1.2;
				TweenMax.to(cash2, time, {x: "-= 30", y: dynamic.height + 30, ease: Sine.easeOut, delay: d + order[i] * 0.3});
				TweenMax.to(cash2, 0.5, {rotation: toRot, ease: Sine.easeInOut, repeat: 6, yoyo: true, delay: d + order[i] * 0.3});
				//TweenMax.to(cash2, 0.6, {rotation: 0, ease: Cubic.easeOut, delay: d + i * 0.1 + 1.2});
			}
		}
		
		function animateInTexts() {
			var d = 0.0;
			TweenMax.to(text1MC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text1MC, 0.8, {y: text1ToY, ease: Expo.easeOut, delay: d});
			
			d += 0.2;
			TweenMax.to(text2MC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
			TweenMax.to(text2MC, 0.8, {y: text2ToY, ease: Expo.easeOut, delay: d});
		}
		
		function animateOutMerken() {
			
		
			var d = 0.0;
			TweenMax.to(self.merkentext, 0.5, {x:-300, ease:Sine.easeInOut, delay:d});
			d += 0.1;
			TweenMax.to(self.logobalk, 0.5, {x:-300, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			d += 0.1;
			TweenMax.to(self.intro_img, 0.5, {x:1000, alpha:0.0,ease:Sine.easeInOut, delay:d});
			TweenMax.to(self.intro_text, 0.5, {x:-300, alpha:0.0,ease:Sine.easeInOut, delay:d});
			
			d += 0.5;
			//self.manual_counter_mc.scaleX = self.manual_counter_mc.scaleY = 0.13;
			//TweenMax.to(self.manual_counter_mc, 0.5, {x:265, y:180,ease:Sine.easeInOut, delay:d});
		}
		function buildServicesButton(caption) {
			
			var servTextField = servButtonMC.dynText;
			editServicesButton(caption);
			var servButtonWidth = 140;
			var servRect = new createjs.Shape();
			servRect.graphics.beginFill("#ffffff").drawRoundRectComplex(0, -10, 140, 38, 0, 0, 10, 10);
			servButtonMC.addChild(servRect);
			servButtonMC.swapChildren(servButtonMC.children[0], servButtonMC.children[1]);
			servButtonToX = 0;
			if (greyMC.currentFrame == 1) {
				//servButtonToX -= 10;
			}
			
		}
		
		function editServicesButton(caption) {
			
			var servTextField = servButtonMC.dynText;
			servTextField.text = caption;
		
		}
		
		function doServices() {
			self.cancelAutoRotate();
			if (servButtonMC.dynText.text == "BEKIJK ONZE DEALS") {
				productID = 0;
				leftButtonDiv.style.display = rightButtonDiv.style.display = "block";
			} else {
				productID = numberOfProducts - numberOfServices;
			}
			bigJump = true;
			changeProduct();
		}
		
		function animateInEndframe() {
			var d = 0.0;
			for (var i = 0; i<lines; i++) {
				var lMC = endMC["l" + i];
				d = i * 0.12;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.9, {rotation: 0, ease: Cubic.easeOut, delay: d});
				TweenMax.to(lMC, 0.8, {y: lMC.toY, ease: Expo.easeOut, delay: d});
			}
		}
		
		function animateOutEndframe() {
			var d = 0.0;
			for (var i = 0; i<lines; i++) {
				var lMC = endMC["l" + i];
				d = (lines - i) * 0.05;
				TweenMax.killTweensOf(lMC);
				TweenMax.to(lMC, 0.4, {y: lMC.toY + 21, ease: Expo.easeInOut, delay: d});
				TweenMax.to(lMC, 0.4, {rotation: 7, ease: Cubic.easeInOut, delay: d});
			}
		}
		
		function animateInLabels() {
			for (i = 0; i<maxGreen; i++) {
				var a = self["g" + i];
				TweenMax.killTweensOf([a, a.gr]);
				if (a.text.currentFrame > 0) {
					TweenMax.delayedCall(0 + i * 0.1, animateInCheck, [a]);
					TweenMax.to(a.gr, 0.6, {scaleX: 1.0, scaleY: 1.0, ease: Elastic.easeOut.config(1.0, 1.0), delay: 0 + 0.1 + i * 0.075});
					TweenMax.to(a, 1.0, {y: a.toY, alpha: 1.0, ease: Expo.easeOut, delay: 0 + i * 0.075});
				}
			}
		}
		function animateInCheck(movieclip) {
			TweenMax.to(movieclip.gr.l0, 0.3, {scaleX: 1.0, ease: Expo.easeIn});
			TweenMax.to(movieclip.gr.l1, 0.5, {scaleX: 1.0, ease: Expo.easeOut, delay: 0.3});
		}
		function animateInLabels2() {
			if (self.cashback.dynText.text.length > 0) {
				TweenMax.to(self.cashback, 0, {alpha: 1.0});
				TweenMax.to(self.cashback, 0.6, {x: labelToX, ease: Elastic.easeOut.config(0.8, 1.2)});
			} else if (self.servicelabel.currentFrame > 0) {
				//TweenMax.to(self.servicelabel, 0.6, {y: 10, ease: Expo.easeOut});
			}
			if (VARIATION == "dollar") {
				var animIn = true;
				self.dollarcashback.alpha = 1.0;
				self.dollarcashback.x = dynamic.width + 150;
				if (productID == 1) {
					self.dollarcashback.text.gotoAndStop(0);
				} else if (productID == 2) {
					self.dollarcashback.text.gotoAndStop(1);
				} else {
					animIn = false;
				}
				if (animIn == true) {
					TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
			}
		if (VARIATION == "merkenweek") {
				var animIn = true;
				//self.dollarcashback.alpha = 1.0;
				//self.dollarcashback.x = dynamic.width+150;
				
				if (productID == 0) {
					self.dollarcashback.text.gotoAndStop(1);
					self.dollarcashback.scaleX=self.dollarcashback.scaleY=0.86;
					//TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX0, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				TweenMax.delayedCall(0.95, gotoLastFrame);
				} 
				else if (productID == 3) {
					self.dollarcashback.text.gotoAndStop(0);
					self.dollarcashback.scaleX=self.dollarcashback.scaleY = 0.9;
				} else {
					animIn = false;
					//self.dollarcashback.text.gotoAndStop(0);
				}
				if (animIn == true && productID == 0) {
					//TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX0, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
				else if (animIn == true && productID == 3) {
					//TweenMax.to(self.dollarcashback, 0.6, {x: dollarCashbackToX, ease: Elastic.easeOut.config(0.8, 1.2), delay: 0.2});
				}
			}
			
		}
		function gotoLastFrame(){
			
			self.dollarcashback.text.gotoAndStop(2);
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// border
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape.setTransform(364.0102,89.4776,2.1012,0.04);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_1.setTransform(364.098,0.5248,2.1011,0.04);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_2.setTransform(0.523,44.9768,0.0028,4.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("A7zB9IAAj5MA3nAAAIAAD5g");
	this.shape_3.setTransform(727.5231,44.9768,0.0028,4.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// counter
	this.counterMC = new lib.counterMC();
	this.counterMC.name = "counterMC";
	this.counterMC.parent = this;
	this.counterMC.setTransform(553.4,2);

	this.timeline.addTween(cjs.Tween.get(this.counterMC).wait(1));

	// elements_over
	this.dollProd3 = new lib.dollar_4();
	this.dollProd3.name = "dollProd3";
	this.dollProd3.parent = this;
	this.dollProd3.setTransform(85.1,-40,1,1,0,0,0,0.1,0);

	this.dollProd2 = new lib.dollar_5();
	this.dollProd2.name = "dollProd2";
	this.dollProd2.parent = this;
	this.dollProd2.setTransform(80,-200);

	this.dollProd0 = new lib.dollar_1_v2();
	this.dollProd0.name = "dollProd0";
	this.dollProd0.parent = this;
	this.dollProd0.setTransform(100,-129.7);

	this.doll2 = new lib.dollar_3();
	this.doll2.name = "doll2";
	this.doll2.parent = this;
	this.doll2.setTransform(350,-100);

	this.doll1 = new lib.dollar_2();
	this.doll1.name = "doll1";
	this.doll1.parent = this;
	this.doll1.setTransform(200,-60);

	this.doll0 = new lib.dollar_1();
	this.doll0.name = "doll0";
	this.doll0.parent = this;
	this.doll0.setTransform(80,-70);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.doll0},{t:this.doll1},{t:this.doll2},{t:this.dollProd0},{t:this.dollProd2},{t:this.dollProd3}]}).wait(1));

	// text2Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A3bFUIAAjwMAu3AAAIAADwg");
	mask.setTransform(149.9995,34.0082);

	// text2
	this.text2 = new lib.text2();
	this.text2.name = "text2";
	this.text2.parent = this;
	this.text2.setTransform(25.9,46.5,1,1,0,0,0,2.9,10.5);

	var maskedShapeInstanceList = [this.text2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.text2).wait(1));

	// text1Mask (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("A57F8IAAjwMAu3AAAIAADwg");
	mask_1.setTransform(-165.975,37.989);

	// text1
	this.text1 = new lib.text1();
	this.text1.name = "text1";
	this.text1.parent = this;
	this.text1.setTransform(25.9,43,1,1,0,0,0,2.9,-2);

	var maskedShapeInstanceList = [this.text1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.text1).wait(1));

	// text1_mask (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("Egu3AEEIAAkXMBdvAAAIAAEXg");
	mask_2.setTransform(300.0025,26.0092);

	// text1
	this.d0 = new lib.dollar_text_1();
	this.d0.name = "d0";
	this.d0.parent = this;
	this.d0.setTransform(8,400);

	var maskedShapeInstanceList = [this.d0];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.d0).wait(1));

	// text2_mask (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("Egu3AFKIAAjmMBdvAAAIAADmg");
	mask_3.setTransform(300.0025,33.0011);

	// text2
	this.d1 = new lib.dollar_text_2();
	this.d1.name = "d1";
	this.d1.parent = this;
	this.d1.setTransform(8,420);

	var maskedShapeInstanceList = [this.d1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.d1).wait(1));

	// rest
	this.d2 = new lib.dollar_text_3();
	this.d2.name = "d2";
	this.d2.parent = this;
	this.d2.setTransform(8,440);

	this.timeline.addTween(cjs.Tween.get(this.d2).wait(1));

	// elements
	this.intro_img = new lib.intro_img_1();
	this.intro_img.name = "intro_img";
	this.intro_img.parent = this;
	this.intro_img.setTransform(778.75,33.3,1,1,0,0,0,67.5,42.5);

	this.intro_text = new lib.Symbol10();
	this.intro_text.name = "intro_text";
	this.intro_text.parent = this;
	this.intro_text.setTransform(-423.95,40,1,1,0,0,0,70.4,35);

	this.logobalk = new lib.logobalk_1();
	this.logobalk.name = "logobalk";
	this.logobalk.parent = this;
	this.logobalk.setTransform(879.5,38,1,1,0,0,0,151.5,38);

	this.merkentext = new lib.merken_text();
	this.merkentext.name = "merkentext";
	this.merkentext.parent = this;
	this.merkentext.setTransform(-613.1,70,1,1,0,0,0,116.9,55);

	this.subsubtext = new lib.subsubtext();
	this.subsubtext.name = "subsubtext";
	this.subsubtext.parent = this;
	this.subsubtext.setTransform(744.4,5);

	this.dollRandom32 = new lib.dollar_4();
	this.dollRandom32.name = "dollRandom32";
	this.dollRandom32.parent = this;
	this.dollRandom32.setTransform(210.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom30 = new lib.dollar_4();
	this.dollRandom30.name = "dollRandom30";
	this.dollRandom30.parent = this;
	this.dollRandom30.setTransform(130.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom31 = new lib.dollar_4();
	this.dollRandom31.name = "dollRandom31";
	this.dollRandom31.parent = this;
	this.dollRandom31.setTransform(30.1,-200,1,1,0,0,0,0.1,0);

	this.dollRandom22 = new lib.dollar_5();
	this.dollRandom22.name = "dollRandom22";
	this.dollRandom22.parent = this;
	this.dollRandom22.setTransform(235,-130);

	this.dollRandom20 = new lib.dollar_5();
	this.dollRandom20.name = "dollRandom20";
	this.dollRandom20.parent = this;
	this.dollRandom20.setTransform(130,-126);

	this.dollRandom21 = new lib.dollar_5();
	this.dollRandom21.name = "dollRandom21";
	this.dollRandom21.parent = this;
	this.dollRandom21.setTransform(30,-125);

	this.dollRandom12 = new lib.dollar_3();
	this.dollRandom12.name = "dollRandom12";
	this.dollRandom12.parent = this;
	this.dollRandom12.setTransform(235.05,-80);

	this.dollRandom10 = new lib.dollar_3();
	this.dollRandom10.name = "dollRandom10";
	this.dollRandom10.parent = this;
	this.dollRandom10.setTransform(125,-40);

	this.dollRandom11 = new lib.dollar_3();
	this.dollRandom11.name = "dollRandom11";
	this.dollRandom11.parent = this;
	this.dollRandom11.setTransform(45,-90);

	this.dollRandom00 = new lib.dollar_1_v2();
	this.dollRandom00.name = "dollRandom00";
	this.dollRandom00.parent = this;
	this.dollRandom00.setTransform(160,-40,0.8,0.8);

	this.dollRandom02 = new lib.dollar_1_v2();
	this.dollRandom02.name = "dollRandom02";
	this.dollRandom02.parent = this;
	this.dollRandom02.setTransform(265,-30,0.8,0.8);

	this.dollRandom01 = new lib.dollar_1_v2();
	this.dollRandom01.name = "dollRandom01";
	this.dollRandom01.parent = this;
	this.dollRandom01.setTransform(40,-30,0.8,0.8);

	this.dollarcashback = new lib.dollar_cashback();
	this.dollarcashback.name = "dollarcashback";
	this.dollarcashback.parent = this;
	this.dollarcashback.setTransform(769.95,44.95,1.1143,1.1143);

	this.dollProd1 = new lib.dollar_3();
	this.dollProd1.name = "dollProd1";
	this.dollProd1.parent = this;
	this.dollProd1.setTransform(100,-38.05);

	this.prijs = new lib.dollar_prijs();
	this.prijs.name = "prijs";
	this.prijs.parent = this;
	this.prijs.setTransform(850,48);
	this.prijs.alpha = 0;

	this.servicelabel = new lib.label_service();
	this.servicelabel.name = "servicelabel";
	this.servicelabel.parent = this;
	this.servicelabel.setTransform(920,-100);

	this.cashback = new lib.label_cashback();
	this.cashback.name = "cashback";
	this.cashback.parent = this;
	this.cashback.setTransform(800,173);

	this.g2 = new lib.label_green();
	this.g2.name = "g2";
	this.g2.parent = this;
	this.g2.setTransform(820,75,0.4375,0.4375,0,0,0,0,0.1);

	this.g1 = new lib.label_green();
	this.g1.name = "g1";
	this.g1.parent = this;
	this.g1.setTransform(820,61,0.4375,0.4375);

	this.g0 = new lib.label_green();
	this.g0.name = "g0";
	this.g0.parent = this;
	this.g0.setTransform(820,47,0.4375,0.4375,0,0,0,0,0.1);

	this.left = new lib.arrowLeftRight();
	this.left.name = "left";
	this.left.parent = this;
	this.left.setTransform(225,45,1,1,0,0,180);
	this.left.alpha = 0;
	this.left.visible = false;

	this.right = new lib.arrowLeftRight();
	this.right.name = "right";
	this.right.parent = this;
	this.right.setTransform(410,45);
	this.right.alpha = 0;
	this.right.visible = false;

	this.lineContainer = new lib.price_old_line_container();
	this.lineContainer.name = "lineContainer";
	this.lineContainer.parent = this;
	this.lineContainer.setTransform(-130,17);

	this.price = new lib.emptyMovieClip();
	this.price.name = "price";
	this.price.parent = this;
	this.price.setTransform(450,46);

	this.cta2 = new lib.cta();
	this.cta2.name = "cta2";
	this.cta2.parent = this;
	this.cta2.setTransform(-650,33);

	this.servButton = new lib.services_button();
	this.servButton.name = "servButton";
	this.servButton.parent = this;
	this.servButton.setTransform(500.1,-39.9,1,1,0,0,0,0.1,0.1);

	this.mediamarkt2 = new lib.mediamarkt_logo();
	this.mediamarkt2.name = "mediamarkt2";
	this.mediamarkt2.parent = this;
	this.mediamarkt2.setTransform(60.15,200.1,0.125,0.1245,0,0,0,1.2,0.8);

	this.endframe = new lib.endframe_static();
	this.endframe.name = "endframe";
	this.endframe.parent = this;
	this.endframe.setTransform(-627,22);

	this.vanaf = new lib.vanaf();
	this.vanaf.name = "vanaf";
	this.vanaf.parent = this;
	this.vanaf.setTransform(-224,31);
	this.vanaf.alpha = 0;

	this.logo = new lib.emptyMovieClip();
	this.logo.name = "logo";
	this.logo.parent = this;
	this.logo.setTransform(10,0);
	this.logo.alpha = 0;

	this.campaign = new lib.emptyMovieClip();
	this.campaign.name = "campaign";
	this.campaign.parent = this;
	this.campaign.setTransform(55,20);

	this.cta = new lib.cta();
	this.cta.name = "cta";
	this.cta.parent = this;
	this.cta.setTransform(1000,33);

	this.white2 = new lib.shape3();
	this.white2.name = "white2";
	this.white2.parent = this;
	this.white2.setTransform(672,95.95);
	this.white2.visible = false;

	this.cover2 = new lib.cover();
	this.cover2.name = "cover2";
	this.cover2.parent = this;
	this.cover2.setTransform(728,-90);

	this.text3 = new lib.text3();
	this.text3.name = "text3";
	this.text3.parent = this;
	this.text3.setTransform(7,63);
	this.text3.alpha = 0;

	this.white = new lib.shape2();
	this.white.name = "white";
	this.white.parent = this;
	this.white.setTransform(-30,0);

	this.label = new lib.emptyMovieClip();
	this.label.name = "label";
	this.label.parent = this;
	this.label.setTransform(225,268);

	this.product = new lib.emptyMovieClip();
	this.product.name = "product";
	this.product.parent = this;
	this.product.setTransform(319,45);

	this.grey = new lib.shape_grey();
	this.grey.name = "grey";
	this.grey.parent = this;
	this.grey.setTransform(728,10,0.9999,1);

	this.cover = new lib.cover1();
	this.cover.name = "cover";
	this.cover.parent = this;
	this.cover.setTransform(164,108.3,1.286,1);

	this.mediamarkt = new lib.mediamarkt_logo();
	this.mediamarkt.name = "mediamarkt";
	this.mediamarkt.parent = this;
	this.mediamarkt.setTransform(364.15,110.1,0.2403,0.2404,0,0,0,0.4,0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mediamarkt},{t:this.cover},{t:this.grey},{t:this.product},{t:this.label},{t:this.white},{t:this.text3},{t:this.cover2},{t:this.white2},{t:this.cta},{t:this.campaign},{t:this.logo},{t:this.vanaf},{t:this.endframe},{t:this.mediamarkt2},{t:this.servButton},{t:this.cta2},{t:this.price},{t:this.lineContainer},{t:this.right},{t:this.left},{t:this.g0},{t:this.g1},{t:this.g2},{t:this.cashback},{t:this.servicelabel},{t:this.prijs},{t:this.dollProd1},{t:this.dollarcashback},{t:this.dollRandom01},{t:this.dollRandom02},{t:this.dollRandom00},{t:this.dollRandom11},{t:this.dollRandom10},{t:this.dollRandom12},{t:this.dollRandom21},{t:this.dollRandom20},{t:this.dollRandom22},{t:this.dollRandom31},{t:this.dollRandom30},{t:this.dollRandom32},{t:this.subsubtext},{t:this.merkentext},{t:this.logobalk},{t:this.intro_text},{t:this.intro_img}]}).wait(1));

	// Layer_1
	this.instance = new lib.loopy();
	this.instance.parent = this;
	this.instance.setTransform(728,38,1,1,0,0,0,728,38);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// hitAreaMC
	this.hitAreaMC = new lib.shape1();
	this.hitAreaMC.name = "hitAreaMC";
	this.hitAreaMC.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.hitAreaMC).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-536,-183.9,1992,641.9);
// library properties:
lib.properties = {
	id: 'CB71CC1E45F7401086B269D4402BC0DC',
	width: 728,
	height: 90,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/grey_field.png", id:"grey_field"},
		{src:"images/intro_img.png", id:"intro_img"},
		{src:"images/logobalk.png", id:"logobalk"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['CB71CC1E45F7401086B269D4402BC0DC'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;